﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Collections.Generic.Stack`1<UnityEngine.RectTransform>
struct Stack_1_t253079184;
// System.Collections.Generic.Stack`1<System.Single>
struct Stack_1_t2240656229;
// System.Collections.Generic.Stack`1<uGuiLayout/Mode>
struct Stack_1_t4010345780;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GUITexture
struct GUITexture_t951903601;
// ZoomCamera
struct ZoomCamera_t1350885688;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.CharacterController
struct CharacterController_t1138636865;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// Joystick
struct Joystick_t9498292;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.Sprite
struct Sprite_t280657092;
// Joystick[]
struct JoystickU5BU5D_t4275182589;
// Boundary
struct Boundary_t2442033035;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745544_H
#define U3CMODULEU3E_T692745544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745544 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745544_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef CONSTRAINTAXIS_T4094119807_H
#define CONSTRAINTAXIS_T4094119807_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConstraintAxis
struct  ConstraintAxis_t4094119807 
{
public:
	// System.Int32 ConstraintAxis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConstraintAxis_t4094119807, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINTAXIS_T4094119807_H
#ifndef CONTROLSTATE_T1059495956_H
#define CONTROLSTATE_T1059495956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControlState
struct  ControlState_t1059495956 
{
public:
	// System.Int32 ControlState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ControlState_t1059495956, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLSTATE_T1059495956_H
#ifndef UGUILAYOUT_T2579011462_H
#define UGUILAYOUT_T2579011462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// uGuiLayout
struct  uGuiLayout_t2579011462  : public RuntimeObject
{
public:

public:
};

struct uGuiLayout_t2579011462_StaticFields
{
public:
	// System.Collections.Generic.Stack`1<UnityEngine.RectTransform> uGuiLayout::subControls
	Stack_1_t253079184 * ___subControls_0;
	// UnityEngine.Vector2 uGuiLayout::currentPosition
	Vector2_t2156229523  ___currentPosition_1;
	// System.Collections.Generic.Stack`1<System.Single> uGuiLayout::positionCache
	Stack_1_t2240656229 * ___positionCache_2;
	// System.Collections.Generic.Stack`1<uGuiLayout/Mode> uGuiLayout::modeCache
	Stack_1_t4010345780 * ___modeCache_3;

public:
	inline static int32_t get_offset_of_subControls_0() { return static_cast<int32_t>(offsetof(uGuiLayout_t2579011462_StaticFields, ___subControls_0)); }
	inline Stack_1_t253079184 * get_subControls_0() const { return ___subControls_0; }
	inline Stack_1_t253079184 ** get_address_of_subControls_0() { return &___subControls_0; }
	inline void set_subControls_0(Stack_1_t253079184 * value)
	{
		___subControls_0 = value;
		Il2CppCodeGenWriteBarrier((&___subControls_0), value);
	}

	inline static int32_t get_offset_of_currentPosition_1() { return static_cast<int32_t>(offsetof(uGuiLayout_t2579011462_StaticFields, ___currentPosition_1)); }
	inline Vector2_t2156229523  get_currentPosition_1() const { return ___currentPosition_1; }
	inline Vector2_t2156229523 * get_address_of_currentPosition_1() { return &___currentPosition_1; }
	inline void set_currentPosition_1(Vector2_t2156229523  value)
	{
		___currentPosition_1 = value;
	}

	inline static int32_t get_offset_of_positionCache_2() { return static_cast<int32_t>(offsetof(uGuiLayout_t2579011462_StaticFields, ___positionCache_2)); }
	inline Stack_1_t2240656229 * get_positionCache_2() const { return ___positionCache_2; }
	inline Stack_1_t2240656229 ** get_address_of_positionCache_2() { return &___positionCache_2; }
	inline void set_positionCache_2(Stack_1_t2240656229 * value)
	{
		___positionCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___positionCache_2), value);
	}

	inline static int32_t get_offset_of_modeCache_3() { return static_cast<int32_t>(offsetof(uGuiLayout_t2579011462_StaticFields, ___modeCache_3)); }
	inline Stack_1_t4010345780 * get_modeCache_3() const { return ___modeCache_3; }
	inline Stack_1_t4010345780 ** get_address_of_modeCache_3() { return &___modeCache_3; }
	inline void set_modeCache_3(Stack_1_t4010345780 * value)
	{
		___modeCache_3 = value;
		Il2CppCodeGenWriteBarrier((&___modeCache_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUILAYOUT_T2579011462_H
#ifndef MODE_T3166956325_H
#define MODE_T3166956325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// uGuiLayout/Mode
struct  Mode_t3166956325 
{
public:
	// System.Int32 uGuiLayout/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t3166956325, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T3166956325_H
#ifndef BOUNDARY_T2442033035_H
#define BOUNDARY_T2442033035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boundary
struct  Boundary_t2442033035  : public RuntimeObject
{
public:
	// UnityEngine.Vector2 Boundary::min
	Vector2_t2156229523  ___min_0;
	// UnityEngine.Vector2 Boundary::max
	Vector2_t2156229523  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Boundary_t2442033035, ___min_0)); }
	inline Vector2_t2156229523  get_min_0() const { return ___min_0; }
	inline Vector2_t2156229523 * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_t2156229523  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Boundary_t2442033035, ___max_1)); }
	inline Vector2_t2156229523  get_max_1() const { return ___max_1; }
	inline Vector2_t2156229523 * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_t2156229523  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDARY_T2442033035_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef DONECAMERAMOVEMENT_T1316814223_H
#define DONECAMERAMOVEMENT_T1316814223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DoneCameraMovement
struct  DoneCameraMovement_t1316814223  : public MonoBehaviour_t3962482529
{
public:
	// System.Single DoneCameraMovement::smooth
	float ___smooth_2;
	// System.Boolean DoneCameraMovement::extraCameraAdjust
	bool ___extraCameraAdjust_3;
	// UnityEngine.Transform DoneCameraMovement::player
	Transform_t3600365921 * ___player_4;
	// UnityEngine.Vector3 DoneCameraMovement::relCameraPos
	Vector3_t3722313464  ___relCameraPos_5;
	// System.Single DoneCameraMovement::relCameraPosMag
	float ___relCameraPosMag_6;
	// UnityEngine.Vector3 DoneCameraMovement::newPos
	Vector3_t3722313464  ___newPos_7;

public:
	inline static int32_t get_offset_of_smooth_2() { return static_cast<int32_t>(offsetof(DoneCameraMovement_t1316814223, ___smooth_2)); }
	inline float get_smooth_2() const { return ___smooth_2; }
	inline float* get_address_of_smooth_2() { return &___smooth_2; }
	inline void set_smooth_2(float value)
	{
		___smooth_2 = value;
	}

	inline static int32_t get_offset_of_extraCameraAdjust_3() { return static_cast<int32_t>(offsetof(DoneCameraMovement_t1316814223, ___extraCameraAdjust_3)); }
	inline bool get_extraCameraAdjust_3() const { return ___extraCameraAdjust_3; }
	inline bool* get_address_of_extraCameraAdjust_3() { return &___extraCameraAdjust_3; }
	inline void set_extraCameraAdjust_3(bool value)
	{
		___extraCameraAdjust_3 = value;
	}

	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(DoneCameraMovement_t1316814223, ___player_4)); }
	inline Transform_t3600365921 * get_player_4() const { return ___player_4; }
	inline Transform_t3600365921 ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(Transform_t3600365921 * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_relCameraPos_5() { return static_cast<int32_t>(offsetof(DoneCameraMovement_t1316814223, ___relCameraPos_5)); }
	inline Vector3_t3722313464  get_relCameraPos_5() const { return ___relCameraPos_5; }
	inline Vector3_t3722313464 * get_address_of_relCameraPos_5() { return &___relCameraPos_5; }
	inline void set_relCameraPos_5(Vector3_t3722313464  value)
	{
		___relCameraPos_5 = value;
	}

	inline static int32_t get_offset_of_relCameraPosMag_6() { return static_cast<int32_t>(offsetof(DoneCameraMovement_t1316814223, ___relCameraPosMag_6)); }
	inline float get_relCameraPosMag_6() const { return ___relCameraPosMag_6; }
	inline float* get_address_of_relCameraPosMag_6() { return &___relCameraPosMag_6; }
	inline void set_relCameraPosMag_6(float value)
	{
		___relCameraPosMag_6 = value;
	}

	inline static int32_t get_offset_of_newPos_7() { return static_cast<int32_t>(offsetof(DoneCameraMovement_t1316814223, ___newPos_7)); }
	inline Vector3_t3722313464  get_newPos_7() const { return ___newPos_7; }
	inline Vector3_t3722313464 * get_address_of_newPos_7() { return &___newPos_7; }
	inline void set_newPos_7(Vector3_t3722313464  value)
	{
		___newPos_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONECAMERAMOVEMENT_T1316814223_H
#ifndef TAPCONTROL_T522795363_H
#define TAPCONTROL_T522795363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// tapcontrol
struct  tapcontrol_t522795363  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject tapcontrol::cameraObject
	GameObject_t1113636619 * ___cameraObject_2;
	// UnityEngine.Transform tapcontrol::cameraPivot
	Transform_t3600365921 * ___cameraPivot_3;
	// UnityEngine.GUITexture tapcontrol::jumpButton
	GUITexture_t951903601 * ___jumpButton_4;
	// System.Single tapcontrol::speed
	float ___speed_5;
	// System.Single tapcontrol::jumpSpeed
	float ___jumpSpeed_6;
	// System.Single tapcontrol::inAirMultiplier
	float ___inAirMultiplier_7;
	// System.Single tapcontrol::minimumDistanceToMove
	float ___minimumDistanceToMove_8;
	// System.Single tapcontrol::minimumTimeUntilMove
	float ___minimumTimeUntilMove_9;
	// System.Boolean tapcontrol::zoomEnabled
	bool ___zoomEnabled_10;
	// System.Single tapcontrol::zoomEpsilon
	float ___zoomEpsilon_11;
	// System.Single tapcontrol::zoomRate
	float ___zoomRate_12;
	// System.Boolean tapcontrol::rotateEnabled
	bool ___rotateEnabled_13;
	// System.Single tapcontrol::rotateEpsilon
	float ___rotateEpsilon_14;
	// ZoomCamera tapcontrol::zoomCamera
	ZoomCamera_t1350885688 * ___zoomCamera_15;
	// UnityEngine.Camera tapcontrol::cam
	Camera_t4157153871 * ___cam_16;
	// UnityEngine.Transform tapcontrol::thisTransform
	Transform_t3600365921 * ___thisTransform_17;
	// UnityEngine.CharacterController tapcontrol::character
	CharacterController_t1138636865 * ___character_18;
	// UnityEngine.Vector3 tapcontrol::targetLocation
	Vector3_t3722313464  ___targetLocation_19;
	// System.Boolean tapcontrol::moving
	bool ___moving_20;
	// System.Single tapcontrol::rotationTarget
	float ___rotationTarget_21;
	// System.Single tapcontrol::rotationVelocity
	float ___rotationVelocity_22;
	// UnityEngine.Vector3 tapcontrol::velocity
	Vector3_t3722313464  ___velocity_23;
	// ControlState tapcontrol::state
	int32_t ___state_24;
	// System.Int32[] tapcontrol::fingerDown
	Int32U5BU5D_t385246372* ___fingerDown_25;
	// UnityEngine.Vector2[] tapcontrol::fingerDownPosition
	Vector2U5BU5D_t1457185986* ___fingerDownPosition_26;
	// System.Int32[] tapcontrol::fingerDownFrame
	Int32U5BU5D_t385246372* ___fingerDownFrame_27;
	// System.Single tapcontrol::firstTouchTime
	float ___firstTouchTime_28;

public:
	inline static int32_t get_offset_of_cameraObject_2() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___cameraObject_2)); }
	inline GameObject_t1113636619 * get_cameraObject_2() const { return ___cameraObject_2; }
	inline GameObject_t1113636619 ** get_address_of_cameraObject_2() { return &___cameraObject_2; }
	inline void set_cameraObject_2(GameObject_t1113636619 * value)
	{
		___cameraObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___cameraObject_2), value);
	}

	inline static int32_t get_offset_of_cameraPivot_3() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___cameraPivot_3)); }
	inline Transform_t3600365921 * get_cameraPivot_3() const { return ___cameraPivot_3; }
	inline Transform_t3600365921 ** get_address_of_cameraPivot_3() { return &___cameraPivot_3; }
	inline void set_cameraPivot_3(Transform_t3600365921 * value)
	{
		___cameraPivot_3 = value;
		Il2CppCodeGenWriteBarrier((&___cameraPivot_3), value);
	}

	inline static int32_t get_offset_of_jumpButton_4() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___jumpButton_4)); }
	inline GUITexture_t951903601 * get_jumpButton_4() const { return ___jumpButton_4; }
	inline GUITexture_t951903601 ** get_address_of_jumpButton_4() { return &___jumpButton_4; }
	inline void set_jumpButton_4(GUITexture_t951903601 * value)
	{
		___jumpButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___jumpButton_4), value);
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_jumpSpeed_6() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___jumpSpeed_6)); }
	inline float get_jumpSpeed_6() const { return ___jumpSpeed_6; }
	inline float* get_address_of_jumpSpeed_6() { return &___jumpSpeed_6; }
	inline void set_jumpSpeed_6(float value)
	{
		___jumpSpeed_6 = value;
	}

	inline static int32_t get_offset_of_inAirMultiplier_7() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___inAirMultiplier_7)); }
	inline float get_inAirMultiplier_7() const { return ___inAirMultiplier_7; }
	inline float* get_address_of_inAirMultiplier_7() { return &___inAirMultiplier_7; }
	inline void set_inAirMultiplier_7(float value)
	{
		___inAirMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_minimumDistanceToMove_8() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___minimumDistanceToMove_8)); }
	inline float get_minimumDistanceToMove_8() const { return ___minimumDistanceToMove_8; }
	inline float* get_address_of_minimumDistanceToMove_8() { return &___minimumDistanceToMove_8; }
	inline void set_minimumDistanceToMove_8(float value)
	{
		___minimumDistanceToMove_8 = value;
	}

	inline static int32_t get_offset_of_minimumTimeUntilMove_9() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___minimumTimeUntilMove_9)); }
	inline float get_minimumTimeUntilMove_9() const { return ___minimumTimeUntilMove_9; }
	inline float* get_address_of_minimumTimeUntilMove_9() { return &___minimumTimeUntilMove_9; }
	inline void set_minimumTimeUntilMove_9(float value)
	{
		___minimumTimeUntilMove_9 = value;
	}

	inline static int32_t get_offset_of_zoomEnabled_10() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___zoomEnabled_10)); }
	inline bool get_zoomEnabled_10() const { return ___zoomEnabled_10; }
	inline bool* get_address_of_zoomEnabled_10() { return &___zoomEnabled_10; }
	inline void set_zoomEnabled_10(bool value)
	{
		___zoomEnabled_10 = value;
	}

	inline static int32_t get_offset_of_zoomEpsilon_11() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___zoomEpsilon_11)); }
	inline float get_zoomEpsilon_11() const { return ___zoomEpsilon_11; }
	inline float* get_address_of_zoomEpsilon_11() { return &___zoomEpsilon_11; }
	inline void set_zoomEpsilon_11(float value)
	{
		___zoomEpsilon_11 = value;
	}

	inline static int32_t get_offset_of_zoomRate_12() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___zoomRate_12)); }
	inline float get_zoomRate_12() const { return ___zoomRate_12; }
	inline float* get_address_of_zoomRate_12() { return &___zoomRate_12; }
	inline void set_zoomRate_12(float value)
	{
		___zoomRate_12 = value;
	}

	inline static int32_t get_offset_of_rotateEnabled_13() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___rotateEnabled_13)); }
	inline bool get_rotateEnabled_13() const { return ___rotateEnabled_13; }
	inline bool* get_address_of_rotateEnabled_13() { return &___rotateEnabled_13; }
	inline void set_rotateEnabled_13(bool value)
	{
		___rotateEnabled_13 = value;
	}

	inline static int32_t get_offset_of_rotateEpsilon_14() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___rotateEpsilon_14)); }
	inline float get_rotateEpsilon_14() const { return ___rotateEpsilon_14; }
	inline float* get_address_of_rotateEpsilon_14() { return &___rotateEpsilon_14; }
	inline void set_rotateEpsilon_14(float value)
	{
		___rotateEpsilon_14 = value;
	}

	inline static int32_t get_offset_of_zoomCamera_15() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___zoomCamera_15)); }
	inline ZoomCamera_t1350885688 * get_zoomCamera_15() const { return ___zoomCamera_15; }
	inline ZoomCamera_t1350885688 ** get_address_of_zoomCamera_15() { return &___zoomCamera_15; }
	inline void set_zoomCamera_15(ZoomCamera_t1350885688 * value)
	{
		___zoomCamera_15 = value;
		Il2CppCodeGenWriteBarrier((&___zoomCamera_15), value);
	}

	inline static int32_t get_offset_of_cam_16() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___cam_16)); }
	inline Camera_t4157153871 * get_cam_16() const { return ___cam_16; }
	inline Camera_t4157153871 ** get_address_of_cam_16() { return &___cam_16; }
	inline void set_cam_16(Camera_t4157153871 * value)
	{
		___cam_16 = value;
		Il2CppCodeGenWriteBarrier((&___cam_16), value);
	}

	inline static int32_t get_offset_of_thisTransform_17() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___thisTransform_17)); }
	inline Transform_t3600365921 * get_thisTransform_17() const { return ___thisTransform_17; }
	inline Transform_t3600365921 ** get_address_of_thisTransform_17() { return &___thisTransform_17; }
	inline void set_thisTransform_17(Transform_t3600365921 * value)
	{
		___thisTransform_17 = value;
		Il2CppCodeGenWriteBarrier((&___thisTransform_17), value);
	}

	inline static int32_t get_offset_of_character_18() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___character_18)); }
	inline CharacterController_t1138636865 * get_character_18() const { return ___character_18; }
	inline CharacterController_t1138636865 ** get_address_of_character_18() { return &___character_18; }
	inline void set_character_18(CharacterController_t1138636865 * value)
	{
		___character_18 = value;
		Il2CppCodeGenWriteBarrier((&___character_18), value);
	}

	inline static int32_t get_offset_of_targetLocation_19() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___targetLocation_19)); }
	inline Vector3_t3722313464  get_targetLocation_19() const { return ___targetLocation_19; }
	inline Vector3_t3722313464 * get_address_of_targetLocation_19() { return &___targetLocation_19; }
	inline void set_targetLocation_19(Vector3_t3722313464  value)
	{
		___targetLocation_19 = value;
	}

	inline static int32_t get_offset_of_moving_20() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___moving_20)); }
	inline bool get_moving_20() const { return ___moving_20; }
	inline bool* get_address_of_moving_20() { return &___moving_20; }
	inline void set_moving_20(bool value)
	{
		___moving_20 = value;
	}

	inline static int32_t get_offset_of_rotationTarget_21() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___rotationTarget_21)); }
	inline float get_rotationTarget_21() const { return ___rotationTarget_21; }
	inline float* get_address_of_rotationTarget_21() { return &___rotationTarget_21; }
	inline void set_rotationTarget_21(float value)
	{
		___rotationTarget_21 = value;
	}

	inline static int32_t get_offset_of_rotationVelocity_22() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___rotationVelocity_22)); }
	inline float get_rotationVelocity_22() const { return ___rotationVelocity_22; }
	inline float* get_address_of_rotationVelocity_22() { return &___rotationVelocity_22; }
	inline void set_rotationVelocity_22(float value)
	{
		___rotationVelocity_22 = value;
	}

	inline static int32_t get_offset_of_velocity_23() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___velocity_23)); }
	inline Vector3_t3722313464  get_velocity_23() const { return ___velocity_23; }
	inline Vector3_t3722313464 * get_address_of_velocity_23() { return &___velocity_23; }
	inline void set_velocity_23(Vector3_t3722313464  value)
	{
		___velocity_23 = value;
	}

	inline static int32_t get_offset_of_state_24() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___state_24)); }
	inline int32_t get_state_24() const { return ___state_24; }
	inline int32_t* get_address_of_state_24() { return &___state_24; }
	inline void set_state_24(int32_t value)
	{
		___state_24 = value;
	}

	inline static int32_t get_offset_of_fingerDown_25() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___fingerDown_25)); }
	inline Int32U5BU5D_t385246372* get_fingerDown_25() const { return ___fingerDown_25; }
	inline Int32U5BU5D_t385246372** get_address_of_fingerDown_25() { return &___fingerDown_25; }
	inline void set_fingerDown_25(Int32U5BU5D_t385246372* value)
	{
		___fingerDown_25 = value;
		Il2CppCodeGenWriteBarrier((&___fingerDown_25), value);
	}

	inline static int32_t get_offset_of_fingerDownPosition_26() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___fingerDownPosition_26)); }
	inline Vector2U5BU5D_t1457185986* get_fingerDownPosition_26() const { return ___fingerDownPosition_26; }
	inline Vector2U5BU5D_t1457185986** get_address_of_fingerDownPosition_26() { return &___fingerDownPosition_26; }
	inline void set_fingerDownPosition_26(Vector2U5BU5D_t1457185986* value)
	{
		___fingerDownPosition_26 = value;
		Il2CppCodeGenWriteBarrier((&___fingerDownPosition_26), value);
	}

	inline static int32_t get_offset_of_fingerDownFrame_27() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___fingerDownFrame_27)); }
	inline Int32U5BU5D_t385246372* get_fingerDownFrame_27() const { return ___fingerDownFrame_27; }
	inline Int32U5BU5D_t385246372** get_address_of_fingerDownFrame_27() { return &___fingerDownFrame_27; }
	inline void set_fingerDownFrame_27(Int32U5BU5D_t385246372* value)
	{
		___fingerDownFrame_27 = value;
		Il2CppCodeGenWriteBarrier((&___fingerDownFrame_27), value);
	}

	inline static int32_t get_offset_of_firstTouchTime_28() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___firstTouchTime_28)); }
	inline float get_firstTouchTime_28() const { return ___firstTouchTime_28; }
	inline float* get_address_of_firstTouchTime_28() { return &___firstTouchTime_28; }
	inline void set_firstTouchTime_28(float value)
	{
		___firstTouchTime_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPCONTROL_T522795363_H
#ifndef ZOOMCAMERA_T1350885688_H
#define ZOOMCAMERA_T1350885688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZoomCamera
struct  ZoomCamera_t1350885688  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform ZoomCamera::origin
	Transform_t3600365921 * ___origin_2;
	// System.Single ZoomCamera::zoom
	float ___zoom_3;
	// System.Single ZoomCamera::zoomMin
	float ___zoomMin_4;
	// System.Single ZoomCamera::zoomMax
	float ___zoomMax_5;
	// System.Single ZoomCamera::seekTime
	float ___seekTime_6;
	// System.Boolean ZoomCamera::smoothZoomIn
	bool ___smoothZoomIn_7;
	// UnityEngine.Vector3 ZoomCamera::defaultLocalPosition
	Vector3_t3722313464  ___defaultLocalPosition_8;
	// UnityEngine.Transform ZoomCamera::thisTransform
	Transform_t3600365921 * ___thisTransform_9;
	// System.Single ZoomCamera::currentZoom
	float ___currentZoom_10;
	// System.Single ZoomCamera::targetZoom
	float ___targetZoom_11;
	// System.Single ZoomCamera::zoomVelocity
	float ___zoomVelocity_12;

public:
	inline static int32_t get_offset_of_origin_2() { return static_cast<int32_t>(offsetof(ZoomCamera_t1350885688, ___origin_2)); }
	inline Transform_t3600365921 * get_origin_2() const { return ___origin_2; }
	inline Transform_t3600365921 ** get_address_of_origin_2() { return &___origin_2; }
	inline void set_origin_2(Transform_t3600365921 * value)
	{
		___origin_2 = value;
		Il2CppCodeGenWriteBarrier((&___origin_2), value);
	}

	inline static int32_t get_offset_of_zoom_3() { return static_cast<int32_t>(offsetof(ZoomCamera_t1350885688, ___zoom_3)); }
	inline float get_zoom_3() const { return ___zoom_3; }
	inline float* get_address_of_zoom_3() { return &___zoom_3; }
	inline void set_zoom_3(float value)
	{
		___zoom_3 = value;
	}

	inline static int32_t get_offset_of_zoomMin_4() { return static_cast<int32_t>(offsetof(ZoomCamera_t1350885688, ___zoomMin_4)); }
	inline float get_zoomMin_4() const { return ___zoomMin_4; }
	inline float* get_address_of_zoomMin_4() { return &___zoomMin_4; }
	inline void set_zoomMin_4(float value)
	{
		___zoomMin_4 = value;
	}

	inline static int32_t get_offset_of_zoomMax_5() { return static_cast<int32_t>(offsetof(ZoomCamera_t1350885688, ___zoomMax_5)); }
	inline float get_zoomMax_5() const { return ___zoomMax_5; }
	inline float* get_address_of_zoomMax_5() { return &___zoomMax_5; }
	inline void set_zoomMax_5(float value)
	{
		___zoomMax_5 = value;
	}

	inline static int32_t get_offset_of_seekTime_6() { return static_cast<int32_t>(offsetof(ZoomCamera_t1350885688, ___seekTime_6)); }
	inline float get_seekTime_6() const { return ___seekTime_6; }
	inline float* get_address_of_seekTime_6() { return &___seekTime_6; }
	inline void set_seekTime_6(float value)
	{
		___seekTime_6 = value;
	}

	inline static int32_t get_offset_of_smoothZoomIn_7() { return static_cast<int32_t>(offsetof(ZoomCamera_t1350885688, ___smoothZoomIn_7)); }
	inline bool get_smoothZoomIn_7() const { return ___smoothZoomIn_7; }
	inline bool* get_address_of_smoothZoomIn_7() { return &___smoothZoomIn_7; }
	inline void set_smoothZoomIn_7(bool value)
	{
		___smoothZoomIn_7 = value;
	}

	inline static int32_t get_offset_of_defaultLocalPosition_8() { return static_cast<int32_t>(offsetof(ZoomCamera_t1350885688, ___defaultLocalPosition_8)); }
	inline Vector3_t3722313464  get_defaultLocalPosition_8() const { return ___defaultLocalPosition_8; }
	inline Vector3_t3722313464 * get_address_of_defaultLocalPosition_8() { return &___defaultLocalPosition_8; }
	inline void set_defaultLocalPosition_8(Vector3_t3722313464  value)
	{
		___defaultLocalPosition_8 = value;
	}

	inline static int32_t get_offset_of_thisTransform_9() { return static_cast<int32_t>(offsetof(ZoomCamera_t1350885688, ___thisTransform_9)); }
	inline Transform_t3600365921 * get_thisTransform_9() const { return ___thisTransform_9; }
	inline Transform_t3600365921 ** get_address_of_thisTransform_9() { return &___thisTransform_9; }
	inline void set_thisTransform_9(Transform_t3600365921 * value)
	{
		___thisTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___thisTransform_9), value);
	}

	inline static int32_t get_offset_of_currentZoom_10() { return static_cast<int32_t>(offsetof(ZoomCamera_t1350885688, ___currentZoom_10)); }
	inline float get_currentZoom_10() const { return ___currentZoom_10; }
	inline float* get_address_of_currentZoom_10() { return &___currentZoom_10; }
	inline void set_currentZoom_10(float value)
	{
		___currentZoom_10 = value;
	}

	inline static int32_t get_offset_of_targetZoom_11() { return static_cast<int32_t>(offsetof(ZoomCamera_t1350885688, ___targetZoom_11)); }
	inline float get_targetZoom_11() const { return ___targetZoom_11; }
	inline float* get_address_of_targetZoom_11() { return &___targetZoom_11; }
	inline void set_targetZoom_11(float value)
	{
		___targetZoom_11 = value;
	}

	inline static int32_t get_offset_of_zoomVelocity_12() { return static_cast<int32_t>(offsetof(ZoomCamera_t1350885688, ___zoomVelocity_12)); }
	inline float get_zoomVelocity_12() const { return ___zoomVelocity_12; }
	inline float* get_address_of_zoomVelocity_12() { return &___zoomVelocity_12; }
	inline void set_zoomVelocity_12(float value)
	{
		___zoomVelocity_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOOMCAMERA_T1350885688_H
#ifndef CAMERARELATIVECONTROL_T2453217546_H
#define CAMERARELATIVECONTROL_T2453217546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraRelativeControl
struct  CameraRelativeControl_t2453217546  : public MonoBehaviour_t3962482529
{
public:
	// Joystick CameraRelativeControl::moveJoystick
	Joystick_t9498292 * ___moveJoystick_2;
	// Joystick CameraRelativeControl::rotateJoystick
	Joystick_t9498292 * ___rotateJoystick_3;
	// UnityEngine.Transform CameraRelativeControl::cameraPivot
	Transform_t3600365921 * ___cameraPivot_4;
	// UnityEngine.Transform CameraRelativeControl::cameraTransform
	Transform_t3600365921 * ___cameraTransform_5;
	// System.Single CameraRelativeControl::speed
	float ___speed_6;
	// System.Single CameraRelativeControl::jumpSpeed
	float ___jumpSpeed_7;
	// System.Single CameraRelativeControl::inAirMultiplier
	float ___inAirMultiplier_8;
	// UnityEngine.Vector2 CameraRelativeControl::rotationSpeed
	Vector2_t2156229523  ___rotationSpeed_9;
	// UnityEngine.Transform CameraRelativeControl::thisTransform
	Transform_t3600365921 * ___thisTransform_10;
	// UnityEngine.CharacterController CameraRelativeControl::character
	CharacterController_t1138636865 * ___character_11;
	// UnityEngine.Vector3 CameraRelativeControl::velocity
	Vector3_t3722313464  ___velocity_12;
	// System.Boolean CameraRelativeControl::canJump
	bool ___canJump_13;

public:
	inline static int32_t get_offset_of_moveJoystick_2() { return static_cast<int32_t>(offsetof(CameraRelativeControl_t2453217546, ___moveJoystick_2)); }
	inline Joystick_t9498292 * get_moveJoystick_2() const { return ___moveJoystick_2; }
	inline Joystick_t9498292 ** get_address_of_moveJoystick_2() { return &___moveJoystick_2; }
	inline void set_moveJoystick_2(Joystick_t9498292 * value)
	{
		___moveJoystick_2 = value;
		Il2CppCodeGenWriteBarrier((&___moveJoystick_2), value);
	}

	inline static int32_t get_offset_of_rotateJoystick_3() { return static_cast<int32_t>(offsetof(CameraRelativeControl_t2453217546, ___rotateJoystick_3)); }
	inline Joystick_t9498292 * get_rotateJoystick_3() const { return ___rotateJoystick_3; }
	inline Joystick_t9498292 ** get_address_of_rotateJoystick_3() { return &___rotateJoystick_3; }
	inline void set_rotateJoystick_3(Joystick_t9498292 * value)
	{
		___rotateJoystick_3 = value;
		Il2CppCodeGenWriteBarrier((&___rotateJoystick_3), value);
	}

	inline static int32_t get_offset_of_cameraPivot_4() { return static_cast<int32_t>(offsetof(CameraRelativeControl_t2453217546, ___cameraPivot_4)); }
	inline Transform_t3600365921 * get_cameraPivot_4() const { return ___cameraPivot_4; }
	inline Transform_t3600365921 ** get_address_of_cameraPivot_4() { return &___cameraPivot_4; }
	inline void set_cameraPivot_4(Transform_t3600365921 * value)
	{
		___cameraPivot_4 = value;
		Il2CppCodeGenWriteBarrier((&___cameraPivot_4), value);
	}

	inline static int32_t get_offset_of_cameraTransform_5() { return static_cast<int32_t>(offsetof(CameraRelativeControl_t2453217546, ___cameraTransform_5)); }
	inline Transform_t3600365921 * get_cameraTransform_5() const { return ___cameraTransform_5; }
	inline Transform_t3600365921 ** get_address_of_cameraTransform_5() { return &___cameraTransform_5; }
	inline void set_cameraTransform_5(Transform_t3600365921 * value)
	{
		___cameraTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_5), value);
	}

	inline static int32_t get_offset_of_speed_6() { return static_cast<int32_t>(offsetof(CameraRelativeControl_t2453217546, ___speed_6)); }
	inline float get_speed_6() const { return ___speed_6; }
	inline float* get_address_of_speed_6() { return &___speed_6; }
	inline void set_speed_6(float value)
	{
		___speed_6 = value;
	}

	inline static int32_t get_offset_of_jumpSpeed_7() { return static_cast<int32_t>(offsetof(CameraRelativeControl_t2453217546, ___jumpSpeed_7)); }
	inline float get_jumpSpeed_7() const { return ___jumpSpeed_7; }
	inline float* get_address_of_jumpSpeed_7() { return &___jumpSpeed_7; }
	inline void set_jumpSpeed_7(float value)
	{
		___jumpSpeed_7 = value;
	}

	inline static int32_t get_offset_of_inAirMultiplier_8() { return static_cast<int32_t>(offsetof(CameraRelativeControl_t2453217546, ___inAirMultiplier_8)); }
	inline float get_inAirMultiplier_8() const { return ___inAirMultiplier_8; }
	inline float* get_address_of_inAirMultiplier_8() { return &___inAirMultiplier_8; }
	inline void set_inAirMultiplier_8(float value)
	{
		___inAirMultiplier_8 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_9() { return static_cast<int32_t>(offsetof(CameraRelativeControl_t2453217546, ___rotationSpeed_9)); }
	inline Vector2_t2156229523  get_rotationSpeed_9() const { return ___rotationSpeed_9; }
	inline Vector2_t2156229523 * get_address_of_rotationSpeed_9() { return &___rotationSpeed_9; }
	inline void set_rotationSpeed_9(Vector2_t2156229523  value)
	{
		___rotationSpeed_9 = value;
	}

	inline static int32_t get_offset_of_thisTransform_10() { return static_cast<int32_t>(offsetof(CameraRelativeControl_t2453217546, ___thisTransform_10)); }
	inline Transform_t3600365921 * get_thisTransform_10() const { return ___thisTransform_10; }
	inline Transform_t3600365921 ** get_address_of_thisTransform_10() { return &___thisTransform_10; }
	inline void set_thisTransform_10(Transform_t3600365921 * value)
	{
		___thisTransform_10 = value;
		Il2CppCodeGenWriteBarrier((&___thisTransform_10), value);
	}

	inline static int32_t get_offset_of_character_11() { return static_cast<int32_t>(offsetof(CameraRelativeControl_t2453217546, ___character_11)); }
	inline CharacterController_t1138636865 * get_character_11() const { return ___character_11; }
	inline CharacterController_t1138636865 ** get_address_of_character_11() { return &___character_11; }
	inline void set_character_11(CharacterController_t1138636865 * value)
	{
		___character_11 = value;
		Il2CppCodeGenWriteBarrier((&___character_11), value);
	}

	inline static int32_t get_offset_of_velocity_12() { return static_cast<int32_t>(offsetof(CameraRelativeControl_t2453217546, ___velocity_12)); }
	inline Vector3_t3722313464  get_velocity_12() const { return ___velocity_12; }
	inline Vector3_t3722313464 * get_address_of_velocity_12() { return &___velocity_12; }
	inline void set_velocity_12(Vector3_t3722313464  value)
	{
		___velocity_12 = value;
	}

	inline static int32_t get_offset_of_canJump_13() { return static_cast<int32_t>(offsetof(CameraRelativeControl_t2453217546, ___canJump_13)); }
	inline bool get_canJump_13() const { return ___canJump_13; }
	inline bool* get_address_of_canJump_13() { return &___canJump_13; }
	inline void set_canJump_13(bool value)
	{
		___canJump_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERARELATIVECONTROL_T2453217546_H
#ifndef FIRSTPERSONCONTROL_T610459381_H
#define FIRSTPERSONCONTROL_T610459381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FirstPersonControl
struct  FirstPersonControl_t610459381  : public MonoBehaviour_t3962482529
{
public:
	// Joystick FirstPersonControl::moveTouchPad
	Joystick_t9498292 * ___moveTouchPad_2;
	// Joystick FirstPersonControl::rotateTouchPad
	Joystick_t9498292 * ___rotateTouchPad_3;
	// UnityEngine.Transform FirstPersonControl::cameraPivot
	Transform_t3600365921 * ___cameraPivot_4;
	// System.Single FirstPersonControl::forwardSpeed
	float ___forwardSpeed_5;
	// System.Single FirstPersonControl::backwardSpeed
	float ___backwardSpeed_6;
	// System.Single FirstPersonControl::sidestepSpeed
	float ___sidestepSpeed_7;
	// System.Single FirstPersonControl::jumpSpeed
	float ___jumpSpeed_8;
	// System.Single FirstPersonControl::inAirMultiplier
	float ___inAirMultiplier_9;
	// UnityEngine.Vector2 FirstPersonControl::rotationSpeed
	Vector2_t2156229523  ___rotationSpeed_10;
	// System.Single FirstPersonControl::tiltPositiveYAxis
	float ___tiltPositiveYAxis_11;
	// System.Single FirstPersonControl::tiltNegativeYAxis
	float ___tiltNegativeYAxis_12;
	// System.Single FirstPersonControl::tiltXAxisMinimum
	float ___tiltXAxisMinimum_13;
	// UnityEngine.Transform FirstPersonControl::thisTransform
	Transform_t3600365921 * ___thisTransform_14;
	// UnityEngine.CharacterController FirstPersonControl::character
	CharacterController_t1138636865 * ___character_15;
	// UnityEngine.Vector3 FirstPersonControl::cameraVelocity
	Vector3_t3722313464  ___cameraVelocity_16;
	// UnityEngine.Vector3 FirstPersonControl::velocity
	Vector3_t3722313464  ___velocity_17;
	// System.Boolean FirstPersonControl::canJump
	bool ___canJump_18;

public:
	inline static int32_t get_offset_of_moveTouchPad_2() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___moveTouchPad_2)); }
	inline Joystick_t9498292 * get_moveTouchPad_2() const { return ___moveTouchPad_2; }
	inline Joystick_t9498292 ** get_address_of_moveTouchPad_2() { return &___moveTouchPad_2; }
	inline void set_moveTouchPad_2(Joystick_t9498292 * value)
	{
		___moveTouchPad_2 = value;
		Il2CppCodeGenWriteBarrier((&___moveTouchPad_2), value);
	}

	inline static int32_t get_offset_of_rotateTouchPad_3() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___rotateTouchPad_3)); }
	inline Joystick_t9498292 * get_rotateTouchPad_3() const { return ___rotateTouchPad_3; }
	inline Joystick_t9498292 ** get_address_of_rotateTouchPad_3() { return &___rotateTouchPad_3; }
	inline void set_rotateTouchPad_3(Joystick_t9498292 * value)
	{
		___rotateTouchPad_3 = value;
		Il2CppCodeGenWriteBarrier((&___rotateTouchPad_3), value);
	}

	inline static int32_t get_offset_of_cameraPivot_4() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___cameraPivot_4)); }
	inline Transform_t3600365921 * get_cameraPivot_4() const { return ___cameraPivot_4; }
	inline Transform_t3600365921 ** get_address_of_cameraPivot_4() { return &___cameraPivot_4; }
	inline void set_cameraPivot_4(Transform_t3600365921 * value)
	{
		___cameraPivot_4 = value;
		Il2CppCodeGenWriteBarrier((&___cameraPivot_4), value);
	}

	inline static int32_t get_offset_of_forwardSpeed_5() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___forwardSpeed_5)); }
	inline float get_forwardSpeed_5() const { return ___forwardSpeed_5; }
	inline float* get_address_of_forwardSpeed_5() { return &___forwardSpeed_5; }
	inline void set_forwardSpeed_5(float value)
	{
		___forwardSpeed_5 = value;
	}

	inline static int32_t get_offset_of_backwardSpeed_6() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___backwardSpeed_6)); }
	inline float get_backwardSpeed_6() const { return ___backwardSpeed_6; }
	inline float* get_address_of_backwardSpeed_6() { return &___backwardSpeed_6; }
	inline void set_backwardSpeed_6(float value)
	{
		___backwardSpeed_6 = value;
	}

	inline static int32_t get_offset_of_sidestepSpeed_7() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___sidestepSpeed_7)); }
	inline float get_sidestepSpeed_7() const { return ___sidestepSpeed_7; }
	inline float* get_address_of_sidestepSpeed_7() { return &___sidestepSpeed_7; }
	inline void set_sidestepSpeed_7(float value)
	{
		___sidestepSpeed_7 = value;
	}

	inline static int32_t get_offset_of_jumpSpeed_8() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___jumpSpeed_8)); }
	inline float get_jumpSpeed_8() const { return ___jumpSpeed_8; }
	inline float* get_address_of_jumpSpeed_8() { return &___jumpSpeed_8; }
	inline void set_jumpSpeed_8(float value)
	{
		___jumpSpeed_8 = value;
	}

	inline static int32_t get_offset_of_inAirMultiplier_9() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___inAirMultiplier_9)); }
	inline float get_inAirMultiplier_9() const { return ___inAirMultiplier_9; }
	inline float* get_address_of_inAirMultiplier_9() { return &___inAirMultiplier_9; }
	inline void set_inAirMultiplier_9(float value)
	{
		___inAirMultiplier_9 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_10() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___rotationSpeed_10)); }
	inline Vector2_t2156229523  get_rotationSpeed_10() const { return ___rotationSpeed_10; }
	inline Vector2_t2156229523 * get_address_of_rotationSpeed_10() { return &___rotationSpeed_10; }
	inline void set_rotationSpeed_10(Vector2_t2156229523  value)
	{
		___rotationSpeed_10 = value;
	}

	inline static int32_t get_offset_of_tiltPositiveYAxis_11() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___tiltPositiveYAxis_11)); }
	inline float get_tiltPositiveYAxis_11() const { return ___tiltPositiveYAxis_11; }
	inline float* get_address_of_tiltPositiveYAxis_11() { return &___tiltPositiveYAxis_11; }
	inline void set_tiltPositiveYAxis_11(float value)
	{
		___tiltPositiveYAxis_11 = value;
	}

	inline static int32_t get_offset_of_tiltNegativeYAxis_12() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___tiltNegativeYAxis_12)); }
	inline float get_tiltNegativeYAxis_12() const { return ___tiltNegativeYAxis_12; }
	inline float* get_address_of_tiltNegativeYAxis_12() { return &___tiltNegativeYAxis_12; }
	inline void set_tiltNegativeYAxis_12(float value)
	{
		___tiltNegativeYAxis_12 = value;
	}

	inline static int32_t get_offset_of_tiltXAxisMinimum_13() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___tiltXAxisMinimum_13)); }
	inline float get_tiltXAxisMinimum_13() const { return ___tiltXAxisMinimum_13; }
	inline float* get_address_of_tiltXAxisMinimum_13() { return &___tiltXAxisMinimum_13; }
	inline void set_tiltXAxisMinimum_13(float value)
	{
		___tiltXAxisMinimum_13 = value;
	}

	inline static int32_t get_offset_of_thisTransform_14() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___thisTransform_14)); }
	inline Transform_t3600365921 * get_thisTransform_14() const { return ___thisTransform_14; }
	inline Transform_t3600365921 ** get_address_of_thisTransform_14() { return &___thisTransform_14; }
	inline void set_thisTransform_14(Transform_t3600365921 * value)
	{
		___thisTransform_14 = value;
		Il2CppCodeGenWriteBarrier((&___thisTransform_14), value);
	}

	inline static int32_t get_offset_of_character_15() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___character_15)); }
	inline CharacterController_t1138636865 * get_character_15() const { return ___character_15; }
	inline CharacterController_t1138636865 ** get_address_of_character_15() { return &___character_15; }
	inline void set_character_15(CharacterController_t1138636865 * value)
	{
		___character_15 = value;
		Il2CppCodeGenWriteBarrier((&___character_15), value);
	}

	inline static int32_t get_offset_of_cameraVelocity_16() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___cameraVelocity_16)); }
	inline Vector3_t3722313464  get_cameraVelocity_16() const { return ___cameraVelocity_16; }
	inline Vector3_t3722313464 * get_address_of_cameraVelocity_16() { return &___cameraVelocity_16; }
	inline void set_cameraVelocity_16(Vector3_t3722313464  value)
	{
		___cameraVelocity_16 = value;
	}

	inline static int32_t get_offset_of_velocity_17() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___velocity_17)); }
	inline Vector3_t3722313464  get_velocity_17() const { return ___velocity_17; }
	inline Vector3_t3722313464 * get_address_of_velocity_17() { return &___velocity_17; }
	inline void set_velocity_17(Vector3_t3722313464  value)
	{
		___velocity_17 = value;
	}

	inline static int32_t get_offset_of_canJump_18() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___canJump_18)); }
	inline bool get_canJump_18() const { return ___canJump_18; }
	inline bool* get_address_of_canJump_18() { return &___canJump_18; }
	inline void set_canJump_18(bool value)
	{
		___canJump_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIRSTPERSONCONTROL_T610459381_H
#ifndef FOLLOWTRANSFORM_T1230726939_H
#define FOLLOWTRANSFORM_T1230726939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FollowTransform
struct  FollowTransform_t1230726939  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform FollowTransform::targetTransform
	Transform_t3600365921 * ___targetTransform_2;
	// System.Boolean FollowTransform::faceForward
	bool ___faceForward_3;
	// UnityEngine.Transform FollowTransform::thisTransform
	Transform_t3600365921 * ___thisTransform_4;

public:
	inline static int32_t get_offset_of_targetTransform_2() { return static_cast<int32_t>(offsetof(FollowTransform_t1230726939, ___targetTransform_2)); }
	inline Transform_t3600365921 * get_targetTransform_2() const { return ___targetTransform_2; }
	inline Transform_t3600365921 ** get_address_of_targetTransform_2() { return &___targetTransform_2; }
	inline void set_targetTransform_2(Transform_t3600365921 * value)
	{
		___targetTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___targetTransform_2), value);
	}

	inline static int32_t get_offset_of_faceForward_3() { return static_cast<int32_t>(offsetof(FollowTransform_t1230726939, ___faceForward_3)); }
	inline bool get_faceForward_3() const { return ___faceForward_3; }
	inline bool* get_address_of_faceForward_3() { return &___faceForward_3; }
	inline void set_faceForward_3(bool value)
	{
		___faceForward_3 = value;
	}

	inline static int32_t get_offset_of_thisTransform_4() { return static_cast<int32_t>(offsetof(FollowTransform_t1230726939, ___thisTransform_4)); }
	inline Transform_t3600365921 * get_thisTransform_4() const { return ___thisTransform_4; }
	inline Transform_t3600365921 ** get_address_of_thisTransform_4() { return &___thisTransform_4; }
	inline void set_thisTransform_4(Transform_t3600365921 * value)
	{
		___thisTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___thisTransform_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLLOWTRANSFORM_T1230726939_H
#ifndef UIWINDOWBASE_T957717849_H
#define UIWINDOWBASE_T957717849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWindowBase
struct  UIWindowBase_t957717849  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform UIWindowBase::m_transform
	RectTransform_t3704657025 * ___m_transform_2;

public:
	inline static int32_t get_offset_of_m_transform_2() { return static_cast<int32_t>(offsetof(UIWindowBase_t957717849, ___m_transform_2)); }
	inline RectTransform_t3704657025 * get_m_transform_2() const { return ___m_transform_2; }
	inline RectTransform_t3704657025 ** get_address_of_m_transform_2() { return &___m_transform_2; }
	inline void set_m_transform_2(RectTransform_t3704657025 * value)
	{
		___m_transform_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIWINDOWBASE_T957717849_H
#ifndef CREATEBUTTONSCRIPT_T92967038_H
#define CREATEBUTTONSCRIPT_T92967038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreateButtonScript
struct  CreateButtonScript_t92967038  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct CreateButtonScript_t92967038_StaticFields
{
public:
	// UnityEngine.Events.UnityAction CreateButtonScript::<>f__am$cache0
	UnityAction_t3245792599 * ___U3CU3Ef__amU24cache0_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(CreateButtonScript_t92967038_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline UnityAction_t3245792599 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline UnityAction_t3245792599 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(UnityAction_t3245792599 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATEBUTTONSCRIPT_T92967038_H
#ifndef RAYCASTMASK_T1362276734_H
#define RAYCASTMASK_T1362276734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RaycastMask
struct  RaycastMask_t1362276734  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image RaycastMask::_image
	Image_t2670269651 * ____image_2;
	// UnityEngine.Sprite RaycastMask::_sprite
	Sprite_t280657092 * ____sprite_3;

public:
	inline static int32_t get_offset_of__image_2() { return static_cast<int32_t>(offsetof(RaycastMask_t1362276734, ____image_2)); }
	inline Image_t2670269651 * get__image_2() const { return ____image_2; }
	inline Image_t2670269651 ** get_address_of__image_2() { return &____image_2; }
	inline void set__image_2(Image_t2670269651 * value)
	{
		____image_2 = value;
		Il2CppCodeGenWriteBarrier((&____image_2), value);
	}

	inline static int32_t get_offset_of__sprite_3() { return static_cast<int32_t>(offsetof(RaycastMask_t1362276734, ____sprite_3)); }
	inline Sprite_t280657092 * get__sprite_3() const { return ____sprite_3; }
	inline Sprite_t280657092 ** get_address_of__sprite_3() { return &____sprite_3; }
	inline void set__sprite_3(Sprite_t280657092 * value)
	{
		____sprite_3 = value;
		Il2CppCodeGenWriteBarrier((&____sprite_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTMASK_T1362276734_H
#ifndef TESTGUILAYOUT_T2138056437_H
#define TESTGUILAYOUT_T2138056437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestGuiLayout
struct  TestGuiLayout_t2138056437  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform TestGuiLayout::root
	RectTransform_t3704657025 * ___root_2;

public:
	inline static int32_t get_offset_of_root_2() { return static_cast<int32_t>(offsetof(TestGuiLayout_t2138056437, ___root_2)); }
	inline RectTransform_t3704657025 * get_root_2() const { return ___root_2; }
	inline RectTransform_t3704657025 ** get_address_of_root_2() { return &___root_2; }
	inline void set_root_2(RectTransform_t3704657025 * value)
	{
		___root_2 = value;
		Il2CppCodeGenWriteBarrier((&___root_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTGUILAYOUT_T2138056437_H
#ifndef ROTATIONCONSTRAINT_T2833302650_H
#define ROTATIONCONSTRAINT_T2833302650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotationConstraint
struct  RotationConstraint_t2833302650  : public MonoBehaviour_t3962482529
{
public:
	// ConstraintAxis RotationConstraint::axis
	int32_t ___axis_2;
	// System.Single RotationConstraint::min
	float ___min_3;
	// System.Single RotationConstraint::max
	float ___max_4;
	// UnityEngine.Transform RotationConstraint::thisTransform
	Transform_t3600365921 * ___thisTransform_5;
	// UnityEngine.Vector3 RotationConstraint::rotateAround
	Vector3_t3722313464  ___rotateAround_6;
	// UnityEngine.Quaternion RotationConstraint::minQuaternion
	Quaternion_t2301928331  ___minQuaternion_7;
	// UnityEngine.Quaternion RotationConstraint::maxQuaternion
	Quaternion_t2301928331  ___maxQuaternion_8;
	// System.Single RotationConstraint::range
	float ___range_9;

public:
	inline static int32_t get_offset_of_axis_2() { return static_cast<int32_t>(offsetof(RotationConstraint_t2833302650, ___axis_2)); }
	inline int32_t get_axis_2() const { return ___axis_2; }
	inline int32_t* get_address_of_axis_2() { return &___axis_2; }
	inline void set_axis_2(int32_t value)
	{
		___axis_2 = value;
	}

	inline static int32_t get_offset_of_min_3() { return static_cast<int32_t>(offsetof(RotationConstraint_t2833302650, ___min_3)); }
	inline float get_min_3() const { return ___min_3; }
	inline float* get_address_of_min_3() { return &___min_3; }
	inline void set_min_3(float value)
	{
		___min_3 = value;
	}

	inline static int32_t get_offset_of_max_4() { return static_cast<int32_t>(offsetof(RotationConstraint_t2833302650, ___max_4)); }
	inline float get_max_4() const { return ___max_4; }
	inline float* get_address_of_max_4() { return &___max_4; }
	inline void set_max_4(float value)
	{
		___max_4 = value;
	}

	inline static int32_t get_offset_of_thisTransform_5() { return static_cast<int32_t>(offsetof(RotationConstraint_t2833302650, ___thisTransform_5)); }
	inline Transform_t3600365921 * get_thisTransform_5() const { return ___thisTransform_5; }
	inline Transform_t3600365921 ** get_address_of_thisTransform_5() { return &___thisTransform_5; }
	inline void set_thisTransform_5(Transform_t3600365921 * value)
	{
		___thisTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___thisTransform_5), value);
	}

	inline static int32_t get_offset_of_rotateAround_6() { return static_cast<int32_t>(offsetof(RotationConstraint_t2833302650, ___rotateAround_6)); }
	inline Vector3_t3722313464  get_rotateAround_6() const { return ___rotateAround_6; }
	inline Vector3_t3722313464 * get_address_of_rotateAround_6() { return &___rotateAround_6; }
	inline void set_rotateAround_6(Vector3_t3722313464  value)
	{
		___rotateAround_6 = value;
	}

	inline static int32_t get_offset_of_minQuaternion_7() { return static_cast<int32_t>(offsetof(RotationConstraint_t2833302650, ___minQuaternion_7)); }
	inline Quaternion_t2301928331  get_minQuaternion_7() const { return ___minQuaternion_7; }
	inline Quaternion_t2301928331 * get_address_of_minQuaternion_7() { return &___minQuaternion_7; }
	inline void set_minQuaternion_7(Quaternion_t2301928331  value)
	{
		___minQuaternion_7 = value;
	}

	inline static int32_t get_offset_of_maxQuaternion_8() { return static_cast<int32_t>(offsetof(RotationConstraint_t2833302650, ___maxQuaternion_8)); }
	inline Quaternion_t2301928331  get_maxQuaternion_8() const { return ___maxQuaternion_8; }
	inline Quaternion_t2301928331 * get_address_of_maxQuaternion_8() { return &___maxQuaternion_8; }
	inline void set_maxQuaternion_8(Quaternion_t2301928331  value)
	{
		___maxQuaternion_8 = value;
	}

	inline static int32_t get_offset_of_range_9() { return static_cast<int32_t>(offsetof(RotationConstraint_t2833302650, ___range_9)); }
	inline float get_range_9() const { return ___range_9; }
	inline float* get_address_of_range_9() { return &___range_9; }
	inline void set_range_9(float value)
	{
		___range_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONCONSTRAINT_T2833302650_H
#ifndef SIDESCROLLCONTROL_T463195584_H
#define SIDESCROLLCONTROL_T463195584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SidescrollControl
struct  SidescrollControl_t463195584  : public MonoBehaviour_t3962482529
{
public:
	// Joystick SidescrollControl::moveTouchPad
	Joystick_t9498292 * ___moveTouchPad_2;
	// Joystick SidescrollControl::jumpTouchPad
	Joystick_t9498292 * ___jumpTouchPad_3;
	// System.Single SidescrollControl::forwardSpeed
	float ___forwardSpeed_4;
	// System.Single SidescrollControl::backwardSpeed
	float ___backwardSpeed_5;
	// System.Single SidescrollControl::jumpSpeed
	float ___jumpSpeed_6;
	// System.Single SidescrollControl::inAirMultiplier
	float ___inAirMultiplier_7;
	// UnityEngine.Transform SidescrollControl::thisTransform
	Transform_t3600365921 * ___thisTransform_8;
	// UnityEngine.CharacterController SidescrollControl::character
	CharacterController_t1138636865 * ___character_9;
	// UnityEngine.Vector3 SidescrollControl::velocity
	Vector3_t3722313464  ___velocity_10;
	// System.Boolean SidescrollControl::canJump
	bool ___canJump_11;

public:
	inline static int32_t get_offset_of_moveTouchPad_2() { return static_cast<int32_t>(offsetof(SidescrollControl_t463195584, ___moveTouchPad_2)); }
	inline Joystick_t9498292 * get_moveTouchPad_2() const { return ___moveTouchPad_2; }
	inline Joystick_t9498292 ** get_address_of_moveTouchPad_2() { return &___moveTouchPad_2; }
	inline void set_moveTouchPad_2(Joystick_t9498292 * value)
	{
		___moveTouchPad_2 = value;
		Il2CppCodeGenWriteBarrier((&___moveTouchPad_2), value);
	}

	inline static int32_t get_offset_of_jumpTouchPad_3() { return static_cast<int32_t>(offsetof(SidescrollControl_t463195584, ___jumpTouchPad_3)); }
	inline Joystick_t9498292 * get_jumpTouchPad_3() const { return ___jumpTouchPad_3; }
	inline Joystick_t9498292 ** get_address_of_jumpTouchPad_3() { return &___jumpTouchPad_3; }
	inline void set_jumpTouchPad_3(Joystick_t9498292 * value)
	{
		___jumpTouchPad_3 = value;
		Il2CppCodeGenWriteBarrier((&___jumpTouchPad_3), value);
	}

	inline static int32_t get_offset_of_forwardSpeed_4() { return static_cast<int32_t>(offsetof(SidescrollControl_t463195584, ___forwardSpeed_4)); }
	inline float get_forwardSpeed_4() const { return ___forwardSpeed_4; }
	inline float* get_address_of_forwardSpeed_4() { return &___forwardSpeed_4; }
	inline void set_forwardSpeed_4(float value)
	{
		___forwardSpeed_4 = value;
	}

	inline static int32_t get_offset_of_backwardSpeed_5() { return static_cast<int32_t>(offsetof(SidescrollControl_t463195584, ___backwardSpeed_5)); }
	inline float get_backwardSpeed_5() const { return ___backwardSpeed_5; }
	inline float* get_address_of_backwardSpeed_5() { return &___backwardSpeed_5; }
	inline void set_backwardSpeed_5(float value)
	{
		___backwardSpeed_5 = value;
	}

	inline static int32_t get_offset_of_jumpSpeed_6() { return static_cast<int32_t>(offsetof(SidescrollControl_t463195584, ___jumpSpeed_6)); }
	inline float get_jumpSpeed_6() const { return ___jumpSpeed_6; }
	inline float* get_address_of_jumpSpeed_6() { return &___jumpSpeed_6; }
	inline void set_jumpSpeed_6(float value)
	{
		___jumpSpeed_6 = value;
	}

	inline static int32_t get_offset_of_inAirMultiplier_7() { return static_cast<int32_t>(offsetof(SidescrollControl_t463195584, ___inAirMultiplier_7)); }
	inline float get_inAirMultiplier_7() const { return ___inAirMultiplier_7; }
	inline float* get_address_of_inAirMultiplier_7() { return &___inAirMultiplier_7; }
	inline void set_inAirMultiplier_7(float value)
	{
		___inAirMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_thisTransform_8() { return static_cast<int32_t>(offsetof(SidescrollControl_t463195584, ___thisTransform_8)); }
	inline Transform_t3600365921 * get_thisTransform_8() const { return ___thisTransform_8; }
	inline Transform_t3600365921 ** get_address_of_thisTransform_8() { return &___thisTransform_8; }
	inline void set_thisTransform_8(Transform_t3600365921 * value)
	{
		___thisTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&___thisTransform_8), value);
	}

	inline static int32_t get_offset_of_character_9() { return static_cast<int32_t>(offsetof(SidescrollControl_t463195584, ___character_9)); }
	inline CharacterController_t1138636865 * get_character_9() const { return ___character_9; }
	inline CharacterController_t1138636865 ** get_address_of_character_9() { return &___character_9; }
	inline void set_character_9(CharacterController_t1138636865 * value)
	{
		___character_9 = value;
		Il2CppCodeGenWriteBarrier((&___character_9), value);
	}

	inline static int32_t get_offset_of_velocity_10() { return static_cast<int32_t>(offsetof(SidescrollControl_t463195584, ___velocity_10)); }
	inline Vector3_t3722313464  get_velocity_10() const { return ___velocity_10; }
	inline Vector3_t3722313464 * get_address_of_velocity_10() { return &___velocity_10; }
	inline void set_velocity_10(Vector3_t3722313464  value)
	{
		___velocity_10 = value;
	}

	inline static int32_t get_offset_of_canJump_11() { return static_cast<int32_t>(offsetof(SidescrollControl_t463195584, ___canJump_11)); }
	inline bool get_canJump_11() const { return ___canJump_11; }
	inline bool* get_address_of_canJump_11() { return &___canJump_11; }
	inline void set_canJump_11(bool value)
	{
		___canJump_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIDESCROLLCONTROL_T463195584_H
#ifndef SMOOTHFOLLOW2D_T2134035804_H
#define SMOOTHFOLLOW2D_T2134035804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmoothFollow2D
struct  SmoothFollow2D_t2134035804  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform SmoothFollow2D::target
	Transform_t3600365921 * ___target_2;
	// System.Single SmoothFollow2D::smoothTime
	float ___smoothTime_3;
	// UnityEngine.Transform SmoothFollow2D::thisTransform
	Transform_t3600365921 * ___thisTransform_4;
	// UnityEngine.Vector2 SmoothFollow2D::velocity
	Vector2_t2156229523  ___velocity_5;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(SmoothFollow2D_t2134035804, ___target_2)); }
	inline Transform_t3600365921 * get_target_2() const { return ___target_2; }
	inline Transform_t3600365921 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t3600365921 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_smoothTime_3() { return static_cast<int32_t>(offsetof(SmoothFollow2D_t2134035804, ___smoothTime_3)); }
	inline float get_smoothTime_3() const { return ___smoothTime_3; }
	inline float* get_address_of_smoothTime_3() { return &___smoothTime_3; }
	inline void set_smoothTime_3(float value)
	{
		___smoothTime_3 = value;
	}

	inline static int32_t get_offset_of_thisTransform_4() { return static_cast<int32_t>(offsetof(SmoothFollow2D_t2134035804, ___thisTransform_4)); }
	inline Transform_t3600365921 * get_thisTransform_4() const { return ___thisTransform_4; }
	inline Transform_t3600365921 ** get_address_of_thisTransform_4() { return &___thisTransform_4; }
	inline void set_thisTransform_4(Transform_t3600365921 * value)
	{
		___thisTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___thisTransform_4), value);
	}

	inline static int32_t get_offset_of_velocity_5() { return static_cast<int32_t>(offsetof(SmoothFollow2D_t2134035804, ___velocity_5)); }
	inline Vector2_t2156229523  get_velocity_5() const { return ___velocity_5; }
	inline Vector2_t2156229523 * get_address_of_velocity_5() { return &___velocity_5; }
	inline void set_velocity_5(Vector2_t2156229523  value)
	{
		___velocity_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHFOLLOW2D_T2134035804_H
#ifndef ROLLABALL_T267000325_H
#define ROLLABALL_T267000325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RollABall
struct  RollABall_t267000325  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 RollABall::tilt
	Vector3_t3722313464  ___tilt_2;
	// System.Single RollABall::speed
	float ___speed_3;
	// System.Single RollABall::circ
	float ___circ_4;
	// UnityEngine.Vector3 RollABall::previousPosition
	Vector3_t3722313464  ___previousPosition_5;

public:
	inline static int32_t get_offset_of_tilt_2() { return static_cast<int32_t>(offsetof(RollABall_t267000325, ___tilt_2)); }
	inline Vector3_t3722313464  get_tilt_2() const { return ___tilt_2; }
	inline Vector3_t3722313464 * get_address_of_tilt_2() { return &___tilt_2; }
	inline void set_tilt_2(Vector3_t3722313464  value)
	{
		___tilt_2 = value;
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(RollABall_t267000325, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_circ_4() { return static_cast<int32_t>(offsetof(RollABall_t267000325, ___circ_4)); }
	inline float get_circ_4() const { return ___circ_4; }
	inline float* get_address_of_circ_4() { return &___circ_4; }
	inline void set_circ_4(float value)
	{
		___circ_4 = value;
	}

	inline static int32_t get_offset_of_previousPosition_5() { return static_cast<int32_t>(offsetof(RollABall_t267000325, ___previousPosition_5)); }
	inline Vector3_t3722313464  get_previousPosition_5() const { return ___previousPosition_5; }
	inline Vector3_t3722313464 * get_address_of_previousPosition_5() { return &___previousPosition_5; }
	inline void set_previousPosition_5(Vector3_t3722313464  value)
	{
		___previousPosition_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROLLABALL_T267000325_H
#ifndef JOYSTICK_T9498292_H
#define JOYSTICK_T9498292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Joystick
struct  Joystick_t9498292  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Joystick::touchPad
	bool ___touchPad_5;
	// UnityEngine.Rect Joystick::touchZone
	Rect_t2360479859  ___touchZone_6;
	// UnityEngine.Vector2 Joystick::deadZone
	Vector2_t2156229523  ___deadZone_7;
	// System.Boolean Joystick::normalize
	bool ___normalize_8;
	// UnityEngine.Vector2 Joystick::position
	Vector2_t2156229523  ___position_9;
	// System.Int32 Joystick::tapCount
	int32_t ___tapCount_10;
	// System.Int32 Joystick::lastFingerId
	int32_t ___lastFingerId_11;
	// System.Single Joystick::tapTimeWindow
	float ___tapTimeWindow_12;
	// UnityEngine.Vector2 Joystick::fingerDownPos
	Vector2_t2156229523  ___fingerDownPos_13;
	// System.Single Joystick::fingerDownTime
	float ___fingerDownTime_14;
	// System.Single Joystick::firstDeltaTime
	float ___firstDeltaTime_15;
	// UnityEngine.GUITexture Joystick::gui
	GUITexture_t951903601 * ___gui_16;
	// UnityEngine.Rect Joystick::defaultRect
	Rect_t2360479859  ___defaultRect_17;
	// Boundary Joystick::guiBoundary
	Boundary_t2442033035 * ___guiBoundary_18;
	// UnityEngine.Vector2 Joystick::guiTouchOffset
	Vector2_t2156229523  ___guiTouchOffset_19;
	// UnityEngine.Vector2 Joystick::guiCenter
	Vector2_t2156229523  ___guiCenter_20;

public:
	inline static int32_t get_offset_of_touchPad_5() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___touchPad_5)); }
	inline bool get_touchPad_5() const { return ___touchPad_5; }
	inline bool* get_address_of_touchPad_5() { return &___touchPad_5; }
	inline void set_touchPad_5(bool value)
	{
		___touchPad_5 = value;
	}

	inline static int32_t get_offset_of_touchZone_6() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___touchZone_6)); }
	inline Rect_t2360479859  get_touchZone_6() const { return ___touchZone_6; }
	inline Rect_t2360479859 * get_address_of_touchZone_6() { return &___touchZone_6; }
	inline void set_touchZone_6(Rect_t2360479859  value)
	{
		___touchZone_6 = value;
	}

	inline static int32_t get_offset_of_deadZone_7() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___deadZone_7)); }
	inline Vector2_t2156229523  get_deadZone_7() const { return ___deadZone_7; }
	inline Vector2_t2156229523 * get_address_of_deadZone_7() { return &___deadZone_7; }
	inline void set_deadZone_7(Vector2_t2156229523  value)
	{
		___deadZone_7 = value;
	}

	inline static int32_t get_offset_of_normalize_8() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___normalize_8)); }
	inline bool get_normalize_8() const { return ___normalize_8; }
	inline bool* get_address_of_normalize_8() { return &___normalize_8; }
	inline void set_normalize_8(bool value)
	{
		___normalize_8 = value;
	}

	inline static int32_t get_offset_of_position_9() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___position_9)); }
	inline Vector2_t2156229523  get_position_9() const { return ___position_9; }
	inline Vector2_t2156229523 * get_address_of_position_9() { return &___position_9; }
	inline void set_position_9(Vector2_t2156229523  value)
	{
		___position_9 = value;
	}

	inline static int32_t get_offset_of_tapCount_10() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___tapCount_10)); }
	inline int32_t get_tapCount_10() const { return ___tapCount_10; }
	inline int32_t* get_address_of_tapCount_10() { return &___tapCount_10; }
	inline void set_tapCount_10(int32_t value)
	{
		___tapCount_10 = value;
	}

	inline static int32_t get_offset_of_lastFingerId_11() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___lastFingerId_11)); }
	inline int32_t get_lastFingerId_11() const { return ___lastFingerId_11; }
	inline int32_t* get_address_of_lastFingerId_11() { return &___lastFingerId_11; }
	inline void set_lastFingerId_11(int32_t value)
	{
		___lastFingerId_11 = value;
	}

	inline static int32_t get_offset_of_tapTimeWindow_12() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___tapTimeWindow_12)); }
	inline float get_tapTimeWindow_12() const { return ___tapTimeWindow_12; }
	inline float* get_address_of_tapTimeWindow_12() { return &___tapTimeWindow_12; }
	inline void set_tapTimeWindow_12(float value)
	{
		___tapTimeWindow_12 = value;
	}

	inline static int32_t get_offset_of_fingerDownPos_13() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___fingerDownPos_13)); }
	inline Vector2_t2156229523  get_fingerDownPos_13() const { return ___fingerDownPos_13; }
	inline Vector2_t2156229523 * get_address_of_fingerDownPos_13() { return &___fingerDownPos_13; }
	inline void set_fingerDownPos_13(Vector2_t2156229523  value)
	{
		___fingerDownPos_13 = value;
	}

	inline static int32_t get_offset_of_fingerDownTime_14() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___fingerDownTime_14)); }
	inline float get_fingerDownTime_14() const { return ___fingerDownTime_14; }
	inline float* get_address_of_fingerDownTime_14() { return &___fingerDownTime_14; }
	inline void set_fingerDownTime_14(float value)
	{
		___fingerDownTime_14 = value;
	}

	inline static int32_t get_offset_of_firstDeltaTime_15() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___firstDeltaTime_15)); }
	inline float get_firstDeltaTime_15() const { return ___firstDeltaTime_15; }
	inline float* get_address_of_firstDeltaTime_15() { return &___firstDeltaTime_15; }
	inline void set_firstDeltaTime_15(float value)
	{
		___firstDeltaTime_15 = value;
	}

	inline static int32_t get_offset_of_gui_16() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___gui_16)); }
	inline GUITexture_t951903601 * get_gui_16() const { return ___gui_16; }
	inline GUITexture_t951903601 ** get_address_of_gui_16() { return &___gui_16; }
	inline void set_gui_16(GUITexture_t951903601 * value)
	{
		___gui_16 = value;
		Il2CppCodeGenWriteBarrier((&___gui_16), value);
	}

	inline static int32_t get_offset_of_defaultRect_17() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___defaultRect_17)); }
	inline Rect_t2360479859  get_defaultRect_17() const { return ___defaultRect_17; }
	inline Rect_t2360479859 * get_address_of_defaultRect_17() { return &___defaultRect_17; }
	inline void set_defaultRect_17(Rect_t2360479859  value)
	{
		___defaultRect_17 = value;
	}

	inline static int32_t get_offset_of_guiBoundary_18() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___guiBoundary_18)); }
	inline Boundary_t2442033035 * get_guiBoundary_18() const { return ___guiBoundary_18; }
	inline Boundary_t2442033035 ** get_address_of_guiBoundary_18() { return &___guiBoundary_18; }
	inline void set_guiBoundary_18(Boundary_t2442033035 * value)
	{
		___guiBoundary_18 = value;
		Il2CppCodeGenWriteBarrier((&___guiBoundary_18), value);
	}

	inline static int32_t get_offset_of_guiTouchOffset_19() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___guiTouchOffset_19)); }
	inline Vector2_t2156229523  get_guiTouchOffset_19() const { return ___guiTouchOffset_19; }
	inline Vector2_t2156229523 * get_address_of_guiTouchOffset_19() { return &___guiTouchOffset_19; }
	inline void set_guiTouchOffset_19(Vector2_t2156229523  value)
	{
		___guiTouchOffset_19 = value;
	}

	inline static int32_t get_offset_of_guiCenter_20() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___guiCenter_20)); }
	inline Vector2_t2156229523  get_guiCenter_20() const { return ___guiCenter_20; }
	inline Vector2_t2156229523 * get_address_of_guiCenter_20() { return &___guiCenter_20; }
	inline void set_guiCenter_20(Vector2_t2156229523  value)
	{
		___guiCenter_20 = value;
	}
};

struct Joystick_t9498292_StaticFields
{
public:
	// Joystick[] Joystick::joysticks
	JoystickU5BU5D_t4275182589* ___joysticks_2;
	// System.Boolean Joystick::enumeratedJoysticks
	bool ___enumeratedJoysticks_3;
	// System.Single Joystick::tapTimeDelta
	float ___tapTimeDelta_4;

public:
	inline static int32_t get_offset_of_joysticks_2() { return static_cast<int32_t>(offsetof(Joystick_t9498292_StaticFields, ___joysticks_2)); }
	inline JoystickU5BU5D_t4275182589* get_joysticks_2() const { return ___joysticks_2; }
	inline JoystickU5BU5D_t4275182589** get_address_of_joysticks_2() { return &___joysticks_2; }
	inline void set_joysticks_2(JoystickU5BU5D_t4275182589* value)
	{
		___joysticks_2 = value;
		Il2CppCodeGenWriteBarrier((&___joysticks_2), value);
	}

	inline static int32_t get_offset_of_enumeratedJoysticks_3() { return static_cast<int32_t>(offsetof(Joystick_t9498292_StaticFields, ___enumeratedJoysticks_3)); }
	inline bool get_enumeratedJoysticks_3() const { return ___enumeratedJoysticks_3; }
	inline bool* get_address_of_enumeratedJoysticks_3() { return &___enumeratedJoysticks_3; }
	inline void set_enumeratedJoysticks_3(bool value)
	{
		___enumeratedJoysticks_3 = value;
	}

	inline static int32_t get_offset_of_tapTimeDelta_4() { return static_cast<int32_t>(offsetof(Joystick_t9498292_StaticFields, ___tapTimeDelta_4)); }
	inline float get_tapTimeDelta_4() const { return ___tapTimeDelta_4; }
	inline float* get_address_of_tapTimeDelta_4() { return &___tapTimeDelta_4; }
	inline void set_tapTimeDelta_4(float value)
	{
		___tapTimeDelta_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICK_T9498292_H
#ifndef OBLIQUENEAR_T1763198098_H
#define OBLIQUENEAR_T1763198098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObliqueNear
struct  ObliqueNear_t1763198098  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform ObliqueNear::plane
	Transform_t3600365921 * ___plane_2;

public:
	inline static int32_t get_offset_of_plane_2() { return static_cast<int32_t>(offsetof(ObliqueNear_t1763198098, ___plane_2)); }
	inline Transform_t3600365921 * get_plane_2() const { return ___plane_2; }
	inline Transform_t3600365921 ** get_address_of_plane_2() { return &___plane_2; }
	inline void set_plane_2(Transform_t3600365921 * value)
	{
		___plane_2 = value;
		Il2CppCodeGenWriteBarrier((&___plane_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBLIQUENEAR_T1763198098_H
#ifndef PLAYERRELATIVECONTROL_T1056195335_H
#define PLAYERRELATIVECONTROL_T1056195335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerRelativeControl
struct  PlayerRelativeControl_t1056195335  : public MonoBehaviour_t3962482529
{
public:
	// Joystick PlayerRelativeControl::moveJoystick
	Joystick_t9498292 * ___moveJoystick_2;
	// Joystick PlayerRelativeControl::rotateJoystick
	Joystick_t9498292 * ___rotateJoystick_3;
	// UnityEngine.Transform PlayerRelativeControl::cameraPivot
	Transform_t3600365921 * ___cameraPivot_4;
	// System.Single PlayerRelativeControl::forwardSpeed
	float ___forwardSpeed_5;
	// System.Single PlayerRelativeControl::backwardSpeed
	float ___backwardSpeed_6;
	// System.Single PlayerRelativeControl::sidestepSpeed
	float ___sidestepSpeed_7;
	// System.Single PlayerRelativeControl::jumpSpeed
	float ___jumpSpeed_8;
	// System.Single PlayerRelativeControl::inAirMultiplier
	float ___inAirMultiplier_9;
	// UnityEngine.Vector2 PlayerRelativeControl::rotationSpeed
	Vector2_t2156229523  ___rotationSpeed_10;
	// UnityEngine.Transform PlayerRelativeControl::thisTransform
	Transform_t3600365921 * ___thisTransform_11;
	// UnityEngine.CharacterController PlayerRelativeControl::character
	CharacterController_t1138636865 * ___character_12;
	// UnityEngine.Vector3 PlayerRelativeControl::cameraVelocity
	Vector3_t3722313464  ___cameraVelocity_13;
	// UnityEngine.Vector3 PlayerRelativeControl::velocity
	Vector3_t3722313464  ___velocity_14;

public:
	inline static int32_t get_offset_of_moveJoystick_2() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___moveJoystick_2)); }
	inline Joystick_t9498292 * get_moveJoystick_2() const { return ___moveJoystick_2; }
	inline Joystick_t9498292 ** get_address_of_moveJoystick_2() { return &___moveJoystick_2; }
	inline void set_moveJoystick_2(Joystick_t9498292 * value)
	{
		___moveJoystick_2 = value;
		Il2CppCodeGenWriteBarrier((&___moveJoystick_2), value);
	}

	inline static int32_t get_offset_of_rotateJoystick_3() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___rotateJoystick_3)); }
	inline Joystick_t9498292 * get_rotateJoystick_3() const { return ___rotateJoystick_3; }
	inline Joystick_t9498292 ** get_address_of_rotateJoystick_3() { return &___rotateJoystick_3; }
	inline void set_rotateJoystick_3(Joystick_t9498292 * value)
	{
		___rotateJoystick_3 = value;
		Il2CppCodeGenWriteBarrier((&___rotateJoystick_3), value);
	}

	inline static int32_t get_offset_of_cameraPivot_4() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___cameraPivot_4)); }
	inline Transform_t3600365921 * get_cameraPivot_4() const { return ___cameraPivot_4; }
	inline Transform_t3600365921 ** get_address_of_cameraPivot_4() { return &___cameraPivot_4; }
	inline void set_cameraPivot_4(Transform_t3600365921 * value)
	{
		___cameraPivot_4 = value;
		Il2CppCodeGenWriteBarrier((&___cameraPivot_4), value);
	}

	inline static int32_t get_offset_of_forwardSpeed_5() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___forwardSpeed_5)); }
	inline float get_forwardSpeed_5() const { return ___forwardSpeed_5; }
	inline float* get_address_of_forwardSpeed_5() { return &___forwardSpeed_5; }
	inline void set_forwardSpeed_5(float value)
	{
		___forwardSpeed_5 = value;
	}

	inline static int32_t get_offset_of_backwardSpeed_6() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___backwardSpeed_6)); }
	inline float get_backwardSpeed_6() const { return ___backwardSpeed_6; }
	inline float* get_address_of_backwardSpeed_6() { return &___backwardSpeed_6; }
	inline void set_backwardSpeed_6(float value)
	{
		___backwardSpeed_6 = value;
	}

	inline static int32_t get_offset_of_sidestepSpeed_7() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___sidestepSpeed_7)); }
	inline float get_sidestepSpeed_7() const { return ___sidestepSpeed_7; }
	inline float* get_address_of_sidestepSpeed_7() { return &___sidestepSpeed_7; }
	inline void set_sidestepSpeed_7(float value)
	{
		___sidestepSpeed_7 = value;
	}

	inline static int32_t get_offset_of_jumpSpeed_8() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___jumpSpeed_8)); }
	inline float get_jumpSpeed_8() const { return ___jumpSpeed_8; }
	inline float* get_address_of_jumpSpeed_8() { return &___jumpSpeed_8; }
	inline void set_jumpSpeed_8(float value)
	{
		___jumpSpeed_8 = value;
	}

	inline static int32_t get_offset_of_inAirMultiplier_9() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___inAirMultiplier_9)); }
	inline float get_inAirMultiplier_9() const { return ___inAirMultiplier_9; }
	inline float* get_address_of_inAirMultiplier_9() { return &___inAirMultiplier_9; }
	inline void set_inAirMultiplier_9(float value)
	{
		___inAirMultiplier_9 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_10() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___rotationSpeed_10)); }
	inline Vector2_t2156229523  get_rotationSpeed_10() const { return ___rotationSpeed_10; }
	inline Vector2_t2156229523 * get_address_of_rotationSpeed_10() { return &___rotationSpeed_10; }
	inline void set_rotationSpeed_10(Vector2_t2156229523  value)
	{
		___rotationSpeed_10 = value;
	}

	inline static int32_t get_offset_of_thisTransform_11() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___thisTransform_11)); }
	inline Transform_t3600365921 * get_thisTransform_11() const { return ___thisTransform_11; }
	inline Transform_t3600365921 ** get_address_of_thisTransform_11() { return &___thisTransform_11; }
	inline void set_thisTransform_11(Transform_t3600365921 * value)
	{
		___thisTransform_11 = value;
		Il2CppCodeGenWriteBarrier((&___thisTransform_11), value);
	}

	inline static int32_t get_offset_of_character_12() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___character_12)); }
	inline CharacterController_t1138636865 * get_character_12() const { return ___character_12; }
	inline CharacterController_t1138636865 ** get_address_of_character_12() { return &___character_12; }
	inline void set_character_12(CharacterController_t1138636865 * value)
	{
		___character_12 = value;
		Il2CppCodeGenWriteBarrier((&___character_12), value);
	}

	inline static int32_t get_offset_of_cameraVelocity_13() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___cameraVelocity_13)); }
	inline Vector3_t3722313464  get_cameraVelocity_13() const { return ___cameraVelocity_13; }
	inline Vector3_t3722313464 * get_address_of_cameraVelocity_13() { return &___cameraVelocity_13; }
	inline void set_cameraVelocity_13(Vector3_t3722313464  value)
	{
		___cameraVelocity_13 = value;
	}

	inline static int32_t get_offset_of_velocity_14() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___velocity_14)); }
	inline Vector3_t3722313464  get_velocity_14() const { return ___velocity_14; }
	inline Vector3_t3722313464 * get_address_of_velocity_14() { return &___velocity_14; }
	inline void set_velocity_14(Vector3_t3722313464  value)
	{
		___velocity_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERRELATIVECONTROL_T1056195335_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (DoneCameraMovement_t1316814223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1800[6] = 
{
	DoneCameraMovement_t1316814223::get_offset_of_smooth_2(),
	DoneCameraMovement_t1316814223::get_offset_of_extraCameraAdjust_3(),
	DoneCameraMovement_t1316814223::get_offset_of_player_4(),
	DoneCameraMovement_t1316814223::get_offset_of_relCameraPos_5(),
	DoneCameraMovement_t1316814223::get_offset_of_relCameraPosMag_6(),
	DoneCameraMovement_t1316814223::get_offset_of_newPos_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (CreateButtonScript_t92967038), -1, sizeof(CreateButtonScript_t92967038_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1801[1] = 
{
	CreateButtonScript_t92967038_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (RaycastMask_t1362276734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1802[2] = 
{
	RaycastMask_t1362276734::get_offset_of__image_2(),
	RaycastMask_t1362276734::get_offset_of__sprite_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (TestGuiLayout_t2138056437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1803[1] = 
{
	TestGuiLayout_t2138056437::get_offset_of_root_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (uGuiLayout_t2579011462), -1, sizeof(uGuiLayout_t2579011462_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1804[4] = 
{
	uGuiLayout_t2579011462_StaticFields::get_offset_of_subControls_0(),
	uGuiLayout_t2579011462_StaticFields::get_offset_of_currentPosition_1(),
	uGuiLayout_t2579011462_StaticFields::get_offset_of_positionCache_2(),
	uGuiLayout_t2579011462_StaticFields::get_offset_of_modeCache_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (Mode_t3166956325)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1805[3] = 
{
	Mode_t3166956325::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (UIWindowBase_t957717849), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1806[1] = 
{
	UIWindowBase_t957717849::get_offset_of_m_transform_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (U3CModuleU3E_t692745544), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (CameraRelativeControl_t2453217546), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1808[12] = 
{
	CameraRelativeControl_t2453217546::get_offset_of_moveJoystick_2(),
	CameraRelativeControl_t2453217546::get_offset_of_rotateJoystick_3(),
	CameraRelativeControl_t2453217546::get_offset_of_cameraPivot_4(),
	CameraRelativeControl_t2453217546::get_offset_of_cameraTransform_5(),
	CameraRelativeControl_t2453217546::get_offset_of_speed_6(),
	CameraRelativeControl_t2453217546::get_offset_of_jumpSpeed_7(),
	CameraRelativeControl_t2453217546::get_offset_of_inAirMultiplier_8(),
	CameraRelativeControl_t2453217546::get_offset_of_rotationSpeed_9(),
	CameraRelativeControl_t2453217546::get_offset_of_thisTransform_10(),
	CameraRelativeControl_t2453217546::get_offset_of_character_11(),
	CameraRelativeControl_t2453217546::get_offset_of_velocity_12(),
	CameraRelativeControl_t2453217546::get_offset_of_canJump_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (FirstPersonControl_t610459381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1809[17] = 
{
	FirstPersonControl_t610459381::get_offset_of_moveTouchPad_2(),
	FirstPersonControl_t610459381::get_offset_of_rotateTouchPad_3(),
	FirstPersonControl_t610459381::get_offset_of_cameraPivot_4(),
	FirstPersonControl_t610459381::get_offset_of_forwardSpeed_5(),
	FirstPersonControl_t610459381::get_offset_of_backwardSpeed_6(),
	FirstPersonControl_t610459381::get_offset_of_sidestepSpeed_7(),
	FirstPersonControl_t610459381::get_offset_of_jumpSpeed_8(),
	FirstPersonControl_t610459381::get_offset_of_inAirMultiplier_9(),
	FirstPersonControl_t610459381::get_offset_of_rotationSpeed_10(),
	FirstPersonControl_t610459381::get_offset_of_tiltPositiveYAxis_11(),
	FirstPersonControl_t610459381::get_offset_of_tiltNegativeYAxis_12(),
	FirstPersonControl_t610459381::get_offset_of_tiltXAxisMinimum_13(),
	FirstPersonControl_t610459381::get_offset_of_thisTransform_14(),
	FirstPersonControl_t610459381::get_offset_of_character_15(),
	FirstPersonControl_t610459381::get_offset_of_cameraVelocity_16(),
	FirstPersonControl_t610459381::get_offset_of_velocity_17(),
	FirstPersonControl_t610459381::get_offset_of_canJump_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (FollowTransform_t1230726939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1810[3] = 
{
	FollowTransform_t1230726939::get_offset_of_targetTransform_2(),
	FollowTransform_t1230726939::get_offset_of_faceForward_3(),
	FollowTransform_t1230726939::get_offset_of_thisTransform_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (Boundary_t2442033035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1811[2] = 
{
	Boundary_t2442033035::get_offset_of_min_0(),
	Boundary_t2442033035::get_offset_of_max_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (Joystick_t9498292), -1, sizeof(Joystick_t9498292_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1812[19] = 
{
	Joystick_t9498292_StaticFields::get_offset_of_joysticks_2(),
	Joystick_t9498292_StaticFields::get_offset_of_enumeratedJoysticks_3(),
	Joystick_t9498292_StaticFields::get_offset_of_tapTimeDelta_4(),
	Joystick_t9498292::get_offset_of_touchPad_5(),
	Joystick_t9498292::get_offset_of_touchZone_6(),
	Joystick_t9498292::get_offset_of_deadZone_7(),
	Joystick_t9498292::get_offset_of_normalize_8(),
	Joystick_t9498292::get_offset_of_position_9(),
	Joystick_t9498292::get_offset_of_tapCount_10(),
	Joystick_t9498292::get_offset_of_lastFingerId_11(),
	Joystick_t9498292::get_offset_of_tapTimeWindow_12(),
	Joystick_t9498292::get_offset_of_fingerDownPos_13(),
	Joystick_t9498292::get_offset_of_fingerDownTime_14(),
	Joystick_t9498292::get_offset_of_firstDeltaTime_15(),
	Joystick_t9498292::get_offset_of_gui_16(),
	Joystick_t9498292::get_offset_of_defaultRect_17(),
	Joystick_t9498292::get_offset_of_guiBoundary_18(),
	Joystick_t9498292::get_offset_of_guiTouchOffset_19(),
	Joystick_t9498292::get_offset_of_guiCenter_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (ObliqueNear_t1763198098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1813[1] = 
{
	ObliqueNear_t1763198098::get_offset_of_plane_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (PlayerRelativeControl_t1056195335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1814[13] = 
{
	PlayerRelativeControl_t1056195335::get_offset_of_moveJoystick_2(),
	PlayerRelativeControl_t1056195335::get_offset_of_rotateJoystick_3(),
	PlayerRelativeControl_t1056195335::get_offset_of_cameraPivot_4(),
	PlayerRelativeControl_t1056195335::get_offset_of_forwardSpeed_5(),
	PlayerRelativeControl_t1056195335::get_offset_of_backwardSpeed_6(),
	PlayerRelativeControl_t1056195335::get_offset_of_sidestepSpeed_7(),
	PlayerRelativeControl_t1056195335::get_offset_of_jumpSpeed_8(),
	PlayerRelativeControl_t1056195335::get_offset_of_inAirMultiplier_9(),
	PlayerRelativeControl_t1056195335::get_offset_of_rotationSpeed_10(),
	PlayerRelativeControl_t1056195335::get_offset_of_thisTransform_11(),
	PlayerRelativeControl_t1056195335::get_offset_of_character_12(),
	PlayerRelativeControl_t1056195335::get_offset_of_cameraVelocity_13(),
	PlayerRelativeControl_t1056195335::get_offset_of_velocity_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (RollABall_t267000325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1815[4] = 
{
	RollABall_t267000325::get_offset_of_tilt_2(),
	RollABall_t267000325::get_offset_of_speed_3(),
	RollABall_t267000325::get_offset_of_circ_4(),
	RollABall_t267000325::get_offset_of_previousPosition_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (ConstraintAxis_t4094119807)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1816[4] = 
{
	ConstraintAxis_t4094119807::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (RotationConstraint_t2833302650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1817[8] = 
{
	RotationConstraint_t2833302650::get_offset_of_axis_2(),
	RotationConstraint_t2833302650::get_offset_of_min_3(),
	RotationConstraint_t2833302650::get_offset_of_max_4(),
	RotationConstraint_t2833302650::get_offset_of_thisTransform_5(),
	RotationConstraint_t2833302650::get_offset_of_rotateAround_6(),
	RotationConstraint_t2833302650::get_offset_of_minQuaternion_7(),
	RotationConstraint_t2833302650::get_offset_of_maxQuaternion_8(),
	RotationConstraint_t2833302650::get_offset_of_range_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (SidescrollControl_t463195584), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[10] = 
{
	SidescrollControl_t463195584::get_offset_of_moveTouchPad_2(),
	SidescrollControl_t463195584::get_offset_of_jumpTouchPad_3(),
	SidescrollControl_t463195584::get_offset_of_forwardSpeed_4(),
	SidescrollControl_t463195584::get_offset_of_backwardSpeed_5(),
	SidescrollControl_t463195584::get_offset_of_jumpSpeed_6(),
	SidescrollControl_t463195584::get_offset_of_inAirMultiplier_7(),
	SidescrollControl_t463195584::get_offset_of_thisTransform_8(),
	SidescrollControl_t463195584::get_offset_of_character_9(),
	SidescrollControl_t463195584::get_offset_of_velocity_10(),
	SidescrollControl_t463195584::get_offset_of_canJump_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (SmoothFollow2D_t2134035804), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1819[4] = 
{
	SmoothFollow2D_t2134035804::get_offset_of_target_2(),
	SmoothFollow2D_t2134035804::get_offset_of_smoothTime_3(),
	SmoothFollow2D_t2134035804::get_offset_of_thisTransform_4(),
	SmoothFollow2D_t2134035804::get_offset_of_velocity_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (ControlState_t1059495956)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1820[8] = 
{
	ControlState_t1059495956::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (tapcontrol_t522795363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1821[27] = 
{
	tapcontrol_t522795363::get_offset_of_cameraObject_2(),
	tapcontrol_t522795363::get_offset_of_cameraPivot_3(),
	tapcontrol_t522795363::get_offset_of_jumpButton_4(),
	tapcontrol_t522795363::get_offset_of_speed_5(),
	tapcontrol_t522795363::get_offset_of_jumpSpeed_6(),
	tapcontrol_t522795363::get_offset_of_inAirMultiplier_7(),
	tapcontrol_t522795363::get_offset_of_minimumDistanceToMove_8(),
	tapcontrol_t522795363::get_offset_of_minimumTimeUntilMove_9(),
	tapcontrol_t522795363::get_offset_of_zoomEnabled_10(),
	tapcontrol_t522795363::get_offset_of_zoomEpsilon_11(),
	tapcontrol_t522795363::get_offset_of_zoomRate_12(),
	tapcontrol_t522795363::get_offset_of_rotateEnabled_13(),
	tapcontrol_t522795363::get_offset_of_rotateEpsilon_14(),
	tapcontrol_t522795363::get_offset_of_zoomCamera_15(),
	tapcontrol_t522795363::get_offset_of_cam_16(),
	tapcontrol_t522795363::get_offset_of_thisTransform_17(),
	tapcontrol_t522795363::get_offset_of_character_18(),
	tapcontrol_t522795363::get_offset_of_targetLocation_19(),
	tapcontrol_t522795363::get_offset_of_moving_20(),
	tapcontrol_t522795363::get_offset_of_rotationTarget_21(),
	tapcontrol_t522795363::get_offset_of_rotationVelocity_22(),
	tapcontrol_t522795363::get_offset_of_velocity_23(),
	tapcontrol_t522795363::get_offset_of_state_24(),
	tapcontrol_t522795363::get_offset_of_fingerDown_25(),
	tapcontrol_t522795363::get_offset_of_fingerDownPosition_26(),
	tapcontrol_t522795363::get_offset_of_fingerDownFrame_27(),
	tapcontrol_t522795363::get_offset_of_firstTouchTime_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (ZoomCamera_t1350885688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1822[11] = 
{
	ZoomCamera_t1350885688::get_offset_of_origin_2(),
	ZoomCamera_t1350885688::get_offset_of_zoom_3(),
	ZoomCamera_t1350885688::get_offset_of_zoomMin_4(),
	ZoomCamera_t1350885688::get_offset_of_zoomMax_5(),
	ZoomCamera_t1350885688::get_offset_of_seekTime_6(),
	ZoomCamera_t1350885688::get_offset_of_smoothZoomIn_7(),
	ZoomCamera_t1350885688::get_offset_of_defaultLocalPosition_8(),
	ZoomCamera_t1350885688::get_offset_of_thisTransform_9(),
	ZoomCamera_t1350885688::get_offset_of_currentZoom_10(),
	ZoomCamera_t1350885688::get_offset_of_targetZoom_11(),
	ZoomCamera_t1350885688::get_offset_of_zoomVelocity_12(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
