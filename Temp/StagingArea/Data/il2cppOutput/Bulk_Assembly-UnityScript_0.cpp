﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};

// Boundary
struct Boundary_t2442033035;
// CameraRelativeControl
struct CameraRelativeControl_t2453217546;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// System.Type
struct Type_t;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.String
struct String_t;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.CharacterController
struct CharacterController_t1138636865;
// UnityEngine.Behaviour
struct Behaviour_t1437897464;
// FirstPersonControl
struct FirstPersonControl_t610459381;
// FollowTransform
struct FollowTransform_t1230726939;
// Joystick
struct Joystick_t9498292;
// UnityEngine.GUITexture
struct GUITexture_t951903601;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Object[]
struct ObjectU5BU5D_t1417781964;
// UnityEngine.GUIElement
struct GUIElement_t3567083079;
// ObliqueNear
struct ObliqueNear_t1763198098;
// UnityEngine.Camera
struct Camera_t4157153871;
// PlayerRelativeControl
struct PlayerRelativeControl_t1056195335;
// RollABall
struct RollABall_t267000325;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// RotationConstraint
struct RotationConstraint_t2833302650;
// SidescrollControl
struct SidescrollControl_t463195584;
// SmoothFollow2D
struct SmoothFollow2D_t2134035804;
// tapcontrol
struct tapcontrol_t522795363;
// UnityEngine.Touch[]
struct TouchU5BU5D_t1849554061;
// ZoomCamera
struct ZoomCamera_t1350885688;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t190067161;
// Joystick[]
struct JoystickU5BU5D_t4275182589;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;

extern RuntimeClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern const uint32_t Boundary__ctor_m1524210624_MetadataUsageId;
extern const RuntimeType* Transform_t3600365921_0_0_0_var;
extern const RuntimeType* CharacterController_t1138636865_0_0_0_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Transform_t3600365921_il2cpp_TypeInfo_var;
extern RuntimeClass* CharacterController_t1138636865_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral4038994995;
extern const uint32_t CameraRelativeControl_Start_m3710056517_MetadataUsageId;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern const uint32_t CameraRelativeControl_Update_m4161957553_MetadataUsageId;
extern const uint32_t FirstPersonControl_Start_m3117946383_MetadataUsageId;
extern const uint32_t FirstPersonControl_OnEndGame_m3664793188_MetadataUsageId;
extern RuntimeClass* Input_t1431474628_il2cpp_TypeInfo_var;
extern const uint32_t FirstPersonControl_Update_m2287003549_MetadataUsageId;
extern RuntimeClass* Boundary_t2442033035_il2cpp_TypeInfo_var;
extern const uint32_t Joystick__ctor_m2202167957_MetadataUsageId;
extern RuntimeClass* Joystick_t9498292_il2cpp_TypeInfo_var;
extern const uint32_t Joystick__cctor_m885003068_MetadataUsageId;
extern const RuntimeType* GUITexture_t951903601_0_0_0_var;
extern RuntimeClass* GUITexture_t951903601_il2cpp_TypeInfo_var;
extern const uint32_t Joystick_Start_m1421348663_MetadataUsageId;
extern const uint32_t Joystick_Disable_m3439812130_MetadataUsageId;
extern const uint32_t Joystick_ResetJoystick_m1139569856_MetadataUsageId;
extern const RuntimeType* Joystick_t9498292_0_0_0_var;
extern RuntimeClass* JoystickU5BU5D_t4275182589_il2cpp_TypeInfo_var;
extern const uint32_t Joystick_Update_m724918018_MetadataUsageId;
extern RuntimeClass* Matrix4x4_t1817901843_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector4_t3319028937_il2cpp_TypeInfo_var;
extern const uint32_t ObliqueNear_CalculateObliqueMatrix_m4080248109_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var;
extern const uint32_t ObliqueNear_OnPreCull_m2018901442_MetadataUsageId;
extern const uint32_t PlayerRelativeControl_Start_m2946113978_MetadataUsageId;
extern const uint32_t PlayerRelativeControl_Update_m1496946618_MetadataUsageId;
extern const uint32_t RollABall__ctor_m476080020_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisCollider_t1773347010_m4226749020_RuntimeMethod_var;
extern const uint32_t RollABall_Start_m2109304508_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisRigidbody_t3916780224_m279685075_RuntimeMethod_var;
extern const uint32_t RollABall_Update_m3453512694_MetadataUsageId;
extern const uint32_t RollABall_LateUpdate_m166097054_MetadataUsageId;
extern RuntimeClass* Quaternion_t2301928331_il2cpp_TypeInfo_var;
extern const uint32_t RotationConstraint_Start_m27902014_MetadataUsageId;
extern const uint32_t RotationConstraint_LateUpdate_m3448396124_MetadataUsageId;
extern const uint32_t SidescrollControl_Start_m3223018177_MetadataUsageId;
extern const uint32_t SidescrollControl_Update_m550749_MetadataUsageId;
extern const uint32_t SmoothFollow2D_Update_m3823838575_MetadataUsageId;
extern RuntimeClass* Int32U5BU5D_t385246372_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2U5BU5D_t1457185986_il2cpp_TypeInfo_var;
extern const uint32_t tapcontrol__ctor_m1318957847_MetadataUsageId;
extern const RuntimeType* ZoomCamera_t1350885688_0_0_0_var;
extern RuntimeClass* ZoomCamera_t1350885688_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponent_TisCamera_t4157153871_m3956151066_RuntimeMethod_var;
extern const uint32_t tapcontrol_Start_m3681805517_MetadataUsageId;
extern const uint32_t tapcontrol_CameraControl_m3642245434_MetadataUsageId;
extern const uint32_t tapcontrol_CharacterControl_m768722118_MetadataUsageId;
extern const uint32_t tapcontrol_Update_m502715398_MetadataUsageId;
extern const uint32_t tapcontrol_LateUpdate_m1804980605_MetadataUsageId;
extern const uint32_t ZoomCamera_Update_m852922365_MetadataUsageId;

struct JoystickU5BU5D_t4275182589;
struct ObjectU5BU5D_t1417781964;
struct Int32U5BU5D_t385246372;
struct Vector2U5BU5D_t1457185986;
struct TouchU5BU5D_t1849554061;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745544_H
#define U3CMODULEU3E_T692745544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745544 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745544_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef TOUCHPHASE_T72348083_H
#define TOUCHPHASE_T72348083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t72348083 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t72348083, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T72348083_H
#ifndef TOUCHTYPE_T2034578258_H
#define TOUCHTYPE_T2034578258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t2034578258 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchType_t2034578258, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T2034578258_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
#ifndef BOUNDS_T2266837910_H
#define BOUNDS_T2266837910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t2266837910 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3722313464  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3722313464  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Center_0)); }
	inline Vector3_t3722313464  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3722313464 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3722313464  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Extents_1)); }
	inline Vector3_t3722313464  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3722313464 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3722313464  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T2266837910_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef BOUNDARY_T2442033035_H
#define BOUNDARY_T2442033035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boundary
struct  Boundary_t2442033035  : public RuntimeObject
{
public:
	// UnityEngine.Vector2 Boundary::min
	Vector2_t2156229523  ___min_0;
	// UnityEngine.Vector2 Boundary::max
	Vector2_t2156229523  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Boundary_t2442033035, ___min_0)); }
	inline Vector2_t2156229523  get_min_0() const { return ___min_0; }
	inline Vector2_t2156229523 * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_t2156229523  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Boundary_t2442033035, ___max_1)); }
	inline Vector2_t2156229523  get_max_1() const { return ___max_1; }
	inline Vector2_t2156229523 * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_t2156229523  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDARY_T2442033035_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t1773347010 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline Collider_t1773347010 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t1773347010 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_pinvoke
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_com
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T1056001966_H
#ifndef COLLISIONFLAGS_T1776808576_H
#define COLLISIONFLAGS_T1776808576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CollisionFlags
struct  CollisionFlags_t1776808576 
{
public:
	// System.Int32 UnityEngine.CollisionFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CollisionFlags_t1776808576, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISIONFLAGS_T1776808576_H
#ifndef SPACE_T654135784_H
#define SPACE_T654135784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Space
struct  Space_t654135784 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Space_t654135784, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACE_T654135784_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef CONSTRAINTAXIS_T4094119807_H
#define CONSTRAINTAXIS_T4094119807_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConstraintAxis
struct  ConstraintAxis_t4094119807 
{
public:
	// System.Int32 ConstraintAxis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConstraintAxis_t4094119807, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINTAXIS_T4094119807_H
#ifndef CONTROLSTATE_T1059495956_H
#define CONTROLSTATE_T1059495956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControlState
struct  ControlState_t1059495956 
{
public:
	// System.Int32 ControlState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ControlState_t1059495956, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLSTATE_T1059495956_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t426314064 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t426314064 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef TOUCH_T1921856868_H
#define TOUCH_T1921856868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t1921856868 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t2156229523  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t2156229523  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t2156229523  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Position_1)); }
	inline Vector2_t2156229523  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_t2156229523 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_t2156229523  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_RawPosition_2)); }
	inline Vector2_t2156229523  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_t2156229523 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_t2156229523  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_PositionDelta_3)); }
	inline Vector2_t2156229523  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_t2156229523 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_t2156229523  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T1921856868_H
#ifndef TEXTURE_T3661962703_H
#define TEXTURE_T3661962703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t3661962703  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T3661962703_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef COLLIDER_T1773347010_H
#define COLLIDER_T1773347010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider
struct  Collider_t1773347010  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER_T1773347010_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef RIGIDBODY_T3916780224_H
#define RIGIDBODY_T3916780224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody
struct  Rigidbody_t3916780224  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY_T3916780224_H
#ifndef GUIELEMENT_T3567083079_H
#define GUIELEMENT_T3567083079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIElement
struct  GUIElement_t3567083079  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIELEMENT_T3567083079_H
#ifndef CHARACTERCONTROLLER_T1138636865_H
#define CHARACTERCONTROLLER_T1138636865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CharacterController
struct  CharacterController_t1138636865  : public Collider_t1773347010
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERCONTROLLER_T1138636865_H
#ifndef CAMERA_T4157153871_H
#define CAMERA_T4157153871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t4157153871  : public Behaviour_t1437897464
{
public:

public:
};

struct Camera_t4157153871_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t190067161 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t190067161 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t190067161 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t190067161 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t190067161 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t190067161 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t190067161 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t190067161 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t190067161 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t190067161 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t190067161 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t190067161 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T4157153871_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef CAMERARELATIVECONTROL_T2453217546_H
#define CAMERARELATIVECONTROL_T2453217546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraRelativeControl
struct  CameraRelativeControl_t2453217546  : public MonoBehaviour_t3962482529
{
public:
	// Joystick CameraRelativeControl::moveJoystick
	Joystick_t9498292 * ___moveJoystick_2;
	// Joystick CameraRelativeControl::rotateJoystick
	Joystick_t9498292 * ___rotateJoystick_3;
	// UnityEngine.Transform CameraRelativeControl::cameraPivot
	Transform_t3600365921 * ___cameraPivot_4;
	// UnityEngine.Transform CameraRelativeControl::cameraTransform
	Transform_t3600365921 * ___cameraTransform_5;
	// System.Single CameraRelativeControl::speed
	float ___speed_6;
	// System.Single CameraRelativeControl::jumpSpeed
	float ___jumpSpeed_7;
	// System.Single CameraRelativeControl::inAirMultiplier
	float ___inAirMultiplier_8;
	// UnityEngine.Vector2 CameraRelativeControl::rotationSpeed
	Vector2_t2156229523  ___rotationSpeed_9;
	// UnityEngine.Transform CameraRelativeControl::thisTransform
	Transform_t3600365921 * ___thisTransform_10;
	// UnityEngine.CharacterController CameraRelativeControl::character
	CharacterController_t1138636865 * ___character_11;
	// UnityEngine.Vector3 CameraRelativeControl::velocity
	Vector3_t3722313464  ___velocity_12;
	// System.Boolean CameraRelativeControl::canJump
	bool ___canJump_13;

public:
	inline static int32_t get_offset_of_moveJoystick_2() { return static_cast<int32_t>(offsetof(CameraRelativeControl_t2453217546, ___moveJoystick_2)); }
	inline Joystick_t9498292 * get_moveJoystick_2() const { return ___moveJoystick_2; }
	inline Joystick_t9498292 ** get_address_of_moveJoystick_2() { return &___moveJoystick_2; }
	inline void set_moveJoystick_2(Joystick_t9498292 * value)
	{
		___moveJoystick_2 = value;
		Il2CppCodeGenWriteBarrier((&___moveJoystick_2), value);
	}

	inline static int32_t get_offset_of_rotateJoystick_3() { return static_cast<int32_t>(offsetof(CameraRelativeControl_t2453217546, ___rotateJoystick_3)); }
	inline Joystick_t9498292 * get_rotateJoystick_3() const { return ___rotateJoystick_3; }
	inline Joystick_t9498292 ** get_address_of_rotateJoystick_3() { return &___rotateJoystick_3; }
	inline void set_rotateJoystick_3(Joystick_t9498292 * value)
	{
		___rotateJoystick_3 = value;
		Il2CppCodeGenWriteBarrier((&___rotateJoystick_3), value);
	}

	inline static int32_t get_offset_of_cameraPivot_4() { return static_cast<int32_t>(offsetof(CameraRelativeControl_t2453217546, ___cameraPivot_4)); }
	inline Transform_t3600365921 * get_cameraPivot_4() const { return ___cameraPivot_4; }
	inline Transform_t3600365921 ** get_address_of_cameraPivot_4() { return &___cameraPivot_4; }
	inline void set_cameraPivot_4(Transform_t3600365921 * value)
	{
		___cameraPivot_4 = value;
		Il2CppCodeGenWriteBarrier((&___cameraPivot_4), value);
	}

	inline static int32_t get_offset_of_cameraTransform_5() { return static_cast<int32_t>(offsetof(CameraRelativeControl_t2453217546, ___cameraTransform_5)); }
	inline Transform_t3600365921 * get_cameraTransform_5() const { return ___cameraTransform_5; }
	inline Transform_t3600365921 ** get_address_of_cameraTransform_5() { return &___cameraTransform_5; }
	inline void set_cameraTransform_5(Transform_t3600365921 * value)
	{
		___cameraTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_5), value);
	}

	inline static int32_t get_offset_of_speed_6() { return static_cast<int32_t>(offsetof(CameraRelativeControl_t2453217546, ___speed_6)); }
	inline float get_speed_6() const { return ___speed_6; }
	inline float* get_address_of_speed_6() { return &___speed_6; }
	inline void set_speed_6(float value)
	{
		___speed_6 = value;
	}

	inline static int32_t get_offset_of_jumpSpeed_7() { return static_cast<int32_t>(offsetof(CameraRelativeControl_t2453217546, ___jumpSpeed_7)); }
	inline float get_jumpSpeed_7() const { return ___jumpSpeed_7; }
	inline float* get_address_of_jumpSpeed_7() { return &___jumpSpeed_7; }
	inline void set_jumpSpeed_7(float value)
	{
		___jumpSpeed_7 = value;
	}

	inline static int32_t get_offset_of_inAirMultiplier_8() { return static_cast<int32_t>(offsetof(CameraRelativeControl_t2453217546, ___inAirMultiplier_8)); }
	inline float get_inAirMultiplier_8() const { return ___inAirMultiplier_8; }
	inline float* get_address_of_inAirMultiplier_8() { return &___inAirMultiplier_8; }
	inline void set_inAirMultiplier_8(float value)
	{
		___inAirMultiplier_8 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_9() { return static_cast<int32_t>(offsetof(CameraRelativeControl_t2453217546, ___rotationSpeed_9)); }
	inline Vector2_t2156229523  get_rotationSpeed_9() const { return ___rotationSpeed_9; }
	inline Vector2_t2156229523 * get_address_of_rotationSpeed_9() { return &___rotationSpeed_9; }
	inline void set_rotationSpeed_9(Vector2_t2156229523  value)
	{
		___rotationSpeed_9 = value;
	}

	inline static int32_t get_offset_of_thisTransform_10() { return static_cast<int32_t>(offsetof(CameraRelativeControl_t2453217546, ___thisTransform_10)); }
	inline Transform_t3600365921 * get_thisTransform_10() const { return ___thisTransform_10; }
	inline Transform_t3600365921 ** get_address_of_thisTransform_10() { return &___thisTransform_10; }
	inline void set_thisTransform_10(Transform_t3600365921 * value)
	{
		___thisTransform_10 = value;
		Il2CppCodeGenWriteBarrier((&___thisTransform_10), value);
	}

	inline static int32_t get_offset_of_character_11() { return static_cast<int32_t>(offsetof(CameraRelativeControl_t2453217546, ___character_11)); }
	inline CharacterController_t1138636865 * get_character_11() const { return ___character_11; }
	inline CharacterController_t1138636865 ** get_address_of_character_11() { return &___character_11; }
	inline void set_character_11(CharacterController_t1138636865 * value)
	{
		___character_11 = value;
		Il2CppCodeGenWriteBarrier((&___character_11), value);
	}

	inline static int32_t get_offset_of_velocity_12() { return static_cast<int32_t>(offsetof(CameraRelativeControl_t2453217546, ___velocity_12)); }
	inline Vector3_t3722313464  get_velocity_12() const { return ___velocity_12; }
	inline Vector3_t3722313464 * get_address_of_velocity_12() { return &___velocity_12; }
	inline void set_velocity_12(Vector3_t3722313464  value)
	{
		___velocity_12 = value;
	}

	inline static int32_t get_offset_of_canJump_13() { return static_cast<int32_t>(offsetof(CameraRelativeControl_t2453217546, ___canJump_13)); }
	inline bool get_canJump_13() const { return ___canJump_13; }
	inline bool* get_address_of_canJump_13() { return &___canJump_13; }
	inline void set_canJump_13(bool value)
	{
		___canJump_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERARELATIVECONTROL_T2453217546_H
#ifndef FOLLOWTRANSFORM_T1230726939_H
#define FOLLOWTRANSFORM_T1230726939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FollowTransform
struct  FollowTransform_t1230726939  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform FollowTransform::targetTransform
	Transform_t3600365921 * ___targetTransform_2;
	// System.Boolean FollowTransform::faceForward
	bool ___faceForward_3;
	// UnityEngine.Transform FollowTransform::thisTransform
	Transform_t3600365921 * ___thisTransform_4;

public:
	inline static int32_t get_offset_of_targetTransform_2() { return static_cast<int32_t>(offsetof(FollowTransform_t1230726939, ___targetTransform_2)); }
	inline Transform_t3600365921 * get_targetTransform_2() const { return ___targetTransform_2; }
	inline Transform_t3600365921 ** get_address_of_targetTransform_2() { return &___targetTransform_2; }
	inline void set_targetTransform_2(Transform_t3600365921 * value)
	{
		___targetTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___targetTransform_2), value);
	}

	inline static int32_t get_offset_of_faceForward_3() { return static_cast<int32_t>(offsetof(FollowTransform_t1230726939, ___faceForward_3)); }
	inline bool get_faceForward_3() const { return ___faceForward_3; }
	inline bool* get_address_of_faceForward_3() { return &___faceForward_3; }
	inline void set_faceForward_3(bool value)
	{
		___faceForward_3 = value;
	}

	inline static int32_t get_offset_of_thisTransform_4() { return static_cast<int32_t>(offsetof(FollowTransform_t1230726939, ___thisTransform_4)); }
	inline Transform_t3600365921 * get_thisTransform_4() const { return ___thisTransform_4; }
	inline Transform_t3600365921 ** get_address_of_thisTransform_4() { return &___thisTransform_4; }
	inline void set_thisTransform_4(Transform_t3600365921 * value)
	{
		___thisTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___thisTransform_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLLOWTRANSFORM_T1230726939_H
#ifndef JOYSTICK_T9498292_H
#define JOYSTICK_T9498292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Joystick
struct  Joystick_t9498292  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Joystick::touchPad
	bool ___touchPad_5;
	// UnityEngine.Rect Joystick::touchZone
	Rect_t2360479859  ___touchZone_6;
	// UnityEngine.Vector2 Joystick::deadZone
	Vector2_t2156229523  ___deadZone_7;
	// System.Boolean Joystick::normalize
	bool ___normalize_8;
	// UnityEngine.Vector2 Joystick::position
	Vector2_t2156229523  ___position_9;
	// System.Int32 Joystick::tapCount
	int32_t ___tapCount_10;
	// System.Int32 Joystick::lastFingerId
	int32_t ___lastFingerId_11;
	// System.Single Joystick::tapTimeWindow
	float ___tapTimeWindow_12;
	// UnityEngine.Vector2 Joystick::fingerDownPos
	Vector2_t2156229523  ___fingerDownPos_13;
	// System.Single Joystick::fingerDownTime
	float ___fingerDownTime_14;
	// System.Single Joystick::firstDeltaTime
	float ___firstDeltaTime_15;
	// UnityEngine.GUITexture Joystick::gui
	GUITexture_t951903601 * ___gui_16;
	// UnityEngine.Rect Joystick::defaultRect
	Rect_t2360479859  ___defaultRect_17;
	// Boundary Joystick::guiBoundary
	Boundary_t2442033035 * ___guiBoundary_18;
	// UnityEngine.Vector2 Joystick::guiTouchOffset
	Vector2_t2156229523  ___guiTouchOffset_19;
	// UnityEngine.Vector2 Joystick::guiCenter
	Vector2_t2156229523  ___guiCenter_20;

public:
	inline static int32_t get_offset_of_touchPad_5() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___touchPad_5)); }
	inline bool get_touchPad_5() const { return ___touchPad_5; }
	inline bool* get_address_of_touchPad_5() { return &___touchPad_5; }
	inline void set_touchPad_5(bool value)
	{
		___touchPad_5 = value;
	}

	inline static int32_t get_offset_of_touchZone_6() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___touchZone_6)); }
	inline Rect_t2360479859  get_touchZone_6() const { return ___touchZone_6; }
	inline Rect_t2360479859 * get_address_of_touchZone_6() { return &___touchZone_6; }
	inline void set_touchZone_6(Rect_t2360479859  value)
	{
		___touchZone_6 = value;
	}

	inline static int32_t get_offset_of_deadZone_7() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___deadZone_7)); }
	inline Vector2_t2156229523  get_deadZone_7() const { return ___deadZone_7; }
	inline Vector2_t2156229523 * get_address_of_deadZone_7() { return &___deadZone_7; }
	inline void set_deadZone_7(Vector2_t2156229523  value)
	{
		___deadZone_7 = value;
	}

	inline static int32_t get_offset_of_normalize_8() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___normalize_8)); }
	inline bool get_normalize_8() const { return ___normalize_8; }
	inline bool* get_address_of_normalize_8() { return &___normalize_8; }
	inline void set_normalize_8(bool value)
	{
		___normalize_8 = value;
	}

	inline static int32_t get_offset_of_position_9() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___position_9)); }
	inline Vector2_t2156229523  get_position_9() const { return ___position_9; }
	inline Vector2_t2156229523 * get_address_of_position_9() { return &___position_9; }
	inline void set_position_9(Vector2_t2156229523  value)
	{
		___position_9 = value;
	}

	inline static int32_t get_offset_of_tapCount_10() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___tapCount_10)); }
	inline int32_t get_tapCount_10() const { return ___tapCount_10; }
	inline int32_t* get_address_of_tapCount_10() { return &___tapCount_10; }
	inline void set_tapCount_10(int32_t value)
	{
		___tapCount_10 = value;
	}

	inline static int32_t get_offset_of_lastFingerId_11() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___lastFingerId_11)); }
	inline int32_t get_lastFingerId_11() const { return ___lastFingerId_11; }
	inline int32_t* get_address_of_lastFingerId_11() { return &___lastFingerId_11; }
	inline void set_lastFingerId_11(int32_t value)
	{
		___lastFingerId_11 = value;
	}

	inline static int32_t get_offset_of_tapTimeWindow_12() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___tapTimeWindow_12)); }
	inline float get_tapTimeWindow_12() const { return ___tapTimeWindow_12; }
	inline float* get_address_of_tapTimeWindow_12() { return &___tapTimeWindow_12; }
	inline void set_tapTimeWindow_12(float value)
	{
		___tapTimeWindow_12 = value;
	}

	inline static int32_t get_offset_of_fingerDownPos_13() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___fingerDownPos_13)); }
	inline Vector2_t2156229523  get_fingerDownPos_13() const { return ___fingerDownPos_13; }
	inline Vector2_t2156229523 * get_address_of_fingerDownPos_13() { return &___fingerDownPos_13; }
	inline void set_fingerDownPos_13(Vector2_t2156229523  value)
	{
		___fingerDownPos_13 = value;
	}

	inline static int32_t get_offset_of_fingerDownTime_14() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___fingerDownTime_14)); }
	inline float get_fingerDownTime_14() const { return ___fingerDownTime_14; }
	inline float* get_address_of_fingerDownTime_14() { return &___fingerDownTime_14; }
	inline void set_fingerDownTime_14(float value)
	{
		___fingerDownTime_14 = value;
	}

	inline static int32_t get_offset_of_firstDeltaTime_15() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___firstDeltaTime_15)); }
	inline float get_firstDeltaTime_15() const { return ___firstDeltaTime_15; }
	inline float* get_address_of_firstDeltaTime_15() { return &___firstDeltaTime_15; }
	inline void set_firstDeltaTime_15(float value)
	{
		___firstDeltaTime_15 = value;
	}

	inline static int32_t get_offset_of_gui_16() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___gui_16)); }
	inline GUITexture_t951903601 * get_gui_16() const { return ___gui_16; }
	inline GUITexture_t951903601 ** get_address_of_gui_16() { return &___gui_16; }
	inline void set_gui_16(GUITexture_t951903601 * value)
	{
		___gui_16 = value;
		Il2CppCodeGenWriteBarrier((&___gui_16), value);
	}

	inline static int32_t get_offset_of_defaultRect_17() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___defaultRect_17)); }
	inline Rect_t2360479859  get_defaultRect_17() const { return ___defaultRect_17; }
	inline Rect_t2360479859 * get_address_of_defaultRect_17() { return &___defaultRect_17; }
	inline void set_defaultRect_17(Rect_t2360479859  value)
	{
		___defaultRect_17 = value;
	}

	inline static int32_t get_offset_of_guiBoundary_18() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___guiBoundary_18)); }
	inline Boundary_t2442033035 * get_guiBoundary_18() const { return ___guiBoundary_18; }
	inline Boundary_t2442033035 ** get_address_of_guiBoundary_18() { return &___guiBoundary_18; }
	inline void set_guiBoundary_18(Boundary_t2442033035 * value)
	{
		___guiBoundary_18 = value;
		Il2CppCodeGenWriteBarrier((&___guiBoundary_18), value);
	}

	inline static int32_t get_offset_of_guiTouchOffset_19() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___guiTouchOffset_19)); }
	inline Vector2_t2156229523  get_guiTouchOffset_19() const { return ___guiTouchOffset_19; }
	inline Vector2_t2156229523 * get_address_of_guiTouchOffset_19() { return &___guiTouchOffset_19; }
	inline void set_guiTouchOffset_19(Vector2_t2156229523  value)
	{
		___guiTouchOffset_19 = value;
	}

	inline static int32_t get_offset_of_guiCenter_20() { return static_cast<int32_t>(offsetof(Joystick_t9498292, ___guiCenter_20)); }
	inline Vector2_t2156229523  get_guiCenter_20() const { return ___guiCenter_20; }
	inline Vector2_t2156229523 * get_address_of_guiCenter_20() { return &___guiCenter_20; }
	inline void set_guiCenter_20(Vector2_t2156229523  value)
	{
		___guiCenter_20 = value;
	}
};

struct Joystick_t9498292_StaticFields
{
public:
	// Joystick[] Joystick::joysticks
	JoystickU5BU5D_t4275182589* ___joysticks_2;
	// System.Boolean Joystick::enumeratedJoysticks
	bool ___enumeratedJoysticks_3;
	// System.Single Joystick::tapTimeDelta
	float ___tapTimeDelta_4;

public:
	inline static int32_t get_offset_of_joysticks_2() { return static_cast<int32_t>(offsetof(Joystick_t9498292_StaticFields, ___joysticks_2)); }
	inline JoystickU5BU5D_t4275182589* get_joysticks_2() const { return ___joysticks_2; }
	inline JoystickU5BU5D_t4275182589** get_address_of_joysticks_2() { return &___joysticks_2; }
	inline void set_joysticks_2(JoystickU5BU5D_t4275182589* value)
	{
		___joysticks_2 = value;
		Il2CppCodeGenWriteBarrier((&___joysticks_2), value);
	}

	inline static int32_t get_offset_of_enumeratedJoysticks_3() { return static_cast<int32_t>(offsetof(Joystick_t9498292_StaticFields, ___enumeratedJoysticks_3)); }
	inline bool get_enumeratedJoysticks_3() const { return ___enumeratedJoysticks_3; }
	inline bool* get_address_of_enumeratedJoysticks_3() { return &___enumeratedJoysticks_3; }
	inline void set_enumeratedJoysticks_3(bool value)
	{
		___enumeratedJoysticks_3 = value;
	}

	inline static int32_t get_offset_of_tapTimeDelta_4() { return static_cast<int32_t>(offsetof(Joystick_t9498292_StaticFields, ___tapTimeDelta_4)); }
	inline float get_tapTimeDelta_4() const { return ___tapTimeDelta_4; }
	inline float* get_address_of_tapTimeDelta_4() { return &___tapTimeDelta_4; }
	inline void set_tapTimeDelta_4(float value)
	{
		___tapTimeDelta_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICK_T9498292_H
#ifndef GUITEXTURE_T951903601_H
#define GUITEXTURE_T951903601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUITexture
struct  GUITexture_t951903601  : public GUIElement_t3567083079
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUITEXTURE_T951903601_H
#ifndef FIRSTPERSONCONTROL_T610459381_H
#define FIRSTPERSONCONTROL_T610459381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FirstPersonControl
struct  FirstPersonControl_t610459381  : public MonoBehaviour_t3962482529
{
public:
	// Joystick FirstPersonControl::moveTouchPad
	Joystick_t9498292 * ___moveTouchPad_2;
	// Joystick FirstPersonControl::rotateTouchPad
	Joystick_t9498292 * ___rotateTouchPad_3;
	// UnityEngine.Transform FirstPersonControl::cameraPivot
	Transform_t3600365921 * ___cameraPivot_4;
	// System.Single FirstPersonControl::forwardSpeed
	float ___forwardSpeed_5;
	// System.Single FirstPersonControl::backwardSpeed
	float ___backwardSpeed_6;
	// System.Single FirstPersonControl::sidestepSpeed
	float ___sidestepSpeed_7;
	// System.Single FirstPersonControl::jumpSpeed
	float ___jumpSpeed_8;
	// System.Single FirstPersonControl::inAirMultiplier
	float ___inAirMultiplier_9;
	// UnityEngine.Vector2 FirstPersonControl::rotationSpeed
	Vector2_t2156229523  ___rotationSpeed_10;
	// System.Single FirstPersonControl::tiltPositiveYAxis
	float ___tiltPositiveYAxis_11;
	// System.Single FirstPersonControl::tiltNegativeYAxis
	float ___tiltNegativeYAxis_12;
	// System.Single FirstPersonControl::tiltXAxisMinimum
	float ___tiltXAxisMinimum_13;
	// UnityEngine.Transform FirstPersonControl::thisTransform
	Transform_t3600365921 * ___thisTransform_14;
	// UnityEngine.CharacterController FirstPersonControl::character
	CharacterController_t1138636865 * ___character_15;
	// UnityEngine.Vector3 FirstPersonControl::cameraVelocity
	Vector3_t3722313464  ___cameraVelocity_16;
	// UnityEngine.Vector3 FirstPersonControl::velocity
	Vector3_t3722313464  ___velocity_17;
	// System.Boolean FirstPersonControl::canJump
	bool ___canJump_18;

public:
	inline static int32_t get_offset_of_moveTouchPad_2() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___moveTouchPad_2)); }
	inline Joystick_t9498292 * get_moveTouchPad_2() const { return ___moveTouchPad_2; }
	inline Joystick_t9498292 ** get_address_of_moveTouchPad_2() { return &___moveTouchPad_2; }
	inline void set_moveTouchPad_2(Joystick_t9498292 * value)
	{
		___moveTouchPad_2 = value;
		Il2CppCodeGenWriteBarrier((&___moveTouchPad_2), value);
	}

	inline static int32_t get_offset_of_rotateTouchPad_3() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___rotateTouchPad_3)); }
	inline Joystick_t9498292 * get_rotateTouchPad_3() const { return ___rotateTouchPad_3; }
	inline Joystick_t9498292 ** get_address_of_rotateTouchPad_3() { return &___rotateTouchPad_3; }
	inline void set_rotateTouchPad_3(Joystick_t9498292 * value)
	{
		___rotateTouchPad_3 = value;
		Il2CppCodeGenWriteBarrier((&___rotateTouchPad_3), value);
	}

	inline static int32_t get_offset_of_cameraPivot_4() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___cameraPivot_4)); }
	inline Transform_t3600365921 * get_cameraPivot_4() const { return ___cameraPivot_4; }
	inline Transform_t3600365921 ** get_address_of_cameraPivot_4() { return &___cameraPivot_4; }
	inline void set_cameraPivot_4(Transform_t3600365921 * value)
	{
		___cameraPivot_4 = value;
		Il2CppCodeGenWriteBarrier((&___cameraPivot_4), value);
	}

	inline static int32_t get_offset_of_forwardSpeed_5() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___forwardSpeed_5)); }
	inline float get_forwardSpeed_5() const { return ___forwardSpeed_5; }
	inline float* get_address_of_forwardSpeed_5() { return &___forwardSpeed_5; }
	inline void set_forwardSpeed_5(float value)
	{
		___forwardSpeed_5 = value;
	}

	inline static int32_t get_offset_of_backwardSpeed_6() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___backwardSpeed_6)); }
	inline float get_backwardSpeed_6() const { return ___backwardSpeed_6; }
	inline float* get_address_of_backwardSpeed_6() { return &___backwardSpeed_6; }
	inline void set_backwardSpeed_6(float value)
	{
		___backwardSpeed_6 = value;
	}

	inline static int32_t get_offset_of_sidestepSpeed_7() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___sidestepSpeed_7)); }
	inline float get_sidestepSpeed_7() const { return ___sidestepSpeed_7; }
	inline float* get_address_of_sidestepSpeed_7() { return &___sidestepSpeed_7; }
	inline void set_sidestepSpeed_7(float value)
	{
		___sidestepSpeed_7 = value;
	}

	inline static int32_t get_offset_of_jumpSpeed_8() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___jumpSpeed_8)); }
	inline float get_jumpSpeed_8() const { return ___jumpSpeed_8; }
	inline float* get_address_of_jumpSpeed_8() { return &___jumpSpeed_8; }
	inline void set_jumpSpeed_8(float value)
	{
		___jumpSpeed_8 = value;
	}

	inline static int32_t get_offset_of_inAirMultiplier_9() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___inAirMultiplier_9)); }
	inline float get_inAirMultiplier_9() const { return ___inAirMultiplier_9; }
	inline float* get_address_of_inAirMultiplier_9() { return &___inAirMultiplier_9; }
	inline void set_inAirMultiplier_9(float value)
	{
		___inAirMultiplier_9 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_10() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___rotationSpeed_10)); }
	inline Vector2_t2156229523  get_rotationSpeed_10() const { return ___rotationSpeed_10; }
	inline Vector2_t2156229523 * get_address_of_rotationSpeed_10() { return &___rotationSpeed_10; }
	inline void set_rotationSpeed_10(Vector2_t2156229523  value)
	{
		___rotationSpeed_10 = value;
	}

	inline static int32_t get_offset_of_tiltPositiveYAxis_11() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___tiltPositiveYAxis_11)); }
	inline float get_tiltPositiveYAxis_11() const { return ___tiltPositiveYAxis_11; }
	inline float* get_address_of_tiltPositiveYAxis_11() { return &___tiltPositiveYAxis_11; }
	inline void set_tiltPositiveYAxis_11(float value)
	{
		___tiltPositiveYAxis_11 = value;
	}

	inline static int32_t get_offset_of_tiltNegativeYAxis_12() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___tiltNegativeYAxis_12)); }
	inline float get_tiltNegativeYAxis_12() const { return ___tiltNegativeYAxis_12; }
	inline float* get_address_of_tiltNegativeYAxis_12() { return &___tiltNegativeYAxis_12; }
	inline void set_tiltNegativeYAxis_12(float value)
	{
		___tiltNegativeYAxis_12 = value;
	}

	inline static int32_t get_offset_of_tiltXAxisMinimum_13() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___tiltXAxisMinimum_13)); }
	inline float get_tiltXAxisMinimum_13() const { return ___tiltXAxisMinimum_13; }
	inline float* get_address_of_tiltXAxisMinimum_13() { return &___tiltXAxisMinimum_13; }
	inline void set_tiltXAxisMinimum_13(float value)
	{
		___tiltXAxisMinimum_13 = value;
	}

	inline static int32_t get_offset_of_thisTransform_14() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___thisTransform_14)); }
	inline Transform_t3600365921 * get_thisTransform_14() const { return ___thisTransform_14; }
	inline Transform_t3600365921 ** get_address_of_thisTransform_14() { return &___thisTransform_14; }
	inline void set_thisTransform_14(Transform_t3600365921 * value)
	{
		___thisTransform_14 = value;
		Il2CppCodeGenWriteBarrier((&___thisTransform_14), value);
	}

	inline static int32_t get_offset_of_character_15() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___character_15)); }
	inline CharacterController_t1138636865 * get_character_15() const { return ___character_15; }
	inline CharacterController_t1138636865 ** get_address_of_character_15() { return &___character_15; }
	inline void set_character_15(CharacterController_t1138636865 * value)
	{
		___character_15 = value;
		Il2CppCodeGenWriteBarrier((&___character_15), value);
	}

	inline static int32_t get_offset_of_cameraVelocity_16() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___cameraVelocity_16)); }
	inline Vector3_t3722313464  get_cameraVelocity_16() const { return ___cameraVelocity_16; }
	inline Vector3_t3722313464 * get_address_of_cameraVelocity_16() { return &___cameraVelocity_16; }
	inline void set_cameraVelocity_16(Vector3_t3722313464  value)
	{
		___cameraVelocity_16 = value;
	}

	inline static int32_t get_offset_of_velocity_17() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___velocity_17)); }
	inline Vector3_t3722313464  get_velocity_17() const { return ___velocity_17; }
	inline Vector3_t3722313464 * get_address_of_velocity_17() { return &___velocity_17; }
	inline void set_velocity_17(Vector3_t3722313464  value)
	{
		___velocity_17 = value;
	}

	inline static int32_t get_offset_of_canJump_18() { return static_cast<int32_t>(offsetof(FirstPersonControl_t610459381, ___canJump_18)); }
	inline bool get_canJump_18() const { return ___canJump_18; }
	inline bool* get_address_of_canJump_18() { return &___canJump_18; }
	inline void set_canJump_18(bool value)
	{
		___canJump_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIRSTPERSONCONTROL_T610459381_H
#ifndef OBLIQUENEAR_T1763198098_H
#define OBLIQUENEAR_T1763198098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObliqueNear
struct  ObliqueNear_t1763198098  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform ObliqueNear::plane
	Transform_t3600365921 * ___plane_2;

public:
	inline static int32_t get_offset_of_plane_2() { return static_cast<int32_t>(offsetof(ObliqueNear_t1763198098, ___plane_2)); }
	inline Transform_t3600365921 * get_plane_2() const { return ___plane_2; }
	inline Transform_t3600365921 ** get_address_of_plane_2() { return &___plane_2; }
	inline void set_plane_2(Transform_t3600365921 * value)
	{
		___plane_2 = value;
		Il2CppCodeGenWriteBarrier((&___plane_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBLIQUENEAR_T1763198098_H
#ifndef ROTATIONCONSTRAINT_T2833302650_H
#define ROTATIONCONSTRAINT_T2833302650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotationConstraint
struct  RotationConstraint_t2833302650  : public MonoBehaviour_t3962482529
{
public:
	// ConstraintAxis RotationConstraint::axis
	int32_t ___axis_2;
	// System.Single RotationConstraint::min
	float ___min_3;
	// System.Single RotationConstraint::max
	float ___max_4;
	// UnityEngine.Transform RotationConstraint::thisTransform
	Transform_t3600365921 * ___thisTransform_5;
	// UnityEngine.Vector3 RotationConstraint::rotateAround
	Vector3_t3722313464  ___rotateAround_6;
	// UnityEngine.Quaternion RotationConstraint::minQuaternion
	Quaternion_t2301928331  ___minQuaternion_7;
	// UnityEngine.Quaternion RotationConstraint::maxQuaternion
	Quaternion_t2301928331  ___maxQuaternion_8;
	// System.Single RotationConstraint::range
	float ___range_9;

public:
	inline static int32_t get_offset_of_axis_2() { return static_cast<int32_t>(offsetof(RotationConstraint_t2833302650, ___axis_2)); }
	inline int32_t get_axis_2() const { return ___axis_2; }
	inline int32_t* get_address_of_axis_2() { return &___axis_2; }
	inline void set_axis_2(int32_t value)
	{
		___axis_2 = value;
	}

	inline static int32_t get_offset_of_min_3() { return static_cast<int32_t>(offsetof(RotationConstraint_t2833302650, ___min_3)); }
	inline float get_min_3() const { return ___min_3; }
	inline float* get_address_of_min_3() { return &___min_3; }
	inline void set_min_3(float value)
	{
		___min_3 = value;
	}

	inline static int32_t get_offset_of_max_4() { return static_cast<int32_t>(offsetof(RotationConstraint_t2833302650, ___max_4)); }
	inline float get_max_4() const { return ___max_4; }
	inline float* get_address_of_max_4() { return &___max_4; }
	inline void set_max_4(float value)
	{
		___max_4 = value;
	}

	inline static int32_t get_offset_of_thisTransform_5() { return static_cast<int32_t>(offsetof(RotationConstraint_t2833302650, ___thisTransform_5)); }
	inline Transform_t3600365921 * get_thisTransform_5() const { return ___thisTransform_5; }
	inline Transform_t3600365921 ** get_address_of_thisTransform_5() { return &___thisTransform_5; }
	inline void set_thisTransform_5(Transform_t3600365921 * value)
	{
		___thisTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___thisTransform_5), value);
	}

	inline static int32_t get_offset_of_rotateAround_6() { return static_cast<int32_t>(offsetof(RotationConstraint_t2833302650, ___rotateAround_6)); }
	inline Vector3_t3722313464  get_rotateAround_6() const { return ___rotateAround_6; }
	inline Vector3_t3722313464 * get_address_of_rotateAround_6() { return &___rotateAround_6; }
	inline void set_rotateAround_6(Vector3_t3722313464  value)
	{
		___rotateAround_6 = value;
	}

	inline static int32_t get_offset_of_minQuaternion_7() { return static_cast<int32_t>(offsetof(RotationConstraint_t2833302650, ___minQuaternion_7)); }
	inline Quaternion_t2301928331  get_minQuaternion_7() const { return ___minQuaternion_7; }
	inline Quaternion_t2301928331 * get_address_of_minQuaternion_7() { return &___minQuaternion_7; }
	inline void set_minQuaternion_7(Quaternion_t2301928331  value)
	{
		___minQuaternion_7 = value;
	}

	inline static int32_t get_offset_of_maxQuaternion_8() { return static_cast<int32_t>(offsetof(RotationConstraint_t2833302650, ___maxQuaternion_8)); }
	inline Quaternion_t2301928331  get_maxQuaternion_8() const { return ___maxQuaternion_8; }
	inline Quaternion_t2301928331 * get_address_of_maxQuaternion_8() { return &___maxQuaternion_8; }
	inline void set_maxQuaternion_8(Quaternion_t2301928331  value)
	{
		___maxQuaternion_8 = value;
	}

	inline static int32_t get_offset_of_range_9() { return static_cast<int32_t>(offsetof(RotationConstraint_t2833302650, ___range_9)); }
	inline float get_range_9() const { return ___range_9; }
	inline float* get_address_of_range_9() { return &___range_9; }
	inline void set_range_9(float value)
	{
		___range_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONCONSTRAINT_T2833302650_H
#ifndef PLAYERRELATIVECONTROL_T1056195335_H
#define PLAYERRELATIVECONTROL_T1056195335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerRelativeControl
struct  PlayerRelativeControl_t1056195335  : public MonoBehaviour_t3962482529
{
public:
	// Joystick PlayerRelativeControl::moveJoystick
	Joystick_t9498292 * ___moveJoystick_2;
	// Joystick PlayerRelativeControl::rotateJoystick
	Joystick_t9498292 * ___rotateJoystick_3;
	// UnityEngine.Transform PlayerRelativeControl::cameraPivot
	Transform_t3600365921 * ___cameraPivot_4;
	// System.Single PlayerRelativeControl::forwardSpeed
	float ___forwardSpeed_5;
	// System.Single PlayerRelativeControl::backwardSpeed
	float ___backwardSpeed_6;
	// System.Single PlayerRelativeControl::sidestepSpeed
	float ___sidestepSpeed_7;
	// System.Single PlayerRelativeControl::jumpSpeed
	float ___jumpSpeed_8;
	// System.Single PlayerRelativeControl::inAirMultiplier
	float ___inAirMultiplier_9;
	// UnityEngine.Vector2 PlayerRelativeControl::rotationSpeed
	Vector2_t2156229523  ___rotationSpeed_10;
	// UnityEngine.Transform PlayerRelativeControl::thisTransform
	Transform_t3600365921 * ___thisTransform_11;
	// UnityEngine.CharacterController PlayerRelativeControl::character
	CharacterController_t1138636865 * ___character_12;
	// UnityEngine.Vector3 PlayerRelativeControl::cameraVelocity
	Vector3_t3722313464  ___cameraVelocity_13;
	// UnityEngine.Vector3 PlayerRelativeControl::velocity
	Vector3_t3722313464  ___velocity_14;

public:
	inline static int32_t get_offset_of_moveJoystick_2() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___moveJoystick_2)); }
	inline Joystick_t9498292 * get_moveJoystick_2() const { return ___moveJoystick_2; }
	inline Joystick_t9498292 ** get_address_of_moveJoystick_2() { return &___moveJoystick_2; }
	inline void set_moveJoystick_2(Joystick_t9498292 * value)
	{
		___moveJoystick_2 = value;
		Il2CppCodeGenWriteBarrier((&___moveJoystick_2), value);
	}

	inline static int32_t get_offset_of_rotateJoystick_3() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___rotateJoystick_3)); }
	inline Joystick_t9498292 * get_rotateJoystick_3() const { return ___rotateJoystick_3; }
	inline Joystick_t9498292 ** get_address_of_rotateJoystick_3() { return &___rotateJoystick_3; }
	inline void set_rotateJoystick_3(Joystick_t9498292 * value)
	{
		___rotateJoystick_3 = value;
		Il2CppCodeGenWriteBarrier((&___rotateJoystick_3), value);
	}

	inline static int32_t get_offset_of_cameraPivot_4() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___cameraPivot_4)); }
	inline Transform_t3600365921 * get_cameraPivot_4() const { return ___cameraPivot_4; }
	inline Transform_t3600365921 ** get_address_of_cameraPivot_4() { return &___cameraPivot_4; }
	inline void set_cameraPivot_4(Transform_t3600365921 * value)
	{
		___cameraPivot_4 = value;
		Il2CppCodeGenWriteBarrier((&___cameraPivot_4), value);
	}

	inline static int32_t get_offset_of_forwardSpeed_5() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___forwardSpeed_5)); }
	inline float get_forwardSpeed_5() const { return ___forwardSpeed_5; }
	inline float* get_address_of_forwardSpeed_5() { return &___forwardSpeed_5; }
	inline void set_forwardSpeed_5(float value)
	{
		___forwardSpeed_5 = value;
	}

	inline static int32_t get_offset_of_backwardSpeed_6() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___backwardSpeed_6)); }
	inline float get_backwardSpeed_6() const { return ___backwardSpeed_6; }
	inline float* get_address_of_backwardSpeed_6() { return &___backwardSpeed_6; }
	inline void set_backwardSpeed_6(float value)
	{
		___backwardSpeed_6 = value;
	}

	inline static int32_t get_offset_of_sidestepSpeed_7() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___sidestepSpeed_7)); }
	inline float get_sidestepSpeed_7() const { return ___sidestepSpeed_7; }
	inline float* get_address_of_sidestepSpeed_7() { return &___sidestepSpeed_7; }
	inline void set_sidestepSpeed_7(float value)
	{
		___sidestepSpeed_7 = value;
	}

	inline static int32_t get_offset_of_jumpSpeed_8() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___jumpSpeed_8)); }
	inline float get_jumpSpeed_8() const { return ___jumpSpeed_8; }
	inline float* get_address_of_jumpSpeed_8() { return &___jumpSpeed_8; }
	inline void set_jumpSpeed_8(float value)
	{
		___jumpSpeed_8 = value;
	}

	inline static int32_t get_offset_of_inAirMultiplier_9() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___inAirMultiplier_9)); }
	inline float get_inAirMultiplier_9() const { return ___inAirMultiplier_9; }
	inline float* get_address_of_inAirMultiplier_9() { return &___inAirMultiplier_9; }
	inline void set_inAirMultiplier_9(float value)
	{
		___inAirMultiplier_9 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_10() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___rotationSpeed_10)); }
	inline Vector2_t2156229523  get_rotationSpeed_10() const { return ___rotationSpeed_10; }
	inline Vector2_t2156229523 * get_address_of_rotationSpeed_10() { return &___rotationSpeed_10; }
	inline void set_rotationSpeed_10(Vector2_t2156229523  value)
	{
		___rotationSpeed_10 = value;
	}

	inline static int32_t get_offset_of_thisTransform_11() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___thisTransform_11)); }
	inline Transform_t3600365921 * get_thisTransform_11() const { return ___thisTransform_11; }
	inline Transform_t3600365921 ** get_address_of_thisTransform_11() { return &___thisTransform_11; }
	inline void set_thisTransform_11(Transform_t3600365921 * value)
	{
		___thisTransform_11 = value;
		Il2CppCodeGenWriteBarrier((&___thisTransform_11), value);
	}

	inline static int32_t get_offset_of_character_12() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___character_12)); }
	inline CharacterController_t1138636865 * get_character_12() const { return ___character_12; }
	inline CharacterController_t1138636865 ** get_address_of_character_12() { return &___character_12; }
	inline void set_character_12(CharacterController_t1138636865 * value)
	{
		___character_12 = value;
		Il2CppCodeGenWriteBarrier((&___character_12), value);
	}

	inline static int32_t get_offset_of_cameraVelocity_13() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___cameraVelocity_13)); }
	inline Vector3_t3722313464  get_cameraVelocity_13() const { return ___cameraVelocity_13; }
	inline Vector3_t3722313464 * get_address_of_cameraVelocity_13() { return &___cameraVelocity_13; }
	inline void set_cameraVelocity_13(Vector3_t3722313464  value)
	{
		___cameraVelocity_13 = value;
	}

	inline static int32_t get_offset_of_velocity_14() { return static_cast<int32_t>(offsetof(PlayerRelativeControl_t1056195335, ___velocity_14)); }
	inline Vector3_t3722313464  get_velocity_14() const { return ___velocity_14; }
	inline Vector3_t3722313464 * get_address_of_velocity_14() { return &___velocity_14; }
	inline void set_velocity_14(Vector3_t3722313464  value)
	{
		___velocity_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERRELATIVECONTROL_T1056195335_H
#ifndef ROLLABALL_T267000325_H
#define ROLLABALL_T267000325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RollABall
struct  RollABall_t267000325  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 RollABall::tilt
	Vector3_t3722313464  ___tilt_2;
	// System.Single RollABall::speed
	float ___speed_3;
	// System.Single RollABall::circ
	float ___circ_4;
	// UnityEngine.Vector3 RollABall::previousPosition
	Vector3_t3722313464  ___previousPosition_5;

public:
	inline static int32_t get_offset_of_tilt_2() { return static_cast<int32_t>(offsetof(RollABall_t267000325, ___tilt_2)); }
	inline Vector3_t3722313464  get_tilt_2() const { return ___tilt_2; }
	inline Vector3_t3722313464 * get_address_of_tilt_2() { return &___tilt_2; }
	inline void set_tilt_2(Vector3_t3722313464  value)
	{
		___tilt_2 = value;
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(RollABall_t267000325, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_circ_4() { return static_cast<int32_t>(offsetof(RollABall_t267000325, ___circ_4)); }
	inline float get_circ_4() const { return ___circ_4; }
	inline float* get_address_of_circ_4() { return &___circ_4; }
	inline void set_circ_4(float value)
	{
		___circ_4 = value;
	}

	inline static int32_t get_offset_of_previousPosition_5() { return static_cast<int32_t>(offsetof(RollABall_t267000325, ___previousPosition_5)); }
	inline Vector3_t3722313464  get_previousPosition_5() const { return ___previousPosition_5; }
	inline Vector3_t3722313464 * get_address_of_previousPosition_5() { return &___previousPosition_5; }
	inline void set_previousPosition_5(Vector3_t3722313464  value)
	{
		___previousPosition_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROLLABALL_T267000325_H
#ifndef SIDESCROLLCONTROL_T463195584_H
#define SIDESCROLLCONTROL_T463195584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SidescrollControl
struct  SidescrollControl_t463195584  : public MonoBehaviour_t3962482529
{
public:
	// Joystick SidescrollControl::moveTouchPad
	Joystick_t9498292 * ___moveTouchPad_2;
	// Joystick SidescrollControl::jumpTouchPad
	Joystick_t9498292 * ___jumpTouchPad_3;
	// System.Single SidescrollControl::forwardSpeed
	float ___forwardSpeed_4;
	// System.Single SidescrollControl::backwardSpeed
	float ___backwardSpeed_5;
	// System.Single SidescrollControl::jumpSpeed
	float ___jumpSpeed_6;
	// System.Single SidescrollControl::inAirMultiplier
	float ___inAirMultiplier_7;
	// UnityEngine.Transform SidescrollControl::thisTransform
	Transform_t3600365921 * ___thisTransform_8;
	// UnityEngine.CharacterController SidescrollControl::character
	CharacterController_t1138636865 * ___character_9;
	// UnityEngine.Vector3 SidescrollControl::velocity
	Vector3_t3722313464  ___velocity_10;
	// System.Boolean SidescrollControl::canJump
	bool ___canJump_11;

public:
	inline static int32_t get_offset_of_moveTouchPad_2() { return static_cast<int32_t>(offsetof(SidescrollControl_t463195584, ___moveTouchPad_2)); }
	inline Joystick_t9498292 * get_moveTouchPad_2() const { return ___moveTouchPad_2; }
	inline Joystick_t9498292 ** get_address_of_moveTouchPad_2() { return &___moveTouchPad_2; }
	inline void set_moveTouchPad_2(Joystick_t9498292 * value)
	{
		___moveTouchPad_2 = value;
		Il2CppCodeGenWriteBarrier((&___moveTouchPad_2), value);
	}

	inline static int32_t get_offset_of_jumpTouchPad_3() { return static_cast<int32_t>(offsetof(SidescrollControl_t463195584, ___jumpTouchPad_3)); }
	inline Joystick_t9498292 * get_jumpTouchPad_3() const { return ___jumpTouchPad_3; }
	inline Joystick_t9498292 ** get_address_of_jumpTouchPad_3() { return &___jumpTouchPad_3; }
	inline void set_jumpTouchPad_3(Joystick_t9498292 * value)
	{
		___jumpTouchPad_3 = value;
		Il2CppCodeGenWriteBarrier((&___jumpTouchPad_3), value);
	}

	inline static int32_t get_offset_of_forwardSpeed_4() { return static_cast<int32_t>(offsetof(SidescrollControl_t463195584, ___forwardSpeed_4)); }
	inline float get_forwardSpeed_4() const { return ___forwardSpeed_4; }
	inline float* get_address_of_forwardSpeed_4() { return &___forwardSpeed_4; }
	inline void set_forwardSpeed_4(float value)
	{
		___forwardSpeed_4 = value;
	}

	inline static int32_t get_offset_of_backwardSpeed_5() { return static_cast<int32_t>(offsetof(SidescrollControl_t463195584, ___backwardSpeed_5)); }
	inline float get_backwardSpeed_5() const { return ___backwardSpeed_5; }
	inline float* get_address_of_backwardSpeed_5() { return &___backwardSpeed_5; }
	inline void set_backwardSpeed_5(float value)
	{
		___backwardSpeed_5 = value;
	}

	inline static int32_t get_offset_of_jumpSpeed_6() { return static_cast<int32_t>(offsetof(SidescrollControl_t463195584, ___jumpSpeed_6)); }
	inline float get_jumpSpeed_6() const { return ___jumpSpeed_6; }
	inline float* get_address_of_jumpSpeed_6() { return &___jumpSpeed_6; }
	inline void set_jumpSpeed_6(float value)
	{
		___jumpSpeed_6 = value;
	}

	inline static int32_t get_offset_of_inAirMultiplier_7() { return static_cast<int32_t>(offsetof(SidescrollControl_t463195584, ___inAirMultiplier_7)); }
	inline float get_inAirMultiplier_7() const { return ___inAirMultiplier_7; }
	inline float* get_address_of_inAirMultiplier_7() { return &___inAirMultiplier_7; }
	inline void set_inAirMultiplier_7(float value)
	{
		___inAirMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_thisTransform_8() { return static_cast<int32_t>(offsetof(SidescrollControl_t463195584, ___thisTransform_8)); }
	inline Transform_t3600365921 * get_thisTransform_8() const { return ___thisTransform_8; }
	inline Transform_t3600365921 ** get_address_of_thisTransform_8() { return &___thisTransform_8; }
	inline void set_thisTransform_8(Transform_t3600365921 * value)
	{
		___thisTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&___thisTransform_8), value);
	}

	inline static int32_t get_offset_of_character_9() { return static_cast<int32_t>(offsetof(SidescrollControl_t463195584, ___character_9)); }
	inline CharacterController_t1138636865 * get_character_9() const { return ___character_9; }
	inline CharacterController_t1138636865 ** get_address_of_character_9() { return &___character_9; }
	inline void set_character_9(CharacterController_t1138636865 * value)
	{
		___character_9 = value;
		Il2CppCodeGenWriteBarrier((&___character_9), value);
	}

	inline static int32_t get_offset_of_velocity_10() { return static_cast<int32_t>(offsetof(SidescrollControl_t463195584, ___velocity_10)); }
	inline Vector3_t3722313464  get_velocity_10() const { return ___velocity_10; }
	inline Vector3_t3722313464 * get_address_of_velocity_10() { return &___velocity_10; }
	inline void set_velocity_10(Vector3_t3722313464  value)
	{
		___velocity_10 = value;
	}

	inline static int32_t get_offset_of_canJump_11() { return static_cast<int32_t>(offsetof(SidescrollControl_t463195584, ___canJump_11)); }
	inline bool get_canJump_11() const { return ___canJump_11; }
	inline bool* get_address_of_canJump_11() { return &___canJump_11; }
	inline void set_canJump_11(bool value)
	{
		___canJump_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIDESCROLLCONTROL_T463195584_H
#ifndef ZOOMCAMERA_T1350885688_H
#define ZOOMCAMERA_T1350885688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZoomCamera
struct  ZoomCamera_t1350885688  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform ZoomCamera::origin
	Transform_t3600365921 * ___origin_2;
	// System.Single ZoomCamera::zoom
	float ___zoom_3;
	// System.Single ZoomCamera::zoomMin
	float ___zoomMin_4;
	// System.Single ZoomCamera::zoomMax
	float ___zoomMax_5;
	// System.Single ZoomCamera::seekTime
	float ___seekTime_6;
	// System.Boolean ZoomCamera::smoothZoomIn
	bool ___smoothZoomIn_7;
	// UnityEngine.Vector3 ZoomCamera::defaultLocalPosition
	Vector3_t3722313464  ___defaultLocalPosition_8;
	// UnityEngine.Transform ZoomCamera::thisTransform
	Transform_t3600365921 * ___thisTransform_9;
	// System.Single ZoomCamera::currentZoom
	float ___currentZoom_10;
	// System.Single ZoomCamera::targetZoom
	float ___targetZoom_11;
	// System.Single ZoomCamera::zoomVelocity
	float ___zoomVelocity_12;

public:
	inline static int32_t get_offset_of_origin_2() { return static_cast<int32_t>(offsetof(ZoomCamera_t1350885688, ___origin_2)); }
	inline Transform_t3600365921 * get_origin_2() const { return ___origin_2; }
	inline Transform_t3600365921 ** get_address_of_origin_2() { return &___origin_2; }
	inline void set_origin_2(Transform_t3600365921 * value)
	{
		___origin_2 = value;
		Il2CppCodeGenWriteBarrier((&___origin_2), value);
	}

	inline static int32_t get_offset_of_zoom_3() { return static_cast<int32_t>(offsetof(ZoomCamera_t1350885688, ___zoom_3)); }
	inline float get_zoom_3() const { return ___zoom_3; }
	inline float* get_address_of_zoom_3() { return &___zoom_3; }
	inline void set_zoom_3(float value)
	{
		___zoom_3 = value;
	}

	inline static int32_t get_offset_of_zoomMin_4() { return static_cast<int32_t>(offsetof(ZoomCamera_t1350885688, ___zoomMin_4)); }
	inline float get_zoomMin_4() const { return ___zoomMin_4; }
	inline float* get_address_of_zoomMin_4() { return &___zoomMin_4; }
	inline void set_zoomMin_4(float value)
	{
		___zoomMin_4 = value;
	}

	inline static int32_t get_offset_of_zoomMax_5() { return static_cast<int32_t>(offsetof(ZoomCamera_t1350885688, ___zoomMax_5)); }
	inline float get_zoomMax_5() const { return ___zoomMax_5; }
	inline float* get_address_of_zoomMax_5() { return &___zoomMax_5; }
	inline void set_zoomMax_5(float value)
	{
		___zoomMax_5 = value;
	}

	inline static int32_t get_offset_of_seekTime_6() { return static_cast<int32_t>(offsetof(ZoomCamera_t1350885688, ___seekTime_6)); }
	inline float get_seekTime_6() const { return ___seekTime_6; }
	inline float* get_address_of_seekTime_6() { return &___seekTime_6; }
	inline void set_seekTime_6(float value)
	{
		___seekTime_6 = value;
	}

	inline static int32_t get_offset_of_smoothZoomIn_7() { return static_cast<int32_t>(offsetof(ZoomCamera_t1350885688, ___smoothZoomIn_7)); }
	inline bool get_smoothZoomIn_7() const { return ___smoothZoomIn_7; }
	inline bool* get_address_of_smoothZoomIn_7() { return &___smoothZoomIn_7; }
	inline void set_smoothZoomIn_7(bool value)
	{
		___smoothZoomIn_7 = value;
	}

	inline static int32_t get_offset_of_defaultLocalPosition_8() { return static_cast<int32_t>(offsetof(ZoomCamera_t1350885688, ___defaultLocalPosition_8)); }
	inline Vector3_t3722313464  get_defaultLocalPosition_8() const { return ___defaultLocalPosition_8; }
	inline Vector3_t3722313464 * get_address_of_defaultLocalPosition_8() { return &___defaultLocalPosition_8; }
	inline void set_defaultLocalPosition_8(Vector3_t3722313464  value)
	{
		___defaultLocalPosition_8 = value;
	}

	inline static int32_t get_offset_of_thisTransform_9() { return static_cast<int32_t>(offsetof(ZoomCamera_t1350885688, ___thisTransform_9)); }
	inline Transform_t3600365921 * get_thisTransform_9() const { return ___thisTransform_9; }
	inline Transform_t3600365921 ** get_address_of_thisTransform_9() { return &___thisTransform_9; }
	inline void set_thisTransform_9(Transform_t3600365921 * value)
	{
		___thisTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___thisTransform_9), value);
	}

	inline static int32_t get_offset_of_currentZoom_10() { return static_cast<int32_t>(offsetof(ZoomCamera_t1350885688, ___currentZoom_10)); }
	inline float get_currentZoom_10() const { return ___currentZoom_10; }
	inline float* get_address_of_currentZoom_10() { return &___currentZoom_10; }
	inline void set_currentZoom_10(float value)
	{
		___currentZoom_10 = value;
	}

	inline static int32_t get_offset_of_targetZoom_11() { return static_cast<int32_t>(offsetof(ZoomCamera_t1350885688, ___targetZoom_11)); }
	inline float get_targetZoom_11() const { return ___targetZoom_11; }
	inline float* get_address_of_targetZoom_11() { return &___targetZoom_11; }
	inline void set_targetZoom_11(float value)
	{
		___targetZoom_11 = value;
	}

	inline static int32_t get_offset_of_zoomVelocity_12() { return static_cast<int32_t>(offsetof(ZoomCamera_t1350885688, ___zoomVelocity_12)); }
	inline float get_zoomVelocity_12() const { return ___zoomVelocity_12; }
	inline float* get_address_of_zoomVelocity_12() { return &___zoomVelocity_12; }
	inline void set_zoomVelocity_12(float value)
	{
		___zoomVelocity_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOOMCAMERA_T1350885688_H
#ifndef TAPCONTROL_T522795363_H
#define TAPCONTROL_T522795363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// tapcontrol
struct  tapcontrol_t522795363  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject tapcontrol::cameraObject
	GameObject_t1113636619 * ___cameraObject_2;
	// UnityEngine.Transform tapcontrol::cameraPivot
	Transform_t3600365921 * ___cameraPivot_3;
	// UnityEngine.GUITexture tapcontrol::jumpButton
	GUITexture_t951903601 * ___jumpButton_4;
	// System.Single tapcontrol::speed
	float ___speed_5;
	// System.Single tapcontrol::jumpSpeed
	float ___jumpSpeed_6;
	// System.Single tapcontrol::inAirMultiplier
	float ___inAirMultiplier_7;
	// System.Single tapcontrol::minimumDistanceToMove
	float ___minimumDistanceToMove_8;
	// System.Single tapcontrol::minimumTimeUntilMove
	float ___minimumTimeUntilMove_9;
	// System.Boolean tapcontrol::zoomEnabled
	bool ___zoomEnabled_10;
	// System.Single tapcontrol::zoomEpsilon
	float ___zoomEpsilon_11;
	// System.Single tapcontrol::zoomRate
	float ___zoomRate_12;
	// System.Boolean tapcontrol::rotateEnabled
	bool ___rotateEnabled_13;
	// System.Single tapcontrol::rotateEpsilon
	float ___rotateEpsilon_14;
	// ZoomCamera tapcontrol::zoomCamera
	ZoomCamera_t1350885688 * ___zoomCamera_15;
	// UnityEngine.Camera tapcontrol::cam
	Camera_t4157153871 * ___cam_16;
	// UnityEngine.Transform tapcontrol::thisTransform
	Transform_t3600365921 * ___thisTransform_17;
	// UnityEngine.CharacterController tapcontrol::character
	CharacterController_t1138636865 * ___character_18;
	// UnityEngine.Vector3 tapcontrol::targetLocation
	Vector3_t3722313464  ___targetLocation_19;
	// System.Boolean tapcontrol::moving
	bool ___moving_20;
	// System.Single tapcontrol::rotationTarget
	float ___rotationTarget_21;
	// System.Single tapcontrol::rotationVelocity
	float ___rotationVelocity_22;
	// UnityEngine.Vector3 tapcontrol::velocity
	Vector3_t3722313464  ___velocity_23;
	// ControlState tapcontrol::state
	int32_t ___state_24;
	// System.Int32[] tapcontrol::fingerDown
	Int32U5BU5D_t385246372* ___fingerDown_25;
	// UnityEngine.Vector2[] tapcontrol::fingerDownPosition
	Vector2U5BU5D_t1457185986* ___fingerDownPosition_26;
	// System.Int32[] tapcontrol::fingerDownFrame
	Int32U5BU5D_t385246372* ___fingerDownFrame_27;
	// System.Single tapcontrol::firstTouchTime
	float ___firstTouchTime_28;

public:
	inline static int32_t get_offset_of_cameraObject_2() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___cameraObject_2)); }
	inline GameObject_t1113636619 * get_cameraObject_2() const { return ___cameraObject_2; }
	inline GameObject_t1113636619 ** get_address_of_cameraObject_2() { return &___cameraObject_2; }
	inline void set_cameraObject_2(GameObject_t1113636619 * value)
	{
		___cameraObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___cameraObject_2), value);
	}

	inline static int32_t get_offset_of_cameraPivot_3() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___cameraPivot_3)); }
	inline Transform_t3600365921 * get_cameraPivot_3() const { return ___cameraPivot_3; }
	inline Transform_t3600365921 ** get_address_of_cameraPivot_3() { return &___cameraPivot_3; }
	inline void set_cameraPivot_3(Transform_t3600365921 * value)
	{
		___cameraPivot_3 = value;
		Il2CppCodeGenWriteBarrier((&___cameraPivot_3), value);
	}

	inline static int32_t get_offset_of_jumpButton_4() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___jumpButton_4)); }
	inline GUITexture_t951903601 * get_jumpButton_4() const { return ___jumpButton_4; }
	inline GUITexture_t951903601 ** get_address_of_jumpButton_4() { return &___jumpButton_4; }
	inline void set_jumpButton_4(GUITexture_t951903601 * value)
	{
		___jumpButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___jumpButton_4), value);
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_jumpSpeed_6() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___jumpSpeed_6)); }
	inline float get_jumpSpeed_6() const { return ___jumpSpeed_6; }
	inline float* get_address_of_jumpSpeed_6() { return &___jumpSpeed_6; }
	inline void set_jumpSpeed_6(float value)
	{
		___jumpSpeed_6 = value;
	}

	inline static int32_t get_offset_of_inAirMultiplier_7() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___inAirMultiplier_7)); }
	inline float get_inAirMultiplier_7() const { return ___inAirMultiplier_7; }
	inline float* get_address_of_inAirMultiplier_7() { return &___inAirMultiplier_7; }
	inline void set_inAirMultiplier_7(float value)
	{
		___inAirMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_minimumDistanceToMove_8() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___minimumDistanceToMove_8)); }
	inline float get_minimumDistanceToMove_8() const { return ___minimumDistanceToMove_8; }
	inline float* get_address_of_minimumDistanceToMove_8() { return &___minimumDistanceToMove_8; }
	inline void set_minimumDistanceToMove_8(float value)
	{
		___minimumDistanceToMove_8 = value;
	}

	inline static int32_t get_offset_of_minimumTimeUntilMove_9() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___minimumTimeUntilMove_9)); }
	inline float get_minimumTimeUntilMove_9() const { return ___minimumTimeUntilMove_9; }
	inline float* get_address_of_minimumTimeUntilMove_9() { return &___minimumTimeUntilMove_9; }
	inline void set_minimumTimeUntilMove_9(float value)
	{
		___minimumTimeUntilMove_9 = value;
	}

	inline static int32_t get_offset_of_zoomEnabled_10() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___zoomEnabled_10)); }
	inline bool get_zoomEnabled_10() const { return ___zoomEnabled_10; }
	inline bool* get_address_of_zoomEnabled_10() { return &___zoomEnabled_10; }
	inline void set_zoomEnabled_10(bool value)
	{
		___zoomEnabled_10 = value;
	}

	inline static int32_t get_offset_of_zoomEpsilon_11() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___zoomEpsilon_11)); }
	inline float get_zoomEpsilon_11() const { return ___zoomEpsilon_11; }
	inline float* get_address_of_zoomEpsilon_11() { return &___zoomEpsilon_11; }
	inline void set_zoomEpsilon_11(float value)
	{
		___zoomEpsilon_11 = value;
	}

	inline static int32_t get_offset_of_zoomRate_12() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___zoomRate_12)); }
	inline float get_zoomRate_12() const { return ___zoomRate_12; }
	inline float* get_address_of_zoomRate_12() { return &___zoomRate_12; }
	inline void set_zoomRate_12(float value)
	{
		___zoomRate_12 = value;
	}

	inline static int32_t get_offset_of_rotateEnabled_13() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___rotateEnabled_13)); }
	inline bool get_rotateEnabled_13() const { return ___rotateEnabled_13; }
	inline bool* get_address_of_rotateEnabled_13() { return &___rotateEnabled_13; }
	inline void set_rotateEnabled_13(bool value)
	{
		___rotateEnabled_13 = value;
	}

	inline static int32_t get_offset_of_rotateEpsilon_14() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___rotateEpsilon_14)); }
	inline float get_rotateEpsilon_14() const { return ___rotateEpsilon_14; }
	inline float* get_address_of_rotateEpsilon_14() { return &___rotateEpsilon_14; }
	inline void set_rotateEpsilon_14(float value)
	{
		___rotateEpsilon_14 = value;
	}

	inline static int32_t get_offset_of_zoomCamera_15() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___zoomCamera_15)); }
	inline ZoomCamera_t1350885688 * get_zoomCamera_15() const { return ___zoomCamera_15; }
	inline ZoomCamera_t1350885688 ** get_address_of_zoomCamera_15() { return &___zoomCamera_15; }
	inline void set_zoomCamera_15(ZoomCamera_t1350885688 * value)
	{
		___zoomCamera_15 = value;
		Il2CppCodeGenWriteBarrier((&___zoomCamera_15), value);
	}

	inline static int32_t get_offset_of_cam_16() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___cam_16)); }
	inline Camera_t4157153871 * get_cam_16() const { return ___cam_16; }
	inline Camera_t4157153871 ** get_address_of_cam_16() { return &___cam_16; }
	inline void set_cam_16(Camera_t4157153871 * value)
	{
		___cam_16 = value;
		Il2CppCodeGenWriteBarrier((&___cam_16), value);
	}

	inline static int32_t get_offset_of_thisTransform_17() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___thisTransform_17)); }
	inline Transform_t3600365921 * get_thisTransform_17() const { return ___thisTransform_17; }
	inline Transform_t3600365921 ** get_address_of_thisTransform_17() { return &___thisTransform_17; }
	inline void set_thisTransform_17(Transform_t3600365921 * value)
	{
		___thisTransform_17 = value;
		Il2CppCodeGenWriteBarrier((&___thisTransform_17), value);
	}

	inline static int32_t get_offset_of_character_18() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___character_18)); }
	inline CharacterController_t1138636865 * get_character_18() const { return ___character_18; }
	inline CharacterController_t1138636865 ** get_address_of_character_18() { return &___character_18; }
	inline void set_character_18(CharacterController_t1138636865 * value)
	{
		___character_18 = value;
		Il2CppCodeGenWriteBarrier((&___character_18), value);
	}

	inline static int32_t get_offset_of_targetLocation_19() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___targetLocation_19)); }
	inline Vector3_t3722313464  get_targetLocation_19() const { return ___targetLocation_19; }
	inline Vector3_t3722313464 * get_address_of_targetLocation_19() { return &___targetLocation_19; }
	inline void set_targetLocation_19(Vector3_t3722313464  value)
	{
		___targetLocation_19 = value;
	}

	inline static int32_t get_offset_of_moving_20() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___moving_20)); }
	inline bool get_moving_20() const { return ___moving_20; }
	inline bool* get_address_of_moving_20() { return &___moving_20; }
	inline void set_moving_20(bool value)
	{
		___moving_20 = value;
	}

	inline static int32_t get_offset_of_rotationTarget_21() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___rotationTarget_21)); }
	inline float get_rotationTarget_21() const { return ___rotationTarget_21; }
	inline float* get_address_of_rotationTarget_21() { return &___rotationTarget_21; }
	inline void set_rotationTarget_21(float value)
	{
		___rotationTarget_21 = value;
	}

	inline static int32_t get_offset_of_rotationVelocity_22() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___rotationVelocity_22)); }
	inline float get_rotationVelocity_22() const { return ___rotationVelocity_22; }
	inline float* get_address_of_rotationVelocity_22() { return &___rotationVelocity_22; }
	inline void set_rotationVelocity_22(float value)
	{
		___rotationVelocity_22 = value;
	}

	inline static int32_t get_offset_of_velocity_23() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___velocity_23)); }
	inline Vector3_t3722313464  get_velocity_23() const { return ___velocity_23; }
	inline Vector3_t3722313464 * get_address_of_velocity_23() { return &___velocity_23; }
	inline void set_velocity_23(Vector3_t3722313464  value)
	{
		___velocity_23 = value;
	}

	inline static int32_t get_offset_of_state_24() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___state_24)); }
	inline int32_t get_state_24() const { return ___state_24; }
	inline int32_t* get_address_of_state_24() { return &___state_24; }
	inline void set_state_24(int32_t value)
	{
		___state_24 = value;
	}

	inline static int32_t get_offset_of_fingerDown_25() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___fingerDown_25)); }
	inline Int32U5BU5D_t385246372* get_fingerDown_25() const { return ___fingerDown_25; }
	inline Int32U5BU5D_t385246372** get_address_of_fingerDown_25() { return &___fingerDown_25; }
	inline void set_fingerDown_25(Int32U5BU5D_t385246372* value)
	{
		___fingerDown_25 = value;
		Il2CppCodeGenWriteBarrier((&___fingerDown_25), value);
	}

	inline static int32_t get_offset_of_fingerDownPosition_26() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___fingerDownPosition_26)); }
	inline Vector2U5BU5D_t1457185986* get_fingerDownPosition_26() const { return ___fingerDownPosition_26; }
	inline Vector2U5BU5D_t1457185986** get_address_of_fingerDownPosition_26() { return &___fingerDownPosition_26; }
	inline void set_fingerDownPosition_26(Vector2U5BU5D_t1457185986* value)
	{
		___fingerDownPosition_26 = value;
		Il2CppCodeGenWriteBarrier((&___fingerDownPosition_26), value);
	}

	inline static int32_t get_offset_of_fingerDownFrame_27() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___fingerDownFrame_27)); }
	inline Int32U5BU5D_t385246372* get_fingerDownFrame_27() const { return ___fingerDownFrame_27; }
	inline Int32U5BU5D_t385246372** get_address_of_fingerDownFrame_27() { return &___fingerDownFrame_27; }
	inline void set_fingerDownFrame_27(Int32U5BU5D_t385246372* value)
	{
		___fingerDownFrame_27 = value;
		Il2CppCodeGenWriteBarrier((&___fingerDownFrame_27), value);
	}

	inline static int32_t get_offset_of_firstTouchTime_28() { return static_cast<int32_t>(offsetof(tapcontrol_t522795363, ___firstTouchTime_28)); }
	inline float get_firstTouchTime_28() const { return ___firstTouchTime_28; }
	inline float* get_address_of_firstTouchTime_28() { return &___firstTouchTime_28; }
	inline void set_firstTouchTime_28(float value)
	{
		___firstTouchTime_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPCONTROL_T522795363_H
#ifndef SMOOTHFOLLOW2D_T2134035804_H
#define SMOOTHFOLLOW2D_T2134035804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmoothFollow2D
struct  SmoothFollow2D_t2134035804  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform SmoothFollow2D::target
	Transform_t3600365921 * ___target_2;
	// System.Single SmoothFollow2D::smoothTime
	float ___smoothTime_3;
	// UnityEngine.Transform SmoothFollow2D::thisTransform
	Transform_t3600365921 * ___thisTransform_4;
	// UnityEngine.Vector2 SmoothFollow2D::velocity
	Vector2_t2156229523  ___velocity_5;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(SmoothFollow2D_t2134035804, ___target_2)); }
	inline Transform_t3600365921 * get_target_2() const { return ___target_2; }
	inline Transform_t3600365921 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t3600365921 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_smoothTime_3() { return static_cast<int32_t>(offsetof(SmoothFollow2D_t2134035804, ___smoothTime_3)); }
	inline float get_smoothTime_3() const { return ___smoothTime_3; }
	inline float* get_address_of_smoothTime_3() { return &___smoothTime_3; }
	inline void set_smoothTime_3(float value)
	{
		___smoothTime_3 = value;
	}

	inline static int32_t get_offset_of_thisTransform_4() { return static_cast<int32_t>(offsetof(SmoothFollow2D_t2134035804, ___thisTransform_4)); }
	inline Transform_t3600365921 * get_thisTransform_4() const { return ___thisTransform_4; }
	inline Transform_t3600365921 ** get_address_of_thisTransform_4() { return &___thisTransform_4; }
	inline void set_thisTransform_4(Transform_t3600365921 * value)
	{
		___thisTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___thisTransform_4), value);
	}

	inline static int32_t get_offset_of_velocity_5() { return static_cast<int32_t>(offsetof(SmoothFollow2D_t2134035804, ___velocity_5)); }
	inline Vector2_t2156229523  get_velocity_5() const { return ___velocity_5; }
	inline Vector2_t2156229523 * get_address_of_velocity_5() { return &___velocity_5; }
	inline void set_velocity_5(Vector2_t2156229523  value)
	{
		___velocity_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHFOLLOW2D_T2134035804_H
// Joystick[]
struct JoystickU5BU5D_t4275182589  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Joystick_t9498292 * m_Items[1];

public:
	inline Joystick_t9498292 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Joystick_t9498292 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Joystick_t9498292 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Joystick_t9498292 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Joystick_t9498292 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Joystick_t9498292 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Object[]
struct ObjectU5BU5D_t1417781964  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Object_t631007953 * m_Items[1];

public:
	inline Object_t631007953 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Object_t631007953 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Object_t631007953 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Object_t631007953 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Object_t631007953 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Object_t631007953 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t385246372  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector2_t2156229523  m_Items[1];

public:
	inline Vector2_t2156229523  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_t2156229523 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_t2156229523  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_t2156229523  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_t2156229523 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_t2156229523  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Touch[]
struct TouchU5BU5D_t1849554061  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Touch_t1921856868  m_Items[1];

public:
	inline Touch_t1921856868  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Touch_t1921856868 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Touch_t1921856868  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Touch_t1921856868  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Touch_t1921856868 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Touch_t1921856868  value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t2156229523  Vector2_get_zero_m540426400 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3970636864 (Vector2_t2156229523 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1620074514 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
extern "C"  Component_t1923634451 * Component_GetComponent_m886226392 (Component_t1923634451 * __this, Type_t * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C"  GameObject_t1113636619 * GameObject_Find_m2032535176 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m3574996620 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3600365921 * GameObject_get_transform_m1369836730 (GameObject_t1113636619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m3387557959 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.CharacterController::get_velocity()
extern "C"  Vector3_t3722313464  CharacterController_get_velocity_m3517335080 (CharacterController_t1138636865 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::get_magnitude()
extern "C"  float Vector3_get_magnitude_m27958459 (Vector3_t3722313464 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C"  Vector3_t3722313464  Vector3_get_normalized_m2454957984 (Vector3_t3722313464 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_forward(UnityEngine.Vector3)
extern "C"  void Transform_set_forward_m1840797198 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m20417929 (Behaviour_t1437897464 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m3353183577 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Transform_TransformDirection_m3784028109 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::Normalize()
extern "C"  void Vector3_Normalize_m914904454 (Vector3_t3722313464 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_op_Multiply_m3376773913 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.CharacterController::get_isGrounded()
extern "C"  bool CharacterController_get_isGrounded_m1151930607 (CharacterController_t1138636865 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Physics::get_gravity()
extern "C"  Vector3_t3722313464  Physics_get_gravity_m2660066594 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m372706562 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Addition_m779775034 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CollisionFlags UnityEngine.CharacterController::Move(UnityEngine.Vector3)
extern "C"  int32_t CharacterController_Move_m1547317252 (CharacterController_t1138636865 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t3722313464  Vector3_get_zero_m1409827619 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2156229523  Vector2_op_Multiply_m2347887432 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single,UnityEngine.Space)
extern "C"  void Transform_Rotate_m1660364534 (Transform_t3600365921 * __this, float p0, float p1, float p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single)
extern "C"  void Transform_Rotate_m3172098886 (Transform_t3600365921 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Input::get_acceleration()
extern "C"  Vector3_t3722313464  Input_get_acceleration_m2528400370 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern "C"  Vector3_t3722313464  Transform_get_forward_m747522392 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Boundary::.ctor()
extern "C"  void Boundary__ctor_m1524210624 (Boundary_t2442033035 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUITexture::get_pixelInset()
extern "C"  Rect_t2360479859  GUITexture_get_pixelInset_m2981475662 (GUITexture_t951903601 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_x()
extern "C"  float Rect_get_x_m3839990490 (Rect_t2360479859 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m345039817 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_x(System.Single)
extern "C"  void Rect_set_x_m2352063068 (Rect_t2360479859 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_y()
extern "C"  float Rect_get_y_m1501338330 (Rect_t2360479859 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m1623532518 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_y(System.Single)
extern "C"  void Rect_set_y_m3702432190 (Rect_t2360479859 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.GUITexture::get_texture()
extern "C"  Texture_t3661962703 * GUITexture_get_texture_m1784897130 (GUITexture_t951903601 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_width()
extern "C"  float Rect_get_width_m3421484486 (Rect_t2360479859 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m1358425599 (Rect_t2360479859 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m796801857 (GameObject_t1113636619 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::set_pixelInset(UnityEngine.Rect)
extern "C"  void GUITexture_set_pixelInset_m2556409115 (GUITexture_t951903601 * __this, Rect_t2360479859  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.GUITexture::get_color()
extern "C"  Color_t2555686324  GUITexture_get_color_m2101240858 (GUITexture_t951903601 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::set_color(UnityEngine.Color)
extern "C"  void GUITexture_set_color_m1660397747 (GUITexture_t951903601 * __this, Color_t2555686324  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t1417781964* Object_FindObjectsOfType_m2295101757 (RuntimeObject * __this /* static, unused */, Type_t * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Input::get_touchCount()
extern "C"  int32_t Input_get_touchCount_m3403849067 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C"  Touch_t1921856868  Input_GetTouch_m2192712756 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C"  Vector2_t2156229523  Touch_get_position_m3109777936 (Touch_t1921856868 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2156229523  Vector2_op_Subtraction_m73004381 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector2)
extern "C"  bool Rect_Contains_m3188543637 (Rect_t2360479859 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t3722313464  Vector2_op_Implicit_m1860157806 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUIElement::HitTest(UnityEngine.Vector3)
extern "C"  bool GUIElement_HitTest_m3916646804 (GUIElement_t3567083079 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Touch::get_fingerId()
extern "C"  int32_t Touch_get_fingerId_m859576425 (Touch_t1921856868 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_time()
extern "C"  float Time_get_time_m2907476221 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::get_Length()
extern "C"  int32_t Array_get_Length_m21610649 (RuntimeArray * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Touch::get_tapCount()
extern "C"  int32_t Touch_get_tapCount_m2125417096 (Touch_t1921856868 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m3350697880 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C"  int32_t Touch_get_phase_m214549210 (Touch_t1921856868 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Sign(System.Single)
extern "C"  float Mathf_Sign_m3457838305 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_inverse()
extern "C"  Matrix4x4_t1817901843  Matrix4x4_get_inverse_m1870592360 (Matrix4x4_t1817901843 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m2498754347 (Vector4_t3319028937 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern "C"  Vector4_t3319028937  Matrix4x4_op_Multiply_m1839501195 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  p0, Vector4_t3319028937  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::Dot(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  float Vector4_Dot_m3492158352 (RuntimeObject * __this /* static, unused */, Vector4_t3319028937  p0, Vector4_t3319028937  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Multiply(UnityEngine.Vector4,System.Single)
extern "C"  Vector4_t3319028937  Vector4_op_Multiply_m213790997 (RuntimeObject * __this /* static, unused */, Vector4_t3319028937  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32)
extern "C"  float Matrix4x4_get_Item_m567451091 (Matrix4x4_t1817901843 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Single)
extern "C"  void Matrix4x4_set_Item_m1906605342 (Matrix4x4_t1817901843 * __this, int32_t p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t4157153871_m1557787507(__this, method) ((  Camera_t4157153871 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_projectionMatrix()
extern "C"  Matrix4x4_t1817901843  Camera_get_projectionMatrix_m667780853 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_worldToCameraMatrix()
extern "C"  Matrix4x4_t1817901843  Camera_get_worldToCameraMatrix_m22661425 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Matrix4x4_MultiplyPoint_m1575665487 (Matrix4x4_t1817901843 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C"  Vector3_t3722313464  Vector3_get_up_m3584168373 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_UnaryNegation(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_UnaryNegation_m1951478815 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyVector(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Matrix4x4_MultiplyVector_m3808798942 (Matrix4x4_t1817901843 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector4_t3319028937  Vector4_op_Implicit_m2966035112 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Dot_m606404487 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_projectionMatrix(UnityEngine.Matrix4x4)
extern "C"  void Camera_set_projectionMatrix_m3293177686 (Camera_t4157153871 * __this, Matrix4x4_t1817901843  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C"  Vector3_t3722313464  Transform_get_localPosition_m4234289348 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::SmoothDamp(System.Single,System.Single,System.Single&,System.Single)
extern "C"  float Mathf_SmoothDamp_m3171073017 (RuntimeObject * __this /* static, unused */, float p0, float p1, float* p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C"  void Transform_set_localPosition_m4128471975 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider>()
#define Component_GetComponent_TisCollider_t1773347010_m4226749020(__this, method) ((  Collider_t1773347010 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// UnityEngine.Bounds UnityEngine.Collider::get_bounds()
extern "C"  Bounds_t2266837910  Collider_get_bounds_m2952418672 (Collider_t1773347010 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_extents()
extern "C"  Vector3_t3722313464  Bounds_get_extents_m1304537151 (Bounds_t2266837910 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t3916780224_m279685075(__this, method) ((  Rigidbody_t3916780224 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3)
extern "C"  void Rigidbody_AddForce_m3395934484 (Rigidbody_t3916780224 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Subtraction_m3073674971 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_op_Division_m510815599 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,UnityEngine.Space)
extern "C"  void Transform_Rotate_m1886816857 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
extern "C"  Vector3_t3722313464  Vector3_get_right_m1913784872 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t3722313464  Vector3_get_forward_m3100859705 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern "C"  Quaternion_t2301928331  Transform_get_localRotation_m3487911431 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern "C"  Vector3_t3722313464  Quaternion_get_eulerAngles_m3425202016 (Quaternion_t2301928331 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::get_Item(System.Int32)
extern "C"  float Vector3_get_Item_m668685504 (Vector3_t3722313464 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
extern "C"  Quaternion_t2301928331  Quaternion_AngleAxis_m1767165696 (RuntimeObject * __this /* static, unused */, float p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t2301928331  Quaternion_op_Multiply_m1294064023 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  p0, Quaternion_t2301928331  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Quaternion::Angle(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  float Quaternion_Angle_m1586774072 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  p0, Quaternion_t2301928331  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::set_Item(System.Int32,System.Single)
extern "C"  void Vector3_set_Item_m1772472431 (Vector3_t3722313464 * __this, int32_t p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localEulerAngles(UnityEngine.Vector3)
extern "C"  void Transform_set_localEulerAngles_m4202601546 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
extern "C"  Component_t1923634451 * GameObject_GetComponent_m1027872079 (GameObject_t1113636619 * __this, Type_t * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Camera>()
#define GameObject_GetComponent_TisCamera_t4157153871_m3956151066(__this, method) ((  Camera_t4157153871 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Single UnityEngine.Vector2::get_magnitude()
extern "C"  float Vector2_get_magnitude_m2752892833 (Vector2_t2156229523 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Division(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2156229523  Vector2_op_Division_m132623573 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_deltaPosition()
extern "C"  Vector2_t2156229523  Touch_get_deltaPosition_m2389653382 (Touch_t1921856868 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::Dot(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  float Vector2_Dot_m1554553447 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
extern "C"  void Vector3__ctor_m1719387948 (Vector3_t3722313464 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Cross(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_Cross_m418170344 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C"  Ray_t3785851493  Camera_ScreenPointToRay_m3764635188 (Camera_t4157153871 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&)
extern "C"  bool Physics_Raycast_m447436869 (RuntimeObject * __this /* static, unused */, Ray_t3785851493  p0, RaycastHit_t1056001966 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C"  Vector3_t3722313464  RaycastHit_get_point_m2236647085 (RaycastHit_t1056001966 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Touch[] UnityEngine.Input::get_touches()
extern "C"  TouchU5BU5D_t1849554061* Input_get_touches_m1702694043 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Time::get_frameCount()
extern "C"  int32_t Time_get_frameCount_m1220035214 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
extern "C"  Vector3_t3722313464  Transform_get_eulerAngles_m2743581774 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::SmoothDampAngle(System.Single,System.Single,System.Single&,System.Single)
extern "C"  float Mathf_SmoothDampAngle_m1801528689 (RuntimeObject * __this /* static, unused */, float p0, float p1, float* p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_eulerAngles(UnityEngine.Vector3)
extern "C"  void Transform_set_eulerAngles_m135219616 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t3600365921 * Transform_get_parent_m835071599 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformDirection(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Transform_InverseTransformDirection_m3843238577 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Transform_TransformPoint_m226827784 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Int32)
extern "C"  bool Physics_Linecast_m2155187179 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, RaycastHit_t1056001966 * p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Boundary::.ctor()
extern "C"  void Boundary__ctor_m1524210624 (Boundary_t2442033035 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Boundary__ctor_m1524210624_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_0 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_min_0(L_0);
		Vector2_t2156229523  L_1 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_max_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CameraRelativeControl::.ctor()
extern "C"  void CameraRelativeControl__ctor_m3651770273 (CameraRelativeControl_t2453217546 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		__this->set_speed_6((((float)((float)5))));
		__this->set_jumpSpeed_7((((float)((float)8))));
		__this->set_inAirMultiplier_8((0.25f));
		Vector2_t2156229523  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m3970636864((&L_0), (((float)((float)((int32_t)50)))), (((float)((float)((int32_t)25)))), /*hidden argument*/NULL);
		__this->set_rotationSpeed_9(L_0);
		__this->set_canJump_13((bool)1);
		return;
	}
}
// System.Void CameraRelativeControl::Start()
extern "C"  void CameraRelativeControl_Start_m3710056517 (CameraRelativeControl_t2453217546 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraRelativeControl_Start_m3710056517_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	{
		RuntimeTypeHandle_t3027515415  L_0 = { reinterpret_cast<intptr_t> (Transform_t3600365921_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Component_t1923634451 * L_2 = Component_GetComponent_m886226392(__this, L_1, /*hidden argument*/NULL);
		__this->set_thisTransform_10(((Transform_t3600365921 *)CastclassClass((RuntimeObject*)L_2, Transform_t3600365921_il2cpp_TypeInfo_var)));
		RuntimeTypeHandle_t3027515415  L_3 = { reinterpret_cast<intptr_t> (CharacterController_t1138636865_0_0_0_var) };
		Type_t * L_4 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Component_t1923634451 * L_5 = Component_GetComponent_m886226392(__this, L_4, /*hidden argument*/NULL);
		__this->set_character_11(((CharacterController_t1138636865 *)CastclassSealed((RuntimeObject*)L_5, CharacterController_t1138636865_il2cpp_TypeInfo_var)));
		GameObject_t1113636619 * L_6 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral4038994995, /*hidden argument*/NULL);
		V_0 = L_6;
		GameObject_t1113636619 * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0062;
		}
	}
	{
		Transform_t3600365921 * L_9 = __this->get_thisTransform_10();
		GameObject_t1113636619 * L_10 = V_0;
		Transform_t3600365921 * L_11 = GameObject_get_transform_m1369836730(L_10, /*hidden argument*/NULL);
		Vector3_t3722313464  L_12 = Transform_get_position_m36019626(L_11, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_9, L_12, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Void CameraRelativeControl::FaceMovementDirection()
extern "C"  void CameraRelativeControl_FaceMovementDirection_m2300084316 (CameraRelativeControl_t2453217546 * __this, const RuntimeMethod* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		CharacterController_t1138636865 * L_0 = __this->get_character_11();
		Vector3_t3722313464  L_1 = CharacterController_get_velocity_m3517335080(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		(&V_0)->set_y_2((((float)((float)0))));
		float L_2 = Vector3_get_magnitude_m27958459((&V_0), /*hidden argument*/NULL);
		if ((((float)L_2) <= ((float)(0.1f))))
		{
			goto IL_0038;
		}
	}
	{
		Transform_t3600365921 * L_3 = __this->get_thisTransform_10();
		Vector3_t3722313464  L_4 = Vector3_get_normalized_m2454957984((&V_0), /*hidden argument*/NULL);
		Transform_set_forward_m1840797198(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void CameraRelativeControl::OnEndGame()
extern "C"  void CameraRelativeControl_OnEndGame_m744629824 (CameraRelativeControl_t2453217546 * __this, const RuntimeMethod* method)
{
	{
		Joystick_t9498292 * L_0 = __this->get_moveJoystick_2();
		VirtActionInvoker0::Invoke(5 /* System.Void Joystick::Disable() */, L_0);
		Joystick_t9498292 * L_1 = __this->get_rotateJoystick_3();
		VirtActionInvoker0::Invoke(5 /* System.Void Joystick::Disable() */, L_1);
		Behaviour_set_enabled_m20417929(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraRelativeControl::Update()
extern "C"  void CameraRelativeControl_Update_m4161957553 (CameraRelativeControl_t2453217546 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CameraRelativeControl_Update_m4161957553_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2156229523  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float G_B2_0 = 0.0f;
	Vector3_t3722313464  G_B2_1;
	memset(&G_B2_1, 0, sizeof(G_B2_1));
	float G_B1_0 = 0.0f;
	Vector3_t3722313464  G_B1_1;
	memset(&G_B1_1, 0, sizeof(G_B1_1));
	float G_B3_0 = 0.0f;
	float G_B3_1 = 0.0f;
	Vector3_t3722313464  G_B3_2;
	memset(&G_B3_2, 0, sizeof(G_B3_2));
	{
		Transform_t3600365921 * L_0 = __this->get_cameraTransform_5();
		Joystick_t9498292 * L_1 = __this->get_moveJoystick_2();
		Vector2_t2156229523 * L_2 = L_1->get_address_of_position_9();
		float L_3 = L_2->get_x_0();
		Joystick_t9498292 * L_4 = __this->get_moveJoystick_2();
		Vector2_t2156229523 * L_5 = L_4->get_address_of_position_9();
		float L_6 = L_5->get_y_1();
		Vector3_t3722313464  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m3353183577((&L_7), L_3, (((float)((float)0))), L_6, /*hidden argument*/NULL);
		Vector3_t3722313464  L_8 = Transform_TransformDirection_m3784028109(L_0, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		(&V_0)->set_y_2((((float)((float)0))));
		Vector3_Normalize_m914904454((&V_0), /*hidden argument*/NULL);
		Joystick_t9498292 * L_9 = __this->get_moveJoystick_2();
		Vector2_t2156229523 * L_10 = L_9->get_address_of_position_9();
		float L_11 = L_10->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_12 = fabsf(L_11);
		Joystick_t9498292 * L_13 = __this->get_moveJoystick_2();
		Vector2_t2156229523 * L_14 = L_13->get_address_of_position_9();
		float L_15 = L_14->get_y_1();
		float L_16 = fabsf(L_15);
		Vector2_t2156229523  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector2__ctor_m3970636864((&L_17), L_12, L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Vector3_t3722313464  L_18 = V_0;
		float L_19 = __this->get_speed_6();
		float L_20 = (&V_1)->get_x_0();
		float L_21 = (&V_1)->get_y_1();
		G_B1_0 = L_19;
		G_B1_1 = L_18;
		if ((((float)L_20) <= ((float)L_21)))
		{
			G_B2_0 = L_19;
			G_B2_1 = L_18;
			goto IL_0099;
		}
	}
	{
		float L_22 = (&V_1)->get_x_0();
		G_B3_0 = L_22;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_00a0;
	}

IL_0099:
	{
		float L_23 = (&V_1)->get_y_1();
		G_B3_0 = L_23;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_00a0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_24 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, G_B3_2, ((float)il2cpp_codegen_multiply((float)G_B3_1, (float)G_B3_0)), /*hidden argument*/NULL);
		V_0 = L_24;
		CharacterController_t1138636865 * L_25 = __this->get_character_11();
		bool L_26 = CharacterController_get_isGrounded_m1151930607(L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_0118;
		}
	}
	{
		Joystick_t9498292 * L_27 = __this->get_rotateJoystick_3();
		bool L_28 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Joystick::IsFingerDown() */, L_27);
		if (L_28)
		{
			goto IL_00ce;
		}
	}
	{
		__this->set_canJump_13((bool)1);
	}

IL_00ce:
	{
		bool L_29 = __this->get_canJump_13();
		if (!L_29)
		{
			goto IL_0113;
		}
	}
	{
		Joystick_t9498292 * L_30 = __this->get_rotateJoystick_3();
		int32_t L_31 = L_30->get_tapCount_10();
		if ((!(((uint32_t)L_31) == ((uint32_t)2))))
		{
			goto IL_0113;
		}
	}
	{
		CharacterController_t1138636865 * L_32 = __this->get_character_11();
		Vector3_t3722313464  L_33 = CharacterController_get_velocity_m3517335080(L_32, /*hidden argument*/NULL);
		__this->set_velocity_12(L_33);
		Vector3_t3722313464 * L_34 = __this->get_address_of_velocity_12();
		float L_35 = __this->get_jumpSpeed_7();
		L_34->set_y_2(L_35);
		__this->set_canJump_13((bool)0);
	}

IL_0113:
	{
		goto IL_016c;
	}

IL_0118:
	{
		Vector3_t3722313464 * L_36 = __this->get_address_of_velocity_12();
		Vector3_t3722313464 * L_37 = __this->get_address_of_velocity_12();
		float L_38 = L_37->get_y_2();
		Vector3_t3722313464  L_39 = Physics_get_gravity_m2660066594(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_39;
		float L_40 = (&V_3)->get_y_2();
		float L_41 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_36->set_y_2(((float)il2cpp_codegen_add((float)L_38, (float)((float)il2cpp_codegen_multiply((float)L_40, (float)L_41)))));
		float L_42 = (&V_0)->get_x_1();
		float L_43 = __this->get_inAirMultiplier_8();
		(&V_0)->set_x_1(((float)il2cpp_codegen_multiply((float)L_42, (float)L_43)));
		float L_44 = (&V_0)->get_z_3();
		float L_45 = __this->get_inAirMultiplier_8();
		(&V_0)->set_z_3(((float)il2cpp_codegen_multiply((float)L_44, (float)L_45)));
	}

IL_016c:
	{
		Vector3_t3722313464  L_46 = V_0;
		Vector3_t3722313464  L_47 = __this->get_velocity_12();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_48 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_46, L_47, /*hidden argument*/NULL);
		V_0 = L_48;
		Vector3_t3722313464  L_49 = V_0;
		Vector3_t3722313464  L_50 = Physics_get_gravity_m2660066594(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_51 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_49, L_50, /*hidden argument*/NULL);
		V_0 = L_51;
		Vector3_t3722313464  L_52 = V_0;
		float L_53 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_54 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_52, L_53, /*hidden argument*/NULL);
		V_0 = L_54;
		CharacterController_t1138636865 * L_55 = __this->get_character_11();
		Vector3_t3722313464  L_56 = V_0;
		CharacterController_Move_m1547317252(L_55, L_56, /*hidden argument*/NULL);
		CharacterController_t1138636865 * L_57 = __this->get_character_11();
		bool L_58 = CharacterController_get_isGrounded_m1151930607(L_57, /*hidden argument*/NULL);
		if (!L_58)
		{
			goto IL_01b9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_59 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_velocity_12(L_59);
	}

IL_01b9:
	{
		VirtActionInvoker0::Invoke(5 /* System.Void CameraRelativeControl::FaceMovementDirection() */, __this);
		Joystick_t9498292 * L_60 = __this->get_rotateJoystick_3();
		Vector2_t2156229523  L_61 = L_60->get_position_9();
		V_2 = L_61;
		float L_62 = (&V_2)->get_x_0();
		Vector2_t2156229523 * L_63 = __this->get_address_of_rotationSpeed_9();
		float L_64 = L_63->get_x_0();
		(&V_2)->set_x_0(((float)il2cpp_codegen_multiply((float)L_62, (float)L_64)));
		float L_65 = (&V_2)->get_y_1();
		Vector2_t2156229523 * L_66 = __this->get_address_of_rotationSpeed_9();
		float L_67 = L_66->get_y_1();
		(&V_2)->set_y_1(((float)il2cpp_codegen_multiply((float)L_65, (float)L_67)));
		Vector2_t2156229523  L_68 = V_2;
		float L_69 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_70 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_68, L_69, /*hidden argument*/NULL);
		V_2 = L_70;
		Transform_t3600365921 * L_71 = __this->get_cameraPivot_4();
		float L_72 = (&V_2)->get_x_0();
		Transform_Rotate_m1660364534(L_71, (((float)((float)0))), L_72, (((float)((float)0))), 0, /*hidden argument*/NULL);
		Transform_t3600365921 * L_73 = __this->get_cameraPivot_4();
		float L_74 = (&V_2)->get_y_1();
		Transform_Rotate_m3172098886(L_73, L_74, (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraRelativeControl::Main()
extern "C"  void CameraRelativeControl_Main_m1291085707 (CameraRelativeControl_t2453217546 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FirstPersonControl::.ctor()
extern "C"  void FirstPersonControl__ctor_m1854408521 (FirstPersonControl_t610459381 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		__this->set_forwardSpeed_5((((float)((float)4))));
		__this->set_backwardSpeed_6((((float)((float)1))));
		__this->set_sidestepSpeed_7((((float)((float)1))));
		__this->set_jumpSpeed_8((((float)((float)8))));
		__this->set_inAirMultiplier_9((0.25f));
		Vector2_t2156229523  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m3970636864((&L_0), (((float)((float)((int32_t)50)))), (((float)((float)((int32_t)25)))), /*hidden argument*/NULL);
		__this->set_rotationSpeed_10(L_0);
		__this->set_tiltPositiveYAxis_11((0.6f));
		__this->set_tiltNegativeYAxis_12((0.4f));
		__this->set_tiltXAxisMinimum_13((0.1f));
		__this->set_canJump_18((bool)1);
		return;
	}
}
// System.Void FirstPersonControl::Start()
extern "C"  void FirstPersonControl_Start_m3117946383 (FirstPersonControl_t610459381 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirstPersonControl_Start_m3117946383_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	{
		RuntimeTypeHandle_t3027515415  L_0 = { reinterpret_cast<intptr_t> (Transform_t3600365921_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Component_t1923634451 * L_2 = Component_GetComponent_m886226392(__this, L_1, /*hidden argument*/NULL);
		__this->set_thisTransform_14(((Transform_t3600365921 *)CastclassClass((RuntimeObject*)L_2, Transform_t3600365921_il2cpp_TypeInfo_var)));
		RuntimeTypeHandle_t3027515415  L_3 = { reinterpret_cast<intptr_t> (CharacterController_t1138636865_0_0_0_var) };
		Type_t * L_4 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Component_t1923634451 * L_5 = Component_GetComponent_m886226392(__this, L_4, /*hidden argument*/NULL);
		__this->set_character_15(((CharacterController_t1138636865 *)CastclassSealed((RuntimeObject*)L_5, CharacterController_t1138636865_il2cpp_TypeInfo_var)));
		GameObject_t1113636619 * L_6 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral4038994995, /*hidden argument*/NULL);
		V_0 = L_6;
		GameObject_t1113636619 * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0062;
		}
	}
	{
		Transform_t3600365921 * L_9 = __this->get_thisTransform_14();
		GameObject_t1113636619 * L_10 = V_0;
		Transform_t3600365921 * L_11 = GameObject_get_transform_m1369836730(L_10, /*hidden argument*/NULL);
		Vector3_t3722313464  L_12 = Transform_get_position_m36019626(L_11, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_9, L_12, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Void FirstPersonControl::OnEndGame()
extern "C"  void FirstPersonControl_OnEndGame_m3664793188 (FirstPersonControl_t610459381 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirstPersonControl_OnEndGame_m3664793188_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Joystick_t9498292 * L_0 = __this->get_moveTouchPad_2();
		VirtActionInvoker0::Invoke(5 /* System.Void Joystick::Disable() */, L_0);
		Joystick_t9498292 * L_1 = __this->get_rotateTouchPad_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		Joystick_t9498292 * L_3 = __this->get_rotateTouchPad_3();
		VirtActionInvoker0::Invoke(5 /* System.Void Joystick::Disable() */, L_3);
	}

IL_0026:
	{
		Behaviour_set_enabled_m20417929(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FirstPersonControl::Update()
extern "C"  void FirstPersonControl_Update_m2287003549 (FirstPersonControl_t610459381 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirstPersonControl_Update_m2287003549_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	bool V_2 = false;
	Joystick_t9498292 * V_3 = NULL;
	Vector2_t2156229523  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	Vector3_t3722313464  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		Transform_t3600365921 * L_0 = __this->get_thisTransform_14();
		Joystick_t9498292 * L_1 = __this->get_moveTouchPad_2();
		Vector2_t2156229523 * L_2 = L_1->get_address_of_position_9();
		float L_3 = L_2->get_x_0();
		Joystick_t9498292 * L_4 = __this->get_moveTouchPad_2();
		Vector2_t2156229523 * L_5 = L_4->get_address_of_position_9();
		float L_6 = L_5->get_y_1();
		Vector3_t3722313464  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m3353183577((&L_7), L_3, (((float)((float)0))), L_6, /*hidden argument*/NULL);
		Vector3_t3722313464  L_8 = Transform_TransformDirection_m3784028109(L_0, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		(&V_0)->set_y_2((((float)((float)0))));
		Vector3_Normalize_m914904454((&V_0), /*hidden argument*/NULL);
		Joystick_t9498292 * L_9 = __this->get_moveTouchPad_2();
		Vector2_t2156229523 * L_10 = L_9->get_address_of_position_9();
		float L_11 = L_10->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_12 = fabsf(L_11);
		Joystick_t9498292 * L_13 = __this->get_moveTouchPad_2();
		Vector2_t2156229523 * L_14 = L_13->get_address_of_position_9();
		float L_15 = L_14->get_y_1();
		float L_16 = fabsf(L_15);
		Vector2_t2156229523  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector2__ctor_m3970636864((&L_17), L_12, L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		float L_18 = (&V_1)->get_y_1();
		float L_19 = (&V_1)->get_x_0();
		if ((((float)L_18) <= ((float)L_19)))
		{
			goto IL_00d1;
		}
	}
	{
		Joystick_t9498292 * L_20 = __this->get_moveTouchPad_2();
		Vector2_t2156229523 * L_21 = L_20->get_address_of_position_9();
		float L_22 = L_21->get_y_1();
		if ((((float)L_22) <= ((float)(((float)((float)0))))))
		{
			goto IL_00b7;
		}
	}
	{
		Vector3_t3722313464  L_23 = V_0;
		float L_24 = __this->get_forwardSpeed_5();
		float L_25 = (&V_1)->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_26 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_23, ((float)il2cpp_codegen_multiply((float)L_24, (float)L_25)), /*hidden argument*/NULL);
		V_0 = L_26;
		goto IL_00cc;
	}

IL_00b7:
	{
		Vector3_t3722313464  L_27 = V_0;
		float L_28 = __this->get_backwardSpeed_6();
		float L_29 = (&V_1)->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_30 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_27, ((float)il2cpp_codegen_multiply((float)L_28, (float)L_29)), /*hidden argument*/NULL);
		V_0 = L_30;
	}

IL_00cc:
	{
		goto IL_00e6;
	}

IL_00d1:
	{
		Vector3_t3722313464  L_31 = V_0;
		float L_32 = __this->get_sidestepSpeed_7();
		float L_33 = (&V_1)->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_34 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_31, ((float)il2cpp_codegen_multiply((float)L_32, (float)L_33)), /*hidden argument*/NULL);
		V_0 = L_34;
	}

IL_00e6:
	{
		CharacterController_t1138636865 * L_35 = __this->get_character_15();
		bool L_36 = CharacterController_get_isGrounded_m1151930607(L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_017c;
		}
	}
	{
		V_2 = (bool)0;
		V_3 = (Joystick_t9498292 *)NULL;
		Joystick_t9498292 * L_37 = __this->get_rotateTouchPad_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_38 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0116;
		}
	}
	{
		Joystick_t9498292 * L_39 = __this->get_rotateTouchPad_3();
		V_3 = L_39;
		goto IL_011d;
	}

IL_0116:
	{
		Joystick_t9498292 * L_40 = __this->get_moveTouchPad_2();
		V_3 = L_40;
	}

IL_011d:
	{
		Joystick_t9498292 * L_41 = V_3;
		bool L_42 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Joystick::IsFingerDown() */, L_41);
		if (L_42)
		{
			goto IL_012f;
		}
	}
	{
		__this->set_canJump_18((bool)1);
	}

IL_012f:
	{
		bool L_43 = __this->get_canJump_18();
		if (!L_43)
		{
			goto IL_014f;
		}
	}
	{
		Joystick_t9498292 * L_44 = V_3;
		int32_t L_45 = L_44->get_tapCount_10();
		if ((((int32_t)L_45) < ((int32_t)2)))
		{
			goto IL_014f;
		}
	}
	{
		V_2 = (bool)1;
		__this->set_canJump_18((bool)0);
	}

IL_014f:
	{
		bool L_46 = V_2;
		if (!L_46)
		{
			goto IL_0177;
		}
	}
	{
		CharacterController_t1138636865 * L_47 = __this->get_character_15();
		Vector3_t3722313464  L_48 = CharacterController_get_velocity_m3517335080(L_47, /*hidden argument*/NULL);
		__this->set_velocity_17(L_48);
		Vector3_t3722313464 * L_49 = __this->get_address_of_velocity_17();
		float L_50 = __this->get_jumpSpeed_8();
		L_49->set_y_2(L_50);
	}

IL_0177:
	{
		goto IL_01d1;
	}

IL_017c:
	{
		Vector3_t3722313464 * L_51 = __this->get_address_of_velocity_17();
		Vector3_t3722313464 * L_52 = __this->get_address_of_velocity_17();
		float L_53 = L_52->get_y_2();
		Vector3_t3722313464  L_54 = Physics_get_gravity_m2660066594(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_7 = L_54;
		float L_55 = (&V_7)->get_y_2();
		float L_56 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_51->set_y_2(((float)il2cpp_codegen_add((float)L_53, (float)((float)il2cpp_codegen_multiply((float)L_55, (float)L_56)))));
		float L_57 = (&V_0)->get_x_1();
		float L_58 = __this->get_inAirMultiplier_9();
		(&V_0)->set_x_1(((float)il2cpp_codegen_multiply((float)L_57, (float)L_58)));
		float L_59 = (&V_0)->get_z_3();
		float L_60 = __this->get_inAirMultiplier_9();
		(&V_0)->set_z_3(((float)il2cpp_codegen_multiply((float)L_59, (float)L_60)));
	}

IL_01d1:
	{
		Vector3_t3722313464  L_61 = V_0;
		Vector3_t3722313464  L_62 = __this->get_velocity_17();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_63 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_61, L_62, /*hidden argument*/NULL);
		V_0 = L_63;
		Vector3_t3722313464  L_64 = V_0;
		Vector3_t3722313464  L_65 = Physics_get_gravity_m2660066594(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_66 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_64, L_65, /*hidden argument*/NULL);
		V_0 = L_66;
		Vector3_t3722313464  L_67 = V_0;
		float L_68 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_69 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_67, L_68, /*hidden argument*/NULL);
		V_0 = L_69;
		CharacterController_t1138636865 * L_70 = __this->get_character_15();
		Vector3_t3722313464  L_71 = V_0;
		CharacterController_Move_m1547317252(L_70, L_71, /*hidden argument*/NULL);
		CharacterController_t1138636865 * L_72 = __this->get_character_15();
		bool L_73 = CharacterController_get_isGrounded_m1151930607(L_72, /*hidden argument*/NULL);
		if (!L_73)
		{
			goto IL_021e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_74 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_velocity_17(L_74);
	}

IL_021e:
	{
		CharacterController_t1138636865 * L_75 = __this->get_character_15();
		bool L_76 = CharacterController_get_isGrounded_m1151930607(L_75, /*hidden argument*/NULL);
		if (!L_76)
		{
			goto IL_0380;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_77 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_77;
		Joystick_t9498292 * L_78 = __this->get_rotateTouchPad_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_79 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_78, /*hidden argument*/NULL);
		if (!L_79)
		{
			goto IL_0257;
		}
	}
	{
		Joystick_t9498292 * L_80 = __this->get_rotateTouchPad_3();
		Vector2_t2156229523  L_81 = L_80->get_position_9();
		V_4 = L_81;
		goto IL_0310;
	}

IL_0257:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_82 = Input_get_acceleration_m2528400370(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = L_82;
		float L_83 = (&V_5)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_84 = fabsf(L_83);
		V_6 = L_84;
		float L_85 = (&V_5)->get_z_3();
		if ((((float)L_85) >= ((float)(((float)((float)0))))))
		{
			goto IL_02d9;
		}
	}
	{
		float L_86 = (&V_5)->get_x_1();
		if ((((float)L_86) >= ((float)(((float)((float)0))))))
		{
			goto IL_02d9;
		}
	}
	{
		float L_87 = V_6;
		float L_88 = __this->get_tiltPositiveYAxis_11();
		if ((((float)L_87) < ((float)L_88)))
		{
			goto IL_02b4;
		}
	}
	{
		float L_89 = V_6;
		float L_90 = __this->get_tiltPositiveYAxis_11();
		float L_91 = __this->get_tiltPositiveYAxis_11();
		(&V_4)->set_y_1(((float)((float)((float)il2cpp_codegen_subtract((float)L_89, (float)L_90))/(float)((float)il2cpp_codegen_subtract((float)(((float)((float)1))), (float)L_91)))));
		goto IL_02d9;
	}

IL_02b4:
	{
		float L_92 = V_6;
		float L_93 = __this->get_tiltNegativeYAxis_12();
		if ((((float)L_92) > ((float)L_93)))
		{
			goto IL_02d9;
		}
	}
	{
		float L_94 = __this->get_tiltNegativeYAxis_12();
		float L_95 = V_6;
		float L_96 = __this->get_tiltNegativeYAxis_12();
		(&V_4)->set_y_1(((float)((float)((-((float)il2cpp_codegen_subtract((float)L_94, (float)L_95))))/(float)L_96)));
	}

IL_02d9:
	{
		float L_97 = (&V_5)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_98 = fabsf(L_97);
		float L_99 = __this->get_tiltXAxisMinimum_13();
		if ((((float)L_98) < ((float)L_99)))
		{
			goto IL_0310;
		}
	}
	{
		float L_100 = (&V_5)->get_y_2();
		float L_101 = __this->get_tiltXAxisMinimum_13();
		float L_102 = __this->get_tiltXAxisMinimum_13();
		(&V_4)->set_x_0(((float)((float)((-((float)il2cpp_codegen_subtract((float)L_100, (float)L_101))))/(float)((float)il2cpp_codegen_subtract((float)(((float)((float)1))), (float)L_102)))));
	}

IL_0310:
	{
		float L_103 = (&V_4)->get_x_0();
		Vector2_t2156229523 * L_104 = __this->get_address_of_rotationSpeed_10();
		float L_105 = L_104->get_x_0();
		(&V_4)->set_x_0(((float)il2cpp_codegen_multiply((float)L_103, (float)L_105)));
		float L_106 = (&V_4)->get_y_1();
		Vector2_t2156229523 * L_107 = __this->get_address_of_rotationSpeed_10();
		float L_108 = L_107->get_y_1();
		(&V_4)->set_y_1(((float)il2cpp_codegen_multiply((float)L_106, (float)L_108)));
		Vector2_t2156229523  L_109 = V_4;
		float L_110 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_111 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_109, L_110, /*hidden argument*/NULL);
		V_4 = L_111;
		Transform_t3600365921 * L_112 = __this->get_thisTransform_14();
		float L_113 = (&V_4)->get_x_0();
		Transform_Rotate_m1660364534(L_112, (((float)((float)0))), L_113, (((float)((float)0))), 0, /*hidden argument*/NULL);
		Transform_t3600365921 * L_114 = __this->get_cameraPivot_4();
		float L_115 = (&V_4)->get_y_1();
		Transform_Rotate_m3172098886(L_114, ((-L_115)), (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
	}

IL_0380:
	{
		return;
	}
}
// System.Void FirstPersonControl::Main()
extern "C"  void FirstPersonControl_Main_m2080926703 (FirstPersonControl_t610459381 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FollowTransform::.ctor()
extern "C"  void FollowTransform__ctor_m431836946 (FollowTransform_t1230726939 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FollowTransform::Start()
extern "C"  void FollowTransform_Start_m993467700 (FollowTransform_t1230726939 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		__this->set_thisTransform_4(L_0);
		return;
	}
}
// System.Void FollowTransform::Update()
extern "C"  void FollowTransform_Update_m3752090140 (FollowTransform_t1230726939 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = __this->get_thisTransform_4();
		Transform_t3600365921 * L_1 = __this->get_targetTransform_2();
		Vector3_t3722313464  L_2 = Transform_get_position_m36019626(L_1, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_0, L_2, /*hidden argument*/NULL);
		bool L_3 = __this->get_faceForward_3();
		if (!L_3)
		{
			goto IL_0037;
		}
	}
	{
		Transform_t3600365921 * L_4 = __this->get_thisTransform_4();
		Transform_t3600365921 * L_5 = __this->get_targetTransform_2();
		Vector3_t3722313464  L_6 = Transform_get_forward_m747522392(L_5, /*hidden argument*/NULL);
		Transform_set_forward_m1840797198(L_4, L_6, /*hidden argument*/NULL);
	}

IL_0037:
	{
		return;
	}
}
// System.Void FollowTransform::Main()
extern "C"  void FollowTransform_Main_m860480972 (FollowTransform_t1230726939 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Joystick::.ctor()
extern "C"  void Joystick__ctor_m2202167957 (Joystick_t9498292 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Joystick__ctor_m2202167957_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_0 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_deadZone_7(L_0);
		__this->set_lastFingerId_11((-1));
		__this->set_firstDeltaTime_15((0.5f));
		Boundary_t2442033035 * L_1 = (Boundary_t2442033035 *)il2cpp_codegen_object_new(Boundary_t2442033035_il2cpp_TypeInfo_var);
		Boundary__ctor_m1524210624(L_1, /*hidden argument*/NULL);
		__this->set_guiBoundary_18(L_1);
		return;
	}
}
// System.Void Joystick::.cctor()
extern "C"  void Joystick__cctor_m885003068 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Joystick__cctor_m885003068_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Joystick_t9498292_StaticFields*)il2cpp_codegen_static_fields_for(Joystick_t9498292_il2cpp_TypeInfo_var))->set_tapTimeDelta_4((0.3f));
		return;
	}
}
// System.Void Joystick::Start()
extern "C"  void Joystick_Start_m1421348663 (Joystick_t9498292 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Joystick_Start_m1421348663_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	Vector3_t3722313464  V_7;
	memset(&V_7, 0, sizeof(V_7));
	float V_8 = 0.0f;
	Vector3_t3722313464  V_9;
	memset(&V_9, 0, sizeof(V_9));
	{
		RuntimeTypeHandle_t3027515415  L_0 = { reinterpret_cast<intptr_t> (GUITexture_t951903601_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Component_t1923634451 * L_2 = Component_GetComponent_m886226392(__this, L_1, /*hidden argument*/NULL);
		__this->set_gui_16(((GUITexture_t951903601 *)CastclassSealed((RuntimeObject*)L_2, GUITexture_t951903601_il2cpp_TypeInfo_var)));
		GUITexture_t951903601 * L_3 = __this->get_gui_16();
		Rect_t2360479859  L_4 = GUITexture_get_pixelInset_m2981475662(L_3, /*hidden argument*/NULL);
		__this->set_defaultRect_17(L_4);
		Rect_t2360479859 * L_5 = __this->get_address_of_defaultRect_17();
		Rect_t2360479859 * L_6 = __this->get_address_of_defaultRect_17();
		float L_7 = Rect_get_x_m3839990490(L_6, /*hidden argument*/NULL);
		Transform_t3600365921 * L_8 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_9 = Transform_get_position_m36019626(L_8, /*hidden argument*/NULL);
		V_4 = L_9;
		float L_10 = (&V_4)->get_x_1();
		int32_t L_11 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_x_m2352063068(L_5, ((float)il2cpp_codegen_add((float)L_7, (float)((float)il2cpp_codegen_multiply((float)L_10, (float)(((float)((float)L_11))))))), /*hidden argument*/NULL);
		Rect_t2360479859 * L_12 = __this->get_address_of_defaultRect_17();
		Rect_t2360479859 * L_13 = __this->get_address_of_defaultRect_17();
		float L_14 = Rect_get_y_m1501338330(L_13, /*hidden argument*/NULL);
		Transform_t3600365921 * L_15 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_16 = Transform_get_position_m36019626(L_15, /*hidden argument*/NULL);
		V_5 = L_16;
		float L_17 = (&V_5)->get_y_2();
		int32_t L_18 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_set_y_m3702432190(L_12, ((float)il2cpp_codegen_add((float)L_14, (float)((float)il2cpp_codegen_multiply((float)L_17, (float)(((float)((float)L_18))))))), /*hidden argument*/NULL);
		float L_19 = (((float)((float)0)));
		V_0 = L_19;
		Transform_t3600365921 * L_20 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_21 = Transform_get_position_m36019626(L_20, /*hidden argument*/NULL);
		Vector3_t3722313464  L_22 = L_21;
		V_1 = L_22;
		float L_23 = V_0;
		float L_24 = L_23;
		V_6 = L_24;
		(&V_1)->set_x_1(L_24);
		float L_25 = V_6;
		Transform_t3600365921 * L_26 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_27 = V_1;
		Vector3_t3722313464  L_28 = L_27;
		V_7 = L_28;
		Transform_set_position_m3387557959(L_26, L_28, /*hidden argument*/NULL);
		Vector3_t3722313464  L_29 = V_7;
		float L_30 = (((float)((float)0)));
		V_2 = L_30;
		Transform_t3600365921 * L_31 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_32 = Transform_get_position_m36019626(L_31, /*hidden argument*/NULL);
		Vector3_t3722313464  L_33 = L_32;
		V_3 = L_33;
		float L_34 = V_2;
		float L_35 = L_34;
		V_8 = L_35;
		(&V_3)->set_y_2(L_35);
		float L_36 = V_8;
		Transform_t3600365921 * L_37 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_38 = V_3;
		Vector3_t3722313464  L_39 = L_38;
		V_9 = L_39;
		Transform_set_position_m3387557959(L_37, L_39, /*hidden argument*/NULL);
		Vector3_t3722313464  L_40 = V_9;
		bool L_41 = __this->get_touchPad_5();
		if (!L_41)
		{
			goto IL_0127;
		}
	}
	{
		GUITexture_t951903601 * L_42 = __this->get_gui_16();
		Texture_t3661962703 * L_43 = GUITexture_get_texture_m1784897130(L_42, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_44 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_0122;
		}
	}
	{
		Rect_t2360479859  L_45 = __this->get_defaultRect_17();
		__this->set_touchZone_6(L_45);
	}

IL_0122:
	{
		goto IL_023f;
	}

IL_0127:
	{
		Vector2_t2156229523 * L_46 = __this->get_address_of_guiTouchOffset_19();
		Rect_t2360479859 * L_47 = __this->get_address_of_defaultRect_17();
		float L_48 = Rect_get_width_m3421484486(L_47, /*hidden argument*/NULL);
		L_46->set_x_0(((float)il2cpp_codegen_multiply((float)L_48, (float)(0.5f))));
		Vector2_t2156229523 * L_49 = __this->get_address_of_guiTouchOffset_19();
		Rect_t2360479859 * L_50 = __this->get_address_of_defaultRect_17();
		float L_51 = Rect_get_height_m1358425599(L_50, /*hidden argument*/NULL);
		L_49->set_y_1(((float)il2cpp_codegen_multiply((float)L_51, (float)(0.5f))));
		Vector2_t2156229523 * L_52 = __this->get_address_of_guiCenter_20();
		Rect_t2360479859 * L_53 = __this->get_address_of_defaultRect_17();
		float L_54 = Rect_get_x_m3839990490(L_53, /*hidden argument*/NULL);
		Vector2_t2156229523 * L_55 = __this->get_address_of_guiTouchOffset_19();
		float L_56 = L_55->get_x_0();
		L_52->set_x_0(((float)il2cpp_codegen_add((float)L_54, (float)L_56)));
		Vector2_t2156229523 * L_57 = __this->get_address_of_guiCenter_20();
		Rect_t2360479859 * L_58 = __this->get_address_of_defaultRect_17();
		float L_59 = Rect_get_y_m1501338330(L_58, /*hidden argument*/NULL);
		Vector2_t2156229523 * L_60 = __this->get_address_of_guiTouchOffset_19();
		float L_61 = L_60->get_y_1();
		L_57->set_y_1(((float)il2cpp_codegen_add((float)L_59, (float)L_61)));
		Boundary_t2442033035 * L_62 = __this->get_guiBoundary_18();
		Vector2_t2156229523 * L_63 = L_62->get_address_of_min_0();
		Rect_t2360479859 * L_64 = __this->get_address_of_defaultRect_17();
		float L_65 = Rect_get_x_m3839990490(L_64, /*hidden argument*/NULL);
		Vector2_t2156229523 * L_66 = __this->get_address_of_guiTouchOffset_19();
		float L_67 = L_66->get_x_0();
		L_63->set_x_0(((float)il2cpp_codegen_subtract((float)L_65, (float)L_67)));
		Boundary_t2442033035 * L_68 = __this->get_guiBoundary_18();
		Vector2_t2156229523 * L_69 = L_68->get_address_of_max_1();
		Rect_t2360479859 * L_70 = __this->get_address_of_defaultRect_17();
		float L_71 = Rect_get_x_m3839990490(L_70, /*hidden argument*/NULL);
		Vector2_t2156229523 * L_72 = __this->get_address_of_guiTouchOffset_19();
		float L_73 = L_72->get_x_0();
		L_69->set_x_0(((float)il2cpp_codegen_add((float)L_71, (float)L_73)));
		Boundary_t2442033035 * L_74 = __this->get_guiBoundary_18();
		Vector2_t2156229523 * L_75 = L_74->get_address_of_min_0();
		Rect_t2360479859 * L_76 = __this->get_address_of_defaultRect_17();
		float L_77 = Rect_get_y_m1501338330(L_76, /*hidden argument*/NULL);
		Vector2_t2156229523 * L_78 = __this->get_address_of_guiTouchOffset_19();
		float L_79 = L_78->get_y_1();
		L_75->set_y_1(((float)il2cpp_codegen_subtract((float)L_77, (float)L_79)));
		Boundary_t2442033035 * L_80 = __this->get_guiBoundary_18();
		Vector2_t2156229523 * L_81 = L_80->get_address_of_max_1();
		Rect_t2360479859 * L_82 = __this->get_address_of_defaultRect_17();
		float L_83 = Rect_get_y_m1501338330(L_82, /*hidden argument*/NULL);
		Vector2_t2156229523 * L_84 = __this->get_address_of_guiTouchOffset_19();
		float L_85 = L_84->get_y_1();
		L_81->set_y_1(((float)il2cpp_codegen_add((float)L_83, (float)L_85)));
	}

IL_023f:
	{
		return;
	}
}
// System.Void Joystick::Disable()
extern "C"  void Joystick_Disable_m3439812130 (Joystick_t9498292 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Joystick_Disable_m3439812130_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		GameObject_SetActive_m796801857(L_0, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Joystick_t9498292_il2cpp_TypeInfo_var);
		((Joystick_t9498292_StaticFields*)il2cpp_codegen_static_fields_for(Joystick_t9498292_il2cpp_TypeInfo_var))->set_enumeratedJoysticks_3((bool)0);
		return;
	}
}
// System.Void Joystick::ResetJoystick()
extern "C"  void Joystick_ResetJoystick_m1139569856 (Joystick_t9498292 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Joystick_ResetJoystick_m1139569856_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Color_t2555686324  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Color_t2555686324  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		GUITexture_t951903601 * L_0 = __this->get_gui_16();
		Rect_t2360479859  L_1 = __this->get_defaultRect_17();
		GUITexture_set_pixelInset_m2556409115(L_0, L_1, /*hidden argument*/NULL);
		__this->set_lastFingerId_11((-1));
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_2 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_position_9(L_2);
		Vector2_t2156229523  L_3 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_fingerDownPos_13(L_3);
		bool L_4 = __this->get_touchPad_5();
		if (!L_4)
		{
			goto IL_006b;
		}
	}
	{
		float L_5 = (0.025f);
		V_0 = L_5;
		GUITexture_t951903601 * L_6 = __this->get_gui_16();
		Color_t2555686324  L_7 = GUITexture_get_color_m2101240858(L_6, /*hidden argument*/NULL);
		Color_t2555686324  L_8 = L_7;
		V_1 = L_8;
		float L_9 = V_0;
		float L_10 = L_9;
		V_2 = L_10;
		(&V_1)->set_a_3(L_10);
		float L_11 = V_2;
		GUITexture_t951903601 * L_12 = __this->get_gui_16();
		Color_t2555686324  L_13 = V_1;
		Color_t2555686324  L_14 = L_13;
		V_3 = L_14;
		GUITexture_set_color_m1660397747(L_12, L_14, /*hidden argument*/NULL);
		Color_t2555686324  L_15 = V_3;
	}

IL_006b:
	{
		return;
	}
}
// System.Boolean Joystick::IsFingerDown()
extern "C"  bool Joystick_IsFingerDown_m3787158851 (Joystick_t9498292 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_lastFingerId_11();
		return (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Joystick::LatchedFinger(System.Int32)
extern "C"  void Joystick_LatchedFinger_m3786409468 (Joystick_t9498292 * __this, int32_t ___fingerId0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_lastFingerId_11();
		int32_t L_1 = ___fingerId0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		VirtActionInvoker0::Invoke(6 /* System.Void Joystick::ResetJoystick() */, __this);
	}

IL_0012:
	{
		return;
	}
}
// System.Void Joystick::Update()
extern "C"  void Joystick_Update_m724918018 (Joystick_t9498292 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Joystick_Update_m724918018_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Touch_t1921856868  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2156229523  V_3;
	memset(&V_3, 0, sizeof(V_3));
	bool V_4 = false;
	Joystick_t9498292 * V_5 = NULL;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	int32_t V_8 = 0;
	JoystickU5BU5D_t4275182589* V_9 = NULL;
	int32_t V_10 = 0;
	float V_11 = 0.0f;
	Color_t2555686324  V_12;
	memset(&V_12, 0, sizeof(V_12));
	float V_13 = 0.0f;
	Rect_t2360479859  V_14;
	memset(&V_14, 0, sizeof(V_14));
	float V_15 = 0.0f;
	Rect_t2360479859  V_16;
	memset(&V_16, 0, sizeof(V_16));
	float V_17 = 0.0f;
	Color_t2555686324  V_18;
	memset(&V_18, 0, sizeof(V_18));
	Vector2_t2156229523  V_19;
	memset(&V_19, 0, sizeof(V_19));
	Vector2_t2156229523  V_20;
	memset(&V_20, 0, sizeof(V_20));
	float V_21 = 0.0f;
	Rect_t2360479859  V_22;
	memset(&V_22, 0, sizeof(V_22));
	float V_23 = 0.0f;
	Rect_t2360479859  V_24;
	memset(&V_24, 0, sizeof(V_24));
	Rect_t2360479859  V_25;
	memset(&V_25, 0, sizeof(V_25));
	Rect_t2360479859  V_26;
	memset(&V_26, 0, sizeof(V_26));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Joystick_t9498292_il2cpp_TypeInfo_var);
		bool L_0 = ((Joystick_t9498292_StaticFields*)il2cpp_codegen_static_fields_for(Joystick_t9498292_il2cpp_TypeInfo_var))->get_enumeratedJoysticks_3();
		if (L_0)
		{
			goto IL_002e;
		}
	}
	{
		RuntimeTypeHandle_t3027515415  L_1 = { reinterpret_cast<intptr_t> (Joystick_t9498292_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t1417781964* L_3 = Object_FindObjectsOfType_m2295101757(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Joystick_t9498292_il2cpp_TypeInfo_var);
		((Joystick_t9498292_StaticFields*)il2cpp_codegen_static_fields_for(Joystick_t9498292_il2cpp_TypeInfo_var))->set_joysticks_2(((JoystickU5BU5D_t4275182589*)IsInst((RuntimeObject*)((JoystickU5BU5D_t4275182589*)Castclass((RuntimeObject*)L_3, JoystickU5BU5D_t4275182589_il2cpp_TypeInfo_var)), JoystickU5BU5D_t4275182589_il2cpp_TypeInfo_var)));
		((Joystick_t9498292_StaticFields*)il2cpp_codegen_static_fields_for(Joystick_t9498292_il2cpp_TypeInfo_var))->set_enumeratedJoysticks_3((bool)1);
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		int32_t L_4 = Input_get_touchCount_m3403849067(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = __this->get_tapTimeWindow_12();
		if ((((float)L_5) <= ((float)(((float)((float)0))))))
		{
			goto IL_0058;
		}
	}
	{
		float L_6 = __this->get_tapTimeWindow_12();
		float L_7 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_tapTimeWindow_12(((float)il2cpp_codegen_subtract((float)L_6, (float)L_7)));
		goto IL_005f;
	}

IL_0058:
	{
		__this->set_tapCount_10(0);
	}

IL_005f:
	{
		int32_t L_8 = V_0;
		if (L_8)
		{
			goto IL_0070;
		}
	}
	{
		VirtActionInvoker0::Invoke(6 /* System.Void Joystick::ResetJoystick() */, __this);
		goto IL_039f;
	}

IL_0070:
	{
		V_1 = 0;
		goto IL_0398;
	}

IL_0077:
	{
		int32_t L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Touch_t1921856868  L_10 = Input_GetTouch_m2192712756(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		Vector2_t2156229523  L_11 = Touch_get_position_m3109777936((&V_2), /*hidden argument*/NULL);
		Vector2_t2156229523  L_12 = __this->get_guiTouchOffset_19();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_13 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		V_4 = (bool)0;
		bool L_14 = __this->get_touchPad_5();
		if (!L_14)
		{
			goto IL_00be;
		}
	}
	{
		Rect_t2360479859 * L_15 = __this->get_address_of_touchZone_6();
		Vector2_t2156229523  L_16 = Touch_get_position_m3109777936((&V_2), /*hidden argument*/NULL);
		bool L_17 = Rect_Contains_m3188543637(L_15, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00b9;
		}
	}
	{
		V_4 = (bool)1;
	}

IL_00b9:
	{
		goto IL_00dd;
	}

IL_00be:
	{
		GUITexture_t951903601 * L_18 = __this->get_gui_16();
		Vector2_t2156229523  L_19 = Touch_get_position_m3109777936((&V_2), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_20 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		bool L_21 = GUIElement_HitTest_m3916646804(L_18, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00dd;
		}
	}
	{
		V_4 = (bool)1;
	}

IL_00dd:
	{
		bool L_22 = V_4;
		if (!L_22)
		{
			goto IL_01f3;
		}
	}
	{
		int32_t L_23 = __this->get_lastFingerId_11();
		if ((((int32_t)L_23) == ((int32_t)(-1))))
		{
			goto IL_0102;
		}
	}
	{
		int32_t L_24 = __this->get_lastFingerId_11();
		int32_t L_25 = Touch_get_fingerId_m859576425((&V_2), /*hidden argument*/NULL);
		if ((((int32_t)L_24) == ((int32_t)L_25)))
		{
			goto IL_01f3;
		}
	}

IL_0102:
	{
		bool L_26 = __this->get_touchPad_5();
		if (!L_26)
		{
			goto IL_016c;
		}
	}
	{
		float L_27 = (0.15f);
		V_11 = L_27;
		GUITexture_t951903601 * L_28 = __this->get_gui_16();
		Color_t2555686324  L_29 = GUITexture_get_color_m2101240858(L_28, /*hidden argument*/NULL);
		Color_t2555686324  L_30 = L_29;
		V_12 = L_30;
		float L_31 = V_11;
		float L_32 = L_31;
		V_17 = L_32;
		(&V_12)->set_a_3(L_32);
		float L_33 = V_17;
		GUITexture_t951903601 * L_34 = __this->get_gui_16();
		Color_t2555686324  L_35 = V_12;
		Color_t2555686324  L_36 = L_35;
		V_18 = L_36;
		GUITexture_set_color_m1660397747(L_34, L_36, /*hidden argument*/NULL);
		Color_t2555686324  L_37 = V_18;
		int32_t L_38 = Touch_get_fingerId_m859576425((&V_2), /*hidden argument*/NULL);
		__this->set_lastFingerId_11(L_38);
		Vector2_t2156229523  L_39 = Touch_get_position_m3109777936((&V_2), /*hidden argument*/NULL);
		__this->set_fingerDownPos_13(L_39);
		float L_40 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_fingerDownTime_14(L_40);
	}

IL_016c:
	{
		int32_t L_41 = Touch_get_fingerId_m859576425((&V_2), /*hidden argument*/NULL);
		__this->set_lastFingerId_11(L_41);
		float L_42 = __this->get_tapTimeWindow_12();
		if ((((float)L_42) <= ((float)(((float)((float)0))))))
		{
			goto IL_0199;
		}
	}
	{
		int32_t L_43 = __this->get_tapCount_10();
		__this->set_tapCount_10(((int32_t)il2cpp_codegen_add((int32_t)L_43, (int32_t)1)));
		goto IL_01ab;
	}

IL_0199:
	{
		__this->set_tapCount_10(1);
		IL2CPP_RUNTIME_CLASS_INIT(Joystick_t9498292_il2cpp_TypeInfo_var);
		float L_44 = ((Joystick_t9498292_StaticFields*)il2cpp_codegen_static_fields_for(Joystick_t9498292_il2cpp_TypeInfo_var))->get_tapTimeDelta_4();
		__this->set_tapTimeWindow_12(L_44);
	}

IL_01ab:
	{
		V_8 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Joystick_t9498292_il2cpp_TypeInfo_var);
		JoystickU5BU5D_t4275182589* L_45 = ((Joystick_t9498292_StaticFields*)il2cpp_codegen_static_fields_for(Joystick_t9498292_il2cpp_TypeInfo_var))->get_joysticks_2();
		V_9 = L_45;
		JoystickU5BU5D_t4275182589* L_46 = V_9;
		int32_t L_47 = Array_get_Length_m21610649((RuntimeArray *)(RuntimeArray *)L_46, /*hidden argument*/NULL);
		V_10 = L_47;
		goto IL_01ea;
	}

IL_01c3:
	{
		JoystickU5BU5D_t4275182589* L_48 = V_9;
		int32_t L_49 = V_8;
		int32_t L_50 = L_49;
		Joystick_t9498292 * L_51 = (L_48)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_50));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_52 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_51, __this, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_01e4;
		}
	}
	{
		JoystickU5BU5D_t4275182589* L_53 = V_9;
		int32_t L_54 = V_8;
		int32_t L_55 = L_54;
		Joystick_t9498292 * L_56 = (L_53)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_55));
		int32_t L_57 = Touch_get_fingerId_m859576425((&V_2), /*hidden argument*/NULL);
		VirtActionInvoker1< int32_t >::Invoke(8 /* System.Void Joystick::LatchedFinger(System.Int32) */, L_56, L_57);
	}

IL_01e4:
	{
		int32_t L_58 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_58, (int32_t)1));
	}

IL_01ea:
	{
		int32_t L_59 = V_8;
		int32_t L_60 = V_10;
		if ((((int32_t)L_59) < ((int32_t)L_60)))
		{
			goto IL_01c3;
		}
	}

IL_01f3:
	{
		int32_t L_61 = __this->get_lastFingerId_11();
		int32_t L_62 = Touch_get_fingerId_m859576425((&V_2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_61) == ((uint32_t)L_62))))
		{
			goto IL_0394;
		}
	}
	{
		int32_t L_63 = Touch_get_tapCount_m2125417096((&V_2), /*hidden argument*/NULL);
		int32_t L_64 = __this->get_tapCount_10();
		if ((((int32_t)L_63) <= ((int32_t)L_64)))
		{
			goto IL_0224;
		}
	}
	{
		int32_t L_65 = Touch_get_tapCount_m2125417096((&V_2), /*hidden argument*/NULL);
		__this->set_tapCount_10(L_65);
	}

IL_0224:
	{
		bool L_66 = __this->get_touchPad_5();
		if (!L_66)
		{
			goto IL_02b2;
		}
	}
	{
		Vector2_t2156229523 * L_67 = __this->get_address_of_position_9();
		Vector2_t2156229523  L_68 = Touch_get_position_m3109777936((&V_2), /*hidden argument*/NULL);
		V_19 = L_68;
		float L_69 = (&V_19)->get_x_0();
		Vector2_t2156229523 * L_70 = __this->get_address_of_fingerDownPos_13();
		float L_71 = L_70->get_x_0();
		Rect_t2360479859 * L_72 = __this->get_address_of_touchZone_6();
		float L_73 = Rect_get_width_m3421484486(L_72, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_74 = Mathf_Clamp_m3350697880(NULL /*static, unused*/, ((float)((float)((float)il2cpp_codegen_subtract((float)L_69, (float)L_71))/(float)((float)((float)L_73/(float)(((float)((float)2))))))), (((float)((float)(-1)))), (((float)((float)1))), /*hidden argument*/NULL);
		L_67->set_x_0(L_74);
		Vector2_t2156229523 * L_75 = __this->get_address_of_position_9();
		Vector2_t2156229523  L_76 = Touch_get_position_m3109777936((&V_2), /*hidden argument*/NULL);
		V_20 = L_76;
		float L_77 = (&V_20)->get_y_1();
		Vector2_t2156229523 * L_78 = __this->get_address_of_fingerDownPos_13();
		float L_79 = L_78->get_y_1();
		Rect_t2360479859 * L_80 = __this->get_address_of_touchZone_6();
		float L_81 = Rect_get_height_m1358425599(L_80, /*hidden argument*/NULL);
		float L_82 = Mathf_Clamp_m3350697880(NULL /*static, unused*/, ((float)((float)((float)il2cpp_codegen_subtract((float)L_77, (float)L_79))/(float)((float)((float)L_81/(float)(((float)((float)2))))))), (((float)((float)(-1)))), (((float)((float)1))), /*hidden argument*/NULL);
		L_75->set_y_1(L_82);
		goto IL_0374;
	}

IL_02b2:
	{
		float L_83 = (&V_3)->get_x_0();
		Boundary_t2442033035 * L_84 = __this->get_guiBoundary_18();
		Vector2_t2156229523 * L_85 = L_84->get_address_of_min_0();
		float L_86 = L_85->get_x_0();
		Boundary_t2442033035 * L_87 = __this->get_guiBoundary_18();
		Vector2_t2156229523 * L_88 = L_87->get_address_of_max_1();
		float L_89 = L_88->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_90 = Mathf_Clamp_m3350697880(NULL /*static, unused*/, L_83, L_86, L_89, /*hidden argument*/NULL);
		float L_91 = L_90;
		V_13 = L_91;
		GUITexture_t951903601 * L_92 = __this->get_gui_16();
		Rect_t2360479859  L_93 = GUITexture_get_pixelInset_m2981475662(L_92, /*hidden argument*/NULL);
		Rect_t2360479859  L_94 = L_93;
		V_14 = L_94;
		float L_95 = V_13;
		float L_96 = L_95;
		V_21 = L_96;
		Rect_set_x_m2352063068((&V_14), L_96, /*hidden argument*/NULL);
		float L_97 = V_21;
		GUITexture_t951903601 * L_98 = __this->get_gui_16();
		Rect_t2360479859  L_99 = V_14;
		Rect_t2360479859  L_100 = L_99;
		V_22 = L_100;
		GUITexture_set_pixelInset_m2556409115(L_98, L_100, /*hidden argument*/NULL);
		Rect_t2360479859  L_101 = V_22;
		float L_102 = (&V_3)->get_y_1();
		Boundary_t2442033035 * L_103 = __this->get_guiBoundary_18();
		Vector2_t2156229523 * L_104 = L_103->get_address_of_min_0();
		float L_105 = L_104->get_y_1();
		Boundary_t2442033035 * L_106 = __this->get_guiBoundary_18();
		Vector2_t2156229523 * L_107 = L_106->get_address_of_max_1();
		float L_108 = L_107->get_y_1();
		float L_109 = Mathf_Clamp_m3350697880(NULL /*static, unused*/, L_102, L_105, L_108, /*hidden argument*/NULL);
		float L_110 = L_109;
		V_15 = L_110;
		GUITexture_t951903601 * L_111 = __this->get_gui_16();
		Rect_t2360479859  L_112 = GUITexture_get_pixelInset_m2981475662(L_111, /*hidden argument*/NULL);
		Rect_t2360479859  L_113 = L_112;
		V_16 = L_113;
		float L_114 = V_15;
		float L_115 = L_114;
		V_23 = L_115;
		Rect_set_y_m3702432190((&V_16), L_115, /*hidden argument*/NULL);
		float L_116 = V_23;
		GUITexture_t951903601 * L_117 = __this->get_gui_16();
		Rect_t2360479859  L_118 = V_16;
		Rect_t2360479859  L_119 = L_118;
		V_24 = L_119;
		GUITexture_set_pixelInset_m2556409115(L_117, L_119, /*hidden argument*/NULL);
		Rect_t2360479859  L_120 = V_24;
	}

IL_0374:
	{
		int32_t L_121 = Touch_get_phase_m214549210((&V_2), /*hidden argument*/NULL);
		if ((((int32_t)L_121) == ((int32_t)3)))
		{
			goto IL_038e;
		}
	}
	{
		int32_t L_122 = Touch_get_phase_m214549210((&V_2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_122) == ((uint32_t)4))))
		{
			goto IL_0394;
		}
	}

IL_038e:
	{
		VirtActionInvoker0::Invoke(6 /* System.Void Joystick::ResetJoystick() */, __this);
	}

IL_0394:
	{
		int32_t L_123 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_123, (int32_t)1));
	}

IL_0398:
	{
		int32_t L_124 = V_1;
		int32_t L_125 = V_0;
		if ((((int32_t)L_124) < ((int32_t)L_125)))
		{
			goto IL_0077;
		}
	}

IL_039f:
	{
		bool L_126 = __this->get_touchPad_5();
		if (L_126)
		{
			goto IL_0430;
		}
	}
	{
		Vector2_t2156229523 * L_127 = __this->get_address_of_position_9();
		GUITexture_t951903601 * L_128 = __this->get_gui_16();
		Rect_t2360479859  L_129 = GUITexture_get_pixelInset_m2981475662(L_128, /*hidden argument*/NULL);
		V_25 = L_129;
		float L_130 = Rect_get_x_m3839990490((&V_25), /*hidden argument*/NULL);
		Vector2_t2156229523 * L_131 = __this->get_address_of_guiTouchOffset_19();
		float L_132 = L_131->get_x_0();
		Vector2_t2156229523 * L_133 = __this->get_address_of_guiCenter_20();
		float L_134 = L_133->get_x_0();
		Vector2_t2156229523 * L_135 = __this->get_address_of_guiTouchOffset_19();
		float L_136 = L_135->get_x_0();
		L_127->set_x_0(((float)((float)((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_add((float)L_130, (float)L_132)), (float)L_134))/(float)L_136)));
		Vector2_t2156229523 * L_137 = __this->get_address_of_position_9();
		GUITexture_t951903601 * L_138 = __this->get_gui_16();
		Rect_t2360479859  L_139 = GUITexture_get_pixelInset_m2981475662(L_138, /*hidden argument*/NULL);
		V_26 = L_139;
		float L_140 = Rect_get_y_m1501338330((&V_26), /*hidden argument*/NULL);
		Vector2_t2156229523 * L_141 = __this->get_address_of_guiTouchOffset_19();
		float L_142 = L_141->get_y_1();
		Vector2_t2156229523 * L_143 = __this->get_address_of_guiCenter_20();
		float L_144 = L_143->get_y_1();
		Vector2_t2156229523 * L_145 = __this->get_address_of_guiTouchOffset_19();
		float L_146 = L_145->get_y_1();
		L_137->set_y_1(((float)((float)((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_add((float)L_140, (float)L_142)), (float)L_144))/(float)L_146)));
	}

IL_0430:
	{
		Vector2_t2156229523 * L_147 = __this->get_address_of_position_9();
		float L_148 = L_147->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_149 = fabsf(L_148);
		V_6 = L_149;
		Vector2_t2156229523 * L_150 = __this->get_address_of_position_9();
		float L_151 = L_150->get_y_1();
		float L_152 = fabsf(L_151);
		V_7 = L_152;
		float L_153 = V_6;
		Vector2_t2156229523 * L_154 = __this->get_address_of_deadZone_7();
		float L_155 = L_154->get_x_0();
		if ((((float)L_153) >= ((float)L_155)))
		{
			goto IL_0478;
		}
	}
	{
		Vector2_t2156229523 * L_156 = __this->get_address_of_position_9();
		L_156->set_x_0((((float)((float)0))));
		goto IL_04bc;
	}

IL_0478:
	{
		bool L_157 = __this->get_normalize_8();
		if (!L_157)
		{
			goto IL_04bc;
		}
	}
	{
		Vector2_t2156229523 * L_158 = __this->get_address_of_position_9();
		Vector2_t2156229523 * L_159 = __this->get_address_of_position_9();
		float L_160 = L_159->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_161 = Mathf_Sign_m3457838305(NULL /*static, unused*/, L_160, /*hidden argument*/NULL);
		float L_162 = V_6;
		Vector2_t2156229523 * L_163 = __this->get_address_of_deadZone_7();
		float L_164 = L_163->get_x_0();
		Vector2_t2156229523 * L_165 = __this->get_address_of_deadZone_7();
		float L_166 = L_165->get_x_0();
		L_158->set_x_0(((float)((float)((float)il2cpp_codegen_multiply((float)L_161, (float)((float)il2cpp_codegen_subtract((float)L_162, (float)L_164))))/(float)((float)il2cpp_codegen_subtract((float)(((float)((float)1))), (float)L_166)))));
	}

IL_04bc:
	{
		float L_167 = V_7;
		Vector2_t2156229523 * L_168 = __this->get_address_of_deadZone_7();
		float L_169 = L_168->get_y_1();
		if ((((float)L_167) >= ((float)L_169)))
		{
			goto IL_04e0;
		}
	}
	{
		Vector2_t2156229523 * L_170 = __this->get_address_of_position_9();
		L_170->set_y_1((((float)((float)0))));
		goto IL_0524;
	}

IL_04e0:
	{
		bool L_171 = __this->get_normalize_8();
		if (!L_171)
		{
			goto IL_0524;
		}
	}
	{
		Vector2_t2156229523 * L_172 = __this->get_address_of_position_9();
		Vector2_t2156229523 * L_173 = __this->get_address_of_position_9();
		float L_174 = L_173->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_175 = Mathf_Sign_m3457838305(NULL /*static, unused*/, L_174, /*hidden argument*/NULL);
		float L_176 = V_7;
		Vector2_t2156229523 * L_177 = __this->get_address_of_deadZone_7();
		float L_178 = L_177->get_y_1();
		Vector2_t2156229523 * L_179 = __this->get_address_of_deadZone_7();
		float L_180 = L_179->get_y_1();
		L_172->set_y_1(((float)((float)((float)il2cpp_codegen_multiply((float)L_175, (float)((float)il2cpp_codegen_subtract((float)L_176, (float)L_178))))/(float)((float)il2cpp_codegen_subtract((float)(((float)((float)1))), (float)L_180)))));
	}

IL_0524:
	{
		return;
	}
}
// System.Void Joystick::Main()
extern "C"  void Joystick_Main_m1062395631 (Joystick_t9498292 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ObliqueNear::.ctor()
extern "C"  void ObliqueNear__ctor_m506255415 (ObliqueNear_t1763198098 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Matrix4x4 ObliqueNear::CalculateObliqueMatrix(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern "C"  Matrix4x4_t1817901843  ObliqueNear_CalculateObliqueMatrix_m4080248109 (ObliqueNear_t1763198098 * __this, Matrix4x4_t1817901843  ___projection0, Vector4_t3319028937  ___clipPlane1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObliqueNear_CalculateObliqueMatrix_m4080248109_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector4_t3319028937  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t3319028937  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Matrix4x4_t1817901843  L_0 = Matrix4x4_get_inverse_m1870592360((&___projection0), /*hidden argument*/NULL);
		float L_1 = (&___clipPlane1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Sign_m3457838305(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		float L_3 = (&___clipPlane1)->get_y_2();
		float L_4 = Mathf_Sign_m3457838305(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Vector4_t3319028937  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector4__ctor_m2498754347((&L_5), L_2, L_4, (1.0f), (1.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t1817901843_il2cpp_TypeInfo_var);
		Vector4_t3319028937  L_6 = Matrix4x4_op_Multiply_m1839501195(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Vector4_t3319028937  L_7 = ___clipPlane1;
		Vector4_t3319028937  L_8 = ___clipPlane1;
		Vector4_t3319028937  L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t3319028937_il2cpp_TypeInfo_var);
		float L_10 = Vector4_Dot_m3492158352(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		Vector4_t3319028937  L_11 = Vector4_op_Multiply_m213790997(NULL /*static, unused*/, L_7, ((float)((float)(2.0f)/(float)L_10)), /*hidden argument*/NULL);
		V_1 = L_11;
		float L_12 = (&V_1)->get_x_1();
		float L_13 = Matrix4x4_get_Item_m567451091((&___projection0), 3, /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1906605342((&___projection0), 2, ((float)il2cpp_codegen_subtract((float)L_12, (float)L_13)), /*hidden argument*/NULL);
		float L_14 = (&V_1)->get_y_2();
		float L_15 = Matrix4x4_get_Item_m567451091((&___projection0), 7, /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1906605342((&___projection0), 6, ((float)il2cpp_codegen_subtract((float)L_14, (float)L_15)), /*hidden argument*/NULL);
		float L_16 = (&V_1)->get_z_3();
		float L_17 = Matrix4x4_get_Item_m567451091((&___projection0), ((int32_t)11), /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1906605342((&___projection0), ((int32_t)10), ((float)il2cpp_codegen_subtract((float)L_16, (float)L_17)), /*hidden argument*/NULL);
		float L_18 = (&V_1)->get_w_4();
		float L_19 = Matrix4x4_get_Item_m567451091((&___projection0), ((int32_t)15), /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1906605342((&___projection0), ((int32_t)14), ((float)il2cpp_codegen_subtract((float)L_18, (float)L_19)), /*hidden argument*/NULL);
		Matrix4x4_t1817901843  L_20 = ___projection0;
		return L_20;
	}
}
// System.Void ObliqueNear::OnPreCull()
extern "C"  void ObliqueNear_OnPreCull_m2018901442 (ObliqueNear_t1763198098 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObliqueNear_OnPreCull_m2018901442_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_t1817901843  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t1817901843  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector4_t3319028937  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Camera_t4157153871 * L_0 = Component_GetComponent_TisCamera_t4157153871_m1557787507(__this, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var);
		Matrix4x4_t1817901843  L_1 = Camera_get_projectionMatrix_m667780853(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Camera_t4157153871 * L_2 = Component_GetComponent_TisCamera_t4157153871_m1557787507(__this, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var);
		Matrix4x4_t1817901843  L_3 = Camera_get_worldToCameraMatrix_m22661425(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Transform_t3600365921 * L_4 = __this->get_plane_2();
		Vector3_t3722313464  L_5 = Transform_get_position_m36019626(L_4, /*hidden argument*/NULL);
		Vector3_t3722313464  L_6 = Matrix4x4_MultiplyPoint_m1575665487((&V_1), L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_7 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_8 = Vector3_op_UnaryNegation_m1951478815(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Vector3_t3722313464  L_9 = Matrix4x4_MultiplyVector_m3808798942((&V_1), L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		Vector3_Normalize_m914904454((&V_3), /*hidden argument*/NULL);
		Vector3_t3722313464  L_10 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t3319028937_il2cpp_TypeInfo_var);
		Vector4_t3319028937  L_11 = Vector4_op_Implicit_m2966035112(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_4 = L_11;
		Vector3_t3722313464  L_12 = V_3;
		Vector3_t3722313464  L_13 = V_2;
		float L_14 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		(&V_4)->set_w_4(((-L_14)));
		Camera_t4157153871 * L_15 = Component_GetComponent_TisCamera_t4157153871_m1557787507(__this, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var);
		Matrix4x4_t1817901843  L_16 = V_0;
		Vector4_t3319028937  L_17 = V_4;
		Matrix4x4_t1817901843  L_18 = VirtFuncInvoker2< Matrix4x4_t1817901843 , Matrix4x4_t1817901843 , Vector4_t3319028937  >::Invoke(4 /* UnityEngine.Matrix4x4 ObliqueNear::CalculateObliqueMatrix(UnityEngine.Matrix4x4,UnityEngine.Vector4) */, __this, L_16, L_17);
		Camera_set_projectionMatrix_m3293177686(L_15, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ObliqueNear::Main()
extern "C"  void ObliqueNear_Main_m3023193363 (ObliqueNear_t1763198098 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayerRelativeControl::.ctor()
extern "C"  void PlayerRelativeControl__ctor_m2105639178 (PlayerRelativeControl_t1056195335 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		__this->set_forwardSpeed_5((((float)((float)4))));
		__this->set_backwardSpeed_6((((float)((float)1))));
		__this->set_sidestepSpeed_7((((float)((float)1))));
		__this->set_jumpSpeed_8((((float)((float)8))));
		__this->set_inAirMultiplier_9((0.25f));
		Vector2_t2156229523  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m3970636864((&L_0), (((float)((float)((int32_t)50)))), (((float)((float)((int32_t)25)))), /*hidden argument*/NULL);
		__this->set_rotationSpeed_10(L_0);
		return;
	}
}
// System.Void PlayerRelativeControl::Start()
extern "C"  void PlayerRelativeControl_Start_m2946113978 (PlayerRelativeControl_t1056195335 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerRelativeControl_Start_m2946113978_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	{
		RuntimeTypeHandle_t3027515415  L_0 = { reinterpret_cast<intptr_t> (Transform_t3600365921_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Component_t1923634451 * L_2 = Component_GetComponent_m886226392(__this, L_1, /*hidden argument*/NULL);
		__this->set_thisTransform_11(((Transform_t3600365921 *)CastclassClass((RuntimeObject*)L_2, Transform_t3600365921_il2cpp_TypeInfo_var)));
		RuntimeTypeHandle_t3027515415  L_3 = { reinterpret_cast<intptr_t> (CharacterController_t1138636865_0_0_0_var) };
		Type_t * L_4 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Component_t1923634451 * L_5 = Component_GetComponent_m886226392(__this, L_4, /*hidden argument*/NULL);
		__this->set_character_12(((CharacterController_t1138636865 *)CastclassSealed((RuntimeObject*)L_5, CharacterController_t1138636865_il2cpp_TypeInfo_var)));
		GameObject_t1113636619 * L_6 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral4038994995, /*hidden argument*/NULL);
		V_0 = L_6;
		GameObject_t1113636619 * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0062;
		}
	}
	{
		Transform_t3600365921 * L_9 = __this->get_thisTransform_11();
		GameObject_t1113636619 * L_10 = V_0;
		Transform_t3600365921 * L_11 = GameObject_get_transform_m1369836730(L_10, /*hidden argument*/NULL);
		Vector3_t3722313464  L_12 = Transform_get_position_m36019626(L_11, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_9, L_12, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Void PlayerRelativeControl::OnEndGame()
extern "C"  void PlayerRelativeControl_OnEndGame_m3922724000 (PlayerRelativeControl_t1056195335 * __this, const RuntimeMethod* method)
{
	{
		Joystick_t9498292 * L_0 = __this->get_moveJoystick_2();
		VirtActionInvoker0::Invoke(5 /* System.Void Joystick::Disable() */, L_0);
		Joystick_t9498292 * L_1 = __this->get_rotateJoystick_3();
		VirtActionInvoker0::Invoke(5 /* System.Void Joystick::Disable() */, L_1);
		Behaviour_set_enabled_m20417929(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerRelativeControl::Update()
extern "C"  void PlayerRelativeControl_Update_m1496946618 (PlayerRelativeControl_t1056195335 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerRelativeControl_Update_m1496946618_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2156229523  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2156229523  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		Transform_t3600365921 * L_0 = __this->get_thisTransform_11();
		Joystick_t9498292 * L_1 = __this->get_moveJoystick_2();
		Vector2_t2156229523 * L_2 = L_1->get_address_of_position_9();
		float L_3 = L_2->get_x_0();
		Joystick_t9498292 * L_4 = __this->get_moveJoystick_2();
		Vector2_t2156229523 * L_5 = L_4->get_address_of_position_9();
		float L_6 = L_5->get_y_1();
		Vector3_t3722313464  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m3353183577((&L_7), L_3, (((float)((float)0))), L_6, /*hidden argument*/NULL);
		Vector3_t3722313464  L_8 = Transform_TransformDirection_m3784028109(L_0, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		(&V_0)->set_y_2((((float)((float)0))));
		Vector3_Normalize_m914904454((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_9 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_9;
		Joystick_t9498292 * L_10 = __this->get_moveJoystick_2();
		Vector2_t2156229523 * L_11 = L_10->get_address_of_position_9();
		float L_12 = L_11->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_13 = fabsf(L_12);
		Joystick_t9498292 * L_14 = __this->get_moveJoystick_2();
		Vector2_t2156229523 * L_15 = L_14->get_address_of_position_9();
		float L_16 = L_15->get_y_1();
		float L_17 = fabsf(L_16);
		Vector2_t2156229523  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector2__ctor_m3970636864((&L_18), L_13, L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		float L_19 = (&V_2)->get_y_1();
		float L_20 = (&V_2)->get_x_0();
		if ((((float)L_19) <= ((float)L_20)))
		{
			goto IL_00f4;
		}
	}
	{
		Joystick_t9498292 * L_21 = __this->get_moveJoystick_2();
		Vector2_t2156229523 * L_22 = L_21->get_address_of_position_9();
		float L_23 = L_22->get_y_1();
		if ((((float)L_23) <= ((float)(((float)((float)0))))))
		{
			goto IL_00bd;
		}
	}
	{
		Vector3_t3722313464  L_24 = V_0;
		float L_25 = __this->get_forwardSpeed_5();
		float L_26 = (&V_2)->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_27 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_24, ((float)il2cpp_codegen_multiply((float)L_25, (float)L_26)), /*hidden argument*/NULL);
		V_0 = L_27;
		goto IL_00ef;
	}

IL_00bd:
	{
		Vector3_t3722313464  L_28 = V_0;
		float L_29 = __this->get_backwardSpeed_6();
		float L_30 = (&V_2)->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_31 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_28, ((float)il2cpp_codegen_multiply((float)L_29, (float)L_30)), /*hidden argument*/NULL);
		V_0 = L_31;
		Joystick_t9498292 * L_32 = __this->get_moveJoystick_2();
		Vector2_t2156229523 * L_33 = L_32->get_address_of_position_9();
		float L_34 = L_33->get_y_1();
		(&V_1)->set_z_3(((float)il2cpp_codegen_multiply((float)L_34, (float)(0.75f))));
	}

IL_00ef:
	{
		goto IL_0127;
	}

IL_00f4:
	{
		Vector3_t3722313464  L_35 = V_0;
		float L_36 = __this->get_sidestepSpeed_7();
		float L_37 = (&V_2)->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_38 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_35, ((float)il2cpp_codegen_multiply((float)L_36, (float)L_37)), /*hidden argument*/NULL);
		V_0 = L_38;
		Joystick_t9498292 * L_39 = __this->get_moveJoystick_2();
		Vector2_t2156229523 * L_40 = L_39->get_address_of_position_9();
		float L_41 = L_40->get_x_0();
		(&V_1)->set_x_1(((float)il2cpp_codegen_multiply((float)((-L_41)), (float)(0.5f))));
	}

IL_0127:
	{
		CharacterController_t1138636865 * L_42 = __this->get_character_12();
		bool L_43 = CharacterController_get_isGrounded_m1151930607(L_42, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_016f;
		}
	}
	{
		Joystick_t9498292 * L_44 = __this->get_rotateJoystick_3();
		int32_t L_45 = L_44->get_tapCount_10();
		if ((!(((uint32_t)L_45) == ((uint32_t)2))))
		{
			goto IL_016a;
		}
	}
	{
		CharacterController_t1138636865 * L_46 = __this->get_character_12();
		Vector3_t3722313464  L_47 = CharacterController_get_velocity_m3517335080(L_46, /*hidden argument*/NULL);
		__this->set_velocity_14(L_47);
		Vector3_t3722313464 * L_48 = __this->get_address_of_velocity_14();
		float L_49 = __this->get_jumpSpeed_8();
		L_48->set_y_2(L_49);
	}

IL_016a:
	{
		goto IL_01d8;
	}

IL_016f:
	{
		Vector3_t3722313464 * L_50 = __this->get_address_of_velocity_14();
		Vector3_t3722313464 * L_51 = __this->get_address_of_velocity_14();
		float L_52 = L_51->get_y_2();
		Vector3_t3722313464  L_53 = Physics_get_gravity_m2660066594(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = L_53;
		float L_54 = (&V_5)->get_y_2();
		float L_55 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_50->set_y_2(((float)il2cpp_codegen_add((float)L_52, (float)((float)il2cpp_codegen_multiply((float)L_54, (float)L_55)))));
		float L_56 = __this->get_jumpSpeed_8();
		(&V_1)->set_z_3(((float)il2cpp_codegen_multiply((float)((-L_56)), (float)(0.25f))));
		float L_57 = (&V_0)->get_x_1();
		float L_58 = __this->get_inAirMultiplier_9();
		(&V_0)->set_x_1(((float)il2cpp_codegen_multiply((float)L_57, (float)L_58)));
		float L_59 = (&V_0)->get_z_3();
		float L_60 = __this->get_inAirMultiplier_9();
		(&V_0)->set_z_3(((float)il2cpp_codegen_multiply((float)L_59, (float)L_60)));
	}

IL_01d8:
	{
		Vector3_t3722313464  L_61 = V_0;
		Vector3_t3722313464  L_62 = __this->get_velocity_14();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_63 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_61, L_62, /*hidden argument*/NULL);
		V_0 = L_63;
		Vector3_t3722313464  L_64 = V_0;
		Vector3_t3722313464  L_65 = Physics_get_gravity_m2660066594(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_66 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_64, L_65, /*hidden argument*/NULL);
		V_0 = L_66;
		Vector3_t3722313464  L_67 = V_0;
		float L_68 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_69 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_67, L_68, /*hidden argument*/NULL);
		V_0 = L_69;
		CharacterController_t1138636865 * L_70 = __this->get_character_12();
		Vector3_t3722313464  L_71 = V_0;
		CharacterController_Move_m1547317252(L_70, L_71, /*hidden argument*/NULL);
		CharacterController_t1138636865 * L_72 = __this->get_character_12();
		bool L_73 = CharacterController_get_isGrounded_m1151930607(L_72, /*hidden argument*/NULL);
		if (!L_73)
		{
			goto IL_0225;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_74 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_velocity_14(L_74);
	}

IL_0225:
	{
		Transform_t3600365921 * L_75 = __this->get_cameraPivot_4();
		Vector3_t3722313464  L_76 = Transform_get_localPosition_m4234289348(L_75, /*hidden argument*/NULL);
		V_3 = L_76;
		float L_77 = (&V_3)->get_x_1();
		float L_78 = (&V_1)->get_x_1();
		Vector3_t3722313464 * L_79 = __this->get_address_of_cameraVelocity_13();
		float* L_80 = L_79->get_address_of_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_81 = Mathf_SmoothDamp_m3171073017(NULL /*static, unused*/, L_77, L_78, L_80, (0.3f), /*hidden argument*/NULL);
		(&V_3)->set_x_1(L_81);
		float L_82 = (&V_3)->get_z_3();
		float L_83 = (&V_1)->get_z_3();
		Vector3_t3722313464 * L_84 = __this->get_address_of_cameraVelocity_13();
		float* L_85 = L_84->get_address_of_z_3();
		float L_86 = Mathf_SmoothDamp_m3171073017(NULL /*static, unused*/, L_82, L_83, L_85, (0.5f), /*hidden argument*/NULL);
		(&V_3)->set_z_3(L_86);
		Transform_t3600365921 * L_87 = __this->get_cameraPivot_4();
		Vector3_t3722313464  L_88 = V_3;
		Transform_set_localPosition_m4128471975(L_87, L_88, /*hidden argument*/NULL);
		CharacterController_t1138636865 * L_89 = __this->get_character_12();
		bool L_90 = CharacterController_get_isGrounded_m1151930607(L_89, /*hidden argument*/NULL);
		if (!L_90)
		{
			goto IL_031d;
		}
	}
	{
		Joystick_t9498292 * L_91 = __this->get_rotateJoystick_3();
		Vector2_t2156229523  L_92 = L_91->get_position_9();
		V_4 = L_92;
		float L_93 = (&V_4)->get_x_0();
		Vector2_t2156229523 * L_94 = __this->get_address_of_rotationSpeed_10();
		float L_95 = L_94->get_x_0();
		(&V_4)->set_x_0(((float)il2cpp_codegen_multiply((float)L_93, (float)L_95)));
		float L_96 = (&V_4)->get_y_1();
		Vector2_t2156229523 * L_97 = __this->get_address_of_rotationSpeed_10();
		float L_98 = L_97->get_y_1();
		(&V_4)->set_y_1(((float)il2cpp_codegen_multiply((float)L_96, (float)L_98)));
		Vector2_t2156229523  L_99 = V_4;
		float L_100 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_101 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_99, L_100, /*hidden argument*/NULL);
		V_4 = L_101;
		Transform_t3600365921 * L_102 = __this->get_thisTransform_11();
		float L_103 = (&V_4)->get_x_0();
		Transform_Rotate_m1660364534(L_102, (((float)((float)0))), L_103, (((float)((float)0))), 0, /*hidden argument*/NULL);
		Transform_t3600365921 * L_104 = __this->get_cameraPivot_4();
		float L_105 = (&V_4)->get_y_1();
		Transform_Rotate_m3172098886(L_104, L_105, (((float)((float)0))), (((float)((float)0))), /*hidden argument*/NULL);
	}

IL_031d:
	{
		return;
	}
}
// System.Void PlayerRelativeControl::Main()
extern "C"  void PlayerRelativeControl_Main_m1815613858 (PlayerRelativeControl_t1056195335 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RollABall::.ctor()
extern "C"  void RollABall__ctor_m476080020 (RollABall_t267000325 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RollABall__ctor_m476080020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_0 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_tilt_2(L_0);
		return;
	}
}
// System.Void RollABall::Start()
extern "C"  void RollABall_Start_m2109304508 (RollABall_t267000325 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RollABall_Start_m2109304508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Bounds_t2266837910  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Collider_t1773347010 * L_0 = Component_GetComponent_TisCollider_t1773347010_m4226749020(__this, /*hidden argument*/Component_GetComponent_TisCollider_t1773347010_m4226749020_RuntimeMethod_var);
		Bounds_t2266837910  L_1 = Collider_get_bounds_m2952418672(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector3_t3722313464  L_2 = Bounds_get_extents_m1304537151((&V_0), /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = (&V_1)->get_x_1();
		__this->set_circ_4(((float)il2cpp_codegen_multiply((float)(6.28318548f), (float)L_3)));
		Transform_t3600365921 * L_4 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_5 = Transform_get_position_m36019626(L_4, /*hidden argument*/NULL);
		__this->set_previousPosition_5(L_5);
		return;
	}
}
// System.Void RollABall::Update()
extern "C"  void RollABall_Update_m3453512694 (RollABall_t267000325 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RollABall_Update_m3453512694_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Vector3_t3722313464 * L_0 = __this->get_address_of_tilt_2();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_1 = Input_get_acceleration_m2528400370(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_y_2();
		L_0->set_x_1(((-L_2)));
		Vector3_t3722313464 * L_3 = __this->get_address_of_tilt_2();
		Vector3_t3722313464  L_4 = Input_get_acceleration_m2528400370(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = (&V_1)->get_x_1();
		L_3->set_z_3(L_5);
		Rigidbody_t3916780224 * L_6 = Component_GetComponent_TisRigidbody_t3916780224_m279685075(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t3916780224_m279685075_RuntimeMethod_var);
		Vector3_t3722313464  L_7 = __this->get_tilt_2();
		float L_8 = __this->get_speed_3();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_9 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		float L_10 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_11 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		Rigidbody_AddForce_m3395934484(L_6, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RollABall::LateUpdate()
extern "C"  void RollABall_LateUpdate_m166097054 (RollABall_t267000325 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RollABall_LateUpdate_m166097054_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		Vector3_t3722313464  L_2 = __this->get_previousPosition_5();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = (&V_0)->get_z_3();
		float L_5 = (&V_0)->get_x_1();
		Vector3_t3722313464  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m3353183577((&L_6), L_4, (((float)((float)0))), ((-L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		Transform_t3600365921 * L_7 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_8 = V_0;
		float L_9 = __this->get_circ_4();
		Vector3_t3722313464  L_10 = Vector3_op_Division_m510815599(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		Vector3_t3722313464  L_11 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_10, (((float)((float)((int32_t)360)))), /*hidden argument*/NULL);
		Transform_Rotate_m1886816857(L_7, L_11, 0, /*hidden argument*/NULL);
		Transform_t3600365921 * L_12 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_13 = Transform_get_position_m36019626(L_12, /*hidden argument*/NULL);
		__this->set_previousPosition_5(L_13);
		return;
	}
}
// System.Void RollABall::Main()
extern "C"  void RollABall_Main_m1869117213 (RollABall_t267000325 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RotationConstraint::.ctor()
extern "C"  void RotationConstraint__ctor_m818495778 (RotationConstraint_t2833302650 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RotationConstraint::Start()
extern "C"  void RotationConstraint_Start_m27902014 (RotationConstraint_t2833302650 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RotationConstraint_Start_m27902014_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Quaternion_t2301928331  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Quaternion_t2301928331  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		__this->set_thisTransform_5(L_0);
		int32_t L_1 = __this->get_axis_2();
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)0))))
		{
			goto IL_002a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Vector3_get_right_m1913784872(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_rotateAround_6(L_3);
		goto IL_0058;
	}

IL_002a:
	{
		int32_t L_4 = V_0;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0041;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_5 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_rotateAround_6(L_5);
		goto IL_0058;
	}

IL_0041:
	{
		int32_t L_6 = V_0;
		if ((!(((uint32_t)L_6) == ((uint32_t)2))))
		{
			goto IL_0058;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_7 = Vector3_get_forward_m3100859705(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_rotateAround_6(L_7);
		goto IL_0058;
	}

IL_0058:
	{
		Transform_t3600365921 * L_8 = __this->get_thisTransform_5();
		Quaternion_t2301928331  L_9 = Transform_get_localRotation_m3487911431(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		Vector3_t3722313464  L_10 = Quaternion_get_eulerAngles_m3425202016((&V_2), /*hidden argument*/NULL);
		V_3 = L_10;
		int32_t L_11 = __this->get_axis_2();
		float L_12 = Vector3_get_Item_m668685504((&V_3), L_11, /*hidden argument*/NULL);
		Vector3_t3722313464  L_13 = __this->get_rotateAround_6();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_14 = Quaternion_AngleAxis_m1767165696(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		Quaternion_t2301928331  L_15 = V_1;
		float L_16 = __this->get_min_3();
		Vector3_t3722313464  L_17 = __this->get_rotateAround_6();
		Quaternion_t2301928331  L_18 = Quaternion_AngleAxis_m1767165696(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_19 = Quaternion_op_Multiply_m1294064023(NULL /*static, unused*/, L_15, L_18, /*hidden argument*/NULL);
		__this->set_minQuaternion_7(L_19);
		Quaternion_t2301928331  L_20 = V_1;
		float L_21 = __this->get_max_4();
		Vector3_t3722313464  L_22 = __this->get_rotateAround_6();
		Quaternion_t2301928331  L_23 = Quaternion_AngleAxis_m1767165696(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_24 = Quaternion_op_Multiply_m1294064023(NULL /*static, unused*/, L_20, L_23, /*hidden argument*/NULL);
		__this->set_maxQuaternion_8(L_24);
		float L_25 = __this->get_max_4();
		float L_26 = __this->get_min_3();
		__this->set_range_9(((float)il2cpp_codegen_subtract((float)L_25, (float)L_26)));
		return;
	}
}
// System.Void RotationConstraint::LateUpdate()
extern "C"  void RotationConstraint_LateUpdate_m3448396124 (RotationConstraint_t2833302650 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RotationConstraint_LateUpdate_m3448396124_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t2301928331  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t2301928331  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t3722313464  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t3722313464  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		Transform_t3600365921 * L_0 = __this->get_thisTransform_5();
		Quaternion_t2301928331  L_1 = Transform_get_localRotation_m3487911431(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector3_t3722313464  L_2 = Quaternion_get_eulerAngles_m3425202016((&V_0), /*hidden argument*/NULL);
		V_5 = L_2;
		int32_t L_3 = __this->get_axis_2();
		float L_4 = Vector3_get_Item_m668685504((&V_5), L_3, /*hidden argument*/NULL);
		Vector3_t3722313464  L_5 = __this->get_rotateAround_6();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_6 = Quaternion_AngleAxis_m1767165696(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		Quaternion_t2301928331  L_7 = V_1;
		Quaternion_t2301928331  L_8 = __this->get_minQuaternion_7();
		float L_9 = Quaternion_Angle_m1586774072(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		Quaternion_t2301928331  L_10 = V_1;
		Quaternion_t2301928331  L_11 = __this->get_maxQuaternion_8();
		float L_12 = Quaternion_Angle_m1586774072(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		float L_13 = V_2;
		float L_14 = __this->get_range_9();
		if ((((float)L_13) > ((float)L_14)))
		{
			goto IL_0065;
		}
	}
	{
		float L_15 = V_3;
		float L_16 = __this->get_range_9();
		if ((((float)L_15) > ((float)L_16)))
		{
			goto IL_0065;
		}
	}
	{
		goto IL_00d5;
	}

IL_0065:
	{
		Vector3_t3722313464  L_17 = Quaternion_get_eulerAngles_m3425202016((&V_0), /*hidden argument*/NULL);
		V_4 = L_17;
		float L_18 = V_2;
		float L_19 = V_3;
		if ((((float)L_18) <= ((float)L_19)))
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_20 = __this->get_axis_2();
		Quaternion_t2301928331 * L_21 = __this->get_address_of_maxQuaternion_8();
		Vector3_t3722313464  L_22 = Quaternion_get_eulerAngles_m3425202016(L_21, /*hidden argument*/NULL);
		V_6 = L_22;
		int32_t L_23 = __this->get_axis_2();
		float L_24 = Vector3_get_Item_m668685504((&V_6), L_23, /*hidden argument*/NULL);
		Vector3_set_Item_m1772472431((&V_4), L_20, L_24, /*hidden argument*/NULL);
		goto IL_00c8;
	}

IL_00a1:
	{
		int32_t L_25 = __this->get_axis_2();
		Quaternion_t2301928331 * L_26 = __this->get_address_of_minQuaternion_7();
		Vector3_t3722313464  L_27 = Quaternion_get_eulerAngles_m3425202016(L_26, /*hidden argument*/NULL);
		V_7 = L_27;
		int32_t L_28 = __this->get_axis_2();
		float L_29 = Vector3_get_Item_m668685504((&V_7), L_28, /*hidden argument*/NULL);
		Vector3_set_Item_m1772472431((&V_4), L_25, L_29, /*hidden argument*/NULL);
	}

IL_00c8:
	{
		Transform_t3600365921 * L_30 = __this->get_thisTransform_5();
		Vector3_t3722313464  L_31 = V_4;
		Transform_set_localEulerAngles_m4202601546(L_30, L_31, /*hidden argument*/NULL);
	}

IL_00d5:
	{
		return;
	}
}
// System.Void RotationConstraint::Main()
extern "C"  void RotationConstraint_Main_m1254073798 (RotationConstraint_t2833302650 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SidescrollControl::.ctor()
extern "C"  void SidescrollControl__ctor_m3765721613 (SidescrollControl_t463195584 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		__this->set_forwardSpeed_4((((float)((float)4))));
		__this->set_backwardSpeed_5((((float)((float)4))));
		__this->set_jumpSpeed_6((((float)((float)((int32_t)16)))));
		__this->set_inAirMultiplier_7((0.25f));
		__this->set_canJump_11((bool)1);
		return;
	}
}
// System.Void SidescrollControl::Start()
extern "C"  void SidescrollControl_Start_m3223018177 (SidescrollControl_t463195584 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SidescrollControl_Start_m3223018177_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	{
		RuntimeTypeHandle_t3027515415  L_0 = { reinterpret_cast<intptr_t> (Transform_t3600365921_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Component_t1923634451 * L_2 = Component_GetComponent_m886226392(__this, L_1, /*hidden argument*/NULL);
		__this->set_thisTransform_8(((Transform_t3600365921 *)CastclassClass((RuntimeObject*)L_2, Transform_t3600365921_il2cpp_TypeInfo_var)));
		RuntimeTypeHandle_t3027515415  L_3 = { reinterpret_cast<intptr_t> (CharacterController_t1138636865_0_0_0_var) };
		Type_t * L_4 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Component_t1923634451 * L_5 = Component_GetComponent_m886226392(__this, L_4, /*hidden argument*/NULL);
		__this->set_character_9(((CharacterController_t1138636865 *)CastclassSealed((RuntimeObject*)L_5, CharacterController_t1138636865_il2cpp_TypeInfo_var)));
		GameObject_t1113636619 * L_6 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral4038994995, /*hidden argument*/NULL);
		V_0 = L_6;
		GameObject_t1113636619 * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0062;
		}
	}
	{
		Transform_t3600365921 * L_9 = __this->get_thisTransform_8();
		GameObject_t1113636619 * L_10 = V_0;
		Transform_t3600365921 * L_11 = GameObject_get_transform_m1369836730(L_10, /*hidden argument*/NULL);
		Vector3_t3722313464  L_12 = Transform_get_position_m36019626(L_11, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_9, L_12, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// System.Void SidescrollControl::OnEndGame()
extern "C"  void SidescrollControl_OnEndGame_m3675724198 (SidescrollControl_t463195584 * __this, const RuntimeMethod* method)
{
	{
		Joystick_t9498292 * L_0 = __this->get_moveTouchPad_2();
		VirtActionInvoker0::Invoke(5 /* System.Void Joystick::Disable() */, L_0);
		Joystick_t9498292 * L_1 = __this->get_jumpTouchPad_3();
		VirtActionInvoker0::Invoke(5 /* System.Void Joystick::Disable() */, L_1);
		Behaviour_set_enabled_m20417929(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SidescrollControl::Update()
extern "C"  void SidescrollControl_Update_m550749 (SidescrollControl_t463195584 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SidescrollControl_Update_m550749_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	Joystick_t9498292 * V_2 = NULL;
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_0 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Joystick_t9498292 * L_1 = __this->get_moveTouchPad_2();
		Vector2_t2156229523 * L_2 = L_1->get_address_of_position_9();
		float L_3 = L_2->get_x_0();
		if ((((float)L_3) <= ((float)(((float)((float)0))))))
		{
			goto IL_0048;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_4 = Vector3_get_right_m1913784872(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = __this->get_forwardSpeed_4();
		Vector3_t3722313464  L_6 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		Joystick_t9498292 * L_7 = __this->get_moveTouchPad_2();
		Vector2_t2156229523 * L_8 = L_7->get_address_of_position_9();
		float L_9 = L_8->get_x_0();
		Vector3_t3722313464  L_10 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_6, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		goto IL_006e;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_11 = Vector3_get_right_m1913784872(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_12 = __this->get_backwardSpeed_5();
		Vector3_t3722313464  L_13 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		Joystick_t9498292 * L_14 = __this->get_moveTouchPad_2();
		Vector2_t2156229523 * L_15 = L_14->get_address_of_position_9();
		float L_16 = L_15->get_x_0();
		Vector3_t3722313464  L_17 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_13, L_16, /*hidden argument*/NULL);
		V_0 = L_17;
	}

IL_006e:
	{
		CharacterController_t1138636865 * L_18 = __this->get_character_9();
		bool L_19 = CharacterController_get_isGrounded_m1151930607(L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00e5;
		}
	}
	{
		V_1 = (bool)0;
		Joystick_t9498292 * L_20 = __this->get_jumpTouchPad_3();
		V_2 = L_20;
		Joystick_t9498292 * L_21 = V_2;
		bool L_22 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Joystick::IsFingerDown() */, L_21);
		if (L_22)
		{
			goto IL_0099;
		}
	}
	{
		__this->set_canJump_11((bool)1);
	}

IL_0099:
	{
		bool L_23 = __this->get_canJump_11();
		if (!L_23)
		{
			goto IL_00b8;
		}
	}
	{
		Joystick_t9498292 * L_24 = V_2;
		bool L_25 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Joystick::IsFingerDown() */, L_24);
		if (!L_25)
		{
			goto IL_00b8;
		}
	}
	{
		V_1 = (bool)1;
		__this->set_canJump_11((bool)0);
	}

IL_00b8:
	{
		bool L_26 = V_1;
		if (!L_26)
		{
			goto IL_00e0;
		}
	}
	{
		CharacterController_t1138636865 * L_27 = __this->get_character_9();
		Vector3_t3722313464  L_28 = CharacterController_get_velocity_m3517335080(L_27, /*hidden argument*/NULL);
		__this->set_velocity_10(L_28);
		Vector3_t3722313464 * L_29 = __this->get_address_of_velocity_10();
		float L_30 = __this->get_jumpSpeed_6();
		L_29->set_y_2(L_30);
	}

IL_00e0:
	{
		goto IL_0124;
	}

IL_00e5:
	{
		Vector3_t3722313464 * L_31 = __this->get_address_of_velocity_10();
		Vector3_t3722313464 * L_32 = __this->get_address_of_velocity_10();
		float L_33 = L_32->get_y_2();
		Vector3_t3722313464  L_34 = Physics_get_gravity_m2660066594(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_34;
		float L_35 = (&V_3)->get_y_2();
		float L_36 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_31->set_y_2(((float)il2cpp_codegen_add((float)L_33, (float)((float)il2cpp_codegen_multiply((float)L_35, (float)L_36)))));
		float L_37 = (&V_0)->get_x_1();
		float L_38 = __this->get_inAirMultiplier_7();
		(&V_0)->set_x_1(((float)il2cpp_codegen_multiply((float)L_37, (float)L_38)));
	}

IL_0124:
	{
		Vector3_t3722313464  L_39 = V_0;
		Vector3_t3722313464  L_40 = __this->get_velocity_10();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_41 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		V_0 = L_41;
		Vector3_t3722313464  L_42 = V_0;
		Vector3_t3722313464  L_43 = Physics_get_gravity_m2660066594(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_44 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_42, L_43, /*hidden argument*/NULL);
		V_0 = L_44;
		Vector3_t3722313464  L_45 = V_0;
		float L_46 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_47 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_45, L_46, /*hidden argument*/NULL);
		V_0 = L_47;
		CharacterController_t1138636865 * L_48 = __this->get_character_9();
		Vector3_t3722313464  L_49 = V_0;
		CharacterController_Move_m1547317252(L_48, L_49, /*hidden argument*/NULL);
		CharacterController_t1138636865 * L_50 = __this->get_character_9();
		bool L_51 = CharacterController_get_isGrounded_m1151930607(L_50, /*hidden argument*/NULL);
		if (!L_51)
		{
			goto IL_0171;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_52 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_velocity_10(L_52);
	}

IL_0171:
	{
		return;
	}
}
// System.Void SidescrollControl::Main()
extern "C"  void SidescrollControl_Main_m1014630907 (SidescrollControl_t463195584 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SmoothFollow2D::.ctor()
extern "C"  void SmoothFollow2D__ctor_m1669975939 (SmoothFollow2D_t2134035804 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		__this->set_smoothTime_3((0.3f));
		return;
	}
}
// System.Void SmoothFollow2D::Start()
extern "C"  void SmoothFollow2D_Start_m1655326509 (SmoothFollow2D_t2134035804 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		__this->set_thisTransform_4(L_0);
		return;
	}
}
// System.Void SmoothFollow2D::Update()
extern "C"  void SmoothFollow2D_Update_m3823838575 (SmoothFollow2D_t2134035804 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SmoothFollow2D_Update_m3823838575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	Vector3_t3722313464  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t3722313464  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t3722313464  V_9;
	memset(&V_9, 0, sizeof(V_9));
	float V_10 = 0.0f;
	Vector3_t3722313464  V_11;
	memset(&V_11, 0, sizeof(V_11));
	{
		Transform_t3600365921 * L_0 = __this->get_thisTransform_4();
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		V_4 = L_1;
		float L_2 = (&V_4)->get_x_1();
		Transform_t3600365921 * L_3 = __this->get_target_2();
		Vector3_t3722313464  L_4 = Transform_get_position_m36019626(L_3, /*hidden argument*/NULL);
		V_5 = L_4;
		float L_5 = (&V_5)->get_x_1();
		Vector2_t2156229523 * L_6 = __this->get_address_of_velocity_5();
		float* L_7 = L_6->get_address_of_x_0();
		float L_8 = __this->get_smoothTime_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_9 = Mathf_SmoothDamp_m3171073017(NULL /*static, unused*/, L_2, L_5, L_7, L_8, /*hidden argument*/NULL);
		float L_10 = L_9;
		V_0 = L_10;
		Transform_t3600365921 * L_11 = __this->get_thisTransform_4();
		Vector3_t3722313464  L_12 = Transform_get_position_m36019626(L_11, /*hidden argument*/NULL);
		Vector3_t3722313464  L_13 = L_12;
		V_1 = L_13;
		float L_14 = V_0;
		float L_15 = L_14;
		V_6 = L_15;
		(&V_1)->set_x_1(L_15);
		float L_16 = V_6;
		Transform_t3600365921 * L_17 = __this->get_thisTransform_4();
		Vector3_t3722313464  L_18 = V_1;
		Vector3_t3722313464  L_19 = L_18;
		V_7 = L_19;
		Transform_set_position_m3387557959(L_17, L_19, /*hidden argument*/NULL);
		Vector3_t3722313464  L_20 = V_7;
		Transform_t3600365921 * L_21 = __this->get_thisTransform_4();
		Vector3_t3722313464  L_22 = Transform_get_position_m36019626(L_21, /*hidden argument*/NULL);
		V_8 = L_22;
		float L_23 = (&V_8)->get_y_2();
		Transform_t3600365921 * L_24 = __this->get_target_2();
		Vector3_t3722313464  L_25 = Transform_get_position_m36019626(L_24, /*hidden argument*/NULL);
		V_9 = L_25;
		float L_26 = (&V_9)->get_y_2();
		Vector2_t2156229523 * L_27 = __this->get_address_of_velocity_5();
		float* L_28 = L_27->get_address_of_y_1();
		float L_29 = __this->get_smoothTime_3();
		float L_30 = Mathf_SmoothDamp_m3171073017(NULL /*static, unused*/, L_23, L_26, L_28, L_29, /*hidden argument*/NULL);
		float L_31 = L_30;
		V_2 = L_31;
		Transform_t3600365921 * L_32 = __this->get_thisTransform_4();
		Vector3_t3722313464  L_33 = Transform_get_position_m36019626(L_32, /*hidden argument*/NULL);
		Vector3_t3722313464  L_34 = L_33;
		V_3 = L_34;
		float L_35 = V_2;
		float L_36 = L_35;
		V_10 = L_36;
		(&V_3)->set_y_2(L_36);
		float L_37 = V_10;
		Transform_t3600365921 * L_38 = __this->get_thisTransform_4();
		Vector3_t3722313464  L_39 = V_3;
		Vector3_t3722313464  L_40 = L_39;
		V_11 = L_40;
		Transform_set_position_m3387557959(L_38, L_40, /*hidden argument*/NULL);
		Vector3_t3722313464  L_41 = V_11;
		return;
	}
}
// System.Void SmoothFollow2D::Main()
extern "C"  void SmoothFollow2D_Main_m3713815573 (SmoothFollow2D_t2134035804 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void tapcontrol::.ctor()
extern "C"  void tapcontrol__ctor_m1318957847 (tapcontrol_t522795363 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (tapcontrol__ctor_m1318957847_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		__this->set_inAirMultiplier_7((0.25f));
		__this->set_minimumDistanceToMove_8((1.0f));
		__this->set_minimumTimeUntilMove_9((0.25f));
		__this->set_rotateEpsilon_14((((float)((float)1))));
		__this->set_state_24(0);
		__this->set_fingerDown_25(((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)2)));
		__this->set_fingerDownPosition_26(((Vector2U5BU5D_t1457185986*)SZArrayNew(Vector2U5BU5D_t1457185986_il2cpp_TypeInfo_var, (uint32_t)2)));
		__this->set_fingerDownFrame_27(((Int32U5BU5D_t385246372*)SZArrayNew(Int32U5BU5D_t385246372_il2cpp_TypeInfo_var, (uint32_t)2)));
		return;
	}
}
// System.Void tapcontrol::Start()
extern "C"  void tapcontrol_Start_m3681805517 (tapcontrol_t522795363 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (tapcontrol_Start_m3681805517_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		__this->set_thisTransform_17(L_0);
		GameObject_t1113636619 * L_1 = __this->get_cameraObject_2();
		RuntimeTypeHandle_t3027515415  L_2 = { reinterpret_cast<intptr_t> (ZoomCamera_t1350885688_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Component_t1923634451 * L_4 = GameObject_GetComponent_m1027872079(L_1, L_3, /*hidden argument*/NULL);
		__this->set_zoomCamera_15(((ZoomCamera_t1350885688 *)CastclassClass((RuntimeObject*)L_4, ZoomCamera_t1350885688_il2cpp_TypeInfo_var)));
		GameObject_t1113636619 * L_5 = __this->get_cameraObject_2();
		Camera_t4157153871 * L_6 = GameObject_GetComponent_TisCamera_t4157153871_m3956151066(L_5, /*hidden argument*/GameObject_GetComponent_TisCamera_t4157153871_m3956151066_RuntimeMethod_var);
		__this->set_cam_16(L_6);
		RuntimeTypeHandle_t3027515415  L_7 = { reinterpret_cast<intptr_t> (CharacterController_t1138636865_0_0_0_var) };
		Type_t * L_8 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Component_t1923634451 * L_9 = Component_GetComponent_m886226392(__this, L_8, /*hidden argument*/NULL);
		__this->set_character_18(((CharacterController_t1138636865 *)CastclassSealed((RuntimeObject*)L_9, CharacterController_t1138636865_il2cpp_TypeInfo_var)));
		VirtActionInvoker0::Invoke(9 /* System.Void tapcontrol::ResetControlState() */, __this);
		GameObject_t1113636619 * L_10 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral4038994995, /*hidden argument*/NULL);
		V_0 = L_10;
		GameObject_t1113636619 * L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_008a;
		}
	}
	{
		Transform_t3600365921 * L_13 = __this->get_thisTransform_17();
		GameObject_t1113636619 * L_14 = V_0;
		Transform_t3600365921 * L_15 = GameObject_get_transform_m1369836730(L_14, /*hidden argument*/NULL);
		Vector3_t3722313464  L_16 = Transform_get_position_m36019626(L_15, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_13, L_16, /*hidden argument*/NULL);
	}

IL_008a:
	{
		return;
	}
}
// System.Void tapcontrol::OnEndGame()
extern "C"  void tapcontrol_OnEndGame_m2179410760 (tapcontrol_t522795363 * __this, const RuntimeMethod* method)
{
	{
		Behaviour_set_enabled_m20417929(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void tapcontrol::FaceMovementDirection()
extern "C"  void tapcontrol_FaceMovementDirection_m24279703 (tapcontrol_t522795363 * __this, const RuntimeMethod* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		CharacterController_t1138636865 * L_0 = __this->get_character_18();
		Vector3_t3722313464  L_1 = CharacterController_get_velocity_m3517335080(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		(&V_0)->set_y_2((((float)((float)0))));
		float L_2 = Vector3_get_magnitude_m27958459((&V_0), /*hidden argument*/NULL);
		if ((((float)L_2) <= ((float)(0.1f))))
		{
			goto IL_0038;
		}
	}
	{
		Transform_t3600365921 * L_3 = __this->get_thisTransform_17();
		Vector3_t3722313464  L_4 = Vector3_get_normalized_m2454957984((&V_0), /*hidden argument*/NULL);
		Transform_set_forward_m1840797198(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void tapcontrol::CameraControl(UnityEngine.Touch,UnityEngine.Touch)
extern "C"  void tapcontrol_CameraControl_m3642245434 (tapcontrol_t522795363 * __this, Touch_t1921856868  ___touch00, Touch_t1921856868  ___touch11, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (tapcontrol_CameraControl_m3642245434_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2156229523  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2156229523  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t3722313464  V_6;
	memset(&V_6, 0, sizeof(V_6));
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	Vector3_t3722313464  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t3722313464  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Vector2_t2156229523  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Vector2_t2156229523  V_15;
	memset(&V_15, 0, sizeof(V_15));
	{
		bool L_0 = __this->get_rotateEnabled_13();
		if (!L_0)
		{
			goto IL_0153;
		}
	}
	{
		int32_t L_1 = __this->get_state_24();
		if ((!(((uint32_t)L_1) == ((uint32_t)5))))
		{
			goto IL_0153;
		}
	}
	{
		Vector2_t2156229523  L_2 = Touch_get_position_m3109777936((&___touch11), /*hidden argument*/NULL);
		Vector2_t2156229523  L_3 = Touch_get_position_m3109777936((&___touch00), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_4 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Vector2_t2156229523  L_5 = V_0;
		float L_6 = Vector2_get_magnitude_m2752892833((&V_0), /*hidden argument*/NULL);
		Vector2_t2156229523  L_7 = Vector2_op_Division_m132623573(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		Vector2_t2156229523  L_8 = Touch_get_position_m3109777936((&___touch11), /*hidden argument*/NULL);
		Vector2_t2156229523  L_9 = Touch_get_deltaPosition_m2389653382((&___touch11), /*hidden argument*/NULL);
		Vector2_t2156229523  L_10 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		Vector2_t2156229523  L_11 = Touch_get_position_m3109777936((&___touch00), /*hidden argument*/NULL);
		Vector2_t2156229523  L_12 = Touch_get_deltaPosition_m2389653382((&___touch00), /*hidden argument*/NULL);
		Vector2_t2156229523  L_13 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		Vector2_t2156229523  L_14 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_10, L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		Vector2_t2156229523  L_15 = V_2;
		float L_16 = Vector2_get_magnitude_m2752892833((&V_2), /*hidden argument*/NULL);
		Vector2_t2156229523  L_17 = Vector2_op_Division_m132623573(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		V_3 = L_17;
		Vector2_t2156229523  L_18 = V_1;
		Vector2_t2156229523  L_19 = V_3;
		float L_20 = Vector2_Dot_m1554553447(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		V_4 = L_20;
		float L_21 = V_4;
		if ((((float)L_21) >= ((float)(((float)((float)1))))))
		{
			goto IL_014e;
		}
	}
	{
		float L_22 = (&V_0)->get_x_0();
		float L_23 = (&V_0)->get_y_1();
		Vector3_t3722313464  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector3__ctor_m1719387948((&L_24), L_22, L_23, /*hidden argument*/NULL);
		V_5 = L_24;
		float L_25 = (&V_2)->get_x_0();
		float L_26 = (&V_2)->get_y_1();
		Vector3_t3722313464  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Vector3__ctor_m1719387948((&L_27), L_25, L_26, /*hidden argument*/NULL);
		V_6 = L_27;
		Vector3_t3722313464  L_28 = V_5;
		Vector3_t3722313464  L_29 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_30 = Vector3_Cross_m418170344(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		V_12 = L_30;
		Vector3_t3722313464  L_31 = Vector3_get_normalized_m2454957984((&V_12), /*hidden argument*/NULL);
		V_13 = L_31;
		float L_32 = (&V_13)->get_z_3();
		V_7 = L_32;
		float L_33 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_34 = acosf(L_33);
		V_8 = L_34;
		float L_35 = __this->get_rotationTarget_21();
		float L_36 = V_8;
		float L_37 = V_7;
		__this->set_rotationTarget_21(((float)il2cpp_codegen_add((float)L_35, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_36, (float)(57.29578f))), (float)L_37)))));
		float L_38 = __this->get_rotationTarget_21();
		if ((((float)L_38) >= ((float)(((float)((float)0))))))
		{
			goto IL_012a;
		}
	}
	{
		float L_39 = __this->get_rotationTarget_21();
		__this->set_rotationTarget_21(((float)il2cpp_codegen_add((float)L_39, (float)(((float)((float)((int32_t)360)))))));
		goto IL_014e;
	}

IL_012a:
	{
		float L_40 = __this->get_rotationTarget_21();
		if ((((float)L_40) < ((float)(((float)((float)((int32_t)360)))))))
		{
			goto IL_014e;
		}
	}
	{
		float L_41 = __this->get_rotationTarget_21();
		__this->set_rotationTarget_21(((float)il2cpp_codegen_subtract((float)L_41, (float)(((float)((float)((int32_t)360)))))));
	}

IL_014e:
	{
		goto IL_0203;
	}

IL_0153:
	{
		bool L_42 = __this->get_zoomEnabled_10();
		if (!L_42)
		{
			goto IL_0203;
		}
	}
	{
		int32_t L_43 = __this->get_state_24();
		if ((!(((uint32_t)L_43) == ((uint32_t)4))))
		{
			goto IL_0203;
		}
	}
	{
		Vector2_t2156229523  L_44 = Touch_get_position_m3109777936((&___touch11), /*hidden argument*/NULL);
		Vector2_t2156229523  L_45 = Touch_get_position_m3109777936((&___touch00), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_46 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_44, L_45, /*hidden argument*/NULL);
		V_14 = L_46;
		float L_47 = Vector2_get_magnitude_m2752892833((&V_14), /*hidden argument*/NULL);
		V_9 = L_47;
		Vector2_t2156229523  L_48 = Touch_get_position_m3109777936((&___touch11), /*hidden argument*/NULL);
		Vector2_t2156229523  L_49 = Touch_get_deltaPosition_m2389653382((&___touch11), /*hidden argument*/NULL);
		Vector2_t2156229523  L_50 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_48, L_49, /*hidden argument*/NULL);
		Vector2_t2156229523  L_51 = Touch_get_position_m3109777936((&___touch00), /*hidden argument*/NULL);
		Vector2_t2156229523  L_52 = Touch_get_deltaPosition_m2389653382((&___touch00), /*hidden argument*/NULL);
		Vector2_t2156229523  L_53 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_51, L_52, /*hidden argument*/NULL);
		Vector2_t2156229523  L_54 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_50, L_53, /*hidden argument*/NULL);
		V_15 = L_54;
		float L_55 = Vector2_get_magnitude_m2752892833((&V_15), /*hidden argument*/NULL);
		V_10 = L_55;
		float L_56 = V_9;
		float L_57 = V_10;
		V_11 = ((float)il2cpp_codegen_subtract((float)L_56, (float)L_57));
		ZoomCamera_t1350885688 * L_58 = __this->get_zoomCamera_15();
		ZoomCamera_t1350885688 * L_59 = __this->get_zoomCamera_15();
		float L_60 = L_59->get_zoom_3();
		float L_61 = V_11;
		float L_62 = __this->get_zoomRate_12();
		float L_63 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_58->set_zoom_3(((float)il2cpp_codegen_add((float)L_60, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_61, (float)L_62)), (float)L_63)))));
	}

IL_0203:
	{
		return;
	}
}
// System.Void tapcontrol::CharacterControl()
extern "C"  void tapcontrol_CharacterControl_m768722118 (tapcontrol_t522795363 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (tapcontrol_CharacterControl_m768722118_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Touch_t1921856868  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Ray_t3785851493  V_2;
	memset(&V_2, 0, sizeof(V_2));
	RaycastHit_t1056001966  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	Vector2_t2156229523  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector2_t2156229523  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t3722313464  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t3722313464  V_10;
	memset(&V_10, 0, sizeof(V_10));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m3403849067(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0125;
		}
	}
	{
		int32_t L_2 = __this->get_state_24();
		if ((!(((uint32_t)L_2) == ((uint32_t)2))))
		{
			goto IL_0125;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Touch_t1921856868  L_3 = Input_GetTouch_m2192712756(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_3;
		CharacterController_t1138636865 * L_4 = __this->get_character_18();
		bool L_5 = CharacterController_get_isGrounded_m1151930607(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0073;
		}
	}
	{
		GUITexture_t951903601 * L_6 = __this->get_jumpButton_4();
		Vector2_t2156229523  L_7 = Touch_get_position_m3109777936((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_8 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		bool L_9 = GUIElement_HitTest_m3916646804(L_6, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0073;
		}
	}
	{
		CharacterController_t1138636865 * L_10 = __this->get_character_18();
		Vector3_t3722313464  L_11 = CharacterController_get_velocity_m3517335080(L_10, /*hidden argument*/NULL);
		__this->set_velocity_23(L_11);
		Vector3_t3722313464 * L_12 = __this->get_address_of_velocity_23();
		float L_13 = __this->get_jumpSpeed_6();
		L_12->set_y_2(L_13);
		goto IL_0125;
	}

IL_0073:
	{
		GUITexture_t951903601 * L_14 = __this->get_jumpButton_4();
		Vector2_t2156229523  L_15 = Touch_get_position_m3109777936((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_16 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		bool L_17 = GUIElement_HitTest_m3916646804(L_14, L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0125;
		}
	}
	{
		int32_t L_18 = Touch_get_phase_m214549210((&V_1), /*hidden argument*/NULL);
		if ((((int32_t)L_18) == ((int32_t)0)))
		{
			goto IL_0125;
		}
	}
	{
		Camera_t4157153871 * L_19 = __this->get_cam_16();
		Vector2_t2156229523  L_20 = Touch_get_position_m3109777936((&V_1), /*hidden argument*/NULL);
		V_7 = L_20;
		float L_21 = (&V_7)->get_x_0();
		Vector2_t2156229523  L_22 = Touch_get_position_m3109777936((&V_1), /*hidden argument*/NULL);
		V_8 = L_22;
		float L_23 = (&V_8)->get_y_1();
		Vector3_t3722313464  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector3__ctor_m1719387948((&L_24), L_21, L_23, /*hidden argument*/NULL);
		Ray_t3785851493  L_25 = Camera_ScreenPointToRay_m3764635188(L_19, L_24, /*hidden argument*/NULL);
		V_2 = L_25;
		il2cpp_codegen_initobj((&V_3), sizeof(RaycastHit_t1056001966 ));
		Ray_t3785851493  L_26 = V_2;
		bool L_27 = Physics_Raycast_m447436869(NULL /*static, unused*/, L_26, (&V_3), /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0125;
		}
	}
	{
		Transform_t3600365921 * L_28 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_29 = Transform_get_position_m36019626(L_28, /*hidden argument*/NULL);
		Vector3_t3722313464  L_30 = RaycastHit_get_point_m2236647085((&V_3), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_31 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		V_9 = L_31;
		float L_32 = Vector3_get_magnitude_m27958459((&V_9), /*hidden argument*/NULL);
		V_4 = L_32;
		float L_33 = V_4;
		float L_34 = __this->get_minimumDistanceToMove_8();
		if ((((float)L_33) <= ((float)L_34)))
		{
			goto IL_011e;
		}
	}
	{
		Vector3_t3722313464  L_35 = RaycastHit_get_point_m2236647085((&V_3), /*hidden argument*/NULL);
		__this->set_targetLocation_19(L_35);
	}

IL_011e:
	{
		__this->set_moving_20((bool)1);
	}

IL_0125:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_36 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = L_36;
		bool L_37 = __this->get_moving_20();
		if (!L_37)
		{
			goto IL_018a;
		}
	}
	{
		Vector3_t3722313464  L_38 = __this->get_targetLocation_19();
		Transform_t3600365921 * L_39 = __this->get_thisTransform_17();
		Vector3_t3722313464  L_40 = Transform_get_position_m36019626(L_39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_41 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_38, L_40, /*hidden argument*/NULL);
		V_5 = L_41;
		(&V_5)->set_y_2((((float)((float)0))));
		float L_42 = Vector3_get_magnitude_m27958459((&V_5), /*hidden argument*/NULL);
		V_6 = L_42;
		float L_43 = V_6;
		if ((((float)L_43) >= ((float)(((float)((float)1))))))
		{
			goto IL_0176;
		}
	}
	{
		__this->set_moving_20((bool)0);
		goto IL_018a;
	}

IL_0176:
	{
		Vector3_t3722313464  L_44 = Vector3_get_normalized_m2454957984((&V_5), /*hidden argument*/NULL);
		float L_45 = __this->get_speed_5();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_46 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_44, L_45, /*hidden argument*/NULL);
		V_5 = L_46;
	}

IL_018a:
	{
		CharacterController_t1138636865 * L_47 = __this->get_character_18();
		bool L_48 = CharacterController_get_isGrounded_m1151930607(L_47, /*hidden argument*/NULL);
		if (L_48)
		{
			goto IL_01ef;
		}
	}
	{
		Vector3_t3722313464 * L_49 = __this->get_address_of_velocity_23();
		Vector3_t3722313464 * L_50 = __this->get_address_of_velocity_23();
		float L_51 = L_50->get_y_2();
		Vector3_t3722313464  L_52 = Physics_get_gravity_m2660066594(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_10 = L_52;
		float L_53 = (&V_10)->get_y_2();
		float L_54 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_49->set_y_2(((float)il2cpp_codegen_add((float)L_51, (float)((float)il2cpp_codegen_multiply((float)L_53, (float)L_54)))));
		float L_55 = (&V_5)->get_x_1();
		float L_56 = __this->get_inAirMultiplier_7();
		(&V_5)->set_x_1(((float)il2cpp_codegen_multiply((float)L_55, (float)L_56)));
		float L_57 = (&V_5)->get_z_3();
		float L_58 = __this->get_inAirMultiplier_7();
		(&V_5)->set_z_3(((float)il2cpp_codegen_multiply((float)L_57, (float)L_58)));
	}

IL_01ef:
	{
		Vector3_t3722313464  L_59 = V_5;
		Vector3_t3722313464  L_60 = __this->get_velocity_23();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_61 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_59, L_60, /*hidden argument*/NULL);
		V_5 = L_61;
		Vector3_t3722313464  L_62 = V_5;
		Vector3_t3722313464  L_63 = Physics_get_gravity_m2660066594(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_64 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_62, L_63, /*hidden argument*/NULL);
		V_5 = L_64;
		Vector3_t3722313464  L_65 = V_5;
		float L_66 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_67 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_65, L_66, /*hidden argument*/NULL);
		V_5 = L_67;
		CharacterController_t1138636865 * L_68 = __this->get_character_18();
		Vector3_t3722313464  L_69 = V_5;
		CharacterController_Move_m1547317252(L_68, L_69, /*hidden argument*/NULL);
		CharacterController_t1138636865 * L_70 = __this->get_character_18();
		bool L_71 = CharacterController_get_isGrounded_m1151930607(L_70, /*hidden argument*/NULL);
		if (!L_71)
		{
			goto IL_0243;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_72 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_velocity_23(L_72);
	}

IL_0243:
	{
		VirtActionInvoker0::Invoke(6 /* System.Void tapcontrol::FaceMovementDirection() */, __this);
		return;
	}
}
// System.Void tapcontrol::ResetControlState()
extern "C"  void tapcontrol_ResetControlState_m2739952648 (tapcontrol_t522795363 * __this, const RuntimeMethod* method)
{
	{
		__this->set_state_24(0);
		Int32U5BU5D_t385246372* L_0 = __this->get_fingerDown_25();
		(L_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (int32_t)(-1));
		Int32U5BU5D_t385246372* L_1 = __this->get_fingerDown_25();
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (int32_t)(-1));
		return;
	}
}
// System.Void tapcontrol::Update()
extern "C"  void tapcontrol_Update_m502715398 (tapcontrol_t522795363 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (tapcontrol_Update_m502715398_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Touch_t1921856868  V_2;
	memset(&V_2, 0, sizeof(V_2));
	TouchU5BU5D_t1849554061* V_3 = NULL;
	Touch_t1921856868  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Touch_t1921856868  V_5;
	memset(&V_5, 0, sizeof(V_5));
	bool V_6 = false;
	bool V_7 = false;
	Vector2_t2156229523  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector2_t2156229523  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector2_t2156229523  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector2_t2156229523  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector2_t2156229523  V_12;
	memset(&V_12, 0, sizeof(V_12));
	float V_13 = 0.0f;
	float V_14 = 0.0f;
	float V_15 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m3403849067(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		VirtActionInvoker0::Invoke(9 /* System.Void tapcontrol::ResetControlState() */, __this);
		goto IL_047b;
	}

IL_0017:
	{
		il2cpp_codegen_initobj((&V_1), sizeof(int32_t));
		il2cpp_codegen_initobj((&V_2), sizeof(Touch_t1921856868 ));
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		TouchU5BU5D_t1849554061* L_2 = Input_get_touches_m1702694043(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_2;
		il2cpp_codegen_initobj((&V_4), sizeof(Touch_t1921856868 ));
		il2cpp_codegen_initobj((&V_5), sizeof(Touch_t1921856868 ));
		V_6 = (bool)0;
		V_7 = (bool)0;
		int32_t L_3 = __this->get_state_24();
		if ((!(((uint32_t)L_3) == ((uint32_t)0))))
		{
			goto IL_00d3;
		}
	}
	{
		V_1 = 0;
		goto IL_00cc;
	}

IL_0056:
	{
		TouchU5BU5D_t1849554061* L_4 = V_3;
		int32_t L_5 = V_1;
		V_2 = (*(Touch_t1921856868 *)((L_4)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_5))));
		int32_t L_6 = Touch_get_phase_m214549210((&V_2), /*hidden argument*/NULL);
		if ((((int32_t)L_6) == ((int32_t)3)))
		{
			goto IL_00c8;
		}
	}
	{
		int32_t L_7 = Touch_get_phase_m214549210((&V_2), /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)4)))
		{
			goto IL_00c8;
		}
	}
	{
		__this->set_state_24(1);
		float L_8 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_firstTouchTime_28(L_8);
		Int32U5BU5D_t385246372* L_9 = __this->get_fingerDown_25();
		int32_t L_10 = Touch_get_fingerId_m859576425((&V_2), /*hidden argument*/NULL);
		(L_9)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (int32_t)L_10);
		Vector2U5BU5D_t1457185986* L_11 = __this->get_fingerDownPosition_26();
		Vector2_t2156229523  L_12 = Touch_get_position_m3109777936((&V_2), /*hidden argument*/NULL);
		*(Vector2_t2156229523 *)((L_11)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(0))) = L_12;
		Int32U5BU5D_t385246372* L_13 = __this->get_fingerDownFrame_27();
		int32_t L_14 = Time_get_frameCount_m1220035214(NULL /*static, unused*/, /*hidden argument*/NULL);
		(L_13)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (int32_t)L_14);
		goto IL_00d3;
	}

IL_00c8:
	{
		int32_t L_15 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_00cc:
	{
		int32_t L_16 = V_1;
		int32_t L_17 = V_0;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0056;
		}
	}

IL_00d3:
	{
		int32_t L_18 = __this->get_state_24();
		if ((!(((uint32_t)L_18) == ((uint32_t)1))))
		{
			goto IL_01d5;
		}
	}
	{
		V_1 = 0;
		goto IL_01ce;
	}

IL_00e6:
	{
		TouchU5BU5D_t1849554061* L_19 = V_3;
		int32_t L_20 = V_1;
		V_2 = (*(Touch_t1921856868 *)((L_19)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_20))));
		int32_t L_21 = Touch_get_phase_m214549210((&V_2), /*hidden argument*/NULL);
		if ((((int32_t)L_21) == ((int32_t)4)))
		{
			goto IL_01ca;
		}
	}
	{
		int32_t L_22 = V_0;
		if ((((int32_t)L_22) < ((int32_t)2)))
		{
			goto IL_0160;
		}
	}
	{
		int32_t L_23 = Touch_get_fingerId_m859576425((&V_2), /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_24 = __this->get_fingerDown_25();
		int32_t L_25 = 0;
		int32_t L_26 = (L_24)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_25));
		if ((((int32_t)L_23) == ((int32_t)L_26)))
		{
			goto IL_0160;
		}
	}
	{
		__this->set_state_24(3);
		Int32U5BU5D_t385246372* L_27 = __this->get_fingerDown_25();
		int32_t L_28 = Touch_get_fingerId_m859576425((&V_2), /*hidden argument*/NULL);
		(L_27)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (int32_t)L_28);
		Vector2U5BU5D_t1457185986* L_29 = __this->get_fingerDownPosition_26();
		Vector2_t2156229523  L_30 = Touch_get_position_m3109777936((&V_2), /*hidden argument*/NULL);
		*(Vector2_t2156229523 *)((L_29)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(1))) = L_30;
		Int32U5BU5D_t385246372* L_31 = __this->get_fingerDownFrame_27();
		int32_t L_32 = Time_get_frameCount_m1220035214(NULL /*static, unused*/, /*hidden argument*/NULL);
		(L_31)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (int32_t)L_32);
		goto IL_01d5;
	}
	// Dead block : IL_015b: br IL_01ca

IL_0160:
	{
		int32_t L_33 = V_0;
		if ((!(((uint32_t)L_33) == ((uint32_t)1))))
		{
			goto IL_01ca;
		}
	}
	{
		Vector2_t2156229523  L_34 = Touch_get_position_m3109777936((&V_2), /*hidden argument*/NULL);
		Vector2U5BU5D_t1457185986* L_35 = __this->get_fingerDownPosition_26();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_36 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_34, (*(Vector2_t2156229523 *)((L_35)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/NULL);
		V_8 = L_36;
		int32_t L_37 = Touch_get_fingerId_m859576425((&V_2), /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_38 = __this->get_fingerDown_25();
		int32_t L_39 = 0;
		int32_t L_40 = (L_38)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_39));
		if ((!(((uint32_t)L_37) == ((uint32_t)L_40))))
		{
			goto IL_01ca;
		}
	}
	{
		float L_41 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_42 = __this->get_firstTouchTime_28();
		float L_43 = __this->get_minimumTimeUntilMove_9();
		if ((((float)L_41) > ((float)((float)il2cpp_codegen_add((float)L_42, (float)L_43)))))
		{
			goto IL_01be;
		}
	}
	{
		int32_t L_44 = Touch_get_phase_m214549210((&V_2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_44) == ((uint32_t)3))))
		{
			goto IL_01ca;
		}
	}

IL_01be:
	{
		__this->set_state_24(2);
		goto IL_01d5;
	}

IL_01ca:
	{
		int32_t L_45 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_45, (int32_t)1));
	}

IL_01ce:
	{
		int32_t L_46 = V_1;
		int32_t L_47 = V_0;
		if ((((int32_t)L_46) < ((int32_t)L_47)))
		{
			goto IL_00e6;
		}
	}

IL_01d5:
	{
		int32_t L_48 = __this->get_state_24();
		if ((!(((uint32_t)L_48) == ((uint32_t)3))))
		{
			goto IL_03c0;
		}
	}
	{
		V_1 = 0;
		goto IL_02d4;
	}

IL_01e8:
	{
		TouchU5BU5D_t1849554061* L_49 = V_3;
		int32_t L_50 = V_1;
		V_2 = (*(Touch_t1921856868 *)((L_49)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_50))));
		int32_t L_51 = Touch_get_phase_m214549210((&V_2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_51) == ((uint32_t)0))))
		{
			goto IL_0270;
		}
	}
	{
		int32_t L_52 = Touch_get_fingerId_m859576425((&V_2), /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_53 = __this->get_fingerDown_25();
		int32_t L_54 = 0;
		int32_t L_55 = (L_53)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_54));
		if ((!(((uint32_t)L_52) == ((uint32_t)L_55))))
		{
			goto IL_0233;
		}
	}
	{
		Int32U5BU5D_t385246372* L_56 = __this->get_fingerDownFrame_27();
		int32_t L_57 = 0;
		int32_t L_58 = (L_56)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_57));
		int32_t L_59 = Time_get_frameCount_m1220035214(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_58) == ((uint32_t)L_59))))
		{
			goto IL_0233;
		}
	}
	{
		Touch_t1921856868  L_60 = V_2;
		V_4 = L_60;
		V_6 = (bool)1;
		goto IL_0270;
	}

IL_0233:
	{
		int32_t L_61 = Touch_get_fingerId_m859576425((&V_2), /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_62 = __this->get_fingerDown_25();
		int32_t L_63 = 0;
		int32_t L_64 = (L_62)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_63));
		if ((((int32_t)L_61) == ((int32_t)L_64)))
		{
			goto IL_0270;
		}
	}
	{
		int32_t L_65 = Touch_get_fingerId_m859576425((&V_2), /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_66 = __this->get_fingerDown_25();
		int32_t L_67 = 1;
		int32_t L_68 = (L_66)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_67));
		if ((((int32_t)L_65) == ((int32_t)L_68)))
		{
			goto IL_0270;
		}
	}
	{
		Int32U5BU5D_t385246372* L_69 = __this->get_fingerDown_25();
		int32_t L_70 = Touch_get_fingerId_m859576425((&V_2), /*hidden argument*/NULL);
		(L_69)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (int32_t)L_70);
		Touch_t1921856868  L_71 = V_2;
		V_5 = L_71;
		V_7 = (bool)1;
	}

IL_0270:
	{
		int32_t L_72 = Touch_get_phase_m214549210((&V_2), /*hidden argument*/NULL);
		if ((((int32_t)L_72) == ((int32_t)1)))
		{
			goto IL_0297;
		}
	}
	{
		int32_t L_73 = Touch_get_phase_m214549210((&V_2), /*hidden argument*/NULL);
		if ((((int32_t)L_73) == ((int32_t)2)))
		{
			goto IL_0297;
		}
	}
	{
		int32_t L_74 = Touch_get_phase_m214549210((&V_2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_74) == ((uint32_t)3))))
		{
			goto IL_02d0;
		}
	}

IL_0297:
	{
		int32_t L_75 = Touch_get_fingerId_m859576425((&V_2), /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_76 = __this->get_fingerDown_25();
		int32_t L_77 = 0;
		int32_t L_78 = (L_76)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_77));
		if ((!(((uint32_t)L_75) == ((uint32_t)L_78))))
		{
			goto IL_02b6;
		}
	}
	{
		Touch_t1921856868  L_79 = V_2;
		V_4 = L_79;
		V_6 = (bool)1;
		goto IL_02d0;
	}

IL_02b6:
	{
		int32_t L_80 = Touch_get_fingerId_m859576425((&V_2), /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_81 = __this->get_fingerDown_25();
		int32_t L_82 = 1;
		int32_t L_83 = (L_81)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_82));
		if ((!(((uint32_t)L_80) == ((uint32_t)L_83))))
		{
			goto IL_02d0;
		}
	}
	{
		Touch_t1921856868  L_84 = V_2;
		V_5 = L_84;
		V_7 = (bool)1;
	}

IL_02d0:
	{
		int32_t L_85 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_85, (int32_t)1));
	}

IL_02d4:
	{
		int32_t L_86 = V_1;
		int32_t L_87 = V_0;
		if ((((int32_t)L_86) < ((int32_t)L_87)))
		{
			goto IL_01e8;
		}
	}
	{
		bool L_88 = V_6;
		if (!L_88)
		{
			goto IL_03b9;
		}
	}
	{
		bool L_89 = V_7;
		if (!L_89)
		{
			goto IL_03b4;
		}
	}
	{
		Vector2U5BU5D_t1457185986* L_90 = __this->get_fingerDownPosition_26();
		Vector2U5BU5D_t1457185986* L_91 = __this->get_fingerDownPosition_26();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_92 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, (*(Vector2_t2156229523 *)((L_90)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(1)))), (*(Vector2_t2156229523 *)((L_91)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/NULL);
		V_9 = L_92;
		Vector2_t2156229523  L_93 = Touch_get_position_m3109777936((&V_5), /*hidden argument*/NULL);
		Vector2_t2156229523  L_94 = Touch_get_position_m3109777936((&V_4), /*hidden argument*/NULL);
		Vector2_t2156229523  L_95 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_93, L_94, /*hidden argument*/NULL);
		V_10 = L_95;
		Vector2_t2156229523  L_96 = V_9;
		float L_97 = Vector2_get_magnitude_m2752892833((&V_9), /*hidden argument*/NULL);
		Vector2_t2156229523  L_98 = Vector2_op_Division_m132623573(NULL /*static, unused*/, L_96, L_97, /*hidden argument*/NULL);
		V_11 = L_98;
		Vector2_t2156229523  L_99 = V_10;
		float L_100 = Vector2_get_magnitude_m2752892833((&V_10), /*hidden argument*/NULL);
		Vector2_t2156229523  L_101 = Vector2_op_Division_m132623573(NULL /*static, unused*/, L_99, L_100, /*hidden argument*/NULL);
		V_12 = L_101;
		Vector2_t2156229523  L_102 = V_11;
		Vector2_t2156229523  L_103 = V_12;
		float L_104 = Vector2_Dot_m1554553447(NULL /*static, unused*/, L_102, L_103, /*hidden argument*/NULL);
		V_13 = L_104;
		float L_105 = V_13;
		if ((((float)L_105) >= ((float)(((float)((float)1))))))
		{
			goto IL_037e;
		}
	}
	{
		float L_106 = V_13;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_107 = acosf(L_106);
		V_14 = L_107;
		float L_108 = V_14;
		float L_109 = __this->get_rotateEpsilon_14();
		if ((((float)L_108) <= ((float)((float)il2cpp_codegen_multiply((float)L_109, (float)(0.0174532924f))))))
		{
			goto IL_037e;
		}
	}
	{
		__this->set_state_24(5);
	}

IL_037e:
	{
		int32_t L_110 = __this->get_state_24();
		if ((!(((uint32_t)L_110) == ((uint32_t)3))))
		{
			goto IL_03b4;
		}
	}
	{
		float L_111 = Vector2_get_magnitude_m2752892833((&V_9), /*hidden argument*/NULL);
		float L_112 = Vector2_get_magnitude_m2752892833((&V_10), /*hidden argument*/NULL);
		V_15 = ((float)il2cpp_codegen_subtract((float)L_111, (float)L_112));
		float L_113 = V_15;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_114 = fabsf(L_113);
		float L_115 = __this->get_zoomEpsilon_11();
		if ((((float)L_114) <= ((float)L_115)))
		{
			goto IL_03b4;
		}
	}
	{
		__this->set_state_24(4);
	}

IL_03b4:
	{
		goto IL_03c0;
	}

IL_03b9:
	{
		__this->set_state_24(6);
	}

IL_03c0:
	{
		int32_t L_116 = __this->get_state_24();
		if ((((int32_t)L_116) == ((int32_t)5)))
		{
			goto IL_03d8;
		}
	}
	{
		int32_t L_117 = __this->get_state_24();
		if ((!(((uint32_t)L_117) == ((uint32_t)4))))
		{
			goto IL_047b;
		}
	}

IL_03d8:
	{
		V_1 = 0;
		goto IL_0450;
	}

IL_03df:
	{
		TouchU5BU5D_t1849554061* L_118 = V_3;
		int32_t L_119 = V_1;
		V_2 = (*(Touch_t1921856868 *)((L_118)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_119))));
		int32_t L_120 = Touch_get_phase_m214549210((&V_2), /*hidden argument*/NULL);
		if ((((int32_t)L_120) == ((int32_t)1)))
		{
			goto IL_0413;
		}
	}
	{
		int32_t L_121 = Touch_get_phase_m214549210((&V_2), /*hidden argument*/NULL);
		if ((((int32_t)L_121) == ((int32_t)2)))
		{
			goto IL_0413;
		}
	}
	{
		int32_t L_122 = Touch_get_phase_m214549210((&V_2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_122) == ((uint32_t)3))))
		{
			goto IL_044c;
		}
	}

IL_0413:
	{
		int32_t L_123 = Touch_get_fingerId_m859576425((&V_2), /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_124 = __this->get_fingerDown_25();
		int32_t L_125 = 0;
		int32_t L_126 = (L_124)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_125));
		if ((!(((uint32_t)L_123) == ((uint32_t)L_126))))
		{
			goto IL_0432;
		}
	}
	{
		Touch_t1921856868  L_127 = V_2;
		V_4 = L_127;
		V_6 = (bool)1;
		goto IL_044c;
	}

IL_0432:
	{
		int32_t L_128 = Touch_get_fingerId_m859576425((&V_2), /*hidden argument*/NULL);
		Int32U5BU5D_t385246372* L_129 = __this->get_fingerDown_25();
		int32_t L_130 = 1;
		int32_t L_131 = (L_129)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_130));
		if ((!(((uint32_t)L_128) == ((uint32_t)L_131))))
		{
			goto IL_044c;
		}
	}
	{
		Touch_t1921856868  L_132 = V_2;
		V_5 = L_132;
		V_7 = (bool)1;
	}

IL_044c:
	{
		int32_t L_133 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_133, (int32_t)1));
	}

IL_0450:
	{
		int32_t L_134 = V_1;
		int32_t L_135 = V_0;
		if ((((int32_t)L_134) < ((int32_t)L_135)))
		{
			goto IL_03df;
		}
	}
	{
		bool L_136 = V_6;
		if (!L_136)
		{
			goto IL_0474;
		}
	}
	{
		bool L_137 = V_7;
		if (!L_137)
		{
			goto IL_046f;
		}
	}
	{
		Touch_t1921856868  L_138 = V_4;
		Touch_t1921856868  L_139 = V_5;
		VirtActionInvoker2< Touch_t1921856868 , Touch_t1921856868  >::Invoke(7 /* System.Void tapcontrol::CameraControl(UnityEngine.Touch,UnityEngine.Touch) */, __this, L_138, L_139);
	}

IL_046f:
	{
		goto IL_047b;
	}

IL_0474:
	{
		__this->set_state_24(6);
	}

IL_047b:
	{
		VirtActionInvoker0::Invoke(8 /* System.Void tapcontrol::CharacterControl() */, __this);
		return;
	}
}
// System.Void tapcontrol::LateUpdate()
extern "C"  void tapcontrol_LateUpdate_m1804980605 (tapcontrol_t522795363 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (tapcontrol_LateUpdate_m1804980605_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Transform_t3600365921 * L_0 = __this->get_cameraPivot_3();
		Vector3_t3722313464  L_1 = Transform_get_eulerAngles_m2743581774(L_0, /*hidden argument*/NULL);
		V_2 = L_1;
		float L_2 = (&V_2)->get_y_2();
		float L_3 = __this->get_rotationTarget_21();
		float* L_4 = __this->get_address_of_rotationVelocity_22();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_5 = Mathf_SmoothDampAngle_m1801528689(NULL /*static, unused*/, L_2, L_3, L_4, (0.3f), /*hidden argument*/NULL);
		float L_6 = L_5;
		V_0 = L_6;
		Transform_t3600365921 * L_7 = __this->get_cameraPivot_3();
		Vector3_t3722313464  L_8 = Transform_get_eulerAngles_m2743581774(L_7, /*hidden argument*/NULL);
		Vector3_t3722313464  L_9 = L_8;
		V_1 = L_9;
		float L_10 = V_0;
		float L_11 = L_10;
		V_3 = L_11;
		(&V_1)->set_y_2(L_11);
		float L_12 = V_3;
		Transform_t3600365921 * L_13 = __this->get_cameraPivot_3();
		Vector3_t3722313464  L_14 = V_1;
		Vector3_t3722313464  L_15 = L_14;
		V_4 = L_15;
		Transform_set_eulerAngles_m135219616(L_13, L_15, /*hidden argument*/NULL);
		Vector3_t3722313464  L_16 = V_4;
		return;
	}
}
// System.Void tapcontrol::Main()
extern "C"  void tapcontrol_Main_m2158237629 (tapcontrol_t522795363 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZoomCamera::.ctor()
extern "C"  void ZoomCamera__ctor_m350134178 (ZoomCamera_t1350885688 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		__this->set_zoomMin_4((((float)((float)((int32_t)-5)))));
		__this->set_zoomMax_5((((float)((float)5))));
		__this->set_seekTime_6((1.0f));
		return;
	}
}
// System.Void ZoomCamera::Start()
extern "C"  void ZoomCamera_Start_m2009484792 (ZoomCamera_t1350885688 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		__this->set_thisTransform_9(L_0);
		Transform_t3600365921 * L_1 = __this->get_thisTransform_9();
		Vector3_t3722313464  L_2 = Transform_get_localPosition_m4234289348(L_1, /*hidden argument*/NULL);
		__this->set_defaultLocalPosition_8(L_2);
		float L_3 = __this->get_zoom_3();
		__this->set_currentZoom_10(L_3);
		return;
	}
}
// System.Void ZoomCamera::Update()
extern "C"  void ZoomCamera_Update_m852922365 (ZoomCamera_t1350885688 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZoomCamera_Update_m852922365_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RaycastHit_t1056001966  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t3722313464  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		float L_0 = __this->get_zoom_3();
		float L_1 = __this->get_zoomMin_4();
		float L_2 = __this->get_zoomMax_5();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Clamp_m3350697880(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		__this->set_zoom_3(L_3);
		V_0 = ((int32_t)-261);
		il2cpp_codegen_initobj((&V_1), sizeof(RaycastHit_t1056001966 ));
		Transform_t3600365921 * L_4 = __this->get_origin_2();
		Vector3_t3722313464  L_5 = Transform_get_position_m36019626(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		Vector3_t3722313464  L_6 = __this->get_defaultLocalPosition_8();
		Transform_t3600365921 * L_7 = __this->get_thisTransform_9();
		Transform_t3600365921 * L_8 = Transform_get_parent_m835071599(L_7, /*hidden argument*/NULL);
		Transform_t3600365921 * L_9 = __this->get_thisTransform_9();
		Vector3_t3722313464  L_10 = Transform_get_forward_m747522392(L_9, /*hidden argument*/NULL);
		float L_11 = __this->get_zoom_3();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_12 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		Vector3_t3722313464  L_13 = Transform_InverseTransformDirection_m3843238577(L_8, L_12, /*hidden argument*/NULL);
		Vector3_t3722313464  L_14 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_6, L_13, /*hidden argument*/NULL);
		V_3 = L_14;
		Transform_t3600365921 * L_15 = __this->get_thisTransform_9();
		Transform_t3600365921 * L_16 = Transform_get_parent_m835071599(L_15, /*hidden argument*/NULL);
		Vector3_t3722313464  L_17 = V_3;
		Vector3_t3722313464  L_18 = Transform_TransformPoint_m226827784(L_16, L_17, /*hidden argument*/NULL);
		V_4 = L_18;
		Vector3_t3722313464  L_19 = V_2;
		Vector3_t3722313464  L_20 = V_4;
		int32_t L_21 = V_0;
		bool L_22 = Physics_Linecast_m2155187179(NULL /*static, unused*/, L_19, L_20, (&V_1), L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00db;
		}
	}
	{
		Vector3_t3722313464  L_23 = RaycastHit_get_point_m2236647085((&V_1), /*hidden argument*/NULL);
		Transform_t3600365921 * L_24 = __this->get_thisTransform_9();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_25 = Vector3_get_forward_m3100859705(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_26 = Transform_TransformDirection_m3784028109(L_24, L_25, /*hidden argument*/NULL);
		Vector3_t3722313464  L_27 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_23, L_26, /*hidden argument*/NULL);
		V_5 = L_27;
		Vector3_t3722313464  L_28 = V_5;
		Transform_t3600365921 * L_29 = __this->get_thisTransform_9();
		Transform_t3600365921 * L_30 = Transform_get_parent_m835071599(L_29, /*hidden argument*/NULL);
		Vector3_t3722313464  L_31 = __this->get_defaultLocalPosition_8();
		Vector3_t3722313464  L_32 = Transform_TransformPoint_m226827784(L_30, L_31, /*hidden argument*/NULL);
		Vector3_t3722313464  L_33 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_28, L_32, /*hidden argument*/NULL);
		V_6 = L_33;
		float L_34 = Vector3_get_magnitude_m27958459((&V_6), /*hidden argument*/NULL);
		__this->set_targetZoom_11(L_34);
		goto IL_00e7;
	}

IL_00db:
	{
		float L_35 = __this->get_zoom_3();
		__this->set_targetZoom_11(L_35);
	}

IL_00e7:
	{
		float L_36 = __this->get_targetZoom_11();
		float L_37 = __this->get_zoomMin_4();
		float L_38 = __this->get_zoomMax_5();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_39 = Mathf_Clamp_m3350697880(NULL /*static, unused*/, L_36, L_37, L_38, /*hidden argument*/NULL);
		__this->set_targetZoom_11(L_39);
		bool L_40 = __this->get_smoothZoomIn_7();
		if (L_40)
		{
			goto IL_0134;
		}
	}
	{
		float L_41 = __this->get_targetZoom_11();
		float L_42 = __this->get_currentZoom_10();
		if ((((float)((float)il2cpp_codegen_subtract((float)L_41, (float)L_42))) <= ((float)(((float)((float)0))))))
		{
			goto IL_0134;
		}
	}
	{
		float L_43 = __this->get_targetZoom_11();
		__this->set_currentZoom_10(L_43);
		goto IL_0157;
	}

IL_0134:
	{
		float L_44 = __this->get_currentZoom_10();
		float L_45 = __this->get_targetZoom_11();
		float* L_46 = __this->get_address_of_zoomVelocity_12();
		float L_47 = __this->get_seekTime_6();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_48 = Mathf_SmoothDamp_m3171073017(NULL /*static, unused*/, L_44, L_45, L_46, L_47, /*hidden argument*/NULL);
		__this->set_currentZoom_10(L_48);
	}

IL_0157:
	{
		Vector3_t3722313464  L_49 = __this->get_defaultLocalPosition_8();
		Transform_t3600365921 * L_50 = __this->get_thisTransform_9();
		Transform_t3600365921 * L_51 = Transform_get_parent_m835071599(L_50, /*hidden argument*/NULL);
		Transform_t3600365921 * L_52 = __this->get_thisTransform_9();
		Vector3_t3722313464  L_53 = Transform_get_forward_m747522392(L_52, /*hidden argument*/NULL);
		float L_54 = __this->get_currentZoom_10();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_55 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_53, L_54, /*hidden argument*/NULL);
		Vector3_t3722313464  L_56 = Transform_InverseTransformDirection_m3843238577(L_51, L_55, /*hidden argument*/NULL);
		Vector3_t3722313464  L_57 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_49, L_56, /*hidden argument*/NULL);
		V_3 = L_57;
		Transform_t3600365921 * L_58 = __this->get_thisTransform_9();
		Vector3_t3722313464  L_59 = V_3;
		Transform_set_localPosition_m4128471975(L_58, L_59, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZoomCamera::Main()
extern "C"  void ZoomCamera_Main_m809698374 (ZoomCamera_t1350885688 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
