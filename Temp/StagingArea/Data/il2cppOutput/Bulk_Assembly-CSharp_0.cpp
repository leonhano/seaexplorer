﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// Captain
struct Captain_t4252579700;
// HumanUnit
struct HumanUnit_t2125463837;
// SkillAbility/SkillAbilityCallBack
struct SkillAbilityCallBack_t2618462130;
// SkillAbility/SkillValue
struct SkillValue_t3350472883;
// SkillAbility
struct SkillAbility_t2516669324;
// ChatBubbleUI
struct ChatBubbleUI_t1508846992;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// WorldCanvasUI
struct WorldCanvasUI_t1156772134;
// UnityEngine.Object
struct Object_t631007953;
// RotateTowardCamera
struct RotateTowardCamera_t118585142;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.Font
struct Font_t1956802104;
// UnityEngine.Font[]
struct FontU5BU5D_t1399044585;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// UnityEngine.Behaviour
struct Behaviour_t1437897464;
// CreateButtonScript
struct CreateButtonScript_t92967038;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// UnityEngine.EventSystems.StandaloneInputModule
struct StandaloneInputModule_t2760469101;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.UI.GraphicRaycaster
struct GraphicRaycaster_t2999697109;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t48803504;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// DoneCameraMovement
struct DoneCameraMovement_t1316814223;
// EnemyPatrol
struct EnemyPatrol_t3346945880;
// Entity
struct Entity_t3391956725;
// GameCamera
struct GameCamera_t2564829307;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t777473367;
// GameCameraTarget
struct GameCameraTarget_t3267842245;
// GameScene
struct GameScene_t4110668666;
// Highlightable
struct Highlightable_t3139352672;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Renderer[]
struct RendererU5BU5D_t3210418286;
// HUDFPS
struct HUDFPS_t4241874542;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// HUDFPS/<FPS>c__Iterator0
struct U3CFPSU3Ec__Iterator0_t392322613;
// UnityEngine.GUISkin
struct GUISkin_t1244372282;
// UnityEngine.GUIStyle
struct GUIStyle_t3956901511;
// UnityEngine.GUIStyleState
struct GUIStyleState_t1397964415;
// UnityEngine.GUI/WindowFunction
struct WindowFunction_t3146511083;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// LevelExp
struct LevelExp_t2602383666;
// LagRotation
struct LagRotation_t3758629971;
// MainGUI
struct MainGUI_t2584036754;
// MinimapCamera
struct MinimapCamera_t1751400688;
// UnityEngine.Camera
struct Camera_t4157153871;
// PlayerShipBehavior
struct PlayerShipBehavior_t729873910;
// UnityEngine.Collider
struct Collider_t1773347010;
// RandomTerrainGenerator
struct RandomTerrainGenerator_t662438485;
// UnityEngine.Terrain
struct Terrain_t3055443660;
// UnityEngine.TerrainData
struct TerrainData_t657004131;
// System.Single[0...,0...]
struct SingleU5B0___U2C0___U5D_t1444911252;
// System.Single[0...,0...,0...]
struct SingleU5B0___U2C0___U2C0___U5D_t1444911253;
// RaycastMask
struct RaycastMask_t1362276734;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// SceneManager
struct SceneManager_t385596982;
// SellDefine
struct SellDefine_t4148876514;
// ShipBobble
struct ShipBobble_t3127994465;
// ShipCamera
struct ShipCamera_t1196952155;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// ShipController
struct ShipController_t2808759107;
// ShipUnit
struct ShipUnit_t4065899274;
// ShipOrbit
struct ShipOrbit_t50882478;
// ShipTouchToDestination
struct ShipTouchToDestination_t4292119180;
// ShipTouchToMove
struct ShipTouchToMove_t1387549923;
// ShowName
struct ShowName_t1001792941;
// UnityEngine.GUIContent
struct GUIContent_t3050628031;
// System.Collections.Generic.Dictionary`2<SkillAbility/SkillEnum,SkillAbility/SkillValue>
struct Dictionary_2_t437934597;
// System.Collections.Generic.Dictionary`2<SkillAbility/SkillEnum,System.Object>
struct Dictionary_2_t167567878;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Delegate
struct Delegate_t1188392813;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// TestGuiLayout
struct TestGuiLayout_t2138056437;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// System.Collections.Generic.List`1<UnityEngine.Rigidbody>
struct List_1_t1093887670;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t4234922487;
// UnityEngine.Object[]
struct ObjectU5BU5D_t1417781964;
// uGuiLayout
struct uGuiLayout_t2579011462;
// System.Collections.Generic.Stack`1<System.Single>
struct Stack_1_t2240656229;
// System.Collections.Generic.Stack`1<uGuiLayout/Mode>
struct Stack_1_t4010345780;
// UnityEngine.UI.HorizontalLayoutGroup
struct HorizontalLayoutGroup_t2586782146;
// UnityEngine.UI.VerticalLayoutGroup
struct VerticalLayoutGroup_t923838031;
// System.Collections.Generic.Stack`1<UnityEngine.RectTransform>
struct Stack_1_t253079184;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t3923495619;
// UIWindowBase
struct UIWindowBase_t957717849;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;
// Water
struct Water_t1083516957;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.Skybox
struct Skybox_t2662837510;
// UnityEngine.FlareLayer
struct FlareLayer_t1739223323;
// uGuiLayout/Mode[]
struct ModeU5BU5D_t612995752;
// UnityEngine.Rigidbody[]
struct RigidbodyU5BU5D_t3308997185;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.Collections.Hashtable/Slot[]
struct SlotU5BU5D_t2994659099;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Hashtable/HashKeys
struct HashKeys_t1568156503;
// System.Collections.Hashtable/HashValues
struct HashValues_t618387445;
// System.Collections.IHashCodeProvider
struct IHashCodeProvider_t267601189;
// System.Collections.IComparer
struct IComparer_t1540313114;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t1493878338;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_t107129948;
// UnityEngine.Transform[]
struct TransformU5BU5D_t807237628;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t964245573;
// SkillAbility/SkillEnum[]
struct SkillEnumU5BU5D_t2878962051;
// SkillAbility/SkillValue[]
struct SkillValueU5BU5D_t732829474;
// System.Collections.Generic.IEqualityComparer`1<SkillAbility/SkillEnum>
struct IEqualityComparer_1_t145247448;
// System.Collections.Generic.Dictionary`2/Transform`1<SkillAbility/SkillEnum,SkillAbility/SkillValue,System.Collections.DictionaryEntry>
struct Transform_1_t882294915;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Byte
struct Byte_t1134296376;
// System.Double
struct Double_t594665363;
// System.UInt16
struct UInt16_t2177724958;
// System.Void
struct Void_t1185182177;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t4150874583;
// System.Action`1<UnityEngine.Font>
struct Action_1_t2129269699;
// UnityEngine.Font/FontTextureRebuildCallback
struct FontTextureRebuildCallback_t2467502454;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// UnityEngine.RectOffset
struct RectOffset_t1369453676;
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t2383250302;
// UnityEngine.GUISettings
struct GUISettings_t1774757634;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t3742157810;
// UnityEngine.GUISkin/SkinChangedDelegate
struct SkinChangedDelegate_t1143955295;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t190067161;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t3309123499;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1258266594;
// System.Collections.Generic.Dictionary`2<System.Int32,Entity>
struct Dictionary_2_t2280670056;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>
struct List_1_t3491343620;
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t2019268878;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem>
struct List_1_t2475741330;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3903027533;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t3135238028;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t537414295;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t2331243652;
// UnityEngine.EventSystems.BaseInput
struct BaseInput_t3630163547;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t881764471;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Dictionary_2_t2696614423;
// UnityEngine.EventSystems.PointerInputModule/MouseState
struct MouseState_t384203932;
// System.Collections.Generic.List`1<UnityEngine.UI.Graphic>
struct List_1_t3132410353;
// System.Comparison`1<UnityEngine.UI.Graphic>
struct Comparison_1_t1435266790;
// UnityEngine.UI.FontData
struct FontData_t746620069;
// UnityEngine.TextGenerator
struct TextGenerator_t3211863866;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;

extern RuntimeClass* SkillAbilityCallBack_t2618462130_il2cpp_TypeInfo_var;
extern RuntimeClass* SkillValue_t3350472883_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Captain_ApplyLeadershipCallback_m1116414556_RuntimeMethod_var;
extern const uint32_t Captain_InitSkills_m4099947469_MetadataUsageId;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern RuntimeClass* Convert_t2465617642_il2cpp_TypeInfo_var;
extern const uint32_t Captain_ApplyLeadershipCallback_m1116414556_MetadataUsageId;
extern RuntimeClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t3319525431_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m706204246_RuntimeMethod_var;
extern const uint32_t ChatBubbleUI__ctor_m1094881035_MetadataUsageId;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* GameObject_t1113636619_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponent_TisWorldCanvasUI_t1156772134_m2038315626_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisWorldCanvasUI_t1156772134_m3564025873_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisRotateTowardCamera_t118585142_m2773773848_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisImage_t2670269651_m1594579417_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisRawImage_t3182918964_m629883703_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisText_t1901882714_m942857714_RuntimeMethod_var;
extern const RuntimeMethod* Resources_FindObjectsOfTypeAll_TisFont_t1956802104_m2894531458_RuntimeMethod_var;
extern String_t* _stringLiteral3306786237;
extern String_t* _stringLiteral2405869902;
extern String_t* _stringLiteral3814786466;
extern const uint32_t ChatBubbleUI_Awake_m3747317158_MetadataUsageId;
extern const uint32_t ChatBubbleUI_LimitCanvasUIsBasedOnSizeDelta_m526648713_MetadataUsageId;
extern const uint32_t ChatBubbleUI_AutoCanvasUIs_m213075858_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398_RuntimeMethod_var;
extern const uint32_t ChatBubbleUI_ResetCanvasUIs_m1868350676_MetadataUsageId;
extern RuntimeClass* Input_t1431474628_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1613539661;
extern const uint32_t ChatBubbleUI_PopChatUI_m2587343892_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Count_m2276455407_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m3346958548_RuntimeMethod_var;
extern const RuntimeMethod* List_1_RemoveAt_m815285786_RuntimeMethod_var;
extern const uint32_t ChatBubbleUI_PopChat_m2172760713_MetadataUsageId;
extern const uint32_t ChatBubbleUI_EnableChatBubbleUI_m3745119852_MetadataUsageId;
extern const uint32_t ChatBubbleUI_DestoryChatBubbleUI_m4189235868_MetadataUsageId;
extern const RuntimeMethod* List_1_Add_m1685793073_RuntimeMethod_var;
extern const uint32_t ChatBubbleUI_AddChatString_m115359011_MetadataUsageId;
extern const RuntimeType* EventSystem_t1003666588_0_0_0_var;
extern RuntimeClass* TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern RuntimeClass* CreateButtonScript_t92967038_il2cpp_TypeInfo_var;
extern RuntimeClass* UnityAction_t3245792599_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Object_FindObjectOfType_TisEventSystem_t1003666588_m1331729420_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisStandaloneInputModule_t2760469101_m1723984740_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisCanvas_t3310196443_m284883347_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisGraphicRaycaster_t2999697109_m1217468238_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisButton_t4055032469_m1106144321_RuntimeMethod_var;
extern const RuntimeMethod* CreateButtonScript_U3CStartU3Em__0_m530170949_RuntimeMethod_var;
extern const RuntimeMethod* CreateButtonScript_onBtnClicked_m2005882438_RuntimeMethod_var;
extern String_t* _stringLiteral3534642813;
extern String_t* _stringLiteral2323074440;
extern String_t* _stringLiteral1208991571;
extern String_t* _stringLiteral3987835886;
extern String_t* _stringLiteral1740661195;
extern const uint32_t CreateButtonScript_Start_m377316117_MetadataUsageId;
extern RuntimeClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3338840811;
extern const uint32_t CreateButtonScript_onBtnClicked_m2005882438_MetadataUsageId;
extern RuntimeClass* Single_t1397266774_il2cpp_TypeInfo_var;
extern const uint32_t CreateButtonScript_U3CStartU3Em__0_m530170949_MetadataUsageId;
extern String_t* _stringLiteral2261822918;
extern const uint32_t DoneCameraMovement_Awake_m430543615_MetadataUsageId;
extern RuntimeClass* Vector3U5BU5D_t1718750761_il2cpp_TypeInfo_var;
extern const uint32_t DoneCameraMovement_FixedUpdate_m2285240065_MetadataUsageId;
extern const uint32_t DoneCameraMovement_ViewingPosCheck_m2483040884_MetadataUsageId;
extern RuntimeClass* Quaternion_t2301928331_il2cpp_TypeInfo_var;
extern const uint32_t DoneCameraMovement_SmoothLookAt_m490920_MetadataUsageId;
extern String_t* _stringLiteral78692563;
extern const uint32_t EnemyPatrol_Start_m2074163177_MetadataUsageId;
extern const uint32_t EnemyPatrol_FixedUpdate_m293570722_MetadataUsageId;
extern const uint32_t EnemyPatrol_PatrolRaycast_m262090287_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral2917778654;
extern const uint32_t EnemyPatrol_TurnAround_m3579663584_MetadataUsageId;
extern String_t* _stringLiteral2705378347;
extern const uint32_t EnemyPatrol_ChasePlayer_m1683778077_MetadataUsageId;
extern RuntimeClass* GameCamera_t2564829307_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_get_Count_m3787308655_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m3022113929_RuntimeMethod_var;
extern const uint32_t GameCamera_get_target_m1194538123_MetadataUsageId;
extern const RuntimeMethod* List_1_Clear_m3082658015_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m4073477735_RuntimeMethod_var;
extern const uint32_t GameCamera_set_target_m2254130448_MetadataUsageId;
extern const RuntimeMethod* List_1_Remove_m4216972_RuntimeMethod_var;
extern const uint32_t GameCamera_AddTarget_m745210319_MetadataUsageId;
extern const uint32_t GameCamera_RemoveTarget_m3068264324_MetadataUsageId;
extern const uint32_t GameCamera_DetachFromParent_m1579485582_MetadataUsageId;
extern const uint32_t GameCamera_DetachFromParent_m564670588_MetadataUsageId;
extern const uint32_t GameCamera_Awake_m4052600003_MetadataUsageId;
extern const uint32_t GameCamera_OnDestroy_m72905221_MetadataUsageId;
extern const uint32_t GameCamera_LateUpdate_m3934829490_MetadataUsageId;
extern RuntimeClass* List_1_t777473367_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m2885667311_RuntimeMethod_var;
extern const uint32_t GameCamera__cctor_m2694908189_MetadataUsageId;
extern const uint32_t GameCameraTarget_Activate_m1421886454_MetadataUsageId;
extern String_t* _stringLiteral1768119585;
extern String_t* _stringLiteral1926007183;
extern const uint32_t GameScene__ctor_m3327038984_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisRenderer_t2627027031_m2651633905_RuntimeMethod_var;
extern const uint32_t Highlightable_Start_m867417398_MetadataUsageId;
extern const uint32_t Highlightable_Update_m828541039_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponentsInChildren_TisRenderer_t2627027031_m2275554434_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisHighlightable_t3139352672_m3233271036_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisHighlightable_t3139352672_m2047509732_RuntimeMethod_var;
extern const uint32_t Highlightable_SetAllChildrenHighlightable_m544031526_MetadataUsageId;
extern const uint32_t HUDFPS__ctor_m4116254002_MetadataUsageId;
extern RuntimeClass* U3CFPSU3Ec__Iterator0_t392322613_il2cpp_TypeInfo_var;
extern const uint32_t HUDFPS_FPS_m1656083209_MetadataUsageId;
extern RuntimeClass* GUI_t1624858472_il2cpp_TypeInfo_var;
extern RuntimeClass* GUIStyle_t3956901511_il2cpp_TypeInfo_var;
extern RuntimeClass* WindowFunction_t3146511083_il2cpp_TypeInfo_var;
extern const RuntimeMethod* HUDFPS_DoMyWindow_m1030748063_RuntimeMethod_var;
extern const uint32_t HUDFPS_OnGUI_m1014030805_MetadataUsageId;
extern String_t* _stringLiteral2787286266;
extern const uint32_t HUDFPS_DoMyWindow_m1030748063_MetadataUsageId;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern RuntimeClass* WaitForSeconds_t1699091251_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3452614586;
extern const uint32_t U3CFPSU3Ec__Iterator0_MoveNext_m3141457867_MetadataUsageId;
extern RuntimeClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern const uint32_t U3CFPSU3Ec__Iterator0_Reset_m487481608_MetadataUsageId;
extern RuntimeClass* LevelExp_t2602383666_il2cpp_TypeInfo_var;
extern const uint32_t HumanUnit__ctor_m1823562469_MetadataUsageId;
extern const uint32_t LagRotation_LateUpdate_m1081189382_MetadataUsageId;
extern RuntimeClass* SceneManager_t385596982_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponent_TisCamera_t4157153871_m3956151066_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var;
extern const uint32_t MinimapCamera_Update_m4231757957_MetadataUsageId;
extern String_t* _stringLiteral1212291584;
extern String_t* _stringLiteral3410314461;
extern String_t* _stringLiteral1119515214;
extern const uint32_t PlayerShipBehavior_OnTriggerEnter_m2658974142_MetadataUsageId;
extern const uint32_t PlayerShipBehavior_OnTriggerExit_m3177691883_MetadataUsageId;
extern const uint32_t PlayerShipBehavior_HighlightCollider_m195144836_MetadataUsageId;
extern String_t* _stringLiteral296396688;
extern String_t* _stringLiteral3076983745;
extern const uint32_t PlayerShipBehavior_InteractiveWithPort_m361013814_MetadataUsageId;
extern String_t* _stringLiteral3531824247;
extern String_t* _stringLiteral3840156928;
extern const uint32_t PlayerShipBehavior_FinishInteractiveWithPort_m354842222_MetadataUsageId;
extern String_t* _stringLiteral3443649515;
extern const uint32_t PlayerShipBehavior_InteractiveWithNPC_m2391546926_MetadataUsageId;
extern String_t* _stringLiteral1320622954;
extern const uint32_t PlayerShipBehavior_FinishInteractiveWithNPC_m3927383204_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisTerrain_t3055443660_m338788394_RuntimeMethod_var;
extern String_t* _stringLiteral4037192534;
extern const uint32_t RandomTerrainGenerator_OnGUI_m790361608_MetadataUsageId;
extern const uint32_t RandomTerrainGenerator_OnWizardCreate_m4115677761_MetadataUsageId;
extern RuntimeClass* SingleU5B0___U2C0___U5D_t1444911252_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral2954705631;
extern String_t* _stringLiteral3522134580;
extern const uint32_t RandomTerrainGenerator_GenerateTerrain_m199714022_MetadataUsageId;
extern const uint32_t RandomTerrainGenerator_UpdateTerrainTexture_m2921420409_MetadataUsageId;
extern const uint32_t RandomTerrainGenerator_CreateTerrainTextureWithHeight_m3848507478_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var;
extern const uint32_t RaycastMask_Start_m4258084315_MetadataUsageId;
extern RuntimeClass* RectTransform_t3704657025_il2cpp_TypeInfo_var;
extern RuntimeClass* RectTransformUtility_t1743242446_il2cpp_TypeInfo_var;
extern RuntimeClass* UnityException_t3598173660_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral2491212491;
extern const uint32_t RaycastMask_IsRaycastLocationValid_m385128278_MetadataUsageId;
extern const uint32_t RotateTowardCamera_Update_m3193687590_MetadataUsageId;
extern const uint32_t RotateTowardCamera_OnRotateTowardCamera_m1367757239_MetadataUsageId;
extern const RuntimeMethod* GameObject_AddComponent_TisHUDFPS_t4241874542_m817494477_RuntimeMethod_var;
extern const uint32_t SceneManager_Start_m3453891390_MetadataUsageId;
extern RuntimeClass* GameScene_t4110668666_il2cpp_TypeInfo_var;
extern const uint32_t SceneManager_InitScene_m1840825294_MetadataUsageId;
extern const uint32_t ShipBobble_Update_m1586417524_MetadataUsageId;
extern const uint32_t ShipCamera_Update_m1783121126_MetadataUsageId;
extern const RuntimeMethod* Entity_Find_TisShipUnit_t4065899274_m28943516_RuntimeMethod_var;
extern const uint32_t ShipController_Start_m228069111_MetadataUsageId;
extern String_t* _stringLiteral549941361;
extern const uint32_t ShipController_IsShallowWater_m967017931_MetadataUsageId;
extern String_t* _stringLiteral3403559637;
extern String_t* _stringLiteral674676282;
extern const uint32_t ShipOrbit_Update_m1428905593_MetadataUsageId;
extern const uint32_t ShipTouchToDestination_Update_m2812452769_MetadataUsageId;
extern const uint32_t ShipTouchToDestination_ShipMovement_m2927501072_MetadataUsageId;
extern const uint32_t ShipTouchToMove_Update_m1057204479_MetadataUsageId;
extern String_t* _stringLiteral3450517380;
extern String_t* _stringLiteral731729006;
extern String_t* _stringLiteral820328377;
extern String_t* _stringLiteral3649710639;
extern String_t* _stringLiteral1377234670;
extern String_t* _stringLiteral793248935;
extern String_t* _stringLiteral1926654611;
extern String_t* _stringLiteral3521318628;
extern String_t* _stringLiteral3504410480;
extern String_t* _stringLiteral3450517376;
extern const uint32_t ShipTouchToMove_ShipMovement_m2549588759_MetadataUsageId;
extern RuntimeClass* SellDefine_t4148876514_il2cpp_TypeInfo_var;
extern const uint32_t ShipUnit__ctor_m3091616017_MetadataUsageId;
extern const uint32_t ShipUnit_Update_m1094417079_MetadataUsageId;
extern const uint32_t ShipUnit_LateUpdate_m2570662116_MetadataUsageId;
extern String_t* _stringLiteral1722375875;
extern const uint32_t ShipUnit_ApplyDamage_m1888147657_MetadataUsageId;
extern RuntimeClass* GUIContent_t3050628031_il2cpp_TypeInfo_var;
extern const uint32_t ShowName_OnGUI_m2312279633_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t437934597_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m3552178049_RuntimeMethod_var;
extern const uint32_t SkillAbility__ctor_m1559185056_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_ContainsKey_m969889043_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_set_Item_m162486917_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m1751118572_RuntimeMethod_var;
extern const uint32_t SkillAbility_AddSkill_m539831979_MetadataUsageId;
extern const uint32_t SkillAbility_AddSkill_m1907196276_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_GetEnumerator_m1696204037_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m2625386591_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Value_m1855475308_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m2515343744_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m3555283856_RuntimeMethod_var;
extern String_t* _stringLiteral2821735793;
extern const uint32_t SkillAbility_ApplySkills_m2312763334_MetadataUsageId;
extern RuntimeClass* SkillEnum_t2332882726_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m2907616642_RuntimeMethod_var;
extern String_t* _stringLiteral1991331367;
extern const uint32_t SkillAbility_ApplySkill_m3978726859_MetadataUsageId;
extern const uint32_t SkillAbility_ApplySkill_m1333928009_MetadataUsageId;
extern String_t* _stringLiteral3787301066;
extern String_t* _stringLiteral2544853177;
extern const uint32_t SkillValue_ToString_m3839163353_MetadataUsageId;
extern RuntimeClass* uGuiLayout_t2579011462_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisCanvas_t3310196443_m782269232_RuntimeMethod_var;
extern String_t* _stringLiteral591802201;
extern const uint32_t TestGuiLayout_Start_m603216618_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisRigidbody_t3916780224_m279685075_RuntimeMethod_var;
extern const uint32_t Tools_GetRigidbody_m2837624754_MetadataUsageId;
extern RuntimeClass* List_1_t1093887670_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m1592361844_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Contains_m2602505829_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m3007157130_RuntimeMethod_var;
extern const uint32_t Tools_GetRigidbodies_m3541047403_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisCollider_t1773347010_m4226749020_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponentsInChildren_TisCollider_t1773347010_m2157248342_RuntimeMethod_var;
extern const uint32_t Tools_CalculateWorldBounds_m2350520555_MetadataUsageId;
extern const uint32_t Tools_CalculateLocalBounds_m2764462892_MetadataUsageId;
extern const uint32_t Tools_DistanceToPlane_m2293493925_MetadataUsageId;
extern const uint32_t Tools_EncodeFloatToInt_m1804285472_MetadataUsageId;
extern const uint32_t Tools_IsChild_m3091374981_MetadataUsageId;
extern String_t* _stringLiteral3452614529;
extern const uint32_t Tools_GetHierarchy_m808099638_MetadataUsageId;
extern String_t* _stringLiteral1973861598;
extern String_t* _stringLiteral452222980;
extern const uint32_t Tools_IsWebAddress_m1893856580_MetadataUsageId;
extern String_t* _stringLiteral3452614644;
extern String_t* _stringLiteral3139614613;
extern const uint32_t Tools_GetFullPath_m3144299145_MetadataUsageId;
extern const uint32_t Tools_Ramp_m3609940933_MetadataUsageId;
extern const uint32_t Tools_Deviates_m3723987936_MetadataUsageId;
extern const uint32_t Tools_Deviates_m728866459_MetadataUsageId;
extern const RuntimeType* GameObject_t1113636619_0_0_0_var;
extern RuntimeClass* GameObjectU5BU5D_t3328599146_il2cpp_TypeInfo_var;
extern const uint32_t Tools_Broadcast_m1011643776_MetadataUsageId;
extern String_t* _stringLiteral2426591911;
extern const uint32_t Tools_GetTouchPositionInGame_m4201674753_MetadataUsageId;
extern const uint32_t Tools_GetTouchPositionInWater_m548322082_MetadataUsageId;
extern const uint32_t Tools_SignedAngleBetween_m19171509_MetadataUsageId;
extern const uint32_t Tools_LookAtPlayerOnYAxis_m199382236_MetadataUsageId;
extern const RuntimeMethod* Stack_1_Push_m2927642228_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_Push_m3148378454_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisHorizontalLayoutGroup_t2586782146_m3608050281_RuntimeMethod_var;
extern const uint32_t uGuiLayout_BeginHorizontal_m3128919469_MetadataUsageId;
extern const RuntimeMethod* Stack_1_Pop_m3170417723_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_Pop_m2243551095_RuntimeMethod_var;
extern const uint32_t uGuiLayout_EndHorizontal_m3903317722_MetadataUsageId;
extern const RuntimeMethod* GameObject_AddComponent_TisVerticalLayoutGroup_t923838031_m1306033316_RuntimeMethod_var;
extern const uint32_t uGuiLayout_BeginVertical_m2791822590_MetadataUsageId;
extern const uint32_t uGuiLayout_EndVertical_m228777174_MetadataUsageId;
extern const RuntimeMethod* GameObject_AddComponent_TisRectTransform_t3704657025_m3669823029_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_get_Count_m545179475_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_Peek_m855668811_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_Push_m811146802_RuntimeMethod_var;
extern const uint32_t uGuiLayout_BeginSubControl_m130035606_MetadataUsageId;
extern const RuntimeMethod* Stack_1_Pop_m1483992283_RuntimeMethod_var;
extern const uint32_t uGuiLayout_EndSubControl_m47764685_MetadataUsageId;
extern const uint32_t uGuiLayout_AddChild_m2495970376_MetadataUsageId;
extern const uint32_t uGuiLayout_Label_m3379332580_MetadataUsageId;
extern RuntimeClass* Stack_1_t253079184_il2cpp_TypeInfo_var;
extern RuntimeClass* Stack_1_t2240656229_il2cpp_TypeInfo_var;
extern RuntimeClass* Stack_1_t4010345780_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Stack_1__ctor_m1535627942_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1__ctor_m20774007_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1__ctor_m2382698871_RuntimeMethod_var;
extern const uint32_t uGuiLayout__cctor_m196720147_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisRectTransform_t3704657025_m3396022872_RuntimeMethod_var;
extern const uint32_t UIWindowBase_Start_m860749202_MetadataUsageId;
extern const uint32_t UIWindowBase_OnDrag_m2792229854_MetadataUsageId;
extern RuntimeClass* Hashtable_t1853889766_il2cpp_TypeInfo_var;
extern const uint32_t Water__ctor_m1387834787_MetadataUsageId;
extern RuntimeClass* Water_t1083516957_il2cpp_TypeInfo_var;
extern RuntimeClass* Matrix4x4_t1817901843_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral253045501;
extern String_t* _stringLiteral521248767;
extern String_t* _stringLiteral1050277699;
extern String_t* _stringLiteral2712144963;
extern String_t* _stringLiteral1727694679;
extern const uint32_t Water_OnWillRenderObject_m2156857888_MetadataUsageId;
extern RuntimeClass* IEnumerator_t1853284238_il2cpp_TypeInfo_var;
extern RuntimeClass* DictionaryEntry_t3123975638_il2cpp_TypeInfo_var;
extern RuntimeClass* Camera_t4157153871_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t3640265483_il2cpp_TypeInfo_var;
extern const uint32_t Water_OnDisable_m2513774421_MetadataUsageId;
extern String_t* _stringLiteral4103337048;
extern String_t* _stringLiteral3029421264;
extern String_t* _stringLiteral2268830102;
extern String_t* _stringLiteral2617886994;
extern String_t* _stringLiteral662295082;
extern String_t* _stringLiteral866112042;
extern String_t* _stringLiteral316339988;
extern const uint32_t Water_LateUpdate_m1551635237_MetadataUsageId;
extern const RuntimeType* Skybox_t2662837510_0_0_0_var;
extern RuntimeClass* Skybox_t2662837510_il2cpp_TypeInfo_var;
extern const uint32_t Water_UpdateCameraModes_m552426476_MetadataUsageId;
extern const RuntimeType* Camera_t4157153871_0_0_0_var;
extern RuntimeClass* RenderTexture_t2108887433_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_AddComponent_TisFlareLayer_t1739223323_m1608860411_RuntimeMethod_var;
extern String_t* _stringLiteral3277177482;
extern String_t* _stringLiteral1537969950;
extern String_t* _stringLiteral1505539685;
extern String_t* _stringLiteral1366986456;
extern String_t* _stringLiteral2541588253;
extern const uint32_t Water_CreateWaterObjects_m3692736303_MetadataUsageId;
extern String_t* _stringLiteral713880165;
extern String_t* _stringLiteral1103090832;
extern String_t* _stringLiteral2505954588;
extern const uint32_t Water_FindHardwareWaterSupport_m1195528645_MetadataUsageId;
extern const uint32_t Water_CameraSpacePlane_m2359059317_MetadataUsageId;
extern RuntimeClass* Vector4_t3319028937_il2cpp_TypeInfo_var;
extern const uint32_t Water_CalculateObliqueMatrix_m3061601437_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponentInChildren_TisCanvas_t3310196443_m2710541779_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisGraphicRaycaster_t2999697109_m3357594245_RuntimeMethod_var;
extern String_t* _stringLiteral4236429424;
extern const uint32_t WorldCanvasUI_InitWorldCanvasUI_m724433920_MetadataUsageId;
struct GUIStyle_t3956901511_marshaled_pinvoke;
struct GUIStyle_t3956901511_marshaled_com;
struct GUIStyleState_t1397964415_marshaled_pinvoke;
struct GUIStyleState_t1397964415_marshaled_com;
struct RectOffset_t1369453676_marshaled_com;

struct FontU5BU5D_t1399044585;
struct TypeU5BU5D_t3940880105;
struct Vector3U5BU5D_t1718750761;
struct RendererU5BU5D_t3210418286;
struct SingleU5B0___U2C0___U5D_t1444911252;
struct ObjectU5BU5D_t2843939325;
struct SingleU5B0___U2C0___U2C0___U5D_t1444911253;
struct TransformU5BU5D_t807237628;
struct ColliderU5BU5D_t4234922487;
struct GameObjectU5BU5D_t3328599146;
struct ObjectU5BU5D_t1417781964;


#ifndef U3CMODULEU3E_T692745543_H
#define U3CMODULEU3E_T692745543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745543 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745543_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef STACK_1_T4010345780_H
#define STACK_1_T4010345780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Stack`1<uGuiLayout/Mode>
struct  Stack_1_t4010345780  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Stack`1::_array
	ModeU5BU5D_t612995752* ____array_0;
	// System.Int32 System.Collections.Generic.Stack`1::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Generic.Stack`1::_version
	int32_t ____version_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Stack_1_t4010345780, ____array_0)); }
	inline ModeU5BU5D_t612995752* get__array_0() const { return ____array_0; }
	inline ModeU5BU5D_t612995752** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(ModeU5BU5D_t612995752* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(Stack_1_t4010345780, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Stack_1_t4010345780, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACK_1_T4010345780_H
#ifndef TOOLS_T613821292_H
#define TOOLS_T613821292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tools
struct  Tools_t613821292  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOOLS_T613821292_H
#ifndef LIST_1_T1093887670_H
#define LIST_1_T1093887670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Rigidbody>
struct  List_1_t1093887670  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	RigidbodyU5BU5D_t3308997185* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1093887670, ____items_1)); }
	inline RigidbodyU5BU5D_t3308997185* get__items_1() const { return ____items_1; }
	inline RigidbodyU5BU5D_t3308997185** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(RigidbodyU5BU5D_t3308997185* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1093887670, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1093887670, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1093887670_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	RigidbodyU5BU5D_t3308997185* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1093887670_StaticFields, ___EmptyArray_4)); }
	inline RigidbodyU5BU5D_t3308997185* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline RigidbodyU5BU5D_t3308997185** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(RigidbodyU5BU5D_t3308997185* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1093887670_H
#ifndef STACK_1_T2240656229_H
#define STACK_1_T2240656229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Stack`1<System.Single>
struct  Stack_1_t2240656229  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Stack`1::_array
	SingleU5BU5D_t1444911251* ____array_0;
	// System.Int32 System.Collections.Generic.Stack`1::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Generic.Stack`1::_version
	int32_t ____version_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Stack_1_t2240656229, ____array_0)); }
	inline SingleU5BU5D_t1444911251* get__array_0() const { return ____array_0; }
	inline SingleU5BU5D_t1444911251** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(SingleU5BU5D_t1444911251* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(Stack_1_t2240656229, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Stack_1_t2240656229, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACK_1_T2240656229_H
#ifndef HASHTABLE_T1853889766_H
#define HASHTABLE_T1853889766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Hashtable
struct  Hashtable_t1853889766  : public RuntimeObject
{
public:
	// System.Int32 System.Collections.Hashtable::inUse
	int32_t ___inUse_1;
	// System.Int32 System.Collections.Hashtable::modificationCount
	int32_t ___modificationCount_2;
	// System.Single System.Collections.Hashtable::loadFactor
	float ___loadFactor_3;
	// System.Collections.Hashtable/Slot[] System.Collections.Hashtable::table
	SlotU5BU5D_t2994659099* ___table_4;
	// System.Int32[] System.Collections.Hashtable::hashes
	Int32U5BU5D_t385246372* ___hashes_5;
	// System.Int32 System.Collections.Hashtable::threshold
	int32_t ___threshold_6;
	// System.Collections.Hashtable/HashKeys System.Collections.Hashtable::hashKeys
	HashKeys_t1568156503 * ___hashKeys_7;
	// System.Collections.Hashtable/HashValues System.Collections.Hashtable::hashValues
	HashValues_t618387445 * ___hashValues_8;
	// System.Collections.IHashCodeProvider System.Collections.Hashtable::hcpRef
	RuntimeObject* ___hcpRef_9;
	// System.Collections.IComparer System.Collections.Hashtable::comparerRef
	RuntimeObject* ___comparerRef_10;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Hashtable::serializationInfo
	SerializationInfo_t950877179 * ___serializationInfo_11;
	// System.Collections.IEqualityComparer System.Collections.Hashtable::equalityComparer
	RuntimeObject* ___equalityComparer_12;

public:
	inline static int32_t get_offset_of_inUse_1() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766, ___inUse_1)); }
	inline int32_t get_inUse_1() const { return ___inUse_1; }
	inline int32_t* get_address_of_inUse_1() { return &___inUse_1; }
	inline void set_inUse_1(int32_t value)
	{
		___inUse_1 = value;
	}

	inline static int32_t get_offset_of_modificationCount_2() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766, ___modificationCount_2)); }
	inline int32_t get_modificationCount_2() const { return ___modificationCount_2; }
	inline int32_t* get_address_of_modificationCount_2() { return &___modificationCount_2; }
	inline void set_modificationCount_2(int32_t value)
	{
		___modificationCount_2 = value;
	}

	inline static int32_t get_offset_of_loadFactor_3() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766, ___loadFactor_3)); }
	inline float get_loadFactor_3() const { return ___loadFactor_3; }
	inline float* get_address_of_loadFactor_3() { return &___loadFactor_3; }
	inline void set_loadFactor_3(float value)
	{
		___loadFactor_3 = value;
	}

	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766, ___table_4)); }
	inline SlotU5BU5D_t2994659099* get_table_4() const { return ___table_4; }
	inline SlotU5BU5D_t2994659099** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(SlotU5BU5D_t2994659099* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_hashes_5() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766, ___hashes_5)); }
	inline Int32U5BU5D_t385246372* get_hashes_5() const { return ___hashes_5; }
	inline Int32U5BU5D_t385246372** get_address_of_hashes_5() { return &___hashes_5; }
	inline void set_hashes_5(Int32U5BU5D_t385246372* value)
	{
		___hashes_5 = value;
		Il2CppCodeGenWriteBarrier((&___hashes_5), value);
	}

	inline static int32_t get_offset_of_threshold_6() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766, ___threshold_6)); }
	inline int32_t get_threshold_6() const { return ___threshold_6; }
	inline int32_t* get_address_of_threshold_6() { return &___threshold_6; }
	inline void set_threshold_6(int32_t value)
	{
		___threshold_6 = value;
	}

	inline static int32_t get_offset_of_hashKeys_7() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766, ___hashKeys_7)); }
	inline HashKeys_t1568156503 * get_hashKeys_7() const { return ___hashKeys_7; }
	inline HashKeys_t1568156503 ** get_address_of_hashKeys_7() { return &___hashKeys_7; }
	inline void set_hashKeys_7(HashKeys_t1568156503 * value)
	{
		___hashKeys_7 = value;
		Il2CppCodeGenWriteBarrier((&___hashKeys_7), value);
	}

	inline static int32_t get_offset_of_hashValues_8() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766, ___hashValues_8)); }
	inline HashValues_t618387445 * get_hashValues_8() const { return ___hashValues_8; }
	inline HashValues_t618387445 ** get_address_of_hashValues_8() { return &___hashValues_8; }
	inline void set_hashValues_8(HashValues_t618387445 * value)
	{
		___hashValues_8 = value;
		Il2CppCodeGenWriteBarrier((&___hashValues_8), value);
	}

	inline static int32_t get_offset_of_hcpRef_9() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766, ___hcpRef_9)); }
	inline RuntimeObject* get_hcpRef_9() const { return ___hcpRef_9; }
	inline RuntimeObject** get_address_of_hcpRef_9() { return &___hcpRef_9; }
	inline void set_hcpRef_9(RuntimeObject* value)
	{
		___hcpRef_9 = value;
		Il2CppCodeGenWriteBarrier((&___hcpRef_9), value);
	}

	inline static int32_t get_offset_of_comparerRef_10() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766, ___comparerRef_10)); }
	inline RuntimeObject* get_comparerRef_10() const { return ___comparerRef_10; }
	inline RuntimeObject** get_address_of_comparerRef_10() { return &___comparerRef_10; }
	inline void set_comparerRef_10(RuntimeObject* value)
	{
		___comparerRef_10 = value;
		Il2CppCodeGenWriteBarrier((&___comparerRef_10), value);
	}

	inline static int32_t get_offset_of_serializationInfo_11() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766, ___serializationInfo_11)); }
	inline SerializationInfo_t950877179 * get_serializationInfo_11() const { return ___serializationInfo_11; }
	inline SerializationInfo_t950877179 ** get_address_of_serializationInfo_11() { return &___serializationInfo_11; }
	inline void set_serializationInfo_11(SerializationInfo_t950877179 * value)
	{
		___serializationInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___serializationInfo_11), value);
	}

	inline static int32_t get_offset_of_equalityComparer_12() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766, ___equalityComparer_12)); }
	inline RuntimeObject* get_equalityComparer_12() const { return ___equalityComparer_12; }
	inline RuntimeObject** get_address_of_equalityComparer_12() { return &___equalityComparer_12; }
	inline void set_equalityComparer_12(RuntimeObject* value)
	{
		___equalityComparer_12 = value;
		Il2CppCodeGenWriteBarrier((&___equalityComparer_12), value);
	}
};

struct Hashtable_t1853889766_StaticFields
{
public:
	// System.Int32[] System.Collections.Hashtable::primeTbl
	Int32U5BU5D_t385246372* ___primeTbl_13;

public:
	inline static int32_t get_offset_of_primeTbl_13() { return static_cast<int32_t>(offsetof(Hashtable_t1853889766_StaticFields, ___primeTbl_13)); }
	inline Int32U5BU5D_t385246372* get_primeTbl_13() const { return ___primeTbl_13; }
	inline Int32U5BU5D_t385246372** get_address_of_primeTbl_13() { return &___primeTbl_13; }
	inline void set_primeTbl_13(Int32U5BU5D_t385246372* value)
	{
		___primeTbl_13 = value;
		Il2CppCodeGenWriteBarrier((&___primeTbl_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHTABLE_T1853889766_H
#ifndef LIST_1_T3319525431_H
#define LIST_1_T3319525431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.String>
struct  List_1_t3319525431  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_t1281789340* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____items_1)); }
	inline StringU5BU5D_t1281789340* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_t1281789340** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_t1281789340* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3319525431_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	StringU5BU5D_t1281789340* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3319525431_StaticFields, ___EmptyArray_4)); }
	inline StringU5BU5D_t1281789340* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline StringU5BU5D_t1281789340** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(StringU5BU5D_t1281789340* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3319525431_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef STACK_1_T253079184_H
#define STACK_1_T253079184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Stack`1<UnityEngine.RectTransform>
struct  Stack_1_t253079184  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Stack`1::_array
	RectTransformU5BU5D_t107129948* ____array_0;
	// System.Int32 System.Collections.Generic.Stack`1::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Generic.Stack`1::_version
	int32_t ____version_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Stack_1_t253079184, ____array_0)); }
	inline RectTransformU5BU5D_t107129948* get__array_0() const { return ____array_0; }
	inline RectTransformU5BU5D_t107129948** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(RectTransformU5BU5D_t107129948* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(Stack_1_t253079184, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Stack_1_t253079184, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACK_1_T253079184_H
#ifndef GAMESCENE_T4110668666_H
#define GAMESCENE_T4110668666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameScene
struct  GameScene_t4110668666  : public RuntimeObject
{
public:
	// UnityEngine.GameObject GameScene::m_waterGO
	GameObject_t1113636619 * ___m_waterGO_0;
	// UnityEngine.GameObject GameScene::m_terrainGO
	GameObject_t1113636619 * ___m_terrainGO_1;
	// UnityEngine.GameObject GameScene::m_mainCamera
	GameObject_t1113636619 * ___m_mainCamera_2;
	// UnityEngine.GameObject GameScene::m_player
	GameObject_t1113636619 * ___m_player_3;

public:
	inline static int32_t get_offset_of_m_waterGO_0() { return static_cast<int32_t>(offsetof(GameScene_t4110668666, ___m_waterGO_0)); }
	inline GameObject_t1113636619 * get_m_waterGO_0() const { return ___m_waterGO_0; }
	inline GameObject_t1113636619 ** get_address_of_m_waterGO_0() { return &___m_waterGO_0; }
	inline void set_m_waterGO_0(GameObject_t1113636619 * value)
	{
		___m_waterGO_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_waterGO_0), value);
	}

	inline static int32_t get_offset_of_m_terrainGO_1() { return static_cast<int32_t>(offsetof(GameScene_t4110668666, ___m_terrainGO_1)); }
	inline GameObject_t1113636619 * get_m_terrainGO_1() const { return ___m_terrainGO_1; }
	inline GameObject_t1113636619 ** get_address_of_m_terrainGO_1() { return &___m_terrainGO_1; }
	inline void set_m_terrainGO_1(GameObject_t1113636619 * value)
	{
		___m_terrainGO_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_terrainGO_1), value);
	}

	inline static int32_t get_offset_of_m_mainCamera_2() { return static_cast<int32_t>(offsetof(GameScene_t4110668666, ___m_mainCamera_2)); }
	inline GameObject_t1113636619 * get_m_mainCamera_2() const { return ___m_mainCamera_2; }
	inline GameObject_t1113636619 ** get_address_of_m_mainCamera_2() { return &___m_mainCamera_2; }
	inline void set_m_mainCamera_2(GameObject_t1113636619 * value)
	{
		___m_mainCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_mainCamera_2), value);
	}

	inline static int32_t get_offset_of_m_player_3() { return static_cast<int32_t>(offsetof(GameScene_t4110668666, ___m_player_3)); }
	inline GameObject_t1113636619 * get_m_player_3() const { return ___m_player_3; }
	inline GameObject_t1113636619 ** get_address_of_m_player_3() { return &___m_player_3; }
	inline void set_m_player_3(GameObject_t1113636619 * value)
	{
		___m_player_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_player_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESCENE_T4110668666_H
#ifndef U3CFPSU3EC__ITERATOR0_T392322613_H
#define U3CFPSU3EC__ITERATOR0_T392322613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HUDFPS/<FPS>c__Iterator0
struct  U3CFPSU3Ec__Iterator0_t392322613  : public RuntimeObject
{
public:
	// System.Single HUDFPS/<FPS>c__Iterator0::<fps>__1
	float ___U3CfpsU3E__1_0;
	// HUDFPS HUDFPS/<FPS>c__Iterator0::$this
	HUDFPS_t4241874542 * ___U24this_1;
	// System.Object HUDFPS/<FPS>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean HUDFPS/<FPS>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 HUDFPS/<FPS>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CfpsU3E__1_0() { return static_cast<int32_t>(offsetof(U3CFPSU3Ec__Iterator0_t392322613, ___U3CfpsU3E__1_0)); }
	inline float get_U3CfpsU3E__1_0() const { return ___U3CfpsU3E__1_0; }
	inline float* get_address_of_U3CfpsU3E__1_0() { return &___U3CfpsU3E__1_0; }
	inline void set_U3CfpsU3E__1_0(float value)
	{
		___U3CfpsU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CFPSU3Ec__Iterator0_t392322613, ___U24this_1)); }
	inline HUDFPS_t4241874542 * get_U24this_1() const { return ___U24this_1; }
	inline HUDFPS_t4241874542 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(HUDFPS_t4241874542 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CFPSU3Ec__Iterator0_t392322613, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CFPSU3Ec__Iterator0_t392322613, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CFPSU3Ec__Iterator0_t392322613, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFPSU3EC__ITERATOR0_T392322613_H
#ifndef DEBUGTOOLS_T3270302741_H
#define DEBUGTOOLS_T3270302741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DebugTools
struct  DebugTools_t3270302741  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGTOOLS_T3270302741_H
#ifndef LIST_1_T777473367_H
#define LIST_1_T777473367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Transform>
struct  List_1_t777473367  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TransformU5BU5D_t807237628* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t777473367, ____items_1)); }
	inline TransformU5BU5D_t807237628* get__items_1() const { return ____items_1; }
	inline TransformU5BU5D_t807237628** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TransformU5BU5D_t807237628* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t777473367, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t777473367, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t777473367_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	TransformU5BU5D_t807237628* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t777473367_StaticFields, ___EmptyArray_4)); }
	inline TransformU5BU5D_t807237628* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline TransformU5BU5D_t807237628** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(TransformU5BU5D_t807237628* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T777473367_H
#ifndef DICTIONARY_2_T437934597_H
#define DICTIONARY_2_T437934597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<SkillAbility/SkillEnum,SkillAbility/SkillValue>
struct  Dictionary_2_t437934597  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	SkillEnumU5BU5D_t2878962051* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	SkillValueU5BU5D_t732829474* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t437934597, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t437934597, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t437934597, ___keySlots_6)); }
	inline SkillEnumU5BU5D_t2878962051* get_keySlots_6() const { return ___keySlots_6; }
	inline SkillEnumU5BU5D_t2878962051** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(SkillEnumU5BU5D_t2878962051* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t437934597, ___valueSlots_7)); }
	inline SkillValueU5BU5D_t732829474* get_valueSlots_7() const { return ___valueSlots_7; }
	inline SkillValueU5BU5D_t732829474** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(SkillValueU5BU5D_t732829474* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t437934597, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t437934597, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t437934597, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t437934597, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t437934597, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t437934597, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t437934597, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t437934597_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t882294915 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t437934597_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t882294915 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t882294915 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t882294915 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T437934597_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef SELLDEFINE_T4148876514_H
#define SELLDEFINE_T4148876514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SellDefine
struct  SellDefine_t4148876514  : public RuntimeObject
{
public:
	// System.Single SellDefine::mSoldValue
	float ___mSoldValue_0;
	// System.Boolean SellDefine::mCanBeSold
	bool ___mCanBeSold_1;

public:
	inline static int32_t get_offset_of_mSoldValue_0() { return static_cast<int32_t>(offsetof(SellDefine_t4148876514, ___mSoldValue_0)); }
	inline float get_mSoldValue_0() const { return ___mSoldValue_0; }
	inline float* get_address_of_mSoldValue_0() { return &___mSoldValue_0; }
	inline void set_mSoldValue_0(float value)
	{
		___mSoldValue_0 = value;
	}

	inline static int32_t get_offset_of_mCanBeSold_1() { return static_cast<int32_t>(offsetof(SellDefine_t4148876514, ___mCanBeSold_1)); }
	inline bool get_mCanBeSold_1() const { return ___mCanBeSold_1; }
	inline bool* get_address_of_mCanBeSold_1() { return &___mCanBeSold_1; }
	inline void set_mCanBeSold_1(bool value)
	{
		___mCanBeSold_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELLDEFINE_T4148876514_H
#ifndef GUICONTENT_T3050628031_H
#define GUICONTENT_T3050628031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIContent
struct  GUIContent_t3050628031  : public RuntimeObject
{
public:
	// System.String UnityEngine.GUIContent::m_Text
	String_t* ___m_Text_0;
	// UnityEngine.Texture UnityEngine.GUIContent::m_Image
	Texture_t3661962703 * ___m_Image_1;
	// System.String UnityEngine.GUIContent::m_Tooltip
	String_t* ___m_Tooltip_2;

public:
	inline static int32_t get_offset_of_m_Text_0() { return static_cast<int32_t>(offsetof(GUIContent_t3050628031, ___m_Text_0)); }
	inline String_t* get_m_Text_0() const { return ___m_Text_0; }
	inline String_t** get_address_of_m_Text_0() { return &___m_Text_0; }
	inline void set_m_Text_0(String_t* value)
	{
		___m_Text_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_0), value);
	}

	inline static int32_t get_offset_of_m_Image_1() { return static_cast<int32_t>(offsetof(GUIContent_t3050628031, ___m_Image_1)); }
	inline Texture_t3661962703 * get_m_Image_1() const { return ___m_Image_1; }
	inline Texture_t3661962703 ** get_address_of_m_Image_1() { return &___m_Image_1; }
	inline void set_m_Image_1(Texture_t3661962703 * value)
	{
		___m_Image_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Image_1), value);
	}

	inline static int32_t get_offset_of_m_Tooltip_2() { return static_cast<int32_t>(offsetof(GUIContent_t3050628031, ___m_Tooltip_2)); }
	inline String_t* get_m_Tooltip_2() const { return ___m_Tooltip_2; }
	inline String_t** get_address_of_m_Tooltip_2() { return &___m_Tooltip_2; }
	inline void set_m_Tooltip_2(String_t* value)
	{
		___m_Tooltip_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tooltip_2), value);
	}
};

struct GUIContent_t3050628031_StaticFields
{
public:
	// UnityEngine.GUIContent UnityEngine.GUIContent::s_Text
	GUIContent_t3050628031 * ___s_Text_3;
	// UnityEngine.GUIContent UnityEngine.GUIContent::s_Image
	GUIContent_t3050628031 * ___s_Image_4;
	// UnityEngine.GUIContent UnityEngine.GUIContent::s_TextImage
	GUIContent_t3050628031 * ___s_TextImage_5;
	// UnityEngine.GUIContent UnityEngine.GUIContent::none
	GUIContent_t3050628031 * ___none_6;

public:
	inline static int32_t get_offset_of_s_Text_3() { return static_cast<int32_t>(offsetof(GUIContent_t3050628031_StaticFields, ___s_Text_3)); }
	inline GUIContent_t3050628031 * get_s_Text_3() const { return ___s_Text_3; }
	inline GUIContent_t3050628031 ** get_address_of_s_Text_3() { return &___s_Text_3; }
	inline void set_s_Text_3(GUIContent_t3050628031 * value)
	{
		___s_Text_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_Text_3), value);
	}

	inline static int32_t get_offset_of_s_Image_4() { return static_cast<int32_t>(offsetof(GUIContent_t3050628031_StaticFields, ___s_Image_4)); }
	inline GUIContent_t3050628031 * get_s_Image_4() const { return ___s_Image_4; }
	inline GUIContent_t3050628031 ** get_address_of_s_Image_4() { return &___s_Image_4; }
	inline void set_s_Image_4(GUIContent_t3050628031 * value)
	{
		___s_Image_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Image_4), value);
	}

	inline static int32_t get_offset_of_s_TextImage_5() { return static_cast<int32_t>(offsetof(GUIContent_t3050628031_StaticFields, ___s_TextImage_5)); }
	inline GUIContent_t3050628031 * get_s_TextImage_5() const { return ___s_TextImage_5; }
	inline GUIContent_t3050628031 ** get_address_of_s_TextImage_5() { return &___s_TextImage_5; }
	inline void set_s_TextImage_5(GUIContent_t3050628031 * value)
	{
		___s_TextImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_TextImage_5), value);
	}

	inline static int32_t get_offset_of_none_6() { return static_cast<int32_t>(offsetof(GUIContent_t3050628031_StaticFields, ___none_6)); }
	inline GUIContent_t3050628031 * get_none_6() const { return ___none_6; }
	inline GUIContent_t3050628031 ** get_address_of_none_6() { return &___none_6; }
	inline void set_none_6(GUIContent_t3050628031 * value)
	{
		___none_6 = value;
		Il2CppCodeGenWriteBarrier((&___none_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.GUIContent
struct GUIContent_t3050628031_marshaled_pinvoke
{
	char* ___m_Text_0;
	Texture_t3661962703 * ___m_Image_1;
	char* ___m_Tooltip_2;
};
// Native definition for COM marshalling of UnityEngine.GUIContent
struct GUIContent_t3050628031_marshaled_com
{
	Il2CppChar* ___m_Text_0;
	Texture_t3661962703 * ___m_Image_1;
	Il2CppChar* ___m_Tooltip_2;
};
#endif // GUICONTENT_T3050628031_H
#ifndef ABSTRACTEVENTDATA_T4171500731_H
#define ABSTRACTEVENTDATA_T4171500731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.AbstractEventData
struct  AbstractEventData_t4171500731  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.EventSystems.AbstractEventData::m_Used
	bool ___m_Used_0;

public:
	inline static int32_t get_offset_of_m_Used_0() { return static_cast<int32_t>(offsetof(AbstractEventData_t4171500731, ___m_Used_0)); }
	inline bool get_m_Used_0() const { return ___m_Used_0; }
	inline bool* get_address_of_m_Used_0() { return &___m_Used_0; }
	inline void set_m_Used_0(bool value)
	{
		___m_Used_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTEVENTDATA_T4171500731_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef YIELDINSTRUCTION_T403091072_H
#define YIELDINSTRUCTION_T403091072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t403091072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T403091072_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef LEVELEXP_T2602383666_H
#define LEVELEXP_T2602383666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelExp
struct  LevelExp_t2602383666  : public RuntimeObject
{
public:
	// System.UInt32 LevelExp::mLevel
	uint32_t ___mLevel_0;
	// System.UInt32 LevelExp::mExp
	uint32_t ___mExp_1;

public:
	inline static int32_t get_offset_of_mLevel_0() { return static_cast<int32_t>(offsetof(LevelExp_t2602383666, ___mLevel_0)); }
	inline uint32_t get_mLevel_0() const { return ___mLevel_0; }
	inline uint32_t* get_address_of_mLevel_0() { return &___mLevel_0; }
	inline void set_mLevel_0(uint32_t value)
	{
		___mLevel_0 = value;
	}

	inline static int32_t get_offset_of_mExp_1() { return static_cast<int32_t>(offsetof(LevelExp_t2602383666, ___mExp_1)); }
	inline uint32_t get_mExp_1() const { return ___mExp_1; }
	inline uint32_t* get_address_of_mExp_1() { return &___mExp_1; }
	inline void set_mExp_1(uint32_t value)
	{
		___mExp_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELEXP_T2602383666_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef SKILLABILITY_T2516669324_H
#define SKILLABILITY_T2516669324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SkillAbility
struct  SkillAbility_t2516669324  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<SkillAbility/SkillEnum,SkillAbility/SkillValue> SkillAbility::mSkillAbilities
	Dictionary_2_t437934597 * ___mSkillAbilities_0;

public:
	inline static int32_t get_offset_of_mSkillAbilities_0() { return static_cast<int32_t>(offsetof(SkillAbility_t2516669324, ___mSkillAbilities_0)); }
	inline Dictionary_2_t437934597 * get_mSkillAbilities_0() const { return ___mSkillAbilities_0; }
	inline Dictionary_2_t437934597 ** get_address_of_mSkillAbilities_0() { return &___mSkillAbilities_0; }
	inline void set_mSkillAbilities_0(Dictionary_2_t437934597 * value)
	{
		___mSkillAbilities_0 = value;
		Il2CppCodeGenWriteBarrier((&___mSkillAbilities_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKILLABILITY_T2516669324_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef LAYERDEF_T767273362_H
#define LAYERDEF_T767273362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LayerDef
struct  LayerDef_t767273362 
{
public:
	union
	{
		struct
		{
		};
		uint8_t LayerDef_t767273362__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERDEF_T767273362_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#define DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t2562230146 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifndef WAITFORSECONDS_T1699091251_H
#define WAITFORSECONDS_T1699091251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t1699091251  : public YieldInstruction_t403091072
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t1699091251, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	float ___m_Seconds_0;
};
#endif // WAITFORSECONDS_T1699091251_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef UNITYEVENT_T2581268647_H
#define UNITYEVENT_T2581268647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t2581268647  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t2581268647, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T2581268647_H
#ifndef METHODBASE_T_H
#define METHODBASE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodBase
struct  MethodBase_t  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODBASE_T_H
#ifndef BASEEVENTDATA_T3903027533_H
#define BASEEVENTDATA_T3903027533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseEventData
struct  BaseEventData_t3903027533  : public AbstractEventData_t4171500731
{
public:
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t1003666588 * ___m_EventSystem_1;

public:
	inline static int32_t get_offset_of_m_EventSystem_1() { return static_cast<int32_t>(offsetof(BaseEventData_t3903027533, ___m_EventSystem_1)); }
	inline EventSystem_t1003666588 * get_m_EventSystem_1() const { return ___m_EventSystem_1; }
	inline EventSystem_t1003666588 ** get_address_of_m_EventSystem_1() { return &___m_EventSystem_1; }
	inline void set_m_EventSystem_1(EventSystem_t1003666588 * value)
	{
		___m_EventSystem_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEEVENTDATA_T3903027533_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef CHAR_T3634460470_H
#define CHAR_T3634460470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3634460470 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t3634460470, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t3634460470_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3634460470_H
#ifndef DOUBLE_T594665363_H
#define DOUBLE_T594665363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t594665363 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t594665363, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T594665363_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef DICTIONARYENTRY_T3123975638_H
#define DICTIONARYENTRY_T3123975638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.DictionaryEntry
struct  DictionaryEntry_t3123975638 
{
public:
	// System.Object System.Collections.DictionaryEntry::_key
	RuntimeObject * ____key_0;
	// System.Object System.Collections.DictionaryEntry::_value
	RuntimeObject * ____value_1;

public:
	inline static int32_t get_offset_of__key_0() { return static_cast<int32_t>(offsetof(DictionaryEntry_t3123975638, ____key_0)); }
	inline RuntimeObject * get__key_0() const { return ____key_0; }
	inline RuntimeObject ** get_address_of__key_0() { return &____key_0; }
	inline void set__key_0(RuntimeObject * value)
	{
		____key_0 = value;
		Il2CppCodeGenWriteBarrier((&____key_0), value);
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(DictionaryEntry_t3123975638, ____value_1)); }
	inline RuntimeObject * get__value_1() const { return ____value_1; }
	inline RuntimeObject ** get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(RuntimeObject * value)
	{
		____value_1 = value;
		Il2CppCodeGenWriteBarrier((&____value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Collections.DictionaryEntry
struct DictionaryEntry_t3123975638_marshaled_pinvoke
{
	Il2CppIUnknown* ____key_0;
	Il2CppIUnknown* ____value_1;
};
// Native definition for COM marshalling of System.Collections.DictionaryEntry
struct DictionaryEntry_t3123975638_marshaled_com
{
	Il2CppIUnknown* ____key_0;
	Il2CppIUnknown* ____value_1;
};
#endif // DICTIONARYENTRY_T3123975638_H
#ifndef UNITYEXCEPTION_T3598173660_H
#define UNITYEXCEPTION_T3598173660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UnityException
struct  UnityException_t3598173660  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEXCEPTION_T3598173660_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef TAGDEF_T1264963638_H
#define TAGDEF_T1264963638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TagDef
struct  TagDef_t1264963638 
{
public:
	union
	{
		struct
		{
		};
		uint8_t TagDef_t1264963638__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGDEF_T1264963638_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef COMMAND_T1218582521_H
#define COMMAND_T1218582521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Command
struct  Command_t1218582521 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Command_t1218582521__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMAND_T1218582521_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef WATERMODE_T1169101781_H
#define WATERMODE_T1169101781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Water/WaterMode
struct  WaterMode_t1169101781 
{
public:
	// System.Int32 Water/WaterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WaterMode_t1169101781, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATERMODE_T1169101781_H
#ifndef METHODINFO_T_H
#define METHODINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodInfo
struct  MethodInfo_t  : public MethodBase_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODINFO_T_H
#ifndef NOTSUPPORTEDEXCEPTION_T1314879016_H
#define NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1314879016  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifndef GUISTYLESTATE_T1397964415_H
#define GUISTYLESTATE_T1397964415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIStyleState
struct  GUIStyleState_t1397964415  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.GUIStyleState::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.GUIStyle UnityEngine.GUIStyleState::m_SourceStyle
	GUIStyle_t3956901511 * ___m_SourceStyle_1;
	// UnityEngine.Texture2D UnityEngine.GUIStyleState::m_Background
	Texture2D_t3840446185 * ___m_Background_2;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(GUIStyleState_t1397964415, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_SourceStyle_1() { return static_cast<int32_t>(offsetof(GUIStyleState_t1397964415, ___m_SourceStyle_1)); }
	inline GUIStyle_t3956901511 * get_m_SourceStyle_1() const { return ___m_SourceStyle_1; }
	inline GUIStyle_t3956901511 ** get_address_of_m_SourceStyle_1() { return &___m_SourceStyle_1; }
	inline void set_m_SourceStyle_1(GUIStyle_t3956901511 * value)
	{
		___m_SourceStyle_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceStyle_1), value);
	}

	inline static int32_t get_offset_of_m_Background_2() { return static_cast<int32_t>(offsetof(GUIStyleState_t1397964415, ___m_Background_2)); }
	inline Texture2D_t3840446185 * get_m_Background_2() const { return ___m_Background_2; }
	inline Texture2D_t3840446185 ** get_address_of_m_Background_2() { return &___m_Background_2; }
	inline void set_m_Background_2(Texture2D_t3840446185 * value)
	{
		___m_Background_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Background_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.GUIStyleState
struct GUIStyleState_t1397964415_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	GUIStyle_t3956901511_marshaled_pinvoke* ___m_SourceStyle_1;
	Texture2D_t3840446185 * ___m_Background_2;
};
// Native definition for COM marshalling of UnityEngine.GUIStyleState
struct GUIStyleState_t1397964415_marshaled_com
{
	intptr_t ___m_Ptr_0;
	GUIStyle_t3956901511_marshaled_com* ___m_SourceStyle_1;
	Texture2D_t3840446185 * ___m_Background_2;
};
#endif // GUISTYLESTATE_T1397964415_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef CAMERACLEARFLAGS_T2362496923_H
#define CAMERACLEARFLAGS_T2362496923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CameraClearFlags
struct  CameraClearFlags_t2362496923 
{
public:
	// System.Int32 UnityEngine.CameraClearFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraClearFlags_t2362496923, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACLEARFLAGS_T2362496923_H
#ifndef HIDEFLAGS_T4250555765_H
#define HIDEFLAGS_T4250555765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t4250555765 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HideFlags_t4250555765, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T4250555765_H
#ifndef STRINGCOMPARISON_T3657712135_H
#define STRINGCOMPARISON_T3657712135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.StringComparison
struct  StringComparison_t3657712135 
{
public:
	// System.Int32 System.StringComparison::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StringComparison_t3657712135, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGCOMPARISON_T3657712135_H
#ifndef ANIMATIONCURVE_T3046754366_H
#define ANIMATIONCURVE_T3046754366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationCurve
struct  AnimationCurve_t3046754366  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t3046754366, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // ANIMATIONCURVE_T3046754366_H
#ifndef TOUCHPHASE_T72348083_H
#define TOUCHPHASE_T72348083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t72348083 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t72348083, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T72348083_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef TEXTANCHOR_T2035777396_H
#define TEXTANCHOR_T2035777396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_t2035777396 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAnchor_t2035777396, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_T2035777396_H
#ifndef BOUNDS_T2266837910_H
#define BOUNDS_T2266837910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t2266837910 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3722313464  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3722313464  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Center_0)); }
	inline Vector3_t3722313464  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3722313464 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3722313464  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Extents_1)); }
	inline Vector3_t3722313464  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3722313464 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3722313464  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T2266837910_H
#ifndef TYPE_T1152881528_H
#define TYPE_T1152881528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/Type
struct  Type_t1152881528 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t1152881528, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T1152881528_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef SENDMESSAGEOPTIONS_T3580193095_H
#define SENDMESSAGEOPTIONS_T3580193095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SendMessageOptions
struct  SendMessageOptions_t3580193095 
{
public:
	// System.Int32 UnityEngine.SendMessageOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SendMessageOptions_t3580193095, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDMESSAGEOPTIONS_T3580193095_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef UGUILAYOUT_T2579011462_H
#define UGUILAYOUT_T2579011462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// uGuiLayout
struct  uGuiLayout_t2579011462  : public RuntimeObject
{
public:

public:
};

struct uGuiLayout_t2579011462_StaticFields
{
public:
	// System.Collections.Generic.Stack`1<UnityEngine.RectTransform> uGuiLayout::subControls
	Stack_1_t253079184 * ___subControls_0;
	// UnityEngine.Vector2 uGuiLayout::currentPosition
	Vector2_t2156229523  ___currentPosition_1;
	// System.Collections.Generic.Stack`1<System.Single> uGuiLayout::positionCache
	Stack_1_t2240656229 * ___positionCache_2;
	// System.Collections.Generic.Stack`1<uGuiLayout/Mode> uGuiLayout::modeCache
	Stack_1_t4010345780 * ___modeCache_3;

public:
	inline static int32_t get_offset_of_subControls_0() { return static_cast<int32_t>(offsetof(uGuiLayout_t2579011462_StaticFields, ___subControls_0)); }
	inline Stack_1_t253079184 * get_subControls_0() const { return ___subControls_0; }
	inline Stack_1_t253079184 ** get_address_of_subControls_0() { return &___subControls_0; }
	inline void set_subControls_0(Stack_1_t253079184 * value)
	{
		___subControls_0 = value;
		Il2CppCodeGenWriteBarrier((&___subControls_0), value);
	}

	inline static int32_t get_offset_of_currentPosition_1() { return static_cast<int32_t>(offsetof(uGuiLayout_t2579011462_StaticFields, ___currentPosition_1)); }
	inline Vector2_t2156229523  get_currentPosition_1() const { return ___currentPosition_1; }
	inline Vector2_t2156229523 * get_address_of_currentPosition_1() { return &___currentPosition_1; }
	inline void set_currentPosition_1(Vector2_t2156229523  value)
	{
		___currentPosition_1 = value;
	}

	inline static int32_t get_offset_of_positionCache_2() { return static_cast<int32_t>(offsetof(uGuiLayout_t2579011462_StaticFields, ___positionCache_2)); }
	inline Stack_1_t2240656229 * get_positionCache_2() const { return ___positionCache_2; }
	inline Stack_1_t2240656229 ** get_address_of_positionCache_2() { return &___positionCache_2; }
	inline void set_positionCache_2(Stack_1_t2240656229 * value)
	{
		___positionCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___positionCache_2), value);
	}

	inline static int32_t get_offset_of_modeCache_3() { return static_cast<int32_t>(offsetof(uGuiLayout_t2579011462_StaticFields, ___modeCache_3)); }
	inline Stack_1_t4010345780 * get_modeCache_3() const { return ___modeCache_3; }
	inline Stack_1_t4010345780 ** get_address_of_modeCache_3() { return &___modeCache_3; }
	inline void set_modeCache_3(Stack_1_t4010345780 * value)
	{
		___modeCache_3 = value;
		Il2CppCodeGenWriteBarrier((&___modeCache_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UGUILAYOUT_T2579011462_H
#ifndef MODE_T3166956325_H
#define MODE_T3166956325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// uGuiLayout/Mode
struct  Mode_t3166956325 
{
public:
	// System.Int32 uGuiLayout/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t3166956325, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T3166956325_H
#ifndef FILLMETHOD_T1167457570_H
#define FILLMETHOD_T1167457570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/FillMethod
struct  FillMethod_t1167457570 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillMethod_t1167457570, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T1167457570_H
#ifndef RECTOFFSET_T1369453676_H
#define RECTOFFSET_T1369453676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectOffset
struct  RectOffset_t1369453676  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.RectOffset::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Object UnityEngine.RectOffset::m_SourceStyle
	RuntimeObject * ___m_SourceStyle_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(RectOffset_t1369453676, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_SourceStyle_1() { return static_cast<int32_t>(offsetof(RectOffset_t1369453676, ___m_SourceStyle_1)); }
	inline RuntimeObject * get_m_SourceStyle_1() const { return ___m_SourceStyle_1; }
	inline RuntimeObject ** get_address_of_m_SourceStyle_1() { return &___m_SourceStyle_1; }
	inline void set_m_SourceStyle_1(RuntimeObject * value)
	{
		___m_SourceStyle_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceStyle_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RectOffset
struct RectOffset_t1369453676_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
// Native definition for COM marshalling of UnityEngine.RectOffset
struct RectOffset_t1369453676_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
#endif // RECTOFFSET_T1369453676_H
#ifndef TOUCHTYPE_T2034578258_H
#define TOUCHTYPE_T2034578258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t2034578258 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchType_t2034578258, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T2034578258_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef RENDERMODE_T4077056833_H
#define RENDERMODE_T4077056833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderMode
struct  RenderMode_t4077056833 
{
public:
	// System.Int32 UnityEngine.RenderMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderMode_t4077056833, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERMODE_T4077056833_H
#ifndef BUTTONCLICKEDEVENT_T48803504_H
#define BUTTONCLICKEDEVENT_T48803504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Button/ButtonClickedEvent
struct  ButtonClickedEvent_t48803504  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONCLICKEDEVENT_T48803504_H
#ifndef PRIMITIVETYPE_T3468579401_H
#define PRIMITIVETYPE_T3468579401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PrimitiveType
struct  PrimitiveType_t3468579401 
{
public:
	// System.Int32 UnityEngine.PrimitiveType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PrimitiveType_t3468579401, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMITIVETYPE_T3468579401_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t1773347010 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline Collider_t1773347010 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t1773347010 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_pinvoke
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_com
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T1056001966_H
#ifndef SKILLENUM_T2332882726_H
#define SKILLENUM_T2332882726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SkillAbility/SkillEnum
struct  SkillEnum_t2332882726 
{
public:
	// System.Int32 SkillAbility/SkillEnum::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SkillEnum_t2332882726, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKILLENUM_T2332882726_H
#ifndef RAYCASTRESULT_T3360306849_H
#define RAYCASTRESULT_T3360306849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t3360306849 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_t1113636619 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_t4150874583 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t3722313464  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t3722313464  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_t2156229523  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___m_GameObject_0)); }
	inline GameObject_t1113636619 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_t1113636619 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_t1113636619 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___module_1)); }
	inline BaseRaycaster_t4150874583 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_t4150874583 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_t4150874583 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___worldPosition_7)); }
	inline Vector3_t3722313464  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t3722313464 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t3722313464  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___worldNormal_8)); }
	inline Vector3_t3722313464  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t3722313464 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t3722313464  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t3360306849, ___screenPosition_9)); }
	inline Vector2_t2156229523  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_t2156229523 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_t2156229523  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3360306849_marshaled_pinvoke
{
	GameObject_t1113636619 * ___m_GameObject_0;
	BaseRaycaster_t4150874583 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t3722313464  ___worldPosition_7;
	Vector3_t3722313464  ___worldNormal_8;
	Vector2_t2156229523  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t3360306849_marshaled_com
{
	GameObject_t1113636619 * ___m_GameObject_0;
	BaseRaycaster_t4150874583 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t3722313464  ___worldPosition_7;
	Vector3_t3722313464  ___worldNormal_8;
	Vector2_t2156229523  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T3360306849_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef INPUTBUTTON_T3704011348_H
#define INPUTBUTTON_T3704011348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData/InputButton
struct  InputButton_t3704011348 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData/InputButton::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputButton_t3704011348, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTBUTTON_T3704011348_H
#ifndef COROUTINE_T3829159415_H
#define COROUTINE_T3829159415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t3829159415  : public YieldInstruction_t403091072
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t3829159415, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T3829159415_H
#ifndef BLOCKINGOBJECTS_T612090948_H
#define BLOCKINGOBJECTS_T612090948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GraphicRaycaster/BlockingObjects
struct  BlockingObjects_t612090948 
{
public:
	// System.Int32 UnityEngine.UI.GraphicRaycaster/BlockingObjects::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlockingObjects_t612090948, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKINGOBJECTS_T612090948_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef KEYVALUEPAIR_2_T2565240045_H
#define KEYVALUEPAIR_2_T2565240045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<SkillAbility/SkillEnum,System.Object>
struct  KeyValuePair_2_t2565240045 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2565240045, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2565240045, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2565240045_H
#ifndef FONT_T1956802104_H
#define FONT_T1956802104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Font
struct  Font_t1956802104  : public Object_t631007953
{
public:
	// UnityEngine.Font/FontTextureRebuildCallback UnityEngine.Font::m_FontTextureRebuildCallback
	FontTextureRebuildCallback_t2467502454 * ___m_FontTextureRebuildCallback_3;

public:
	inline static int32_t get_offset_of_m_FontTextureRebuildCallback_3() { return static_cast<int32_t>(offsetof(Font_t1956802104, ___m_FontTextureRebuildCallback_3)); }
	inline FontTextureRebuildCallback_t2467502454 * get_m_FontTextureRebuildCallback_3() const { return ___m_FontTextureRebuildCallback_3; }
	inline FontTextureRebuildCallback_t2467502454 ** get_address_of_m_FontTextureRebuildCallback_3() { return &___m_FontTextureRebuildCallback_3; }
	inline void set_m_FontTextureRebuildCallback_3(FontTextureRebuildCallback_t2467502454 * value)
	{
		___m_FontTextureRebuildCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontTextureRebuildCallback_3), value);
	}
};

struct Font_t1956802104_StaticFields
{
public:
	// System.Action`1<UnityEngine.Font> UnityEngine.Font::textureRebuilt
	Action_1_t2129269699 * ___textureRebuilt_2;

public:
	inline static int32_t get_offset_of_textureRebuilt_2() { return static_cast<int32_t>(offsetof(Font_t1956802104_StaticFields, ___textureRebuilt_2)); }
	inline Action_1_t2129269699 * get_textureRebuilt_2() const { return ___textureRebuilt_2; }
	inline Action_1_t2129269699 ** get_address_of_textureRebuilt_2() { return &___textureRebuilt_2; }
	inline void set_textureRebuilt_2(Action_1_t2129269699 * value)
	{
		___textureRebuilt_2 = value;
		Il2CppCodeGenWriteBarrier((&___textureRebuilt_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONT_T1956802104_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef TEXTURE_T3661962703_H
#define TEXTURE_T3661962703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t3661962703  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T3661962703_H
#ifndef KEYVALUEPAIR_2_T2835606764_H
#define KEYVALUEPAIR_2_T2835606764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<SkillAbility/SkillEnum,SkillAbility/SkillValue>
struct  KeyValuePair_2_t2835606764 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	SkillValue_t3350472883 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2835606764, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2835606764, ___value_1)); }
	inline SkillValue_t3350472883 * get_value_1() const { return ___value_1; }
	inline SkillValue_t3350472883 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(SkillValue_t3350472883 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2835606764_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t426314064 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t426314064 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef MATERIAL_T340375123_H
#define MATERIAL_T340375123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t340375123  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T340375123_H
#ifndef SKILLVALUE_T3350472883_H
#define SKILLVALUE_T3350472883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SkillAbility/SkillValue
struct  SkillValue_t3350472883  : public RuntimeObject
{
public:
	// SkillAbility/SkillEnum SkillAbility/SkillValue::ID
	int32_t ___ID_0;
	// System.Single SkillAbility/SkillValue::value
	float ___value_1;
	// SkillAbility/SkillAbilityCallBack SkillAbility/SkillValue::callback
	SkillAbilityCallBack_t2618462130 * ___callback_2;
	// System.Single SkillAbility/SkillValue::callbackArg
	float ___callbackArg_3;

public:
	inline static int32_t get_offset_of_ID_0() { return static_cast<int32_t>(offsetof(SkillValue_t3350472883, ___ID_0)); }
	inline int32_t get_ID_0() const { return ___ID_0; }
	inline int32_t* get_address_of_ID_0() { return &___ID_0; }
	inline void set_ID_0(int32_t value)
	{
		___ID_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(SkillValue_t3350472883, ___value_1)); }
	inline float get_value_1() const { return ___value_1; }
	inline float* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(float value)
	{
		___value_1 = value;
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(SkillValue_t3350472883, ___callback_2)); }
	inline SkillAbilityCallBack_t2618462130 * get_callback_2() const { return ___callback_2; }
	inline SkillAbilityCallBack_t2618462130 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(SkillAbilityCallBack_t2618462130 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}

	inline static int32_t get_offset_of_callbackArg_3() { return static_cast<int32_t>(offsetof(SkillValue_t3350472883, ___callbackArg_3)); }
	inline float get_callbackArg_3() const { return ___callbackArg_3; }
	inline float* get_address_of_callbackArg_3() { return &___callbackArg_3; }
	inline void set_callbackArg_3(float value)
	{
		___callbackArg_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKILLVALUE_T3350472883_H
#ifndef POINTEREVENTDATA_T3807901092_H
#define POINTEREVENTDATA_T3807901092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData
struct  PointerEventData_t3807901092  : public BaseEventData_t3903027533
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerEnter>k__BackingField
	GameObject_t1113636619 * ___U3CpointerEnterU3Ek__BackingField_2;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::m_PointerPress
	GameObject_t1113636619 * ___m_PointerPress_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<lastPress>k__BackingField
	GameObject_t1113636619 * ___U3ClastPressU3Ek__BackingField_4;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<rawPointerPress>k__BackingField
	GameObject_t1113636619 * ___U3CrawPointerPressU3Ek__BackingField_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerDrag>k__BackingField
	GameObject_t1113636619 * ___U3CpointerDragU3Ek__BackingField_6;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerCurrentRaycast>k__BackingField
	RaycastResult_t3360306849  ___U3CpointerCurrentRaycastU3Ek__BackingField_7;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerPressRaycast>k__BackingField
	RaycastResult_t3360306849  ___U3CpointerPressRaycastU3Ek__BackingField_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.EventSystems.PointerEventData::hovered
	List_1_t2585711361 * ___hovered_9;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<eligibleForClick>k__BackingField
	bool ___U3CeligibleForClickU3Ek__BackingField_10;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<pointerId>k__BackingField
	int32_t ___U3CpointerIdU3Ek__BackingField_11;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<position>k__BackingField
	Vector2_t2156229523  ___U3CpositionU3Ek__BackingField_12;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<delta>k__BackingField
	Vector2_t2156229523  ___U3CdeltaU3Ek__BackingField_13;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<pressPosition>k__BackingField
	Vector2_t2156229523  ___U3CpressPositionU3Ek__BackingField_14;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldPosition>k__BackingField
	Vector3_t3722313464  ___U3CworldPositionU3Ek__BackingField_15;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldNormal>k__BackingField
	Vector3_t3722313464  ___U3CworldNormalU3Ek__BackingField_16;
	// System.Single UnityEngine.EventSystems.PointerEventData::<clickTime>k__BackingField
	float ___U3CclickTimeU3Ek__BackingField_17;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<clickCount>k__BackingField
	int32_t ___U3CclickCountU3Ek__BackingField_18;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<scrollDelta>k__BackingField
	Vector2_t2156229523  ___U3CscrollDeltaU3Ek__BackingField_19;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<useDragThreshold>k__BackingField
	bool ___U3CuseDragThresholdU3Ek__BackingField_20;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<dragging>k__BackingField
	bool ___U3CdraggingU3Ek__BackingField_21;
	// UnityEngine.EventSystems.PointerEventData/InputButton UnityEngine.EventSystems.PointerEventData::<button>k__BackingField
	int32_t ___U3CbuttonU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_U3CpointerEnterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerEnterU3Ek__BackingField_2)); }
	inline GameObject_t1113636619 * get_U3CpointerEnterU3Ek__BackingField_2() const { return ___U3CpointerEnterU3Ek__BackingField_2; }
	inline GameObject_t1113636619 ** get_address_of_U3CpointerEnterU3Ek__BackingField_2() { return &___U3CpointerEnterU3Ek__BackingField_2; }
	inline void set_U3CpointerEnterU3Ek__BackingField_2(GameObject_t1113636619 * value)
	{
		___U3CpointerEnterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerEnterU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_m_PointerPress_3() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___m_PointerPress_3)); }
	inline GameObject_t1113636619 * get_m_PointerPress_3() const { return ___m_PointerPress_3; }
	inline GameObject_t1113636619 ** get_address_of_m_PointerPress_3() { return &___m_PointerPress_3; }
	inline void set_m_PointerPress_3(GameObject_t1113636619 * value)
	{
		___m_PointerPress_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerPress_3), value);
	}

	inline static int32_t get_offset_of_U3ClastPressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3ClastPressU3Ek__BackingField_4)); }
	inline GameObject_t1113636619 * get_U3ClastPressU3Ek__BackingField_4() const { return ___U3ClastPressU3Ek__BackingField_4; }
	inline GameObject_t1113636619 ** get_address_of_U3ClastPressU3Ek__BackingField_4() { return &___U3ClastPressU3Ek__BackingField_4; }
	inline void set_U3ClastPressU3Ek__BackingField_4(GameObject_t1113636619 * value)
	{
		___U3ClastPressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClastPressU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CrawPointerPressU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CrawPointerPressU3Ek__BackingField_5)); }
	inline GameObject_t1113636619 * get_U3CrawPointerPressU3Ek__BackingField_5() const { return ___U3CrawPointerPressU3Ek__BackingField_5; }
	inline GameObject_t1113636619 ** get_address_of_U3CrawPointerPressU3Ek__BackingField_5() { return &___U3CrawPointerPressU3Ek__BackingField_5; }
	inline void set_U3CrawPointerPressU3Ek__BackingField_5(GameObject_t1113636619 * value)
	{
		___U3CrawPointerPressU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrawPointerPressU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CpointerDragU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerDragU3Ek__BackingField_6)); }
	inline GameObject_t1113636619 * get_U3CpointerDragU3Ek__BackingField_6() const { return ___U3CpointerDragU3Ek__BackingField_6; }
	inline GameObject_t1113636619 ** get_address_of_U3CpointerDragU3Ek__BackingField_6() { return &___U3CpointerDragU3Ek__BackingField_6; }
	inline void set_U3CpointerDragU3Ek__BackingField_6(GameObject_t1113636619 * value)
	{
		___U3CpointerDragU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerDragU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerCurrentRaycastU3Ek__BackingField_7)); }
	inline RaycastResult_t3360306849  get_U3CpointerCurrentRaycastU3Ek__BackingField_7() const { return ___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline RaycastResult_t3360306849 * get_address_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return &___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline void set_U3CpointerCurrentRaycastU3Ek__BackingField_7(RaycastResult_t3360306849  value)
	{
		___U3CpointerCurrentRaycastU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerPressRaycastU3Ek__BackingField_8)); }
	inline RaycastResult_t3360306849  get_U3CpointerPressRaycastU3Ek__BackingField_8() const { return ___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline RaycastResult_t3360306849 * get_address_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return &___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline void set_U3CpointerPressRaycastU3Ek__BackingField_8(RaycastResult_t3360306849  value)
	{
		___U3CpointerPressRaycastU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_hovered_9() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___hovered_9)); }
	inline List_1_t2585711361 * get_hovered_9() const { return ___hovered_9; }
	inline List_1_t2585711361 ** get_address_of_hovered_9() { return &___hovered_9; }
	inline void set_hovered_9(List_1_t2585711361 * value)
	{
		___hovered_9 = value;
		Il2CppCodeGenWriteBarrier((&___hovered_9), value);
	}

	inline static int32_t get_offset_of_U3CeligibleForClickU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CeligibleForClickU3Ek__BackingField_10)); }
	inline bool get_U3CeligibleForClickU3Ek__BackingField_10() const { return ___U3CeligibleForClickU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CeligibleForClickU3Ek__BackingField_10() { return &___U3CeligibleForClickU3Ek__BackingField_10; }
	inline void set_U3CeligibleForClickU3Ek__BackingField_10(bool value)
	{
		___U3CeligibleForClickU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CpointerIdU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpointerIdU3Ek__BackingField_11)); }
	inline int32_t get_U3CpointerIdU3Ek__BackingField_11() const { return ___U3CpointerIdU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CpointerIdU3Ek__BackingField_11() { return &___U3CpointerIdU3Ek__BackingField_11; }
	inline void set_U3CpointerIdU3Ek__BackingField_11(int32_t value)
	{
		___U3CpointerIdU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpositionU3Ek__BackingField_12)); }
	inline Vector2_t2156229523  get_U3CpositionU3Ek__BackingField_12() const { return ___U3CpositionU3Ek__BackingField_12; }
	inline Vector2_t2156229523 * get_address_of_U3CpositionU3Ek__BackingField_12() { return &___U3CpositionU3Ek__BackingField_12; }
	inline void set_U3CpositionU3Ek__BackingField_12(Vector2_t2156229523  value)
	{
		___U3CpositionU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CdeltaU3Ek__BackingField_13)); }
	inline Vector2_t2156229523  get_U3CdeltaU3Ek__BackingField_13() const { return ___U3CdeltaU3Ek__BackingField_13; }
	inline Vector2_t2156229523 * get_address_of_U3CdeltaU3Ek__BackingField_13() { return &___U3CdeltaU3Ek__BackingField_13; }
	inline void set_U3CdeltaU3Ek__BackingField_13(Vector2_t2156229523  value)
	{
		___U3CdeltaU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CpressPositionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CpressPositionU3Ek__BackingField_14)); }
	inline Vector2_t2156229523  get_U3CpressPositionU3Ek__BackingField_14() const { return ___U3CpressPositionU3Ek__BackingField_14; }
	inline Vector2_t2156229523 * get_address_of_U3CpressPositionU3Ek__BackingField_14() { return &___U3CpressPositionU3Ek__BackingField_14; }
	inline void set_U3CpressPositionU3Ek__BackingField_14(Vector2_t2156229523  value)
	{
		___U3CpressPositionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CworldPositionU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CworldPositionU3Ek__BackingField_15)); }
	inline Vector3_t3722313464  get_U3CworldPositionU3Ek__BackingField_15() const { return ___U3CworldPositionU3Ek__BackingField_15; }
	inline Vector3_t3722313464 * get_address_of_U3CworldPositionU3Ek__BackingField_15() { return &___U3CworldPositionU3Ek__BackingField_15; }
	inline void set_U3CworldPositionU3Ek__BackingField_15(Vector3_t3722313464  value)
	{
		___U3CworldPositionU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CworldNormalU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CworldNormalU3Ek__BackingField_16)); }
	inline Vector3_t3722313464  get_U3CworldNormalU3Ek__BackingField_16() const { return ___U3CworldNormalU3Ek__BackingField_16; }
	inline Vector3_t3722313464 * get_address_of_U3CworldNormalU3Ek__BackingField_16() { return &___U3CworldNormalU3Ek__BackingField_16; }
	inline void set_U3CworldNormalU3Ek__BackingField_16(Vector3_t3722313464  value)
	{
		___U3CworldNormalU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CclickTimeU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CclickTimeU3Ek__BackingField_17)); }
	inline float get_U3CclickTimeU3Ek__BackingField_17() const { return ___U3CclickTimeU3Ek__BackingField_17; }
	inline float* get_address_of_U3CclickTimeU3Ek__BackingField_17() { return &___U3CclickTimeU3Ek__BackingField_17; }
	inline void set_U3CclickTimeU3Ek__BackingField_17(float value)
	{
		___U3CclickTimeU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CclickCountU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CclickCountU3Ek__BackingField_18)); }
	inline int32_t get_U3CclickCountU3Ek__BackingField_18() const { return ___U3CclickCountU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CclickCountU3Ek__BackingField_18() { return &___U3CclickCountU3Ek__BackingField_18; }
	inline void set_U3CclickCountU3Ek__BackingField_18(int32_t value)
	{
		___U3CclickCountU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CscrollDeltaU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CscrollDeltaU3Ek__BackingField_19)); }
	inline Vector2_t2156229523  get_U3CscrollDeltaU3Ek__BackingField_19() const { return ___U3CscrollDeltaU3Ek__BackingField_19; }
	inline Vector2_t2156229523 * get_address_of_U3CscrollDeltaU3Ek__BackingField_19() { return &___U3CscrollDeltaU3Ek__BackingField_19; }
	inline void set_U3CscrollDeltaU3Ek__BackingField_19(Vector2_t2156229523  value)
	{
		___U3CscrollDeltaU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CuseDragThresholdU3Ek__BackingField_20)); }
	inline bool get_U3CuseDragThresholdU3Ek__BackingField_20() const { return ___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseDragThresholdU3Ek__BackingField_20() { return &___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline void set_U3CuseDragThresholdU3Ek__BackingField_20(bool value)
	{
		___U3CuseDragThresholdU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CdraggingU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CdraggingU3Ek__BackingField_21)); }
	inline bool get_U3CdraggingU3Ek__BackingField_21() const { return ___U3CdraggingU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CdraggingU3Ek__BackingField_21() { return &___U3CdraggingU3Ek__BackingField_21; }
	inline void set_U3CdraggingU3Ek__BackingField_21(bool value)
	{
		___U3CdraggingU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CbuttonU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PointerEventData_t3807901092, ___U3CbuttonU3Ek__BackingField_22)); }
	inline int32_t get_U3CbuttonU3Ek__BackingField_22() const { return ___U3CbuttonU3Ek__BackingField_22; }
	inline int32_t* get_address_of_U3CbuttonU3Ek__BackingField_22() { return &___U3CbuttonU3Ek__BackingField_22; }
	inline void set_U3CbuttonU3Ek__BackingField_22(int32_t value)
	{
		___U3CbuttonU3Ek__BackingField_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTEREVENTDATA_T3807901092_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef GUISTYLE_T3956901511_H
#define GUISTYLE_T3956901511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIStyle
struct  GUIStyle_t3956901511  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.GUIStyle::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Normal
	GUIStyleState_t1397964415 * ___m_Normal_1;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Hover
	GUIStyleState_t1397964415 * ___m_Hover_2;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Active
	GUIStyleState_t1397964415 * ___m_Active_3;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Focused
	GUIStyleState_t1397964415 * ___m_Focused_4;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnNormal
	GUIStyleState_t1397964415 * ___m_OnNormal_5;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnHover
	GUIStyleState_t1397964415 * ___m_OnHover_6;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnActive
	GUIStyleState_t1397964415 * ___m_OnActive_7;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnFocused
	GUIStyleState_t1397964415 * ___m_OnFocused_8;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Border
	RectOffset_t1369453676 * ___m_Border_9;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Padding
	RectOffset_t1369453676 * ___m_Padding_10;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Margin
	RectOffset_t1369453676 * ___m_Margin_11;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Overflow
	RectOffset_t1369453676 * ___m_Overflow_12;
	// UnityEngine.Font UnityEngine.GUIStyle::m_FontInternal
	Font_t1956802104 * ___m_FontInternal_13;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Normal_1)); }
	inline GUIStyleState_t1397964415 * get_m_Normal_1() const { return ___m_Normal_1; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(GUIStyleState_t1397964415 * value)
	{
		___m_Normal_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normal_1), value);
	}

	inline static int32_t get_offset_of_m_Hover_2() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Hover_2)); }
	inline GUIStyleState_t1397964415 * get_m_Hover_2() const { return ___m_Hover_2; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_Hover_2() { return &___m_Hover_2; }
	inline void set_m_Hover_2(GUIStyleState_t1397964415 * value)
	{
		___m_Hover_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Hover_2), value);
	}

	inline static int32_t get_offset_of_m_Active_3() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Active_3)); }
	inline GUIStyleState_t1397964415 * get_m_Active_3() const { return ___m_Active_3; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_Active_3() { return &___m_Active_3; }
	inline void set_m_Active_3(GUIStyleState_t1397964415 * value)
	{
		___m_Active_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Active_3), value);
	}

	inline static int32_t get_offset_of_m_Focused_4() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Focused_4)); }
	inline GUIStyleState_t1397964415 * get_m_Focused_4() const { return ___m_Focused_4; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_Focused_4() { return &___m_Focused_4; }
	inline void set_m_Focused_4(GUIStyleState_t1397964415 * value)
	{
		___m_Focused_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Focused_4), value);
	}

	inline static int32_t get_offset_of_m_OnNormal_5() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_OnNormal_5)); }
	inline GUIStyleState_t1397964415 * get_m_OnNormal_5() const { return ___m_OnNormal_5; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_OnNormal_5() { return &___m_OnNormal_5; }
	inline void set_m_OnNormal_5(GUIStyleState_t1397964415 * value)
	{
		___m_OnNormal_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnNormal_5), value);
	}

	inline static int32_t get_offset_of_m_OnHover_6() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_OnHover_6)); }
	inline GUIStyleState_t1397964415 * get_m_OnHover_6() const { return ___m_OnHover_6; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_OnHover_6() { return &___m_OnHover_6; }
	inline void set_m_OnHover_6(GUIStyleState_t1397964415 * value)
	{
		___m_OnHover_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnHover_6), value);
	}

	inline static int32_t get_offset_of_m_OnActive_7() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_OnActive_7)); }
	inline GUIStyleState_t1397964415 * get_m_OnActive_7() const { return ___m_OnActive_7; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_OnActive_7() { return &___m_OnActive_7; }
	inline void set_m_OnActive_7(GUIStyleState_t1397964415 * value)
	{
		___m_OnActive_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnActive_7), value);
	}

	inline static int32_t get_offset_of_m_OnFocused_8() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_OnFocused_8)); }
	inline GUIStyleState_t1397964415 * get_m_OnFocused_8() const { return ___m_OnFocused_8; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_OnFocused_8() { return &___m_OnFocused_8; }
	inline void set_m_OnFocused_8(GUIStyleState_t1397964415 * value)
	{
		___m_OnFocused_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnFocused_8), value);
	}

	inline static int32_t get_offset_of_m_Border_9() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Border_9)); }
	inline RectOffset_t1369453676 * get_m_Border_9() const { return ___m_Border_9; }
	inline RectOffset_t1369453676 ** get_address_of_m_Border_9() { return &___m_Border_9; }
	inline void set_m_Border_9(RectOffset_t1369453676 * value)
	{
		___m_Border_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Border_9), value);
	}

	inline static int32_t get_offset_of_m_Padding_10() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Padding_10)); }
	inline RectOffset_t1369453676 * get_m_Padding_10() const { return ___m_Padding_10; }
	inline RectOffset_t1369453676 ** get_address_of_m_Padding_10() { return &___m_Padding_10; }
	inline void set_m_Padding_10(RectOffset_t1369453676 * value)
	{
		___m_Padding_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_10), value);
	}

	inline static int32_t get_offset_of_m_Margin_11() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Margin_11)); }
	inline RectOffset_t1369453676 * get_m_Margin_11() const { return ___m_Margin_11; }
	inline RectOffset_t1369453676 ** get_address_of_m_Margin_11() { return &___m_Margin_11; }
	inline void set_m_Margin_11(RectOffset_t1369453676 * value)
	{
		___m_Margin_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Margin_11), value);
	}

	inline static int32_t get_offset_of_m_Overflow_12() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Overflow_12)); }
	inline RectOffset_t1369453676 * get_m_Overflow_12() const { return ___m_Overflow_12; }
	inline RectOffset_t1369453676 ** get_address_of_m_Overflow_12() { return &___m_Overflow_12; }
	inline void set_m_Overflow_12(RectOffset_t1369453676 * value)
	{
		___m_Overflow_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Overflow_12), value);
	}

	inline static int32_t get_offset_of_m_FontInternal_13() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_FontInternal_13)); }
	inline Font_t1956802104 * get_m_FontInternal_13() const { return ___m_FontInternal_13; }
	inline Font_t1956802104 ** get_address_of_m_FontInternal_13() { return &___m_FontInternal_13; }
	inline void set_m_FontInternal_13(Font_t1956802104 * value)
	{
		___m_FontInternal_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontInternal_13), value);
	}
};

struct GUIStyle_t3956901511_StaticFields
{
public:
	// System.Boolean UnityEngine.GUIStyle::showKeyboardFocus
	bool ___showKeyboardFocus_14;
	// UnityEngine.GUIStyle UnityEngine.GUIStyle::s_None
	GUIStyle_t3956901511 * ___s_None_15;

public:
	inline static int32_t get_offset_of_showKeyboardFocus_14() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511_StaticFields, ___showKeyboardFocus_14)); }
	inline bool get_showKeyboardFocus_14() const { return ___showKeyboardFocus_14; }
	inline bool* get_address_of_showKeyboardFocus_14() { return &___showKeyboardFocus_14; }
	inline void set_showKeyboardFocus_14(bool value)
	{
		___showKeyboardFocus_14 = value;
	}

	inline static int32_t get_offset_of_s_None_15() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511_StaticFields, ___s_None_15)); }
	inline GUIStyle_t3956901511 * get_s_None_15() const { return ___s_None_15; }
	inline GUIStyle_t3956901511 ** get_address_of_s_None_15() { return &___s_None_15; }
	inline void set_s_None_15(GUIStyle_t3956901511 * value)
	{
		___s_None_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_None_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.GUIStyle
struct GUIStyle_t3956901511_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_Normal_1;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_Hover_2;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_Active_3;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_Focused_4;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_OnNormal_5;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_OnHover_6;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_OnActive_7;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_OnFocused_8;
	RectOffset_t1369453676_marshaled_pinvoke ___m_Border_9;
	RectOffset_t1369453676_marshaled_pinvoke ___m_Padding_10;
	RectOffset_t1369453676_marshaled_pinvoke ___m_Margin_11;
	RectOffset_t1369453676_marshaled_pinvoke ___m_Overflow_12;
	Font_t1956802104 * ___m_FontInternal_13;
};
// Native definition for COM marshalling of UnityEngine.GUIStyle
struct GUIStyle_t3956901511_marshaled_com
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t1397964415_marshaled_com* ___m_Normal_1;
	GUIStyleState_t1397964415_marshaled_com* ___m_Hover_2;
	GUIStyleState_t1397964415_marshaled_com* ___m_Active_3;
	GUIStyleState_t1397964415_marshaled_com* ___m_Focused_4;
	GUIStyleState_t1397964415_marshaled_com* ___m_OnNormal_5;
	GUIStyleState_t1397964415_marshaled_com* ___m_OnHover_6;
	GUIStyleState_t1397964415_marshaled_com* ___m_OnActive_7;
	GUIStyleState_t1397964415_marshaled_com* ___m_OnFocused_8;
	RectOffset_t1369453676_marshaled_com* ___m_Border_9;
	RectOffset_t1369453676_marshaled_com* ___m_Padding_10;
	RectOffset_t1369453676_marshaled_com* ___m_Margin_11;
	RectOffset_t1369453676_marshaled_com* ___m_Overflow_12;
	Font_t1956802104 * ___m_FontInternal_13;
};
#endif // GUISTYLE_T3956901511_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef SPRITE_T280657092_H
#define SPRITE_T280657092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprite
struct  Sprite_t280657092  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T280657092_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef TERRAINDATA_T657004131_H
#define TERRAINDATA_T657004131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TerrainData
struct  TerrainData_t657004131  : public Object_t631007953
{
public:

public:
};

struct TerrainData_t657004131_StaticFields
{
public:
	// System.Int32 UnityEngine.TerrainData::k_MaximumResolution
	int32_t ___k_MaximumResolution_2;
	// System.Int32 UnityEngine.TerrainData::k_MinimumDetailResolutionPerPatch
	int32_t ___k_MinimumDetailResolutionPerPatch_3;
	// System.Int32 UnityEngine.TerrainData::k_MaximumDetailResolutionPerPatch
	int32_t ___k_MaximumDetailResolutionPerPatch_4;
	// System.Int32 UnityEngine.TerrainData::k_MaximumDetailPatchCount
	int32_t ___k_MaximumDetailPatchCount_5;
	// System.Int32 UnityEngine.TerrainData::k_MinimumAlphamapResolution
	int32_t ___k_MinimumAlphamapResolution_6;
	// System.Int32 UnityEngine.TerrainData::k_MaximumAlphamapResolution
	int32_t ___k_MaximumAlphamapResolution_7;
	// System.Int32 UnityEngine.TerrainData::k_MinimumBaseMapResolution
	int32_t ___k_MinimumBaseMapResolution_8;
	// System.Int32 UnityEngine.TerrainData::k_MaximumBaseMapResolution
	int32_t ___k_MaximumBaseMapResolution_9;

public:
	inline static int32_t get_offset_of_k_MaximumResolution_2() { return static_cast<int32_t>(offsetof(TerrainData_t657004131_StaticFields, ___k_MaximumResolution_2)); }
	inline int32_t get_k_MaximumResolution_2() const { return ___k_MaximumResolution_2; }
	inline int32_t* get_address_of_k_MaximumResolution_2() { return &___k_MaximumResolution_2; }
	inline void set_k_MaximumResolution_2(int32_t value)
	{
		___k_MaximumResolution_2 = value;
	}

	inline static int32_t get_offset_of_k_MinimumDetailResolutionPerPatch_3() { return static_cast<int32_t>(offsetof(TerrainData_t657004131_StaticFields, ___k_MinimumDetailResolutionPerPatch_3)); }
	inline int32_t get_k_MinimumDetailResolutionPerPatch_3() const { return ___k_MinimumDetailResolutionPerPatch_3; }
	inline int32_t* get_address_of_k_MinimumDetailResolutionPerPatch_3() { return &___k_MinimumDetailResolutionPerPatch_3; }
	inline void set_k_MinimumDetailResolutionPerPatch_3(int32_t value)
	{
		___k_MinimumDetailResolutionPerPatch_3 = value;
	}

	inline static int32_t get_offset_of_k_MaximumDetailResolutionPerPatch_4() { return static_cast<int32_t>(offsetof(TerrainData_t657004131_StaticFields, ___k_MaximumDetailResolutionPerPatch_4)); }
	inline int32_t get_k_MaximumDetailResolutionPerPatch_4() const { return ___k_MaximumDetailResolutionPerPatch_4; }
	inline int32_t* get_address_of_k_MaximumDetailResolutionPerPatch_4() { return &___k_MaximumDetailResolutionPerPatch_4; }
	inline void set_k_MaximumDetailResolutionPerPatch_4(int32_t value)
	{
		___k_MaximumDetailResolutionPerPatch_4 = value;
	}

	inline static int32_t get_offset_of_k_MaximumDetailPatchCount_5() { return static_cast<int32_t>(offsetof(TerrainData_t657004131_StaticFields, ___k_MaximumDetailPatchCount_5)); }
	inline int32_t get_k_MaximumDetailPatchCount_5() const { return ___k_MaximumDetailPatchCount_5; }
	inline int32_t* get_address_of_k_MaximumDetailPatchCount_5() { return &___k_MaximumDetailPatchCount_5; }
	inline void set_k_MaximumDetailPatchCount_5(int32_t value)
	{
		___k_MaximumDetailPatchCount_5 = value;
	}

	inline static int32_t get_offset_of_k_MinimumAlphamapResolution_6() { return static_cast<int32_t>(offsetof(TerrainData_t657004131_StaticFields, ___k_MinimumAlphamapResolution_6)); }
	inline int32_t get_k_MinimumAlphamapResolution_6() const { return ___k_MinimumAlphamapResolution_6; }
	inline int32_t* get_address_of_k_MinimumAlphamapResolution_6() { return &___k_MinimumAlphamapResolution_6; }
	inline void set_k_MinimumAlphamapResolution_6(int32_t value)
	{
		___k_MinimumAlphamapResolution_6 = value;
	}

	inline static int32_t get_offset_of_k_MaximumAlphamapResolution_7() { return static_cast<int32_t>(offsetof(TerrainData_t657004131_StaticFields, ___k_MaximumAlphamapResolution_7)); }
	inline int32_t get_k_MaximumAlphamapResolution_7() const { return ___k_MaximumAlphamapResolution_7; }
	inline int32_t* get_address_of_k_MaximumAlphamapResolution_7() { return &___k_MaximumAlphamapResolution_7; }
	inline void set_k_MaximumAlphamapResolution_7(int32_t value)
	{
		___k_MaximumAlphamapResolution_7 = value;
	}

	inline static int32_t get_offset_of_k_MinimumBaseMapResolution_8() { return static_cast<int32_t>(offsetof(TerrainData_t657004131_StaticFields, ___k_MinimumBaseMapResolution_8)); }
	inline int32_t get_k_MinimumBaseMapResolution_8() const { return ___k_MinimumBaseMapResolution_8; }
	inline int32_t* get_address_of_k_MinimumBaseMapResolution_8() { return &___k_MinimumBaseMapResolution_8; }
	inline void set_k_MinimumBaseMapResolution_8(int32_t value)
	{
		___k_MinimumBaseMapResolution_8 = value;
	}

	inline static int32_t get_offset_of_k_MaximumBaseMapResolution_9() { return static_cast<int32_t>(offsetof(TerrainData_t657004131_StaticFields, ___k_MaximumBaseMapResolution_9)); }
	inline int32_t get_k_MaximumBaseMapResolution_9() const { return ___k_MaximumBaseMapResolution_9; }
	inline int32_t* get_address_of_k_MaximumBaseMapResolution_9() { return &___k_MaximumBaseMapResolution_9; }
	inline void set_k_MaximumBaseMapResolution_9(int32_t value)
	{
		___k_MaximumBaseMapResolution_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERRAINDATA_T657004131_H
#ifndef TOUCH_T1921856868_H
#define TOUCH_T1921856868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t1921856868 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t2156229523  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t2156229523  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t2156229523  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Position_1)); }
	inline Vector2_t2156229523  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_t2156229523 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_t2156229523  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_RawPosition_2)); }
	inline Vector2_t2156229523  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_t2156229523 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_t2156229523  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_PositionDelta_3)); }
	inline Vector2_t2156229523  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_t2156229523 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_t2156229523  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T1921856868_H
#ifndef RIGIDBODY_T3916780224_H
#define RIGIDBODY_T3916780224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody
struct  Rigidbody_t3916780224  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY_T3916780224_H
#ifndef SKILLABILITYCALLBACK_T2618462130_H
#define SKILLABILITYCALLBACK_T2618462130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SkillAbility/SkillAbilityCallBack
struct  SkillAbilityCallBack_t2618462130  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKILLABILITYCALLBACK_T2618462130_H
#ifndef RENDERTEXTURE_T2108887433_H
#define RENDERTEXTURE_T2108887433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTexture
struct  RenderTexture_t2108887433  : public Texture_t3661962703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTURE_T2108887433_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef WINDOWFUNCTION_T3146511083_H
#define WINDOWFUNCTION_T3146511083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUI/WindowFunction
struct  WindowFunction_t3146511083  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINDOWFUNCTION_T3146511083_H
#ifndef RENDERER_T2627027031_H
#define RENDERER_T2627027031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t2627027031  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T2627027031_H
#ifndef GUISKIN_T1244372282_H
#define GUISKIN_T1244372282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUISkin
struct  GUISkin_t1244372282  : public ScriptableObject_t2528358522
{
public:
	// UnityEngine.Font UnityEngine.GUISkin::m_Font
	Font_t1956802104 * ___m_Font_2;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_box
	GUIStyle_t3956901511 * ___m_box_3;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_button
	GUIStyle_t3956901511 * ___m_button_4;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_toggle
	GUIStyle_t3956901511 * ___m_toggle_5;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_label
	GUIStyle_t3956901511 * ___m_label_6;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_textField
	GUIStyle_t3956901511 * ___m_textField_7;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_textArea
	GUIStyle_t3956901511 * ___m_textArea_8;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_window
	GUIStyle_t3956901511 * ___m_window_9;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSlider
	GUIStyle_t3956901511 * ___m_horizontalSlider_10;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSliderThumb
	GUIStyle_t3956901511 * ___m_horizontalSliderThumb_11;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSlider
	GUIStyle_t3956901511 * ___m_verticalSlider_12;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSliderThumb
	GUIStyle_t3956901511 * ___m_verticalSliderThumb_13;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbar
	GUIStyle_t3956901511 * ___m_horizontalScrollbar_14;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarThumb
	GUIStyle_t3956901511 * ___m_horizontalScrollbarThumb_15;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarLeftButton
	GUIStyle_t3956901511 * ___m_horizontalScrollbarLeftButton_16;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarRightButton
	GUIStyle_t3956901511 * ___m_horizontalScrollbarRightButton_17;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbar
	GUIStyle_t3956901511 * ___m_verticalScrollbar_18;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarThumb
	GUIStyle_t3956901511 * ___m_verticalScrollbarThumb_19;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarUpButton
	GUIStyle_t3956901511 * ___m_verticalScrollbarUpButton_20;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarDownButton
	GUIStyle_t3956901511 * ___m_verticalScrollbarDownButton_21;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_ScrollView
	GUIStyle_t3956901511 * ___m_ScrollView_22;
	// UnityEngine.GUIStyle[] UnityEngine.GUISkin::m_CustomStyles
	GUIStyleU5BU5D_t2383250302* ___m_CustomStyles_23;
	// UnityEngine.GUISettings UnityEngine.GUISkin::m_Settings
	GUISettings_t1774757634 * ___m_Settings_24;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle> UnityEngine.GUISkin::m_Styles
	Dictionary_2_t3742157810 * ___m_Styles_25;

public:
	inline static int32_t get_offset_of_m_Font_2() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_Font_2)); }
	inline Font_t1956802104 * get_m_Font_2() const { return ___m_Font_2; }
	inline Font_t1956802104 ** get_address_of_m_Font_2() { return &___m_Font_2; }
	inline void set_m_Font_2(Font_t1956802104 * value)
	{
		___m_Font_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Font_2), value);
	}

	inline static int32_t get_offset_of_m_box_3() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_box_3)); }
	inline GUIStyle_t3956901511 * get_m_box_3() const { return ___m_box_3; }
	inline GUIStyle_t3956901511 ** get_address_of_m_box_3() { return &___m_box_3; }
	inline void set_m_box_3(GUIStyle_t3956901511 * value)
	{
		___m_box_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_box_3), value);
	}

	inline static int32_t get_offset_of_m_button_4() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_button_4)); }
	inline GUIStyle_t3956901511 * get_m_button_4() const { return ___m_button_4; }
	inline GUIStyle_t3956901511 ** get_address_of_m_button_4() { return &___m_button_4; }
	inline void set_m_button_4(GUIStyle_t3956901511 * value)
	{
		___m_button_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_button_4), value);
	}

	inline static int32_t get_offset_of_m_toggle_5() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_toggle_5)); }
	inline GUIStyle_t3956901511 * get_m_toggle_5() const { return ___m_toggle_5; }
	inline GUIStyle_t3956901511 ** get_address_of_m_toggle_5() { return &___m_toggle_5; }
	inline void set_m_toggle_5(GUIStyle_t3956901511 * value)
	{
		___m_toggle_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_toggle_5), value);
	}

	inline static int32_t get_offset_of_m_label_6() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_label_6)); }
	inline GUIStyle_t3956901511 * get_m_label_6() const { return ___m_label_6; }
	inline GUIStyle_t3956901511 ** get_address_of_m_label_6() { return &___m_label_6; }
	inline void set_m_label_6(GUIStyle_t3956901511 * value)
	{
		___m_label_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_label_6), value);
	}

	inline static int32_t get_offset_of_m_textField_7() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_textField_7)); }
	inline GUIStyle_t3956901511 * get_m_textField_7() const { return ___m_textField_7; }
	inline GUIStyle_t3956901511 ** get_address_of_m_textField_7() { return &___m_textField_7; }
	inline void set_m_textField_7(GUIStyle_t3956901511 * value)
	{
		___m_textField_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textField_7), value);
	}

	inline static int32_t get_offset_of_m_textArea_8() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_textArea_8)); }
	inline GUIStyle_t3956901511 * get_m_textArea_8() const { return ___m_textArea_8; }
	inline GUIStyle_t3956901511 ** get_address_of_m_textArea_8() { return &___m_textArea_8; }
	inline void set_m_textArea_8(GUIStyle_t3956901511 * value)
	{
		___m_textArea_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_textArea_8), value);
	}

	inline static int32_t get_offset_of_m_window_9() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_window_9)); }
	inline GUIStyle_t3956901511 * get_m_window_9() const { return ___m_window_9; }
	inline GUIStyle_t3956901511 ** get_address_of_m_window_9() { return &___m_window_9; }
	inline void set_m_window_9(GUIStyle_t3956901511 * value)
	{
		___m_window_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_window_9), value);
	}

	inline static int32_t get_offset_of_m_horizontalSlider_10() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalSlider_10)); }
	inline GUIStyle_t3956901511 * get_m_horizontalSlider_10() const { return ___m_horizontalSlider_10; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalSlider_10() { return &___m_horizontalSlider_10; }
	inline void set_m_horizontalSlider_10(GUIStyle_t3956901511 * value)
	{
		___m_horizontalSlider_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalSlider_10), value);
	}

	inline static int32_t get_offset_of_m_horizontalSliderThumb_11() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalSliderThumb_11)); }
	inline GUIStyle_t3956901511 * get_m_horizontalSliderThumb_11() const { return ___m_horizontalSliderThumb_11; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalSliderThumb_11() { return &___m_horizontalSliderThumb_11; }
	inline void set_m_horizontalSliderThumb_11(GUIStyle_t3956901511 * value)
	{
		___m_horizontalSliderThumb_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalSliderThumb_11), value);
	}

	inline static int32_t get_offset_of_m_verticalSlider_12() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalSlider_12)); }
	inline GUIStyle_t3956901511 * get_m_verticalSlider_12() const { return ___m_verticalSlider_12; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalSlider_12() { return &___m_verticalSlider_12; }
	inline void set_m_verticalSlider_12(GUIStyle_t3956901511 * value)
	{
		___m_verticalSlider_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalSlider_12), value);
	}

	inline static int32_t get_offset_of_m_verticalSliderThumb_13() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalSliderThumb_13)); }
	inline GUIStyle_t3956901511 * get_m_verticalSliderThumb_13() const { return ___m_verticalSliderThumb_13; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalSliderThumb_13() { return &___m_verticalSliderThumb_13; }
	inline void set_m_verticalSliderThumb_13(GUIStyle_t3956901511 * value)
	{
		___m_verticalSliderThumb_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalSliderThumb_13), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbar_14() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalScrollbar_14)); }
	inline GUIStyle_t3956901511 * get_m_horizontalScrollbar_14() const { return ___m_horizontalScrollbar_14; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalScrollbar_14() { return &___m_horizontalScrollbar_14; }
	inline void set_m_horizontalScrollbar_14(GUIStyle_t3956901511 * value)
	{
		___m_horizontalScrollbar_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbar_14), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbarThumb_15() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalScrollbarThumb_15)); }
	inline GUIStyle_t3956901511 * get_m_horizontalScrollbarThumb_15() const { return ___m_horizontalScrollbarThumb_15; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalScrollbarThumb_15() { return &___m_horizontalScrollbarThumb_15; }
	inline void set_m_horizontalScrollbarThumb_15(GUIStyle_t3956901511 * value)
	{
		___m_horizontalScrollbarThumb_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbarThumb_15), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbarLeftButton_16() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalScrollbarLeftButton_16)); }
	inline GUIStyle_t3956901511 * get_m_horizontalScrollbarLeftButton_16() const { return ___m_horizontalScrollbarLeftButton_16; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalScrollbarLeftButton_16() { return &___m_horizontalScrollbarLeftButton_16; }
	inline void set_m_horizontalScrollbarLeftButton_16(GUIStyle_t3956901511 * value)
	{
		___m_horizontalScrollbarLeftButton_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbarLeftButton_16), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbarRightButton_17() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalScrollbarRightButton_17)); }
	inline GUIStyle_t3956901511 * get_m_horizontalScrollbarRightButton_17() const { return ___m_horizontalScrollbarRightButton_17; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalScrollbarRightButton_17() { return &___m_horizontalScrollbarRightButton_17; }
	inline void set_m_horizontalScrollbarRightButton_17(GUIStyle_t3956901511 * value)
	{
		___m_horizontalScrollbarRightButton_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbarRightButton_17), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbar_18() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalScrollbar_18)); }
	inline GUIStyle_t3956901511 * get_m_verticalScrollbar_18() const { return ___m_verticalScrollbar_18; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalScrollbar_18() { return &___m_verticalScrollbar_18; }
	inline void set_m_verticalScrollbar_18(GUIStyle_t3956901511 * value)
	{
		___m_verticalScrollbar_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbar_18), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbarThumb_19() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalScrollbarThumb_19)); }
	inline GUIStyle_t3956901511 * get_m_verticalScrollbarThumb_19() const { return ___m_verticalScrollbarThumb_19; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalScrollbarThumb_19() { return &___m_verticalScrollbarThumb_19; }
	inline void set_m_verticalScrollbarThumb_19(GUIStyle_t3956901511 * value)
	{
		___m_verticalScrollbarThumb_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbarThumb_19), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbarUpButton_20() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalScrollbarUpButton_20)); }
	inline GUIStyle_t3956901511 * get_m_verticalScrollbarUpButton_20() const { return ___m_verticalScrollbarUpButton_20; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalScrollbarUpButton_20() { return &___m_verticalScrollbarUpButton_20; }
	inline void set_m_verticalScrollbarUpButton_20(GUIStyle_t3956901511 * value)
	{
		___m_verticalScrollbarUpButton_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbarUpButton_20), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbarDownButton_21() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalScrollbarDownButton_21)); }
	inline GUIStyle_t3956901511 * get_m_verticalScrollbarDownButton_21() const { return ___m_verticalScrollbarDownButton_21; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalScrollbarDownButton_21() { return &___m_verticalScrollbarDownButton_21; }
	inline void set_m_verticalScrollbarDownButton_21(GUIStyle_t3956901511 * value)
	{
		___m_verticalScrollbarDownButton_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbarDownButton_21), value);
	}

	inline static int32_t get_offset_of_m_ScrollView_22() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_ScrollView_22)); }
	inline GUIStyle_t3956901511 * get_m_ScrollView_22() const { return ___m_ScrollView_22; }
	inline GUIStyle_t3956901511 ** get_address_of_m_ScrollView_22() { return &___m_ScrollView_22; }
	inline void set_m_ScrollView_22(GUIStyle_t3956901511 * value)
	{
		___m_ScrollView_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_ScrollView_22), value);
	}

	inline static int32_t get_offset_of_m_CustomStyles_23() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_CustomStyles_23)); }
	inline GUIStyleU5BU5D_t2383250302* get_m_CustomStyles_23() const { return ___m_CustomStyles_23; }
	inline GUIStyleU5BU5D_t2383250302** get_address_of_m_CustomStyles_23() { return &___m_CustomStyles_23; }
	inline void set_m_CustomStyles_23(GUIStyleU5BU5D_t2383250302* value)
	{
		___m_CustomStyles_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomStyles_23), value);
	}

	inline static int32_t get_offset_of_m_Settings_24() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_Settings_24)); }
	inline GUISettings_t1774757634 * get_m_Settings_24() const { return ___m_Settings_24; }
	inline GUISettings_t1774757634 ** get_address_of_m_Settings_24() { return &___m_Settings_24; }
	inline void set_m_Settings_24(GUISettings_t1774757634 * value)
	{
		___m_Settings_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_Settings_24), value);
	}

	inline static int32_t get_offset_of_m_Styles_25() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_Styles_25)); }
	inline Dictionary_2_t3742157810 * get_m_Styles_25() const { return ___m_Styles_25; }
	inline Dictionary_2_t3742157810 ** get_address_of_m_Styles_25() { return &___m_Styles_25; }
	inline void set_m_Styles_25(Dictionary_2_t3742157810 * value)
	{
		___m_Styles_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_Styles_25), value);
	}
};

struct GUISkin_t1244372282_StaticFields
{
public:
	// UnityEngine.GUISkin/SkinChangedDelegate UnityEngine.GUISkin::m_SkinChanged
	SkinChangedDelegate_t1143955295 * ___m_SkinChanged_26;
	// UnityEngine.GUISkin UnityEngine.GUISkin::current
	GUISkin_t1244372282 * ___current_27;

public:
	inline static int32_t get_offset_of_m_SkinChanged_26() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282_StaticFields, ___m_SkinChanged_26)); }
	inline SkinChangedDelegate_t1143955295 * get_m_SkinChanged_26() const { return ___m_SkinChanged_26; }
	inline SkinChangedDelegate_t1143955295 ** get_address_of_m_SkinChanged_26() { return &___m_SkinChanged_26; }
	inline void set_m_SkinChanged_26(SkinChangedDelegate_t1143955295 * value)
	{
		___m_SkinChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_SkinChanged_26), value);
	}

	inline static int32_t get_offset_of_current_27() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282_StaticFields, ___current_27)); }
	inline GUISkin_t1244372282 * get_current_27() const { return ___current_27; }
	inline GUISkin_t1244372282 ** get_address_of_current_27() { return &___current_27; }
	inline void set_current_27(GUISkin_t1244372282 * value)
	{
		___current_27 = value;
		Il2CppCodeGenWriteBarrier((&___current_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUISKIN_T1244372282_H
#ifndef COLLIDER_T1773347010_H
#define COLLIDER_T1773347010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider
struct  Collider_t1773347010  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER_T1773347010_H
#ifndef TEXTURE2D_T3840446185_H
#define TEXTURE2D_T3840446185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t3840446185  : public Texture_t3661962703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T3840446185_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ENUMERATOR_T2121750653_H
#define ENUMERATOR_T2121750653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<SkillAbility/SkillEnum,System.Object>
struct  Enumerator_t2121750653 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t167567878 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t2565240045  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t2121750653, ___dictionary_0)); }
	inline Dictionary_2_t167567878 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t167567878 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t167567878 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2121750653, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t2121750653, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2121750653, ___current_3)); }
	inline KeyValuePair_2_t2565240045  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2565240045 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2565240045  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2121750653_H
#ifndef UNITYACTION_T3245792599_H
#define UNITYACTION_T3245792599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityAction
struct  UnityAction_t3245792599  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYACTION_T3245792599_H
#ifndef ASYNCCALLBACK_T3962456242_H
#define ASYNCCALLBACK_T3962456242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3962456242  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3962456242_H
#ifndef ENUMERATOR_T2392117372_H
#define ENUMERATOR_T2392117372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<SkillAbility/SkillEnum,SkillAbility/SkillValue>
struct  Enumerator_t2392117372 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t437934597 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t2835606764  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t2392117372, ___dictionary_0)); }
	inline Dictionary_2_t437934597 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t437934597 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t437934597 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2392117372, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t2392117372, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2392117372, ___current_3)); }
	inline KeyValuePair_2_t2835606764  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2835606764 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2835606764  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2392117372_H
#ifndef CAMERA_T4157153871_H
#define CAMERA_T4157153871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t4157153871  : public Behaviour_t1437897464
{
public:

public:
};

struct Camera_t4157153871_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t190067161 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t190067161 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t190067161 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t190067161 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t190067161 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t190067161 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t190067161 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t190067161 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t190067161 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t190067161 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t190067161 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t190067161 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T4157153871_H
#ifndef CANVAS_T3310196443_H
#define CANVAS_T3310196443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Canvas
struct  Canvas_t3310196443  : public Behaviour_t1437897464
{
public:

public:
};

struct Canvas_t3310196443_StaticFields
{
public:
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_t3309123499 * ___willRenderCanvases_2;

public:
	inline static int32_t get_offset_of_willRenderCanvases_2() { return static_cast<int32_t>(offsetof(Canvas_t3310196443_StaticFields, ___willRenderCanvases_2)); }
	inline WillRenderCanvases_t3309123499 * get_willRenderCanvases_2() const { return ___willRenderCanvases_2; }
	inline WillRenderCanvases_t3309123499 ** get_address_of_willRenderCanvases_2() { return &___willRenderCanvases_2; }
	inline void set_willRenderCanvases_2(WillRenderCanvases_t3309123499 * value)
	{
		___willRenderCanvases_2 = value;
		Il2CppCodeGenWriteBarrier((&___willRenderCanvases_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVAS_T3310196443_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef FLARELAYER_T1739223323_H
#define FLARELAYER_T1739223323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FlareLayer
struct  FlareLayer_t1739223323  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLARELAYER_T1739223323_H
#ifndef RECTTRANSFORM_T3704657025_H
#define RECTTRANSFORM_T3704657025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t3704657025  : public Transform_t3600365921
{
public:

public:
};

struct RectTransform_t3704657025_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1258266594 * ___reapplyDrivenProperties_2;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_2() { return static_cast<int32_t>(offsetof(RectTransform_t3704657025_StaticFields, ___reapplyDrivenProperties_2)); }
	inline ReapplyDrivenProperties_t1258266594 * get_reapplyDrivenProperties_2() const { return ___reapplyDrivenProperties_2; }
	inline ReapplyDrivenProperties_t1258266594 ** get_address_of_reapplyDrivenProperties_2() { return &___reapplyDrivenProperties_2; }
	inline void set_reapplyDrivenProperties_2(ReapplyDrivenProperties_t1258266594 * value)
	{
		___reapplyDrivenProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T3704657025_H
#ifndef SKYBOX_T2662837510_H
#define SKYBOX_T2662837510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Skybox
struct  Skybox_t2662837510  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKYBOX_T2662837510_H
#ifndef TERRAIN_T3055443660_H
#define TERRAIN_T3055443660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Terrain
struct  Terrain_t3055443660  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERRAIN_T3055443660_H
#ifndef MAINGUI_T2584036754_H
#define MAINGUI_T2584036754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainGUI
struct  MainGUI_t2584036754  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINGUI_T2584036754_H
#ifndef HUDFPS_T4241874542_H
#define HUDFPS_T4241874542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HUDFPS
struct  HUDFPS_t4241874542  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rect HUDFPS::startRect
	Rect_t2360479859  ___startRect_2;
	// System.Boolean HUDFPS::updateColor
	bool ___updateColor_3;
	// System.Boolean HUDFPS::allowDrag
	bool ___allowDrag_4;
	// System.Single HUDFPS::frequency
	float ___frequency_5;
	// System.Int32 HUDFPS::nbDecimal
	int32_t ___nbDecimal_6;
	// System.Single HUDFPS::accum
	float ___accum_7;
	// System.Int32 HUDFPS::frames
	int32_t ___frames_8;
	// UnityEngine.Color HUDFPS::color
	Color_t2555686324  ___color_9;
	// System.String HUDFPS::sFPS
	String_t* ___sFPS_10;
	// UnityEngine.GUIStyle HUDFPS::style
	GUIStyle_t3956901511 * ___style_11;

public:
	inline static int32_t get_offset_of_startRect_2() { return static_cast<int32_t>(offsetof(HUDFPS_t4241874542, ___startRect_2)); }
	inline Rect_t2360479859  get_startRect_2() const { return ___startRect_2; }
	inline Rect_t2360479859 * get_address_of_startRect_2() { return &___startRect_2; }
	inline void set_startRect_2(Rect_t2360479859  value)
	{
		___startRect_2 = value;
	}

	inline static int32_t get_offset_of_updateColor_3() { return static_cast<int32_t>(offsetof(HUDFPS_t4241874542, ___updateColor_3)); }
	inline bool get_updateColor_3() const { return ___updateColor_3; }
	inline bool* get_address_of_updateColor_3() { return &___updateColor_3; }
	inline void set_updateColor_3(bool value)
	{
		___updateColor_3 = value;
	}

	inline static int32_t get_offset_of_allowDrag_4() { return static_cast<int32_t>(offsetof(HUDFPS_t4241874542, ___allowDrag_4)); }
	inline bool get_allowDrag_4() const { return ___allowDrag_4; }
	inline bool* get_address_of_allowDrag_4() { return &___allowDrag_4; }
	inline void set_allowDrag_4(bool value)
	{
		___allowDrag_4 = value;
	}

	inline static int32_t get_offset_of_frequency_5() { return static_cast<int32_t>(offsetof(HUDFPS_t4241874542, ___frequency_5)); }
	inline float get_frequency_5() const { return ___frequency_5; }
	inline float* get_address_of_frequency_5() { return &___frequency_5; }
	inline void set_frequency_5(float value)
	{
		___frequency_5 = value;
	}

	inline static int32_t get_offset_of_nbDecimal_6() { return static_cast<int32_t>(offsetof(HUDFPS_t4241874542, ___nbDecimal_6)); }
	inline int32_t get_nbDecimal_6() const { return ___nbDecimal_6; }
	inline int32_t* get_address_of_nbDecimal_6() { return &___nbDecimal_6; }
	inline void set_nbDecimal_6(int32_t value)
	{
		___nbDecimal_6 = value;
	}

	inline static int32_t get_offset_of_accum_7() { return static_cast<int32_t>(offsetof(HUDFPS_t4241874542, ___accum_7)); }
	inline float get_accum_7() const { return ___accum_7; }
	inline float* get_address_of_accum_7() { return &___accum_7; }
	inline void set_accum_7(float value)
	{
		___accum_7 = value;
	}

	inline static int32_t get_offset_of_frames_8() { return static_cast<int32_t>(offsetof(HUDFPS_t4241874542, ___frames_8)); }
	inline int32_t get_frames_8() const { return ___frames_8; }
	inline int32_t* get_address_of_frames_8() { return &___frames_8; }
	inline void set_frames_8(int32_t value)
	{
		___frames_8 = value;
	}

	inline static int32_t get_offset_of_color_9() { return static_cast<int32_t>(offsetof(HUDFPS_t4241874542, ___color_9)); }
	inline Color_t2555686324  get_color_9() const { return ___color_9; }
	inline Color_t2555686324 * get_address_of_color_9() { return &___color_9; }
	inline void set_color_9(Color_t2555686324  value)
	{
		___color_9 = value;
	}

	inline static int32_t get_offset_of_sFPS_10() { return static_cast<int32_t>(offsetof(HUDFPS_t4241874542, ___sFPS_10)); }
	inline String_t* get_sFPS_10() const { return ___sFPS_10; }
	inline String_t** get_address_of_sFPS_10() { return &___sFPS_10; }
	inline void set_sFPS_10(String_t* value)
	{
		___sFPS_10 = value;
		Il2CppCodeGenWriteBarrier((&___sFPS_10), value);
	}

	inline static int32_t get_offset_of_style_11() { return static_cast<int32_t>(offsetof(HUDFPS_t4241874542, ___style_11)); }
	inline GUIStyle_t3956901511 * get_style_11() const { return ___style_11; }
	inline GUIStyle_t3956901511 ** get_address_of_style_11() { return &___style_11; }
	inline void set_style_11(GUIStyle_t3956901511 * value)
	{
		___style_11 = value;
		Il2CppCodeGenWriteBarrier((&___style_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HUDFPS_T4241874542_H
#ifndef HIGHLIGHTABLE_T3139352672_H
#define HIGHLIGHTABLE_T3139352672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Highlightable
struct  Highlightable_t3139352672  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color Highlightable::mColor
	Color_t2555686324  ___mColor_2;
	// UnityEngine.Color Highlightable::mTargetColor
	Color_t2555686324  ___mTargetColor_3;
	// System.Boolean Highlightable::mHighlight
	bool ___mHighlight_4;
	// System.Boolean Highlightable::mModified
	bool ___mModified_5;
	// System.Single Highlightable::mAlpha
	float ___mAlpha_6;
	// UnityEngine.Renderer[] Highlightable::mChildrenRenderers
	RendererU5BU5D_t3210418286* ___mChildrenRenderers_7;

public:
	inline static int32_t get_offset_of_mColor_2() { return static_cast<int32_t>(offsetof(Highlightable_t3139352672, ___mColor_2)); }
	inline Color_t2555686324  get_mColor_2() const { return ___mColor_2; }
	inline Color_t2555686324 * get_address_of_mColor_2() { return &___mColor_2; }
	inline void set_mColor_2(Color_t2555686324  value)
	{
		___mColor_2 = value;
	}

	inline static int32_t get_offset_of_mTargetColor_3() { return static_cast<int32_t>(offsetof(Highlightable_t3139352672, ___mTargetColor_3)); }
	inline Color_t2555686324  get_mTargetColor_3() const { return ___mTargetColor_3; }
	inline Color_t2555686324 * get_address_of_mTargetColor_3() { return &___mTargetColor_3; }
	inline void set_mTargetColor_3(Color_t2555686324  value)
	{
		___mTargetColor_3 = value;
	}

	inline static int32_t get_offset_of_mHighlight_4() { return static_cast<int32_t>(offsetof(Highlightable_t3139352672, ___mHighlight_4)); }
	inline bool get_mHighlight_4() const { return ___mHighlight_4; }
	inline bool* get_address_of_mHighlight_4() { return &___mHighlight_4; }
	inline void set_mHighlight_4(bool value)
	{
		___mHighlight_4 = value;
	}

	inline static int32_t get_offset_of_mModified_5() { return static_cast<int32_t>(offsetof(Highlightable_t3139352672, ___mModified_5)); }
	inline bool get_mModified_5() const { return ___mModified_5; }
	inline bool* get_address_of_mModified_5() { return &___mModified_5; }
	inline void set_mModified_5(bool value)
	{
		___mModified_5 = value;
	}

	inline static int32_t get_offset_of_mAlpha_6() { return static_cast<int32_t>(offsetof(Highlightable_t3139352672, ___mAlpha_6)); }
	inline float get_mAlpha_6() const { return ___mAlpha_6; }
	inline float* get_address_of_mAlpha_6() { return &___mAlpha_6; }
	inline void set_mAlpha_6(float value)
	{
		___mAlpha_6 = value;
	}

	inline static int32_t get_offset_of_mChildrenRenderers_7() { return static_cast<int32_t>(offsetof(Highlightable_t3139352672, ___mChildrenRenderers_7)); }
	inline RendererU5BU5D_t3210418286* get_mChildrenRenderers_7() const { return ___mChildrenRenderers_7; }
	inline RendererU5BU5D_t3210418286** get_address_of_mChildrenRenderers_7() { return &___mChildrenRenderers_7; }
	inline void set_mChildrenRenderers_7(RendererU5BU5D_t3210418286* value)
	{
		___mChildrenRenderers_7 = value;
		Il2CppCodeGenWriteBarrier((&___mChildrenRenderers_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIGHLIGHTABLE_T3139352672_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef SHIPCAMERA_T1196952155_H
#define SHIPCAMERA_T1196952155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShipCamera
struct  ShipCamera_t1196952155  : public MonoBehaviour_t3962482529
{
public:
	// ShipController ShipCamera::control
	ShipController_t2808759107 * ___control_2;
	// UnityEngine.AnimationCurve ShipCamera::distance
	AnimationCurve_t3046754366 * ___distance_3;
	// UnityEngine.AnimationCurve ShipCamera::angle
	AnimationCurve_t3046754366 * ___angle_4;
	// UnityEngine.Transform ShipCamera::mTrans
	Transform_t3600365921 * ___mTrans_5;

public:
	inline static int32_t get_offset_of_control_2() { return static_cast<int32_t>(offsetof(ShipCamera_t1196952155, ___control_2)); }
	inline ShipController_t2808759107 * get_control_2() const { return ___control_2; }
	inline ShipController_t2808759107 ** get_address_of_control_2() { return &___control_2; }
	inline void set_control_2(ShipController_t2808759107 * value)
	{
		___control_2 = value;
		Il2CppCodeGenWriteBarrier((&___control_2), value);
	}

	inline static int32_t get_offset_of_distance_3() { return static_cast<int32_t>(offsetof(ShipCamera_t1196952155, ___distance_3)); }
	inline AnimationCurve_t3046754366 * get_distance_3() const { return ___distance_3; }
	inline AnimationCurve_t3046754366 ** get_address_of_distance_3() { return &___distance_3; }
	inline void set_distance_3(AnimationCurve_t3046754366 * value)
	{
		___distance_3 = value;
		Il2CppCodeGenWriteBarrier((&___distance_3), value);
	}

	inline static int32_t get_offset_of_angle_4() { return static_cast<int32_t>(offsetof(ShipCamera_t1196952155, ___angle_4)); }
	inline AnimationCurve_t3046754366 * get_angle_4() const { return ___angle_4; }
	inline AnimationCurve_t3046754366 ** get_address_of_angle_4() { return &___angle_4; }
	inline void set_angle_4(AnimationCurve_t3046754366 * value)
	{
		___angle_4 = value;
		Il2CppCodeGenWriteBarrier((&___angle_4), value);
	}

	inline static int32_t get_offset_of_mTrans_5() { return static_cast<int32_t>(offsetof(ShipCamera_t1196952155, ___mTrans_5)); }
	inline Transform_t3600365921 * get_mTrans_5() const { return ___mTrans_5; }
	inline Transform_t3600365921 ** get_address_of_mTrans_5() { return &___mTrans_5; }
	inline void set_mTrans_5(Transform_t3600365921 * value)
	{
		___mTrans_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHIPCAMERA_T1196952155_H
#ifndef LAGROTATION_T3758629971_H
#define LAGROTATION_T3758629971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LagRotation
struct  LagRotation_t3758629971  : public MonoBehaviour_t3962482529
{
public:
	// System.Single LagRotation::speed
	float ___speed_2;
	// UnityEngine.Transform LagRotation::mTrans
	Transform_t3600365921 * ___mTrans_3;
	// UnityEngine.Transform LagRotation::mParent
	Transform_t3600365921 * ___mParent_4;
	// UnityEngine.Quaternion LagRotation::mRelative
	Quaternion_t2301928331  ___mRelative_5;
	// UnityEngine.Quaternion LagRotation::mParentRot
	Quaternion_t2301928331  ___mParentRot_6;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(LagRotation_t3758629971, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_mTrans_3() { return static_cast<int32_t>(offsetof(LagRotation_t3758629971, ___mTrans_3)); }
	inline Transform_t3600365921 * get_mTrans_3() const { return ___mTrans_3; }
	inline Transform_t3600365921 ** get_address_of_mTrans_3() { return &___mTrans_3; }
	inline void set_mTrans_3(Transform_t3600365921 * value)
	{
		___mTrans_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_3), value);
	}

	inline static int32_t get_offset_of_mParent_4() { return static_cast<int32_t>(offsetof(LagRotation_t3758629971, ___mParent_4)); }
	inline Transform_t3600365921 * get_mParent_4() const { return ___mParent_4; }
	inline Transform_t3600365921 ** get_address_of_mParent_4() { return &___mParent_4; }
	inline void set_mParent_4(Transform_t3600365921 * value)
	{
		___mParent_4 = value;
		Il2CppCodeGenWriteBarrier((&___mParent_4), value);
	}

	inline static int32_t get_offset_of_mRelative_5() { return static_cast<int32_t>(offsetof(LagRotation_t3758629971, ___mRelative_5)); }
	inline Quaternion_t2301928331  get_mRelative_5() const { return ___mRelative_5; }
	inline Quaternion_t2301928331 * get_address_of_mRelative_5() { return &___mRelative_5; }
	inline void set_mRelative_5(Quaternion_t2301928331  value)
	{
		___mRelative_5 = value;
	}

	inline static int32_t get_offset_of_mParentRot_6() { return static_cast<int32_t>(offsetof(LagRotation_t3758629971, ___mParentRot_6)); }
	inline Quaternion_t2301928331  get_mParentRot_6() const { return ___mParentRot_6; }
	inline Quaternion_t2301928331 * get_address_of_mParentRot_6() { return &___mParentRot_6; }
	inline void set_mParentRot_6(Quaternion_t2301928331  value)
	{
		___mParentRot_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAGROTATION_T3758629971_H
#ifndef SHIPORBIT_T50882478_H
#define SHIPORBIT_T50882478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShipOrbit
struct  ShipOrbit_t50882478  : public MonoBehaviour_t3962482529
{
public:
	// ShipController ShipOrbit::control
	ShipController_t2808759107 * ___control_2;
	// System.Single ShipOrbit::sensitivity
	float ___sensitivity_3;
	// UnityEngine.Vector2 ShipOrbit::horizontalTiltRange
	Vector2_t2156229523  ___horizontalTiltRange_4;
	// UnityEngine.Transform ShipOrbit::mTrans
	Transform_t3600365921 * ___mTrans_5;
	// UnityEngine.Vector2 ShipOrbit::mInput
	Vector2_t2156229523  ___mInput_6;
	// UnityEngine.Vector2 ShipOrbit::mOffset
	Vector2_t2156229523  ___mOffset_7;

public:
	inline static int32_t get_offset_of_control_2() { return static_cast<int32_t>(offsetof(ShipOrbit_t50882478, ___control_2)); }
	inline ShipController_t2808759107 * get_control_2() const { return ___control_2; }
	inline ShipController_t2808759107 ** get_address_of_control_2() { return &___control_2; }
	inline void set_control_2(ShipController_t2808759107 * value)
	{
		___control_2 = value;
		Il2CppCodeGenWriteBarrier((&___control_2), value);
	}

	inline static int32_t get_offset_of_sensitivity_3() { return static_cast<int32_t>(offsetof(ShipOrbit_t50882478, ___sensitivity_3)); }
	inline float get_sensitivity_3() const { return ___sensitivity_3; }
	inline float* get_address_of_sensitivity_3() { return &___sensitivity_3; }
	inline void set_sensitivity_3(float value)
	{
		___sensitivity_3 = value;
	}

	inline static int32_t get_offset_of_horizontalTiltRange_4() { return static_cast<int32_t>(offsetof(ShipOrbit_t50882478, ___horizontalTiltRange_4)); }
	inline Vector2_t2156229523  get_horizontalTiltRange_4() const { return ___horizontalTiltRange_4; }
	inline Vector2_t2156229523 * get_address_of_horizontalTiltRange_4() { return &___horizontalTiltRange_4; }
	inline void set_horizontalTiltRange_4(Vector2_t2156229523  value)
	{
		___horizontalTiltRange_4 = value;
	}

	inline static int32_t get_offset_of_mTrans_5() { return static_cast<int32_t>(offsetof(ShipOrbit_t50882478, ___mTrans_5)); }
	inline Transform_t3600365921 * get_mTrans_5() const { return ___mTrans_5; }
	inline Transform_t3600365921 ** get_address_of_mTrans_5() { return &___mTrans_5; }
	inline void set_mTrans_5(Transform_t3600365921 * value)
	{
		___mTrans_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_5), value);
	}

	inline static int32_t get_offset_of_mInput_6() { return static_cast<int32_t>(offsetof(ShipOrbit_t50882478, ___mInput_6)); }
	inline Vector2_t2156229523  get_mInput_6() const { return ___mInput_6; }
	inline Vector2_t2156229523 * get_address_of_mInput_6() { return &___mInput_6; }
	inline void set_mInput_6(Vector2_t2156229523  value)
	{
		___mInput_6 = value;
	}

	inline static int32_t get_offset_of_mOffset_7() { return static_cast<int32_t>(offsetof(ShipOrbit_t50882478, ___mOffset_7)); }
	inline Vector2_t2156229523  get_mOffset_7() const { return ___mOffset_7; }
	inline Vector2_t2156229523 * get_address_of_mOffset_7() { return &___mOffset_7; }
	inline void set_mOffset_7(Vector2_t2156229523  value)
	{
		___mOffset_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHIPORBIT_T50882478_H
#ifndef GAMECAMERATARGET_T3267842245_H
#define GAMECAMERATARGET_T3267842245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameCameraTarget
struct  GameCameraTarget_t3267842245  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECAMERATARGET_T3267842245_H
#ifndef CREATEBUTTONSCRIPT_T92967038_H
#define CREATEBUTTONSCRIPT_T92967038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreateButtonScript
struct  CreateButtonScript_t92967038  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct CreateButtonScript_t92967038_StaticFields
{
public:
	// UnityEngine.Events.UnityAction CreateButtonScript::<>f__am$cache0
	UnityAction_t3245792599 * ___U3CU3Ef__amU24cache0_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(CreateButtonScript_t92967038_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline UnityAction_t3245792599 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline UnityAction_t3245792599 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(UnityAction_t3245792599 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATEBUTTONSCRIPT_T92967038_H
#ifndef DONECAMERAMOVEMENT_T1316814223_H
#define DONECAMERAMOVEMENT_T1316814223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DoneCameraMovement
struct  DoneCameraMovement_t1316814223  : public MonoBehaviour_t3962482529
{
public:
	// System.Single DoneCameraMovement::smooth
	float ___smooth_2;
	// System.Boolean DoneCameraMovement::extraCameraAdjust
	bool ___extraCameraAdjust_3;
	// UnityEngine.Transform DoneCameraMovement::player
	Transform_t3600365921 * ___player_4;
	// UnityEngine.Vector3 DoneCameraMovement::relCameraPos
	Vector3_t3722313464  ___relCameraPos_5;
	// System.Single DoneCameraMovement::relCameraPosMag
	float ___relCameraPosMag_6;
	// UnityEngine.Vector3 DoneCameraMovement::newPos
	Vector3_t3722313464  ___newPos_7;

public:
	inline static int32_t get_offset_of_smooth_2() { return static_cast<int32_t>(offsetof(DoneCameraMovement_t1316814223, ___smooth_2)); }
	inline float get_smooth_2() const { return ___smooth_2; }
	inline float* get_address_of_smooth_2() { return &___smooth_2; }
	inline void set_smooth_2(float value)
	{
		___smooth_2 = value;
	}

	inline static int32_t get_offset_of_extraCameraAdjust_3() { return static_cast<int32_t>(offsetof(DoneCameraMovement_t1316814223, ___extraCameraAdjust_3)); }
	inline bool get_extraCameraAdjust_3() const { return ___extraCameraAdjust_3; }
	inline bool* get_address_of_extraCameraAdjust_3() { return &___extraCameraAdjust_3; }
	inline void set_extraCameraAdjust_3(bool value)
	{
		___extraCameraAdjust_3 = value;
	}

	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(DoneCameraMovement_t1316814223, ___player_4)); }
	inline Transform_t3600365921 * get_player_4() const { return ___player_4; }
	inline Transform_t3600365921 ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(Transform_t3600365921 * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_relCameraPos_5() { return static_cast<int32_t>(offsetof(DoneCameraMovement_t1316814223, ___relCameraPos_5)); }
	inline Vector3_t3722313464  get_relCameraPos_5() const { return ___relCameraPos_5; }
	inline Vector3_t3722313464 * get_address_of_relCameraPos_5() { return &___relCameraPos_5; }
	inline void set_relCameraPos_5(Vector3_t3722313464  value)
	{
		___relCameraPos_5 = value;
	}

	inline static int32_t get_offset_of_relCameraPosMag_6() { return static_cast<int32_t>(offsetof(DoneCameraMovement_t1316814223, ___relCameraPosMag_6)); }
	inline float get_relCameraPosMag_6() const { return ___relCameraPosMag_6; }
	inline float* get_address_of_relCameraPosMag_6() { return &___relCameraPosMag_6; }
	inline void set_relCameraPosMag_6(float value)
	{
		___relCameraPosMag_6 = value;
	}

	inline static int32_t get_offset_of_newPos_7() { return static_cast<int32_t>(offsetof(DoneCameraMovement_t1316814223, ___newPos_7)); }
	inline Vector3_t3722313464  get_newPos_7() const { return ___newPos_7; }
	inline Vector3_t3722313464 * get_address_of_newPos_7() { return &___newPos_7; }
	inline void set_newPos_7(Vector3_t3722313464  value)
	{
		___newPos_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONECAMERAMOVEMENT_T1316814223_H
#ifndef TESTGUILAYOUT_T2138056437_H
#define TESTGUILAYOUT_T2138056437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestGuiLayout
struct  TestGuiLayout_t2138056437  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform TestGuiLayout::root
	RectTransform_t3704657025 * ___root_2;

public:
	inline static int32_t get_offset_of_root_2() { return static_cast<int32_t>(offsetof(TestGuiLayout_t2138056437, ___root_2)); }
	inline RectTransform_t3704657025 * get_root_2() const { return ___root_2; }
	inline RectTransform_t3704657025 ** get_address_of_root_2() { return &___root_2; }
	inline void set_root_2(RectTransform_t3704657025 * value)
	{
		___root_2 = value;
		Il2CppCodeGenWriteBarrier((&___root_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTGUILAYOUT_T2138056437_H
#ifndef ENEMYPATROL_T3346945880_H
#define ENEMYPATROL_T3346945880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyPatrol
struct  EnemyPatrol_t3346945880  : public MonoBehaviour_t3962482529
{
public:
	// System.Single EnemyPatrol::patrolSpeed
	float ___patrolSpeed_2;
	// System.Single EnemyPatrol::detectXDistance
	float ___detectXDistance_3;
	// UnityEngine.LayerMask EnemyPatrol::raycastMask
	LayerMask_t3493934918  ___raycastMask_4;
	// UnityEngine.Quaternion EnemyPatrol::newRotation
	Quaternion_t2301928331  ___newRotation_5;
	// System.Boolean EnemyPatrol::bStopAndRotate
	bool ___bStopAndRotate_6;

public:
	inline static int32_t get_offset_of_patrolSpeed_2() { return static_cast<int32_t>(offsetof(EnemyPatrol_t3346945880, ___patrolSpeed_2)); }
	inline float get_patrolSpeed_2() const { return ___patrolSpeed_2; }
	inline float* get_address_of_patrolSpeed_2() { return &___patrolSpeed_2; }
	inline void set_patrolSpeed_2(float value)
	{
		___patrolSpeed_2 = value;
	}

	inline static int32_t get_offset_of_detectXDistance_3() { return static_cast<int32_t>(offsetof(EnemyPatrol_t3346945880, ___detectXDistance_3)); }
	inline float get_detectXDistance_3() const { return ___detectXDistance_3; }
	inline float* get_address_of_detectXDistance_3() { return &___detectXDistance_3; }
	inline void set_detectXDistance_3(float value)
	{
		___detectXDistance_3 = value;
	}

	inline static int32_t get_offset_of_raycastMask_4() { return static_cast<int32_t>(offsetof(EnemyPatrol_t3346945880, ___raycastMask_4)); }
	inline LayerMask_t3493934918  get_raycastMask_4() const { return ___raycastMask_4; }
	inline LayerMask_t3493934918 * get_address_of_raycastMask_4() { return &___raycastMask_4; }
	inline void set_raycastMask_4(LayerMask_t3493934918  value)
	{
		___raycastMask_4 = value;
	}

	inline static int32_t get_offset_of_newRotation_5() { return static_cast<int32_t>(offsetof(EnemyPatrol_t3346945880, ___newRotation_5)); }
	inline Quaternion_t2301928331  get_newRotation_5() const { return ___newRotation_5; }
	inline Quaternion_t2301928331 * get_address_of_newRotation_5() { return &___newRotation_5; }
	inline void set_newRotation_5(Quaternion_t2301928331  value)
	{
		___newRotation_5 = value;
	}

	inline static int32_t get_offset_of_bStopAndRotate_6() { return static_cast<int32_t>(offsetof(EnemyPatrol_t3346945880, ___bStopAndRotate_6)); }
	inline bool get_bStopAndRotate_6() const { return ___bStopAndRotate_6; }
	inline bool* get_address_of_bStopAndRotate_6() { return &___bStopAndRotate_6; }
	inline void set_bStopAndRotate_6(bool value)
	{
		___bStopAndRotate_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYPATROL_T3346945880_H
#ifndef SHOWNAME_T1001792941_H
#define SHOWNAME_T1001792941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShowName
struct  ShowName_t1001792941  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera ShowName::camera
	Camera_t4157153871 * ___camera_2;
	// System.String ShowName::mName
	String_t* ___mName_3;
	// UnityEngine.Color ShowName::mColor
	Color_t2555686324  ___mColor_4;
	// System.Single ShowName::npcHeight
	float ___npcHeight_5;

public:
	inline static int32_t get_offset_of_camera_2() { return static_cast<int32_t>(offsetof(ShowName_t1001792941, ___camera_2)); }
	inline Camera_t4157153871 * get_camera_2() const { return ___camera_2; }
	inline Camera_t4157153871 ** get_address_of_camera_2() { return &___camera_2; }
	inline void set_camera_2(Camera_t4157153871 * value)
	{
		___camera_2 = value;
		Il2CppCodeGenWriteBarrier((&___camera_2), value);
	}

	inline static int32_t get_offset_of_mName_3() { return static_cast<int32_t>(offsetof(ShowName_t1001792941, ___mName_3)); }
	inline String_t* get_mName_3() const { return ___mName_3; }
	inline String_t** get_address_of_mName_3() { return &___mName_3; }
	inline void set_mName_3(String_t* value)
	{
		___mName_3 = value;
		Il2CppCodeGenWriteBarrier((&___mName_3), value);
	}

	inline static int32_t get_offset_of_mColor_4() { return static_cast<int32_t>(offsetof(ShowName_t1001792941, ___mColor_4)); }
	inline Color_t2555686324  get_mColor_4() const { return ___mColor_4; }
	inline Color_t2555686324 * get_address_of_mColor_4() { return &___mColor_4; }
	inline void set_mColor_4(Color_t2555686324  value)
	{
		___mColor_4 = value;
	}

	inline static int32_t get_offset_of_npcHeight_5() { return static_cast<int32_t>(offsetof(ShowName_t1001792941, ___npcHeight_5)); }
	inline float get_npcHeight_5() const { return ___npcHeight_5; }
	inline float* get_address_of_npcHeight_5() { return &___npcHeight_5; }
	inline void set_npcHeight_5(float value)
	{
		___npcHeight_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWNAME_T1001792941_H
#ifndef GAMECAMERA_T2564829307_H
#define GAMECAMERA_T2564829307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameCamera
struct  GameCamera_t2564829307  : public MonoBehaviour_t3962482529
{
public:
	// System.Single GameCamera::interpolationTime
	float ___interpolationTime_7;
	// UnityEngine.Transform GameCamera::mTrans
	Transform_t3600365921 * ___mTrans_8;
	// UnityEngine.Vector3 GameCamera::mPos
	Vector3_t3722313464  ___mPos_9;
	// UnityEngine.Quaternion GameCamera::mRot
	Quaternion_t2301928331  ___mRot_10;

public:
	inline static int32_t get_offset_of_interpolationTime_7() { return static_cast<int32_t>(offsetof(GameCamera_t2564829307, ___interpolationTime_7)); }
	inline float get_interpolationTime_7() const { return ___interpolationTime_7; }
	inline float* get_address_of_interpolationTime_7() { return &___interpolationTime_7; }
	inline void set_interpolationTime_7(float value)
	{
		___interpolationTime_7 = value;
	}

	inline static int32_t get_offset_of_mTrans_8() { return static_cast<int32_t>(offsetof(GameCamera_t2564829307, ___mTrans_8)); }
	inline Transform_t3600365921 * get_mTrans_8() const { return ___mTrans_8; }
	inline Transform_t3600365921 ** get_address_of_mTrans_8() { return &___mTrans_8; }
	inline void set_mTrans_8(Transform_t3600365921 * value)
	{
		___mTrans_8 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_8), value);
	}

	inline static int32_t get_offset_of_mPos_9() { return static_cast<int32_t>(offsetof(GameCamera_t2564829307, ___mPos_9)); }
	inline Vector3_t3722313464  get_mPos_9() const { return ___mPos_9; }
	inline Vector3_t3722313464 * get_address_of_mPos_9() { return &___mPos_9; }
	inline void set_mPos_9(Vector3_t3722313464  value)
	{
		___mPos_9 = value;
	}

	inline static int32_t get_offset_of_mRot_10() { return static_cast<int32_t>(offsetof(GameCamera_t2564829307, ___mRot_10)); }
	inline Quaternion_t2301928331  get_mRot_10() const { return ___mRot_10; }
	inline Quaternion_t2301928331 * get_address_of_mRot_10() { return &___mRot_10; }
	inline void set_mRot_10(Quaternion_t2301928331  value)
	{
		___mRot_10 = value;
	}
};

struct GameCamera_t2564829307_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Transform> GameCamera::mTargets
	List_1_t777473367 * ___mTargets_2;
	// System.Single GameCamera::mAlpha
	float ___mAlpha_3;
	// GameCamera GameCamera::mInstance
	GameCamera_t2564829307 * ___mInstance_4;
	// UnityEngine.Vector3 GameCamera::direction
	Vector3_t3722313464  ___direction_5;
	// UnityEngine.Vector3 GameCamera::flatDirection
	Vector3_t3722313464  ___flatDirection_6;

public:
	inline static int32_t get_offset_of_mTargets_2() { return static_cast<int32_t>(offsetof(GameCamera_t2564829307_StaticFields, ___mTargets_2)); }
	inline List_1_t777473367 * get_mTargets_2() const { return ___mTargets_2; }
	inline List_1_t777473367 ** get_address_of_mTargets_2() { return &___mTargets_2; }
	inline void set_mTargets_2(List_1_t777473367 * value)
	{
		___mTargets_2 = value;
		Il2CppCodeGenWriteBarrier((&___mTargets_2), value);
	}

	inline static int32_t get_offset_of_mAlpha_3() { return static_cast<int32_t>(offsetof(GameCamera_t2564829307_StaticFields, ___mAlpha_3)); }
	inline float get_mAlpha_3() const { return ___mAlpha_3; }
	inline float* get_address_of_mAlpha_3() { return &___mAlpha_3; }
	inline void set_mAlpha_3(float value)
	{
		___mAlpha_3 = value;
	}

	inline static int32_t get_offset_of_mInstance_4() { return static_cast<int32_t>(offsetof(GameCamera_t2564829307_StaticFields, ___mInstance_4)); }
	inline GameCamera_t2564829307 * get_mInstance_4() const { return ___mInstance_4; }
	inline GameCamera_t2564829307 ** get_address_of_mInstance_4() { return &___mInstance_4; }
	inline void set_mInstance_4(GameCamera_t2564829307 * value)
	{
		___mInstance_4 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_4), value);
	}

	inline static int32_t get_offset_of_direction_5() { return static_cast<int32_t>(offsetof(GameCamera_t2564829307_StaticFields, ___direction_5)); }
	inline Vector3_t3722313464  get_direction_5() const { return ___direction_5; }
	inline Vector3_t3722313464 * get_address_of_direction_5() { return &___direction_5; }
	inline void set_direction_5(Vector3_t3722313464  value)
	{
		___direction_5 = value;
	}

	inline static int32_t get_offset_of_flatDirection_6() { return static_cast<int32_t>(offsetof(GameCamera_t2564829307_StaticFields, ___flatDirection_6)); }
	inline Vector3_t3722313464  get_flatDirection_6() const { return ___flatDirection_6; }
	inline Vector3_t3722313464 * get_address_of_flatDirection_6() { return &___flatDirection_6; }
	inline void set_flatDirection_6(Vector3_t3722313464  value)
	{
		___flatDirection_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECAMERA_T2564829307_H
#ifndef ENTITY_T3391956725_H
#define ENTITY_T3391956725_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entity
struct  Entity_t3391956725  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITY_T3391956725_H
#ifndef SHIPCONTROLLER_T2808759107_H
#define SHIPCONTROLLER_T2808759107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShipController
struct  ShipController_t2808759107  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform[] ShipController::raycastPoints
	TransformU5BU5D_t807237628* ___raycastPoints_2;
	// UnityEngine.LayerMask ShipController::raycastMask
	LayerMask_t3493934918  ___raycastMask_3;
	// System.Single ShipController::m_seaGauge
	float ___m_seaGauge_4;
	// System.Single ShipController::mSpeed
	float ___mSpeed_5;
	// ShipUnit ShipController::mShipUnit
	ShipUnit_t4065899274 * ___mShipUnit_6;

public:
	inline static int32_t get_offset_of_raycastPoints_2() { return static_cast<int32_t>(offsetof(ShipController_t2808759107, ___raycastPoints_2)); }
	inline TransformU5BU5D_t807237628* get_raycastPoints_2() const { return ___raycastPoints_2; }
	inline TransformU5BU5D_t807237628** get_address_of_raycastPoints_2() { return &___raycastPoints_2; }
	inline void set_raycastPoints_2(TransformU5BU5D_t807237628* value)
	{
		___raycastPoints_2 = value;
		Il2CppCodeGenWriteBarrier((&___raycastPoints_2), value);
	}

	inline static int32_t get_offset_of_raycastMask_3() { return static_cast<int32_t>(offsetof(ShipController_t2808759107, ___raycastMask_3)); }
	inline LayerMask_t3493934918  get_raycastMask_3() const { return ___raycastMask_3; }
	inline LayerMask_t3493934918 * get_address_of_raycastMask_3() { return &___raycastMask_3; }
	inline void set_raycastMask_3(LayerMask_t3493934918  value)
	{
		___raycastMask_3 = value;
	}

	inline static int32_t get_offset_of_m_seaGauge_4() { return static_cast<int32_t>(offsetof(ShipController_t2808759107, ___m_seaGauge_4)); }
	inline float get_m_seaGauge_4() const { return ___m_seaGauge_4; }
	inline float* get_address_of_m_seaGauge_4() { return &___m_seaGauge_4; }
	inline void set_m_seaGauge_4(float value)
	{
		___m_seaGauge_4 = value;
	}

	inline static int32_t get_offset_of_mSpeed_5() { return static_cast<int32_t>(offsetof(ShipController_t2808759107, ___mSpeed_5)); }
	inline float get_mSpeed_5() const { return ___mSpeed_5; }
	inline float* get_address_of_mSpeed_5() { return &___mSpeed_5; }
	inline void set_mSpeed_5(float value)
	{
		___mSpeed_5 = value;
	}

	inline static int32_t get_offset_of_mShipUnit_6() { return static_cast<int32_t>(offsetof(ShipController_t2808759107, ___mShipUnit_6)); }
	inline ShipUnit_t4065899274 * get_mShipUnit_6() const { return ___mShipUnit_6; }
	inline ShipUnit_t4065899274 ** get_address_of_mShipUnit_6() { return &___mShipUnit_6; }
	inline void set_mShipUnit_6(ShipUnit_t4065899274 * value)
	{
		___mShipUnit_6 = value;
		Il2CppCodeGenWriteBarrier((&___mShipUnit_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHIPCONTROLLER_T2808759107_H
#ifndef ROTATETOWARDCAMERA_T118585142_H
#define ROTATETOWARDCAMERA_T118585142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotateTowardCamera
struct  RotateTowardCamera_t118585142  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera RotateTowardCamera::mainCamera
	Camera_t4157153871 * ___mainCamera_2;

public:
	inline static int32_t get_offset_of_mainCamera_2() { return static_cast<int32_t>(offsetof(RotateTowardCamera_t118585142, ___mainCamera_2)); }
	inline Camera_t4157153871 * get_mainCamera_2() const { return ___mainCamera_2; }
	inline Camera_t4157153871 ** get_address_of_mainCamera_2() { return &___mainCamera_2; }
	inline void set_mainCamera_2(Camera_t4157153871 * value)
	{
		___mainCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATETOWARDCAMERA_T118585142_H
#ifndef PLAYERSHIPBEHAVIOR_T729873910_H
#define PLAYERSHIPBEHAVIOR_T729873910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerShipBehavior
struct  PlayerShipBehavior_t729873910  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean PlayerShipBehavior::bIsInteractiving
	bool ___bIsInteractiving_2;

public:
	inline static int32_t get_offset_of_bIsInteractiving_2() { return static_cast<int32_t>(offsetof(PlayerShipBehavior_t729873910, ___bIsInteractiving_2)); }
	inline bool get_bIsInteractiving_2() const { return ___bIsInteractiving_2; }
	inline bool* get_address_of_bIsInteractiving_2() { return &___bIsInteractiving_2; }
	inline void set_bIsInteractiving_2(bool value)
	{
		___bIsInteractiving_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERSHIPBEHAVIOR_T729873910_H
#ifndef SCENEMANAGER_T385596982_H
#define SCENEMANAGER_T385596982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneManager
struct  SceneManager_t385596982  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct SceneManager_t385596982_StaticFields
{
public:
	// GameScene SceneManager::m_gameScene
	GameScene_t4110668666 * ___m_gameScene_2;

public:
	inline static int32_t get_offset_of_m_gameScene_2() { return static_cast<int32_t>(offsetof(SceneManager_t385596982_StaticFields, ___m_gameScene_2)); }
	inline GameScene_t4110668666 * get_m_gameScene_2() const { return ___m_gameScene_2; }
	inline GameScene_t4110668666 ** get_address_of_m_gameScene_2() { return &___m_gameScene_2; }
	inline void set_m_gameScene_2(GameScene_t4110668666 * value)
	{
		___m_gameScene_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_gameScene_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEMANAGER_T385596982_H
#ifndef WATER_T1083516957_H
#define WATER_T1083516957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Water
struct  Water_t1083516957  : public MonoBehaviour_t3962482529
{
public:
	// Water/WaterMode Water::m_WaterMode
	int32_t ___m_WaterMode_2;
	// System.Boolean Water::m_DisablePixelLights
	bool ___m_DisablePixelLights_3;
	// System.Int32 Water::m_TextureSize
	int32_t ___m_TextureSize_4;
	// System.Single Water::m_ClipPlaneOffset
	float ___m_ClipPlaneOffset_5;
	// UnityEngine.LayerMask Water::m_ReflectLayers
	LayerMask_t3493934918  ___m_ReflectLayers_6;
	// UnityEngine.LayerMask Water::m_RefractLayers
	LayerMask_t3493934918  ___m_RefractLayers_7;
	// System.Collections.Hashtable Water::m_ReflectionCameras
	Hashtable_t1853889766 * ___m_ReflectionCameras_8;
	// System.Collections.Hashtable Water::m_RefractionCameras
	Hashtable_t1853889766 * ___m_RefractionCameras_9;
	// UnityEngine.RenderTexture Water::m_ReflectionTexture
	RenderTexture_t2108887433 * ___m_ReflectionTexture_10;
	// UnityEngine.RenderTexture Water::m_RefractionTexture
	RenderTexture_t2108887433 * ___m_RefractionTexture_11;
	// Water/WaterMode Water::m_HardwareWaterSupport
	int32_t ___m_HardwareWaterSupport_12;
	// System.Int32 Water::m_OldReflectionTextureSize
	int32_t ___m_OldReflectionTextureSize_13;
	// System.Int32 Water::m_OldRefractionTextureSize
	int32_t ___m_OldRefractionTextureSize_14;

public:
	inline static int32_t get_offset_of_m_WaterMode_2() { return static_cast<int32_t>(offsetof(Water_t1083516957, ___m_WaterMode_2)); }
	inline int32_t get_m_WaterMode_2() const { return ___m_WaterMode_2; }
	inline int32_t* get_address_of_m_WaterMode_2() { return &___m_WaterMode_2; }
	inline void set_m_WaterMode_2(int32_t value)
	{
		___m_WaterMode_2 = value;
	}

	inline static int32_t get_offset_of_m_DisablePixelLights_3() { return static_cast<int32_t>(offsetof(Water_t1083516957, ___m_DisablePixelLights_3)); }
	inline bool get_m_DisablePixelLights_3() const { return ___m_DisablePixelLights_3; }
	inline bool* get_address_of_m_DisablePixelLights_3() { return &___m_DisablePixelLights_3; }
	inline void set_m_DisablePixelLights_3(bool value)
	{
		___m_DisablePixelLights_3 = value;
	}

	inline static int32_t get_offset_of_m_TextureSize_4() { return static_cast<int32_t>(offsetof(Water_t1083516957, ___m_TextureSize_4)); }
	inline int32_t get_m_TextureSize_4() const { return ___m_TextureSize_4; }
	inline int32_t* get_address_of_m_TextureSize_4() { return &___m_TextureSize_4; }
	inline void set_m_TextureSize_4(int32_t value)
	{
		___m_TextureSize_4 = value;
	}

	inline static int32_t get_offset_of_m_ClipPlaneOffset_5() { return static_cast<int32_t>(offsetof(Water_t1083516957, ___m_ClipPlaneOffset_5)); }
	inline float get_m_ClipPlaneOffset_5() const { return ___m_ClipPlaneOffset_5; }
	inline float* get_address_of_m_ClipPlaneOffset_5() { return &___m_ClipPlaneOffset_5; }
	inline void set_m_ClipPlaneOffset_5(float value)
	{
		___m_ClipPlaneOffset_5 = value;
	}

	inline static int32_t get_offset_of_m_ReflectLayers_6() { return static_cast<int32_t>(offsetof(Water_t1083516957, ___m_ReflectLayers_6)); }
	inline LayerMask_t3493934918  get_m_ReflectLayers_6() const { return ___m_ReflectLayers_6; }
	inline LayerMask_t3493934918 * get_address_of_m_ReflectLayers_6() { return &___m_ReflectLayers_6; }
	inline void set_m_ReflectLayers_6(LayerMask_t3493934918  value)
	{
		___m_ReflectLayers_6 = value;
	}

	inline static int32_t get_offset_of_m_RefractLayers_7() { return static_cast<int32_t>(offsetof(Water_t1083516957, ___m_RefractLayers_7)); }
	inline LayerMask_t3493934918  get_m_RefractLayers_7() const { return ___m_RefractLayers_7; }
	inline LayerMask_t3493934918 * get_address_of_m_RefractLayers_7() { return &___m_RefractLayers_7; }
	inline void set_m_RefractLayers_7(LayerMask_t3493934918  value)
	{
		___m_RefractLayers_7 = value;
	}

	inline static int32_t get_offset_of_m_ReflectionCameras_8() { return static_cast<int32_t>(offsetof(Water_t1083516957, ___m_ReflectionCameras_8)); }
	inline Hashtable_t1853889766 * get_m_ReflectionCameras_8() const { return ___m_ReflectionCameras_8; }
	inline Hashtable_t1853889766 ** get_address_of_m_ReflectionCameras_8() { return &___m_ReflectionCameras_8; }
	inline void set_m_ReflectionCameras_8(Hashtable_t1853889766 * value)
	{
		___m_ReflectionCameras_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReflectionCameras_8), value);
	}

	inline static int32_t get_offset_of_m_RefractionCameras_9() { return static_cast<int32_t>(offsetof(Water_t1083516957, ___m_RefractionCameras_9)); }
	inline Hashtable_t1853889766 * get_m_RefractionCameras_9() const { return ___m_RefractionCameras_9; }
	inline Hashtable_t1853889766 ** get_address_of_m_RefractionCameras_9() { return &___m_RefractionCameras_9; }
	inline void set_m_RefractionCameras_9(Hashtable_t1853889766 * value)
	{
		___m_RefractionCameras_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RefractionCameras_9), value);
	}

	inline static int32_t get_offset_of_m_ReflectionTexture_10() { return static_cast<int32_t>(offsetof(Water_t1083516957, ___m_ReflectionTexture_10)); }
	inline RenderTexture_t2108887433 * get_m_ReflectionTexture_10() const { return ___m_ReflectionTexture_10; }
	inline RenderTexture_t2108887433 ** get_address_of_m_ReflectionTexture_10() { return &___m_ReflectionTexture_10; }
	inline void set_m_ReflectionTexture_10(RenderTexture_t2108887433 * value)
	{
		___m_ReflectionTexture_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReflectionTexture_10), value);
	}

	inline static int32_t get_offset_of_m_RefractionTexture_11() { return static_cast<int32_t>(offsetof(Water_t1083516957, ___m_RefractionTexture_11)); }
	inline RenderTexture_t2108887433 * get_m_RefractionTexture_11() const { return ___m_RefractionTexture_11; }
	inline RenderTexture_t2108887433 ** get_address_of_m_RefractionTexture_11() { return &___m_RefractionTexture_11; }
	inline void set_m_RefractionTexture_11(RenderTexture_t2108887433 * value)
	{
		___m_RefractionTexture_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_RefractionTexture_11), value);
	}

	inline static int32_t get_offset_of_m_HardwareWaterSupport_12() { return static_cast<int32_t>(offsetof(Water_t1083516957, ___m_HardwareWaterSupport_12)); }
	inline int32_t get_m_HardwareWaterSupport_12() const { return ___m_HardwareWaterSupport_12; }
	inline int32_t* get_address_of_m_HardwareWaterSupport_12() { return &___m_HardwareWaterSupport_12; }
	inline void set_m_HardwareWaterSupport_12(int32_t value)
	{
		___m_HardwareWaterSupport_12 = value;
	}

	inline static int32_t get_offset_of_m_OldReflectionTextureSize_13() { return static_cast<int32_t>(offsetof(Water_t1083516957, ___m_OldReflectionTextureSize_13)); }
	inline int32_t get_m_OldReflectionTextureSize_13() const { return ___m_OldReflectionTextureSize_13; }
	inline int32_t* get_address_of_m_OldReflectionTextureSize_13() { return &___m_OldReflectionTextureSize_13; }
	inline void set_m_OldReflectionTextureSize_13(int32_t value)
	{
		___m_OldReflectionTextureSize_13 = value;
	}

	inline static int32_t get_offset_of_m_OldRefractionTextureSize_14() { return static_cast<int32_t>(offsetof(Water_t1083516957, ___m_OldRefractionTextureSize_14)); }
	inline int32_t get_m_OldRefractionTextureSize_14() const { return ___m_OldRefractionTextureSize_14; }
	inline int32_t* get_address_of_m_OldRefractionTextureSize_14() { return &___m_OldRefractionTextureSize_14; }
	inline void set_m_OldRefractionTextureSize_14(int32_t value)
	{
		___m_OldRefractionTextureSize_14 = value;
	}
};

struct Water_t1083516957_StaticFields
{
public:
	// System.Boolean Water::s_InsideWater
	bool ___s_InsideWater_15;

public:
	inline static int32_t get_offset_of_s_InsideWater_15() { return static_cast<int32_t>(offsetof(Water_t1083516957_StaticFields, ___s_InsideWater_15)); }
	inline bool get_s_InsideWater_15() const { return ___s_InsideWater_15; }
	inline bool* get_address_of_s_InsideWater_15() { return &___s_InsideWater_15; }
	inline void set_s_InsideWater_15(bool value)
	{
		___s_InsideWater_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATER_T1083516957_H
#ifndef RAYCASTMASK_T1362276734_H
#define RAYCASTMASK_T1362276734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RaycastMask
struct  RaycastMask_t1362276734  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image RaycastMask::_image
	Image_t2670269651 * ____image_2;
	// UnityEngine.Sprite RaycastMask::_sprite
	Sprite_t280657092 * ____sprite_3;

public:
	inline static int32_t get_offset_of__image_2() { return static_cast<int32_t>(offsetof(RaycastMask_t1362276734, ____image_2)); }
	inline Image_t2670269651 * get__image_2() const { return ____image_2; }
	inline Image_t2670269651 ** get_address_of__image_2() { return &____image_2; }
	inline void set__image_2(Image_t2670269651 * value)
	{
		____image_2 = value;
		Il2CppCodeGenWriteBarrier((&____image_2), value);
	}

	inline static int32_t get_offset_of__sprite_3() { return static_cast<int32_t>(offsetof(RaycastMask_t1362276734, ____sprite_3)); }
	inline Sprite_t280657092 * get__sprite_3() const { return ____sprite_3; }
	inline Sprite_t280657092 ** get_address_of__sprite_3() { return &____sprite_3; }
	inline void set__sprite_3(Sprite_t280657092 * value)
	{
		____sprite_3 = value;
		Il2CppCodeGenWriteBarrier((&____sprite_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTMASK_T1362276734_H
#ifndef UIWINDOWBASE_T957717849_H
#define UIWINDOWBASE_T957717849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIWindowBase
struct  UIWindowBase_t957717849  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform UIWindowBase::m_transform
	RectTransform_t3704657025 * ___m_transform_2;

public:
	inline static int32_t get_offset_of_m_transform_2() { return static_cast<int32_t>(offsetof(UIWindowBase_t957717849, ___m_transform_2)); }
	inline RectTransform_t3704657025 * get_m_transform_2() const { return ___m_transform_2; }
	inline RectTransform_t3704657025 ** get_address_of_m_transform_2() { return &___m_transform_2; }
	inline void set_m_transform_2(RectTransform_t3704657025 * value)
	{
		___m_transform_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIWINDOWBASE_T957717849_H
#ifndef RANDOMTERRAINGENERATOR_T662438485_H
#define RANDOMTERRAINGENERATOR_T662438485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RandomTerrainGenerator
struct  RandomTerrainGenerator_t662438485  : public MonoBehaviour_t3962482529
{
public:
	// System.Single RandomTerrainGenerator::HM
	float ___HM_2;
	// System.Single RandomTerrainGenerator::divRange
	float ___divRange_3;

public:
	inline static int32_t get_offset_of_HM_2() { return static_cast<int32_t>(offsetof(RandomTerrainGenerator_t662438485, ___HM_2)); }
	inline float get_HM_2() const { return ___HM_2; }
	inline float* get_address_of_HM_2() { return &___HM_2; }
	inline void set_HM_2(float value)
	{
		___HM_2 = value;
	}

	inline static int32_t get_offset_of_divRange_3() { return static_cast<int32_t>(offsetof(RandomTerrainGenerator_t662438485, ___divRange_3)); }
	inline float get_divRange_3() const { return ___divRange_3; }
	inline float* get_address_of_divRange_3() { return &___divRange_3; }
	inline void set_divRange_3(float value)
	{
		___divRange_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMTERRAINGENERATOR_T662438485_H
#ifndef MINIMAPCAMERA_T1751400688_H
#define MINIMAPCAMERA_T1751400688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MinimapCamera
struct  MinimapCamera_t1751400688  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera MinimapCamera::minimapCamera
	Camera_t4157153871 * ___minimapCamera_2;
	// UnityEngine.Camera MinimapCamera::mainCamera
	Camera_t4157153871 * ___mainCamera_3;

public:
	inline static int32_t get_offset_of_minimapCamera_2() { return static_cast<int32_t>(offsetof(MinimapCamera_t1751400688, ___minimapCamera_2)); }
	inline Camera_t4157153871 * get_minimapCamera_2() const { return ___minimapCamera_2; }
	inline Camera_t4157153871 ** get_address_of_minimapCamera_2() { return &___minimapCamera_2; }
	inline void set_minimapCamera_2(Camera_t4157153871 * value)
	{
		___minimapCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___minimapCamera_2), value);
	}

	inline static int32_t get_offset_of_mainCamera_3() { return static_cast<int32_t>(offsetof(MinimapCamera_t1751400688, ___mainCamera_3)); }
	inline Camera_t4157153871 * get_mainCamera_3() const { return ___mainCamera_3; }
	inline Camera_t4157153871 ** get_address_of_mainCamera_3() { return &___mainCamera_3; }
	inline void set_mainCamera_3(Camera_t4157153871 * value)
	{
		___mainCamera_3 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMAPCAMERA_T1751400688_H
#ifndef SHIPBOBBLE_T3127994465_H
#define SHIPBOBBLE_T3127994465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShipBobble
struct  ShipBobble_t3127994465  : public MonoBehaviour_t3962482529
{
public:
	// ShipController ShipBobble::control
	ShipController_t2808759107 * ___control_2;
	// UnityEngine.Transform ShipBobble::mTrans
	Transform_t3600365921 * ___mTrans_3;
	// UnityEngine.Vector3 ShipBobble::mOffset
	Vector3_t3722313464  ___mOffset_4;
	// UnityEngine.Vector2 ShipBobble::mTime
	Vector2_t2156229523  ___mTime_5;

public:
	inline static int32_t get_offset_of_control_2() { return static_cast<int32_t>(offsetof(ShipBobble_t3127994465, ___control_2)); }
	inline ShipController_t2808759107 * get_control_2() const { return ___control_2; }
	inline ShipController_t2808759107 ** get_address_of_control_2() { return &___control_2; }
	inline void set_control_2(ShipController_t2808759107 * value)
	{
		___control_2 = value;
		Il2CppCodeGenWriteBarrier((&___control_2), value);
	}

	inline static int32_t get_offset_of_mTrans_3() { return static_cast<int32_t>(offsetof(ShipBobble_t3127994465, ___mTrans_3)); }
	inline Transform_t3600365921 * get_mTrans_3() const { return ___mTrans_3; }
	inline Transform_t3600365921 ** get_address_of_mTrans_3() { return &___mTrans_3; }
	inline void set_mTrans_3(Transform_t3600365921 * value)
	{
		___mTrans_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_3), value);
	}

	inline static int32_t get_offset_of_mOffset_4() { return static_cast<int32_t>(offsetof(ShipBobble_t3127994465, ___mOffset_4)); }
	inline Vector3_t3722313464  get_mOffset_4() const { return ___mOffset_4; }
	inline Vector3_t3722313464 * get_address_of_mOffset_4() { return &___mOffset_4; }
	inline void set_mOffset_4(Vector3_t3722313464  value)
	{
		___mOffset_4 = value;
	}

	inline static int32_t get_offset_of_mTime_5() { return static_cast<int32_t>(offsetof(ShipBobble_t3127994465, ___mTime_5)); }
	inline Vector2_t2156229523  get_mTime_5() const { return ___mTime_5; }
	inline Vector2_t2156229523 * get_address_of_mTime_5() { return &___mTime_5; }
	inline void set_mTime_5(Vector2_t2156229523  value)
	{
		___mTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHIPBOBBLE_T3127994465_H
#ifndef CHATBUBBLEUI_T1508846992_H
#define CHATBUBBLEUI_T1508846992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatBubbleUI
struct  ChatBubbleUI_t1508846992  : public MonoBehaviour_t3962482529
{
public:
	// WorldCanvasUI ChatBubbleUI::worldCanvasUI
	WorldCanvasUI_t1156772134 * ___worldCanvasUI_2;
	// UnityEngine.Vector2 ChatBubbleUI::sizeDelta
	Vector2_t2156229523  ___sizeDelta_3;
	// System.Boolean ChatBubbleUI::autoCanvasUIs
	bool ___autoCanvasUIs_4;
	// UnityEngine.Sprite ChatBubbleUI::bgSprite
	Sprite_t280657092 * ___bgSprite_5;
	// UnityEngine.UI.Image ChatBubbleUI::bgImage
	Image_t2670269651 * ___bgImage_6;
	// UnityEngine.Texture ChatBubbleUI::bgTexture
	Texture_t3661962703 * ___bgTexture_7;
	// UnityEngine.UI.RawImage ChatBubbleUI::bgRawImage
	RawImage_t3182918964 * ___bgRawImage_8;
	// UnityEngine.UI.Text ChatBubbleUI::textUI
	Text_t1901882714 * ___textUI_9;
	// System.Int32 ChatBubbleUI::fontSize
	int32_t ___fontSize_10;
	// UnityEngine.Font ChatBubbleUI::font
	Font_t1956802104 * ___font_11;
	// System.Collections.Generic.List`1<System.String> ChatBubbleUI::chatStrList
	List_1_t3319525431 * ___chatStrList_12;
	// System.Single ChatBubbleUI::timeInterval
	float ___timeInterval_13;
	// System.Boolean ChatBubbleUI::autoPop
	bool ___autoPop_14;
	// System.Single ChatBubbleUI::curDeltaTime
	float ___curDeltaTime_15;
	// System.Boolean ChatBubbleUI::HideAfterAllChatPoped
	bool ___HideAfterAllChatPoped_16;

public:
	inline static int32_t get_offset_of_worldCanvasUI_2() { return static_cast<int32_t>(offsetof(ChatBubbleUI_t1508846992, ___worldCanvasUI_2)); }
	inline WorldCanvasUI_t1156772134 * get_worldCanvasUI_2() const { return ___worldCanvasUI_2; }
	inline WorldCanvasUI_t1156772134 ** get_address_of_worldCanvasUI_2() { return &___worldCanvasUI_2; }
	inline void set_worldCanvasUI_2(WorldCanvasUI_t1156772134 * value)
	{
		___worldCanvasUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___worldCanvasUI_2), value);
	}

	inline static int32_t get_offset_of_sizeDelta_3() { return static_cast<int32_t>(offsetof(ChatBubbleUI_t1508846992, ___sizeDelta_3)); }
	inline Vector2_t2156229523  get_sizeDelta_3() const { return ___sizeDelta_3; }
	inline Vector2_t2156229523 * get_address_of_sizeDelta_3() { return &___sizeDelta_3; }
	inline void set_sizeDelta_3(Vector2_t2156229523  value)
	{
		___sizeDelta_3 = value;
	}

	inline static int32_t get_offset_of_autoCanvasUIs_4() { return static_cast<int32_t>(offsetof(ChatBubbleUI_t1508846992, ___autoCanvasUIs_4)); }
	inline bool get_autoCanvasUIs_4() const { return ___autoCanvasUIs_4; }
	inline bool* get_address_of_autoCanvasUIs_4() { return &___autoCanvasUIs_4; }
	inline void set_autoCanvasUIs_4(bool value)
	{
		___autoCanvasUIs_4 = value;
	}

	inline static int32_t get_offset_of_bgSprite_5() { return static_cast<int32_t>(offsetof(ChatBubbleUI_t1508846992, ___bgSprite_5)); }
	inline Sprite_t280657092 * get_bgSprite_5() const { return ___bgSprite_5; }
	inline Sprite_t280657092 ** get_address_of_bgSprite_5() { return &___bgSprite_5; }
	inline void set_bgSprite_5(Sprite_t280657092 * value)
	{
		___bgSprite_5 = value;
		Il2CppCodeGenWriteBarrier((&___bgSprite_5), value);
	}

	inline static int32_t get_offset_of_bgImage_6() { return static_cast<int32_t>(offsetof(ChatBubbleUI_t1508846992, ___bgImage_6)); }
	inline Image_t2670269651 * get_bgImage_6() const { return ___bgImage_6; }
	inline Image_t2670269651 ** get_address_of_bgImage_6() { return &___bgImage_6; }
	inline void set_bgImage_6(Image_t2670269651 * value)
	{
		___bgImage_6 = value;
		Il2CppCodeGenWriteBarrier((&___bgImage_6), value);
	}

	inline static int32_t get_offset_of_bgTexture_7() { return static_cast<int32_t>(offsetof(ChatBubbleUI_t1508846992, ___bgTexture_7)); }
	inline Texture_t3661962703 * get_bgTexture_7() const { return ___bgTexture_7; }
	inline Texture_t3661962703 ** get_address_of_bgTexture_7() { return &___bgTexture_7; }
	inline void set_bgTexture_7(Texture_t3661962703 * value)
	{
		___bgTexture_7 = value;
		Il2CppCodeGenWriteBarrier((&___bgTexture_7), value);
	}

	inline static int32_t get_offset_of_bgRawImage_8() { return static_cast<int32_t>(offsetof(ChatBubbleUI_t1508846992, ___bgRawImage_8)); }
	inline RawImage_t3182918964 * get_bgRawImage_8() const { return ___bgRawImage_8; }
	inline RawImage_t3182918964 ** get_address_of_bgRawImage_8() { return &___bgRawImage_8; }
	inline void set_bgRawImage_8(RawImage_t3182918964 * value)
	{
		___bgRawImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___bgRawImage_8), value);
	}

	inline static int32_t get_offset_of_textUI_9() { return static_cast<int32_t>(offsetof(ChatBubbleUI_t1508846992, ___textUI_9)); }
	inline Text_t1901882714 * get_textUI_9() const { return ___textUI_9; }
	inline Text_t1901882714 ** get_address_of_textUI_9() { return &___textUI_9; }
	inline void set_textUI_9(Text_t1901882714 * value)
	{
		___textUI_9 = value;
		Il2CppCodeGenWriteBarrier((&___textUI_9), value);
	}

	inline static int32_t get_offset_of_fontSize_10() { return static_cast<int32_t>(offsetof(ChatBubbleUI_t1508846992, ___fontSize_10)); }
	inline int32_t get_fontSize_10() const { return ___fontSize_10; }
	inline int32_t* get_address_of_fontSize_10() { return &___fontSize_10; }
	inline void set_fontSize_10(int32_t value)
	{
		___fontSize_10 = value;
	}

	inline static int32_t get_offset_of_font_11() { return static_cast<int32_t>(offsetof(ChatBubbleUI_t1508846992, ___font_11)); }
	inline Font_t1956802104 * get_font_11() const { return ___font_11; }
	inline Font_t1956802104 ** get_address_of_font_11() { return &___font_11; }
	inline void set_font_11(Font_t1956802104 * value)
	{
		___font_11 = value;
		Il2CppCodeGenWriteBarrier((&___font_11), value);
	}

	inline static int32_t get_offset_of_chatStrList_12() { return static_cast<int32_t>(offsetof(ChatBubbleUI_t1508846992, ___chatStrList_12)); }
	inline List_1_t3319525431 * get_chatStrList_12() const { return ___chatStrList_12; }
	inline List_1_t3319525431 ** get_address_of_chatStrList_12() { return &___chatStrList_12; }
	inline void set_chatStrList_12(List_1_t3319525431 * value)
	{
		___chatStrList_12 = value;
		Il2CppCodeGenWriteBarrier((&___chatStrList_12), value);
	}

	inline static int32_t get_offset_of_timeInterval_13() { return static_cast<int32_t>(offsetof(ChatBubbleUI_t1508846992, ___timeInterval_13)); }
	inline float get_timeInterval_13() const { return ___timeInterval_13; }
	inline float* get_address_of_timeInterval_13() { return &___timeInterval_13; }
	inline void set_timeInterval_13(float value)
	{
		___timeInterval_13 = value;
	}

	inline static int32_t get_offset_of_autoPop_14() { return static_cast<int32_t>(offsetof(ChatBubbleUI_t1508846992, ___autoPop_14)); }
	inline bool get_autoPop_14() const { return ___autoPop_14; }
	inline bool* get_address_of_autoPop_14() { return &___autoPop_14; }
	inline void set_autoPop_14(bool value)
	{
		___autoPop_14 = value;
	}

	inline static int32_t get_offset_of_curDeltaTime_15() { return static_cast<int32_t>(offsetof(ChatBubbleUI_t1508846992, ___curDeltaTime_15)); }
	inline float get_curDeltaTime_15() const { return ___curDeltaTime_15; }
	inline float* get_address_of_curDeltaTime_15() { return &___curDeltaTime_15; }
	inline void set_curDeltaTime_15(float value)
	{
		___curDeltaTime_15 = value;
	}

	inline static int32_t get_offset_of_HideAfterAllChatPoped_16() { return static_cast<int32_t>(offsetof(ChatBubbleUI_t1508846992, ___HideAfterAllChatPoped_16)); }
	inline bool get_HideAfterAllChatPoped_16() const { return ___HideAfterAllChatPoped_16; }
	inline bool* get_address_of_HideAfterAllChatPoped_16() { return &___HideAfterAllChatPoped_16; }
	inline void set_HideAfterAllChatPoped_16(bool value)
	{
		___HideAfterAllChatPoped_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATBUBBLEUI_T1508846992_H
#ifndef WORLDCANVASUI_T1156772134_H
#define WORLDCANVASUI_T1156772134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WorldCanvasUI
struct  WorldCanvasUI_t1156772134  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Canvas WorldCanvasUI::worldCanvas
	Canvas_t3310196443 * ___worldCanvas_2;
	// UnityEngine.RenderMode WorldCanvasUI::renderMode
	int32_t ___renderMode_3;
	// UnityEngine.GameObject WorldCanvasUI::canvasGO
	GameObject_t1113636619 * ___canvasGO_4;

public:
	inline static int32_t get_offset_of_worldCanvas_2() { return static_cast<int32_t>(offsetof(WorldCanvasUI_t1156772134, ___worldCanvas_2)); }
	inline Canvas_t3310196443 * get_worldCanvas_2() const { return ___worldCanvas_2; }
	inline Canvas_t3310196443 ** get_address_of_worldCanvas_2() { return &___worldCanvas_2; }
	inline void set_worldCanvas_2(Canvas_t3310196443 * value)
	{
		___worldCanvas_2 = value;
		Il2CppCodeGenWriteBarrier((&___worldCanvas_2), value);
	}

	inline static int32_t get_offset_of_renderMode_3() { return static_cast<int32_t>(offsetof(WorldCanvasUI_t1156772134, ___renderMode_3)); }
	inline int32_t get_renderMode_3() const { return ___renderMode_3; }
	inline int32_t* get_address_of_renderMode_3() { return &___renderMode_3; }
	inline void set_renderMode_3(int32_t value)
	{
		___renderMode_3 = value;
	}

	inline static int32_t get_offset_of_canvasGO_4() { return static_cast<int32_t>(offsetof(WorldCanvasUI_t1156772134, ___canvasGO_4)); }
	inline GameObject_t1113636619 * get_canvasGO_4() const { return ___canvasGO_4; }
	inline GameObject_t1113636619 ** get_address_of_canvasGO_4() { return &___canvasGO_4; }
	inline void set_canvasGO_4(GameObject_t1113636619 * value)
	{
		___canvasGO_4 = value;
		Il2CppCodeGenWriteBarrier((&___canvasGO_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDCANVASUI_T1156772134_H
#ifndef SHIPUNIT_T4065899274_H
#define SHIPUNIT_T4065899274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShipUnit
struct  ShipUnit_t4065899274  : public Entity_t3391956725
{
public:
	// UnityEngine.Vector2 ShipUnit::mHealth
	Vector2_t2156229523  ___mHealth_2;
	// LevelExp ShipUnit::mLevelExp
	LevelExp_t2602383666 * ___mLevelExp_3;
	// System.Single ShipUnit::maxMovementSpeed
	float ___maxMovementSpeed_4;
	// System.Single ShipUnit::maxTurningSpeed
	float ___maxTurningSpeed_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,Entity> ShipUnit::m_container
	Dictionary_2_t2280670056 * ___m_container_6;
	// System.Single ShipUnit::mDamage
	float ___mDamage_7;
	// System.Single ShipUnit::mDamageReduction
	float ___mDamageReduction_8;
	// System.Single ShipUnit::mConsumeRate
	float ___mConsumeRate_9;
	// System.Single ShipUnit::mLuckyRate
	float ___mLuckyRate_10;
	// SellDefine ShipUnit::mSellValue
	SellDefine_t4148876514 * ___mSellValue_11;
	// UnityEngine.Vector3 ShipUnit::mVelocity
	Vector3_t3722313464  ___mVelocity_12;
	// UnityEngine.Vector3 ShipUnit::mLastPos
	Vector3_t3722313464  ___mLastPos_13;
	// UnityEngine.Transform ShipUnit::mTrans
	Transform_t3600365921 * ___mTrans_14;
	// System.Boolean ShipUnit::mDestroyed
	bool ___mDestroyed_15;

public:
	inline static int32_t get_offset_of_mHealth_2() { return static_cast<int32_t>(offsetof(ShipUnit_t4065899274, ___mHealth_2)); }
	inline Vector2_t2156229523  get_mHealth_2() const { return ___mHealth_2; }
	inline Vector2_t2156229523 * get_address_of_mHealth_2() { return &___mHealth_2; }
	inline void set_mHealth_2(Vector2_t2156229523  value)
	{
		___mHealth_2 = value;
	}

	inline static int32_t get_offset_of_mLevelExp_3() { return static_cast<int32_t>(offsetof(ShipUnit_t4065899274, ___mLevelExp_3)); }
	inline LevelExp_t2602383666 * get_mLevelExp_3() const { return ___mLevelExp_3; }
	inline LevelExp_t2602383666 ** get_address_of_mLevelExp_3() { return &___mLevelExp_3; }
	inline void set_mLevelExp_3(LevelExp_t2602383666 * value)
	{
		___mLevelExp_3 = value;
		Il2CppCodeGenWriteBarrier((&___mLevelExp_3), value);
	}

	inline static int32_t get_offset_of_maxMovementSpeed_4() { return static_cast<int32_t>(offsetof(ShipUnit_t4065899274, ___maxMovementSpeed_4)); }
	inline float get_maxMovementSpeed_4() const { return ___maxMovementSpeed_4; }
	inline float* get_address_of_maxMovementSpeed_4() { return &___maxMovementSpeed_4; }
	inline void set_maxMovementSpeed_4(float value)
	{
		___maxMovementSpeed_4 = value;
	}

	inline static int32_t get_offset_of_maxTurningSpeed_5() { return static_cast<int32_t>(offsetof(ShipUnit_t4065899274, ___maxTurningSpeed_5)); }
	inline float get_maxTurningSpeed_5() const { return ___maxTurningSpeed_5; }
	inline float* get_address_of_maxTurningSpeed_5() { return &___maxTurningSpeed_5; }
	inline void set_maxTurningSpeed_5(float value)
	{
		___maxTurningSpeed_5 = value;
	}

	inline static int32_t get_offset_of_m_container_6() { return static_cast<int32_t>(offsetof(ShipUnit_t4065899274, ___m_container_6)); }
	inline Dictionary_2_t2280670056 * get_m_container_6() const { return ___m_container_6; }
	inline Dictionary_2_t2280670056 ** get_address_of_m_container_6() { return &___m_container_6; }
	inline void set_m_container_6(Dictionary_2_t2280670056 * value)
	{
		___m_container_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_container_6), value);
	}

	inline static int32_t get_offset_of_mDamage_7() { return static_cast<int32_t>(offsetof(ShipUnit_t4065899274, ___mDamage_7)); }
	inline float get_mDamage_7() const { return ___mDamage_7; }
	inline float* get_address_of_mDamage_7() { return &___mDamage_7; }
	inline void set_mDamage_7(float value)
	{
		___mDamage_7 = value;
	}

	inline static int32_t get_offset_of_mDamageReduction_8() { return static_cast<int32_t>(offsetof(ShipUnit_t4065899274, ___mDamageReduction_8)); }
	inline float get_mDamageReduction_8() const { return ___mDamageReduction_8; }
	inline float* get_address_of_mDamageReduction_8() { return &___mDamageReduction_8; }
	inline void set_mDamageReduction_8(float value)
	{
		___mDamageReduction_8 = value;
	}

	inline static int32_t get_offset_of_mConsumeRate_9() { return static_cast<int32_t>(offsetof(ShipUnit_t4065899274, ___mConsumeRate_9)); }
	inline float get_mConsumeRate_9() const { return ___mConsumeRate_9; }
	inline float* get_address_of_mConsumeRate_9() { return &___mConsumeRate_9; }
	inline void set_mConsumeRate_9(float value)
	{
		___mConsumeRate_9 = value;
	}

	inline static int32_t get_offset_of_mLuckyRate_10() { return static_cast<int32_t>(offsetof(ShipUnit_t4065899274, ___mLuckyRate_10)); }
	inline float get_mLuckyRate_10() const { return ___mLuckyRate_10; }
	inline float* get_address_of_mLuckyRate_10() { return &___mLuckyRate_10; }
	inline void set_mLuckyRate_10(float value)
	{
		___mLuckyRate_10 = value;
	}

	inline static int32_t get_offset_of_mSellValue_11() { return static_cast<int32_t>(offsetof(ShipUnit_t4065899274, ___mSellValue_11)); }
	inline SellDefine_t4148876514 * get_mSellValue_11() const { return ___mSellValue_11; }
	inline SellDefine_t4148876514 ** get_address_of_mSellValue_11() { return &___mSellValue_11; }
	inline void set_mSellValue_11(SellDefine_t4148876514 * value)
	{
		___mSellValue_11 = value;
		Il2CppCodeGenWriteBarrier((&___mSellValue_11), value);
	}

	inline static int32_t get_offset_of_mVelocity_12() { return static_cast<int32_t>(offsetof(ShipUnit_t4065899274, ___mVelocity_12)); }
	inline Vector3_t3722313464  get_mVelocity_12() const { return ___mVelocity_12; }
	inline Vector3_t3722313464 * get_address_of_mVelocity_12() { return &___mVelocity_12; }
	inline void set_mVelocity_12(Vector3_t3722313464  value)
	{
		___mVelocity_12 = value;
	}

	inline static int32_t get_offset_of_mLastPos_13() { return static_cast<int32_t>(offsetof(ShipUnit_t4065899274, ___mLastPos_13)); }
	inline Vector3_t3722313464  get_mLastPos_13() const { return ___mLastPos_13; }
	inline Vector3_t3722313464 * get_address_of_mLastPos_13() { return &___mLastPos_13; }
	inline void set_mLastPos_13(Vector3_t3722313464  value)
	{
		___mLastPos_13 = value;
	}

	inline static int32_t get_offset_of_mTrans_14() { return static_cast<int32_t>(offsetof(ShipUnit_t4065899274, ___mTrans_14)); }
	inline Transform_t3600365921 * get_mTrans_14() const { return ___mTrans_14; }
	inline Transform_t3600365921 ** get_address_of_mTrans_14() { return &___mTrans_14; }
	inline void set_mTrans_14(Transform_t3600365921 * value)
	{
		___mTrans_14 = value;
		Il2CppCodeGenWriteBarrier((&___mTrans_14), value);
	}

	inline static int32_t get_offset_of_mDestroyed_15() { return static_cast<int32_t>(offsetof(ShipUnit_t4065899274, ___mDestroyed_15)); }
	inline bool get_mDestroyed_15() const { return ___mDestroyed_15; }
	inline bool* get_address_of_mDestroyed_15() { return &___mDestroyed_15; }
	inline void set_mDestroyed_15(bool value)
	{
		___mDestroyed_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHIPUNIT_T4065899274_H
#ifndef HUMANUNIT_T2125463837_H
#define HUMANUNIT_T2125463837_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HumanUnit
struct  HumanUnit_t2125463837  : public Entity_t3391956725
{
public:
	// UnityEngine.Vector2 HumanUnit::mHealth
	Vector2_t2156229523  ___mHealth_2;
	// LevelExp HumanUnit::mLevelExp
	LevelExp_t2602383666 * ___mLevelExp_3;
	// SkillAbility HumanUnit::mSkillAbilities
	SkillAbility_t2516669324 * ___mSkillAbilities_4;

public:
	inline static int32_t get_offset_of_mHealth_2() { return static_cast<int32_t>(offsetof(HumanUnit_t2125463837, ___mHealth_2)); }
	inline Vector2_t2156229523  get_mHealth_2() const { return ___mHealth_2; }
	inline Vector2_t2156229523 * get_address_of_mHealth_2() { return &___mHealth_2; }
	inline void set_mHealth_2(Vector2_t2156229523  value)
	{
		___mHealth_2 = value;
	}

	inline static int32_t get_offset_of_mLevelExp_3() { return static_cast<int32_t>(offsetof(HumanUnit_t2125463837, ___mLevelExp_3)); }
	inline LevelExp_t2602383666 * get_mLevelExp_3() const { return ___mLevelExp_3; }
	inline LevelExp_t2602383666 ** get_address_of_mLevelExp_3() { return &___mLevelExp_3; }
	inline void set_mLevelExp_3(LevelExp_t2602383666 * value)
	{
		___mLevelExp_3 = value;
		Il2CppCodeGenWriteBarrier((&___mLevelExp_3), value);
	}

	inline static int32_t get_offset_of_mSkillAbilities_4() { return static_cast<int32_t>(offsetof(HumanUnit_t2125463837, ___mSkillAbilities_4)); }
	inline SkillAbility_t2516669324 * get_mSkillAbilities_4() const { return ___mSkillAbilities_4; }
	inline SkillAbility_t2516669324 ** get_address_of_mSkillAbilities_4() { return &___mSkillAbilities_4; }
	inline void set_mSkillAbilities_4(SkillAbility_t2516669324 * value)
	{
		___mSkillAbilities_4 = value;
		Il2CppCodeGenWriteBarrier((&___mSkillAbilities_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HUMANUNIT_T2125463837_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_3;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_4;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_5;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_6;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_7;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_8;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_9;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_10;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_11;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_12;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_13;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_15;

public:
	inline static int32_t get_offset_of_m_Navigation_3() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_3)); }
	inline Navigation_t3049316579  get_m_Navigation_3() const { return ___m_Navigation_3; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_3() { return &___m_Navigation_3; }
	inline void set_m_Navigation_3(Navigation_t3049316579  value)
	{
		___m_Navigation_3 = value;
	}

	inline static int32_t get_offset_of_m_Transition_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_4)); }
	inline int32_t get_m_Transition_4() const { return ___m_Transition_4; }
	inline int32_t* get_address_of_m_Transition_4() { return &___m_Transition_4; }
	inline void set_m_Transition_4(int32_t value)
	{
		___m_Transition_4 = value;
	}

	inline static int32_t get_offset_of_m_Colors_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_5)); }
	inline ColorBlock_t2139031574  get_m_Colors_5() const { return ___m_Colors_5; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_5() { return &___m_Colors_5; }
	inline void set_m_Colors_5(ColorBlock_t2139031574  value)
	{
		___m_Colors_5 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_6)); }
	inline SpriteState_t1362986479  get_m_SpriteState_6() const { return ___m_SpriteState_6; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_6() { return &___m_SpriteState_6; }
	inline void set_m_SpriteState_6(SpriteState_t1362986479  value)
	{
		___m_SpriteState_6 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_7)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_7() const { return ___m_AnimationTriggers_7; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_7() { return &___m_AnimationTriggers_7; }
	inline void set_m_AnimationTriggers_7(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_7), value);
	}

	inline static int32_t get_offset_of_m_Interactable_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_8)); }
	inline bool get_m_Interactable_8() const { return ___m_Interactable_8; }
	inline bool* get_address_of_m_Interactable_8() { return &___m_Interactable_8; }
	inline void set_m_Interactable_8(bool value)
	{
		___m_Interactable_8 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_9)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_9() const { return ___m_TargetGraphic_9; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_9() { return &___m_TargetGraphic_9; }
	inline void set_m_TargetGraphic_9(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_9), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_10)); }
	inline bool get_m_GroupsAllowInteraction_10() const { return ___m_GroupsAllowInteraction_10; }
	inline bool* get_address_of_m_GroupsAllowInteraction_10() { return &___m_GroupsAllowInteraction_10; }
	inline void set_m_GroupsAllowInteraction_10(bool value)
	{
		___m_GroupsAllowInteraction_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_11)); }
	inline int32_t get_m_CurrentSelectionState_11() const { return ___m_CurrentSelectionState_11; }
	inline int32_t* get_address_of_m_CurrentSelectionState_11() { return &___m_CurrentSelectionState_11; }
	inline void set_m_CurrentSelectionState_11(int32_t value)
	{
		___m_CurrentSelectionState_11 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_12)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_12() const { return ___U3CisPointerInsideU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_12() { return &___U3CisPointerInsideU3Ek__BackingField_12; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_12(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_13)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_13() const { return ___U3CisPointerDownU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_13() { return &___U3CisPointerDownU3Ek__BackingField_13; }
	inline void set_U3CisPointerDownU3Ek__BackingField_13(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_14)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_14() const { return ___U3ChasSelectionU3Ek__BackingField_14; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_14() { return &___U3ChasSelectionU3Ek__BackingField_14; }
	inline void set_U3ChasSelectionU3Ek__BackingField_14(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_15)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_15() const { return ___m_CanvasGroupCache_15; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_15() { return &___m_CanvasGroupCache_15; }
	inline void set_m_CanvasGroupCache_15(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_15), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_2;

public:
	inline static int32_t get_offset_of_s_List_2() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_2)); }
	inline List_1_t427135887 * get_s_List_2() const { return ___s_List_2; }
	inline List_1_t427135887 ** get_address_of_s_List_2() { return &___s_List_2; }
	inline void set_s_List_2(List_1_t427135887 * value)
	{
		___s_List_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef EVENTSYSTEM_T1003666588_H
#define EVENTSYSTEM_T1003666588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventSystem
struct  EventSystem_t1003666588  : public UIBehaviour_t3495933518
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule> UnityEngine.EventSystems.EventSystem::m_SystemInputModules
	List_1_t3491343620 * ___m_SystemInputModules_2;
	// UnityEngine.EventSystems.BaseInputModule UnityEngine.EventSystems.EventSystem::m_CurrentInputModule
	BaseInputModule_t2019268878 * ___m_CurrentInputModule_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_FirstSelected
	GameObject_t1113636619 * ___m_FirstSelected_5;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_sendNavigationEvents
	bool ___m_sendNavigationEvents_6;
	// System.Int32 UnityEngine.EventSystems.EventSystem::m_DragThreshold
	int32_t ___m_DragThreshold_7;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_CurrentSelected
	GameObject_t1113636619 * ___m_CurrentSelected_8;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_HasFocus
	bool ___m_HasFocus_9;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_SelectionGuard
	bool ___m_SelectionGuard_10;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.EventSystem::m_DummyData
	BaseEventData_t3903027533 * ___m_DummyData_11;

public:
	inline static int32_t get_offset_of_m_SystemInputModules_2() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_SystemInputModules_2)); }
	inline List_1_t3491343620 * get_m_SystemInputModules_2() const { return ___m_SystemInputModules_2; }
	inline List_1_t3491343620 ** get_address_of_m_SystemInputModules_2() { return &___m_SystemInputModules_2; }
	inline void set_m_SystemInputModules_2(List_1_t3491343620 * value)
	{
		___m_SystemInputModules_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SystemInputModules_2), value);
	}

	inline static int32_t get_offset_of_m_CurrentInputModule_3() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_CurrentInputModule_3)); }
	inline BaseInputModule_t2019268878 * get_m_CurrentInputModule_3() const { return ___m_CurrentInputModule_3; }
	inline BaseInputModule_t2019268878 ** get_address_of_m_CurrentInputModule_3() { return &___m_CurrentInputModule_3; }
	inline void set_m_CurrentInputModule_3(BaseInputModule_t2019268878 * value)
	{
		___m_CurrentInputModule_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentInputModule_3), value);
	}

	inline static int32_t get_offset_of_m_FirstSelected_5() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_FirstSelected_5)); }
	inline GameObject_t1113636619 * get_m_FirstSelected_5() const { return ___m_FirstSelected_5; }
	inline GameObject_t1113636619 ** get_address_of_m_FirstSelected_5() { return &___m_FirstSelected_5; }
	inline void set_m_FirstSelected_5(GameObject_t1113636619 * value)
	{
		___m_FirstSelected_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_FirstSelected_5), value);
	}

	inline static int32_t get_offset_of_m_sendNavigationEvents_6() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_sendNavigationEvents_6)); }
	inline bool get_m_sendNavigationEvents_6() const { return ___m_sendNavigationEvents_6; }
	inline bool* get_address_of_m_sendNavigationEvents_6() { return &___m_sendNavigationEvents_6; }
	inline void set_m_sendNavigationEvents_6(bool value)
	{
		___m_sendNavigationEvents_6 = value;
	}

	inline static int32_t get_offset_of_m_DragThreshold_7() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_DragThreshold_7)); }
	inline int32_t get_m_DragThreshold_7() const { return ___m_DragThreshold_7; }
	inline int32_t* get_address_of_m_DragThreshold_7() { return &___m_DragThreshold_7; }
	inline void set_m_DragThreshold_7(int32_t value)
	{
		___m_DragThreshold_7 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelected_8() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_CurrentSelected_8)); }
	inline GameObject_t1113636619 * get_m_CurrentSelected_8() const { return ___m_CurrentSelected_8; }
	inline GameObject_t1113636619 ** get_address_of_m_CurrentSelected_8() { return &___m_CurrentSelected_8; }
	inline void set_m_CurrentSelected_8(GameObject_t1113636619 * value)
	{
		___m_CurrentSelected_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentSelected_8), value);
	}

	inline static int32_t get_offset_of_m_HasFocus_9() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_HasFocus_9)); }
	inline bool get_m_HasFocus_9() const { return ___m_HasFocus_9; }
	inline bool* get_address_of_m_HasFocus_9() { return &___m_HasFocus_9; }
	inline void set_m_HasFocus_9(bool value)
	{
		___m_HasFocus_9 = value;
	}

	inline static int32_t get_offset_of_m_SelectionGuard_10() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_SelectionGuard_10)); }
	inline bool get_m_SelectionGuard_10() const { return ___m_SelectionGuard_10; }
	inline bool* get_address_of_m_SelectionGuard_10() { return &___m_SelectionGuard_10; }
	inline void set_m_SelectionGuard_10(bool value)
	{
		___m_SelectionGuard_10 = value;
	}

	inline static int32_t get_offset_of_m_DummyData_11() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588, ___m_DummyData_11)); }
	inline BaseEventData_t3903027533 * get_m_DummyData_11() const { return ___m_DummyData_11; }
	inline BaseEventData_t3903027533 ** get_address_of_m_DummyData_11() { return &___m_DummyData_11; }
	inline void set_m_DummyData_11(BaseEventData_t3903027533 * value)
	{
		___m_DummyData_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_DummyData_11), value);
	}
};

struct EventSystem_t1003666588_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem> UnityEngine.EventSystems.EventSystem::m_EventSystems
	List_1_t2475741330 * ___m_EventSystems_4;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::s_RaycastComparer
	Comparison_1_t3135238028 * ___s_RaycastComparer_12;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::<>f__mg$cache0
	Comparison_1_t3135238028 * ___U3CU3Ef__mgU24cache0_13;

public:
	inline static int32_t get_offset_of_m_EventSystems_4() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588_StaticFields, ___m_EventSystems_4)); }
	inline List_1_t2475741330 * get_m_EventSystems_4() const { return ___m_EventSystems_4; }
	inline List_1_t2475741330 ** get_address_of_m_EventSystems_4() { return &___m_EventSystems_4; }
	inline void set_m_EventSystems_4(List_1_t2475741330 * value)
	{
		___m_EventSystems_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystems_4), value);
	}

	inline static int32_t get_offset_of_s_RaycastComparer_12() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588_StaticFields, ___s_RaycastComparer_12)); }
	inline Comparison_1_t3135238028 * get_s_RaycastComparer_12() const { return ___s_RaycastComparer_12; }
	inline Comparison_1_t3135238028 ** get_address_of_s_RaycastComparer_12() { return &___s_RaycastComparer_12; }
	inline void set_s_RaycastComparer_12(Comparison_1_t3135238028 * value)
	{
		___s_RaycastComparer_12 = value;
		Il2CppCodeGenWriteBarrier((&___s_RaycastComparer_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_13() { return static_cast<int32_t>(offsetof(EventSystem_t1003666588_StaticFields, ___U3CU3Ef__mgU24cache0_13)); }
	inline Comparison_1_t3135238028 * get_U3CU3Ef__mgU24cache0_13() const { return ___U3CU3Ef__mgU24cache0_13; }
	inline Comparison_1_t3135238028 ** get_address_of_U3CU3Ef__mgU24cache0_13() { return &___U3CU3Ef__mgU24cache0_13; }
	inline void set_U3CU3Ef__mgU24cache0_13(Comparison_1_t3135238028 * value)
	{
		___U3CU3Ef__mgU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTSYSTEM_T1003666588_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t2598313366 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_5)); }
	inline Color_t2555686324  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2555686324 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2555686324  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_7)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_9)); }
	inline Canvas_t3310196443 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t3310196443 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t340375123 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t340375123 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t3648964284 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t3648964284 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef SHIPTOUCHTODESTINATION_T4292119180_H
#define SHIPTOUCHTODESTINATION_T4292119180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShipTouchToDestination
struct  ShipTouchToDestination_t4292119180  : public ShipController_t2808759107
{
public:
	// System.Boolean ShipTouchToDestination::disableTouches
	bool ___disableTouches_7;
	// UnityEngine.Vector3 ShipTouchToDestination::mDestination
	Vector3_t3722313464  ___mDestination_8;

public:
	inline static int32_t get_offset_of_disableTouches_7() { return static_cast<int32_t>(offsetof(ShipTouchToDestination_t4292119180, ___disableTouches_7)); }
	inline bool get_disableTouches_7() const { return ___disableTouches_7; }
	inline bool* get_address_of_disableTouches_7() { return &___disableTouches_7; }
	inline void set_disableTouches_7(bool value)
	{
		___disableTouches_7 = value;
	}

	inline static int32_t get_offset_of_mDestination_8() { return static_cast<int32_t>(offsetof(ShipTouchToDestination_t4292119180, ___mDestination_8)); }
	inline Vector3_t3722313464  get_mDestination_8() const { return ___mDestination_8; }
	inline Vector3_t3722313464 * get_address_of_mDestination_8() { return &___mDestination_8; }
	inline void set_mDestination_8(Vector3_t3722313464  value)
	{
		___mDestination_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHIPTOUCHTODESTINATION_T4292119180_H
#ifndef BASERAYCASTER_T4150874583_H
#define BASERAYCASTER_T4150874583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseRaycaster
struct  BaseRaycaster_t4150874583  : public UIBehaviour_t3495933518
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASERAYCASTER_T4150874583_H
#ifndef SHIPTOUCHTOMOVE_T1387549923_H
#define SHIPTOUCHTOMOVE_T1387549923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShipTouchToMove
struct  ShipTouchToMove_t1387549923  : public ShipController_t2808759107
{
public:
	// UnityEngine.Vector2 ShipTouchToMove::mSensitivity
	Vector2_t2156229523  ___mSensitivity_7;
	// System.Single ShipTouchToMove::mSteering
	float ___mSteering_8;
	// System.Single ShipTouchToMove::mTargetSpeed
	float ___mTargetSpeed_9;
	// System.Single ShipTouchToMove::mTargetSteering
	float ___mTargetSteering_10;

public:
	inline static int32_t get_offset_of_mSensitivity_7() { return static_cast<int32_t>(offsetof(ShipTouchToMove_t1387549923, ___mSensitivity_7)); }
	inline Vector2_t2156229523  get_mSensitivity_7() const { return ___mSensitivity_7; }
	inline Vector2_t2156229523 * get_address_of_mSensitivity_7() { return &___mSensitivity_7; }
	inline void set_mSensitivity_7(Vector2_t2156229523  value)
	{
		___mSensitivity_7 = value;
	}

	inline static int32_t get_offset_of_mSteering_8() { return static_cast<int32_t>(offsetof(ShipTouchToMove_t1387549923, ___mSteering_8)); }
	inline float get_mSteering_8() const { return ___mSteering_8; }
	inline float* get_address_of_mSteering_8() { return &___mSteering_8; }
	inline void set_mSteering_8(float value)
	{
		___mSteering_8 = value;
	}

	inline static int32_t get_offset_of_mTargetSpeed_9() { return static_cast<int32_t>(offsetof(ShipTouchToMove_t1387549923, ___mTargetSpeed_9)); }
	inline float get_mTargetSpeed_9() const { return ___mTargetSpeed_9; }
	inline float* get_address_of_mTargetSpeed_9() { return &___mTargetSpeed_9; }
	inline void set_mTargetSpeed_9(float value)
	{
		___mTargetSpeed_9 = value;
	}

	inline static int32_t get_offset_of_mTargetSteering_10() { return static_cast<int32_t>(offsetof(ShipTouchToMove_t1387549923, ___mTargetSteering_10)); }
	inline float get_mTargetSteering_10() const { return ___mTargetSteering_10; }
	inline float* get_address_of_mTargetSteering_10() { return &___mTargetSteering_10; }
	inline void set_mTargetSteering_10(float value)
	{
		___mTargetSteering_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHIPTOUCHTOMOVE_T1387549923_H
#ifndef BASEINPUTMODULE_T2019268878_H
#define BASEINPUTMODULE_T2019268878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseInputModule
struct  BaseInputModule_t2019268878  : public UIBehaviour_t3495933518
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.BaseInputModule::m_RaycastResultCache
	List_1_t537414295 * ___m_RaycastResultCache_2;
	// UnityEngine.EventSystems.AxisEventData UnityEngine.EventSystems.BaseInputModule::m_AxisEventData
	AxisEventData_t2331243652 * ___m_AxisEventData_3;
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseInputModule::m_EventSystem
	EventSystem_t1003666588 * ___m_EventSystem_4;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::m_BaseEventData
	BaseEventData_t3903027533 * ___m_BaseEventData_5;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_InputOverride
	BaseInput_t3630163547 * ___m_InputOverride_6;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_DefaultInput
	BaseInput_t3630163547 * ___m_DefaultInput_7;

public:
	inline static int32_t get_offset_of_m_RaycastResultCache_2() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_RaycastResultCache_2)); }
	inline List_1_t537414295 * get_m_RaycastResultCache_2() const { return ___m_RaycastResultCache_2; }
	inline List_1_t537414295 ** get_address_of_m_RaycastResultCache_2() { return &___m_RaycastResultCache_2; }
	inline void set_m_RaycastResultCache_2(List_1_t537414295 * value)
	{
		___m_RaycastResultCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_RaycastResultCache_2), value);
	}

	inline static int32_t get_offset_of_m_AxisEventData_3() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_AxisEventData_3)); }
	inline AxisEventData_t2331243652 * get_m_AxisEventData_3() const { return ___m_AxisEventData_3; }
	inline AxisEventData_t2331243652 ** get_address_of_m_AxisEventData_3() { return &___m_AxisEventData_3; }
	inline void set_m_AxisEventData_3(AxisEventData_t2331243652 * value)
	{
		___m_AxisEventData_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AxisEventData_3), value);
	}

	inline static int32_t get_offset_of_m_EventSystem_4() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_EventSystem_4)); }
	inline EventSystem_t1003666588 * get_m_EventSystem_4() const { return ___m_EventSystem_4; }
	inline EventSystem_t1003666588 ** get_address_of_m_EventSystem_4() { return &___m_EventSystem_4; }
	inline void set_m_EventSystem_4(EventSystem_t1003666588 * value)
	{
		___m_EventSystem_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_4), value);
	}

	inline static int32_t get_offset_of_m_BaseEventData_5() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_BaseEventData_5)); }
	inline BaseEventData_t3903027533 * get_m_BaseEventData_5() const { return ___m_BaseEventData_5; }
	inline BaseEventData_t3903027533 ** get_address_of_m_BaseEventData_5() { return &___m_BaseEventData_5; }
	inline void set_m_BaseEventData_5(BaseEventData_t3903027533 * value)
	{
		___m_BaseEventData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_BaseEventData_5), value);
	}

	inline static int32_t get_offset_of_m_InputOverride_6() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_InputOverride_6)); }
	inline BaseInput_t3630163547 * get_m_InputOverride_6() const { return ___m_InputOverride_6; }
	inline BaseInput_t3630163547 ** get_address_of_m_InputOverride_6() { return &___m_InputOverride_6; }
	inline void set_m_InputOverride_6(BaseInput_t3630163547 * value)
	{
		___m_InputOverride_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputOverride_6), value);
	}

	inline static int32_t get_offset_of_m_DefaultInput_7() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_DefaultInput_7)); }
	inline BaseInput_t3630163547 * get_m_DefaultInput_7() const { return ___m_DefaultInput_7; }
	inline BaseInput_t3630163547 ** get_address_of_m_DefaultInput_7() { return &___m_DefaultInput_7; }
	inline void set_m_DefaultInput_7(BaseInput_t3630163547 * value)
	{
		___m_DefaultInput_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultInput_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUTMODULE_T2019268878_H
#ifndef LAYOUTGROUP_T2436138090_H
#define LAYOUTGROUP_T2436138090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup
struct  LayoutGroup_t2436138090  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::m_Padding
	RectOffset_t1369453676 * ___m_Padding_2;
	// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::m_ChildAlignment
	int32_t ___m_ChildAlignment_3;
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::m_Rect
	RectTransform_t3704657025 * ___m_Rect_4;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.LayoutGroup::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_5;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalMinSize
	Vector2_t2156229523  ___m_TotalMinSize_6;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalPreferredSize
	Vector2_t2156229523  ___m_TotalPreferredSize_7;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalFlexibleSize
	Vector2_t2156229523  ___m_TotalFlexibleSize_8;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::m_RectChildren
	List_1_t881764471 * ___m_RectChildren_9;

public:
	inline static int32_t get_offset_of_m_Padding_2() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_Padding_2)); }
	inline RectOffset_t1369453676 * get_m_Padding_2() const { return ___m_Padding_2; }
	inline RectOffset_t1369453676 ** get_address_of_m_Padding_2() { return &___m_Padding_2; }
	inline void set_m_Padding_2(RectOffset_t1369453676 * value)
	{
		___m_Padding_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_2), value);
	}

	inline static int32_t get_offset_of_m_ChildAlignment_3() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_ChildAlignment_3)); }
	inline int32_t get_m_ChildAlignment_3() const { return ___m_ChildAlignment_3; }
	inline int32_t* get_address_of_m_ChildAlignment_3() { return &___m_ChildAlignment_3; }
	inline void set_m_ChildAlignment_3(int32_t value)
	{
		___m_ChildAlignment_3 = value;
	}

	inline static int32_t get_offset_of_m_Rect_4() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_Rect_4)); }
	inline RectTransform_t3704657025 * get_m_Rect_4() const { return ___m_Rect_4; }
	inline RectTransform_t3704657025 ** get_address_of_m_Rect_4() { return &___m_Rect_4; }
	inline void set_m_Rect_4(RectTransform_t3704657025 * value)
	{
		___m_Rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_4), value);
	}

	inline static int32_t get_offset_of_m_Tracker_5() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_Tracker_5)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_5() const { return ___m_Tracker_5; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_5() { return &___m_Tracker_5; }
	inline void set_m_Tracker_5(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_5 = value;
	}

	inline static int32_t get_offset_of_m_TotalMinSize_6() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_TotalMinSize_6)); }
	inline Vector2_t2156229523  get_m_TotalMinSize_6() const { return ___m_TotalMinSize_6; }
	inline Vector2_t2156229523 * get_address_of_m_TotalMinSize_6() { return &___m_TotalMinSize_6; }
	inline void set_m_TotalMinSize_6(Vector2_t2156229523  value)
	{
		___m_TotalMinSize_6 = value;
	}

	inline static int32_t get_offset_of_m_TotalPreferredSize_7() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_TotalPreferredSize_7)); }
	inline Vector2_t2156229523  get_m_TotalPreferredSize_7() const { return ___m_TotalPreferredSize_7; }
	inline Vector2_t2156229523 * get_address_of_m_TotalPreferredSize_7() { return &___m_TotalPreferredSize_7; }
	inline void set_m_TotalPreferredSize_7(Vector2_t2156229523  value)
	{
		___m_TotalPreferredSize_7 = value;
	}

	inline static int32_t get_offset_of_m_TotalFlexibleSize_8() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_TotalFlexibleSize_8)); }
	inline Vector2_t2156229523  get_m_TotalFlexibleSize_8() const { return ___m_TotalFlexibleSize_8; }
	inline Vector2_t2156229523 * get_address_of_m_TotalFlexibleSize_8() { return &___m_TotalFlexibleSize_8; }
	inline void set_m_TotalFlexibleSize_8(Vector2_t2156229523  value)
	{
		___m_TotalFlexibleSize_8 = value;
	}

	inline static int32_t get_offset_of_m_RectChildren_9() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_RectChildren_9)); }
	inline List_1_t881764471 * get_m_RectChildren_9() const { return ___m_RectChildren_9; }
	inline List_1_t881764471 ** get_address_of_m_RectChildren_9() { return &___m_RectChildren_9; }
	inline void set_m_RectChildren_9(List_1_t881764471 * value)
	{
		___m_RectChildren_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectChildren_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTGROUP_T2436138090_H
#ifndef HORIZONTALORVERTICALLAYOUTGROUP_T729725570_H
#define HORIZONTALORVERTICALLAYOUTGROUP_T729725570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
struct  HorizontalOrVerticalLayoutGroup_t729725570  : public LayoutGroup_t2436138090
{
public:
	// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_Spacing
	float ___m_Spacing_10;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandWidth
	bool ___m_ChildForceExpandWidth_11;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandHeight
	bool ___m_ChildForceExpandHeight_12;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlWidth
	bool ___m_ChildControlWidth_13;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlHeight
	bool ___m_ChildControlHeight_14;

public:
	inline static int32_t get_offset_of_m_Spacing_10() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_Spacing_10)); }
	inline float get_m_Spacing_10() const { return ___m_Spacing_10; }
	inline float* get_address_of_m_Spacing_10() { return &___m_Spacing_10; }
	inline void set_m_Spacing_10(float value)
	{
		___m_Spacing_10 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandWidth_11() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_ChildForceExpandWidth_11)); }
	inline bool get_m_ChildForceExpandWidth_11() const { return ___m_ChildForceExpandWidth_11; }
	inline bool* get_address_of_m_ChildForceExpandWidth_11() { return &___m_ChildForceExpandWidth_11; }
	inline void set_m_ChildForceExpandWidth_11(bool value)
	{
		___m_ChildForceExpandWidth_11 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandHeight_12() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_ChildForceExpandHeight_12)); }
	inline bool get_m_ChildForceExpandHeight_12() const { return ___m_ChildForceExpandHeight_12; }
	inline bool* get_address_of_m_ChildForceExpandHeight_12() { return &___m_ChildForceExpandHeight_12; }
	inline void set_m_ChildForceExpandHeight_12(bool value)
	{
		___m_ChildForceExpandHeight_12 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlWidth_13() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_ChildControlWidth_13)); }
	inline bool get_m_ChildControlWidth_13() const { return ___m_ChildControlWidth_13; }
	inline bool* get_address_of_m_ChildControlWidth_13() { return &___m_ChildControlWidth_13; }
	inline void set_m_ChildControlWidth_13(bool value)
	{
		___m_ChildControlWidth_13 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlHeight_14() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_ChildControlHeight_14)); }
	inline bool get_m_ChildControlHeight_14() const { return ___m_ChildControlHeight_14; }
	inline bool* get_address_of_m_ChildControlHeight_14() { return &___m_ChildControlHeight_14; }
	inline void set_m_ChildControlHeight_14(bool value)
	{
		___m_ChildControlHeight_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALORVERTICALLAYOUTGROUP_T729725570_H
#ifndef BUTTON_T4055032469_H
#define BUTTON_T4055032469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Button
struct  Button_t4055032469  : public Selectable_t3250028441
{
public:
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_t48803504 * ___m_OnClick_16;

public:
	inline static int32_t get_offset_of_m_OnClick_16() { return static_cast<int32_t>(offsetof(Button_t4055032469, ___m_OnClick_16)); }
	inline ButtonClickedEvent_t48803504 * get_m_OnClick_16() const { return ___m_OnClick_16; }
	inline ButtonClickedEvent_t48803504 ** get_address_of_m_OnClick_16() { return &___m_OnClick_16; }
	inline void set_m_OnClick_16(ButtonClickedEvent_t48803504 * value)
	{
		___m_OnClick_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnClick_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON_T4055032469_H
#ifndef CAPTAIN_T4252579700_H
#define CAPTAIN_T4252579700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Captain
struct  Captain_t4252579700  : public HumanUnit_t2125463837
{
public:
	// SkillAbility/SkillValue Captain::mleadershipSkill
	SkillValue_t3350472883 * ___mleadershipSkill_5;

public:
	inline static int32_t get_offset_of_mleadershipSkill_5() { return static_cast<int32_t>(offsetof(Captain_t4252579700, ___mleadershipSkill_5)); }
	inline SkillValue_t3350472883 * get_mleadershipSkill_5() const { return ___mleadershipSkill_5; }
	inline SkillValue_t3350472883 ** get_address_of_mleadershipSkill_5() { return &___mleadershipSkill_5; }
	inline void set_mleadershipSkill_5(SkillValue_t3350472883 * value)
	{
		___mleadershipSkill_5 = value;
		Il2CppCodeGenWriteBarrier((&___mleadershipSkill_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTAIN_T4252579700_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_20)); }
	inline Material_t340375123 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t340375123 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_21)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef POINTERINPUTMODULE_T3453173740_H
#define POINTERINPUTMODULE_T3453173740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerInputModule
struct  PointerInputModule_t3453173740  : public BaseInputModule_t2019268878
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData> UnityEngine.EventSystems.PointerInputModule::m_PointerData
	Dictionary_2_t2696614423 * ___m_PointerData_12;
	// UnityEngine.EventSystems.PointerInputModule/MouseState UnityEngine.EventSystems.PointerInputModule::m_MouseState
	MouseState_t384203932 * ___m_MouseState_13;

public:
	inline static int32_t get_offset_of_m_PointerData_12() { return static_cast<int32_t>(offsetof(PointerInputModule_t3453173740, ___m_PointerData_12)); }
	inline Dictionary_2_t2696614423 * get_m_PointerData_12() const { return ___m_PointerData_12; }
	inline Dictionary_2_t2696614423 ** get_address_of_m_PointerData_12() { return &___m_PointerData_12; }
	inline void set_m_PointerData_12(Dictionary_2_t2696614423 * value)
	{
		___m_PointerData_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerData_12), value);
	}

	inline static int32_t get_offset_of_m_MouseState_13() { return static_cast<int32_t>(offsetof(PointerInputModule_t3453173740, ___m_MouseState_13)); }
	inline MouseState_t384203932 * get_m_MouseState_13() const { return ___m_MouseState_13; }
	inline MouseState_t384203932 ** get_address_of_m_MouseState_13() { return &___m_MouseState_13; }
	inline void set_m_MouseState_13(MouseState_t384203932 * value)
	{
		___m_MouseState_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_MouseState_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTERINPUTMODULE_T3453173740_H
#ifndef GRAPHICRAYCASTER_T2999697109_H
#define GRAPHICRAYCASTER_T2999697109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GraphicRaycaster
struct  GraphicRaycaster_t2999697109  : public BaseRaycaster_t4150874583
{
public:
	// System.Boolean UnityEngine.UI.GraphicRaycaster::m_IgnoreReversedGraphics
	bool ___m_IgnoreReversedGraphics_3;
	// UnityEngine.UI.GraphicRaycaster/BlockingObjects UnityEngine.UI.GraphicRaycaster::m_BlockingObjects
	int32_t ___m_BlockingObjects_4;
	// UnityEngine.LayerMask UnityEngine.UI.GraphicRaycaster::m_BlockingMask
	LayerMask_t3493934918  ___m_BlockingMask_5;
	// UnityEngine.Canvas UnityEngine.UI.GraphicRaycaster::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_6;
	// System.Collections.Generic.List`1<UnityEngine.UI.Graphic> UnityEngine.UI.GraphicRaycaster::m_RaycastResults
	List_1_t3132410353 * ___m_RaycastResults_7;

public:
	inline static int32_t get_offset_of_m_IgnoreReversedGraphics_3() { return static_cast<int32_t>(offsetof(GraphicRaycaster_t2999697109, ___m_IgnoreReversedGraphics_3)); }
	inline bool get_m_IgnoreReversedGraphics_3() const { return ___m_IgnoreReversedGraphics_3; }
	inline bool* get_address_of_m_IgnoreReversedGraphics_3() { return &___m_IgnoreReversedGraphics_3; }
	inline void set_m_IgnoreReversedGraphics_3(bool value)
	{
		___m_IgnoreReversedGraphics_3 = value;
	}

	inline static int32_t get_offset_of_m_BlockingObjects_4() { return static_cast<int32_t>(offsetof(GraphicRaycaster_t2999697109, ___m_BlockingObjects_4)); }
	inline int32_t get_m_BlockingObjects_4() const { return ___m_BlockingObjects_4; }
	inline int32_t* get_address_of_m_BlockingObjects_4() { return &___m_BlockingObjects_4; }
	inline void set_m_BlockingObjects_4(int32_t value)
	{
		___m_BlockingObjects_4 = value;
	}

	inline static int32_t get_offset_of_m_BlockingMask_5() { return static_cast<int32_t>(offsetof(GraphicRaycaster_t2999697109, ___m_BlockingMask_5)); }
	inline LayerMask_t3493934918  get_m_BlockingMask_5() const { return ___m_BlockingMask_5; }
	inline LayerMask_t3493934918 * get_address_of_m_BlockingMask_5() { return &___m_BlockingMask_5; }
	inline void set_m_BlockingMask_5(LayerMask_t3493934918  value)
	{
		___m_BlockingMask_5 = value;
	}

	inline static int32_t get_offset_of_m_Canvas_6() { return static_cast<int32_t>(offsetof(GraphicRaycaster_t2999697109, ___m_Canvas_6)); }
	inline Canvas_t3310196443 * get_m_Canvas_6() const { return ___m_Canvas_6; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_6() { return &___m_Canvas_6; }
	inline void set_m_Canvas_6(Canvas_t3310196443 * value)
	{
		___m_Canvas_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_6), value);
	}

	inline static int32_t get_offset_of_m_RaycastResults_7() { return static_cast<int32_t>(offsetof(GraphicRaycaster_t2999697109, ___m_RaycastResults_7)); }
	inline List_1_t3132410353 * get_m_RaycastResults_7() const { return ___m_RaycastResults_7; }
	inline List_1_t3132410353 ** get_address_of_m_RaycastResults_7() { return &___m_RaycastResults_7; }
	inline void set_m_RaycastResults_7(List_1_t3132410353 * value)
	{
		___m_RaycastResults_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RaycastResults_7), value);
	}
};

struct GraphicRaycaster_t2999697109_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Graphic> UnityEngine.UI.GraphicRaycaster::s_SortedGraphics
	List_1_t3132410353 * ___s_SortedGraphics_8;
	// System.Comparison`1<UnityEngine.UI.Graphic> UnityEngine.UI.GraphicRaycaster::<>f__am$cache0
	Comparison_1_t1435266790 * ___U3CU3Ef__amU24cache0_9;

public:
	inline static int32_t get_offset_of_s_SortedGraphics_8() { return static_cast<int32_t>(offsetof(GraphicRaycaster_t2999697109_StaticFields, ___s_SortedGraphics_8)); }
	inline List_1_t3132410353 * get_s_SortedGraphics_8() const { return ___s_SortedGraphics_8; }
	inline List_1_t3132410353 ** get_address_of_s_SortedGraphics_8() { return &___s_SortedGraphics_8; }
	inline void set_s_SortedGraphics_8(List_1_t3132410353 * value)
	{
		___s_SortedGraphics_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_SortedGraphics_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(GraphicRaycaster_t2999697109_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Comparison_1_t1435266790 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Comparison_1_t1435266790 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Comparison_1_t1435266790 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHICRAYCASTER_T2999697109_H
#ifndef TEXT_T1901882714_H
#define TEXT_T1901882714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t1901882714  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t746620069 * ___m_FontData_28;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_29;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t3211863866 * ___m_TextCache_30;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t3211863866 * ___m_TextCacheForLayout_31;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_33;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t1981460040* ___m_TempVerts_34;

public:
	inline static int32_t get_offset_of_m_FontData_28() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_FontData_28)); }
	inline FontData_t746620069 * get_m_FontData_28() const { return ___m_FontData_28; }
	inline FontData_t746620069 ** get_address_of_m_FontData_28() { return &___m_FontData_28; }
	inline void set_m_FontData_28(FontData_t746620069 * value)
	{
		___m_FontData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_28), value);
	}

	inline static int32_t get_offset_of_m_Text_29() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_Text_29)); }
	inline String_t* get_m_Text_29() const { return ___m_Text_29; }
	inline String_t** get_address_of_m_Text_29() { return &___m_Text_29; }
	inline void set_m_Text_29(String_t* value)
	{
		___m_Text_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_29), value);
	}

	inline static int32_t get_offset_of_m_TextCache_30() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCache_30)); }
	inline TextGenerator_t3211863866 * get_m_TextCache_30() const { return ___m_TextCache_30; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCache_30() { return &___m_TextCache_30; }
	inline void set_m_TextCache_30(TextGenerator_t3211863866 * value)
	{
		___m_TextCache_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_30), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_31() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCacheForLayout_31)); }
	inline TextGenerator_t3211863866 * get_m_TextCacheForLayout_31() const { return ___m_TextCacheForLayout_31; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCacheForLayout_31() { return &___m_TextCacheForLayout_31; }
	inline void set_m_TextCacheForLayout_31(TextGenerator_t3211863866 * value)
	{
		___m_TextCacheForLayout_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_31), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_33() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_DisableFontTextureRebuiltCallback_33)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_33() const { return ___m_DisableFontTextureRebuiltCallback_33; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_33() { return &___m_DisableFontTextureRebuiltCallback_33; }
	inline void set_m_DisableFontTextureRebuiltCallback_33(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_33 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_34() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TempVerts_34)); }
	inline UIVertexU5BU5D_t1981460040* get_m_TempVerts_34() const { return ___m_TempVerts_34; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_TempVerts_34() { return &___m_TempVerts_34; }
	inline void set_m_TempVerts_34(UIVertexU5BU5D_t1981460040* value)
	{
		___m_TempVerts_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_34), value);
	}
};

struct Text_t1901882714_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t340375123 * ___s_DefaultText_32;

public:
	inline static int32_t get_offset_of_s_DefaultText_32() { return static_cast<int32_t>(offsetof(Text_t1901882714_StaticFields, ___s_DefaultText_32)); }
	inline Material_t340375123 * get_s_DefaultText_32() const { return ___s_DefaultText_32; }
	inline Material_t340375123 ** get_address_of_s_DefaultText_32() { return &___s_DefaultText_32; }
	inline void set_s_DefaultText_32(Material_t340375123 * value)
	{
		___s_DefaultText_32 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T1901882714_H
#ifndef VERTICALLAYOUTGROUP_T923838031_H
#define VERTICALLAYOUTGROUP_T923838031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VerticalLayoutGroup
struct  VerticalLayoutGroup_t923838031  : public HorizontalOrVerticalLayoutGroup_t729725570
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTICALLAYOUTGROUP_T923838031_H
#ifndef IMAGE_T2670269651_H
#define IMAGE_T2670269651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image
struct  Image_t2670269651  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t280657092 * ___m_Sprite_29;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t280657092 * ___m_OverrideSprite_30;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_31;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_32;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_33;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_34;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_35;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_36;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_37;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_38;

public:
	inline static int32_t get_offset_of_m_Sprite_29() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Sprite_29)); }
	inline Sprite_t280657092 * get_m_Sprite_29() const { return ___m_Sprite_29; }
	inline Sprite_t280657092 ** get_address_of_m_Sprite_29() { return &___m_Sprite_29; }
	inline void set_m_Sprite_29(Sprite_t280657092 * value)
	{
		___m_Sprite_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sprite_29), value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_30() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_OverrideSprite_30)); }
	inline Sprite_t280657092 * get_m_OverrideSprite_30() const { return ___m_OverrideSprite_30; }
	inline Sprite_t280657092 ** get_address_of_m_OverrideSprite_30() { return &___m_OverrideSprite_30; }
	inline void set_m_OverrideSprite_30(Sprite_t280657092 * value)
	{
		___m_OverrideSprite_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_OverrideSprite_30), value);
	}

	inline static int32_t get_offset_of_m_Type_31() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Type_31)); }
	inline int32_t get_m_Type_31() const { return ___m_Type_31; }
	inline int32_t* get_address_of_m_Type_31() { return &___m_Type_31; }
	inline void set_m_Type_31(int32_t value)
	{
		___m_Type_31 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_32() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_PreserveAspect_32)); }
	inline bool get_m_PreserveAspect_32() const { return ___m_PreserveAspect_32; }
	inline bool* get_address_of_m_PreserveAspect_32() { return &___m_PreserveAspect_32; }
	inline void set_m_PreserveAspect_32(bool value)
	{
		___m_PreserveAspect_32 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_33() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillCenter_33)); }
	inline bool get_m_FillCenter_33() const { return ___m_FillCenter_33; }
	inline bool* get_address_of_m_FillCenter_33() { return &___m_FillCenter_33; }
	inline void set_m_FillCenter_33(bool value)
	{
		___m_FillCenter_33 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_34() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillMethod_34)); }
	inline int32_t get_m_FillMethod_34() const { return ___m_FillMethod_34; }
	inline int32_t* get_address_of_m_FillMethod_34() { return &___m_FillMethod_34; }
	inline void set_m_FillMethod_34(int32_t value)
	{
		___m_FillMethod_34 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_35() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillAmount_35)); }
	inline float get_m_FillAmount_35() const { return ___m_FillAmount_35; }
	inline float* get_address_of_m_FillAmount_35() { return &___m_FillAmount_35; }
	inline void set_m_FillAmount_35(float value)
	{
		___m_FillAmount_35 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_36() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillClockwise_36)); }
	inline bool get_m_FillClockwise_36() const { return ___m_FillClockwise_36; }
	inline bool* get_address_of_m_FillClockwise_36() { return &___m_FillClockwise_36; }
	inline void set_m_FillClockwise_36(bool value)
	{
		___m_FillClockwise_36 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_37() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillOrigin_37)); }
	inline int32_t get_m_FillOrigin_37() const { return ___m_FillOrigin_37; }
	inline int32_t* get_address_of_m_FillOrigin_37() { return &___m_FillOrigin_37; }
	inline void set_m_FillOrigin_37(int32_t value)
	{
		___m_FillOrigin_37 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_38() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_AlphaHitTestMinimumThreshold_38)); }
	inline float get_m_AlphaHitTestMinimumThreshold_38() const { return ___m_AlphaHitTestMinimumThreshold_38; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_38() { return &___m_AlphaHitTestMinimumThreshold_38; }
	inline void set_m_AlphaHitTestMinimumThreshold_38(float value)
	{
		___m_AlphaHitTestMinimumThreshold_38 = value;
	}
};

struct Image_t2670269651_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t340375123 * ___s_ETC1DefaultUI_28;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_t1457185986* ___s_VertScratch_39;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_t1457185986* ___s_UVScratch_40;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t1718750761* ___s_Xy_41;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t1718750761* ___s_Uv_42;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_28() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_ETC1DefaultUI_28)); }
	inline Material_t340375123 * get_s_ETC1DefaultUI_28() const { return ___s_ETC1DefaultUI_28; }
	inline Material_t340375123 ** get_address_of_s_ETC1DefaultUI_28() { return &___s_ETC1DefaultUI_28; }
	inline void set_s_ETC1DefaultUI_28(Material_t340375123 * value)
	{
		___s_ETC1DefaultUI_28 = value;
		Il2CppCodeGenWriteBarrier((&___s_ETC1DefaultUI_28), value);
	}

	inline static int32_t get_offset_of_s_VertScratch_39() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_VertScratch_39)); }
	inline Vector2U5BU5D_t1457185986* get_s_VertScratch_39() const { return ___s_VertScratch_39; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_VertScratch_39() { return &___s_VertScratch_39; }
	inline void set_s_VertScratch_39(Vector2U5BU5D_t1457185986* value)
	{
		___s_VertScratch_39 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertScratch_39), value);
	}

	inline static int32_t get_offset_of_s_UVScratch_40() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_UVScratch_40)); }
	inline Vector2U5BU5D_t1457185986* get_s_UVScratch_40() const { return ___s_UVScratch_40; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_UVScratch_40() { return &___s_UVScratch_40; }
	inline void set_s_UVScratch_40(Vector2U5BU5D_t1457185986* value)
	{
		___s_UVScratch_40 = value;
		Il2CppCodeGenWriteBarrier((&___s_UVScratch_40), value);
	}

	inline static int32_t get_offset_of_s_Xy_41() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Xy_41)); }
	inline Vector3U5BU5D_t1718750761* get_s_Xy_41() const { return ___s_Xy_41; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Xy_41() { return &___s_Xy_41; }
	inline void set_s_Xy_41(Vector3U5BU5D_t1718750761* value)
	{
		___s_Xy_41 = value;
		Il2CppCodeGenWriteBarrier((&___s_Xy_41), value);
	}

	inline static int32_t get_offset_of_s_Uv_42() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Uv_42)); }
	inline Vector3U5BU5D_t1718750761* get_s_Uv_42() const { return ___s_Uv_42; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Uv_42() { return &___s_Uv_42; }
	inline void set_s_Uv_42(Vector3U5BU5D_t1718750761* value)
	{
		___s_Uv_42 = value;
		Il2CppCodeGenWriteBarrier((&___s_Uv_42), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T2670269651_H
#ifndef STANDALONEINPUTMODULE_T2760469101_H
#define STANDALONEINPUTMODULE_T2760469101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.StandaloneInputModule
struct  StandaloneInputModule_t2760469101  : public PointerInputModule_t3453173740
{
public:
	// System.Single UnityEngine.EventSystems.StandaloneInputModule::m_PrevActionTime
	float ___m_PrevActionTime_14;
	// UnityEngine.Vector2 UnityEngine.EventSystems.StandaloneInputModule::m_LastMoveVector
	Vector2_t2156229523  ___m_LastMoveVector_15;
	// System.Int32 UnityEngine.EventSystems.StandaloneInputModule::m_ConsecutiveMoveCount
	int32_t ___m_ConsecutiveMoveCount_16;
	// UnityEngine.Vector2 UnityEngine.EventSystems.StandaloneInputModule::m_LastMousePosition
	Vector2_t2156229523  ___m_LastMousePosition_17;
	// UnityEngine.Vector2 UnityEngine.EventSystems.StandaloneInputModule::m_MousePosition
	Vector2_t2156229523  ___m_MousePosition_18;
	// UnityEngine.GameObject UnityEngine.EventSystems.StandaloneInputModule::m_CurrentFocusedGameObject
	GameObject_t1113636619 * ___m_CurrentFocusedGameObject_19;
	// System.String UnityEngine.EventSystems.StandaloneInputModule::m_HorizontalAxis
	String_t* ___m_HorizontalAxis_20;
	// System.String UnityEngine.EventSystems.StandaloneInputModule::m_VerticalAxis
	String_t* ___m_VerticalAxis_21;
	// System.String UnityEngine.EventSystems.StandaloneInputModule::m_SubmitButton
	String_t* ___m_SubmitButton_22;
	// System.String UnityEngine.EventSystems.StandaloneInputModule::m_CancelButton
	String_t* ___m_CancelButton_23;
	// System.Single UnityEngine.EventSystems.StandaloneInputModule::m_InputActionsPerSecond
	float ___m_InputActionsPerSecond_24;
	// System.Single UnityEngine.EventSystems.StandaloneInputModule::m_RepeatDelay
	float ___m_RepeatDelay_25;
	// System.Boolean UnityEngine.EventSystems.StandaloneInputModule::m_ForceModuleActive
	bool ___m_ForceModuleActive_26;

public:
	inline static int32_t get_offset_of_m_PrevActionTime_14() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_PrevActionTime_14)); }
	inline float get_m_PrevActionTime_14() const { return ___m_PrevActionTime_14; }
	inline float* get_address_of_m_PrevActionTime_14() { return &___m_PrevActionTime_14; }
	inline void set_m_PrevActionTime_14(float value)
	{
		___m_PrevActionTime_14 = value;
	}

	inline static int32_t get_offset_of_m_LastMoveVector_15() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_LastMoveVector_15)); }
	inline Vector2_t2156229523  get_m_LastMoveVector_15() const { return ___m_LastMoveVector_15; }
	inline Vector2_t2156229523 * get_address_of_m_LastMoveVector_15() { return &___m_LastMoveVector_15; }
	inline void set_m_LastMoveVector_15(Vector2_t2156229523  value)
	{
		___m_LastMoveVector_15 = value;
	}

	inline static int32_t get_offset_of_m_ConsecutiveMoveCount_16() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_ConsecutiveMoveCount_16)); }
	inline int32_t get_m_ConsecutiveMoveCount_16() const { return ___m_ConsecutiveMoveCount_16; }
	inline int32_t* get_address_of_m_ConsecutiveMoveCount_16() { return &___m_ConsecutiveMoveCount_16; }
	inline void set_m_ConsecutiveMoveCount_16(int32_t value)
	{
		___m_ConsecutiveMoveCount_16 = value;
	}

	inline static int32_t get_offset_of_m_LastMousePosition_17() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_LastMousePosition_17)); }
	inline Vector2_t2156229523  get_m_LastMousePosition_17() const { return ___m_LastMousePosition_17; }
	inline Vector2_t2156229523 * get_address_of_m_LastMousePosition_17() { return &___m_LastMousePosition_17; }
	inline void set_m_LastMousePosition_17(Vector2_t2156229523  value)
	{
		___m_LastMousePosition_17 = value;
	}

	inline static int32_t get_offset_of_m_MousePosition_18() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_MousePosition_18)); }
	inline Vector2_t2156229523  get_m_MousePosition_18() const { return ___m_MousePosition_18; }
	inline Vector2_t2156229523 * get_address_of_m_MousePosition_18() { return &___m_MousePosition_18; }
	inline void set_m_MousePosition_18(Vector2_t2156229523  value)
	{
		___m_MousePosition_18 = value;
	}

	inline static int32_t get_offset_of_m_CurrentFocusedGameObject_19() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_CurrentFocusedGameObject_19)); }
	inline GameObject_t1113636619 * get_m_CurrentFocusedGameObject_19() const { return ___m_CurrentFocusedGameObject_19; }
	inline GameObject_t1113636619 ** get_address_of_m_CurrentFocusedGameObject_19() { return &___m_CurrentFocusedGameObject_19; }
	inline void set_m_CurrentFocusedGameObject_19(GameObject_t1113636619 * value)
	{
		___m_CurrentFocusedGameObject_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentFocusedGameObject_19), value);
	}

	inline static int32_t get_offset_of_m_HorizontalAxis_20() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_HorizontalAxis_20)); }
	inline String_t* get_m_HorizontalAxis_20() const { return ___m_HorizontalAxis_20; }
	inline String_t** get_address_of_m_HorizontalAxis_20() { return &___m_HorizontalAxis_20; }
	inline void set_m_HorizontalAxis_20(String_t* value)
	{
		___m_HorizontalAxis_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalAxis_20), value);
	}

	inline static int32_t get_offset_of_m_VerticalAxis_21() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_VerticalAxis_21)); }
	inline String_t* get_m_VerticalAxis_21() const { return ___m_VerticalAxis_21; }
	inline String_t** get_address_of_m_VerticalAxis_21() { return &___m_VerticalAxis_21; }
	inline void set_m_VerticalAxis_21(String_t* value)
	{
		___m_VerticalAxis_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalAxis_21), value);
	}

	inline static int32_t get_offset_of_m_SubmitButton_22() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_SubmitButton_22)); }
	inline String_t* get_m_SubmitButton_22() const { return ___m_SubmitButton_22; }
	inline String_t** get_address_of_m_SubmitButton_22() { return &___m_SubmitButton_22; }
	inline void set_m_SubmitButton_22(String_t* value)
	{
		___m_SubmitButton_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_SubmitButton_22), value);
	}

	inline static int32_t get_offset_of_m_CancelButton_23() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_CancelButton_23)); }
	inline String_t* get_m_CancelButton_23() const { return ___m_CancelButton_23; }
	inline String_t** get_address_of_m_CancelButton_23() { return &___m_CancelButton_23; }
	inline void set_m_CancelButton_23(String_t* value)
	{
		___m_CancelButton_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_CancelButton_23), value);
	}

	inline static int32_t get_offset_of_m_InputActionsPerSecond_24() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_InputActionsPerSecond_24)); }
	inline float get_m_InputActionsPerSecond_24() const { return ___m_InputActionsPerSecond_24; }
	inline float* get_address_of_m_InputActionsPerSecond_24() { return &___m_InputActionsPerSecond_24; }
	inline void set_m_InputActionsPerSecond_24(float value)
	{
		___m_InputActionsPerSecond_24 = value;
	}

	inline static int32_t get_offset_of_m_RepeatDelay_25() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_RepeatDelay_25)); }
	inline float get_m_RepeatDelay_25() const { return ___m_RepeatDelay_25; }
	inline float* get_address_of_m_RepeatDelay_25() { return &___m_RepeatDelay_25; }
	inline void set_m_RepeatDelay_25(float value)
	{
		___m_RepeatDelay_25 = value;
	}

	inline static int32_t get_offset_of_m_ForceModuleActive_26() { return static_cast<int32_t>(offsetof(StandaloneInputModule_t2760469101, ___m_ForceModuleActive_26)); }
	inline bool get_m_ForceModuleActive_26() const { return ___m_ForceModuleActive_26; }
	inline bool* get_address_of_m_ForceModuleActive_26() { return &___m_ForceModuleActive_26; }
	inline void set_m_ForceModuleActive_26(bool value)
	{
		___m_ForceModuleActive_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDALONEINPUTMODULE_T2760469101_H
#ifndef HORIZONTALLAYOUTGROUP_T2586782146_H
#define HORIZONTALLAYOUTGROUP_T2586782146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalLayoutGroup
struct  HorizontalLayoutGroup_t2586782146  : public HorizontalOrVerticalLayoutGroup_t729725570
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALLAYOUTGROUP_T2586782146_H
#ifndef RAWIMAGE_T3182918964_H
#define RAWIMAGE_T3182918964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.RawImage
struct  RawImage_t3182918964  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.Texture UnityEngine.UI.RawImage::m_Texture
	Texture_t3661962703 * ___m_Texture_28;
	// UnityEngine.Rect UnityEngine.UI.RawImage::m_UVRect
	Rect_t2360479859  ___m_UVRect_29;

public:
	inline static int32_t get_offset_of_m_Texture_28() { return static_cast<int32_t>(offsetof(RawImage_t3182918964, ___m_Texture_28)); }
	inline Texture_t3661962703 * get_m_Texture_28() const { return ___m_Texture_28; }
	inline Texture_t3661962703 ** get_address_of_m_Texture_28() { return &___m_Texture_28; }
	inline void set_m_Texture_28(Texture_t3661962703 * value)
	{
		___m_Texture_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_Texture_28), value);
	}

	inline static int32_t get_offset_of_m_UVRect_29() { return static_cast<int32_t>(offsetof(RawImage_t3182918964, ___m_UVRect_29)); }
	inline Rect_t2360479859  get_m_UVRect_29() const { return ___m_UVRect_29; }
	inline Rect_t2360479859 * get_address_of_m_UVRect_29() { return &___m_UVRect_29; }
	inline void set_m_UVRect_29(Rect_t2360479859  value)
	{
		___m_UVRect_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWIMAGE_T3182918964_H
// UnityEngine.Font[]
struct FontU5BU5D_t1399044585  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Font_t1956802104 * m_Items[1];

public:
	inline Font_t1956802104 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Font_t1956802104 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Font_t1956802104 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Font_t1956802104 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Font_t1956802104 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Font_t1956802104 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Type[]
struct TypeU5BU5D_t3940880105  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t3722313464  m_Items[1];

public:
	inline Vector3_t3722313464  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t3722313464 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t3722313464  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t3722313464  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t3722313464 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t3722313464  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Renderer[]
struct RendererU5BU5D_t3210418286  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Renderer_t2627027031 * m_Items[1];

public:
	inline Renderer_t2627027031 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Renderer_t2627027031 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Renderer_t2627027031 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Renderer_t2627027031 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Renderer_t2627027031 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Renderer_t2627027031 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Single[0...,0...]
struct SingleU5B0___U2C0___U5D_t1444911252  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
	inline float GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, float value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, float value)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t2843939325  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Single[0...,0...,0...]
struct SingleU5B0___U2C0___U2C0___U5D_t1444911253  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
	inline float GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);
		il2cpp_array_size_t kBound = bounds[2].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(k, kBound);

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);
		il2cpp_array_size_t kBound = bounds[2].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(k, kBound);

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k, float value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);
		il2cpp_array_size_t kBound = bounds[2].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(k, kBound);

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k) const
	{
		il2cpp_array_size_t jBound = bounds[1].length;
		il2cpp_array_size_t kBound = bounds[2].length;

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k)
	{
		il2cpp_array_size_t jBound = bounds[1].length;
		il2cpp_array_size_t kBound = bounds[2].length;

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k, float value)
	{
		il2cpp_array_size_t jBound = bounds[1].length;
		il2cpp_array_size_t kBound = bounds[2].length;

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		m_Items[index] = value;
	}
};
// UnityEngine.Transform[]
struct TransformU5BU5D_t807237628  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Transform_t3600365921 * m_Items[1];

public:
	inline Transform_t3600365921 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Transform_t3600365921 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Transform_t3600365921 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Transform_t3600365921 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Transform_t3600365921 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Transform_t3600365921 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Collider[]
struct ColliderU5BU5D_t4234922487  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Collider_t1773347010 * m_Items[1];

public:
	inline Collider_t1773347010 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Collider_t1773347010 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Collider_t1773347010 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Collider_t1773347010 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Collider_t1773347010 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Collider_t1773347010 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_t1113636619 * m_Items[1];

public:
	inline GameObject_t1113636619 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_t1113636619 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_t1113636619 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GameObject_t1113636619 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_t1113636619 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_t1113636619 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Object[]
struct ObjectU5BU5D_t1417781964  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Object_t631007953 * m_Items[1];

public:
	inline Object_t631007953 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Object_t631007953 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Object_t631007953 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Object_t631007953 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Object_t631007953 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Object_t631007953 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.Resources::FindObjectsOfTypeAll<System.Object>()
extern "C"  ObjectU5BU5D_t2843939325* Resources_FindObjectsOfTypeAll_TisRuntimeObject_m2909456956_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m2287542950_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2730968292_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  RuntimeObject * Object_FindObjectOfType_TisRuntimeObject_m2612646359_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m3697625829_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
extern "C"  bool List_1_Remove_m1416767016_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t2843939325* GameObject_GetComponentsInChildren_TisRuntimeObject_m1982918030_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// T Entity::Find<System.Object>(UnityEngine.GameObject)
extern "C"  RuntimeObject * Entity_Find_TisRuntimeObject_m310867926_gshared (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___go0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<SkillAbility/SkillEnum,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m2768249451_gshared (Dictionary_2_t167567878 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<SkillAbility/SkillEnum,System.Object>::ContainsKey(!0)
extern "C"  bool Dictionary_2_ContainsKey_m3302013818_gshared (Dictionary_2_t167567878 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<SkillAbility/SkillEnum,System.Object>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m2781824001_gshared (Dictionary_2_t167567878 * __this, int32_t p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<SkillAbility/SkillEnum,System.Object>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m2931076137_gshared (Dictionary_2_t167567878 * __this, int32_t p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<SkillAbility/SkillEnum,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2121750653  Dictionary_2_GetEnumerator_m2019897888_gshared (Dictionary_2_t167567878 * __this, const RuntimeMethod* method);
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<SkillAbility/SkillEnum,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t2565240045  Enumerator_get_Current_m2680200070_gshared (Enumerator_t2121750653 * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.KeyValuePair`2<SkillAbility/SkillEnum,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m2847946307_gshared (KeyValuePair_2_t2565240045 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<SkillAbility/SkillEnum,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m917637076_gshared (Enumerator_t2121750653 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<SkillAbility/SkillEnum,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1149657600_gshared (Enumerator_t2121750653 * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<SkillAbility/SkillEnum,System.Object>::get_Item(!0)
extern "C"  RuntimeObject * Dictionary_2_get_Item_m3281886030_gshared (Dictionary_2_t167567878 * __this, int32_t p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(!0)
extern "C"  bool List_1_Contains_m2654125393_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t2843939325* Component_GetComponentsInChildren_TisRuntimeObject_m1308288322_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Single>::Push(!0)
extern "C"  void Stack_1_Push_m2927642228_gshared (Stack_1_t2240656229 * __this, float p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<uGuiLayout/Mode>::Push(!0)
extern "C"  void Stack_1_Push_m3148378454_gshared (Stack_1_t4010345780 * __this, int32_t p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.Stack`1<System.Single>::Pop()
extern "C"  float Stack_1_Pop_m3170417723_gshared (Stack_1_t2240656229 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.Stack`1<uGuiLayout/Mode>::Pop()
extern "C"  int32_t Stack_1_Pop_m2243551095_gshared (Stack_1_t4010345780 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Stack`1<System.Object>::get_Count()
extern "C"  int32_t Stack_1_get_Count_m1599740434_gshared (Stack_1_t3923495619 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.Stack`1<System.Object>::Peek()
extern "C"  RuntimeObject * Stack_1_Peek_m326384648_gshared (Stack_1_t3923495619 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::Push(!0)
extern "C"  void Stack_1_Push_m1669856732_gshared (Stack_1_t3923495619 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.Stack`1<System.Object>::Pop()
extern "C"  RuntimeObject * Stack_1_Pop_m756553478_gshared (Stack_1_t3923495619 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor()
extern "C"  void Stack_1__ctor_m3164958980_gshared (Stack_1_t3923495619 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Single>::.ctor()
extern "C"  void Stack_1__ctor_m20774007_gshared (Stack_1_t2240656229 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<uGuiLayout/Mode>::.ctor()
extern "C"  void Stack_1__ctor_m2382698871_gshared (Stack_1_t4010345780 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponentInChildren_TisRuntimeObject_m1513755678_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);

// System.Void HumanUnit::.ctor()
extern "C"  void HumanUnit__ctor_m1823562469 (HumanUnit_t2125463837 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SkillAbility/SkillAbilityCallBack::.ctor(System.Object,System.IntPtr)
extern "C"  void SkillAbilityCallBack__ctor_m3872213263 (SkillAbilityCallBack_t2618462130 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SkillAbility/SkillValue::.ctor(SkillAbility/SkillEnum,System.Single,SkillAbility/SkillAbilityCallBack)
extern "C"  void SkillValue__ctor_m488509249 (SkillValue_t3350472883 * __this, int32_t ___se0, float ___v1, SkillAbilityCallBack_t2618462130 * ___cb2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SkillAbility::AddSkill(SkillAbility/SkillValue)
extern "C"  void SkillAbility_AddSkill_m539831979 (SkillAbility_t2516669324 * __this, SkillValue_t3350472883 * ___skillValue0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
extern "C"  int32_t Mathf_FloorToInt_m1870542928 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Convert::ToUInt32(System.Int32)
extern "C"  uint32_t Convert_ToUInt32_m2215525276 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean SkillAbility::ApplySkill(SkillAbility/SkillEnum,System.Single)
extern "C"  bool SkillAbility_ApplySkill_m1333928009 (SkillAbility_t2516669324 * __this, int32_t ___skill0, float ___data1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t2156229523  Vector2_get_zero_m540426400 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
#define List_1__ctor_m706204246(__this, method) ((  void (*) (List_1_t3319525431 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<WorldCanvasUI>()
#define GameObject_GetComponent_TisWorldCanvasUI_t1156772134_m2038315626(__this, method) ((  WorldCanvasUI_t1156772134 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<WorldCanvasUI>()
#define GameObject_AddComponent_TisWorldCanvasUI_t1156772134_m3564025873(__this, method) ((  WorldCanvasUI_t1156772134 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// UnityEngine.GameObject WorldCanvasUI::get_CanvasGO()
extern "C"  GameObject_t1113636619 * WorldCanvasUI_get_CanvasGO_m332321093 (WorldCanvasUI_t1156772134 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<RotateTowardCamera>()
#define GameObject_AddComponent_TisRotateTowardCamera_t118585142_m2773773848(__this, method) ((  RotateTowardCamera_t118585142 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C"  void GameObject__ctor_m2093116449 (GameObject_t1113636619 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3600365921 * GameObject_get_transform_m1369836730 (GameObject_t1113636619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
extern "C"  void Transform_SetParent_m381167889 (Transform_t3600365921 * __this, Transform_t3600365921 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.UI.Image>()
#define GameObject_AddComponent_TisImage_t2670269651_m1594579417(__this, method) ((  Image_t2670269651 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// System.Void UnityEngine.UI.Image::set_fillCenter(System.Boolean)
extern "C"  void Image_set_fillCenter_m3776786491 (Image_t2670269651 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
extern "C"  void Image_set_sprite_m2369174689 (Image_t2670269651 * __this, Sprite_t280657092 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool Vector2_op_Equality_m2303255133 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.Sprite::get_rect()
extern "C"  Rect_t2360479859  Sprite_get_rect_m2575211689 (Sprite_t280657092 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_width()
extern "C"  float Rect_get_width_m3421484486 (Rect_t2360479859 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m1358425599 (Rect_t2360479859 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3970636864 (Vector2_t2156229523 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform UnityEngine.UI.Graphic::get_rectTransform()
extern "C"  RectTransform_t3704657025 * Graphic_get_rectTransform_m1167152468 (Graphic_t1660335611 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C"  void RectTransform_set_sizeDelta_m3462269772 (RectTransform_t3704657025 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.UI.RawImage>()
#define GameObject_AddComponent_TisRawImage_t3182918964_m629883703(__this, method) ((  RawImage_t3182918964 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// System.Void UnityEngine.UI.RawImage::set_texture(UnityEngine.Texture)
extern "C"  void RawImage_set_texture_m415027901 (RawImage_t3182918964 * __this, Texture_t3661962703 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.UI.Text>()
#define GameObject_AddComponent_TisText_t1901882714_m942857714(__this, method) ((  Text_t1901882714 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMin_m4230103102 (RectTransform_t3704657025 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
extern "C"  Vector2_t2156229523  Vector2_get_one_m738793577 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMax_m2998668828 (RectTransform_t3704657025 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchoredPosition_m4126691837 (RectTransform_t3704657025 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m3574996620 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Text::set_font(UnityEngine.Font)
extern "C"  void Text_set_font_m2192091651 (Text_t1901882714 * __this, Font_t1956802104 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.Resources::FindObjectsOfTypeAll<UnityEngine.Font>()
#define Resources_FindObjectsOfTypeAll_TisFont_t1956802104_m2894531458(__this /* static, unused */, method) ((  FontU5BU5D_t1399044585* (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Resources_FindObjectsOfTypeAll_TisRuntimeObject_m2909456956_gshared)(__this /* static, unused */, method)
// System.Void UnityEngine.UI.Text::set_fontSize(System.Int32)
extern "C"  void Text_set_fontSize_m3617617524 (Text_t1901882714 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C"  Color_t2555686324  Color_get_black_m719512684 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Text::set_alignment(UnityEngine.TextAnchor)
extern "C"  void Text_set_alignment_m88714888 (Text_t1901882714 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ChatBubbleUI::ResetCanvasUIs()
extern "C"  void ChatBubbleUI_ResetCanvasUIs_m1740381972 (ChatBubbleUI_t1508846992 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ChatBubbleUI::PopChatUI()
extern "C"  void ChatBubbleUI_PopChatUI_m2587343892 (ChatBubbleUI_t1508846992 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C"  float Mathf_Max_m3146388979 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ChatBubbleUI::ResetCanvasUIs(UnityEngine.Vector2)
extern "C"  void ChatBubbleUI_ResetCanvasUIs_m1868350676 (ChatBubbleUI_t1508846992 * __this, Vector2_t2156229523  ___size0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ChatBubbleUI::AutoCanvasUIs()
extern "C"  void ChatBubbleUI_AutoCanvasUIs_m213075858 (ChatBubbleUI_t1508846992 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ChatBubbleUI::LimitCanvasUIsBasedOnSizeDelta()
extern "C"  void ChatBubbleUI_LimitCanvasUIsBasedOnSizeDelta_m526648713 (ChatBubbleUI_t1508846992 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.RectTransform>()
#define GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398(__this, method) ((  RectTransform_t3704657025 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m3353183577 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C"  void Transform_set_localPosition_m4128471975 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m372706562 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ChatBubbleUI::PopChat()
extern "C"  void ChatBubbleUI_PopChat_m2172760713 (ChatBubbleUI_t1508846992 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C"  bool Input_GetMouseButtonDown_m2081676745 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKeyDown(System.String)
extern "C"  bool Input_GetKeyDown_m2928138282 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Input::get_touchCount()
extern "C"  int32_t Input_get_touchCount_m3403849067 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C"  Vector2_t2156229523  RectTransform_get_sizeDelta_m2183112744 (RectTransform_t3704657025 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<System.String>::get_Count()
#define List_1_get_Count_m2276455407(__this, method) ((  int32_t (*) (List_1_t3319525431 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// System.Void ChatBubbleUI::EnableChatBubbleUI(System.Boolean)
extern "C"  void ChatBubbleUI_EnableChatBubbleUI_m3745119852 (ChatBubbleUI_t1508846992 * __this, bool ___enable0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32)
#define List_1_get_Item_m3346958548(__this, p0, method) ((  String_t* (*) (List_1_t3319525431 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<System.String>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m815285786(__this, p0, method) ((  void (*) (List_1_t3319525431 *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m2730968292_gshared)(__this, p0, method)
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m20417929 (Behaviour_t1437897464 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern "C"  void Object_DestroyImmediate_m3193525861 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m565254235 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::get_Length()
extern "C"  int32_t String_get_Length_m3847582255 (String_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.String>::Add(!0)
#define List_1_Add_m1685793073(__this, p0, method) ((  void (*) (List_1_t3319525431 *, String_t*, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// !!0 UnityEngine.Object::FindObjectOfType<UnityEngine.EventSystems.EventSystem>()
#define Object_FindObjectOfType_TisEventSystem_t1003666588_m1331729420(__this /* static, unused */, method) ((  EventSystem_t1003666588 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m2612646359_gshared)(__this /* static, unused */, method)
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1620074514 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::.ctor(System.String,System.Type[])
extern "C"  void GameObject__ctor_m1350607670 (GameObject_t1113636619 * __this, String_t* p0, TypeU5BU5D_t3940880105* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.EventSystems.StandaloneInputModule>()
#define GameObject_AddComponent_TisStandaloneInputModule_t2760469101_m1723984740(__this, method) ((  StandaloneInputModule_t2760469101 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Canvas>()
#define GameObject_AddComponent_TisCanvas_t3310196443_m284883347(__this, method) ((  Canvas_t3310196443 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.UI.GraphicRaycaster>()
#define GameObject_AddComponent_TisGraphicRaycaster_t2999697109_m1217468238(__this, method) ((  GraphicRaycaster_t2999697109 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// System.Void UnityEngine.Canvas::set_renderMode(UnityEngine.RenderMode)
extern "C"  void Canvas_set_renderMode_m1564704306 (Canvas_t3310196443 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern "C"  void Transform_set_parent_m786917804 (Transform_t3600365921 * __this, Transform_t3600365921 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t3722313464  Vector3_get_zero_m1409827619 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t2156229523  Vector2_op_Implicit_m4260192859 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m2943235014 (Color_t2555686324 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.UI.Button>()
#define GameObject_AddComponent_TisButton_t4055032469_m1106144321(__this, method) ((  Button_t4055032469 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// System.Void UnityEngine.UI.Selectable::set_targetGraphic(UnityEngine.UI.Graphic)
extern "C"  void Selectable_set_targetGraphic_m1003546643 (Selectable_t3250028441 * __this, Graphic_t1660335611 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::get_onClick()
extern "C"  ButtonClickedEvent_t48803504 * Button_get_onClick_m2332132945 (Button_t4055032469 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction__ctor_m772160306 (UnityAction_t3245792599 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEvent::AddListener(UnityEngine.Events.UnityAction)
extern "C"  void UnityEvent_AddListener_m2276267359 (UnityEvent_t2581268647 * __this, UnityAction_t3245792599 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_yellow()
extern "C"  Color_t2555686324  Color_get_yellow_m1287957903 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m4051431634 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_time()
extern "C"  float Time_get_time_m2907476221 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::CreatePrimitive(UnityEngine.PrimitiveType)
extern "C"  GameObject_t1113636619 * GameObject_CreatePrimitive_m2902598419 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m3387557959 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
extern "C"  GameObject_t1113636619 * GameObject_FindGameObjectWithTag_m2129039296 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Subtraction_m3073674971 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::get_magnitude()
extern "C"  float Vector3_get_magnitude_m27958459 (Vector3_t3722313464 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Addition_m779775034 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C"  Vector3_t3722313464  Vector3_get_up_m3584168373 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_op_Multiply_m3376773913 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_Lerp_m407887542 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean DoneCameraMovement::ViewingPosCheck(UnityEngine.Vector3)
extern "C"  bool DoneCameraMovement_ViewingPosCheck_m2483040884 (DoneCameraMovement_t1316814223 * __this, Vector3_t3722313464  ___checkPos0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DoneCameraMovement::SmoothLookAt()
extern "C"  void DoneCameraMovement_SmoothLookAt_m490920 (DoneCameraMovement_t1316814223 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single)
extern "C"  bool Physics_Raycast_m261647105 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, RaycastHit_t1056001966 * p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.RaycastHit::get_transform()
extern "C"  Transform_t3600365921 * RaycastHit_get_transform_m942054759 (RaycastHit_t1056001966 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t2301928331  Quaternion_LookRotation_m3197602968 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t2301928331  Transform_get_rotation_m3502953881 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Lerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t2301928331  Quaternion_Lerp_m1238806789 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  p0, Quaternion_t2301928331  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_rotation_m3524318132 (Transform_t3600365921 * __this, Quaternion_t2301928331  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.LayerMask::op_Implicit(UnityEngine.LayerMask)
extern "C"  int32_t LayerMask_op_Implicit_m3296792737 (RuntimeObject * __this /* static, unused */, LayerMask_t3493934918  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.LayerMask::NameToLayer(System.String)
extern "C"  int32_t LayerMask_NameToLayer_m2359665122 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.LayerMask UnityEngine.LayerMask::op_Implicit(System.Int32)
extern "C"  LayerMask_t3493934918  LayerMask_op_Implicit_m90232283 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Quaternion::Angle(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  float Quaternion_Angle_m1586774072 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  p0, Quaternion_t2301928331  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Slerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t2301928331  Quaternion_Slerp_m1234055455 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  p0, Quaternion_t2301928331  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyPatrol::PatrolRaycast()
extern "C"  void EnemyPatrol_PatrolRaycast_m262090287 (EnemyPatrol_t3346945880 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Translate(System.Single,System.Single,System.Single)
extern "C"  void Transform_Translate_m3762500149 (Transform_t3600365921 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t3722313464  Vector3_get_forward_m3100859705 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Quaternion_op_Multiply_m2607404835 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m4145022031 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, RaycastHit_t1056001966 * p2, float p3, int32_t p4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GameObject::CompareTag(System.String)
extern "C"  bool GameObject_CompareTag_m3144439756 (GameObject_t1113636619 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyPatrol::ChasePlayer(UnityEngine.GameObject)
extern "C"  void EnemyPatrol_ChasePlayer_m1683778077 (EnemyPatrol_t3346945880 * __this, GameObject_t1113636619 * ___player0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C"  Vector3_t3722313464  RaycastHit_get_point_m2236647085 (RaycastHit_t1056001966 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyPatrol::TurnAround(UnityEngine.Vector3)
extern "C"  void EnemyPatrol_TurnAround_m3579663584 (EnemyPatrol_t3346945880 * __this, Vector3_t3722313464  ___hitPoint0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion Tools::LookAtPlayerOnYAxis(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t2301928331  Tools_LookAtPlayerOnYAxis_m199382236 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___srcPos0, Vector3_t3722313464  ___targetPos1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_up()
extern "C"  Vector3_t3722313464  Transform_get_up_m3972993886 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
extern "C"  Quaternion_t2301928331  Quaternion_AngleAxis_m1767165696 (RuntimeObject * __this /* static, unused */, float p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t2301928331  Quaternion_op_Multiply_m1294064023 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  p0, Quaternion_t2301928331  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Quaternion::op_Equality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Quaternion_op_Equality_m1582314779 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  p0, Quaternion_t2301928331  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m4211327027 (Object_t631007953 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object,System.Object)
extern "C"  String_t* String_Concat_m1715369213 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m2850623458 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Count()
#define List_1_get_Count_m3787308655(__this, method) ((  int32_t (*) (List_1_t777473367 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Item(System.Int32)
#define List_1_get_Item_m3022113929(__this, p0, method) ((  Transform_t3600365921 * (*) (List_1_t777473367 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Clear()
#define List_1_Clear_m3082658015(__this, method) ((  void (*) (List_1_t777473367 *, const RuntimeMethod*))List_1_Clear_m3697625829_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Add(!0)
#define List_1_Add_m4073477735(__this, p0, method) ((  void (*) (List_1_t777473367 *, Transform_t3600365921 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Transform>::Remove(!0)
#define List_1_Remove_m4216972(__this, p0, method) ((  bool (*) (List_1_t777473367 *, Transform_t3600365921 *, const RuntimeMethod*))List_1_Remove_m1416767016_gshared)(__this, p0, method)
// UnityEngine.Transform GameCamera::get_target()
extern "C"  Transform_t3600365921 * GameCamera_get_target_m1194538123 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t3600365921 * Transform_get_parent_m835071599 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Tools::IsChild(UnityEngine.Transform,UnityEngine.Transform)
extern "C"  bool Tools_IsChild_m3091374981 (RuntimeObject * __this /* static, unused */, Transform_t3600365921 * ___parent0, Transform_t3600365921 * ___child1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::Normalize()
extern "C"  void Vector3_Normalize_m914904454 (Vector3_t3722313464 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::.ctor()
#define List_1__ctor_m2885667311(__this, method) ((  void (*) (List_1_t777473367 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void GameCamera::AddTarget(UnityEngine.Transform)
extern "C"  void GameCamera_AddTarget_m745210319 (RuntimeObject * __this /* static, unused */, Transform_t3600365921 * ___t0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void GameCamera::RemoveTarget(UnityEngine.Transform)
extern "C"  void GameCamera_RemoveTarget_m3068264324 (RuntimeObject * __this /* static, unused */, Transform_t3600365921 * ___t0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void GameCameraTarget::Activate(System.Boolean)
extern "C"  void GameCameraTarget_Activate_m1421886454 (GameCameraTarget_t3267842245 * __this, bool ___val0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::FindWithTag(System.String)
extern "C"  GameObject_t1113636619 * GameObject_FindWithTag_m981614592 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t2627027031_m2651633905(__this, method) ((  Renderer_t2627027031 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C"  Material_t340375123 * Renderer_get_material_m4171603682 (Renderer_t2627027031 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Material::get_color()
extern "C"  Color_t2555686324  Material_get_color_m3827673574 (Material_t340375123 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C"  float Mathf_Min_m1073399594 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Lerp_m1004423579 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::set_color(UnityEngine.Color)
extern "C"  void Material_set_color_m1794818007 (Material_t340375123 * __this, Color_t2555686324  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::Lerp(UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C"  Color_t2555686324  Color_Lerp_m973389909 (RuntimeObject * __this /* static, unused */, Color_t2555686324  p0, Color_t2555686324  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Renderer>()
#define GameObject_GetComponentsInChildren_TisRenderer_t2627027031_m2275554434(__this, method) ((  RendererU5BU5D_t3210418286* (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponentsInChildren_TisRuntimeObject_m1982918030_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<Highlightable>()
#define Component_GetComponent_TisHighlightable_t3139352672_m3233271036(__this, method) ((  Highlightable_t3139352672 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Highlightable>()
#define GameObject_AddComponent_TisHighlightable_t3139352672_m2047509732(__this, method) ((  Highlightable_t3139352672 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// System.Void Highlightable::SetHighlight(System.Boolean)
extern "C"  void Highlightable_SetHighlight_m1780462549 (Highlightable_t3139352672 * __this, bool ___highlight0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m2614021312 (Rect_t2360479859 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C"  Color_t2555686324  Color_get_white_m332174077 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator HUDFPS::FPS()
extern "C"  RuntimeObject* HUDFPS_FPS_m1656083209 (HUDFPS_t4241874542 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t3829159415 * MonoBehaviour_StartCoroutine_m3411253000 (MonoBehaviour_t3962482529 * __this, RuntimeObject* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_timeScale()
extern "C"  float Time_get_timeScale_m701790074 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void HUDFPS/<FPS>c__Iterator0::.ctor()
extern "C"  void U3CFPSU3Ec__Iterator0__ctor_m594114655 (U3CFPSU3Ec__Iterator0_t392322613 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUISkin UnityEngine.GUI::get_skin()
extern "C"  GUISkin_t1244372282 * GUI_get_skin_m1874615010 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_label()
extern "C"  GUIStyle_t3956901511 * GUISkin_get_label_m1693050720 (GUISkin_t1244372282 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyle::.ctor(UnityEngine.GUIStyle)
extern "C"  void GUIStyle__ctor_m2912682974 (GUIStyle_t3956901511 * __this, GUIStyle_t3956901511 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIStyleState UnityEngine.GUIStyle::get_normal()
extern "C"  GUIStyleState_t1397964415 * GUIStyle_get_normal_m729441812 (GUIStyle_t3956901511 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyleState::set_textColor(UnityEngine.Color)
extern "C"  void GUIStyleState_set_textColor_m1105876047 (GUIStyleState_t1397964415 * __this, Color_t2555686324  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyle::set_alignment(UnityEngine.TextAnchor)
extern "C"  void GUIStyle_set_alignment_m3944619660 (GUIStyle_t3956901511 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_color(UnityEngine.Color)
extern "C"  void GUI_set_color_m1028198571 (RuntimeObject * __this /* static, unused */, Color_t2555686324  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI/WindowFunction::.ctor(System.Object,System.IntPtr)
extern "C"  void WindowFunction__ctor_m2544237635 (WindowFunction_t3146511083 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUI::Window(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,System.String)
extern "C"  Rect_t2360479859  GUI_Window_m1088326791 (RuntimeObject * __this /* static, unused */, int32_t p0, Rect_t2360479859  p1, WindowFunction_t3146511083 * p2, String_t* p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,System.String,UnityEngine.GUIStyle)
extern "C"  void GUI_Label_m2420537077 (RuntimeObject * __this /* static, unused */, Rect_t2360479859  p0, String_t* p1, GUIStyle_t3956901511 * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m345039817 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m1623532518 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::DragWindow(UnityEngine.Rect)
extern "C"  void GUI_DragWindow_m3406543436 (RuntimeObject * __this /* static, unused */, Rect_t2360479859  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::Clamp(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t Mathf_Clamp_m2756574208 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m904156431 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Single::ToString(System.String)
extern "C"  String_t* Single_ToString_m3489843083 (float* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_green()
extern "C"  Color_t2555686324  Color_get_green_m490390750 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_red()
extern "C"  Color_t2555686324  Color_get_red_m3227813939 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C"  void WaitForSeconds__ctor_m2199082655 (WaitForSeconds_t1699091251 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LevelExp::.ctor(System.UInt32,System.UInt32)
extern "C"  void LevelExp__ctor_m598325136 (LevelExp_t2602383666 * __this, uint32_t ___level0, uint32_t ___exp1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Entity::.ctor()
extern "C"  void Entity__ctor_m643651842 (Entity_t3391956725 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern "C"  Quaternion_t2301928331  Transform_get_localRotation_m3487911431 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Camera>()
#define GameObject_GetComponent_TisCamera_t4157153871_m3956151066(__this, method) ((  Camera_t4157153871 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Boolean UnityEngine.Input::GetKey(UnityEngine.KeyCode)
extern "C"  bool Input_GetKey_m3736388334 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t4157153871_m1557787507(__this, method) ((  Camera_t4157153871 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.String UnityEngine.GameObject::get_tag()
extern "C"  String_t* GameObject_get_tag_m3951609671 (GameObject_t1113636619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m920492651 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerShipBehavior::InteractiveWithPort(UnityEngine.GameObject)
extern "C"  void PlayerShipBehavior_InteractiveWithPort_m361013814 (PlayerShipBehavior_t729873910 * __this, GameObject_t1113636619 * ___port0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerShipBehavior::InteractiveWithNPC(UnityEngine.GameObject)
extern "C"  void PlayerShipBehavior_InteractiveWithNPC_m2391546926 (PlayerShipBehavior_t729873910 * __this, GameObject_t1113636619 * ___npc0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerShipBehavior::FinishInteractiveWithPort(UnityEngine.GameObject)
extern "C"  void PlayerShipBehavior_FinishInteractiveWithPort_m354842222 (PlayerShipBehavior_t729873910 * __this, GameObject_t1113636619 * ___port0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerShipBehavior::FinishInteractiveWithNPC(UnityEngine.GameObject)
extern "C"  void PlayerShipBehavior_FinishInteractiveWithNPC_m3927383204 (PlayerShipBehavior_t729873910 * __this, GameObject_t1113636619 * ___npc0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Highlightable::SetAllChildrenHighlightable(System.Boolean)
extern "C"  void Highlightable_SetAllChildrenHighlightable_m544031526 (Highlightable_t3139352672 * __this, bool ___highlight0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerShipBehavior::HighlightCollider(UnityEngine.GameObject,System.Boolean)
extern "C"  void PlayerShipBehavior_HighlightCollider_m195144836 (PlayerShipBehavior_t729873910 * __this, GameObject_t1113636619 * ___colliderGO0, bool ___highlight1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void GameObject_SendMessage_m3720186693 (GameObject_t1113636619 * __this, String_t* p0, RuntimeObject * p1, int32_t p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,System.String)
extern "C"  bool GUI_Button_m1518979886 (RuntimeObject * __this /* static, unused */, Rect_t2360479859  p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Terrain>()
#define GameObject_GetComponent_TisTerrain_t3055443660_m338788394(__this, method) ((  Terrain_t3055443660 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void RandomTerrainGenerator::CreateTerrainTextureWithHeight(UnityEngine.Terrain,System.Single)
extern "C"  void RandomTerrainGenerator_CreateTerrainTextureWithHeight_m3848507478 (RandomTerrainGenerator_t662438485 * __this, Terrain_t3055443660 * ___t0, float ___tileSize1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void RandomTerrainGenerator::GenerateTerrain(UnityEngine.Terrain,System.Single)
extern "C"  void RandomTerrainGenerator_GenerateTerrain_m199714022 (RandomTerrainGenerator_t662438485 * __this, Terrain_t3055443660 * ___t0, float ___tileSize1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TerrainData UnityEngine.Terrain::get_terrainData()
extern "C"  TerrainData_t657004131 * Terrain_get_terrainData_m2711583617 (Terrain_t3055443660 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.TerrainData::get_heightmapWidth()
extern "C"  int32_t TerrainData_get_heightmapWidth_m3057970466 (TerrainData_t657004131 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.TerrainData::get_heightmapHeight()
extern "C"  int32_t TerrainData_get_heightmapHeight_m3110588835 (TerrainData_t657004131 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::PerlinNoise(System.Single,System.Single)
extern "C"  float Mathf_PerlinNoise_m2335393878 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object[])
extern "C"  String_t* String_Concat_m2971454694 (RuntimeObject * __this /* static, unused */, ObjectU5BU5D_t2843939325* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m3752629331 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TerrainData::SetHeights(System.Int32,System.Int32,System.Single[0...,0...])
extern "C"  void TerrainData_SetHeights_m690219761 (TerrainData_t657004131 * __this, int32_t p0, int32_t p1, SingleU5B0___U2C0___U5D_t1444911252* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Terrain::set_detailObjectDensity(System.Single)
extern "C"  void Terrain_set_detailObjectDensity_m3204163592 (Terrain_t3055443660 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.TerrainData::get_alphamapWidth()
extern "C"  int32_t TerrainData_get_alphamapWidth_m315223358 (TerrainData_t657004131 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.TerrainData::get_alphamapHeight()
extern "C"  int32_t TerrainData_get_alphamapHeight_m595805513 (TerrainData_t657004131 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single[0...,0...,0...] UnityEngine.TerrainData::GetAlphamaps(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  SingleU5B0___U2C0___U2C0___U5D_t1444911253* TerrainData_GetAlphamaps_m3939888263 (TerrainData_t657004131 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TerrainData::SetAlphamaps(System.Int32,System.Int32,System.Single[0...,0...,0...])
extern "C"  void TerrainData_SetAlphamaps_m75475119 (TerrainData_t657004131 * __this, int32_t p0, int32_t p1, SingleU5B0___U2C0___U2C0___U5D_t1444911253* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t2670269651_m980647750(__this, method) ((  Image_t2670269651 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// UnityEngine.Sprite UnityEngine.UI.Image::get_sprite()
extern "C"  Sprite_t280657092 * Image_get_sprite_m1811690853 (Image_t2670269651 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToLocalPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector2&)
extern "C"  bool RectTransformUtility_ScreenPointToLocalPointInRectangle_m2327269187 (RuntimeObject * __this /* static, unused */, RectTransform_t3704657025 * p0, Vector2_t2156229523  p1, Camera_t4157153871 * p2, Vector2_t2156229523 * p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
extern "C"  Vector2_t2156229523  RectTransform_get_pivot_m3425744470 (RectTransform_t3704657025 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C"  Rect_t2360479859  RectTransform_get_rect_m574169965 (RectTransform_t3704657025 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.Sprite::get_textureRect()
extern "C"  Rect_t2360479859  Sprite_get_textureRect_m3217515846 (Sprite_t280657092 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Image/Type UnityEngine.UI.Image::get_type()
extern "C"  int32_t Image_get_type_m3606908055 (Image_t2670269651 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Sprite::get_border()
extern "C"  Vector4_t3319028937  Sprite_get_border_m2985609076 (Sprite_t280657092 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_x()
extern "C"  float Rect_get_x_m3839990490 (Rect_t2360479859 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_y()
extern "C"  float Rect_get_y_m1501338330 (Rect_t2360479859 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UnityEngine.Sprite::get_texture()
extern "C"  Texture2D_t3840446185 * Sprite_get_texture_m3976398399 (Sprite_t280657092 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Texture2D::GetPixel(System.Int32,System.Int32)
extern "C"  Color_t2555686324  Texture2D_GetPixel_m1195410881 (Texture2D_t3840446185 * __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void RotateTowardCamera::OnRotateTowardCamera(UnityEngine.Camera)
extern "C"  void RotateTowardCamera_OnRotateTowardCamera_m1367757239 (RotateTowardCamera_t118585142 * __this, Camera_t4157153871 * ___camera0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern "C"  Vector3_t3722313464  Transform_get_forward_m747522392 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<HUDFPS>()
#define GameObject_AddComponent_TisHUDFPS_t4241874542_m817494477(__this, method) ((  HUDFPS_t4241874542 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// System.Void SceneManager::InitScene()
extern "C"  void SceneManager_InitScene_m1840825294 (SceneManager_t385596982 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void GameScene::.ctor()
extern "C"  void GameScene__ctor_m3327038984 (GameScene_t4110668666 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C"  float Random_Range_m2202990745 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
extern "C"  Quaternion_t2301928331  Quaternion_Euler_m1803555822 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_localRotation_m19445462 (Transform_t3600365921 * __this, Quaternion_t2301928331  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
extern "C"  float AnimationCurve_Evaluate_m2125563588 (AnimationCurve_t3046754366 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C"  Quaternion_t2301928331  Quaternion_Euler_m3049309462 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_back()
extern "C"  Vector3_t3722313464  Vector3_get_back_m4077847766 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// T Entity::Find<ShipUnit>(UnityEngine.GameObject)
#define Entity_Find_TisShipUnit_t4065899274_m28943516(__this /* static, unused */, ___go0, method) ((  ShipUnit_t4065899274 * (*) (RuntimeObject * /* static, unused */, GameObject_t1113636619 *, const RuntimeMethod*))Entity_Find_TisRuntimeObject_m310867926_gshared)(__this /* static, unused */, ___go0, method)
// UnityEngine.Vector3 UnityEngine.Vector3::get_down()
extern "C"  Vector3_t3722313464  Vector3_get_down_m3781355428 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m234523501 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, float p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
extern "C"  bool Input_GetMouseButton_m513753021 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Application::get_isEditor()
extern "C"  bool Application_get_isEditor_m857789090 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetMouseButtonUp(System.Int32)
extern "C"  bool Input_GetMouseButtonUp_m2924350851 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cursor::set_visible(System.Boolean)
extern "C"  void Cursor_set_visible_m2693238713 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Input::GetAxis(System.String)
extern "C"  float Input_GetAxis_m4009438427 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::get_sqrMagnitude()
extern "C"  float Vector2_get_sqrMagnitude_m837837635 (Vector2_t2156229523 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Tools::WrapAngle(System.Single)
extern "C"  float Tools_WrapAngle_m228518310 (RuntimeObject * __this /* static, unused */, float ___a0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m3350697880 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::get_magnitude()
extern "C"  float Vector2_get_magnitude_m2752892833 (Vector2_t2156229523 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C"  float Mathf_Clamp01_m56433566 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ShipController::.ctor()
extern "C"  void ShipController__ctor_m2344806045 (ShipController_t2808759107 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Tools::GetTouchPositionInWater(UnityEngine.Vector3&)
extern "C"  bool Tools_GetTouchPositionInWater_m548322082 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464 * ___touchPosInGame0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ShipTouchToDestination::ShipMovement()
extern "C"  void ShipTouchToDestination_ShipMovement_m2927501072 (ShipTouchToDestination_t4292119180 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Distance_m886789632 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single ShipUnit::get_turningSpeed()
extern "C"  float ShipUnit_get_turningSpeed_m2858136543 (ShipUnit_t4065899274 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::RotateTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern "C"  Vector3_t3722313464  Vector3_RotateTowards_m4112951104 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3)
extern "C"  Quaternion_t2301928331  Quaternion_LookRotation_m4040767668 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean ShipController::IsShallowWater()
extern "C"  bool ShipController_IsShallowWater_m967017931 (ShipController_t2808759107 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single ShipUnit::get_movementSpeed()
extern "C"  float ShipUnit_get_movementSpeed_m3177165377 (ShipUnit_t4065899274 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::MoveTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_MoveTowards_m2786395547 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
extern "C"  bool Input_GetKeyDown_m17791917 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ShipTouchToMove::ShipMovement(System.Single,System.Single)
extern "C"  void ShipTouchToMove_ShipMovement_m2549588759 (ShipTouchToMove_t1387549923 * __this, float ___inputX0, float ___inputY1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Quaternion::ToString()
extern "C"  String_t* Quaternion_ToString_m2203056442 (Quaternion_t2301928331 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C"  Vector3_t3722313464  Transform_get_localPosition_m4234289348 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Vector3::ToString()
extern "C"  String_t* Vector3_ToString_m759076600 (Vector3_t3722313464 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SellDefine::.ctor(System.Single,System.Boolean)
extern "C"  void SellDefine__ctor_m822927741 (SellDefine_t4148876514 * __this, float ___soldValue0, bool ___canBeSold1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ShipUnit::OnStart()
extern "C"  void ShipUnit_OnStart_m1772374397 (ShipUnit_t4065899274 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t4157153871 * Camera_get_main_m3643453163 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m286683560 (Color_t2555686324 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Camera::WorldToScreenPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Camera_WorldToScreenPoint_m3726311023 (Camera_t4157153871 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::.ctor(System.String)
extern "C"  void GUIContent__ctor_m890195579 (GUIContent_t3050628031 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.GUIStyle::CalcSize(UnityEngine.GUIContent)
extern "C"  Vector2_t2156229523  GUIStyle_CalcSize_m1046812636 (GUIStyle_t3956901511 * __this, GUIContent_t3050628031 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,System.String)
extern "C"  void GUI_Label_m2454565404 (RuntimeObject * __this /* static, unused */, Rect_t2360479859  p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<SkillAbility/SkillEnum,SkillAbility/SkillValue>::.ctor()
#define Dictionary_2__ctor_m3552178049(__this, method) ((  void (*) (Dictionary_2_t437934597 *, const RuntimeMethod*))Dictionary_2__ctor_m2768249451_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SkillAbility/SkillEnum,SkillAbility/SkillValue>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m969889043(__this, p0, method) ((  bool (*) (Dictionary_2_t437934597 *, int32_t, const RuntimeMethod*))Dictionary_2_ContainsKey_m3302013818_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<SkillAbility/SkillEnum,SkillAbility/SkillValue>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m162486917(__this, p0, p1, method) ((  void (*) (Dictionary_2_t437934597 *, int32_t, SkillValue_t3350472883 *, const RuntimeMethod*))Dictionary_2_set_Item_m2781824001_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.Dictionary`2<SkillAbility/SkillEnum,SkillAbility/SkillValue>::Add(!0,!1)
#define Dictionary_2_Add_m1751118572(__this, p0, p1, method) ((  void (*) (Dictionary_2_t437934597 *, int32_t, SkillValue_t3350472883 *, const RuntimeMethod*))Dictionary_2_Add_m2931076137_gshared)(__this, p0, p1, method)
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<SkillAbility/SkillEnum,SkillAbility/SkillValue>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m1696204037(__this, method) ((  Enumerator_t2392117372  (*) (Dictionary_2_t437934597 *, const RuntimeMethod*))Dictionary_2_GetEnumerator_m2019897888_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<SkillAbility/SkillEnum,SkillAbility/SkillValue>::get_Current()
#define Enumerator_get_Current_m2625386591(__this, method) ((  KeyValuePair_2_t2835606764  (*) (Enumerator_t2392117372 *, const RuntimeMethod*))Enumerator_get_Current_m2680200070_gshared)(__this, method)
// !1 System.Collections.Generic.KeyValuePair`2<SkillAbility/SkillEnum,SkillAbility/SkillValue>::get_Value()
#define KeyValuePair_2_get_Value_m1855475308(__this, method) ((  SkillValue_t3350472883 * (*) (KeyValuePair_2_t2835606764 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m2847946307_gshared)(__this, method)
// System.Void SkillAbility/SkillAbilityCallBack::Invoke()
extern "C"  void SkillAbilityCallBack_Invoke_m4222624785 (SkillAbilityCallBack_t2618462130 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<SkillAbility/SkillEnum,SkillAbility/SkillValue>::MoveNext()
#define Enumerator_MoveNext_m2515343744(__this, method) ((  bool (*) (Enumerator_t2392117372 *, const RuntimeMethod*))Enumerator_MoveNext_m917637076_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<SkillAbility/SkillEnum,SkillAbility/SkillValue>::Dispose()
#define Enumerator_Dispose_m3555283856(__this, method) ((  void (*) (Enumerator_t2392117372 *, const RuntimeMethod*))Enumerator_Dispose_m1149657600_gshared)(__this, method)
// !1 System.Collections.Generic.Dictionary`2<SkillAbility/SkillEnum,SkillAbility/SkillValue>::get_Item(!0)
#define Dictionary_2_get_Item_m2907616642(__this, p0, method) ((  SkillValue_t3350472883 * (*) (Dictionary_2_t437934597 *, int32_t, const RuntimeMethod*))Dictionary_2_get_Item_m3281886030_gshared)(__this, p0, method)
// System.Void SkillAbility/SkillValue::OnCallback(System.Single)
extern "C"  void SkillValue_OnCallback_m1414099155 (SkillValue_t3350472883 * __this, float ___data0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Delegate::get_Method()
extern "C"  MethodInfo_t * Delegate_get_Method_m3071622864 (Delegate_t1188392813 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform uGuiLayout::BeginSubControl()
extern "C"  RectTransform_t3704657025 * uGuiLayout_BeginSubControl_m130035606 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Canvas>()
#define Component_GetComponent_TisCanvas_t3310196443_m782269232(__this, method) ((  Canvas_t3310196443 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void uGuiLayout::BeginVertical()
extern "C"  void uGuiLayout_BeginVertical_m2791822590 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Text uGuiLayout::Label(System.String,System.String)
extern "C"  Text_t1901882714 * uGuiLayout_Label_m3379332580 (RuntimeObject * __this /* static, unused */, String_t* ___text0, String_t* ___name1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void uGuiLayout::EndVertical()
extern "C"  void uGuiLayout_EndVertical_m228777174 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void uGuiLayout::EndSubControl()
extern "C"  void uGuiLayout_EndSubControl_m47764685 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t3916780224_m279685075(__this, method) ((  Rigidbody_t3916780224 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::.ctor()
#define List_1__ctor_m1592361844(__this, method) ((  void (*) (List_1_t1093887670 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// UnityEngine.Rigidbody Tools::GetRigidbody(UnityEngine.Transform)
extern "C"  Rigidbody_t3916780224 * Tools_GetRigidbody_m2837624754 (RuntimeObject * __this /* static, unused */, Transform_t3600365921 * ___trans0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody>::Contains(!0)
#define List_1_Contains_m2602505829(__this, p0, method) ((  bool (*) (List_1_t1093887670 *, Rigidbody_t3916780224 *, const RuntimeMethod*))List_1_Contains_m2654125393_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody>::Add(!0)
#define List_1_Add_m3007157130(__this, p0, method) ((  void (*) (List_1_t1093887670 *, Rigidbody_t3916780224 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider>()
#define Component_GetComponent_TisCollider_t1773347010_m4226749020(__this, method) ((  Collider_t1773347010 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// UnityEngine.Bounds UnityEngine.Collider::get_bounds()
extern "C"  Bounds_t2266837910  Collider_get_bounds_m2952418672 (Collider_t1773347010 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Bounds__ctor_m1937678907 (Bounds_t2266837910 * __this, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Collider>()
#define Component_GetComponentsInChildren_TisCollider_t1773347010_m2157248342(__this, method) ((  ColliderU5BU5D_t4234922487* (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m1308288322_gshared)(__this, method)
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Bounds)
extern "C"  void Bounds_Encapsulate_m1263362003 (Bounds_t2266837910 * __this, Bounds_t2266837910  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds Tools::CalculateWorldBounds(UnityEngine.Transform)
extern "C"  Bounds_t2266837910  Tools_CalculateWorldBounds_m2350520555 (RuntimeObject * __this /* static, unused */, Transform_t3600365921 * ___transform0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_min()
extern "C"  Vector3_t3722313464  Bounds_get_min_m3755135869 (Bounds_t2266837910 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_max()
extern "C"  Vector3_t3722313464  Bounds_get_max_m3756577669 (Bounds_t2266837910 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Dot_m606404487 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
extern "C"  int32_t Mathf_RoundToInt_m1874334613 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Tools::EncodeFloatToInt(System.Single)
extern "C"  int32_t Tools_EncodeFloatToInt_m1804285472 (RuntimeObject * __this /* static, unused */, float ___val0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m3755062657 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::StartsWith(System.String,System.StringComparison)
extern "C"  bool String_StartsWith_m2640722675 (String_t* __this, String_t* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Application::get_dataPath()
extern "C"  String_t* Application_get_dataPath_m4232621142 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::EndsWith(System.String)
extern "C"  bool String_EndsWith_m1901926500 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Replace(System.String,System.String)
extern "C"  String_t* String_Replace_m1273907647 (String_t* __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOf(System.String)
extern "C"  int32_t String_IndexOf_m1977622757 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Remove(System.Int32)
extern "C"  String_t* String_Remove_m1524948975 (String_t* __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32)
extern "C"  String_t* String_Substring_m2848979100 (String_t* __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::LastIndexOf(System.Char)
extern "C"  int32_t String_LastIndexOf_m3451222878 (String_t* __this, Il2CppChar p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Sign(System.Single)
extern "C"  float Mathf_Sign_m3457838305 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t1417781964* Object_FindObjectsOfType_m2295101757 (RuntimeObject * __this /* static, unused */, Type_t * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SendMessage(System.String,UnityEngine.SendMessageOptions)
extern "C"  void GameObject_SendMessage_m1121218340 (GameObject_t1113636619 * __this, String_t* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C"  Touch_t1921856868  Input_GetTouch_m2192712756 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C"  int32_t Touch_get_phase_m214549210 (Touch_t1921856868 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C"  Vector2_t2156229523  Touch_get_position_m3109777936 (Touch_t1921856868 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C"  Vector3_t3722313464  Input_get_mousePosition_m1616496925 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t3722313464  Vector2_op_Implicit_m1860157806 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C"  Ray_t3785851493  Camera_ScreenPointToRay_m3764635188 (Camera_t4157153871 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m1893809531 (RuntimeObject * __this /* static, unused */, Ray_t3785851493  p0, RaycastHit_t1056001966 * p1, float p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Tools::GetTouchPositionInGame(System.Int32,UnityEngine.Vector3&)
extern "C"  bool Tools_GetTouchPositionInGame_m4201674753 (RuntimeObject * __this /* static, unused */, int32_t ___layerMask0, Vector3_t3722313464 * ___touchPosInGame1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Angle(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Angle_m3731191531 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Cross(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_Cross_m418170344 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern "C"  Vector3_t3722313464  Quaternion_get_eulerAngles_m3425202016 (Quaternion_t2301928331 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Stack`1<System.Single>::Push(!0)
#define Stack_1_Push_m2927642228(__this, p0, method) ((  void (*) (Stack_1_t2240656229 *, float, const RuntimeMethod*))Stack_1_Push_m2927642228_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Stack`1<uGuiLayout/Mode>::Push(!0)
#define Stack_1_Push_m3148378454(__this, p0, method) ((  void (*) (Stack_1_t4010345780 *, int32_t, const RuntimeMethod*))Stack_1_Push_m3148378454_gshared)(__this, p0, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.UI.HorizontalLayoutGroup>()
#define GameObject_AddComponent_TisHorizontalLayoutGroup_t2586782146_m3608050281(__this, method) ((  HorizontalLayoutGroup_t2586782146 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// !0 System.Collections.Generic.Stack`1<System.Single>::Pop()
#define Stack_1_Pop_m3170417723(__this, method) ((  float (*) (Stack_1_t2240656229 *, const RuntimeMethod*))Stack_1_Pop_m3170417723_gshared)(__this, method)
// !0 System.Collections.Generic.Stack`1<uGuiLayout/Mode>::Pop()
#define Stack_1_Pop_m2243551095(__this, method) ((  int32_t (*) (Stack_1_t4010345780 *, const RuntimeMethod*))Stack_1_Pop_m2243551095_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.UI.VerticalLayoutGroup>()
#define GameObject_AddComponent_TisVerticalLayoutGroup_t923838031_m1306033316(__this, method) ((  VerticalLayoutGroup_t923838031 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// System.Void UnityEngine.GameObject::.ctor()
extern "C"  void GameObject__ctor_m3707688467 (GameObject_t1113636619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.RectTransform>()
#define GameObject_AddComponent_TisRectTransform_t3704657025_m3669823029(__this, method) ((  RectTransform_t3704657025 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Stack`1<UnityEngine.RectTransform>::get_Count()
#define Stack_1_get_Count_m545179475(__this, method) ((  int32_t (*) (Stack_1_t253079184 *, const RuntimeMethod*))Stack_1_get_Count_m1599740434_gshared)(__this, method)
// !0 System.Collections.Generic.Stack`1<UnityEngine.RectTransform>::Peek()
#define Stack_1_Peek_m855668811(__this, method) ((  RectTransform_t3704657025 * (*) (Stack_1_t253079184 *, const RuntimeMethod*))Stack_1_Peek_m326384648_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<UnityEngine.RectTransform>::Push(!0)
#define Stack_1_Push_m811146802(__this, p0, method) ((  void (*) (Stack_1_t253079184 *, RectTransform_t3704657025 *, const RuntimeMethod*))Stack_1_Push_m1669856732_gshared)(__this, p0, method)
// System.Void UnityEngine.RectTransform::set_offsetMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_offsetMax_m2526664592 (RectTransform_t3704657025 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_offsetMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_offsetMin_m1512629941 (RectTransform_t3704657025 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
extern "C"  void RectTransform_set_pivot_m909387058 (RectTransform_t3704657025 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.Stack`1<UnityEngine.RectTransform>::Pop()
#define Stack_1_Pop_m1483992283(__this, method) ((  RectTransform_t3704657025 * (*) (Stack_1_t253079184 *, const RuntimeMethod*))Stack_1_Pop_m756553478_gshared)(__this, method)
// System.Void uGuiLayout::AddChild(UnityEngine.RectTransform)
extern "C"  void uGuiLayout_AddChild_m2495970376 (RuntimeObject * __this /* static, unused */, RectTransform_t3704657025 * ___trans0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Stack`1<UnityEngine.RectTransform>::.ctor()
#define Stack_1__ctor_m1535627942(__this, method) ((  void (*) (Stack_1_t253079184 *, const RuntimeMethod*))Stack_1__ctor_m3164958980_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Single>::.ctor()
#define Stack_1__ctor_m20774007(__this, method) ((  void (*) (Stack_1_t2240656229 *, const RuntimeMethod*))Stack_1__ctor_m20774007_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<uGuiLayout/Mode>::.ctor()
#define Stack_1__ctor_m2382698871(__this, method) ((  void (*) (Stack_1_t4010345780 *, const RuntimeMethod*))Stack_1__ctor_m2382698871_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
#define Component_GetComponent_TisRectTransform_t3704657025_m3396022872(__this, method) ((  RectTransform_t3704657025 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::get_delta()
extern "C"  Vector2_t2156229523  PointerEventData_get_delta_m1062010255 (PointerEventData_t3807901092 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
extern "C"  void Vector3__ctor_m1719387948 (Vector3_t3722313464 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Hashtable::.ctor()
extern "C"  void Hashtable__ctor_m1815022027 (Hashtable_t1853889766 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C"  bool Behaviour_get_enabled_m753527255 (Behaviour_t1437897464 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.Renderer::get_sharedMaterial()
extern "C"  Material_t340375123 * Renderer_get_sharedMaterial_m1936632411 (Renderer_t2627027031 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Renderer::get_enabled()
extern "C"  bool Renderer_get_enabled_m3482452518 (Renderer_t2627027031 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_current()
extern "C"  Camera_t4157153871 * Camera_get_current_m929992396 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Water/WaterMode Water::FindHardwareWaterSupport()
extern "C"  int32_t Water_FindHardwareWaterSupport_m1195528645 (Water_t1083516957 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Water/WaterMode Water::GetWaterMode()
extern "C"  int32_t Water_GetWaterMode_m3334219937 (Water_t1083516957 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Water::CreateWaterObjects(UnityEngine.Camera,UnityEngine.Camera&,UnityEngine.Camera&)
extern "C"  void Water_CreateWaterObjects_m3692736303 (Water_t1083516957 * __this, Camera_t4157153871 * ___currentCamera0, Camera_t4157153871 ** ___reflectionCamera1, Camera_t4157153871 ** ___refractionCamera2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.QualitySettings::get_pixelLightCount()
extern "C"  int32_t QualitySettings_get_pixelLightCount_m3013306133 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_pixelLightCount(System.Int32)
extern "C"  void QualitySettings_set_pixelLightCount_m3523654033 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Water::UpdateCameraModes(UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void Water_UpdateCameraModes_m552426476 (Water_t1083516957 * __this, Camera_t4157153871 * ___src0, Camera_t4157153871 * ___dest1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.QualitySettings::get_shadowDistance()
extern "C"  float QualitySettings_get_shadowDistance_m2189244662 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.QualitySettings::set_shadowDistance(System.Single)
extern "C"  void QualitySettings_set_shadowDistance_m3878605578 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m2498754347 (Vector4_t3319028937 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_zero()
extern "C"  Matrix4x4_t1817901843  Matrix4x4_get_zero_m2898777066 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Water::CalculateReflectionMatrix(UnityEngine.Matrix4x4&,UnityEngine.Vector4)
extern "C"  void Water_CalculateReflectionMatrix_m1781634015 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843 * ___reflectionMat0, Vector4_t3319028937  ___plane1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Matrix4x4_MultiplyPoint_m1575665487 (Matrix4x4_t1817901843 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_worldToCameraMatrix()
extern "C"  Matrix4x4_t1817901843  Camera_get_worldToCameraMatrix_m22661425 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern "C"  Matrix4x4_t1817901843  Matrix4x4_op_Multiply_m1876492807 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  p0, Matrix4x4_t1817901843  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_worldToCameraMatrix(UnityEngine.Matrix4x4)
extern "C"  void Camera_set_worldToCameraMatrix_m2548466927 (Camera_t4157153871 * __this, Matrix4x4_t1817901843  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 Water::CameraSpacePlane(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector4_t3319028937  Water_CameraSpacePlane_m2359059317 (Water_t1083516957 * __this, Camera_t4157153871 * ___cam0, Vector3_t3722313464  ___pos1, Vector3_t3722313464  ___normal2, float ___sideSign3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_projectionMatrix()
extern "C"  Matrix4x4_t1817901843  Camera_get_projectionMatrix_m667780853 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Water::CalculateObliqueMatrix(UnityEngine.Matrix4x4&,UnityEngine.Vector4)
extern "C"  void Water_CalculateObliqueMatrix_m3061601437 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843 * ___projection0, Vector4_t3319028937  ___clipPlane1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_projectionMatrix(UnityEngine.Matrix4x4)
extern "C"  void Camera_set_projectionMatrix_m3293177686 (Camera_t4157153871 * __this, Matrix4x4_t1817901843  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.LayerMask::get_value()
extern "C"  int32_t LayerMask_get_value_m1881709263 (LayerMask_t3493934918 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_cullingMask(System.Int32)
extern "C"  void Camera_set_cullingMask_m1402455777 (Camera_t4157153871 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_targetTexture(UnityEngine.RenderTexture)
extern "C"  void Camera_set_targetTexture_m3148311140 (Camera_t4157153871 * __this, RenderTexture_t2108887433 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::SetRevertBackfacing(System.Boolean)
extern "C"  void GL_SetRevertBackfacing_m310228429 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
extern "C"  Vector3_t3722313464  Transform_get_eulerAngles_m2743581774 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_eulerAngles(UnityEngine.Vector3)
extern "C"  void Transform_set_eulerAngles_m135219616 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::Render()
extern "C"  void Camera_Render_m2813253190 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.Texture)
extern "C"  void Material_SetTexture_m1829349465 (Material_t340375123 * __this, String_t* p0, Texture_t3661962703 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::EnableKeyword(System.String)
extern "C"  void Shader_EnableKeyword_m3103559844 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Shader::DisableKeyword(System.String)
extern "C"  void Shader_DisableKeyword_m433641454 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.DictionaryEntry::get_Value()
extern "C"  RuntimeObject * DictionaryEntry_get_Value_m618120527 (DictionaryEntry_t3123975638 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Material::GetVector(System.String)
extern "C"  Vector4_t3319028937  Material_GetVector_m806950826 (Material_t340375123 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Material::GetFloat(System.String)
extern "C"  float Material_GetFloat_m2210875428 (Material_t340375123 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_timeSinceLevelLoad()
extern "C"  float Time_get_timeSinceLevelLoad_m2224611026 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::IEEERemainder(System.Double,System.Double)
extern "C"  double Math_IEEERemainder_m1747102664 (RuntimeObject * __this /* static, unused */, double p0, double p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetVector(System.String,UnityEngine.Vector4)
extern "C"  void Material_SetVector_m4214217286 (Material_t340375123 * __this, String_t* p0, Vector4_t3319028937  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds UnityEngine.Renderer::get_bounds()
extern "C"  Bounds_t2266837910  Renderer_get_bounds_m1803204000 (Renderer_t2627027031 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern "C"  Vector3_t3722313464  Bounds_get_size_m1178783246 (Bounds_t2266837910 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t2301928331  Quaternion_get_identity_m3722672781 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::TRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Matrix4x4_t1817901843  Matrix4x4_TRS_m3801934620 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Quaternion_t2301928331  p1, Vector3_t3722313464  p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetMatrix(System.String,UnityEngine.Matrix4x4)
extern "C"  void Material_SetMatrix_m4094650785 (Material_t340375123 * __this, String_t* p0, Matrix4x4_t1817901843  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Terrain UnityEngine.Terrain::get_activeTerrain()
extern "C"  Terrain_t3055443660 * Terrain_get_activeTerrain_m2378246603 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.TerrainData::get_size()
extern "C"  Vector3_t3722313464  TerrainData_get_size_m1871576403 (TerrainData_t657004131 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
extern "C"  int32_t Camera_get_clearFlags_m992534691 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_clearFlags(UnityEngine.CameraClearFlags)
extern "C"  void Camera_set_clearFlags_m2207032996 (Camera_t4157153871 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Camera::get_backgroundColor()
extern "C"  Color_t2555686324  Camera_get_backgroundColor_m3310993309 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_backgroundColor(UnityEngine.Color)
extern "C"  void Camera_set_backgroundColor_m1332346802 (Camera_t4157153871 * __this, Color_t2555686324  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
extern "C"  Component_t1923634451 * Component_GetComponent_m886226392 (Component_t1923634451 * __this, Type_t * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.Skybox::get_material()
extern "C"  Material_t340375123 * Skybox_get_material_m3789022787 (Skybox_t2662837510 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Skybox::set_material(UnityEngine.Material)
extern "C"  void Skybox_set_material_m3176166872 (Skybox_t2662837510 * __this, Material_t340375123 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_farClipPlane()
extern "C"  float Camera_get_farClipPlane_m538536689 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_farClipPlane(System.Single)
extern "C"  void Camera_set_farClipPlane_m3828313665 (Camera_t4157153871 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_nearClipPlane()
extern "C"  float Camera_get_nearClipPlane_m837839537 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_nearClipPlane(System.Single)
extern "C"  void Camera_set_nearClipPlane_m3667419702 (Camera_t4157153871 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Camera::get_orthographic()
extern "C"  bool Camera_get_orthographic_m2831464531 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_orthographic(System.Boolean)
extern "C"  void Camera_set_orthographic_m2855749523 (Camera_t4157153871 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_fieldOfView()
extern "C"  float Camera_get_fieldOfView_m1018585504 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_fieldOfView(System.Single)
extern "C"  void Camera_set_fieldOfView_m1438246590 (Camera_t4157153871 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_aspect()
extern "C"  float Camera_get_aspect_m862507514 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_aspect(System.Single)
extern "C"  void Camera_set_aspect_m2625464181 (Camera_t4157153871 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Camera::get_orthographicSize()
extern "C"  float Camera_get_orthographicSize_m3903216845 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_orthographicSize(System.Single)
extern "C"  void Camera_set_orthographicSize_m76971700 (Camera_t4157153871 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32)
extern "C"  void RenderTexture__ctor_m769234016 (RenderTexture_t2108887433 * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Object::GetInstanceID()
extern "C"  int32_t Object_GetInstanceID_m1255174761 (Object_t631007953 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::set_name(System.String)
extern "C"  void Object_set_name_m291480324 (Object_t631007953 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::set_isPowerOfTwo(System.Boolean)
extern "C"  void RenderTexture_set_isPowerOfTwo_m3873419893 (RenderTexture_t2108887433 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m1648752846 (Object_t631007953 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.FlareLayer>()
#define GameObject_AddComponent_TisFlareLayer_t1739223323_m1608860411(__this, method) ((  FlareLayer_t1739223323 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// System.Boolean UnityEngine.SystemInfo::get_supportsRenderTextures()
extern "C"  bool SystemInfo_get_supportsRenderTextures_m466309287 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Material::GetTag(System.String,System.Boolean)
extern "C"  String_t* Material_GetTag_m2091721776 (Material_t340375123 * __this, String_t* p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyVector(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Matrix4x4_MultiplyVector_m3808798942 (Matrix4x4_t1817901843 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C"  Vector3_t3722313464  Vector3_get_normalized_m2454957984 (Vector3_t3722313464 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_inverse()
extern "C"  Matrix4x4_t1817901843  Matrix4x4_get_inverse_m1870592360 (Matrix4x4_t1817901843 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Water::sgn(System.Single)
extern "C"  float Water_sgn_m123353488 (RuntimeObject * __this /* static, unused */, float ___a0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern "C"  Vector4_t3319028937  Matrix4x4_op_Multiply_m1839501195 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843  p0, Vector4_t3319028937  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::Dot(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  float Vector4_Dot_m3492158352 (RuntimeObject * __this /* static, unused */, Vector4_t3319028937  p0, Vector4_t3319028937  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Multiply(UnityEngine.Vector4,System.Single)
extern "C"  Vector4_t3319028937  Vector4_op_Multiply_m213790997 (RuntimeObject * __this /* static, unused */, Vector4_t3319028937  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32)
extern "C"  float Matrix4x4_get_Item_m567451091 (Matrix4x4_t1817901843 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Single)
extern "C"  void Matrix4x4_set_Item_m1906605342 (Matrix4x4_t1817901843 * __this, int32_t p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::get_Item(System.Int32)
extern "C"  float Vector4_get_Item_m2380866393 (Vector4_t3319028937 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void WorldCanvasUI::InitWorldCanvasUI()
extern "C"  void WorldCanvasUI_InitWorldCanvasUI_m724433920 (WorldCanvasUI_t1156772134 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponentInChildren<UnityEngine.Canvas>()
#define GameObject_GetComponentInChildren_TisCanvas_t3310196443_m2710541779(__this, method) ((  Canvas_t3310196443 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponentInChildren_TisRuntimeObject_m1513755678_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.GraphicRaycaster>()
#define GameObject_GetComponent_TisGraphicRaycaster_t2999697109_m3357594245(__this, method) ((  GraphicRaycaster_t2999697109 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Captain::.ctor()
extern "C"  void Captain__ctor_m1184877007 (Captain_t4252579700 * __this, const RuntimeMethod* method)
{
	{
		HumanUnit__ctor_m1823562469(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Captain::Start()
extern "C"  void Captain_Start_m906864657 (Captain_t4252579700 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Captain::Update()
extern "C"  void Captain_Update_m1104222398 (Captain_t4252579700 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Captain::InitSkills()
extern "C"  void Captain_InitSkills_m4099947469 (Captain_t4252579700 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Captain_InitSkills_m4099947469_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		V_0 = (0.1f);
		float L_0 = V_0;
		intptr_t L_1 = (intptr_t)Captain_ApplyLeadershipCallback_m1116414556_RuntimeMethod_var;
		SkillAbilityCallBack_t2618462130 * L_2 = (SkillAbilityCallBack_t2618462130 *)il2cpp_codegen_object_new(SkillAbilityCallBack_t2618462130_il2cpp_TypeInfo_var);
		SkillAbilityCallBack__ctor_m3872213263(L_2, __this, L_1, /*hidden argument*/NULL);
		SkillValue_t3350472883 * L_3 = (SkillValue_t3350472883 *)il2cpp_codegen_object_new(SkillValue_t3350472883_il2cpp_TypeInfo_var);
		SkillValue__ctor_m488509249(L_3, 1, L_0, L_2, /*hidden argument*/NULL);
		__this->set_mleadershipSkill_5(L_3);
		SkillAbility_t2516669324 * L_4 = ((HumanUnit_t2125463837 *)__this)->get_mSkillAbilities_4();
		SkillValue_t3350472883 * L_5 = __this->get_mleadershipSkill_5();
		SkillAbility_AddSkill_m539831979(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Captain::ApplyLeadershipCallback()
extern "C"  void Captain_ApplyLeadershipCallback_m1116414556 (Captain_t4252579700 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Captain_ApplyLeadershipCallback_m1116414556_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		SkillValue_t3350472883 * L_0 = __this->get_mleadershipSkill_5();
		float L_1 = L_0->get_callbackArg_3();
		SkillValue_t3350472883 * L_2 = __this->get_mleadershipSkill_5();
		float L_3 = L_2->get_value_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_4 = Mathf_FloorToInt_m1870542928(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)(((float)((float)L_1))), (float)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		LevelExp_t2602383666 * L_5 = ((HumanUnit_t2125463837 *)__this)->get_mLevelExp_3();
		LevelExp_t2602383666 * L_6 = L_5;
		uint32_t L_7 = L_6->get_mExp_1();
		int32_t L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2465617642_il2cpp_TypeInfo_var);
		uint32_t L_9 = Convert_ToUInt32_m2215525276(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		L_6->set_mExp_1(((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)L_9)));
		return;
	}
}
// System.Void Captain::EarnExp(System.UInt32)
extern "C"  void Captain_EarnExp_m2906668812 (Captain_t4252579700 * __this, uint32_t ___exp0, const RuntimeMethod* method)
{
	{
		LevelExp_t2602383666 * L_0 = ((HumanUnit_t2125463837 *)__this)->get_mLevelExp_3();
		LevelExp_t2602383666 * L_1 = L_0;
		uint32_t L_2 = L_1->get_mExp_1();
		uint32_t L_3 = ___exp0;
		L_1->set_mExp_1(((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)L_3)));
		SkillAbility_t2516669324 * L_4 = ((HumanUnit_t2125463837 *)__this)->get_mSkillAbilities_4();
		uint32_t L_5 = ___exp0;
		SkillAbility_ApplySkill_m1333928009(L_4, 1, (((float)((float)(((double)((uint32_t)L_5)))))), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ChatBubbleUI::.ctor()
extern "C"  void ChatBubbleUI__ctor_m1094881035 (ChatBubbleUI_t1508846992 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChatBubbleUI__ctor_m1094881035_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_0 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_sizeDelta_3(L_0);
		__this->set_autoCanvasUIs_4((bool)1);
		__this->set_fontSize_10(((int32_t)10));
		List_1_t3319525431 * L_1 = (List_1_t3319525431 *)il2cpp_codegen_object_new(List_1_t3319525431_il2cpp_TypeInfo_var);
		List_1__ctor_m706204246(L_1, /*hidden argument*/List_1__ctor_m706204246_RuntimeMethod_var);
		__this->set_chatStrList_12(L_1);
		__this->set_autoPop_14((bool)1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ChatBubbleUI::Awake()
extern "C"  void ChatBubbleUI_Awake_m3747317158 (ChatBubbleUI_t1508846992 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChatBubbleUI_Awake_m3747317158_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	GameObject_t1113636619 * V_1 = NULL;
	Rect_t2360479859  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Rect_t2360479859  V_3;
	memset(&V_3, 0, sizeof(V_3));
	GameObject_t1113636619 * V_4 = NULL;
	GameObject_t1113636619 * V_5 = NULL;
	ChatBubbleUI_t1508846992 * G_B15_0 = NULL;
	ChatBubbleUI_t1508846992 * G_B14_0 = NULL;
	int32_t G_B16_0 = 0;
	ChatBubbleUI_t1508846992 * G_B16_1 = NULL;
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		WorldCanvasUI_t1156772134 * L_1 = GameObject_GetComponent_TisWorldCanvasUI_t1156772134_m2038315626(L_0, /*hidden argument*/GameObject_GetComponent_TisWorldCanvasUI_t1156772134_m2038315626_RuntimeMethod_var);
		__this->set_worldCanvasUI_2(L_1);
		WorldCanvasUI_t1156772134 * L_2 = __this->get_worldCanvasUI_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0033;
		}
	}
	{
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		WorldCanvasUI_t1156772134 * L_5 = GameObject_AddComponent_TisWorldCanvasUI_t1156772134_m3564025873(L_4, /*hidden argument*/GameObject_AddComponent_TisWorldCanvasUI_t1156772134_m3564025873_RuntimeMethod_var);
		__this->set_worldCanvasUI_2(L_5);
	}

IL_0033:
	{
		WorldCanvasUI_t1156772134 * L_6 = __this->get_worldCanvasUI_2();
		GameObject_t1113636619 * L_7 = WorldCanvasUI_get_CanvasGO_m332321093(L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		GameObject_t1113636619 * L_8 = V_0;
		GameObject_AddComponent_TisRotateTowardCamera_t118585142_m2773773848(L_8, /*hidden argument*/GameObject_AddComponent_TisRotateTowardCamera_t118585142_m2773773848_RuntimeMethod_var);
		Sprite_t280657092 * L_9 = __this->get_bgSprite_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_9, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00fd;
		}
	}
	{
		GameObject_t1113636619 * L_11 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m2093116449(L_11, _stringLiteral3306786237, /*hidden argument*/NULL);
		V_1 = L_11;
		GameObject_t1113636619 * L_12 = V_1;
		Transform_t3600365921 * L_13 = GameObject_get_transform_m1369836730(L_12, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_14 = V_0;
		Transform_t3600365921 * L_15 = GameObject_get_transform_m1369836730(L_14, /*hidden argument*/NULL);
		Transform_SetParent_m381167889(L_13, L_15, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_16 = V_1;
		Image_t2670269651 * L_17 = GameObject_AddComponent_TisImage_t2670269651_m1594579417(L_16, /*hidden argument*/GameObject_AddComponent_TisImage_t2670269651_m1594579417_RuntimeMethod_var);
		__this->set_bgImage_6(L_17);
		Image_t2670269651 * L_18 = __this->get_bgImage_6();
		Image_set_fillCenter_m3776786491(L_18, (bool)1, /*hidden argument*/NULL);
		Image_t2670269651 * L_19 = __this->get_bgImage_6();
		Sprite_t280657092 * L_20 = __this->get_bgSprite_5();
		Image_set_sprite_m2369174689(L_19, L_20, /*hidden argument*/NULL);
		Vector2_t2156229523  L_21 = __this->get_sizeDelta_3();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_22 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_23 = Vector2_op_Equality_m2303255133(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00e2;
		}
	}
	{
		Sprite_t280657092 * L_24 = __this->get_bgSprite_5();
		Rect_t2360479859  L_25 = Sprite_get_rect_m2575211689(L_24, /*hidden argument*/NULL);
		V_2 = L_25;
		float L_26 = Rect_get_width_m3421484486((&V_2), /*hidden argument*/NULL);
		Sprite_t280657092 * L_27 = __this->get_bgSprite_5();
		Rect_t2360479859  L_28 = Sprite_get_rect_m2575211689(L_27, /*hidden argument*/NULL);
		V_3 = L_28;
		float L_29 = Rect_get_height_m1358425599((&V_3), /*hidden argument*/NULL);
		Vector2_t2156229523  L_30;
		memset(&L_30, 0, sizeof(L_30));
		Vector2__ctor_m3970636864((&L_30), L_26, L_29, /*hidden argument*/NULL);
		__this->set_sizeDelta_3(L_30);
	}

IL_00e2:
	{
		Image_t2670269651 * L_31 = __this->get_bgImage_6();
		RectTransform_t3704657025 * L_32 = Graphic_get_rectTransform_m1167152468(L_31, /*hidden argument*/NULL);
		Vector2_t2156229523  L_33 = __this->get_sizeDelta_3();
		RectTransform_set_sizeDelta_m3462269772(L_32, L_33, /*hidden argument*/NULL);
		goto IL_0198;
	}

IL_00fd:
	{
		Texture_t3661962703 * L_34 = __this->get_bgTexture_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_35 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_34, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_0198;
		}
	}
	{
		GameObject_t1113636619 * L_36 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m2093116449(L_36, _stringLiteral2405869902, /*hidden argument*/NULL);
		V_4 = L_36;
		GameObject_t1113636619 * L_37 = V_4;
		Transform_t3600365921 * L_38 = GameObject_get_transform_m1369836730(L_37, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_39 = V_0;
		Transform_t3600365921 * L_40 = GameObject_get_transform_m1369836730(L_39, /*hidden argument*/NULL);
		Transform_SetParent_m381167889(L_38, L_40, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_41 = V_4;
		RawImage_t3182918964 * L_42 = GameObject_AddComponent_TisRawImage_t3182918964_m629883703(L_41, /*hidden argument*/GameObject_AddComponent_TisRawImage_t3182918964_m629883703_RuntimeMethod_var);
		__this->set_bgRawImage_8(L_42);
		RawImage_t3182918964 * L_43 = __this->get_bgRawImage_8();
		Texture_t3661962703 * L_44 = __this->get_bgTexture_7();
		RawImage_set_texture_m415027901(L_43, L_44, /*hidden argument*/NULL);
		Vector2_t2156229523  L_45 = __this->get_sizeDelta_3();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_46 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_47 = Vector2_op_Equality_m2303255133(NULL /*static, unused*/, L_45, L_46, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_0182;
		}
	}
	{
		Texture_t3661962703 * L_48 = __this->get_bgTexture_7();
		int32_t L_49 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_48);
		Texture_t3661962703 * L_50 = __this->get_bgTexture_7();
		int32_t L_51 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_50);
		Vector2_t2156229523  L_52;
		memset(&L_52, 0, sizeof(L_52));
		Vector2__ctor_m3970636864((&L_52), (((float)((float)L_49))), (((float)((float)L_51))), /*hidden argument*/NULL);
		__this->set_sizeDelta_3(L_52);
	}

IL_0182:
	{
		RawImage_t3182918964 * L_53 = __this->get_bgRawImage_8();
		RectTransform_t3704657025 * L_54 = Graphic_get_rectTransform_m1167152468(L_53, /*hidden argument*/NULL);
		Vector2_t2156229523  L_55 = __this->get_sizeDelta_3();
		RectTransform_set_sizeDelta_m3462269772(L_54, L_55, /*hidden argument*/NULL);
	}

IL_0198:
	{
		GameObject_t1113636619 * L_56 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m2093116449(L_56, _stringLiteral3814786466, /*hidden argument*/NULL);
		V_5 = L_56;
		GameObject_t1113636619 * L_57 = V_5;
		Transform_t3600365921 * L_58 = GameObject_get_transform_m1369836730(L_57, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_59 = V_0;
		Transform_t3600365921 * L_60 = GameObject_get_transform_m1369836730(L_59, /*hidden argument*/NULL);
		Transform_SetParent_m381167889(L_58, L_60, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_61 = V_5;
		Text_t1901882714 * L_62 = GameObject_AddComponent_TisText_t1901882714_m942857714(L_61, /*hidden argument*/GameObject_AddComponent_TisText_t1901882714_m942857714_RuntimeMethod_var);
		__this->set_textUI_9(L_62);
		Text_t1901882714 * L_63 = __this->get_textUI_9();
		RectTransform_t3704657025 * L_64 = Graphic_get_rectTransform_m1167152468(L_63, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_65 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		RectTransform_set_sizeDelta_m3462269772(L_64, L_65, /*hidden argument*/NULL);
		Text_t1901882714 * L_66 = __this->get_textUI_9();
		RectTransform_t3704657025 * L_67 = Graphic_get_rectTransform_m1167152468(L_66, /*hidden argument*/NULL);
		Vector2_t2156229523  L_68 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		RectTransform_set_anchorMin_m4230103102(L_67, L_68, /*hidden argument*/NULL);
		Text_t1901882714 * L_69 = __this->get_textUI_9();
		RectTransform_t3704657025 * L_70 = Graphic_get_rectTransform_m1167152468(L_69, /*hidden argument*/NULL);
		Vector2_t2156229523  L_71 = Vector2_get_one_m738793577(NULL /*static, unused*/, /*hidden argument*/NULL);
		RectTransform_set_anchorMax_m2998668828(L_70, L_71, /*hidden argument*/NULL);
		Text_t1901882714 * L_72 = __this->get_textUI_9();
		RectTransform_t3704657025 * L_73 = Graphic_get_rectTransform_m1167152468(L_72, /*hidden argument*/NULL);
		Vector2_t2156229523  L_74;
		memset(&L_74, 0, sizeof(L_74));
		Vector2__ctor_m3970636864((&L_74), (0.5f), (0.5f), /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m4126691837(L_73, L_74, /*hidden argument*/NULL);
		Font_t1956802104 * L_75 = __this->get_font_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_76 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_75, /*hidden argument*/NULL);
		if (!L_76)
		{
			goto IL_0247;
		}
	}
	{
		Text_t1901882714 * L_77 = __this->get_textUI_9();
		Font_t1956802104 * L_78 = __this->get_font_11();
		Text_set_font_m2192091651(L_77, L_78, /*hidden argument*/NULL);
		goto IL_0259;
	}

IL_0247:
	{
		Text_t1901882714 * L_79 = __this->get_textUI_9();
		FontU5BU5D_t1399044585* L_80 = Resources_FindObjectsOfTypeAll_TisFont_t1956802104_m2894531458(NULL /*static, unused*/, /*hidden argument*/Resources_FindObjectsOfTypeAll_TisFont_t1956802104_m2894531458_RuntimeMethod_var);
		int32_t L_81 = 0;
		Font_t1956802104 * L_82 = (L_80)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_81));
		Text_set_font_m2192091651(L_79, L_82, /*hidden argument*/NULL);
	}

IL_0259:
	{
		Text_t1901882714 * L_83 = __this->get_textUI_9();
		int32_t L_84 = __this->get_fontSize_10();
		Text_set_fontSize_m3617617524(L_83, L_84, /*hidden argument*/NULL);
		Text_t1901882714 * L_85 = __this->get_textUI_9();
		Color_t2555686324  L_86 = Color_get_black_m719512684(NULL /*static, unused*/, /*hidden argument*/NULL);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_85, L_86);
		Text_t1901882714 * L_87 = __this->get_textUI_9();
		Text_set_alignment_m88714888(L_87, 4, /*hidden argument*/NULL);
		ChatBubbleUI_ResetCanvasUIs_m1740381972(__this, /*hidden argument*/NULL);
		float L_88 = __this->get_timeInterval_13();
		G_B14_0 = __this;
		if ((!(((float)L_88) >= ((float)(0.01f)))))
		{
			G_B15_0 = __this;
			goto IL_02a3;
		}
	}
	{
		G_B16_0 = 1;
		G_B16_1 = G_B14_0;
		goto IL_02a4;
	}

IL_02a3:
	{
		G_B16_0 = 0;
		G_B16_1 = G_B15_0;
	}

IL_02a4:
	{
		G_B16_1->set_autoPop_14((bool)G_B16_0);
		return;
	}
}
// System.Void ChatBubbleUI::Update()
extern "C"  void ChatBubbleUI_Update_m2640407471 (ChatBubbleUI_t1508846992 * __this, const RuntimeMethod* method)
{
	{
		ChatBubbleUI_PopChatUI_m2587343892(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ChatBubbleUI::LimitCanvasUIsBasedOnSizeDelta()
extern "C"  void ChatBubbleUI_LimitCanvasUIsBasedOnSizeDelta_m526648713 (ChatBubbleUI_t1508846992 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChatBubbleUI_LimitCanvasUIsBasedOnSizeDelta_m526648713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		Vector2_t2156229523  L_0 = __this->get_sizeDelta_3();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_1 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_2 = Vector2_op_Equality_m2303255133(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_005a;
		}
	}
	{
		Vector2_t2156229523 * L_3 = __this->get_address_of_sizeDelta_3();
		float L_4 = L_3->get_x_0();
		Text_t1901882714 * L_5 = __this->get_textUI_9();
		float L_6 = VirtFuncInvoker0< float >::Invoke(76 /* System.Single UnityEngine.UI.Text::get_preferredWidth() */, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Max_m3146388979(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Vector2_t2156229523 * L_8 = __this->get_address_of_sizeDelta_3();
		float L_9 = L_8->get_y_1();
		Text_t1901882714 * L_10 = __this->get_textUI_9();
		float L_11 = VirtFuncInvoker0< float >::Invoke(79 /* System.Single UnityEngine.UI.Text::get_preferredHeight() */, L_10);
		float L_12 = Mathf_Max_m3146388979(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		float L_13 = V_0;
		float L_14 = V_1;
		Vector2_t2156229523  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Vector2__ctor_m3970636864((&L_15), L_13, L_14, /*hidden argument*/NULL);
		__this->set_sizeDelta_3(L_15);
	}

IL_005a:
	{
		Vector2_t2156229523  L_16 = __this->get_sizeDelta_3();
		ChatBubbleUI_ResetCanvasUIs_m1868350676(__this, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ChatBubbleUI::AutoCanvasUIs()
extern "C"  void ChatBubbleUI_AutoCanvasUIs_m213075858 (ChatBubbleUI_t1508846992 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChatBubbleUI_AutoCanvasUIs_m213075858_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		Vector2_t2156229523 * L_0 = __this->get_address_of_sizeDelta_3();
		float L_1 = L_0->get_x_0();
		Text_t1901882714 * L_2 = __this->get_textUI_9();
		float L_3 = VirtFuncInvoker0< float >::Invoke(76 /* System.Single UnityEngine.UI.Text::get_preferredWidth() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_4 = Mathf_Max_m3146388979(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Vector2_t2156229523 * L_5 = __this->get_address_of_sizeDelta_3();
		float L_6 = L_5->get_y_1();
		Text_t1901882714 * L_7 = __this->get_textUI_9();
		float L_8 = VirtFuncInvoker0< float >::Invoke(79 /* System.Single UnityEngine.UI.Text::get_preferredHeight() */, L_7);
		float L_9 = Mathf_Max_m3146388979(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		float L_10 = V_0;
		float L_11 = V_1;
		Vector2_t2156229523  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector2__ctor_m3970636864((&L_12), L_10, L_11, /*hidden argument*/NULL);
		ChatBubbleUI_ResetCanvasUIs_m1868350676(__this, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ChatBubbleUI::ResetCanvasUIs()
extern "C"  void ChatBubbleUI_ResetCanvasUIs_m1740381972 (ChatBubbleUI_t1508846992 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_autoCanvasUIs_4();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ChatBubbleUI_AutoCanvasUIs_m213075858(__this, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_0016:
	{
		ChatBubbleUI_LimitCanvasUIsBasedOnSizeDelta_m526648713(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void ChatBubbleUI::ResetCanvasUIs(UnityEngine.Vector2)
extern "C"  void ChatBubbleUI_ResetCanvasUIs_m1868350676 (ChatBubbleUI_t1508846992 * __this, Vector2_t2156229523  ___size0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChatBubbleUI_ResetCanvasUIs_m1868350676_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectTransform_t3704657025 * V_0 = NULL;
	{
		Vector2_t2156229523  L_0 = ___size0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_1 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_2 = Vector2_op_Equality_m2303255133(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Vector2_t2156229523  L_3 = ___size0;
		__this->set_sizeDelta_3(L_3);
		WorldCanvasUI_t1156772134 * L_4 = __this->get_worldCanvasUI_2();
		GameObject_t1113636619 * L_5 = WorldCanvasUI_get_CanvasGO_m332321093(L_4, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_6 = GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398(L_5, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3704657025_m2005094398_RuntimeMethod_var);
		V_0 = L_6;
		RectTransform_t3704657025 * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0071;
		}
	}
	{
		RectTransform_t3704657025 * L_9 = V_0;
		Vector2_t2156229523  L_10 = __this->get_sizeDelta_3();
		RectTransform_set_sizeDelta_m3462269772(L_9, L_10, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_11 = V_0;
		Transform_t3600365921 * L_12 = Component_get_transform_m3162698980(L_11, /*hidden argument*/NULL);
		Vector2_t2156229523 * L_13 = __this->get_address_of_sizeDelta_3();
		float L_14 = L_13->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_15 = Mathf_FloorToInt_m1870542928(NULL /*static, unused*/, ((float)((float)L_14/(float)(2.0f))), /*hidden argument*/NULL);
		Vector3_t3722313464  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector3__ctor_m3353183577((&L_16), (0.0f), (((float)((float)L_15))), (0.0f), /*hidden argument*/NULL);
		Transform_set_localPosition_m4128471975(L_12, L_16, /*hidden argument*/NULL);
	}

IL_0071:
	{
		Image_t2670269651 * L_17 = __this->get_bgImage_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_18 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0097;
		}
	}
	{
		Image_t2670269651 * L_19 = __this->get_bgImage_6();
		RectTransform_t3704657025 * L_20 = Graphic_get_rectTransform_m1167152468(L_19, /*hidden argument*/NULL);
		Vector2_t2156229523  L_21 = __this->get_sizeDelta_3();
		RectTransform_set_sizeDelta_m3462269772(L_20, L_21, /*hidden argument*/NULL);
	}

IL_0097:
	{
		RawImage_t3182918964 * L_22 = __this->get_bgRawImage_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_23 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00bd;
		}
	}
	{
		RawImage_t3182918964 * L_24 = __this->get_bgRawImage_8();
		RectTransform_t3704657025 * L_25 = Graphic_get_rectTransform_m1167152468(L_24, /*hidden argument*/NULL);
		Vector2_t2156229523  L_26 = __this->get_sizeDelta_3();
		RectTransform_set_sizeDelta_m3462269772(L_25, L_26, /*hidden argument*/NULL);
	}

IL_00bd:
	{
		Text_t1901882714 * L_27 = __this->get_textUI_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_28 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00e3;
		}
	}
	{
		Text_t1901882714 * L_29 = __this->get_textUI_9();
		RectTransform_t3704657025 * L_30 = Graphic_get_rectTransform_m1167152468(L_29, /*hidden argument*/NULL);
		Vector2_t2156229523  L_31 = __this->get_sizeDelta_3();
		RectTransform_set_sizeDelta_m3462269772(L_30, L_31, /*hidden argument*/NULL);
	}

IL_00e3:
	{
		return;
	}
}
// System.Void ChatBubbleUI::PopChatUI()
extern "C"  void ChatBubbleUI_PopChatUI_m2587343892 (ChatBubbleUI_t1508846992 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChatBubbleUI_PopChatUI_m2587343892_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t1901882714 * L_0 = __this->get_textUI_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		bool L_2 = __this->get_autoPop_14();
		if (!L_2)
		{
			goto IL_004b;
		}
	}
	{
		float L_3 = __this->get_curDeltaTime_15();
		float L_4 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_curDeltaTime_15(((float)il2cpp_codegen_add((float)L_3, (float)L_4)));
		float L_5 = __this->get_curDeltaTime_15();
		float L_6 = __this->get_timeInterval_13();
		if ((!(((float)L_5) >= ((float)L_6))))
		{
			goto IL_0046;
		}
	}
	{
		ChatBubbleUI_PopChat_m2172760713(__this, /*hidden argument*/NULL);
	}

IL_0046:
	{
		goto IL_0076;
	}

IL_004b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_7 = Input_GetMouseButtonDown_m2081676745(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0070;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_8 = Input_GetKeyDown_m2928138282(NULL /*static, unused*/, _stringLiteral1613539661, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0070;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		int32_t L_9 = Input_get_touchCount_m3403849067(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_0076;
		}
	}

IL_0070:
	{
		ChatBubbleUI_PopChat_m2172760713(__this, /*hidden argument*/NULL);
	}

IL_0076:
	{
		return;
	}
}
// System.Void ChatBubbleUI::PopChat()
extern "C"  void ChatBubbleUI_PopChat_m2172760713 (ChatBubbleUI_t1508846992 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChatBubbleUI_PopChat_m2172760713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		Image_t2670269651 * L_0 = __this->get_bgImage_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		Image_t2670269651 * L_2 = __this->get_bgImage_6();
		RectTransform_t3704657025 * L_3 = Graphic_get_rectTransform_m1167152468(L_2, /*hidden argument*/NULL);
		Vector2_t2156229523  L_4 = RectTransform_get_sizeDelta_m2183112744(L_3, /*hidden argument*/NULL);
		__this->set_sizeDelta_3(L_4);
		goto IL_0063;
	}

IL_002c:
	{
		RawImage_t3182918964 * L_5 = __this->get_bgRawImage_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_5, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0058;
		}
	}
	{
		RawImage_t3182918964 * L_7 = __this->get_bgRawImage_8();
		RectTransform_t3704657025 * L_8 = Graphic_get_rectTransform_m1167152468(L_7, /*hidden argument*/NULL);
		Vector2_t2156229523  L_9 = RectTransform_get_sizeDelta_m2183112744(L_8, /*hidden argument*/NULL);
		__this->set_sizeDelta_3(L_9);
		goto IL_0063;
	}

IL_0058:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_10 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_sizeDelta_3(L_10);
	}

IL_0063:
	{
		List_1_t3319525431 * L_11 = __this->get_chatStrList_12();
		int32_t L_12 = List_1_get_Count_m2276455407(L_11, /*hidden argument*/List_1_get_Count_m2276455407_RuntimeMethod_var);
		if ((((int32_t)L_12) <= ((int32_t)0)))
		{
			goto IL_00ab;
		}
	}
	{
		ChatBubbleUI_EnableChatBubbleUI_m3745119852(__this, (bool)1, /*hidden argument*/NULL);
		List_1_t3319525431 * L_13 = __this->get_chatStrList_12();
		String_t* L_14 = List_1_get_Item_m3346958548(L_13, 0, /*hidden argument*/List_1_get_Item_m3346958548_RuntimeMethod_var);
		V_0 = L_14;
		Text_t1901882714 * L_15 = __this->get_textUI_9();
		String_t* L_16 = V_0;
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_15, L_16);
		List_1_t3319525431 * L_17 = __this->get_chatStrList_12();
		List_1_RemoveAt_m815285786(L_17, 0, /*hidden argument*/List_1_RemoveAt_m815285786_RuntimeMethod_var);
		ChatBubbleUI_ResetCanvasUIs_m1740381972(__this, /*hidden argument*/NULL);
		goto IL_00bd;
	}

IL_00ab:
	{
		bool L_18 = __this->get_HideAfterAllChatPoped_16();
		if (!L_18)
		{
			goto IL_00bd;
		}
	}
	{
		ChatBubbleUI_EnableChatBubbleUI_m3745119852(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_00bd:
	{
		__this->set_curDeltaTime_15((0.0f));
		return;
	}
}
// System.Void ChatBubbleUI::EnableChatBubbleUI(System.Boolean)
extern "C"  void ChatBubbleUI_EnableChatBubbleUI_m3745119852 (ChatBubbleUI_t1508846992 * __this, bool ___enable0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChatBubbleUI_EnableChatBubbleUI_m3745119852_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t1901882714 * L_0 = __this->get_textUI_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Text_t1901882714 * L_2 = __this->get_textUI_9();
		bool L_3 = ___enable0;
		Behaviour_set_enabled_m20417929(L_2, L_3, /*hidden argument*/NULL);
	}

IL_001c:
	{
		Image_t2670269651 * L_4 = __this->get_bgImage_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0038;
		}
	}
	{
		Image_t2670269651 * L_6 = __this->get_bgImage_6();
		bool L_7 = ___enable0;
		Behaviour_set_enabled_m20417929(L_6, L_7, /*hidden argument*/NULL);
	}

IL_0038:
	{
		RawImage_t3182918964 * L_8 = __this->get_bgRawImage_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0054;
		}
	}
	{
		RawImage_t3182918964 * L_10 = __this->get_bgRawImage_8();
		bool L_11 = ___enable0;
		Behaviour_set_enabled_m20417929(L_10, L_11, /*hidden argument*/NULL);
	}

IL_0054:
	{
		return;
	}
}
// System.Void ChatBubbleUI::DestoryChatBubbleUI()
extern "C"  void ChatBubbleUI_DestoryChatBubbleUI_m4189235868 (ChatBubbleUI_t1508846992 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChatBubbleUI_DestoryChatBubbleUI_m4189235868_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	GameObject_t1113636619 * V_1 = NULL;
	GameObject_t1113636619 * V_2 = NULL;
	{
		Text_t1901882714 * L_0 = __this->get_textUI_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0038;
		}
	}
	{
		Text_t1901882714 * L_2 = __this->get_textUI_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m3193525861(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Text_t1901882714 * L_3 = __this->get_textUI_9();
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m442555142(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		GameObject_t1113636619 * L_5 = V_0;
		bool L_6 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		GameObject_t1113636619 * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m3193525861(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0038:
	{
		Image_t2670269651 * L_8 = __this->get_bgImage_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0070;
		}
	}
	{
		Image_t2670269651 * L_10 = __this->get_bgImage_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m3193525861(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Image_t2670269651 * L_11 = __this->get_bgImage_6();
		GameObject_t1113636619 * L_12 = Component_get_gameObject_m442555142(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		GameObject_t1113636619 * L_13 = V_1;
		bool L_14 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0070;
		}
	}
	{
		GameObject_t1113636619 * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m3193525861(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
	}

IL_0070:
	{
		RawImage_t3182918964 * L_16 = __this->get_bgRawImage_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00a8;
		}
	}
	{
		RawImage_t3182918964 * L_18 = __this->get_bgRawImage_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m3193525861(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		RawImage_t3182918964 * L_19 = __this->get_bgRawImage_8();
		GameObject_t1113636619 * L_20 = Component_get_gameObject_m442555142(L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		GameObject_t1113636619 * L_21 = V_2;
		bool L_22 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00a8;
		}
	}
	{
		GameObject_t1113636619 * L_23 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m3193525861(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
	}

IL_00a8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ChatBubbleUI::AddChatString(System.String)
extern "C"  void ChatBubbleUI_AddChatString_m115359011 (ChatBubbleUI_t1508846992 * __this, String_t* ___chat0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ChatBubbleUI_AddChatString_m115359011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___chat0;
		int32_t L_1 = String_get_Length_m3847582255(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		List_1_t3319525431 * L_2 = __this->get_chatStrList_12();
		String_t* L_3 = ___chat0;
		List_1_Add_m1685793073(L_2, L_3, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CreateButtonScript::.ctor()
extern "C"  void CreateButtonScript__ctor_m3550083259 (CreateButtonScript_t92967038 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CreateButtonScript::Start()
extern "C"  void CreateButtonScript_Start_m377316117 (CreateButtonScript_t92967038 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CreateButtonScript_Start_m377316117_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	GameObject_t1113636619 * V_1 = NULL;
	Canvas_t3310196443 * V_2 = NULL;
	GameObject_t1113636619 * V_3 = NULL;
	Image_t2670269651 * V_4 = NULL;
	Button_t4055032469 * V_5 = NULL;
	GameObject_t1113636619 * V_6 = NULL;
	Text_t1901882714 * V_7 = NULL;
	ButtonClickedEvent_t48803504 * G_B4_0 = NULL;
	ButtonClickedEvent_t48803504 * G_B3_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		EventSystem_t1003666588 * L_0 = Object_FindObjectOfType_TisEventSystem_t1003666588_m1331729420(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisEventSystem_t1003666588_m1331729420_RuntimeMethod_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0035;
		}
	}
	{
		TypeU5BU5D_t3940880105* L_2 = ((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)1));
		RuntimeTypeHandle_t3027515415  L_3 = { reinterpret_cast<intptr_t> (EventSystem_t1003666588_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_2, L_4);
		(L_2)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_4);
		GameObject_t1113636619 * L_5 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m1350607670(L_5, _stringLiteral3534642813, L_2, /*hidden argument*/NULL);
		V_0 = L_5;
		GameObject_t1113636619 * L_6 = V_0;
		GameObject_AddComponent_TisStandaloneInputModule_t2760469101_m1723984740(L_6, /*hidden argument*/GameObject_AddComponent_TisStandaloneInputModule_t2760469101_m1723984740_RuntimeMethod_var);
	}

IL_0035:
	{
		GameObject_t1113636619 * L_7 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m2093116449(L_7, _stringLiteral2323074440, /*hidden argument*/NULL);
		V_1 = L_7;
		GameObject_t1113636619 * L_8 = V_1;
		Canvas_t3310196443 * L_9 = GameObject_AddComponent_TisCanvas_t3310196443_m284883347(L_8, /*hidden argument*/GameObject_AddComponent_TisCanvas_t3310196443_m284883347_RuntimeMethod_var);
		V_2 = L_9;
		GameObject_t1113636619 * L_10 = V_1;
		GameObject_AddComponent_TisGraphicRaycaster_t2999697109_m1217468238(L_10, /*hidden argument*/GameObject_AddComponent_TisGraphicRaycaster_t2999697109_m1217468238_RuntimeMethod_var);
		Canvas_t3310196443 * L_11 = V_2;
		Canvas_set_renderMode_m1564704306(L_11, 0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_12 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m2093116449(L_12, _stringLiteral1208991571, /*hidden argument*/NULL);
		V_3 = L_12;
		GameObject_t1113636619 * L_13 = V_3;
		Image_t2670269651 * L_14 = GameObject_AddComponent_TisImage_t2670269651_m1594579417(L_13, /*hidden argument*/GameObject_AddComponent_TisImage_t2670269651_m1594579417_RuntimeMethod_var);
		V_4 = L_14;
		Image_t2670269651 * L_15 = V_4;
		Transform_t3600365921 * L_16 = Component_get_transform_m3162698980(L_15, /*hidden argument*/NULL);
		Canvas_t3310196443 * L_17 = V_2;
		Transform_t3600365921 * L_18 = Component_get_transform_m3162698980(L_17, /*hidden argument*/NULL);
		Transform_set_parent_m786917804(L_16, L_18, /*hidden argument*/NULL);
		Image_t2670269651 * L_19 = V_4;
		RectTransform_t3704657025 * L_20 = Graphic_get_rectTransform_m1167152468(L_19, /*hidden argument*/NULL);
		Vector2_t2156229523  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector2__ctor_m3970636864((&L_21), (180.0f), (50.0f), /*hidden argument*/NULL);
		RectTransform_set_sizeDelta_m3462269772(L_20, L_21, /*hidden argument*/NULL);
		Image_t2670269651 * L_22 = V_4;
		RectTransform_t3704657025 * L_23 = Graphic_get_rectTransform_m1167152468(L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_24 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_25 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m4126691837(L_23, L_25, /*hidden argument*/NULL);
		Image_t2670269651 * L_26 = V_4;
		Color_t2555686324  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Color__ctor_m2943235014((&L_27), (1.0f), (0.3f), (0.3f), (0.5f), /*hidden argument*/NULL);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_26, L_27);
		GameObject_t1113636619 * L_28 = V_3;
		Button_t4055032469 * L_29 = GameObject_AddComponent_TisButton_t4055032469_m1106144321(L_28, /*hidden argument*/GameObject_AddComponent_TisButton_t4055032469_m1106144321_RuntimeMethod_var);
		V_5 = L_29;
		Button_t4055032469 * L_30 = V_5;
		Image_t2670269651 * L_31 = V_4;
		Selectable_set_targetGraphic_m1003546643(L_30, L_31, /*hidden argument*/NULL);
		Button_t4055032469 * L_32 = V_5;
		ButtonClickedEvent_t48803504 * L_33 = Button_get_onClick_m2332132945(L_32, /*hidden argument*/NULL);
		UnityAction_t3245792599 * L_34 = ((CreateButtonScript_t92967038_StaticFields*)il2cpp_codegen_static_fields_for(CreateButtonScript_t92967038_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_2();
		G_B3_0 = L_33;
		if (L_34)
		{
			G_B4_0 = L_33;
			goto IL_00fb;
		}
	}
	{
		intptr_t L_35 = (intptr_t)CreateButtonScript_U3CStartU3Em__0_m530170949_RuntimeMethod_var;
		UnityAction_t3245792599 * L_36 = (UnityAction_t3245792599 *)il2cpp_codegen_object_new(UnityAction_t3245792599_il2cpp_TypeInfo_var);
		UnityAction__ctor_m772160306(L_36, NULL, L_35, /*hidden argument*/NULL);
		((CreateButtonScript_t92967038_StaticFields*)il2cpp_codegen_static_fields_for(CreateButtonScript_t92967038_il2cpp_TypeInfo_var))->set_U3CU3Ef__amU24cache0_2(L_36);
		G_B4_0 = G_B3_0;
	}

IL_00fb:
	{
		UnityAction_t3245792599 * L_37 = ((CreateButtonScript_t92967038_StaticFields*)il2cpp_codegen_static_fields_for(CreateButtonScript_t92967038_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_2();
		UnityEvent_AddListener_m2276267359(G_B4_0, L_37, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_38 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m2093116449(L_38, _stringLiteral3987835886, /*hidden argument*/NULL);
		V_6 = L_38;
		GameObject_t1113636619 * L_39 = V_6;
		Transform_t3600365921 * L_40 = GameObject_get_transform_m1369836730(L_39, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_41 = V_3;
		Transform_t3600365921 * L_42 = GameObject_get_transform_m1369836730(L_41, /*hidden argument*/NULL);
		Transform_set_parent_m786917804(L_40, L_42, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_43 = V_6;
		Text_t1901882714 * L_44 = GameObject_AddComponent_TisText_t1901882714_m942857714(L_43, /*hidden argument*/GameObject_AddComponent_TisText_t1901882714_m942857714_RuntimeMethod_var);
		V_7 = L_44;
		Text_t1901882714 * L_45 = V_7;
		RectTransform_t3704657025 * L_46 = Graphic_get_rectTransform_m1167152468(L_45, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_47 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		RectTransform_set_sizeDelta_m3462269772(L_46, L_47, /*hidden argument*/NULL);
		Text_t1901882714 * L_48 = V_7;
		RectTransform_t3704657025 * L_49 = Graphic_get_rectTransform_m1167152468(L_48, /*hidden argument*/NULL);
		Vector2_t2156229523  L_50 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		RectTransform_set_anchorMin_m4230103102(L_49, L_50, /*hidden argument*/NULL);
		Text_t1901882714 * L_51 = V_7;
		RectTransform_t3704657025 * L_52 = Graphic_get_rectTransform_m1167152468(L_51, /*hidden argument*/NULL);
		Vector2_t2156229523  L_53 = Vector2_get_one_m738793577(NULL /*static, unused*/, /*hidden argument*/NULL);
		RectTransform_set_anchorMax_m2998668828(L_52, L_53, /*hidden argument*/NULL);
		Text_t1901882714 * L_54 = V_7;
		RectTransform_t3704657025 * L_55 = Graphic_get_rectTransform_m1167152468(L_54, /*hidden argument*/NULL);
		Vector2_t2156229523  L_56;
		memset(&L_56, 0, sizeof(L_56));
		Vector2__ctor_m3970636864((&L_56), (0.5f), (0.5f), /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m4126691837(L_55, L_56, /*hidden argument*/NULL);
		Text_t1901882714 * L_57 = V_7;
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_57, _stringLiteral1740661195);
		Text_t1901882714 * L_58 = V_7;
		FontU5BU5D_t1399044585* L_59 = Resources_FindObjectsOfTypeAll_TisFont_t1956802104_m2894531458(NULL /*static, unused*/, /*hidden argument*/Resources_FindObjectsOfTypeAll_TisFont_t1956802104_m2894531458_RuntimeMethod_var);
		int32_t L_60 = 0;
		Font_t1956802104 * L_61 = (L_59)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_60));
		Text_set_font_m2192091651(L_58, L_61, /*hidden argument*/NULL);
		Text_t1901882714 * L_62 = V_7;
		Text_set_fontSize_m3617617524(L_62, ((int32_t)20), /*hidden argument*/NULL);
		Text_t1901882714 * L_63 = V_7;
		Color_t2555686324  L_64 = Color_get_yellow_m1287957903(NULL /*static, unused*/, /*hidden argument*/NULL);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_63, L_64);
		Text_t1901882714 * L_65 = V_7;
		Text_set_alignment_m88714888(L_65, 4, /*hidden argument*/NULL);
		Button_t4055032469 * L_66 = V_5;
		ButtonClickedEvent_t48803504 * L_67 = Button_get_onClick_m2332132945(L_66, /*hidden argument*/NULL);
		intptr_t L_68 = (intptr_t)CreateButtonScript_onBtnClicked_m2005882438_RuntimeMethod_var;
		UnityAction_t3245792599 * L_69 = (UnityAction_t3245792599 *)il2cpp_codegen_object_new(UnityAction_t3245792599_il2cpp_TypeInfo_var);
		UnityAction__ctor_m772160306(L_69, __this, L_68, /*hidden argument*/NULL);
		UnityEvent_AddListener_m2276267359(L_67, L_69, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CreateButtonScript::onBtnClicked()
extern "C"  void CreateButtonScript_onBtnClicked_m2005882438 (CreateButtonScript_t92967038 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CreateButtonScript_onBtnClicked_m2005882438_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3338840811, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CreateButtonScript::<Start>m__0()
extern "C"  void CreateButtonScript_U3CStartU3Em__0_m530170949 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CreateButtonScript_U3CStartU3Em__0_m530170949_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = L_0;
		RuntimeObject * L_2 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DebugTools::DrawSphere(UnityEngine.Vector3)
extern "C"  void DebugTools_DrawSphere_m2444497830 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___pos0, const RuntimeMethod* method)
{
	GameObject_t1113636619 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = GameObject_CreatePrimitive_m2902598419(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1113636619 * L_1 = V_0;
		Transform_t3600365921 * L_2 = GameObject_get_transform_m1369836730(L_1, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3 = ___pos0;
		Transform_set_position_m3387557959(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DoneCameraMovement::.ctor()
extern "C"  void DoneCameraMovement__ctor_m3455277338 (DoneCameraMovement_t1316814223 * __this, const RuntimeMethod* method)
{
	{
		__this->set_smooth_2((1.5f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DoneCameraMovement::Awake()
extern "C"  void DoneCameraMovement_Awake_m430543615 (DoneCameraMovement_t1316814223 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DoneCameraMovement_Awake_m430543615_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = GameObject_FindGameObjectWithTag_m2129039296(NULL /*static, unused*/, _stringLiteral2261822918, /*hidden argument*/NULL);
		Transform_t3600365921 * L_1 = GameObject_get_transform_m1369836730(L_0, /*hidden argument*/NULL);
		__this->set_player_4(L_1);
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3 = Transform_get_position_m36019626(L_2, /*hidden argument*/NULL);
		Transform_t3600365921 * L_4 = __this->get_player_4();
		Vector3_t3722313464  L_5 = Transform_get_position_m36019626(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_6 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		__this->set_relCameraPos_5(L_6);
		Vector3_t3722313464 * L_7 = __this->get_address_of_relCameraPos_5();
		float L_8 = Vector3_get_magnitude_m27958459(L_7, /*hidden argument*/NULL);
		__this->set_relCameraPosMag_6(((float)il2cpp_codegen_subtract((float)L_8, (float)(0.5f))));
		return;
	}
}
// System.Void DoneCameraMovement::FixedUpdate()
extern "C"  void DoneCameraMovement_FixedUpdate_m2285240065 (DoneCameraMovement_t1316814223 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DoneCameraMovement_FixedUpdate_m2285240065_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3U5BU5D_t1718750761* V_2 = NULL;
	int32_t V_3 = 0;
	{
		bool L_0 = __this->get_extraCameraAdjust_3();
		if (!L_0)
		{
			goto IL_0109;
		}
	}
	{
		Transform_t3600365921 * L_1 = __this->get_player_4();
		Vector3_t3722313464  L_2 = Transform_get_position_m36019626(L_1, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3 = __this->get_relCameraPos_5();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_4 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Transform_t3600365921 * L_5 = __this->get_player_4();
		Vector3_t3722313464  L_6 = Transform_get_position_m36019626(L_5, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = __this->get_relCameraPosMag_6();
		Vector3_t3722313464  L_9 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		Vector3_t3722313464  L_10 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_6, L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		V_2 = ((Vector3U5BU5D_t1718750761*)SZArrayNew(Vector3U5BU5D_t1718750761_il2cpp_TypeInfo_var, (uint32_t)5));
		Vector3U5BU5D_t1718750761* L_11 = V_2;
		Vector3_t3722313464  L_12 = V_0;
		*(Vector3_t3722313464 *)((L_11)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(0))) = L_12;
		Vector3U5BU5D_t1718750761* L_13 = V_2;
		Vector3_t3722313464  L_14 = V_0;
		Vector3_t3722313464  L_15 = V_1;
		Vector3_t3722313464  L_16 = Vector3_Lerp_m407887542(NULL /*static, unused*/, L_14, L_15, (0.25f), /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)((L_13)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(1))) = L_16;
		Vector3U5BU5D_t1718750761* L_17 = V_2;
		Vector3_t3722313464  L_18 = V_0;
		Vector3_t3722313464  L_19 = V_1;
		Vector3_t3722313464  L_20 = Vector3_Lerp_m407887542(NULL /*static, unused*/, L_18, L_19, (0.5f), /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)((L_17)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(2))) = L_20;
		Vector3U5BU5D_t1718750761* L_21 = V_2;
		Vector3_t3722313464  L_22 = V_0;
		Vector3_t3722313464  L_23 = V_1;
		Vector3_t3722313464  L_24 = Vector3_Lerp_m407887542(NULL /*static, unused*/, L_22, L_23, (0.75f), /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)((L_21)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(3))) = L_24;
		Vector3U5BU5D_t1718750761* L_25 = V_2;
		Vector3_t3722313464  L_26 = V_1;
		*(Vector3_t3722313464 *)((L_25)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(4))) = L_26;
		V_3 = 0;
		goto IL_00d3;
	}

IL_00b3:
	{
		Vector3U5BU5D_t1718750761* L_27 = V_2;
		int32_t L_28 = V_3;
		bool L_29 = DoneCameraMovement_ViewingPosCheck_m2483040884(__this, (*(Vector3_t3722313464 *)((L_27)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_28)))), /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00cf;
		}
	}
	{
		goto IL_00dc;
	}

IL_00cf:
	{
		int32_t L_30 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)1));
	}

IL_00d3:
	{
		int32_t L_31 = V_3;
		Vector3U5BU5D_t1718750761* L_32 = V_2;
		if ((((int32_t)L_31) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_32)->max_length)))))))
		{
			goto IL_00b3;
		}
	}

IL_00dc:
	{
		Transform_t3600365921 * L_33 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_34 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_35 = Transform_get_position_m36019626(L_34, /*hidden argument*/NULL);
		Vector3_t3722313464  L_36 = __this->get_newPos_7();
		float L_37 = __this->get_smooth_2();
		float L_38 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_39 = Vector3_Lerp_m407887542(NULL /*static, unused*/, L_35, L_36, ((float)il2cpp_codegen_multiply((float)L_37, (float)L_38)), /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_33, L_39, /*hidden argument*/NULL);
	}

IL_0109:
	{
		DoneCameraMovement_SmoothLookAt_m490920(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DoneCameraMovement::ViewingPosCheck(UnityEngine.Vector3)
extern "C"  bool DoneCameraMovement_ViewingPosCheck_m2483040884 (DoneCameraMovement_t1316814223 * __this, Vector3_t3722313464  ___checkPos0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DoneCameraMovement_ViewingPosCheck_m2483040884_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit_t1056001966  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = ___checkPos0;
		Transform_t3600365921 * L_1 = __this->get_player_4();
		Vector3_t3722313464  L_2 = Transform_get_position_m36019626(L_1, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3 = ___checkPos0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_4 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		float L_5 = __this->get_relCameraPosMag_6();
		bool L_6 = Physics_Raycast_m261647105(NULL /*static, unused*/, L_0, L_4, (&V_0), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003d;
		}
	}
	{
		Transform_t3600365921 * L_7 = RaycastHit_get_transform_m942054759((&V_0), /*hidden argument*/NULL);
		Transform_t3600365921 * L_8 = __this->get_player_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003d;
		}
	}
	{
		return (bool)0;
	}

IL_003d:
	{
		Vector3_t3722313464  L_10 = ___checkPos0;
		__this->set_newPos_7(L_10);
		return (bool)1;
	}
}
// System.Void DoneCameraMovement::SmoothLookAt()
extern "C"  void DoneCameraMovement_SmoothLookAt_m490920 (DoneCameraMovement_t1316814223 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DoneCameraMovement_SmoothLookAt_m490920_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t2301928331  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t3600365921 * L_0 = __this->get_player_4();
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3 = Transform_get_position_m36019626(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_4 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Vector3_t3722313464  L_5 = V_0;
		Vector3_t3722313464  L_6 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_7 = Quaternion_LookRotation_m3197602968(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		Transform_t3600365921 * L_8 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_9 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_10 = Transform_get_rotation_m3502953881(L_9, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_11 = V_1;
		float L_12 = __this->get_smooth_2();
		float L_13 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_14 = Quaternion_Lerp_m1238806789(NULL /*static, unused*/, L_10, L_11, ((float)il2cpp_codegen_multiply((float)L_12, (float)L_13)), /*hidden argument*/NULL);
		Transform_set_rotation_m3524318132(L_8, L_14, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EnemyPatrol::.ctor()
extern "C"  void EnemyPatrol__ctor_m1304340711 (EnemyPatrol_t3346945880 * __this, const RuntimeMethod* method)
{
	{
		__this->set_patrolSpeed_2((2.5f));
		__this->set_detectXDistance_3((15.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyPatrol::Start()
extern "C"  void EnemyPatrol_Start_m2074163177 (EnemyPatrol_t3346945880 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyPatrol_Start_m2074163177_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LayerMask_t3493934918  L_0 = __this->get_raycastMask_4();
		int32_t L_1 = LayerMask_op_Implicit_m3296792737(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = LayerMask_NameToLayer_m2359665122(NULL /*static, unused*/, _stringLiteral78692563, /*hidden argument*/NULL);
		LayerMask_t3493934918  L_3 = LayerMask_op_Implicit_m90232283(NULL /*static, unused*/, ((int32_t)((int32_t)L_1|(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_2&(int32_t)((int32_t)31))))))), /*hidden argument*/NULL);
		__this->set_raycastMask_4(L_3);
		LayerMask_t3493934918  L_4 = __this->get_raycastMask_4();
		int32_t L_5 = LayerMask_op_Implicit_m3296792737(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		LayerMask_t3493934918  L_6 = LayerMask_op_Implicit_m90232283(NULL /*static, unused*/, ((~L_5)), /*hidden argument*/NULL);
		__this->set_raycastMask_4(L_6);
		Transform_t3600365921 * L_7 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_8 = Transform_get_rotation_m3502953881(L_7, /*hidden argument*/NULL);
		__this->set_newRotation_5(L_8);
		return;
	}
}
// System.Void EnemyPatrol::FixedUpdate()
extern "C"  void EnemyPatrol_FixedUpdate_m293570722 (EnemyPatrol_t3346945880 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyPatrol_FixedUpdate_m293570722_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_1 = Transform_get_rotation_m3502953881(L_0, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_2 = __this->get_newRotation_5();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		float L_3 = Quaternion_Angle_m1586774072(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if ((!(((float)L_3) > ((float)(1.0f)))))
		{
			goto IL_004b;
		}
	}
	{
		Transform_t3600365921 * L_4 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_5 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_6 = Transform_get_rotation_m3502953881(L_5, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_7 = __this->get_newRotation_5();
		float L_8 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_9 = Quaternion_Slerp_m1234055455(NULL /*static, unused*/, L_6, L_7, L_8, /*hidden argument*/NULL);
		Transform_set_rotation_m3524318132(L_4, L_9, /*hidden argument*/NULL);
		goto IL_0051;
	}

IL_004b:
	{
		EnemyPatrol_PatrolRaycast_m262090287(__this, /*hidden argument*/NULL);
	}

IL_0051:
	{
		bool L_10 = __this->get_bStopAndRotate_6();
		if (L_10)
		{
			goto IL_007d;
		}
	}
	{
		Transform_t3600365921 * L_11 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		float L_12 = __this->get_patrolSpeed_2();
		float L_13 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_Translate_m3762500149(L_11, (0.0f), (0.0f), ((float)il2cpp_codegen_multiply((float)L_12, (float)L_13)), /*hidden argument*/NULL);
	}

IL_007d:
	{
		return;
	}
}
// System.Void EnemyPatrol::PatrolRaycast()
extern "C"  void EnemyPatrol_PatrolRaycast_m262090287 (EnemyPatrol_t3346945880 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyPatrol_PatrolRaycast_m262090287_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RaycastHit_t1056001966  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		__this->set_bStopAndRotate_6((bool)0);
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_1 = Transform_get_rotation_m3502953881(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Vector3_get_forward_m3100859705(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Quaternion_op_Multiply_m2607404835(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Transform_t3600365921 * L_4 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_5 = Transform_get_position_m36019626(L_4, /*hidden argument*/NULL);
		Vector3_t3722313464  L_6 = V_0;
		float L_7 = __this->get_detectXDistance_3();
		LayerMask_t3493934918  L_8 = __this->get_raycastMask_4();
		int32_t L_9 = LayerMask_op_Implicit_m3296792737(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		bool L_10 = Physics_Raycast_m4145022031(NULL /*static, unused*/, L_5, L_6, (&V_1), L_7, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0091;
		}
	}
	{
		Transform_t3600365921 * L_11 = RaycastHit_get_transform_m942054759((&V_1), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_12 = Component_get_gameObject_m442555142(L_11, /*hidden argument*/NULL);
		bool L_13 = GameObject_CompareTag_m3144439756(L_12, _stringLiteral2261822918, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0078;
		}
	}
	{
		Transform_t3600365921 * L_14 = RaycastHit_get_transform_m942054759((&V_1), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_15 = Component_get_gameObject_m442555142(L_14, /*hidden argument*/NULL);
		EnemyPatrol_ChasePlayer_m1683778077(__this, L_15, /*hidden argument*/NULL);
		goto IL_008c;
	}

IL_0078:
	{
		__this->set_bStopAndRotate_6((bool)1);
		Vector3_t3722313464  L_16 = RaycastHit_get_point_m2236647085((&V_1), /*hidden argument*/NULL);
		EnemyPatrol_TurnAround_m3579663584(__this, L_16, /*hidden argument*/NULL);
	}

IL_008c:
	{
		goto IL_00a2;
	}

IL_0091:
	{
		Transform_t3600365921 * L_17 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_18 = Transform_get_rotation_m3502953881(L_17, /*hidden argument*/NULL);
		__this->set_newRotation_5(L_18);
	}

IL_00a2:
	{
		return;
	}
}
// System.Void EnemyPatrol::TurnAround(UnityEngine.Vector3)
extern "C"  void EnemyPatrol_TurnAround_m3579663584 (EnemyPatrol_t3346945880 * __this, Vector3_t3722313464  ___hitPoint0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyPatrol_TurnAround_m3579663584_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t2301928331  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		Vector3_t3722313464  L_2 = ___hitPoint0;
		Quaternion_t2301928331  L_3 = Tools_LookAtPlayerOnYAxis_m199382236(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Quaternion_t2301928331  L_4 = V_0;
		Transform_t3600365921 * L_5 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_6 = Transform_get_up_m3972993886(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_7 = Quaternion_AngleAxis_m1767165696(NULL /*static, unused*/, (180.0f), L_6, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_8 = Quaternion_op_Multiply_m1294064023(NULL /*static, unused*/, L_4, L_7, /*hidden argument*/NULL);
		__this->set_newRotation_5(L_8);
		Quaternion_t2301928331  L_9 = __this->get_newRotation_5();
		Transform_t3600365921 * L_10 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_11 = Transform_get_rotation_m3502953881(L_10, /*hidden argument*/NULL);
		bool L_12 = Quaternion_op_Equality_m1582314779(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_006f;
		}
	}
	{
		String_t* L_13 = Object_get_name_m4211327027(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_14 = __this->get_newRotation_5();
		Quaternion_t2301928331  L_15 = L_14;
		RuntimeObject * L_16 = Box(Quaternion_t2301928331_il2cpp_TypeInfo_var, &L_15);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m1715369213(NULL /*static, unused*/, L_13, _stringLiteral2917778654, L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		return;
	}

IL_006f:
	{
		return;
	}
}
// System.Void EnemyPatrol::InteractiveWithPlayer(UnityEngine.GameObject)
extern "C"  void EnemyPatrol_InteractiveWithPlayer_m915949764 (EnemyPatrol_t3346945880 * __this, GameObject_t1113636619 * ___player0, const RuntimeMethod* method)
{
	{
		GameObject_t1113636619 * L_0 = ___player0;
		EnemyPatrol_ChasePlayer_m1683778077(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnemyPatrol::ChasePlayer(UnityEngine.GameObject)
extern "C"  void EnemyPatrol_ChasePlayer_m1683778077 (EnemyPatrol_t3346945880 * __this, GameObject_t1113636619 * ___player0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyPatrol_ChasePlayer_m1683778077_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = ___player0;
		Transform_t3600365921 * L_3 = GameObject_get_transform_m1369836730(L_2, /*hidden argument*/NULL);
		Vector3_t3722313464  L_4 = Transform_get_position_m36019626(L_3, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_5 = Tools_LookAtPlayerOnYAxis_m199382236(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		__this->set_newRotation_5(L_5);
		String_t* L_6 = Object_get_name_m4211327027(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_7 = __this->get_newRotation_5();
		Quaternion_t2301928331  L_8 = L_7;
		RuntimeObject * L_9 = Box(Quaternion_t2301928331_il2cpp_TypeInfo_var, &L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1715369213(NULL /*static, unused*/, L_6, _stringLiteral2705378347, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Entity::.ctor()
extern "C"  void Entity__ctor_m643651842 (Entity_t3391956725 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Entity::get_ID()
extern "C"  int32_t Entity_get_ID_m2600501175 (Entity_t3391956725 * __this, const RuntimeMethod* method)
{
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_0);
		return L_1;
	}
}
// System.Void Entity::Start()
extern "C"  void Entity_Start_m2858852706 (Entity_t3391956725 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Entity::Update()
extern "C"  void Entity_Update_m1477389504 (Entity_t3391956725 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GameCamera::.ctor()
extern "C"  void GameCamera__ctor_m57809962 (GameCamera_t2564829307 * __this, const RuntimeMethod* method)
{
	{
		__this->set_interpolationTime_7((0.25f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform GameCamera::get_target()
extern "C"  Transform_t3600365921 * GameCamera_get_target_m1194538123 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCamera_get_target_m1194538123_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3600365921 * G_B3_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		List_1_t777473367 * L_0 = ((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->get_mTargets_2();
		int32_t L_1 = List_1_get_Count_m3787308655(L_0, /*hidden argument*/List_1_get_Count_m3787308655_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		G_B3_0 = ((Transform_t3600365921 *)(NULL));
		goto IL_002b;
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		List_1_t777473367 * L_2 = ((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->get_mTargets_2();
		List_1_t777473367 * L_3 = ((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->get_mTargets_2();
		int32_t L_4 = List_1_get_Count_m3787308655(L_3, /*hidden argument*/List_1_get_Count_m3787308655_RuntimeMethod_var);
		Transform_t3600365921 * L_5 = List_1_get_Item_m3022113929(L_2, ((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)1)), /*hidden argument*/List_1_get_Item_m3022113929_RuntimeMethod_var);
		G_B3_0 = L_5;
	}

IL_002b:
	{
		return G_B3_0;
	}
}
// System.Void GameCamera::set_target(UnityEngine.Transform)
extern "C"  void GameCamera_set_target_m2254130448 (RuntimeObject * __this /* static, unused */, Transform_t3600365921 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCamera_set_target_m2254130448_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		List_1_t777473367 * L_0 = ((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->get_mTargets_2();
		List_1_Clear_m3082658015(L_0, /*hidden argument*/List_1_Clear_m3082658015_RuntimeMethod_var);
		Transform_t3600365921 * L_1 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		List_1_t777473367 * L_3 = ((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->get_mTargets_2();
		Transform_t3600365921 * L_4 = ___value0;
		List_1_Add_m4073477735(L_3, L_4, /*hidden argument*/List_1_Add_m4073477735_RuntimeMethod_var);
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->set_mAlpha_3((0.0f));
		return;
	}
}
// System.Void GameCamera::AddTarget(UnityEngine.Transform)
extern "C"  void GameCamera_AddTarget_m745210319 (RuntimeObject * __this /* static, unused */, Transform_t3600365921 * ___t0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCamera_AddTarget_m745210319_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = ___t0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		List_1_t777473367 * L_2 = ((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->get_mTargets_2();
		Transform_t3600365921 * L_3 = ___t0;
		List_1_Remove_m4216972(L_2, L_3, /*hidden argument*/List_1_Remove_m4216972_RuntimeMethod_var);
		List_1_t777473367 * L_4 = ((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->get_mTargets_2();
		Transform_t3600365921 * L_5 = ___t0;
		List_1_Add_m4073477735(L_4, L_5, /*hidden argument*/List_1_Add_m4073477735_RuntimeMethod_var);
		((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->set_mAlpha_3((0.0f));
	}

IL_002d:
	{
		return;
	}
}
// System.Void GameCamera::RemoveTarget(UnityEngine.Transform)
extern "C"  void GameCamera_RemoveTarget_m3068264324 (RuntimeObject * __this /* static, unused */, Transform_t3600365921 * ___t0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCamera_RemoveTarget_m3068264324_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = ___t0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		Transform_t3600365921 * L_2 = GameCamera_get_target_m1194538123(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3600365921 * L_3 = ___t0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->set_mAlpha_3((0.0f));
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		List_1_t777473367 * L_5 = ((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->get_mTargets_2();
		Transform_t3600365921 * L_6 = ___t0;
		List_1_Remove_m4216972(L_5, L_6, /*hidden argument*/List_1_Remove_m4216972_RuntimeMethod_var);
	}

IL_0032:
	{
		return;
	}
}
// System.Void GameCamera::DetachFromParent()
extern "C"  void GameCamera_DetachFromParent_m1579485582 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCamera_DetachFromParent_m1579485582_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		GameCamera_t2564829307 * L_0 = ((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->get_mInstance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		GameCamera_t2564829307 * L_2 = ((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->get_mInstance_4();
		Transform_t3600365921 * L_3 = L_2->get_mTrans_8();
		Transform_t3600365921 * L_4 = Transform_get_parent_m835071599(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		GameCamera_t2564829307 * L_6 = ((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->get_mInstance_4();
		Transform_t3600365921 * L_7 = L_6->get_mTrans_8();
		Transform_set_parent_m786917804(L_7, (Transform_t3600365921 *)NULL, /*hidden argument*/NULL);
	}

IL_003a:
	{
		return;
	}
}
// System.Boolean GameCamera::DetachFromParent(UnityEngine.Transform)
extern "C"  bool GameCamera_DetachFromParent_m564670588 (RuntimeObject * __this /* static, unused */, Transform_t3600365921 * ___t0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCamera_DetachFromParent_m564670588_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		GameCamera_t2564829307 * L_0 = ((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->get_mInstance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0037;
		}
	}
	{
		Transform_t3600365921 * L_2 = ___t0;
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		GameCamera_t2564829307 * L_3 = ((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->get_mInstance_4();
		Transform_t3600365921 * L_4 = L_3->get_mTrans_8();
		bool L_5 = Tools_IsChild_m3091374981(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0037;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		GameCamera_t2564829307 * L_6 = ((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->get_mInstance_4();
		Transform_t3600365921 * L_7 = L_6->get_mTrans_8();
		Transform_set_parent_m786917804(L_7, (Transform_t3600365921 *)NULL, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0037:
	{
		return (bool)0;
	}
}
// System.Void GameCamera::Awake()
extern "C"  void GameCamera_Awake_m4052600003 (GameCamera_t2564829307 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCamera_Awake_m4052600003_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->set_mInstance_4(__this);
		return;
	}
}
// System.Void GameCamera::OnDestroy()
extern "C"  void GameCamera_OnDestroy_m72905221 (GameCamera_t2564829307 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCamera_OnDestroy_m72905221_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->set_mInstance_4((GameCamera_t2564829307 *)NULL);
		return;
	}
}
// System.Void GameCamera::Start()
extern "C"  void GameCamera_Start_m2841233292 (GameCamera_t2564829307 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		__this->set_mTrans_8(L_0);
		return;
	}
}
// System.Void GameCamera::LateUpdate()
extern "C"  void GameCamera_LateUpdate_m3934829490 (GameCamera_t2564829307 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCamera_LateUpdate_m3934829490_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3600365921 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		Transform_t3600365921 * L_0 = GameCamera_get_target_m1194538123(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Transform_t3600365921 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Transform_t3600365921 * L_3 = __this->get_mTrans_8();
		Transform_set_parent_m786917804(L_3, (Transform_t3600365921 *)NULL, /*hidden argument*/NULL);
		goto IL_0129;
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		float L_4 = ((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->get_mAlpha_3();
		if ((!(((float)L_4) < ((float)(1.0f)))))
		{
			goto IL_0129;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		float L_5 = ((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->get_mAlpha_3();
		if ((!(((float)L_5) == ((float)(0.0f)))))
		{
			goto IL_006f;
		}
	}
	{
		Transform_t3600365921 * L_6 = __this->get_mTrans_8();
		Transform_set_parent_m786917804(L_6, (Transform_t3600365921 *)NULL, /*hidden argument*/NULL);
		Transform_t3600365921 * L_7 = __this->get_mTrans_8();
		Vector3_t3722313464  L_8 = Transform_get_position_m36019626(L_7, /*hidden argument*/NULL);
		__this->set_mPos_9(L_8);
		Transform_t3600365921 * L_9 = __this->get_mTrans_8();
		Quaternion_t2301928331  L_10 = Transform_get_rotation_m3502953881(L_9, /*hidden argument*/NULL);
		__this->set_mRot_10(L_10);
	}

IL_006f:
	{
		float L_11 = __this->get_interpolationTime_7();
		if ((!(((float)L_11) > ((float)(0.0f)))))
		{
			goto IL_009b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		float L_12 = ((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->get_mAlpha_3();
		float L_13 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_14 = __this->get_interpolationTime_7();
		((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->set_mAlpha_3(((float)il2cpp_codegen_add((float)L_12, (float)((float)((float)L_13/(float)L_14)))));
		goto IL_00a5;
	}

IL_009b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->set_mAlpha_3((1.0f));
	}

IL_00a5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		float L_15 = ((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->get_mAlpha_3();
		if ((!(((float)L_15) < ((float)(1.0f)))))
		{
			goto IL_00fb;
		}
	}
	{
		Transform_t3600365921 * L_16 = __this->get_mTrans_8();
		Vector3_t3722313464  L_17 = __this->get_mPos_9();
		Transform_t3600365921 * L_18 = V_0;
		Vector3_t3722313464  L_19 = Transform_get_position_m36019626(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		float L_20 = ((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->get_mAlpha_3();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_21 = Vector3_Lerp_m407887542(NULL /*static, unused*/, L_17, L_19, L_20, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_16, L_21, /*hidden argument*/NULL);
		Transform_t3600365921 * L_22 = __this->get_mTrans_8();
		Quaternion_t2301928331  L_23 = __this->get_mRot_10();
		Transform_t3600365921 * L_24 = V_0;
		Quaternion_t2301928331  L_25 = Transform_get_rotation_m3502953881(L_24, /*hidden argument*/NULL);
		float L_26 = ((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->get_mAlpha_3();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_27 = Quaternion_Slerp_m1234055455(NULL /*static, unused*/, L_23, L_25, L_26, /*hidden argument*/NULL);
		Transform_set_rotation_m3524318132(L_22, L_27, /*hidden argument*/NULL);
		goto IL_0129;
	}

IL_00fb:
	{
		Transform_t3600365921 * L_28 = __this->get_mTrans_8();
		Transform_t3600365921 * L_29 = V_0;
		Transform_set_parent_m786917804(L_28, L_29, /*hidden argument*/NULL);
		Transform_t3600365921 * L_30 = __this->get_mTrans_8();
		Transform_t3600365921 * L_31 = V_0;
		Vector3_t3722313464  L_32 = Transform_get_position_m36019626(L_31, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_30, L_32, /*hidden argument*/NULL);
		Transform_t3600365921 * L_33 = __this->get_mTrans_8();
		Transform_t3600365921 * L_34 = V_0;
		Quaternion_t2301928331  L_35 = Transform_get_rotation_m3502953881(L_34, /*hidden argument*/NULL);
		Transform_set_rotation_m3524318132(L_33, L_35, /*hidden argument*/NULL);
	}

IL_0129:
	{
		Transform_t3600365921 * L_36 = __this->get_mTrans_8();
		Quaternion_t2301928331  L_37 = Transform_get_rotation_m3502953881(L_36, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_38 = Vector3_get_forward_m3100859705(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_39 = Quaternion_op_Multiply_m2607404835(NULL /*static, unused*/, L_37, L_38, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->set_direction_5(L_39);
		Vector3_t3722313464  L_40 = ((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->get_direction_5();
		((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->set_flatDirection_6(L_40);
		(((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->get_address_of_flatDirection_6())->set_y_2((0.0f));
		Vector3_Normalize_m914904454((((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->get_address_of_flatDirection_6()), /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameCamera::.cctor()
extern "C"  void GameCamera__cctor_m2694908189 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCamera__cctor_m2694908189_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t777473367 * L_0 = (List_1_t777473367 *)il2cpp_codegen_object_new(List_1_t777473367_il2cpp_TypeInfo_var);
		List_1__ctor_m2885667311(L_0, /*hidden argument*/List_1__ctor_m2885667311_RuntimeMethod_var);
		((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->set_mTargets_2(L_0);
		((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->set_mInstance_4((GameCamera_t2564829307 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_1 = Vector3_get_forward_m3100859705(NULL /*static, unused*/, /*hidden argument*/NULL);
		((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->set_direction_5(L_1);
		Vector3_t3722313464  L_2 = Vector3_get_forward_m3100859705(NULL /*static, unused*/, /*hidden argument*/NULL);
		((GameCamera_t2564829307_StaticFields*)il2cpp_codegen_static_fields_for(GameCamera_t2564829307_il2cpp_TypeInfo_var))->set_flatDirection_6(L_2);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GameCameraTarget::.ctor()
extern "C"  void GameCameraTarget__ctor_m506069389 (GameCameraTarget_t3267842245 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameCameraTarget::Activate(System.Boolean)
extern "C"  void GameCameraTarget_Activate_m1421886454 (GameCameraTarget_t3267842245 * __this, bool ___val0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCameraTarget_Activate_m1421886454_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___val0;
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		GameCamera_AddTarget_m745210319(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		goto IL_0021;
	}

IL_0016:
	{
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameCamera_t2564829307_il2cpp_TypeInfo_var);
		GameCamera_RemoveTarget_m3068264324(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void GameCameraTarget::OnEnable()
extern "C"  void GameCameraTarget_OnEnable_m2902021404 (GameCameraTarget_t3267842245 * __this, const RuntimeMethod* method)
{
	{
		GameCameraTarget_Activate_m1421886454(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameCameraTarget::OnDisable()
extern "C"  void GameCameraTarget_OnDisable_m1977620125 (GameCameraTarget_t3267842245 * __this, const RuntimeMethod* method)
{
	{
		GameCameraTarget_Activate_m1421886454(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GameScene::.ctor()
extern "C"  void GameScene__ctor_m3327038984 (GameScene_t4110668666 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameScene__ctor_m3327038984_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_0 = GameObject_FindWithTag_m981614592(NULL /*static, unused*/, _stringLiteral1768119585, /*hidden argument*/NULL);
		__this->set_m_terrainGO_1(L_0);
		GameObject_t1113636619 * L_1 = GameObject_FindWithTag_m981614592(NULL /*static, unused*/, _stringLiteral78692563, /*hidden argument*/NULL);
		__this->set_m_waterGO_0(L_1);
		GameObject_t1113636619 * L_2 = GameObject_FindGameObjectWithTag_m2129039296(NULL /*static, unused*/, _stringLiteral1926007183, /*hidden argument*/NULL);
		__this->set_m_mainCamera_2(L_2);
		GameObject_t1113636619 * L_3 = GameObject_FindGameObjectWithTag_m2129039296(NULL /*static, unused*/, _stringLiteral2261822918, /*hidden argument*/NULL);
		__this->set_m_player_3(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Highlightable::.ctor()
extern "C"  void Highlightable__ctor_m864511538 (Highlightable_t3139352672 * __this, const RuntimeMethod* method)
{
	{
		Color_t2555686324  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m2943235014((&L_0), (1.5f), (1.5f), (1.5f), (1.0f), /*hidden argument*/NULL);
		__this->set_mTargetColor_3(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Highlightable::Start()
extern "C"  void Highlightable_Start_m867417398 (Highlightable_t3139352672 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Highlightable_Start_m867417398_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Renderer_t2627027031 * L_0 = Component_GetComponent_TisRenderer_t2627027031_m2651633905(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m2651633905_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		goto IL_0032;
	}

IL_001c:
	{
		Renderer_t2627027031 * L_2 = Component_GetComponent_TisRenderer_t2627027031_m2651633905(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m2651633905_RuntimeMethod_var);
		Material_t340375123 * L_3 = Renderer_get_material_m4171603682(L_2, /*hidden argument*/NULL);
		Color_t2555686324  L_4 = Material_get_color_m3827673574(L_3, /*hidden argument*/NULL);
		__this->set_mColor_2(L_4);
	}

IL_0032:
	{
		return;
	}
}
// System.Void Highlightable::OnMouseEnter()
extern "C"  void Highlightable_OnMouseEnter_m1835650037 (Highlightable_t3139352672 * __this, const RuntimeMethod* method)
{
	{
		__this->set_mHighlight_4((bool)1);
		return;
	}
}
// System.Void Highlightable::OnMouseExit()
extern "C"  void Highlightable_OnMouseExit_m2364943698 (Highlightable_t3139352672 * __this, const RuntimeMethod* method)
{
	{
		__this->set_mHighlight_4((bool)0);
		return;
	}
}
// System.Void Highlightable::Update()
extern "C"  void Highlightable_Update_m828541039 (Highlightable_t3139352672 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Highlightable_Update_m828541039_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float G_B6_0 = 0.0f;
	{
		bool L_0 = __this->get_mHighlight_4();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		bool L_1 = __this->get_mModified_5();
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		float L_2 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Min_m1073399594(NULL /*static, unused*/, (1.0f), ((float)il2cpp_codegen_multiply((float)L_2, (float)(10.0f))), /*hidden argument*/NULL);
		V_0 = L_3;
		bool L_4 = __this->get_mHighlight_4();
		if (!L_4)
		{
			goto IL_0042;
		}
	}
	{
		G_B6_0 = (1.0f);
		goto IL_0047;
	}

IL_0042:
	{
		G_B6_0 = (0.0f);
	}

IL_0047:
	{
		V_1 = G_B6_0;
		float L_5 = __this->get_mAlpha_6();
		float L_6 = V_1;
		float L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_8 = Mathf_Lerp_m1004423579(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		__this->set_mAlpha_6(L_8);
		bool L_9 = __this->get_mHighlight_4();
		if (L_9)
		{
			goto IL_0098;
		}
	}
	{
		float L_10 = __this->get_mAlpha_6();
		if ((!(((float)L_10) < ((float)(0.001f)))))
		{
			goto IL_0098;
		}
	}
	{
		__this->set_mModified_5((bool)0);
		Renderer_t2627027031 * L_11 = Component_GetComponent_TisRenderer_t2627027031_m2651633905(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m2651633905_RuntimeMethod_var);
		Material_t340375123 * L_12 = Renderer_get_material_m4171603682(L_11, /*hidden argument*/NULL);
		Color_t2555686324  L_13 = __this->get_mColor_2();
		Material_set_color_m1794818007(L_12, L_13, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_0098:
	{
		__this->set_mModified_5((bool)1);
		Renderer_t2627027031 * L_14 = Component_GetComponent_TisRenderer_t2627027031_m2651633905(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m2651633905_RuntimeMethod_var);
		Material_t340375123 * L_15 = Renderer_get_material_m4171603682(L_14, /*hidden argument*/NULL);
		Color_t2555686324  L_16 = __this->get_mColor_2();
		Color_t2555686324  L_17 = __this->get_mTargetColor_3();
		float L_18 = __this->get_mAlpha_6();
		Color_t2555686324  L_19 = Color_Lerp_m973389909(NULL /*static, unused*/, L_16, L_17, L_18, /*hidden argument*/NULL);
		Material_set_color_m1794818007(L_15, L_19, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		return;
	}
}
// System.Void Highlightable::SetHighlight(System.Boolean)
extern "C"  void Highlightable_SetHighlight_m1780462549 (Highlightable_t3139352672 * __this, bool ___highlight0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___highlight0;
		__this->set_mHighlight_4(L_0);
		return;
	}
}
// System.Void Highlightable::SetAllChildrenHighlightable(System.Boolean)
extern "C"  void Highlightable_SetAllChildrenHighlightable_m544031526 (Highlightable_t3139352672 * __this, bool ___highlight0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Highlightable_SetAllChildrenHighlightable_m544031526_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Renderer_t2627027031 * V_0 = NULL;
	RendererU5BU5D_t3210418286* V_1 = NULL;
	int32_t V_2 = 0;
	Highlightable_t3139352672 * V_3 = NULL;
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		RendererU5BU5D_t3210418286* L_1 = GameObject_GetComponentsInChildren_TisRenderer_t2627027031_m2275554434(L_0, /*hidden argument*/GameObject_GetComponentsInChildren_TisRenderer_t2627027031_m2275554434_RuntimeMethod_var);
		__this->set_mChildrenRenderers_7(L_1);
		RendererU5BU5D_t3210418286* L_2 = __this->get_mChildrenRenderers_7();
		if (!L_2)
		{
			goto IL_0061;
		}
	}
	{
		RendererU5BU5D_t3210418286* L_3 = __this->get_mChildrenRenderers_7();
		V_1 = L_3;
		V_2 = 0;
		goto IL_0058;
	}

IL_002a:
	{
		RendererU5BU5D_t3210418286* L_4 = V_1;
		int32_t L_5 = V_2;
		int32_t L_6 = L_5;
		Renderer_t2627027031 * L_7 = (L_4)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_6));
		V_0 = L_7;
		Highlightable_t3139352672 * L_8 = Component_GetComponent_TisHighlightable_t3139352672_m3233271036(__this, /*hidden argument*/Component_GetComponent_TisHighlightable_t3139352672_m3233271036_RuntimeMethod_var);
		V_3 = L_8;
		Highlightable_t3139352672 * L_9 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_9, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_004d;
		}
	}
	{
		Renderer_t2627027031 * L_11 = V_0;
		GameObject_t1113636619 * L_12 = Component_get_gameObject_m442555142(L_11, /*hidden argument*/NULL);
		Highlightable_t3139352672 * L_13 = GameObject_AddComponent_TisHighlightable_t3139352672_m2047509732(L_12, /*hidden argument*/GameObject_AddComponent_TisHighlightable_t3139352672_m2047509732_RuntimeMethod_var);
		V_3 = L_13;
	}

IL_004d:
	{
		Highlightable_t3139352672 * L_14 = V_3;
		bool L_15 = ___highlight0;
		Highlightable_SetHighlight_m1780462549(L_14, L_15, /*hidden argument*/NULL);
		int32_t L_16 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)1));
	}

IL_0058:
	{
		int32_t L_17 = V_2;
		RendererU5BU5D_t3210418286* L_18 = V_1;
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_18)->max_length)))))))
		{
			goto IL_002a;
		}
	}

IL_0061:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void HUDFPS::.ctor()
extern "C"  void HUDFPS__ctor_m4116254002 (HUDFPS_t4241874542 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HUDFPS__ctor_m4116254002_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rect_t2360479859  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Rect__ctor_m2614021312((&L_0), (10.0f), (10.0f), (75.0f), (50.0f), /*hidden argument*/NULL);
		__this->set_startRect_2(L_0);
		__this->set_updateColor_3((bool)1);
		__this->set_allowDrag_4((bool)1);
		__this->set_frequency_5((0.5f));
		__this->set_nbDecimal_6(1);
		Color_t2555686324  L_1 = Color_get_white_m332174077(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_color_9(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		__this->set_sFPS_10(L_2);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HUDFPS::Start()
extern "C"  void HUDFPS_Start_m1975495016 (HUDFPS_t4241874542 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = HUDFPS_FPS_m1656083209(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HUDFPS::Update()
extern "C"  void HUDFPS_Update_m1118315064 (HUDFPS_t4241874542 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_accum_7();
		float L_1 = Time_get_timeScale_m701790074(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_accum_7(((float)il2cpp_codegen_add((float)L_0, (float)((float)((float)L_1/(float)L_2)))));
		int32_t L_3 = __this->get_frames_8();
		__this->set_frames_8(((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1)));
		return;
	}
}
// System.Collections.IEnumerator HUDFPS::FPS()
extern "C"  RuntimeObject* HUDFPS_FPS_m1656083209 (HUDFPS_t4241874542 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HUDFPS_FPS_m1656083209_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CFPSU3Ec__Iterator0_t392322613 * V_0 = NULL;
	{
		U3CFPSU3Ec__Iterator0_t392322613 * L_0 = (U3CFPSU3Ec__Iterator0_t392322613 *)il2cpp_codegen_object_new(U3CFPSU3Ec__Iterator0_t392322613_il2cpp_TypeInfo_var);
		U3CFPSU3Ec__Iterator0__ctor_m594114655(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFPSU3Ec__Iterator0_t392322613 * L_1 = V_0;
		L_1->set_U24this_1(__this);
		U3CFPSU3Ec__Iterator0_t392322613 * L_2 = V_0;
		return L_2;
	}
}
// System.Void HUDFPS::OnGUI()
extern "C"  void HUDFPS_OnGUI_m1014030805 (HUDFPS_t4241874542 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HUDFPS_OnGUI_m1014030805_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2555686324  G_B5_0;
	memset(&G_B5_0, 0, sizeof(G_B5_0));
	{
		GUIStyle_t3956901511 * L_0 = __this->get_style_11();
		if (L_0)
		{
			goto IL_0041;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUISkin_t1244372282 * L_1 = GUI_get_skin_m1874615010(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_2 = GUISkin_get_label_m1693050720(L_1, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_3 = (GUIStyle_t3956901511 *)il2cpp_codegen_object_new(GUIStyle_t3956901511_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2912682974(L_3, L_2, /*hidden argument*/NULL);
		__this->set_style_11(L_3);
		GUIStyle_t3956901511 * L_4 = __this->get_style_11();
		GUIStyleState_t1397964415 * L_5 = GUIStyle_get_normal_m729441812(L_4, /*hidden argument*/NULL);
		Color_t2555686324  L_6 = Color_get_white_m332174077(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIStyleState_set_textColor_m1105876047(L_5, L_6, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_7 = __this->get_style_11();
		GUIStyle_set_alignment_m3944619660(L_7, 4, /*hidden argument*/NULL);
	}

IL_0041:
	{
		bool L_8 = __this->get_updateColor_3();
		if (!L_8)
		{
			goto IL_0057;
		}
	}
	{
		Color_t2555686324  L_9 = __this->get_color_9();
		G_B5_0 = L_9;
		goto IL_005c;
	}

IL_0057:
	{
		Color_t2555686324  L_10 = Color_get_white_m332174077(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B5_0 = L_10;
	}

IL_005c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_set_color_m1028198571(NULL /*static, unused*/, G_B5_0, /*hidden argument*/NULL);
		Rect_t2360479859  L_11 = __this->get_startRect_2();
		intptr_t L_12 = (intptr_t)HUDFPS_DoMyWindow_m1030748063_RuntimeMethod_var;
		WindowFunction_t3146511083 * L_13 = (WindowFunction_t3146511083 *)il2cpp_codegen_object_new(WindowFunction_t3146511083_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m2544237635(L_13, __this, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		Rect_t2360479859  L_15 = GUI_Window_m1088326791(NULL /*static, unused*/, 0, L_11, L_13, L_14, /*hidden argument*/NULL);
		__this->set_startRect_2(L_15);
		return;
	}
}
// System.Void HUDFPS::DoMyWindow(System.Int32)
extern "C"  void HUDFPS_DoMyWindow_m1030748063 (HUDFPS_t4241874542 * __this, int32_t ___windowID0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HUDFPS_DoMyWindow_m1030748063_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rect_t2360479859 * L_0 = __this->get_address_of_startRect_2();
		float L_1 = Rect_get_width_m3421484486(L_0, /*hidden argument*/NULL);
		Rect_t2360479859 * L_2 = __this->get_address_of_startRect_2();
		float L_3 = Rect_get_height_m1358425599(L_2, /*hidden argument*/NULL);
		Rect_t2360479859  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Rect__ctor_m2614021312((&L_4), (0.0f), (0.0f), L_1, L_3, /*hidden argument*/NULL);
		String_t* L_5 = __this->get_sFPS_10();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m3937257545(NULL /*static, unused*/, L_5, _stringLiteral2787286266, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_7 = __this->get_style_11();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_Label_m2420537077(NULL /*static, unused*/, L_4, L_6, L_7, /*hidden argument*/NULL);
		bool L_8 = __this->get_allowDrag_4();
		if (!L_8)
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_9 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_10 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t2360479859  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Rect__ctor_m2614021312((&L_11), (0.0f), (0.0f), (((float)((float)L_9))), (((float)((float)L_10))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_DragWindow_m3406543436(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
	}

IL_006b:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void HUDFPS/<FPS>c__Iterator0::.ctor()
extern "C"  void U3CFPSU3Ec__Iterator0__ctor_m594114655 (U3CFPSU3Ec__Iterator0_t392322613 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean HUDFPS/<FPS>c__Iterator0::MoveNext()
extern "C"  bool U3CFPSU3Ec__Iterator0_MoveNext_m3141457867 (U3CFPSU3Ec__Iterator0_t392322613 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CFPSU3Ec__Iterator0_MoveNext_m3141457867_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	HUDFPS_t4241874542 * G_B4_0 = NULL;
	HUDFPS_t4241874542 * G_B3_0 = NULL;
	Color_t2555686324  G_B7_0;
	memset(&G_B7_0, 0, sizeof(G_B7_0));
	HUDFPS_t4241874542 * G_B7_1 = NULL;
	HUDFPS_t4241874542 * G_B6_0 = NULL;
	HUDFPS_t4241874542 * G_B5_0 = NULL;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0101;
			}
		}
	}
	{
		goto IL_010d;
	}

IL_0021:
	{
		HUDFPS_t4241874542 * L_2 = __this->get_U24this_1();
		float L_3 = L_2->get_accum_7();
		HUDFPS_t4241874542 * L_4 = __this->get_U24this_1();
		int32_t L_5 = L_4->get_frames_8();
		__this->set_U3CfpsU3E__1_0(((float)((float)L_3/(float)(((float)((float)L_5))))));
		HUDFPS_t4241874542 * L_6 = __this->get_U24this_1();
		float* L_7 = __this->get_address_of_U3CfpsU3E__1_0();
		HUDFPS_t4241874542 * L_8 = __this->get_U24this_1();
		int32_t L_9 = L_8->get_nbDecimal_6();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_10 = Mathf_Clamp_m2756574208(NULL /*static, unused*/, L_9, 0, ((int32_t)10), /*hidden argument*/NULL);
		int32_t L_11 = L_10;
		RuntimeObject * L_12 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral3452614586, L_12, /*hidden argument*/NULL);
		String_t* L_14 = Single_ToString_m3489843083(L_7, L_13, /*hidden argument*/NULL);
		L_6->set_sFPS_10(L_14);
		HUDFPS_t4241874542 * L_15 = __this->get_U24this_1();
		float L_16 = __this->get_U3CfpsU3E__1_0();
		G_B3_0 = L_15;
		if ((!(((float)L_16) >= ((float)(30.0f)))))
		{
			G_B4_0 = L_15;
			goto IL_0097;
		}
	}
	{
		Color_t2555686324  L_17 = Color_get_green_m490390750(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B7_0 = L_17;
		G_B7_1 = G_B3_0;
		goto IL_00b6;
	}

IL_0097:
	{
		float L_18 = __this->get_U3CfpsU3E__1_0();
		G_B5_0 = G_B4_0;
		if ((!(((float)L_18) > ((float)(10.0f)))))
		{
			G_B6_0 = G_B4_0;
			goto IL_00b1;
		}
	}
	{
		Color_t2555686324  L_19 = Color_get_red_m3227813939(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B7_0 = L_19;
		G_B7_1 = G_B5_0;
		goto IL_00b6;
	}

IL_00b1:
	{
		Color_t2555686324  L_20 = Color_get_yellow_m1287957903(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B7_0 = L_20;
		G_B7_1 = G_B6_0;
	}

IL_00b6:
	{
		G_B7_1->set_color_9(G_B7_0);
		HUDFPS_t4241874542 * L_21 = __this->get_U24this_1();
		L_21->set_accum_7((0.0f));
		HUDFPS_t4241874542 * L_22 = __this->get_U24this_1();
		L_22->set_frames_8(0);
		HUDFPS_t4241874542 * L_23 = __this->get_U24this_1();
		float L_24 = L_23->get_frequency_5();
		WaitForSeconds_t1699091251 * L_25 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_25, L_24, /*hidden argument*/NULL);
		__this->set_U24current_2(L_25);
		bool L_26 = __this->get_U24disposing_3();
		if (L_26)
		{
			goto IL_00fc;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_00fc:
	{
		goto IL_010f;
	}

IL_0101:
	{
		goto IL_0021;
	}
	// Dead block : IL_0106: ldarg.0

IL_010d:
	{
		return (bool)0;
	}

IL_010f:
	{
		return (bool)1;
	}
}
// System.Object HUDFPS/<FPS>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CFPSU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2909785216 (U3CFPSU3Ec__Iterator0_t392322613 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object HUDFPS/<FPS>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CFPSU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1064310338 (U3CFPSU3Ec__Iterator0_t392322613 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void HUDFPS/<FPS>c__Iterator0::Dispose()
extern "C"  void U3CFPSU3Ec__Iterator0_Dispose_m1397676539 (U3CFPSU3Ec__Iterator0_t392322613 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void HUDFPS/<FPS>c__Iterator0::Reset()
extern "C"  void U3CFPSU3Ec__Iterator0_Reset_m487481608 (U3CFPSU3Ec__Iterator0_t392322613 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CFPSU3Ec__Iterator0_Reset_m487481608_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void HumanUnit::.ctor()
extern "C"  void HumanUnit__ctor_m1823562469 (HumanUnit_t2125463837 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HumanUnit__ctor_m1823562469_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector2_t2156229523  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m3970636864((&L_0), (100.0f), (100.0f), /*hidden argument*/NULL);
		__this->set_mHealth_2(L_0);
		LevelExp_t2602383666 * L_1 = (LevelExp_t2602383666 *)il2cpp_codegen_object_new(LevelExp_t2602383666_il2cpp_TypeInfo_var);
		LevelExp__ctor_m598325136(L_1, 0, 0, /*hidden argument*/NULL);
		__this->set_mLevelExp_3(L_1);
		Entity__ctor_m643651842(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HumanUnit::Start()
extern "C"  void HumanUnit_Start_m1296807101 (HumanUnit_t2125463837 * __this, const RuntimeMethod* method)
{
	{
		VirtActionInvoker0::Invoke(4 /* System.Void HumanUnit::InitSkills() */, __this);
		return;
	}
}
// System.Void HumanUnit::Update()
extern "C"  void HumanUnit_Update_m665409056 (HumanUnit_t2125463837 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void HumanUnit::InitSkills()
extern "C"  void HumanUnit_InitSkills_m501231748 (HumanUnit_t2125463837 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LagRotation::.ctor()
extern "C"  void LagRotation__ctor_m951385781 (LagRotation_t3758629971 * __this, const RuntimeMethod* method)
{
	{
		__this->set_speed_2((10.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LagRotation::Start()
extern "C"  void LagRotation_Start_m961694897 (LagRotation_t3758629971 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		__this->set_mTrans_3(L_0);
		Transform_t3600365921 * L_1 = __this->get_mTrans_3();
		Transform_t3600365921 * L_2 = Transform_get_parent_m835071599(L_1, /*hidden argument*/NULL);
		__this->set_mParent_4(L_2);
		Transform_t3600365921 * L_3 = __this->get_mTrans_3();
		Quaternion_t2301928331  L_4 = Transform_get_localRotation_m3487911431(L_3, /*hidden argument*/NULL);
		__this->set_mRelative_5(L_4);
		Transform_t3600365921 * L_5 = __this->get_mParent_4();
		Quaternion_t2301928331  L_6 = Transform_get_rotation_m3502953881(L_5, /*hidden argument*/NULL);
		__this->set_mParentRot_6(L_6);
		return;
	}
}
// System.Void LagRotation::LateUpdate()
extern "C"  void LagRotation_LateUpdate_m1081189382 (LagRotation_t3758629971 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LagRotation_LateUpdate_m1081189382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Quaternion_t2301928331  L_0 = __this->get_mParentRot_6();
		Transform_t3600365921 * L_1 = __this->get_mParent_4();
		Quaternion_t2301928331  L_2 = Transform_get_rotation_m3502953881(L_1, /*hidden argument*/NULL);
		float L_3 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = __this->get_speed_2();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_5 = Quaternion_Slerp_m1234055455(NULL /*static, unused*/, L_0, L_2, ((float)il2cpp_codegen_multiply((float)L_3, (float)L_4)), /*hidden argument*/NULL);
		__this->set_mParentRot_6(L_5);
		Transform_t3600365921 * L_6 = __this->get_mTrans_3();
		Quaternion_t2301928331  L_7 = __this->get_mParentRot_6();
		Quaternion_t2301928331  L_8 = __this->get_mRelative_5();
		Quaternion_t2301928331  L_9 = Quaternion_op_Multiply_m1294064023(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		Transform_set_rotation_m3524318132(L_6, L_9, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LevelExp::.ctor(System.UInt32,System.UInt32)
extern "C"  void LevelExp__ctor_m598325136 (LevelExp_t2602383666 * __this, uint32_t ___level0, uint32_t ___exp1, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		uint32_t L_0 = ___level0;
		__this->set_mLevel_0(L_0);
		uint32_t L_1 = ___exp1;
		__this->set_mExp_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MainGUI::.ctor()
extern "C"  void MainGUI__ctor_m1387098348 (MainGUI_t2584036754 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainGUI::Start()
extern "C"  void MainGUI_Start_m178317606 (MainGUI_t2584036754 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void MainGUI::Update()
extern "C"  void MainGUI_Update_m1167073022 (MainGUI_t2584036754 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MinimapCamera::.ctor()
extern "C"  void MinimapCamera__ctor_m29654710 (MinimapCamera_t1751400688 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MinimapCamera::Awake()
extern "C"  void MinimapCamera_Awake_m2471996870 (MinimapCamera_t1751400688 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void MinimapCamera::Update()
extern "C"  void MinimapCamera_Update_m4231757957 (MinimapCamera_t1751400688 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MinimapCamera_Update_m4231757957_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t4157153871 * L_0 = __this->get_mainCamera_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		GameScene_t4110668666 * L_2 = ((SceneManager_t385596982_StaticFields*)il2cpp_codegen_static_fields_for(SceneManager_t385596982_il2cpp_TypeInfo_var))->get_m_gameScene_2();
		GameObject_t1113636619 * L_3 = L_2->get_m_mainCamera_2();
		Camera_t4157153871 * L_4 = GameObject_GetComponent_TisCamera_t4157153871_m3956151066(L_3, /*hidden argument*/GameObject_GetComponent_TisCamera_t4157153871_m3956151066_RuntimeMethod_var);
		__this->set_mainCamera_3(L_4);
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_5 = Input_GetKey_m3736388334(NULL /*static, unused*/, ((int32_t)49), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0054;
		}
	}
	{
		Camera_t4157153871 * L_6 = __this->get_minimapCamera_2();
		Camera_t4157153871 * L_7 = Component_GetComponent_TisCamera_t4157153871_m1557787507(L_6, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var);
		Behaviour_set_enabled_m20417929(L_7, (bool)0, /*hidden argument*/NULL);
		Camera_t4157153871 * L_8 = __this->get_mainCamera_3();
		Camera_t4157153871 * L_9 = Component_GetComponent_TisCamera_t4157153871_m1557787507(L_8, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var);
		Behaviour_set_enabled_m20417929(L_9, (bool)1, /*hidden argument*/NULL);
	}

IL_0054:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_10 = Input_GetKey_m3736388334(NULL /*static, unused*/, ((int32_t)50), /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0082;
		}
	}
	{
		Camera_t4157153871 * L_11 = __this->get_mainCamera_3();
		Camera_t4157153871 * L_12 = Component_GetComponent_TisCamera_t4157153871_m1557787507(L_11, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var);
		Behaviour_set_enabled_m20417929(L_12, (bool)0, /*hidden argument*/NULL);
		Camera_t4157153871 * L_13 = __this->get_minimapCamera_2();
		Camera_t4157153871 * L_14 = Component_GetComponent_TisCamera_t4157153871_m1557787507(L_13, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var);
		Behaviour_set_enabled_m20417929(L_14, (bool)1, /*hidden argument*/NULL);
	}

IL_0082:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayerShipBehavior::.ctor()
extern "C"  void PlayerShipBehavior__ctor_m815437503 (PlayerShipBehavior_t729873910 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerShipBehavior::Start()
extern "C"  void PlayerShipBehavior_Start_m2112410415 (PlayerShipBehavior_t729873910 * __this, const RuntimeMethod* method)
{
	{
		__this->set_bIsInteractiving_2((bool)0);
		return;
	}
}
// System.Void PlayerShipBehavior::Update()
extern "C"  void PlayerShipBehavior_Update_m3781386249 (PlayerShipBehavior_t729873910 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void PlayerShipBehavior::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void PlayerShipBehavior_OnTriggerEnter_m2658974142 (PlayerShipBehavior_t729873910 * __this, Collider_t1773347010 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerShipBehavior_OnTriggerEnter_m2658974142_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	{
		Collider_t1773347010 * L_0 = ___other0;
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t1113636619 * L_2 = V_0;
		String_t* L_3 = GameObject_get_tag_m3951609671(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m920492651(NULL /*static, unused*/, L_3, _stringLiteral78692563, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0031;
		}
	}
	{
		GameObject_t1113636619 * L_5 = V_0;
		String_t* L_6 = GameObject_get_tag_m3951609671(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m920492651(NULL /*static, unused*/, L_6, _stringLiteral1768119585, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0032;
		}
	}

IL_0031:
	{
		return;
	}

IL_0032:
	{
		GameObject_t1113636619 * L_8 = V_0;
		String_t* L_9 = GameObject_get_tag_m3951609671(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m920492651(NULL /*static, unused*/, L_9, _stringLiteral1212291584, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0053;
		}
	}
	{
		GameObject_t1113636619 * L_11 = V_0;
		PlayerShipBehavior_InteractiveWithPort_m361013814(__this, L_11, /*hidden argument*/NULL);
		goto IL_0089;
	}

IL_0053:
	{
		GameObject_t1113636619 * L_12 = V_0;
		String_t* L_13 = GameObject_get_tag_m3951609671(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_14 = String_op_Equality_m920492651(NULL /*static, unused*/, L_13, _stringLiteral3410314461, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0074;
		}
	}
	{
		GameObject_t1113636619 * L_15 = V_0;
		PlayerShipBehavior_InteractiveWithNPC_m2391546926(__this, L_15, /*hidden argument*/NULL);
		goto IL_0089;
	}

IL_0074:
	{
		GameObject_t1113636619 * L_16 = V_0;
		String_t* L_17 = Object_get_name_m4211327027(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral1119515214, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
	}

IL_0089:
	{
		return;
	}
}
// System.Void PlayerShipBehavior::OnTriggerExit(UnityEngine.Collider)
extern "C"  void PlayerShipBehavior_OnTriggerExit_m3177691883 (PlayerShipBehavior_t729873910 * __this, Collider_t1773347010 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerShipBehavior_OnTriggerExit_m3177691883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	{
		Collider_t1773347010 * L_0 = ___other0;
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t1113636619 * L_2 = V_0;
		String_t* L_3 = GameObject_get_tag_m3951609671(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m920492651(NULL /*static, unused*/, L_3, _stringLiteral78692563, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0031;
		}
	}
	{
		GameObject_t1113636619 * L_5 = V_0;
		String_t* L_6 = GameObject_get_tag_m3951609671(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m920492651(NULL /*static, unused*/, L_6, _stringLiteral1768119585, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0032;
		}
	}

IL_0031:
	{
		return;
	}

IL_0032:
	{
		GameObject_t1113636619 * L_8 = V_0;
		String_t* L_9 = GameObject_get_tag_m3951609671(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m920492651(NULL /*static, unused*/, L_9, _stringLiteral1212291584, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0053;
		}
	}
	{
		GameObject_t1113636619 * L_11 = V_0;
		PlayerShipBehavior_FinishInteractiveWithPort_m354842222(__this, L_11, /*hidden argument*/NULL);
		goto IL_0089;
	}

IL_0053:
	{
		GameObject_t1113636619 * L_12 = V_0;
		String_t* L_13 = GameObject_get_tag_m3951609671(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_14 = String_op_Equality_m920492651(NULL /*static, unused*/, L_13, _stringLiteral3410314461, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0074;
		}
	}
	{
		GameObject_t1113636619 * L_15 = V_0;
		PlayerShipBehavior_FinishInteractiveWithNPC_m3927383204(__this, L_15, /*hidden argument*/NULL);
		goto IL_0089;
	}

IL_0074:
	{
		GameObject_t1113636619 * L_16 = V_0;
		String_t* L_17 = Object_get_name_m4211327027(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral1119515214, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
	}

IL_0089:
	{
		return;
	}
}
// System.Void PlayerShipBehavior::HighlightCollider(UnityEngine.GameObject,System.Boolean)
extern "C"  void PlayerShipBehavior_HighlightCollider_m195144836 (PlayerShipBehavior_t729873910 * __this, GameObject_t1113636619 * ___colliderGO0, bool ___highlight1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerShipBehavior_HighlightCollider_m195144836_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Highlightable_t3139352672 * V_0 = NULL;
	{
		Highlightable_t3139352672 * L_0 = Component_GetComponent_TisHighlightable_t3139352672_m3233271036(__this, /*hidden argument*/Component_GetComponent_TisHighlightable_t3139352672_m3233271036_RuntimeMethod_var);
		V_0 = L_0;
		Highlightable_t3139352672 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		GameObject_t1113636619 * L_3 = ___colliderGO0;
		Highlightable_t3139352672 * L_4 = GameObject_AddComponent_TisHighlightable_t3139352672_m2047509732(L_3, /*hidden argument*/GameObject_AddComponent_TisHighlightable_t3139352672_m2047509732_RuntimeMethod_var);
		V_0 = L_4;
	}

IL_001a:
	{
		Highlightable_t3139352672 * L_5 = V_0;
		bool L_6 = ___highlight1;
		Highlightable_SetAllChildrenHighlightable_m544031526(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerShipBehavior::InteractiveWithPort(UnityEngine.GameObject)
extern "C"  void PlayerShipBehavior_InteractiveWithPort_m361013814 (PlayerShipBehavior_t729873910 * __this, GameObject_t1113636619 * ___port0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerShipBehavior_InteractiveWithPort_m361013814_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_bIsInteractiving_2((bool)1);
		GameObject_t1113636619 * L_0 = ___port0;
		String_t* L_1 = Object_get_name_m4211327027(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral296396688, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_3 = ___port0;
		PlayerShipBehavior_HighlightCollider_m195144836(__this, L_3, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_4 = ___port0;
		GameObject_SendMessage_m3720186693(L_4, _stringLiteral3076983745, __this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerShipBehavior::FinishInteractiveWithPort(UnityEngine.GameObject)
extern "C"  void PlayerShipBehavior_FinishInteractiveWithPort_m354842222 (PlayerShipBehavior_t729873910 * __this, GameObject_t1113636619 * ___port0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerShipBehavior_FinishInteractiveWithPort_m354842222_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_bIsInteractiving_2((bool)0);
		GameObject_t1113636619 * L_0 = ___port0;
		String_t* L_1 = Object_get_name_m4211327027(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3531824247, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_3 = ___port0;
		PlayerShipBehavior_HighlightCollider_m195144836(__this, L_3, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_4 = ___port0;
		GameObject_SendMessage_m3720186693(L_4, _stringLiteral3840156928, __this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerShipBehavior::InteractiveWithNPC(UnityEngine.GameObject)
extern "C"  void PlayerShipBehavior_InteractiveWithNPC_m2391546926 (PlayerShipBehavior_t729873910 * __this, GameObject_t1113636619 * ___npc0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerShipBehavior_InteractiveWithNPC_m2391546926_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_bIsInteractiving_2((bool)1);
		GameObject_t1113636619 * L_0 = ___npc0;
		String_t* L_1 = Object_get_name_m4211327027(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3443649515, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_3 = ___npc0;
		PlayerShipBehavior_HighlightCollider_m195144836(__this, L_3, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_4 = ___npc0;
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		GameObject_SendMessage_m3720186693(L_4, _stringLiteral3076983745, L_5, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerShipBehavior::FinishInteractiveWithNPC(UnityEngine.GameObject)
extern "C"  void PlayerShipBehavior_FinishInteractiveWithNPC_m3927383204 (PlayerShipBehavior_t729873910 * __this, GameObject_t1113636619 * ___npc0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerShipBehavior_FinishInteractiveWithNPC_m3927383204_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_bIsInteractiving_2((bool)0);
		GameObject_t1113636619 * L_0 = ___npc0;
		PlayerShipBehavior_HighlightCollider_m195144836(__this, L_0, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_1 = ___npc0;
		String_t* L_2 = Object_get_name_m4211327027(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral1320622954, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_4 = ___npc0;
		GameObject_SendMessage_m3720186693(L_4, _stringLiteral3840156928, __this, 1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RandomTerrainGenerator::.ctor()
extern "C"  void RandomTerrainGenerator__ctor_m2168726465 (RandomTerrainGenerator_t662438485 * __this, const RuntimeMethod* method)
{
	{
		__this->set_HM_2((6.0f));
		__this->set_divRange_3((2.3f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RandomTerrainGenerator::Start()
extern "C"  void RandomTerrainGenerator_Start_m1198348437 (RandomTerrainGenerator_t662438485 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void RandomTerrainGenerator::OnGUI()
extern "C"  void RandomTerrainGenerator_OnGUI_m790361608 (RandomTerrainGenerator_t662438485 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RandomTerrainGenerator_OnGUI_m790361608_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rect_t2360479859  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Rect__ctor_m2614021312((&L_0), (10.0f), (10.0f), (100.0f), (50.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		bool L_1 = GUI_Button_m1518979886(NULL /*static, unused*/, L_0, _stringLiteral4037192534, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003f;
		}
	}
	{
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Terrain_t3055443660 * L_3 = GameObject_GetComponent_TisTerrain_t3055443660_m338788394(L_2, /*hidden argument*/GameObject_GetComponent_TisTerrain_t3055443660_m338788394_RuntimeMethod_var);
		float L_4 = __this->get_HM_2();
		RandomTerrainGenerator_CreateTerrainTextureWithHeight_m3848507478(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// System.Void RandomTerrainGenerator::OnWizardCreate()
extern "C"  void RandomTerrainGenerator_OnWizardCreate_m4115677761 (RandomTerrainGenerator_t662438485 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RandomTerrainGenerator_OnWizardCreate_m4115677761_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1113636619 * L_1 = V_0;
		Terrain_t3055443660 * L_2 = GameObject_GetComponent_TisTerrain_t3055443660_m338788394(L_1, /*hidden argument*/GameObject_GetComponent_TisTerrain_t3055443660_m338788394_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		GameObject_t1113636619 * L_4 = V_0;
		Terrain_t3055443660 * L_5 = GameObject_GetComponent_TisTerrain_t3055443660_m338788394(L_4, /*hidden argument*/GameObject_GetComponent_TisTerrain_t3055443660_m338788394_RuntimeMethod_var);
		float L_6 = __this->get_HM_2();
		RandomTerrainGenerator_GenerateTerrain_m199714022(__this, L_5, L_6, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Void RandomTerrainGenerator::GenerateTerrain(UnityEngine.Terrain,System.Single)
extern "C"  void RandomTerrainGenerator_GenerateTerrain_m199714022 (RandomTerrainGenerator_t662438485 * __this, Terrain_t3055443660 * ___t0, float ___tileSize1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RandomTerrainGenerator_GenerateTerrain_m199714022_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SingleU5B0___U2C0___U5D_t1444911252* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		Terrain_t3055443660 * L_0 = ___t0;
		TerrainData_t657004131 * L_1 = Terrain_get_terrainData_m2711583617(L_0, /*hidden argument*/NULL);
		int32_t L_2 = TerrainData_get_heightmapWidth_m3057970466(L_1, /*hidden argument*/NULL);
		Terrain_t3055443660 * L_3 = ___t0;
		TerrainData_t657004131 * L_4 = Terrain_get_terrainData_m2711583617(L_3, /*hidden argument*/NULL);
		int32_t L_5 = TerrainData_get_heightmapHeight_m3110588835(L_4, /*hidden argument*/NULL);
		il2cpp_array_size_t L_7[] = { (il2cpp_array_size_t)L_2, (il2cpp_array_size_t)L_5 };
		SingleU5B0___U2C0___U5D_t1444911252* L_6 = (SingleU5B0___U2C0___U5D_t1444911252*)GenArrayNew(SingleU5B0___U2C0___U5D_t1444911252_il2cpp_TypeInfo_var, L_7);
		V_0 = L_6;
		V_1 = 0;
		goto IL_0079;
	}

IL_0023:
	{
		V_2 = 0;
		goto IL_0064;
	}

IL_002a:
	{
		SingleU5B0___U2C0___U5D_t1444911252* L_8 = V_0;
		int32_t L_9 = V_1;
		int32_t L_10 = V_2;
		int32_t L_11 = V_1;
		Terrain_t3055443660 * L_12 = ___t0;
		TerrainData_t657004131 * L_13 = Terrain_get_terrainData_m2711583617(L_12, /*hidden argument*/NULL);
		int32_t L_14 = TerrainData_get_heightmapWidth_m3057970466(L_13, /*hidden argument*/NULL);
		float L_15 = ___tileSize1;
		int32_t L_16 = V_2;
		Terrain_t3055443660 * L_17 = ___t0;
		TerrainData_t657004131 * L_18 = Terrain_get_terrainData_m2711583617(L_17, /*hidden argument*/NULL);
		int32_t L_19 = TerrainData_get_heightmapHeight_m3110588835(L_18, /*hidden argument*/NULL);
		float L_20 = ___tileSize1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_21 = Mathf_PerlinNoise_m2335393878(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)((float)((float)(((float)((float)L_11)))/(float)(((float)((float)L_14))))), (float)L_15)), ((float)il2cpp_codegen_multiply((float)((float)((float)(((float)((float)L_16)))/(float)(((float)((float)L_19))))), (float)L_20)), /*hidden argument*/NULL);
		float L_22 = __this->get_divRange_3();
		(L_8)->SetAtUnchecked(L_9, L_10, ((float)((float)L_21/(float)L_22)));
		int32_t L_23 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
	}

IL_0064:
	{
		int32_t L_24 = V_2;
		Terrain_t3055443660 * L_25 = ___t0;
		TerrainData_t657004131 * L_26 = Terrain_get_terrainData_m2711583617(L_25, /*hidden argument*/NULL);
		int32_t L_27 = TerrainData_get_heightmapHeight_m3110588835(L_26, /*hidden argument*/NULL);
		if ((((int32_t)L_24) < ((int32_t)L_27)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_28 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_28, (int32_t)1));
	}

IL_0079:
	{
		int32_t L_29 = V_1;
		Terrain_t3055443660 * L_30 = ___t0;
		TerrainData_t657004131 * L_31 = Terrain_get_terrainData_m2711583617(L_30, /*hidden argument*/NULL);
		int32_t L_32 = TerrainData_get_heightmapWidth_m3057970466(L_31, /*hidden argument*/NULL);
		if ((((int32_t)L_29) < ((int32_t)L_32)))
		{
			goto IL_0023;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_33 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4));
		ArrayElementTypeCheck (L_33, _stringLiteral2954705631);
		(L_33)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral2954705631);
		ObjectU5BU5D_t2843939325* L_34 = L_33;
		float L_35 = __this->get_divRange_3();
		float L_36 = L_35;
		RuntimeObject * L_37 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_36);
		ArrayElementTypeCheck (L_34, L_37);
		(L_34)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_37);
		ObjectU5BU5D_t2843939325* L_38 = L_34;
		ArrayElementTypeCheck (L_38, _stringLiteral3522134580);
		(L_38)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral3522134580);
		ObjectU5BU5D_t2843939325* L_39 = L_38;
		float L_40 = __this->get_HM_2();
		float L_41 = L_40;
		RuntimeObject * L_42 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_41);
		ArrayElementTypeCheck (L_39, L_42);
		(L_39)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_42);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_43 = String_Concat_m2971454694(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		Terrain_t3055443660 * L_44 = ___t0;
		TerrainData_t657004131 * L_45 = Terrain_get_terrainData_m2711583617(L_44, /*hidden argument*/NULL);
		SingleU5B0___U2C0___U5D_t1444911252* L_46 = V_0;
		TerrainData_SetHeights_m690219761(L_45, 0, 0, L_46, /*hidden argument*/NULL);
		Terrain_t3055443660 * L_47 = ___t0;
		Terrain_set_detailObjectDensity_m3204163592(L_47, (0.2f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void RandomTerrainGenerator::UpdateTerrainTexture(UnityEngine.TerrainData,System.Int32,System.Int32)
extern "C"  void RandomTerrainGenerator_UpdateTerrainTexture_m2921420409 (RuntimeObject * __this /* static, unused */, TerrainData_t657004131 * ___terrainData0, int32_t ___textureNumberFrom1, int32_t ___textureNumberTo2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RandomTerrainGenerator_UpdateTerrainTexture_m2921420409_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SingleU5B0___U2C0___U2C0___U5D_t1444911253* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		TerrainData_t657004131 * L_0 = ___terrainData0;
		TerrainData_t657004131 * L_1 = ___terrainData0;
		int32_t L_2 = TerrainData_get_alphamapWidth_m315223358(L_1, /*hidden argument*/NULL);
		TerrainData_t657004131 * L_3 = ___terrainData0;
		int32_t L_4 = TerrainData_get_alphamapHeight_m595805513(L_3, /*hidden argument*/NULL);
		SingleU5B0___U2C0___U2C0___U5D_t1444911253* L_5 = TerrainData_GetAlphamaps_m3939888263(L_0, 0, 0, L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		V_1 = 0;
		goto IL_0065;
	}

IL_001c:
	{
		V_2 = 0;
		goto IL_0055;
	}

IL_0023:
	{
		SingleU5B0___U2C0___U2C0___U5D_t1444911253* L_6 = V_0;
		int32_t L_7 = V_1;
		int32_t L_8 = V_2;
		int32_t L_9 = ___textureNumberTo2;
		SingleU5B0___U2C0___U2C0___U5D_t1444911253* L_10 = V_0;
		int32_t L_11 = V_1;
		int32_t L_12 = V_2;
		int32_t L_13 = ___textureNumberFrom1;
		float L_14 = (L_10)->GetAtUnchecked(L_11, L_12, L_13);
		SingleU5B0___U2C0___U2C0___U5D_t1444911253* L_15 = V_0;
		int32_t L_16 = V_1;
		int32_t L_17 = V_2;
		int32_t L_18 = ___textureNumberTo2;
		float L_19 = (L_15)->GetAtUnchecked(L_16, L_17, L_18);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_20 = Mathf_Max_m3146388979(NULL /*static, unused*/, L_14, L_19, /*hidden argument*/NULL);
		(L_6)->SetAtUnchecked(L_7, L_8, L_9, L_20);
		SingleU5B0___U2C0___U2C0___U5D_t1444911253* L_21 = V_0;
		int32_t L_22 = V_1;
		int32_t L_23 = V_2;
		int32_t L_24 = ___textureNumberFrom1;
		(L_21)->SetAtUnchecked(L_22, L_23, L_24, (0.0f));
		int32_t L_25 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_25, (int32_t)1));
	}

IL_0055:
	{
		int32_t L_26 = V_2;
		TerrainData_t657004131 * L_27 = ___terrainData0;
		int32_t L_28 = TerrainData_get_alphamapHeight_m595805513(L_27, /*hidden argument*/NULL);
		if ((((int32_t)L_26) < ((int32_t)L_28)))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_29 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)1));
	}

IL_0065:
	{
		int32_t L_30 = V_1;
		TerrainData_t657004131 * L_31 = ___terrainData0;
		int32_t L_32 = TerrainData_get_alphamapWidth_m315223358(L_31, /*hidden argument*/NULL);
		if ((((int32_t)L_30) < ((int32_t)L_32)))
		{
			goto IL_001c;
		}
	}
	{
		TerrainData_t657004131 * L_33 = ___terrainData0;
		SingleU5B0___U2C0___U2C0___U5D_t1444911253* L_34 = V_0;
		TerrainData_SetAlphamaps_m75475119(L_33, 0, 0, L_34, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RandomTerrainGenerator::CreateTerrainTextureWithHeight(UnityEngine.Terrain,System.Single)
extern "C"  void RandomTerrainGenerator_CreateTerrainTextureWithHeight_m3848507478 (RandomTerrainGenerator_t662438485 * __this, Terrain_t3055443660 * ___t0, float ___tileSize1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RandomTerrainGenerator_CreateTerrainTextureWithHeight_m3848507478_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TerrainData_t657004131 * V_0 = NULL;
	SingleU5B0___U2C0___U5D_t1444911252* V_1 = NULL;
	SingleU5B0___U2C0___U2C0___U5D_t1444911253* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	float V_7 = 0.0f;
	{
		Terrain_t3055443660 * L_0 = ___t0;
		TerrainData_t657004131 * L_1 = Terrain_get_terrainData_m2711583617(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		TerrainData_t657004131 * L_2 = V_0;
		int32_t L_3 = TerrainData_get_heightmapWidth_m3057970466(L_2, /*hidden argument*/NULL);
		TerrainData_t657004131 * L_4 = V_0;
		int32_t L_5 = TerrainData_get_heightmapHeight_m3110588835(L_4, /*hidden argument*/NULL);
		il2cpp_array_size_t L_7[] = { (il2cpp_array_size_t)L_3, (il2cpp_array_size_t)L_5 };
		SingleU5B0___U2C0___U5D_t1444911252* L_6 = (SingleU5B0___U2C0___U5D_t1444911252*)GenArrayNew(SingleU5B0___U2C0___U5D_t1444911252_il2cpp_TypeInfo_var, L_7);
		V_1 = L_6;
		TerrainData_t657004131 * L_8 = V_0;
		TerrainData_t657004131 * L_9 = V_0;
		int32_t L_10 = TerrainData_get_alphamapWidth_m315223358(L_9, /*hidden argument*/NULL);
		TerrainData_t657004131 * L_11 = V_0;
		int32_t L_12 = TerrainData_get_alphamapHeight_m595805513(L_11, /*hidden argument*/NULL);
		SingleU5B0___U2C0___U2C0___U5D_t1444911253* L_13 = TerrainData_GetAlphamaps_m3939888263(L_8, 0, 0, L_10, L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		V_3 = 0;
		V_4 = 0;
		V_5 = 0;
		goto IL_012d;
	}

IL_003b:
	{
		V_6 = 0;
		goto IL_011a;
	}

IL_0043:
	{
		int32_t L_14 = V_5;
		TerrainData_t657004131 * L_15 = V_0;
		int32_t L_16 = TerrainData_get_heightmapWidth_m3057970466(L_15, /*hidden argument*/NULL);
		float L_17 = ___tileSize1;
		int32_t L_18 = V_6;
		Terrain_t3055443660 * L_19 = ___t0;
		TerrainData_t657004131 * L_20 = Terrain_get_terrainData_m2711583617(L_19, /*hidden argument*/NULL);
		int32_t L_21 = TerrainData_get_heightmapHeight_m3110588835(L_20, /*hidden argument*/NULL);
		float L_22 = ___tileSize1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_23 = Mathf_PerlinNoise_m2335393878(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)((float)((float)(((float)((float)L_14)))/(float)(((float)((float)L_16))))), (float)L_17)), ((float)il2cpp_codegen_multiply((float)((float)((float)(((float)((float)L_18)))/(float)(((float)((float)L_21))))), (float)L_22)), /*hidden argument*/NULL);
		float L_24 = __this->get_divRange_3();
		V_7 = ((float)((float)L_23/(float)L_24));
		SingleU5B0___U2C0___U5D_t1444911252* L_25 = V_1;
		int32_t L_26 = V_5;
		int32_t L_27 = V_6;
		float L_28 = V_7;
		(L_25)->SetAtUnchecked(L_26, L_27, L_28);
		int32_t L_29 = V_6;
		TerrainData_t657004131 * L_30 = V_0;
		int32_t L_31 = TerrainData_get_alphamapHeight_m595805513(L_30, /*hidden argument*/NULL);
		if ((((int32_t)L_29) >= ((int32_t)L_31)))
		{
			goto IL_0114;
		}
	}
	{
		int32_t L_32 = V_5;
		TerrainData_t657004131 * L_33 = V_0;
		int32_t L_34 = TerrainData_get_alphamapWidth_m315223358(L_33, /*hidden argument*/NULL);
		if ((((int32_t)L_32) >= ((int32_t)L_34)))
		{
			goto IL_0114;
		}
	}
	{
		float L_35 = V_7;
		if ((!(((double)(((double)((double)L_35)))) > ((double)(0.25)))))
		{
			goto IL_0114;
		}
	}
	{
		float L_36 = V_7;
		if ((!(((double)(((double)((double)L_36)))) > ((double)(0.4)))))
		{
			goto IL_00c0;
		}
	}
	{
		V_4 = 3;
		goto IL_00dc;
	}

IL_00c0:
	{
		float L_37 = V_7;
		if ((!(((double)(((double)((double)L_37)))) > ((double)(0.3)))))
		{
			goto IL_00d9;
		}
	}
	{
		V_4 = 2;
		goto IL_00dc;
	}

IL_00d9:
	{
		V_4 = 1;
	}

IL_00dc:
	{
		SingleU5B0___U2C0___U2C0___U5D_t1444911253* L_38 = V_2;
		int32_t L_39 = V_5;
		int32_t L_40 = V_6;
		int32_t L_41 = V_4;
		SingleU5B0___U2C0___U2C0___U5D_t1444911253* L_42 = V_2;
		int32_t L_43 = V_5;
		int32_t L_44 = V_6;
		int32_t L_45 = V_3;
		float L_46 = (L_42)->GetAtUnchecked(L_43, L_44, L_45);
		SingleU5B0___U2C0___U2C0___U5D_t1444911253* L_47 = V_2;
		int32_t L_48 = V_5;
		int32_t L_49 = V_6;
		int32_t L_50 = V_4;
		float L_51 = (L_47)->GetAtUnchecked(L_48, L_49, L_50);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_52 = Mathf_Max_m3146388979(NULL /*static, unused*/, L_46, L_51, /*hidden argument*/NULL);
		(L_38)->SetAtUnchecked(L_39, L_40, L_41, L_52);
		SingleU5B0___U2C0___U2C0___U5D_t1444911253* L_53 = V_2;
		int32_t L_54 = V_5;
		int32_t L_55 = V_6;
		int32_t L_56 = V_3;
		(L_53)->SetAtUnchecked(L_54, L_55, L_56, (0.0f));
	}

IL_0114:
	{
		int32_t L_57 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_57, (int32_t)1));
	}

IL_011a:
	{
		int32_t L_58 = V_6;
		TerrainData_t657004131 * L_59 = V_0;
		int32_t L_60 = TerrainData_get_heightmapHeight_m3110588835(L_59, /*hidden argument*/NULL);
		if ((((int32_t)L_58) < ((int32_t)L_60)))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_61 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_61, (int32_t)1));
	}

IL_012d:
	{
		int32_t L_62 = V_5;
		TerrainData_t657004131 * L_63 = V_0;
		int32_t L_64 = TerrainData_get_heightmapWidth_m3057970466(L_63, /*hidden argument*/NULL);
		if ((((int32_t)L_62) < ((int32_t)L_64)))
		{
			goto IL_003b;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_65 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4));
		ArrayElementTypeCheck (L_65, _stringLiteral2954705631);
		(L_65)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral2954705631);
		ObjectU5BU5D_t2843939325* L_66 = L_65;
		float L_67 = __this->get_divRange_3();
		float L_68 = L_67;
		RuntimeObject * L_69 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_68);
		ArrayElementTypeCheck (L_66, L_69);
		(L_66)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_69);
		ObjectU5BU5D_t2843939325* L_70 = L_66;
		ArrayElementTypeCheck (L_70, _stringLiteral3522134580);
		(L_70)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral3522134580);
		ObjectU5BU5D_t2843939325* L_71 = L_70;
		float L_72 = __this->get_HM_2();
		float L_73 = L_72;
		RuntimeObject * L_74 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_73);
		ArrayElementTypeCheck (L_71, L_74);
		(L_71)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_74);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_75 = String_Concat_m2971454694(NULL /*static, unused*/, L_71, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, L_75, /*hidden argument*/NULL);
		TerrainData_t657004131 * L_76 = V_0;
		SingleU5B0___U2C0___U5D_t1444911252* L_77 = V_1;
		TerrainData_SetHeights_m690219761(L_76, 0, 0, L_77, /*hidden argument*/NULL);
		TerrainData_t657004131 * L_78 = V_0;
		SingleU5B0___U2C0___U2C0___U5D_t1444911253* L_79 = V_2;
		TerrainData_SetAlphamaps_m75475119(L_78, 0, 0, L_79, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RaycastMask::.ctor()
extern "C"  void RaycastMask__ctor_m662013837 (RaycastMask_t1362276734 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RaycastMask::Start()
extern "C"  void RaycastMask_Start_m4258084315 (RaycastMask_t1362276734 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RaycastMask_Start_m4258084315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Image_t2670269651 * L_0 = Component_GetComponent_TisImage_t2670269651_m980647750(__this, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		__this->set__image_2(L_0);
		return;
	}
}
// System.Boolean RaycastMask::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern "C"  bool RaycastMask_IsRaycastLocationValid_m385128278 (RaycastMask_t1362276734 * __this, Vector2_t2156229523  ___sp0, Camera_t4157153871 * ___eventCamera1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RaycastMask_IsRaycastLocationValid_m385128278_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectTransform_t3704657025 * V_0 = NULL;
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2156229523  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2156229523  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t2360479859  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t2156229523  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Rect_t2360479859  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Rect_t2360479859  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Rect_t2360479859  V_8;
	memset(&V_8, 0, sizeof(V_8));
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	Vector4_t3319028937  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Color_t2555686324  V_13;
	memset(&V_13, 0, sizeof(V_13));
	bool V_14 = false;
	UnityException_t3598173660 * V_15 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Image_t2670269651 * L_0 = __this->get__image_2();
		Sprite_t280657092 * L_1 = Image_get_sprite_m1811690853(L_0, /*hidden argument*/NULL);
		__this->set__sprite_3(L_1);
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		V_0 = ((RectTransform_t3704657025 *)CastclassSealed((RuntimeObject*)L_2, RectTransform_t3704657025_il2cpp_TypeInfo_var));
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_4 = ___sp0;
		Camera_t4157153871 * L_5 = ___eventCamera1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1743242446_il2cpp_TypeInfo_var);
		RectTransformUtility_ScreenPointToLocalPointInRectangle_m2327269187(NULL /*static, unused*/, ((RectTransform_t3704657025 *)CastclassSealed((RuntimeObject*)L_3, RectTransform_t3704657025_il2cpp_TypeInfo_var)), L_4, L_5, (&V_1), /*hidden argument*/NULL);
		float L_6 = (&V_1)->get_x_0();
		RectTransform_t3704657025 * L_7 = V_0;
		Vector2_t2156229523  L_8 = RectTransform_get_pivot_m3425744470(L_7, /*hidden argument*/NULL);
		V_3 = L_8;
		float L_9 = (&V_3)->get_x_0();
		RectTransform_t3704657025 * L_10 = V_0;
		Rect_t2360479859  L_11 = RectTransform_get_rect_m574169965(L_10, /*hidden argument*/NULL);
		V_4 = L_11;
		float L_12 = Rect_get_width_m3421484486((&V_4), /*hidden argument*/NULL);
		float L_13 = (&V_1)->get_y_1();
		RectTransform_t3704657025 * L_14 = V_0;
		Vector2_t2156229523  L_15 = RectTransform_get_pivot_m3425744470(L_14, /*hidden argument*/NULL);
		V_5 = L_15;
		float L_16 = (&V_5)->get_y_1();
		RectTransform_t3704657025 * L_17 = V_0;
		Rect_t2360479859  L_18 = RectTransform_get_rect_m574169965(L_17, /*hidden argument*/NULL);
		V_6 = L_18;
		float L_19 = Rect_get_height_m1358425599((&V_6), /*hidden argument*/NULL);
		Vector2__ctor_m3970636864((&V_2), ((float)il2cpp_codegen_add((float)L_6, (float)((float)il2cpp_codegen_multiply((float)L_9, (float)L_12)))), ((float)il2cpp_codegen_add((float)L_13, (float)((float)il2cpp_codegen_multiply((float)L_16, (float)L_19)))), /*hidden argument*/NULL);
		Sprite_t280657092 * L_20 = __this->get__sprite_3();
		Rect_t2360479859  L_21 = Sprite_get_textureRect_m3217515846(L_20, /*hidden argument*/NULL);
		V_7 = L_21;
		RectTransform_t3704657025 * L_22 = V_0;
		Rect_t2360479859  L_23 = RectTransform_get_rect_m574169965(L_22, /*hidden argument*/NULL);
		V_8 = L_23;
		V_9 = 0;
		V_10 = 0;
		Image_t2670269651 * L_24 = __this->get__image_2();
		int32_t L_25 = Image_get_type_m3606908055(L_24, /*hidden argument*/NULL);
		V_11 = L_25;
		int32_t L_26 = V_11;
		if ((((int32_t)L_26) == ((int32_t)1)))
		{
			goto IL_00c2;
		}
	}
	{
		int32_t L_27 = V_11;
		if (!L_27)
		{
			goto IL_0268;
		}
	}
	{
		goto IL_0268;
	}

IL_00c2:
	{
		Sprite_t280657092 * L_28 = __this->get__sprite_3();
		Vector4_t3319028937  L_29 = Sprite_get_border_m2985609076(L_28, /*hidden argument*/NULL);
		V_12 = L_29;
		float L_30 = (&V_2)->get_x_0();
		float L_31 = (&V_12)->get_x_1();
		if ((!(((float)L_30) < ((float)L_31))))
		{
			goto IL_00fd;
		}
	}
	{
		float L_32 = Rect_get_x_m3839990490((&V_7), /*hidden argument*/NULL);
		float L_33 = (&V_2)->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_34 = Mathf_FloorToInt_m1870542928(NULL /*static, unused*/, ((float)il2cpp_codegen_add((float)L_32, (float)L_33)), /*hidden argument*/NULL);
		V_9 = L_34;
		goto IL_0199;
	}

IL_00fd:
	{
		float L_35 = (&V_2)->get_x_0();
		float L_36 = Rect_get_width_m3421484486((&V_8), /*hidden argument*/NULL);
		float L_37 = (&V_12)->get_z_3();
		if ((!(((float)L_35) > ((float)((float)il2cpp_codegen_subtract((float)L_36, (float)L_37))))))
		{
			goto IL_0143;
		}
	}
	{
		float L_38 = Rect_get_x_m3839990490((&V_7), /*hidden argument*/NULL);
		float L_39 = Rect_get_width_m3421484486((&V_7), /*hidden argument*/NULL);
		float L_40 = Rect_get_width_m3421484486((&V_8), /*hidden argument*/NULL);
		float L_41 = (&V_2)->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_42 = Mathf_FloorToInt_m1870542928(NULL /*static, unused*/, ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_add((float)L_38, (float)L_39)), (float)((float)il2cpp_codegen_subtract((float)L_40, (float)L_41)))), /*hidden argument*/NULL);
		V_9 = L_42;
		goto IL_0199;
	}

IL_0143:
	{
		float L_43 = Rect_get_x_m3839990490((&V_7), /*hidden argument*/NULL);
		float L_44 = (&V_12)->get_x_1();
		float L_45 = (&V_2)->get_x_0();
		float L_46 = (&V_12)->get_x_1();
		float L_47 = Rect_get_width_m3421484486((&V_8), /*hidden argument*/NULL);
		float L_48 = (&V_12)->get_x_1();
		float L_49 = (&V_12)->get_z_3();
		float L_50 = Rect_get_width_m3421484486((&V_7), /*hidden argument*/NULL);
		float L_51 = (&V_12)->get_x_1();
		float L_52 = (&V_12)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_53 = Mathf_FloorToInt_m1870542928(NULL /*static, unused*/, ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)L_43, (float)L_44)), (float)((float)il2cpp_codegen_multiply((float)((float)((float)((float)il2cpp_codegen_subtract((float)L_45, (float)L_46))/(float)((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_subtract((float)L_47, (float)L_48)), (float)L_49)))), (float)((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_subtract((float)L_50, (float)L_51)), (float)L_52)))))), /*hidden argument*/NULL);
		V_9 = L_53;
	}

IL_0199:
	{
		float L_54 = (&V_2)->get_y_1();
		float L_55 = (&V_12)->get_y_2();
		if ((!(((float)L_54) < ((float)L_55))))
		{
			goto IL_01c7;
		}
	}
	{
		float L_56 = Rect_get_y_m1501338330((&V_7), /*hidden argument*/NULL);
		float L_57 = (&V_2)->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_58 = Mathf_FloorToInt_m1870542928(NULL /*static, unused*/, ((float)il2cpp_codegen_add((float)L_56, (float)L_57)), /*hidden argument*/NULL);
		V_10 = L_58;
		goto IL_0263;
	}

IL_01c7:
	{
		float L_59 = (&V_2)->get_y_1();
		float L_60 = Rect_get_height_m1358425599((&V_8), /*hidden argument*/NULL);
		float L_61 = (&V_12)->get_w_4();
		if ((!(((float)L_59) > ((float)((float)il2cpp_codegen_subtract((float)L_60, (float)L_61))))))
		{
			goto IL_020d;
		}
	}
	{
		float L_62 = Rect_get_y_m1501338330((&V_7), /*hidden argument*/NULL);
		float L_63 = Rect_get_height_m1358425599((&V_7), /*hidden argument*/NULL);
		float L_64 = Rect_get_height_m1358425599((&V_8), /*hidden argument*/NULL);
		float L_65 = (&V_2)->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_66 = Mathf_FloorToInt_m1870542928(NULL /*static, unused*/, ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_add((float)L_62, (float)L_63)), (float)((float)il2cpp_codegen_subtract((float)L_64, (float)L_65)))), /*hidden argument*/NULL);
		V_10 = L_66;
		goto IL_0263;
	}

IL_020d:
	{
		float L_67 = Rect_get_y_m1501338330((&V_7), /*hidden argument*/NULL);
		float L_68 = (&V_12)->get_y_2();
		float L_69 = (&V_2)->get_y_1();
		float L_70 = (&V_12)->get_y_2();
		float L_71 = Rect_get_height_m1358425599((&V_8), /*hidden argument*/NULL);
		float L_72 = (&V_12)->get_y_2();
		float L_73 = (&V_12)->get_w_4();
		float L_74 = Rect_get_height_m1358425599((&V_7), /*hidden argument*/NULL);
		float L_75 = (&V_12)->get_y_2();
		float L_76 = (&V_12)->get_w_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_77 = Mathf_FloorToInt_m1870542928(NULL /*static, unused*/, ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)L_67, (float)L_68)), (float)((float)il2cpp_codegen_multiply((float)((float)((float)((float)il2cpp_codegen_subtract((float)L_69, (float)L_70))/(float)((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_subtract((float)L_71, (float)L_72)), (float)L_73)))), (float)((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_subtract((float)L_74, (float)L_75)), (float)L_76)))))), /*hidden argument*/NULL);
		V_10 = L_77;
	}

IL_0263:
	{
		goto IL_02b9;
	}

IL_0268:
	{
		float L_78 = Rect_get_x_m3839990490((&V_7), /*hidden argument*/NULL);
		float L_79 = Rect_get_width_m3421484486((&V_7), /*hidden argument*/NULL);
		float L_80 = (&V_2)->get_x_0();
		float L_81 = Rect_get_width_m3421484486((&V_8), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_82 = Mathf_FloorToInt_m1870542928(NULL /*static, unused*/, ((float)il2cpp_codegen_add((float)L_78, (float)((float)((float)((float)il2cpp_codegen_multiply((float)L_79, (float)L_80))/(float)L_81)))), /*hidden argument*/NULL);
		V_9 = L_82;
		float L_83 = Rect_get_y_m1501338330((&V_7), /*hidden argument*/NULL);
		float L_84 = Rect_get_height_m1358425599((&V_7), /*hidden argument*/NULL);
		float L_85 = (&V_2)->get_y_1();
		float L_86 = Rect_get_height_m1358425599((&V_8), /*hidden argument*/NULL);
		int32_t L_87 = Mathf_FloorToInt_m1870542928(NULL /*static, unused*/, ((float)il2cpp_codegen_add((float)L_83, (float)((float)((float)((float)il2cpp_codegen_multiply((float)L_84, (float)L_85))/(float)L_86)))), /*hidden argument*/NULL);
		V_10 = L_87;
		goto IL_02b9;
	}

IL_02b9:
	try
	{ // begin try (depth: 1)
		Sprite_t280657092 * L_88 = __this->get__sprite_3();
		Texture2D_t3840446185 * L_89 = Sprite_get_texture_m3976398399(L_88, /*hidden argument*/NULL);
		int32_t L_90 = V_9;
		int32_t L_91 = V_10;
		Color_t2555686324  L_92 = Texture2D_GetPixel_m1195410881(L_89, L_90, L_91, /*hidden argument*/NULL);
		V_13 = L_92;
		float L_93 = (&V_13)->get_a_3();
		V_14 = (bool)((((float)L_93) > ((float)(0.0f)))? 1 : 0);
		goto IL_030a;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (UnityException_t3598173660_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_02e4;
		throw e;
	}

CATCH_02e4:
	{ // begin catch(UnityEngine.UnityException)
		V_15 = ((UnityException_t3598173660 *)__exception_local);
		UnityException_t3598173660 * L_94 = V_15;
		String_t* L_95 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_94);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_96 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2491212491, L_95, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2850623458(NULL /*static, unused*/, L_96, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_14 = (bool)0;
		goto IL_030a;
	} // end catch (depth: 1)

IL_030a:
	{
		bool L_97 = V_14;
		return L_97;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RotateTowardCamera::.ctor()
extern "C"  void RotateTowardCamera__ctor_m3317674712 (RotateTowardCamera_t118585142 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RotateTowardCamera::Start()
extern "C"  void RotateTowardCamera_Start_m1643822062 (RotateTowardCamera_t118585142 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void RotateTowardCamera::Update()
extern "C"  void RotateTowardCamera_Update_m3193687590 (RotateTowardCamera_t118585142 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RotateTowardCamera_Update_m3193687590_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t4157153871 * L_0 = __this->get_mainCamera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		GameScene_t4110668666 * L_2 = ((SceneManager_t385596982_StaticFields*)il2cpp_codegen_static_fields_for(SceneManager_t385596982_il2cpp_TypeInfo_var))->get_m_gameScene_2();
		GameObject_t1113636619 * L_3 = L_2->get_m_mainCamera_2();
		Camera_t4157153871 * L_4 = GameObject_GetComponent_TisCamera_t4157153871_m3956151066(L_3, /*hidden argument*/GameObject_GetComponent_TisCamera_t4157153871_m3956151066_RuntimeMethod_var);
		__this->set_mainCamera_2(L_4);
	}

IL_0026:
	{
		Camera_t4157153871 * L_5 = __this->get_mainCamera_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0042;
		}
	}
	{
		Camera_t4157153871 * L_7 = __this->get_mainCamera_2();
		RotateTowardCamera_OnRotateTowardCamera_m1367757239(__this, L_7, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return;
	}
}
// System.Void RotateTowardCamera::OnRotateTowardCamera(UnityEngine.Camera)
extern "C"  void RotateTowardCamera_OnRotateTowardCamera_m1367757239 (RotateTowardCamera_t118585142 * __this, Camera_t4157153871 * ___camera0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RotateTowardCamera_OnRotateTowardCamera_m1367757239_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Camera_t4157153871 * L_1 = ___camera0;
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(L_1, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3 = Transform_get_forward_m747522392(L_2, /*hidden argument*/NULL);
		Camera_t4157153871 * L_4 = ___camera0;
		Transform_t3600365921 * L_5 = Component_get_transform_m3162698980(L_4, /*hidden argument*/NULL);
		Vector3_t3722313464  L_6 = Transform_get_up_m3972993886(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_7 = Quaternion_LookRotation_m3197602968(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/NULL);
		Transform_set_rotation_m3524318132(L_0, L_7, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SceneManager::.ctor()
extern "C"  void SceneManager__ctor_m1409300582 (SceneManager_t385596982 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SceneManager::Start()
extern "C"  void SceneManager_Start_m3453891390 (SceneManager_t385596982 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Start_m3453891390_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		GameObject_AddComponent_TisHUDFPS_t4241874542_m817494477(L_0, /*hidden argument*/GameObject_AddComponent_TisHUDFPS_t4241874542_m817494477_RuntimeMethod_var);
		SceneManager_InitScene_m1840825294(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SceneManager::Update()
extern "C"  void SceneManager_Update_m3409230370 (SceneManager_t385596982 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void SceneManager::InitScene()
extern "C"  void SceneManager_InitScene_m1840825294 (SceneManager_t385596982 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_InitScene_m1840825294_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameScene_t4110668666 * L_0 = (GameScene_t4110668666 *)il2cpp_codegen_object_new(GameScene_t4110668666_il2cpp_TypeInfo_var);
		GameScene__ctor_m3327038984(L_0, /*hidden argument*/NULL);
		((SceneManager_t385596982_StaticFields*)il2cpp_codegen_static_fields_for(SceneManager_t385596982_il2cpp_TypeInfo_var))->set_m_gameScene_2(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SellDefine::.ctor(System.Single,System.Boolean)
extern "C"  void SellDefine__ctor_m822927741 (SellDefine_t4148876514 * __this, float ___soldValue0, bool ___canBeSold1, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		bool L_0 = ___canBeSold1;
		__this->set_mCanBeSold_1(L_0);
		float L_1 = ___soldValue0;
		__this->set_mSoldValue_0(L_1);
		return;
	}
}
// System.Single SellDefine::Sold()
extern "C"  float SellDefine_Sold_m1232112716 (SellDefine_t4148876514 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_mCanBeSold_1();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		float L_1 = __this->get_mSoldValue_0();
		return L_1;
	}

IL_0012:
	{
		return (0.0f);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ShipBobble::.ctor()
extern "C"  void ShipBobble__ctor_m1760285056 (ShipBobble_t3127994465 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShipBobble::Start()
extern "C"  void ShipBobble_Start_m2504219244 (ShipBobble_t3127994465 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		__this->set_mTrans_3(L_0);
		Vector3_t3722313464 * L_1 = __this->get_address_of_mOffset_4();
		float L_2 = Random_Range_m2202990745(NULL /*static, unused*/, (0.0f), (10.0f), /*hidden argument*/NULL);
		L_1->set_x_1(L_2);
		Vector3_t3722313464 * L_3 = __this->get_address_of_mOffset_4();
		float L_4 = Random_Range_m2202990745(NULL /*static, unused*/, (0.0f), (10.0f), /*hidden argument*/NULL);
		L_3->set_y_2(L_4);
		return;
	}
}
// System.Void ShipBobble::Update()
extern "C"  void ShipBobble_Update_m1586417524 (ShipBobble_t3127994465 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipBobble_Update_m1586417524_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float G_B2_0 = 0.0f;
	float G_B1_0 = 0.0f;
	float G_B3_0 = 0.0f;
	float G_B3_1 = 0.0f;
	{
		ShipController_t2808759107 * L_0 = __this->get_control_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		G_B1_0 = (1.0f);
		if (!L_1)
		{
			G_B2_0 = (1.0f);
			goto IL_0026;
		}
	}
	{
		ShipController_t2808759107 * L_2 = __this->get_control_2();
		float L_3 = L_2->get_mSpeed_5();
		G_B3_0 = L_3;
		G_B3_1 = G_B1_0;
		goto IL_002b;
	}

IL_0026:
	{
		G_B3_0 = (0.0f);
		G_B3_1 = G_B2_0;
	}

IL_002b:
	{
		V_0 = ((float)il2cpp_codegen_add((float)G_B3_1, (float)G_B3_0));
		float L_4 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = V_0;
		V_1 = ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5));
		Vector2_t2156229523 * L_6 = __this->get_address_of_mTime_5();
		Vector2_t2156229523 * L_7 = L_6;
		float L_8 = L_7->get_x_0();
		float L_9 = V_1;
		L_7->set_x_0(((float)il2cpp_codegen_add((float)L_8, (float)((float)il2cpp_codegen_multiply((float)L_9, (float)(0.7326f))))));
		Vector2_t2156229523 * L_10 = __this->get_address_of_mTime_5();
		Vector2_t2156229523 * L_11 = L_10;
		float L_12 = L_11->get_y_1();
		float L_13 = V_1;
		L_11->set_y_1(((float)il2cpp_codegen_add((float)L_12, (float)((float)il2cpp_codegen_multiply((float)L_13, (float)(1.2265f))))));
		Vector3_t3722313464 * L_14 = __this->get_address_of_mOffset_4();
		float L_15 = L_14->get_x_1();
		Vector2_t2156229523 * L_16 = __this->get_address_of_mTime_5();
		float L_17 = L_16->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_18 = sinf(((float)il2cpp_codegen_add((float)L_15, (float)L_17)));
		float L_19 = V_0;
		Vector3_t3722313464 * L_20 = __this->get_address_of_mOffset_4();
		float L_21 = L_20->get_y_2();
		Vector2_t2156229523 * L_22 = __this->get_address_of_mTime_5();
		float L_23 = L_22->get_y_1();
		float L_24 = sinf(((float)il2cpp_codegen_add((float)L_21, (float)L_23)));
		float L_25 = V_0;
		Vector3__ctor_m3353183577((&V_2), ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_18, (float)(0.75f))), (float)L_19)), (0.0f), ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_24, (float)(1.5f))), (float)L_25)), /*hidden argument*/NULL);
		Transform_t3600365921 * L_26 = __this->get_mTrans_3();
		Vector3_t3722313464  L_27 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_28 = Quaternion_Euler_m1803555822(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		Transform_set_localRotation_m19445462(L_26, L_28, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ShipCamera::.ctor()
extern "C"  void ShipCamera__ctor_m2096458726 (ShipCamera_t1196952155 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShipCamera::Start()
extern "C"  void ShipCamera_Start_m1208419120 (ShipCamera_t1196952155 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		__this->set_mTrans_5(L_0);
		return;
	}
}
// System.Void ShipCamera::Update()
extern "C"  void ShipCamera_Update_m1783121126 (ShipCamera_t1196952155 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipCamera_Update_m1783121126_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Quaternion_t2301928331  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		ShipController_t2808759107 * L_0 = __this->get_control_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_006c;
		}
	}
	{
		ShipController_t2808759107 * L_2 = __this->get_control_2();
		float L_3 = L_2->get_mSpeed_5();
		V_0 = L_3;
		AnimationCurve_t3046754366 * L_4 = __this->get_angle_4();
		float L_5 = V_0;
		float L_6 = AnimationCurve_Evaluate_m2125563588(L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_7 = Quaternion_Euler_m3049309462(NULL /*static, unused*/, L_6, (0.0f), (0.0f), /*hidden argument*/NULL);
		V_1 = L_7;
		Transform_t3600365921 * L_8 = __this->get_mTrans_5();
		Quaternion_t2301928331  L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_10 = Vector3_get_back_m4077847766(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_11 = Quaternion_op_Multiply_m2607404835(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		AnimationCurve_t3046754366 * L_12 = __this->get_distance_3();
		float L_13 = V_0;
		float L_14 = AnimationCurve_Evaluate_m2125563588(L_12, L_13, /*hidden argument*/NULL);
		Vector3_t3722313464  L_15 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_11, L_14, /*hidden argument*/NULL);
		Transform_set_localPosition_m4128471975(L_8, L_15, /*hidden argument*/NULL);
		Transform_t3600365921 * L_16 = __this->get_mTrans_5();
		Quaternion_t2301928331  L_17 = V_1;
		Transform_set_localRotation_m19445462(L_16, L_17, /*hidden argument*/NULL);
	}

IL_006c:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ShipController::.ctor()
extern "C"  void ShipController__ctor_m2344806045 (ShipController_t2808759107 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_seaGauge_4((0.1f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShipController::Start()
extern "C"  void ShipController_Start_m228069111 (ShipController_t2808759107 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipController_Start_m228069111_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		ShipUnit_t4065899274 * L_1 = Entity_Find_TisShipUnit_t4065899274_m28943516(NULL /*static, unused*/, L_0, /*hidden argument*/Entity_Find_TisShipUnit_t4065899274_m28943516_RuntimeMethod_var);
		__this->set_mShipUnit_6(L_1);
		ShipUnit_t4065899274 * L_2 = __this->get_mShipUnit_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		LayerMask_t3493934918  L_4 = __this->get_raycastMask_3();
		int32_t L_5 = LayerMask_op_Implicit_m3296792737(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		int32_t L_6 = LayerMask_NameToLayer_m2359665122(NULL /*static, unused*/, _stringLiteral78692563, /*hidden argument*/NULL);
		LayerMask_t3493934918  L_7 = LayerMask_op_Implicit_m90232283(NULL /*static, unused*/, ((int32_t)((int32_t)L_5|(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_6&(int32_t)((int32_t)31))))))), /*hidden argument*/NULL);
		__this->set_raycastMask_3(L_7);
		LayerMask_t3493934918  L_8 = __this->get_raycastMask_3();
		int32_t L_9 = LayerMask_op_Implicit_m3296792737(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		LayerMask_t3493934918  L_10 = LayerMask_op_Implicit_m90232283(NULL /*static, unused*/, ((~L_9)), /*hidden argument*/NULL);
		__this->set_raycastMask_3(L_10);
		return;
	}
}
// System.Void ShipController::Update()
extern "C"  void ShipController_Update_m3550679585 (ShipController_t2808759107 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean ShipController::IsShallowWater()
extern "C"  bool ShipController_IsShallowWater_m967017931 (ShipController_t2808759107 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipController_IsShallowWater_m967017931_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3600365921 * V_0 = NULL;
	TransformU5BU5D_t807237628* V_1 = NULL;
	int32_t V_2 = 0;
	{
		TransformU5BU5D_t807237628* L_0 = __this->get_raycastPoints_2();
		if (!L_0)
		{
			goto IL_005c;
		}
	}
	{
		TransformU5BU5D_t807237628* L_1 = __this->get_raycastPoints_2();
		V_1 = L_1;
		V_2 = 0;
		goto IL_0053;
	}

IL_0019:
	{
		TransformU5BU5D_t807237628* L_2 = V_1;
		int32_t L_3 = V_2;
		int32_t L_4 = L_3;
		Transform_t3600365921 * L_5 = (L_2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_4));
		V_0 = L_5;
		Transform_t3600365921 * L_6 = V_0;
		Vector3_t3722313464  L_7 = Transform_get_position_m36019626(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_8 = Vector3_get_down_m3781355428(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = __this->get_m_seaGauge_4();
		LayerMask_t3493934918  L_10 = __this->get_raycastMask_3();
		int32_t L_11 = LayerMask_op_Implicit_m3296792737(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		bool L_12 = Physics_Raycast_m234523501(NULL /*static, unused*/, L_7, L_8, L_9, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_004f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral549941361, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_004f:
	{
		int32_t L_13 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0053:
	{
		int32_t L_14 = V_2;
		TransformU5BU5D_t807237628* L_15 = V_1;
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_15)->max_length)))))))
		{
			goto IL_0019;
		}
	}

IL_005c:
	{
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ShipOrbit::.ctor()
extern "C"  void ShipOrbit__ctor_m2333111908 (ShipOrbit_t50882478 * __this, const RuntimeMethod* method)
{
	{
		__this->set_sensitivity_3((1.0f));
		Vector2_t2156229523  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m3970636864((&L_0), (-20.0f), (20.0f), /*hidden argument*/NULL);
		__this->set_horizontalTiltRange_4(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShipOrbit::Start()
extern "C"  void ShipOrbit_Start_m1509933074 (ShipOrbit_t50882478 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		__this->set_mTrans_5(L_0);
		return;
	}
}
// System.Void ShipOrbit::Update()
extern "C"  void ShipOrbit_Update_m1428905593 (ShipOrbit_t50882478 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipOrbit_Update_m1428905593_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	float V_2 = 0.0f;
	Quaternion_t2301928331  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		ShipController_t2808759107 * L_0 = __this->get_control_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_021c;
		}
	}
	{
		float L_2 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = __this->get_sensitivity_3();
		V_0 = ((float)il2cpp_codegen_multiply((float)L_2, (float)L_3));
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_4 = Input_GetMouseButton_m513753021(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = Application_get_isEditor_m857789090(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_6 = Input_GetMouseButtonUp_m2924350851(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		Cursor_set_visible_m2693238713(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
	}

IL_0040:
	{
		bool L_7 = V_1;
		if (!L_7)
		{
			goto IL_007d;
		}
	}
	{
		Vector2_t2156229523 * L_8 = __this->get_address_of_mInput_6();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_9 = Input_GetAxis_m4009438427(NULL /*static, unused*/, _stringLiteral3403559637, /*hidden argument*/NULL);
		L_8->set_x_0(L_9);
		Vector2_t2156229523 * L_10 = __this->get_address_of_mInput_6();
		float L_11 = Input_GetAxis_m4009438427(NULL /*static, unused*/, _stringLiteral674676282, /*hidden argument*/NULL);
		L_10->set_y_1(L_11);
		float L_12 = V_0;
		V_0 = ((float)il2cpp_codegen_multiply((float)L_12, (float)(300.0f)));
		goto IL_0085;
	}

IL_007d:
	{
		float L_13 = V_0;
		V_0 = ((float)il2cpp_codegen_multiply((float)L_13, (float)(75.0f)));
	}

IL_0085:
	{
		bool L_14 = V_1;
		if (L_14)
		{
			goto IL_00a0;
		}
	}
	{
		Vector2_t2156229523 * L_15 = __this->get_address_of_mInput_6();
		float L_16 = Vector2_get_sqrMagnitude_m837837635(L_15, /*hidden argument*/NULL);
		if ((!(((float)L_16) > ((float)(0.001f)))))
		{
			goto IL_015a;
		}
	}

IL_00a0:
	{
		Vector2_t2156229523 * L_17 = __this->get_address_of_mOffset_7();
		Vector2_t2156229523 * L_18 = L_17;
		float L_19 = L_18->get_x_0();
		Vector2_t2156229523 * L_20 = __this->get_address_of_mInput_6();
		float L_21 = L_20->get_x_0();
		float L_22 = V_0;
		L_18->set_x_0(((float)il2cpp_codegen_add((float)L_19, (float)((float)il2cpp_codegen_multiply((float)L_21, (float)L_22)))));
		Vector2_t2156229523 * L_23 = __this->get_address_of_mOffset_7();
		Vector2_t2156229523 * L_24 = L_23;
		float L_25 = L_24->get_y_1();
		Vector2_t2156229523 * L_26 = __this->get_address_of_mInput_6();
		float L_27 = L_26->get_y_1();
		float L_28 = V_0;
		L_24->set_y_1(((float)il2cpp_codegen_add((float)L_25, (float)((float)il2cpp_codegen_multiply((float)L_27, (float)L_28)))));
		Vector2_t2156229523 * L_29 = __this->get_address_of_mOffset_7();
		Vector2_t2156229523 * L_30 = __this->get_address_of_mOffset_7();
		float L_31 = L_30->get_x_0();
		float L_32 = Tools_WrapAngle_m228518310(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		L_29->set_x_0(L_32);
		Vector2_t2156229523 * L_33 = __this->get_address_of_mOffset_7();
		Vector2_t2156229523 * L_34 = __this->get_address_of_mOffset_7();
		float L_35 = L_34->get_y_1();
		Vector2_t2156229523 * L_36 = __this->get_address_of_horizontalTiltRange_4();
		float L_37 = L_36->get_x_0();
		Vector2_t2156229523 * L_38 = __this->get_address_of_horizontalTiltRange_4();
		float L_39 = L_38->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_40 = Mathf_Clamp_m3350697880(NULL /*static, unused*/, L_35, L_37, L_39, /*hidden argument*/NULL);
		L_33->set_y_1(L_40);
		bool L_41 = V_1;
		if (!L_41)
		{
			goto IL_0155;
		}
	}
	{
		bool L_42 = Application_get_isEditor_m857789090(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_42)
		{
			goto IL_0155;
		}
	}
	{
		Vector2_t2156229523 * L_43 = __this->get_address_of_mOffset_7();
		float L_44 = Vector2_get_magnitude_m2752892833(L_43, /*hidden argument*/NULL);
		if ((!(((float)L_44) > ((float)(10.0f)))))
		{
			goto IL_0155;
		}
	}
	{
		Cursor_set_visible_m2693238713(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
	}

IL_0155:
	{
		goto IL_01ce;
	}

IL_015a:
	{
		Vector2_t2156229523 * L_45 = __this->get_address_of_mOffset_7();
		float L_46 = L_45->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_47 = fabsf(L_46);
		if ((!(((float)L_47) < ((float)(35.0f)))))
		{
			goto IL_01ce;
		}
	}
	{
		float L_48 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		ShipController_t2808759107 * L_49 = __this->get_control_2();
		float L_50 = L_49->get_mSpeed_5();
		V_2 = ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_48, (float)L_50)), (float)(4.0f)));
		Vector2_t2156229523 * L_51 = __this->get_address_of_mOffset_7();
		Vector2_t2156229523 * L_52 = __this->get_address_of_mOffset_7();
		float L_53 = L_52->get_x_0();
		float L_54 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_55 = Mathf_Lerp_m1004423579(NULL /*static, unused*/, L_53, (0.0f), L_54, /*hidden argument*/NULL);
		L_51->set_x_0(L_55);
		Vector2_t2156229523 * L_56 = __this->get_address_of_mOffset_7();
		Vector2_t2156229523 * L_57 = __this->get_address_of_mOffset_7();
		float L_58 = L_57->get_y_1();
		float L_59 = V_2;
		float L_60 = Mathf_Lerp_m1004423579(NULL /*static, unused*/, L_58, (0.0f), L_59, /*hidden argument*/NULL);
		L_56->set_y_1(L_60);
	}

IL_01ce:
	{
		Vector2_t2156229523 * L_61 = __this->get_address_of_mOffset_7();
		float L_62 = L_61->get_y_1();
		Vector2_t2156229523 * L_63 = __this->get_address_of_mOffset_7();
		float L_64 = L_63->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_65 = Quaternion_Euler_m3049309462(NULL /*static, unused*/, ((-L_62)), L_64, (1.0f), /*hidden argument*/NULL);
		V_3 = L_65;
		Transform_t3600365921 * L_66 = __this->get_mTrans_5();
		Transform_t3600365921 * L_67 = __this->get_mTrans_5();
		Quaternion_t2301928331  L_68 = Transform_get_localRotation_m3487911431(L_67, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_69 = V_3;
		float L_70 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_71 = Mathf_Clamp01_m56433566(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)L_70, (float)(10.0f))), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_72 = Quaternion_Slerp_m1234055455(NULL /*static, unused*/, L_68, L_69, L_71, /*hidden argument*/NULL);
		Transform_set_localRotation_m19445462(L_66, L_72, /*hidden argument*/NULL);
	}

IL_021c:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ShipTouchToDestination::.ctor()
extern "C"  void ShipTouchToDestination__ctor_m2626335871 (ShipTouchToDestination_t4292119180 * __this, const RuntimeMethod* method)
{
	{
		ShipController__ctor_m2344806045(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShipTouchToDestination::Start()
extern "C"  void ShipTouchToDestination_Start_m2083482749 (ShipTouchToDestination_t4292119180 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		__this->set_mDestination_8(L_1);
		return;
	}
}
// System.Void ShipTouchToDestination::Update()
extern "C"  void ShipTouchToDestination_Update_m2812452769 (ShipTouchToDestination_t4292119180 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipTouchToDestination_Update_m2812452769_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = __this->get_disableTouches_7();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_1 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = Tools_GetTouchPositionInWater_m548322082(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		Vector3_t3722313464  L_3 = V_0;
		__this->set_mDestination_8(L_3);
	}

IL_0025:
	{
		ShipTouchToDestination_ShipMovement_m2927501072(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShipTouchToDestination::ShipMovement()
extern "C"  void ShipTouchToDestination_ShipMovement_m2927501072 (ShipTouchToDestination_t4292119180 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipTouchToDestination_ShipMovement_m2927501072_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		Vector3_t3722313464  L_2 = __this->get_mDestination_8();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_3 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector3_t3722313464  L_4 = __this->get_mDestination_8();
		Transform_t3600365921 * L_5 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_6 = Transform_get_position_m36019626(L_5, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		ShipUnit_t4065899274 * L_8 = ((ShipController_t2808759107 *)__this)->get_mShipUnit_6();
		float L_9 = ShipUnit_get_turningSpeed_m2858136543(L_8, /*hidden argument*/NULL);
		float L_10 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = ((float)il2cpp_codegen_multiply((float)L_9, (float)L_10));
		Transform_t3600365921 * L_11 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_12 = Transform_get_forward_m747522392(L_11, /*hidden argument*/NULL);
		Vector3_t3722313464  L_13 = V_1;
		float L_14 = V_2;
		Vector3_t3722313464  L_15 = Vector3_RotateTowards_m4112951104(NULL /*static, unused*/, L_12, L_13, L_14, (0.0f), /*hidden argument*/NULL);
		V_3 = L_15;
		Transform_t3600365921 * L_16 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_17 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_18 = Quaternion_LookRotation_m4040767668(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		Transform_set_rotation_m3524318132(L_16, L_18, /*hidden argument*/NULL);
		float L_19 = V_0;
		if ((!(((float)L_19) < ((float)(0.1f)))))
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		bool L_20 = ShipController_IsShallowWater_m967017931(__this, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0096;
		}
	}
	{
		Transform_t3600365921 * L_21 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_22 = Transform_get_position_m36019626(L_21, /*hidden argument*/NULL);
		__this->set_mDestination_8(L_22);
		goto IL_00cc;
	}

IL_0096:
	{
		ShipUnit_t4065899274 * L_23 = ((ShipController_t2808759107 *)__this)->get_mShipUnit_6();
		float L_24 = ShipUnit_get_movementSpeed_m3177165377(L_23, /*hidden argument*/NULL);
		float L_25 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = ((float)il2cpp_codegen_multiply((float)L_24, (float)L_25));
		Transform_t3600365921 * L_26 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_27 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_28 = Transform_get_position_m36019626(L_27, /*hidden argument*/NULL);
		Vector3_t3722313464  L_29 = __this->get_mDestination_8();
		float L_30 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_31 = Vector3_MoveTowards_m2786395547(NULL /*static, unused*/, L_28, L_29, L_30, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_26, L_31, /*hidden argument*/NULL);
	}

IL_00cc:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ShipTouchToMove::.ctor()
extern "C"  void ShipTouchToMove__ctor_m1959022108 (ShipTouchToMove_t1387549923 * __this, const RuntimeMethod* method)
{
	{
		Vector2_t2156229523  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m3970636864((&L_0), (6.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_mSensitivity_7(L_0);
		ShipController__ctor_m2344806045(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShipTouchToMove::Update()
extern "C"  void ShipTouchToMove_Update_m1057204479 (ShipTouchToMove_t1387549923 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipTouchToMove_Update_m1057204479_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)97), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		ShipTouchToMove_ShipMovement_m2549588759(__this, (-1.0f), (0.0f), /*hidden argument*/NULL);
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)113), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0038;
		}
	}
	{
		ShipTouchToMove_ShipMovement_m2549588759(__this, (-1.0f), (-1.0f), /*hidden argument*/NULL);
	}

IL_0038:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)119), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		ShipTouchToMove_ShipMovement_m2549588759(__this, (0.0f), (1.0f), /*hidden argument*/NULL);
	}

IL_0054:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_3 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)115), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0070;
		}
	}
	{
		ShipTouchToMove_ShipMovement_m2549588759(__this, (0.0f), (-1.0f), /*hidden argument*/NULL);
	}

IL_0070:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_4 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)101), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_008c;
		}
	}
	{
		ShipTouchToMove_ShipMovement_m2549588759(__this, (1.0f), (1.0f), /*hidden argument*/NULL);
	}

IL_008c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_5 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)100), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_00a8;
		}
	}
	{
		ShipTouchToMove_ShipMovement_m2549588759(__this, (1.0f), (0.0f), /*hidden argument*/NULL);
	}

IL_00a8:
	{
		return;
	}
}
// System.Void ShipTouchToMove::ShipMovement(System.Single,System.Single)
extern "C"  void ShipTouchToMove_ShipMovement_m2549588759 (ShipTouchToMove_t1387549923 * __this, float ___inputX0, float ___inputY1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipTouchToMove_ShipMovement_m2549588759_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	float V_1 = 0.0f;
	String_t* V_2 = NULL;
	Quaternion_t2301928331  V_3;
	memset(&V_3, 0, sizeof(V_3));
	String_t* V_4 = NULL;
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	ShipTouchToMove_t1387549923 * G_B4_0 = NULL;
	ShipTouchToMove_t1387549923 * G_B3_0 = NULL;
	float G_B5_0 = 0.0f;
	ShipTouchToMove_t1387549923 * G_B5_1 = NULL;
	float G_B7_0 = 0.0f;
	float G_B7_1 = 0.0f;
	float G_B7_2 = 0.0f;
	ShipTouchToMove_t1387549923 * G_B7_3 = NULL;
	float G_B6_0 = 0.0f;
	float G_B6_1 = 0.0f;
	float G_B6_2 = 0.0f;
	ShipTouchToMove_t1387549923 * G_B6_3 = NULL;
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	float G_B8_2 = 0.0f;
	float G_B8_3 = 0.0f;
	ShipTouchToMove_t1387549923 * G_B8_4 = NULL;
	{
		bool L_0 = ShipController_IsShallowWater_m967017931(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		___inputY1 = (0.0f);
	}

IL_0014:
	{
		float L_2 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = __this->get_mTargetSpeed_9();
		float L_4 = V_1;
		float L_5 = __this->get_mTargetSteering_10();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_6 = fabsf(L_5);
		float L_7 = Mathf_Lerp_m1004423579(NULL /*static, unused*/, L_3, (0.0f), ((float)il2cpp_codegen_multiply((float)L_4, (float)((float)il2cpp_codegen_add((float)(0.5f), (float)L_6)))), /*hidden argument*/NULL);
		__this->set_mTargetSpeed_9(L_7);
		float L_8 = __this->get_mTargetSteering_10();
		float L_9 = V_1;
		float L_10 = Mathf_Lerp_m1004423579(NULL /*static, unused*/, L_8, (0.0f), ((float)il2cpp_codegen_multiply((float)L_9, (float)(3.0f))), /*hidden argument*/NULL);
		__this->set_mTargetSteering_10(L_10);
		bool L_11 = V_0;
		G_B3_0 = __this;
		if (!L_11)
		{
			G_B4_0 = __this;
			goto IL_0071;
		}
	}
	{
		G_B5_0 = (0.0f);
		G_B5_1 = G_B3_0;
		goto IL_008c;
	}

IL_0071:
	{
		float L_12 = __this->get_mTargetSpeed_9();
		float L_13 = V_1;
		Vector2_t2156229523 * L_14 = __this->get_address_of_mSensitivity_7();
		float L_15 = L_14->get_y_1();
		float L_16 = ___inputY1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_17 = Mathf_Clamp01_m56433566(NULL /*static, unused*/, ((float)il2cpp_codegen_add((float)L_12, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_13, (float)L_15)), (float)L_16)))), /*hidden argument*/NULL);
		G_B5_0 = L_17;
		G_B5_1 = G_B4_0;
	}

IL_008c:
	{
		G_B5_1->set_mTargetSpeed_9(G_B5_0);
		float L_18 = ((ShipController_t2808759107 *)__this)->get_mSpeed_5();
		float L_19 = __this->get_mTargetSpeed_9();
		float L_20 = V_1;
		bool L_21 = V_0;
		G_B6_0 = L_20;
		G_B6_1 = L_19;
		G_B6_2 = L_18;
		G_B6_3 = __this;
		if (!L_21)
		{
			G_B7_0 = L_20;
			G_B7_1 = L_19;
			G_B7_2 = L_18;
			G_B7_3 = __this;
			goto IL_00af;
		}
	}
	{
		G_B8_0 = (8.0f);
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		G_B8_3 = G_B6_2;
		G_B8_4 = G_B6_3;
		goto IL_00b4;
	}

IL_00af:
	{
		G_B8_0 = (5.0f);
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
		G_B8_3 = G_B7_2;
		G_B8_4 = G_B7_3;
	}

IL_00b4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_22 = Mathf_Clamp01_m56433566(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)G_B8_1, (float)G_B8_0)), /*hidden argument*/NULL);
		float L_23 = Mathf_Lerp_m1004423579(NULL /*static, unused*/, G_B8_3, G_B8_2, L_22, /*hidden argument*/NULL);
		((ShipController_t2808759107 *)G_B8_4)->set_mSpeed_5(L_23);
		float L_24 = __this->get_mTargetSteering_10();
		float L_25 = V_1;
		Vector2_t2156229523 * L_26 = __this->get_address_of_mSensitivity_7();
		float L_27 = L_26->get_x_0();
		float L_28 = ___inputX0;
		float L_29 = ((ShipController_t2808759107 *)__this)->get_mSpeed_5();
		float L_30 = Mathf_Clamp_m3350697880(NULL /*static, unused*/, ((float)il2cpp_codegen_add((float)L_24, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_25, (float)L_27)), (float)L_28)), (float)((float)il2cpp_codegen_add((float)(0.1f), (float)((float)il2cpp_codegen_multiply((float)(0.9f), (float)L_29)))))))), (-1.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_mTargetSteering_10(L_30);
		float L_31 = __this->get_mSteering_8();
		float L_32 = __this->get_mTargetSteering_10();
		float L_33 = V_1;
		float L_34 = Mathf_Lerp_m1004423579(NULL /*static, unused*/, L_31, L_32, ((float)il2cpp_codegen_multiply((float)L_33, (float)(5.0f))), /*hidden argument*/NULL);
		__this->set_mSteering_8(L_34);
		Transform_t3600365921 * L_35 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_36 = Transform_get_localRotation_m3487911431(L_35, /*hidden argument*/NULL);
		V_3 = L_36;
		String_t* L_37 = Quaternion_ToString_m2203056442((&V_3), /*hidden argument*/NULL);
		V_2 = L_37;
		Transform_t3600365921 * L_38 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_39 = Transform_get_localPosition_m4234289348(L_38, /*hidden argument*/NULL);
		V_5 = L_39;
		String_t* L_40 = Vector3_ToString_m759076600((&V_5), /*hidden argument*/NULL);
		V_4 = L_40;
		ShipUnit_t4065899274 * L_41 = ((ShipController_t2808759107 *)__this)->get_mShipUnit_6();
		float L_42 = ShipUnit_get_turningSpeed_m2858136543(L_41, /*hidden argument*/NULL);
		V_6 = L_42;
		Transform_t3600365921 * L_43 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_44 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_45 = Transform_get_localRotation_m3487911431(L_44, /*hidden argument*/NULL);
		float L_46 = __this->get_mSteering_8();
		float L_47 = V_1;
		float L_48 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_49 = Quaternion_Euler_m3049309462(NULL /*static, unused*/, (0.0f), ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_46, (float)L_47)), (float)L_48)), (0.0f), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_50 = Quaternion_op_Multiply_m1294064023(NULL /*static, unused*/, L_45, L_49, /*hidden argument*/NULL);
		Transform_set_localRotation_m19445462(L_43, L_50, /*hidden argument*/NULL);
		Transform_t3600365921 * L_51 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_52 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_53 = Transform_get_localPosition_m4234289348(L_52, /*hidden argument*/NULL);
		Transform_t3600365921 * L_54 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_55 = Transform_get_localRotation_m3487911431(L_54, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_56 = Vector3_get_forward_m3100859705(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_57 = Quaternion_op_Multiply_m2607404835(NULL /*static, unused*/, L_55, L_56, /*hidden argument*/NULL);
		float L_58 = ((ShipController_t2808759107 *)__this)->get_mSpeed_5();
		float L_59 = V_1;
		float L_60 = V_6;
		Vector3_t3722313464  L_61 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_57, ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_58, (float)L_59)), (float)L_60)), /*hidden argument*/NULL);
		Vector3_t3722313464  L_62 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_53, L_61, /*hidden argument*/NULL);
		Transform_set_localPosition_m4128471975(L_51, L_62, /*hidden argument*/NULL);
		ObjectU5BU5D_t2843939325* L_63 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)((int32_t)21)));
		float L_64 = ___inputX0;
		float L_65 = L_64;
		RuntimeObject * L_66 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_65);
		ArrayElementTypeCheck (L_63, L_66);
		(L_63)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_66);
		ObjectU5BU5D_t2843939325* L_67 = L_63;
		ArrayElementTypeCheck (L_67, _stringLiteral3450517380);
		(L_67)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)_stringLiteral3450517380);
		ObjectU5BU5D_t2843939325* L_68 = L_67;
		float L_69 = ___inputY1;
		float L_70 = L_69;
		RuntimeObject * L_71 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_70);
		ArrayElementTypeCheck (L_68, L_71);
		(L_68)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_71);
		ObjectU5BU5D_t2843939325* L_72 = L_68;
		ArrayElementTypeCheck (L_72, _stringLiteral731729006);
		(L_72)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)_stringLiteral731729006);
		ObjectU5BU5D_t2843939325* L_73 = L_72;
		float L_74 = V_1;
		float L_75 = L_74;
		RuntimeObject * L_76 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_75);
		ArrayElementTypeCheck (L_73, L_76);
		(L_73)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_76);
		ObjectU5BU5D_t2843939325* L_77 = L_73;
		ArrayElementTypeCheck (L_77, _stringLiteral820328377);
		(L_77)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)_stringLiteral820328377);
		ObjectU5BU5D_t2843939325* L_78 = L_77;
		float L_79 = __this->get_mTargetSpeed_9();
		float L_80 = L_79;
		RuntimeObject * L_81 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_80);
		ArrayElementTypeCheck (L_78, L_81);
		(L_78)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)L_81);
		ObjectU5BU5D_t2843939325* L_82 = L_78;
		ArrayElementTypeCheck (L_82, _stringLiteral3649710639);
		(L_82)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(7), (RuntimeObject *)_stringLiteral3649710639);
		ObjectU5BU5D_t2843939325* L_83 = L_82;
		float L_84 = __this->get_mTargetSteering_10();
		float L_85 = L_84;
		RuntimeObject * L_86 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_85);
		ArrayElementTypeCheck (L_83, L_86);
		(L_83)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(8), (RuntimeObject *)L_86);
		ObjectU5BU5D_t2843939325* L_87 = L_83;
		ArrayElementTypeCheck (L_87, _stringLiteral1377234670);
		(L_87)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)9)), (RuntimeObject *)_stringLiteral1377234670);
		ObjectU5BU5D_t2843939325* L_88 = L_87;
		float L_89 = ((ShipController_t2808759107 *)__this)->get_mSpeed_5();
		float L_90 = L_89;
		RuntimeObject * L_91 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_90);
		ArrayElementTypeCheck (L_88, L_91);
		(L_88)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)10)), (RuntimeObject *)L_91);
		ObjectU5BU5D_t2843939325* L_92 = L_88;
		ArrayElementTypeCheck (L_92, _stringLiteral793248935);
		(L_92)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)11)), (RuntimeObject *)_stringLiteral793248935);
		ObjectU5BU5D_t2843939325* L_93 = L_92;
		float L_94 = __this->get_mSteering_8();
		float L_95 = L_94;
		RuntimeObject * L_96 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_95);
		ArrayElementTypeCheck (L_93, L_96);
		(L_93)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)12)), (RuntimeObject *)L_96);
		ObjectU5BU5D_t2843939325* L_97 = L_93;
		ArrayElementTypeCheck (L_97, _stringLiteral1926654611);
		(L_97)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)13)), (RuntimeObject *)_stringLiteral1926654611);
		ObjectU5BU5D_t2843939325* L_98 = L_97;
		String_t* L_99 = V_2;
		ArrayElementTypeCheck (L_98, L_99);
		(L_98)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)14)), (RuntimeObject *)L_99);
		ObjectU5BU5D_t2843939325* L_100 = L_98;
		ArrayElementTypeCheck (L_100, _stringLiteral3521318628);
		(L_100)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)15)), (RuntimeObject *)_stringLiteral3521318628);
		ObjectU5BU5D_t2843939325* L_101 = L_100;
		Transform_t3600365921 * L_102 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_103 = Transform_get_localRotation_m3487911431(L_102, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_104 = L_103;
		RuntimeObject * L_105 = Box(Quaternion_t2301928331_il2cpp_TypeInfo_var, &L_104);
		ArrayElementTypeCheck (L_101, L_105);
		(L_101)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)16)), (RuntimeObject *)L_105);
		ObjectU5BU5D_t2843939325* L_106 = L_101;
		ArrayElementTypeCheck (L_106, _stringLiteral3504410480);
		(L_106)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)17)), (RuntimeObject *)_stringLiteral3504410480);
		ObjectU5BU5D_t2843939325* L_107 = L_106;
		String_t* L_108 = V_4;
		ArrayElementTypeCheck (L_107, L_108);
		(L_107)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)18)), (RuntimeObject *)L_108);
		ObjectU5BU5D_t2843939325* L_109 = L_107;
		ArrayElementTypeCheck (L_109, _stringLiteral3450517376);
		(L_109)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)19)), (RuntimeObject *)_stringLiteral3450517376);
		ObjectU5BU5D_t2843939325* L_110 = L_109;
		Transform_t3600365921 * L_111 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_112 = Transform_get_localPosition_m4234289348(L_111, /*hidden argument*/NULL);
		Vector3_t3722313464  L_113 = L_112;
		RuntimeObject * L_114 = Box(Vector3_t3722313464_il2cpp_TypeInfo_var, &L_113);
		ArrayElementTypeCheck (L_110, L_114);
		(L_110)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)20)), (RuntimeObject *)L_114);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_115 = String_Concat_m2971454694(NULL /*static, unused*/, L_110, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_115, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ShipUnit::.ctor()
extern "C"  void ShipUnit__ctor_m3091616017 (ShipUnit_t4065899274 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipUnit__ctor_m3091616017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector2_t2156229523  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m3970636864((&L_0), (100.0f), (100.0f), /*hidden argument*/NULL);
		__this->set_mHealth_2(L_0);
		LevelExp_t2602383666 * L_1 = (LevelExp_t2602383666 *)il2cpp_codegen_object_new(LevelExp_t2602383666_il2cpp_TypeInfo_var);
		LevelExp__ctor_m598325136(L_1, 0, 0, /*hidden argument*/NULL);
		__this->set_mLevelExp_3(L_1);
		__this->set_maxMovementSpeed_4((7.0f));
		__this->set_maxTurningSpeed_5((60.0f));
		__this->set_mConsumeRate_9((1.0f));
		SellDefine_t4148876514 * L_2 = (SellDefine_t4148876514 *)il2cpp_codegen_object_new(SellDefine_t4148876514_il2cpp_TypeInfo_var);
		SellDefine__ctor_m822927741(L_2, (1000.0f), (bool)0, /*hidden argument*/NULL);
		__this->set_mSellValue_11(L_2);
		Entity__ctor_m643651842(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single ShipUnit::get_movementSpeed()
extern "C"  float ShipUnit_get_movementSpeed_m3177165377 (ShipUnit_t4065899274 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_maxMovementSpeed_4();
		return L_0;
	}
}
// System.Single ShipUnit::get_turningSpeed()
extern "C"  float ShipUnit_get_turningSpeed_m2858136543 (ShipUnit_t4065899274 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_maxTurningSpeed_5();
		return L_0;
	}
}
// System.Void ShipUnit::Start()
extern "C"  void ShipUnit_Start_m553162675 (ShipUnit_t4065899274 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		__this->set_mTrans_14(L_0);
		ShipUnit_OnStart_m1772374397(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShipUnit::Update()
extern "C"  void ShipUnit_Update_m1094417079 (ShipUnit_t4065899274 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipUnit_Update_m1094417079_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_mDestroyed_15();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void ShipUnit::LateUpdate()
extern "C"  void ShipUnit_LateUpdate_m2570662116 (ShipUnit_t4065899274 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipUnit_LateUpdate_m2570662116_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t3600365921 * L_0 = __this->get_mTrans_14();
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector3_t3722313464  L_2 = V_0;
		Vector3_t3722313464  L_3 = __this->get_mLastPos_13();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_4 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		float L_5 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_6 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_4, ((float)((float)(1.0f)/(float)L_5)), /*hidden argument*/NULL);
		__this->set_mVelocity_12(L_6);
		Vector3_t3722313464  L_7 = V_0;
		__this->set_mLastPos_13(L_7);
		return;
	}
}
// System.Single ShipUnit::ApplyDamage(System.Single,UnityEngine.GameObject)
extern "C"  float ShipUnit_ApplyDamage_m1888147657 (ShipUnit_t4065899274 * __this, float ___val0, GameObject_t1113636619 * ___go1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShipUnit_ApplyDamage_m1888147657_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_mDestroyed_15();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		return (0.0f);
	}

IL_0011:
	{
		float L_1 = ___val0;
		if ((!(((float)L_1) < ((float)(0.0f)))))
		{
			goto IL_0023;
		}
	}
	{
		___val0 = (0.0f);
	}

IL_0023:
	{
		float L_2 = ___val0;
		float L_3 = __this->get_mDamageReduction_8();
		___val0 = ((float)il2cpp_codegen_multiply((float)L_2, (float)((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_3))));
		Vector2_t2156229523 * L_4 = __this->get_address_of_mHealth_2();
		float L_5 = L_4->get_x_0();
		float L_6 = ___val0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Min_m1073399594(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		___val0 = L_7;
		Vector2_t2156229523 * L_8 = __this->get_address_of_mHealth_2();
		Vector2_t2156229523 * L_9 = L_8;
		float L_10 = L_9->get_x_0();
		float L_11 = ___val0;
		L_9->set_x_0(((float)il2cpp_codegen_subtract((float)L_10, (float)L_11)));
		Vector2_t2156229523 * L_12 = __this->get_address_of_mHealth_2();
		float L_13 = L_12->get_x_0();
		if ((!(((float)L_13) == ((float)(0.0f)))))
		{
			goto IL_0087;
		}
	}
	{
		__this->set_mDestroyed_15((bool)1);
		GameObject_t1113636619 * L_14 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_15 = ___go1;
		GameObject_SendMessage_m3720186693(L_14, _stringLiteral1722375875, L_15, 1, /*hidden argument*/NULL);
	}

IL_0087:
	{
		float L_16 = ___val0;
		return L_16;
	}
}
// System.Void ShipUnit::OnStart()
extern "C"  void ShipUnit_OnStart_m1772374397 (ShipUnit_t4065899274 * __this, const RuntimeMethod* method)
{
	{
		Transform_t3600365921 * L_0 = __this->get_mTrans_14();
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		__this->set_mLastPos_13(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ShowName::.ctor()
extern "C"  void ShowName__ctor_m555671152 (ShowName_t1001792941 * __this, const RuntimeMethod* method)
{
	{
		__this->set_npcHeight_5((2.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShowName::Start()
extern "C"  void ShowName_Start_m3163494608 (ShowName_t1001792941 * __this, const RuntimeMethod* method)
{
	{
		Camera_t4157153871 * L_0 = Camera_get_main_m3643453163(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_camera_2(L_0);
		Color_t2555686324  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Color__ctor_m286683560((&L_1), (0.5f), (0.2f), (0.6f), /*hidden argument*/NULL);
		__this->set_mColor_4(L_1);
		return;
	}
}
// System.Void ShowName::Update()
extern "C"  void ShowName_Update_m3216064794 (ShowName_t1001792941 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void ShowName::OnGUI()
extern "C"  void ShowName_OnGUI_m2312279633 (ShowName_t1001792941 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShowName_OnGUI_m2312279633_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2156229523  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t2156229523  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		String_t* L_0 = Object_get_name_m4211327027(__this, /*hidden argument*/NULL);
		int32_t L_1 = String_get_Length_m3847582255(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3 = Transform_get_position_m36019626(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = (&V_1)->get_x_1();
		Transform_t3600365921 * L_5 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_6 = Transform_get_position_m36019626(L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		float L_7 = (&V_2)->get_y_2();
		float L_8 = __this->get_npcHeight_5();
		Transform_t3600365921 * L_9 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_10 = Transform_get_position_m36019626(L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		float L_11 = (&V_3)->get_z_3();
		Vector3__ctor_m3353183577((&V_0), L_4, ((float)il2cpp_codegen_add((float)L_7, (float)L_8)), L_11, /*hidden argument*/NULL);
		Camera_t4157153871 * L_12 = __this->get_camera_2();
		Vector3_t3722313464  L_13 = V_0;
		Vector3_t3722313464  L_14 = Camera_WorldToScreenPoint_m3726311023(L_12, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_15 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		V_4 = L_15;
		float L_16 = (&V_4)->get_x_0();
		int32_t L_17 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_18 = (&V_4)->get_y_1();
		Vector2__ctor_m3970636864((&V_4), L_16, ((float)il2cpp_codegen_subtract((float)(((float)((float)L_17))), (float)L_18)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUISkin_t1244372282 * L_19 = GUI_get_skin_m1874615010(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_20 = GUISkin_get_label_m1693050720(L_19, /*hidden argument*/NULL);
		String_t* L_21 = Object_get_name_m4211327027(__this, /*hidden argument*/NULL);
		GUIContent_t3050628031 * L_22 = (GUIContent_t3050628031 *)il2cpp_codegen_object_new(GUIContent_t3050628031_il2cpp_TypeInfo_var);
		GUIContent__ctor_m890195579(L_22, L_21, /*hidden argument*/NULL);
		Vector2_t2156229523  L_23 = GUIStyle_CalcSize_m1046812636(L_20, L_22, /*hidden argument*/NULL);
		V_5 = L_23;
		Color_t2555686324  L_24 = __this->get_mColor_4();
		GUI_set_color_m1028198571(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		float L_25 = (&V_4)->get_x_0();
		float L_26 = (&V_5)->get_x_0();
		float L_27 = (&V_4)->get_y_1();
		float L_28 = (&V_5)->get_y_1();
		float L_29 = (&V_5)->get_x_0();
		float L_30 = (&V_5)->get_y_1();
		Rect_t2360479859  L_31;
		memset(&L_31, 0, sizeof(L_31));
		Rect__ctor_m2614021312((&L_31), ((float)il2cpp_codegen_subtract((float)L_25, (float)((float)((float)L_26/(float)(2.0f))))), ((float)il2cpp_codegen_subtract((float)L_27, (float)L_28)), L_29, L_30, /*hidden argument*/NULL);
		String_t* L_32 = Object_get_name_m4211327027(__this, /*hidden argument*/NULL);
		GUI_Label_m2454565404(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShowName::SetText(System.String,UnityEngine.Color)
extern "C"  void ShowName_SetText_m1626712228 (ShowName_t1001792941 * __this, String_t* ___str0, Color_t2555686324  ___color1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___str0;
		__this->set_mName_3(L_0);
		Color_t2555686324  L_1 = ___color1;
		__this->set_mColor_4(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SkillAbility::.ctor()
extern "C"  void SkillAbility__ctor_m1559185056 (SkillAbility_t2516669324 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SkillAbility__ctor_m1559185056_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t437934597 * L_0 = (Dictionary_2_t437934597 *)il2cpp_codegen_object_new(Dictionary_2_t437934597_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3552178049(L_0, /*hidden argument*/Dictionary_2__ctor_m3552178049_RuntimeMethod_var);
		__this->set_mSkillAbilities_0(L_0);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SkillAbility::AddSkill(SkillAbility/SkillValue)
extern "C"  void SkillAbility_AddSkill_m539831979 (SkillAbility_t2516669324 * __this, SkillValue_t3350472883 * ___skillValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SkillAbility_AddSkill_m539831979_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t437934597 * L_0 = __this->get_mSkillAbilities_0();
		SkillValue_t3350472883 * L_1 = ___skillValue0;
		int32_t L_2 = L_1->get_ID_0();
		bool L_3 = Dictionary_2_ContainsKey_m969889043(L_0, L_2, /*hidden argument*/Dictionary_2_ContainsKey_m969889043_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_002d;
		}
	}
	{
		Dictionary_2_t437934597 * L_4 = __this->get_mSkillAbilities_0();
		SkillValue_t3350472883 * L_5 = ___skillValue0;
		int32_t L_6 = L_5->get_ID_0();
		SkillValue_t3350472883 * L_7 = ___skillValue0;
		Dictionary_2_set_Item_m162486917(L_4, L_6, L_7, /*hidden argument*/Dictionary_2_set_Item_m162486917_RuntimeMethod_var);
		goto IL_003f;
	}

IL_002d:
	{
		Dictionary_2_t437934597 * L_8 = __this->get_mSkillAbilities_0();
		SkillValue_t3350472883 * L_9 = ___skillValue0;
		int32_t L_10 = L_9->get_ID_0();
		SkillValue_t3350472883 * L_11 = ___skillValue0;
		Dictionary_2_Add_m1751118572(L_8, L_10, L_11, /*hidden argument*/Dictionary_2_Add_m1751118572_RuntimeMethod_var);
	}

IL_003f:
	{
		return;
	}
}
// System.Void SkillAbility::AddSkill(SkillAbility/SkillEnum,System.Single,SkillAbility/SkillAbilityCallBack)
extern "C"  void SkillAbility_AddSkill_m1907196276 (SkillAbility_t2516669324 * __this, int32_t ___skill0, float ___value1, SkillAbilityCallBack_t2618462130 * ___callback2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SkillAbility_AddSkill_m1907196276_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SkillValue_t3350472883 * V_0 = NULL;
	{
		int32_t L_0 = ___skill0;
		float L_1 = ___value1;
		SkillAbilityCallBack_t2618462130 * L_2 = ___callback2;
		SkillValue_t3350472883 * L_3 = (SkillValue_t3350472883 *)il2cpp_codegen_object_new(SkillValue_t3350472883_il2cpp_TypeInfo_var);
		SkillValue__ctor_m488509249(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		SkillValue_t3350472883 * L_4 = V_0;
		SkillAbility_AddSkill_m539831979(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SkillAbility::ApplySkills()
extern "C"  void SkillAbility_ApplySkills_m2312763334 (SkillAbility_t2516669324 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SkillAbility_ApplySkills_m2312763334_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2835606764  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t2392117372  V_1;
	memset(&V_1, 0, sizeof(V_1));
	SkillValue_t3350472883 * V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t437934597 * L_0 = __this->get_mSkillAbilities_0();
		Enumerator_t2392117372  L_1 = Dictionary_2_GetEnumerator_m1696204037(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m1696204037_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0041;
		}

IL_0011:
		{
			KeyValuePair_2_t2835606764  L_2 = Enumerator_get_Current_m2625386591((&V_1), /*hidden argument*/Enumerator_get_Current_m2625386591_RuntimeMethod_var);
			V_0 = L_2;
			SkillValue_t3350472883 * L_3 = KeyValuePair_2_get_Value_m1855475308((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m1855475308_RuntimeMethod_var);
			V_2 = L_3;
			SkillValue_t3350472883 * L_4 = V_2;
			SkillAbilityCallBack_t2618462130 * L_5 = L_4->get_callback_2();
			SkillAbilityCallBack_Invoke_m4222624785(L_5, /*hidden argument*/NULL);
			SkillValue_t3350472883 * L_6 = V_2;
			String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_8 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2821735793, L_7, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
			Debug_Log_m4051431634(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		}

IL_0041:
		{
			bool L_9 = Enumerator_MoveNext_m2515343744((&V_1), /*hidden argument*/Enumerator_MoveNext_m2515343744_RuntimeMethod_var);
			if (L_9)
			{
				goto IL_0011;
			}
		}

IL_004d:
		{
			IL2CPP_LEAVE(0x60, FINALLY_0052);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0052;
	}

FINALLY_0052:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3555283856((&V_1), /*hidden argument*/Enumerator_Dispose_m3555283856_RuntimeMethod_var);
		IL2CPP_END_FINALLY(82)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(82)
	{
		IL2CPP_JUMP_TBL(0x60, IL_0060)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0060:
	{
		return;
	}
}
// System.Boolean SkillAbility::ApplySkill(SkillAbility/SkillEnum)
extern "C"  bool SkillAbility_ApplySkill_m3978726859 (SkillAbility_t2516669324 * __this, int32_t ___skill0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SkillAbility_ApplySkill_m3978726859_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t437934597 * L_0 = __this->get_mSkillAbilities_0();
		int32_t L_1 = ___skill0;
		bool L_2 = Dictionary_2_ContainsKey_m969889043(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m969889043_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		Dictionary_2_t437934597 * L_3 = __this->get_mSkillAbilities_0();
		int32_t L_4 = ___skill0;
		SkillValue_t3350472883 * L_5 = Dictionary_2_get_Item_m2907616642(L_3, L_4, /*hidden argument*/Dictionary_2_get_Item_m2907616642_RuntimeMethod_var);
		SkillAbilityCallBack_t2618462130 * L_6 = L_5->get_callback_2();
		SkillAbilityCallBack_Invoke_m4222624785(L_6, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0029:
	{
		RuntimeObject * L_7 = Box(SkillEnum_t2332882726_il2cpp_TypeInfo_var, (&___skill0));
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_7);
		___skill0 = *(int32_t*)UnBox(L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral1991331367, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// System.Boolean SkillAbility::ApplySkill(SkillAbility/SkillEnum,System.Single)
extern "C"  bool SkillAbility_ApplySkill_m1333928009 (SkillAbility_t2516669324 * __this, int32_t ___skill0, float ___data1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SkillAbility_ApplySkill_m1333928009_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t437934597 * L_0 = __this->get_mSkillAbilities_0();
		int32_t L_1 = ___skill0;
		bool L_2 = Dictionary_2_ContainsKey_m969889043(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m969889043_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		Dictionary_2_t437934597 * L_3 = __this->get_mSkillAbilities_0();
		int32_t L_4 = ___skill0;
		SkillValue_t3350472883 * L_5 = Dictionary_2_get_Item_m2907616642(L_3, L_4, /*hidden argument*/Dictionary_2_get_Item_m2907616642_RuntimeMethod_var);
		float L_6 = ___data1;
		SkillValue_OnCallback_m1414099155(L_5, L_6, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0025:
	{
		RuntimeObject * L_7 = Box(SkillEnum_t2332882726_il2cpp_TypeInfo_var, (&___skill0));
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_7);
		___skill0 = *(int32_t*)UnBox(L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral1991331367, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  void DelegatePInvokeWrapper_SkillAbilityCallBack_t2618462130 (SkillAbilityCallBack_t2618462130 * __this, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void SkillAbility/SkillAbilityCallBack::.ctor(System.Object,System.IntPtr)
extern "C"  void SkillAbilityCallBack__ctor_m3872213263 (SkillAbilityCallBack_t2618462130 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void SkillAbility/SkillAbilityCallBack::Invoke()
extern "C"  void SkillAbilityCallBack_Invoke_m4222624785 (SkillAbilityCallBack_t2618462130 * __this, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		SkillAbilityCallBack_Invoke_m4222624785((SkillAbilityCallBack_t2618462130 *)__this->get_prev_9(), method);
	}
	Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
	RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
	RuntimeObject* targetThis = __this->get_m_target_2();
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
	bool ___methodIsStatic = MethodIsStatic(targetMethod);
	if (___methodIsStatic)
	{
		if (il2cpp_codegen_method_parameter_count(targetMethod) == 0)
		{
			// open
			typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
			((FunctionPointerType)targetMethodPointer)(NULL, targetMethod);
		}
		else
		{
			// closed
			typedef void (*FunctionPointerType) (RuntimeObject *, void*, const RuntimeMethod*);
			((FunctionPointerType)targetMethodPointer)(NULL, targetThis, targetMethod);
		}
	}
	else
	{
		{
			// closed
			typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
			((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
		}
	}
}
// System.IAsyncResult SkillAbility/SkillAbilityCallBack::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* SkillAbilityCallBack_BeginInvoke_m989854809 (SkillAbilityCallBack_t2618462130 * __this, AsyncCallback_t3962456242 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void SkillAbility/SkillAbilityCallBack::EndInvoke(System.IAsyncResult)
extern "C"  void SkillAbilityCallBack_EndInvoke_m3071425485 (SkillAbilityCallBack_t2618462130 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SkillAbility/SkillValue::.ctor(SkillAbility/SkillEnum,System.Single,SkillAbility/SkillAbilityCallBack)
extern "C"  void SkillValue__ctor_m488509249 (SkillValue_t3350472883 * __this, int32_t ___se0, float ___v1, SkillAbilityCallBack_t2618462130 * ___cb2, const RuntimeMethod* method)
{
	{
		__this->set_ID_0((-1));
		__this->set_value_1((std::numeric_limits<float>::quiet_NaN()));
		__this->set_callbackArg_3((std::numeric_limits<float>::quiet_NaN()));
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___se0;
		__this->set_ID_0(L_0);
		float L_1 = ___v1;
		__this->set_value_1(L_1);
		SkillAbilityCallBack_t2618462130 * L_2 = ___cb2;
		__this->set_callback_2(L_2);
		return;
	}
}
// System.String SkillAbility/SkillValue::ToString()
extern "C"  String_t* SkillValue_ToString_m3839163353 (SkillValue_t3350472883 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SkillValue_ToString_m3839163353_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		int32_t* L_0 = __this->get_address_of_ID_0();
		RuntimeObject * L_1 = Box(SkillEnum_t2332882726_il2cpp_TypeInfo_var, L_0);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		*L_0 = *(int32_t*)UnBox(L_1);
		V_0 = L_2;
		String_t* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m3937257545(NULL /*static, unused*/, L_3, _stringLiteral3787301066, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		float L_6 = __this->get_value_1();
		float L_7 = L_6;
		RuntimeObject * L_8 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_7);
		String_t* L_9 = String_Concat_m904156431(NULL /*static, unused*/, L_5, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		String_t* L_10 = V_0;
		String_t* L_11 = String_Concat_m3937257545(NULL /*static, unused*/, L_10, _stringLiteral2544853177, /*hidden argument*/NULL);
		V_0 = L_11;
		SkillAbilityCallBack_t2618462130 * L_12 = __this->get_callback_2();
		if (!L_12)
		{
			goto IL_005e;
		}
	}
	{
		String_t* L_13 = V_0;
		SkillAbilityCallBack_t2618462130 * L_14 = __this->get_callback_2();
		MethodInfo_t * L_15 = Delegate_get_Method_m3071622864(L_14, /*hidden argument*/NULL);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_15);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m3937257545(NULL /*static, unused*/, L_13, L_16, /*hidden argument*/NULL);
		V_0 = L_17;
	}

IL_005e:
	{
		String_t* L_18 = V_0;
		return L_18;
	}
}
// System.Void SkillAbility/SkillValue::OnCallback(System.Single)
extern "C"  void SkillValue_OnCallback_m1414099155 (SkillValue_t3350472883 * __this, float ___data0, const RuntimeMethod* method)
{
	{
		float L_0 = ___data0;
		__this->set_callbackArg_3(L_0);
		SkillAbilityCallBack_t2618462130 * L_1 = __this->get_callback_2();
		SkillAbilityCallBack_Invoke_m4222624785(L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TestGuiLayout::.ctor()
extern "C"  void TestGuiLayout__ctor_m92583338 (TestGuiLayout_t2138056437 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TestGuiLayout::Start()
extern "C"  void TestGuiLayout_Start_m603216618 (TestGuiLayout_t2138056437 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestGuiLayout_Start_m603216618_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(uGuiLayout_t2579011462_il2cpp_TypeInfo_var);
		RectTransform_t3704657025 * L_0 = uGuiLayout_BeginSubControl_m130035606(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_root_2(L_0);
		RectTransform_t3704657025 * L_1 = __this->get_root_2();
		Canvas_t3310196443 * L_2 = Component_GetComponent_TisCanvas_t3310196443_m782269232(__this, /*hidden argument*/Component_GetComponent_TisCanvas_t3310196443_m782269232_RuntimeMethod_var);
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(L_2, /*hidden argument*/NULL);
		Transform_set_parent_m786917804(L_1, L_3, /*hidden argument*/NULL);
		uGuiLayout_BeginVertical_m2791822590(NULL /*static, unused*/, /*hidden argument*/NULL);
		uGuiLayout_Label_m3379332580(NULL /*static, unused*/, _stringLiteral591802201, _stringLiteral3987835886, /*hidden argument*/NULL);
		uGuiLayout_EndVertical_m228777174(NULL /*static, unused*/, /*hidden argument*/NULL);
		uGuiLayout_EndSubControl_m47764685(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TestGuiLayout::Update()
extern "C"  void TestGuiLayout_Update_m3514040940 (TestGuiLayout_t2138056437 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single Tools::WrapAngle(System.Single)
extern "C"  float Tools_WrapAngle_m228518310 (RuntimeObject * __this /* static, unused */, float ___a0, const RuntimeMethod* method)
{
	{
		goto IL_000e;
	}

IL_0005:
	{
		float L_0 = ___a0;
		___a0 = ((float)il2cpp_codegen_add((float)L_0, (float)(360.0f)));
	}

IL_000e:
	{
		float L_1 = ___a0;
		if ((((float)L_1) < ((float)(-180.0f))))
		{
			goto IL_0005;
		}
	}
	{
		goto IL_0027;
	}

IL_001e:
	{
		float L_2 = ___a0;
		___a0 = ((float)il2cpp_codegen_subtract((float)L_2, (float)(360.0f)));
	}

IL_0027:
	{
		float L_3 = ___a0;
		if ((((float)L_3) > ((float)(180.0f))))
		{
			goto IL_001e;
		}
	}
	{
		float L_4 = ___a0;
		return L_4;
	}
}
// UnityEngine.Rigidbody Tools::GetRigidbody(UnityEngine.Transform)
extern "C"  Rigidbody_t3916780224 * Tools_GetRigidbody_m2837624754 (RuntimeObject * __this /* static, unused */, Transform_t3600365921 * ___trans0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tools_GetRigidbody_m2837624754_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rigidbody_t3916780224 * V_0 = NULL;
	{
		V_0 = (Rigidbody_t3916780224 *)NULL;
		goto IL_0016;
	}

IL_0007:
	{
		Transform_t3600365921 * L_0 = ___trans0;
		Rigidbody_t3916780224 * L_1 = Component_GetComponent_TisRigidbody_t3916780224_m279685075(L_0, /*hidden argument*/Component_GetComponent_TisRigidbody_t3916780224_m279685075_RuntimeMethod_var);
		V_0 = L_1;
		Transform_t3600365921 * L_2 = ___trans0;
		Transform_t3600365921 * L_3 = Transform_get_parent_m835071599(L_2, /*hidden argument*/NULL);
		___trans0 = L_3;
	}

IL_0016:
	{
		Rigidbody_t3916780224 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		Transform_t3600365921 * L_6 = ___trans0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_6, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0007;
		}
	}

IL_002e:
	{
		Rigidbody_t3916780224 * L_8 = V_0;
		return L_8;
	}
}
// System.Collections.Generic.List`1<UnityEngine.Rigidbody> Tools::GetRigidbodies(UnityEngine.Collider[])
extern "C"  List_1_t1093887670 * Tools_GetRigidbodies_m3541047403 (RuntimeObject * __this /* static, unused */, ColliderU5BU5D_t4234922487* ___cols0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tools_GetRigidbodies_m3541047403_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1093887670 * V_0 = NULL;
	Collider_t1773347010 * V_1 = NULL;
	ColliderU5BU5D_t4234922487* V_2 = NULL;
	int32_t V_3 = 0;
	Rigidbody_t3916780224 * V_4 = NULL;
	{
		List_1_t1093887670 * L_0 = (List_1_t1093887670 *)il2cpp_codegen_object_new(List_1_t1093887670_il2cpp_TypeInfo_var);
		List_1__ctor_m1592361844(L_0, /*hidden argument*/List_1__ctor_m1592361844_RuntimeMethod_var);
		V_0 = L_0;
		ColliderU5BU5D_t4234922487* L_1 = ___cols0;
		V_2 = L_1;
		V_3 = 0;
		goto IL_0052;
	}

IL_000f:
	{
		ColliderU5BU5D_t4234922487* L_2 = V_2;
		int32_t L_3 = V_3;
		int32_t L_4 = L_3;
		Collider_t1773347010 * L_5 = (L_2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_4));
		V_1 = L_5;
		Collider_t1773347010 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_6, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004e;
		}
	}
	{
		Collider_t1773347010 * L_8 = V_1;
		Transform_t3600365921 * L_9 = Component_get_transform_m3162698980(L_8, /*hidden argument*/NULL);
		Rigidbody_t3916780224 * L_10 = Tools_GetRigidbody_m2837624754(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		Rigidbody_t3916780224 * L_11 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_11, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_004e;
		}
	}
	{
		List_1_t1093887670 * L_13 = V_0;
		Rigidbody_t3916780224 * L_14 = V_4;
		bool L_15 = List_1_Contains_m2602505829(L_13, L_14, /*hidden argument*/List_1_Contains_m2602505829_RuntimeMethod_var);
		if (L_15)
		{
			goto IL_004e;
		}
	}
	{
		List_1_t1093887670 * L_16 = V_0;
		Rigidbody_t3916780224 * L_17 = V_4;
		List_1_Add_m3007157130(L_16, L_17, /*hidden argument*/List_1_Add_m3007157130_RuntimeMethod_var);
	}

IL_004e:
	{
		int32_t L_18 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_0052:
	{
		int32_t L_19 = V_3;
		ColliderU5BU5D_t4234922487* L_20 = V_2;
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_20)->max_length)))))))
		{
			goto IL_000f;
		}
	}
	{
		List_1_t1093887670 * L_21 = V_0;
		return L_21;
	}
}
// UnityEngine.Bounds Tools::CalculateWorldBounds(UnityEngine.Transform)
extern "C"  Bounds_t2266837910  Tools_CalculateWorldBounds_m2350520555 (RuntimeObject * __this /* static, unused */, Transform_t3600365921 * ___transform0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tools_CalculateWorldBounds_m2350520555_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Collider_t1773347010 * V_0 = NULL;
	Bounds_t2266837910  V_1;
	memset(&V_1, 0, sizeof(V_1));
	ColliderU5BU5D_t4234922487* V_2 = NULL;
	Collider_t1773347010 * V_3 = NULL;
	ColliderU5BU5D_t4234922487* V_4 = NULL;
	int32_t V_5 = 0;
	Bounds_t2266837910  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		Transform_t3600365921 * L_0 = ___transform0;
		Collider_t1773347010 * L_1 = Component_GetComponent_TisCollider_t1773347010_m4226749020(L_0, /*hidden argument*/Component_GetComponent_TisCollider_t1773347010_m4226749020_RuntimeMethod_var);
		V_0 = L_1;
		Collider_t1773347010 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		Collider_t1773347010 * L_4 = V_0;
		Bounds_t2266837910  L_5 = Collider_get_bounds_m2952418672(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		goto IL_002e;
	}

IL_001e:
	{
		Transform_t3600365921 * L_6 = ___transform0;
		Vector3_t3722313464  L_7 = Transform_get_position_m36019626(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_8 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		Bounds_t2266837910  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Bounds__ctor_m1937678907((&L_9), L_7, L_8, /*hidden argument*/NULL);
		G_B3_0 = L_9;
	}

IL_002e:
	{
		V_1 = G_B3_0;
		Transform_t3600365921 * L_10 = ___transform0;
		ColliderU5BU5D_t4234922487* L_11 = Component_GetComponentsInChildren_TisCollider_t1773347010_m2157248342(L_10, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t1773347010_m2157248342_RuntimeMethod_var);
		V_2 = L_11;
		ColliderU5BU5D_t4234922487* L_12 = V_2;
		V_4 = L_12;
		V_5 = 0;
		goto IL_005a;
	}

IL_0041:
	{
		ColliderU5BU5D_t4234922487* L_13 = V_4;
		int32_t L_14 = V_5;
		int32_t L_15 = L_14;
		Collider_t1773347010 * L_16 = (L_13)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_15));
		V_3 = L_16;
		Collider_t1773347010 * L_17 = V_3;
		Bounds_t2266837910  L_18 = Collider_get_bounds_m2952418672(L_17, /*hidden argument*/NULL);
		Bounds_Encapsulate_m1263362003((&V_1), L_18, /*hidden argument*/NULL);
		int32_t L_19 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
	}

IL_005a:
	{
		int32_t L_20 = V_5;
		ColliderU5BU5D_t4234922487* L_21 = V_4;
		if ((((int32_t)L_20) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_21)->max_length)))))))
		{
			goto IL_0041;
		}
	}
	{
		Bounds_t2266837910  L_22 = V_1;
		return L_22;
	}
}
// UnityEngine.Bounds Tools::CalculateLocalBounds(UnityEngine.Transform)
extern "C"  Bounds_t2266837910  Tools_CalculateLocalBounds_m2764462892 (RuntimeObject * __this /* static, unused */, Transform_t3600365921 * ___transform0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tools_CalculateLocalBounds_m2764462892_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Bounds_t2266837910  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t3600365921 * L_0 = ___transform0;
		Bounds_t2266837910  L_1 = Tools_CalculateWorldBounds_m2350520555(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector3_t3722313464  L_2 = Bounds_get_min_m3755135869((&V_0), /*hidden argument*/NULL);
		Transform_t3600365921 * L_3 = ___transform0;
		Vector3_t3722313464  L_4 = Transform_get_position_m36019626(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_5 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		Vector3_t3722313464  L_6 = Bounds_get_max_m3756577669((&V_0), /*hidden argument*/NULL);
		Transform_t3600365921 * L_7 = ___transform0;
		Vector3_t3722313464  L_8 = Transform_get_position_m36019626(L_7, /*hidden argument*/NULL);
		Vector3_t3722313464  L_9 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		Bounds_t2266837910  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Bounds__ctor_m1937678907((&L_10), L_5, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Single Tools::DistanceToPlane(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Tools_DistanceToPlane_m2293493925 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___planeOrigin0, Vector3_t3722313464  ___planeNormal1, Vector3_t3722313464  ___rayOrigin2, Vector3_t3722313464  ___rayNormal3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tools_DistanceToPlane_m2293493925_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		Vector3_t3722313464  L_0 = ___planeOrigin0;
		Vector3_t3722313464  L_1 = ___planeNormal1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_2 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t3722313464  L_3 = ___planeNormal1;
		Vector3_t3722313464  L_4 = ___rayOrigin2;
		float L_5 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Vector3_t3722313464  L_6 = ___planeNormal1;
		Vector3_t3722313464  L_7 = ___rayNormal3;
		float L_8 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		float L_9 = V_1;
		float L_10 = V_0;
		float L_11 = V_2;
		return ((-((float)((float)((float)il2cpp_codegen_add((float)L_9, (float)L_10))/(float)L_11))));
	}
}
// System.Int32 Tools::EncodeFloatToInt(System.Single)
extern "C"  int32_t Tools_EncodeFloatToInt_m1804285472 (RuntimeObject * __this /* static, unused */, float ___val0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tools_EncodeFloatToInt_m1804285472_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		float L_0 = ___val0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_1 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_3 = V_0;
		V_0 = ((-L_3));
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4<<(int32_t)1));
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5|(int32_t)1));
		goto IL_0022;
	}

IL_001e:
	{
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6<<(int32_t)1));
	}

IL_0022:
	{
		int32_t L_7 = V_0;
		return L_7;
	}
}
// System.Int32 Tools::GetPositionID(UnityEngine.Vector3)
extern "C"  int32_t Tools_GetPositionID_m3688850456 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___pos0, const RuntimeMethod* method)
{
	{
		float L_0 = (&___pos0)->get_x_1();
		int32_t L_1 = Tools_EncodeFloatToInt_m1804285472(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = (&___pos0)->get_z_3();
		int32_t L_3 = Tools_EncodeFloatToInt_m1804285472(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_1<<(int32_t)((int32_t)16)))|(int32_t)L_3));
	}
}
// System.Boolean Tools::IsChild(UnityEngine.Transform,UnityEngine.Transform)
extern "C"  bool Tools_IsChild_m3091374981 (RuntimeObject * __this /* static, unused */, Transform_t3600365921 * ___parent0, Transform_t3600365921 * ___child1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tools_IsChild_m3091374981_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3600365921 * L_0 = ___parent0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		Transform_t3600365921 * L_2 = ___child1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001a;
		}
	}

IL_0018:
	{
		return (bool)0;
	}

IL_001a:
	{
		goto IL_0035;
	}

IL_001f:
	{
		Transform_t3600365921 * L_4 = ___child1;
		Transform_t3600365921 * L_5 = ___parent0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002d;
		}
	}
	{
		return (bool)1;
	}

IL_002d:
	{
		Transform_t3600365921 * L_7 = ___child1;
		Transform_t3600365921 * L_8 = Transform_get_parent_m835071599(L_7, /*hidden argument*/NULL);
		___child1 = L_8;
	}

IL_0035:
	{
		Transform_t3600365921 * L_9 = ___child1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_9, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_001f;
		}
	}
	{
		return (bool)0;
	}
}
// System.String Tools::GetHierarchy(UnityEngine.GameObject)
extern "C"  String_t* Tools_GetHierarchy_m808099638 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tools_GetHierarchy_m808099638_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = ___obj0;
		String_t* L_1 = Object_get_name_m4211327027(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3452614529, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_003a;
	}

IL_0016:
	{
		GameObject_t1113636619 * L_3 = ___obj0;
		Transform_t3600365921 * L_4 = GameObject_get_transform_m1369836730(L_3, /*hidden argument*/NULL);
		Transform_t3600365921 * L_5 = Transform_get_parent_m835071599(L_4, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_6 = Component_get_gameObject_m442555142(L_5, /*hidden argument*/NULL);
		___obj0 = L_6;
		GameObject_t1113636619 * L_7 = ___obj0;
		String_t* L_8 = Object_get_name_m4211327027(L_7, /*hidden argument*/NULL);
		String_t* L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral3452614529, L_8, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
	}

IL_003a:
	{
		GameObject_t1113636619 * L_11 = ___obj0;
		Transform_t3600365921 * L_12 = GameObject_get_transform_m1369836730(L_11, /*hidden argument*/NULL);
		Transform_t3600365921 * L_13 = Transform_get_parent_m835071599(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_13, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0016;
		}
	}
	{
		String_t* L_15 = V_0;
		return L_15;
	}
}
// System.Boolean Tools::IsWebAddress(System.String)
extern "C"  bool Tools_IsWebAddress_m1893856580 (RuntimeObject * __this /* static, unused */, String_t* ___s0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tools_IsWebAddress_m1893856580_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___s0;
		bool L_1 = String_StartsWith_m2640722675(L_0, _stringLiteral1973861598, 5, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_2 = ___s0;
		bool L_3 = String_StartsWith_m2640722675(L_2, _stringLiteral452222980, 5, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0020;
	}

IL_001f:
	{
		G_B3_0 = 1;
	}

IL_0020:
	{
		return (bool)G_B3_0;
	}
}
// System.String Tools::GetFullPath(System.String)
extern "C"  String_t* Tools_GetFullPath_m3144299145 (RuntimeObject * __this /* static, unused */, String_t* ___s0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tools_GetFullPath_m3144299145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	{
		String_t* L_0 = Application_get_dataPath_m4232621142(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = V_0;
		bool L_2 = String_EndsWith_m1901926500(L_1, _stringLiteral3452614529, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m3937257545(NULL /*static, unused*/, L_3, _stringLiteral3452614529, /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_0022:
	{
		String_t* L_5 = V_0;
		String_t* L_6 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m3937257545(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		String_t* L_8 = V_0;
		String_t* L_9 = String_Replace_m1273907647(L_8, _stringLiteral3452614644, _stringLiteral3452614529, /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0094;
	}

IL_0040:
	{
		String_t* L_10 = V_0;
		int32_t L_11 = String_IndexOf_m1977622757(L_10, _stringLiteral3139614613, /*hidden argument*/NULL);
		V_1 = L_11;
		int32_t L_12 = V_1;
		if ((!(((uint32_t)L_12) == ((uint32_t)(-1)))))
		{
			goto IL_0058;
		}
	}
	{
		goto IL_0099;
	}

IL_0058:
	{
		String_t* L_13 = V_0;
		int32_t L_14 = V_1;
		String_t* L_15 = String_Remove_m1524948975(L_13, L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		String_t* L_16 = V_0;
		int32_t L_17 = V_1;
		String_t* L_18 = String_Substring_m2848979100(L_16, ((int32_t)il2cpp_codegen_add((int32_t)L_17, (int32_t)4)), /*hidden argument*/NULL);
		V_3 = L_18;
		String_t* L_19 = V_2;
		int32_t L_20 = String_LastIndexOf_m3451222878(L_19, ((int32_t)47), /*hidden argument*/NULL);
		V_1 = L_20;
		int32_t L_21 = V_1;
		if ((!(((uint32_t)L_21) == ((uint32_t)(-1)))))
		{
			goto IL_007f;
		}
	}
	{
		goto IL_0099;
	}

IL_007f:
	{
		String_t* L_22 = V_2;
		int32_t L_23 = V_1;
		String_t* L_24 = String_Remove_m1524948975(L_22, L_23, /*hidden argument*/NULL);
		V_2 = L_24;
		String_t* L_25 = V_2;
		String_t* L_26 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m3755062657(NULL /*static, unused*/, L_25, _stringLiteral3452614529, L_26, /*hidden argument*/NULL);
		V_0 = L_27;
	}

IL_0094:
	{
		goto IL_0040;
	}

IL_0099:
	{
		String_t* L_28 = V_0;
		return L_28;
	}
}
// System.Single Tools::Ramp(System.Single,System.Single)
extern "C"  float Tools_Ramp_m3609940933 (RuntimeObject * __this /* static, unused */, float ___val0, float ___factor1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tools_Ramp_m3609940933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = ___val0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Sign_m3457838305(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = ___val0;
		float L_3 = V_0;
		___val0 = ((float)il2cpp_codegen_multiply((float)L_2, (float)L_3));
		float L_4 = V_0;
		float L_5 = ___val0;
		float L_6 = ___val0;
		float L_7 = ___val0;
		float L_8 = ___factor1;
		float L_9 = Mathf_Lerp_m1004423579(NULL /*static, unused*/, L_5, ((float)il2cpp_codegen_multiply((float)L_6, (float)L_7)), L_8, /*hidden argument*/NULL);
		return ((float)il2cpp_codegen_multiply((float)L_4, (float)L_9));
	}
}
// System.Boolean Tools::Deviates(System.Single,System.Single,System.Single)
extern "C"  bool Tools_Deviates_m3723987936 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, float ___units2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tools_Deviates_m3723987936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___a0;
		float L_1 = ___b1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_2 = fabsf(((float)il2cpp_codegen_subtract((float)L_0, (float)L_1)));
		float L_3 = ___units2;
		return (bool)((((float)L_2) > ((float)L_3))? 1 : 0);
	}
}
// System.Boolean Tools::Deviates(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  bool Tools_Deviates_m728866459 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___a0, Vector3_t3722313464  ___b1, float ___units2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tools_Deviates_m728866459_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t G_B4_0 = 0;
	{
		Vector3_t3722313464  L_0 = ___a0;
		Vector3_t3722313464  L_1 = ___b1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_4 = fabsf(L_3);
		float L_5 = ___units2;
		if ((((float)L_4) > ((float)L_5)))
		{
			goto IL_003d;
		}
	}
	{
		float L_6 = (&V_0)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_7 = fabsf(L_6);
		float L_8 = ___units2;
		if ((((float)L_7) > ((float)L_8)))
		{
			goto IL_003d;
		}
	}
	{
		float L_9 = (&V_0)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_10 = fabsf(L_9);
		float L_11 = ___units2;
		G_B4_0 = ((((float)L_10) > ((float)L_11))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B4_0 = 1;
	}

IL_003e:
	{
		return (bool)G_B4_0;
	}
}
// System.Void Tools::Broadcast(System.String)
extern "C"  void Tools_Broadcast_m1011643776 (RuntimeObject * __this /* static, unused */, String_t* ___funcName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tools_Broadcast_m1011643776_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObjectU5BU5D_t3328599146* V_0 = NULL;
	GameObject_t1113636619 * V_1 = NULL;
	GameObjectU5BU5D_t3328599146* V_2 = NULL;
	int32_t V_3 = 0;
	{
		RuntimeTypeHandle_t3027515415  L_0 = { reinterpret_cast<intptr_t> (GameObject_t1113636619_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t1417781964* L_2 = Object_FindObjectsOfType_m2295101757(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = ((GameObjectU5BU5D_t3328599146*)IsInst((RuntimeObject*)L_2, GameObjectU5BU5D_t3328599146_il2cpp_TypeInfo_var));
		GameObjectU5BU5D_t3328599146* L_3 = V_0;
		V_2 = L_3;
		V_3 = 0;
		goto IL_002e;
	}

IL_001e:
	{
		GameObjectU5BU5D_t3328599146* L_4 = V_2;
		int32_t L_5 = V_3;
		int32_t L_6 = L_5;
		GameObject_t1113636619 * L_7 = (L_4)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_6));
		V_1 = L_7;
		GameObject_t1113636619 * L_8 = V_1;
		String_t* L_9 = ___funcName0;
		GameObject_SendMessage_m1121218340(L_8, L_9, 1, /*hidden argument*/NULL);
		int32_t L_10 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_002e:
	{
		int32_t L_11 = V_3;
		GameObjectU5BU5D_t3328599146* L_12 = V_2;
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_12)->max_length)))))))
		{
			goto IL_001e;
		}
	}
	{
		return;
	}
}
// System.Boolean Tools::GetTouchPositionInGame(System.Int32,UnityEngine.Vector3&)
extern "C"  bool Tools_GetTouchPositionInGame_m4201674753 (RuntimeObject * __this /* static, unused */, int32_t ___layerMask0, Vector3_t3722313464 * ___touchPosInGame1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tools_GetTouchPositionInGame_m4201674753_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	Touch_t1921856868  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Touch_t1921856868  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Ray_t3785851493  V_4;
	memset(&V_4, 0, sizeof(V_4));
	RaycastHit_t1056001966  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_0 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_t3722313464 * L_1 = ___touchPosInGame1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)L_1 = L_2;
		V_1 = (bool)0;
		Camera_t4157153871 * L_3 = Camera_get_main_m3643453163(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_3, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0025;
		}
	}
	{
		return (bool)0;
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		int32_t L_5 = Input_get_touchCount_m3403849067(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0059;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Touch_t1921856868  L_6 = Input_GetTouch_m2192712756(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_2 = L_6;
		int32_t L_7 = Touch_get_phase_m214549210((&V_2), /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0059;
		}
	}
	{
		V_1 = (bool)1;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Touch_t1921856868  L_8 = Input_GetTouch_m2192712756(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_3 = L_8;
		Vector2_t2156229523  L_9 = Touch_get_position_m3109777936((&V_3), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0078;
	}

IL_0059:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_10 = Input_GetMouseButtonDown_m2081676745(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0076;
		}
	}
	{
		V_1 = (bool)1;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_11 = Input_get_mousePosition_m1616496925(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_12 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0078;
	}

IL_0076:
	{
		return (bool)0;
	}

IL_0078:
	{
		Camera_t4157153871 * L_13 = Camera_get_main_m3643453163(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2156229523  L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_15 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Ray_t3785851493  L_16 = Camera_ScreenPointToRay_m3764635188(L_13, L_15, /*hidden argument*/NULL);
		V_4 = L_16;
		Ray_t3785851493  L_17 = V_4;
		int32_t L_18 = ___layerMask0;
		bool L_19 = Physics_Raycast_m1893809531(NULL /*static, unused*/, L_17, (&V_5), (std::numeric_limits<float>::infinity()), L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00c7;
		}
	}
	{
		Vector3_t3722313464 * L_20 = ___touchPosInGame1;
		Vector3_t3722313464  L_21 = RaycastHit_get_point_m2236647085((&V_5), /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)L_20 = L_21;
		Vector3_t3722313464 * L_22 = ___touchPosInGame1;
		Vector3_t3722313464  L_23 = (*(Vector3_t3722313464 *)L_22);
		RuntimeObject * L_24 = Box(Vector3_t3722313464_il2cpp_TypeInfo_var, &L_23);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral2426591911, L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_00c7:
	{
		return (bool)0;
	}
}
// System.Boolean Tools::GetTouchPositionInWater(UnityEngine.Vector3&)
extern "C"  bool Tools_GetTouchPositionInWater_m548322082 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464 * ___touchPosInGame0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tools_GetTouchPositionInWater_m548322082_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = LayerMask_NameToLayer_m2359665122(NULL /*static, unused*/, _stringLiteral78692563, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)31)))));
		int32_t L_1 = V_0;
		Vector3_t3722313464 * L_2 = ___touchPosInGame0;
		bool L_3 = Tools_GetTouchPositionInGame_m4201674753(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single Tools::SignedAngleBetween(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Tools_SignedAngleBetween_m19171509 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___a0, Vector3_t3722313464  ___b1, Vector3_t3722313464  ___n2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tools_SignedAngleBetween_m19171509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		Vector3_t3722313464  L_0 = ___a0;
		Vector3_t3722313464  L_1 = ___b1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_2 = Vector3_Angle_m3731191531(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t3722313464  L_3 = ___n2;
		Vector3_t3722313464  L_4 = ___a0;
		Vector3_t3722313464  L_5 = ___b1;
		Vector3_t3722313464  L_6 = Vector3_Cross_m418170344(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		float L_7 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_8 = Mathf_Sign_m3457838305(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		float L_9 = V_0;
		float L_10 = V_1;
		V_2 = ((float)il2cpp_codegen_multiply((float)L_9, (float)L_10));
		float L_11 = V_2;
		return L_11;
	}
}
// UnityEngine.Quaternion Tools::LookAtPlayerOnYAxis(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t2301928331  Tools_LookAtPlayerOnYAxis_m199382236 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  ___srcPos0, Vector3_t3722313464  ___targetPos1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tools_LookAtPlayerOnYAxis_m199382236_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t2301928331  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Vector3_t3722313464  L_0 = ___targetPos1;
		Vector3_t3722313464  L_1 = ___srcPos0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_3 = Quaternion_LookRotation_m4040767668(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Vector3_t3722313464  L_4 = Quaternion_get_eulerAngles_m3425202016((&V_1), /*hidden argument*/NULL);
		V_0 = L_4;
		(&V_0)->set_x_1((0.0f));
		(&V_0)->set_z_3((0.0f));
		Vector3_t3722313464  L_5 = V_0;
		Quaternion_t2301928331  L_6 = Quaternion_Euler_m1803555822(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void uGuiLayout::.ctor()
extern "C"  void uGuiLayout__ctor_m590412008 (uGuiLayout_t2579011462 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void uGuiLayout::BeginHorizontal()
extern "C"  void uGuiLayout_BeginHorizontal_m3128919469 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (uGuiLayout_BeginHorizontal_m3128919469_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectTransform_t3704657025 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(uGuiLayout_t2579011462_il2cpp_TypeInfo_var);
		Stack_1_t2240656229 * L_0 = ((uGuiLayout_t2579011462_StaticFields*)il2cpp_codegen_static_fields_for(uGuiLayout_t2579011462_il2cpp_TypeInfo_var))->get_positionCache_2();
		float L_1 = (((uGuiLayout_t2579011462_StaticFields*)il2cpp_codegen_static_fields_for(uGuiLayout_t2579011462_il2cpp_TypeInfo_var))->get_address_of_currentPosition_1())->get_x_0();
		Stack_1_Push_m2927642228(L_0, L_1, /*hidden argument*/Stack_1_Push_m2927642228_RuntimeMethod_var);
		Stack_1_t4010345780 * L_2 = ((uGuiLayout_t2579011462_StaticFields*)il2cpp_codegen_static_fields_for(uGuiLayout_t2579011462_il2cpp_TypeInfo_var))->get_modeCache_3();
		Stack_1_Push_m3148378454(L_2, 0, /*hidden argument*/Stack_1_Push_m3148378454_RuntimeMethod_var);
		RectTransform_t3704657025 * L_3 = uGuiLayout_BeginSubControl_m130035606(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_3;
		RectTransform_t3704657025 * L_4 = V_0;
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(L_4, /*hidden argument*/NULL);
		GameObject_AddComponent_TisHorizontalLayoutGroup_t2586782146_m3608050281(L_5, /*hidden argument*/GameObject_AddComponent_TisHorizontalLayoutGroup_t2586782146_m3608050281_RuntimeMethod_var);
		return;
	}
}
// System.Void uGuiLayout::EndHorizontal()
extern "C"  void uGuiLayout_EndHorizontal_m3903317722 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (uGuiLayout_EndHorizontal_m3903317722_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(uGuiLayout_t2579011462_il2cpp_TypeInfo_var);
		Stack_1_t2240656229 * L_0 = ((uGuiLayout_t2579011462_StaticFields*)il2cpp_codegen_static_fields_for(uGuiLayout_t2579011462_il2cpp_TypeInfo_var))->get_positionCache_2();
		float L_1 = Stack_1_Pop_m3170417723(L_0, /*hidden argument*/Stack_1_Pop_m3170417723_RuntimeMethod_var);
		(((uGuiLayout_t2579011462_StaticFields*)il2cpp_codegen_static_fields_for(uGuiLayout_t2579011462_il2cpp_TypeInfo_var))->get_address_of_currentPosition_1())->set_x_0(L_1);
		Stack_1_t4010345780 * L_2 = ((uGuiLayout_t2579011462_StaticFields*)il2cpp_codegen_static_fields_for(uGuiLayout_t2579011462_il2cpp_TypeInfo_var))->get_modeCache_3();
		Stack_1_Pop_m2243551095(L_2, /*hidden argument*/Stack_1_Pop_m2243551095_RuntimeMethod_var);
		uGuiLayout_EndSubControl_m47764685(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void uGuiLayout::BeginVertical()
extern "C"  void uGuiLayout_BeginVertical_m2791822590 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (uGuiLayout_BeginVertical_m2791822590_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectTransform_t3704657025 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(uGuiLayout_t2579011462_il2cpp_TypeInfo_var);
		uGuiLayout_BeginSubControl_m130035606(NULL /*static, unused*/, /*hidden argument*/NULL);
		Stack_1_t2240656229 * L_0 = ((uGuiLayout_t2579011462_StaticFields*)il2cpp_codegen_static_fields_for(uGuiLayout_t2579011462_il2cpp_TypeInfo_var))->get_positionCache_2();
		float L_1 = (((uGuiLayout_t2579011462_StaticFields*)il2cpp_codegen_static_fields_for(uGuiLayout_t2579011462_il2cpp_TypeInfo_var))->get_address_of_currentPosition_1())->get_y_1();
		Stack_1_Push_m2927642228(L_0, L_1, /*hidden argument*/Stack_1_Push_m2927642228_RuntimeMethod_var);
		Stack_1_t4010345780 * L_2 = ((uGuiLayout_t2579011462_StaticFields*)il2cpp_codegen_static_fields_for(uGuiLayout_t2579011462_il2cpp_TypeInfo_var))->get_modeCache_3();
		Stack_1_Push_m3148378454(L_2, 1, /*hidden argument*/Stack_1_Push_m3148378454_RuntimeMethod_var);
		RectTransform_t3704657025 * L_3 = uGuiLayout_BeginSubControl_m130035606(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_3;
		RectTransform_t3704657025 * L_4 = V_0;
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(L_4, /*hidden argument*/NULL);
		GameObject_AddComponent_TisVerticalLayoutGroup_t923838031_m1306033316(L_5, /*hidden argument*/GameObject_AddComponent_TisVerticalLayoutGroup_t923838031_m1306033316_RuntimeMethod_var);
		return;
	}
}
// System.Void uGuiLayout::EndVertical()
extern "C"  void uGuiLayout_EndVertical_m228777174 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (uGuiLayout_EndVertical_m228777174_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(uGuiLayout_t2579011462_il2cpp_TypeInfo_var);
		Stack_1_t2240656229 * L_0 = ((uGuiLayout_t2579011462_StaticFields*)il2cpp_codegen_static_fields_for(uGuiLayout_t2579011462_il2cpp_TypeInfo_var))->get_positionCache_2();
		float L_1 = Stack_1_Pop_m3170417723(L_0, /*hidden argument*/Stack_1_Pop_m3170417723_RuntimeMethod_var);
		(((uGuiLayout_t2579011462_StaticFields*)il2cpp_codegen_static_fields_for(uGuiLayout_t2579011462_il2cpp_TypeInfo_var))->get_address_of_currentPosition_1())->set_y_1(L_1);
		Stack_1_t4010345780 * L_2 = ((uGuiLayout_t2579011462_StaticFields*)il2cpp_codegen_static_fields_for(uGuiLayout_t2579011462_il2cpp_TypeInfo_var))->get_modeCache_3();
		Stack_1_Pop_m2243551095(L_2, /*hidden argument*/Stack_1_Pop_m2243551095_RuntimeMethod_var);
		uGuiLayout_EndSubControl_m47764685(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RectTransform uGuiLayout::BeginSubControl()
extern "C"  RectTransform_t3704657025 * uGuiLayout_BeginSubControl_m130035606 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (uGuiLayout_BeginSubControl_m130035606_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	RectTransform_t3704657025 * V_1 = NULL;
	{
		GameObject_t1113636619 * L_0 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m3707688467(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t1113636619 * L_1 = V_0;
		RectTransform_t3704657025 * L_2 = GameObject_AddComponent_TisRectTransform_t3704657025_m3669823029(L_1, /*hidden argument*/GameObject_AddComponent_TisRectTransform_t3704657025_m3669823029_RuntimeMethod_var);
		V_1 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(uGuiLayout_t2579011462_il2cpp_TypeInfo_var);
		Stack_1_t253079184 * L_3 = ((uGuiLayout_t2579011462_StaticFields*)il2cpp_codegen_static_fields_for(uGuiLayout_t2579011462_il2cpp_TypeInfo_var))->get_subControls_0();
		int32_t L_4 = Stack_1_get_Count_m545179475(L_3, /*hidden argument*/Stack_1_get_Count_m545179475_RuntimeMethod_var);
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_002d;
		}
	}
	{
		RectTransform_t3704657025 * L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(uGuiLayout_t2579011462_il2cpp_TypeInfo_var);
		Stack_1_t253079184 * L_6 = ((uGuiLayout_t2579011462_StaticFields*)il2cpp_codegen_static_fields_for(uGuiLayout_t2579011462_il2cpp_TypeInfo_var))->get_subControls_0();
		RectTransform_t3704657025 * L_7 = Stack_1_Peek_m855668811(L_6, /*hidden argument*/Stack_1_Peek_m855668811_RuntimeMethod_var);
		Transform_set_parent_m786917804(L_5, L_7, /*hidden argument*/NULL);
	}

IL_002d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(uGuiLayout_t2579011462_il2cpp_TypeInfo_var);
		Stack_1_t253079184 * L_8 = ((uGuiLayout_t2579011462_StaticFields*)il2cpp_codegen_static_fields_for(uGuiLayout_t2579011462_il2cpp_TypeInfo_var))->get_subControls_0();
		RectTransform_t3704657025 * L_9 = V_1;
		Stack_1_Push_m811146802(L_8, L_9, /*hidden argument*/Stack_1_Push_m811146802_RuntimeMethod_var);
		RectTransform_t3704657025 * L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_11 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m4126691837(L_10, L_11, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_12 = V_1;
		Vector2_t2156229523  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector2__ctor_m3970636864((&L_13), (0.0f), (1.0f), /*hidden argument*/NULL);
		RectTransform_set_anchorMin_m4230103102(L_12, L_13, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_14 = V_1;
		Vector2_t2156229523  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Vector2__ctor_m3970636864((&L_15), (0.0f), (1.0f), /*hidden argument*/NULL);
		RectTransform_set_anchorMax_m2998668828(L_14, L_15, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_16 = V_1;
		Vector2_t2156229523  L_17 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		RectTransform_set_offsetMax_m2526664592(L_16, L_17, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_18 = V_1;
		Vector2_t2156229523  L_19 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		RectTransform_set_offsetMin_m1512629941(L_18, L_19, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_20 = V_1;
		Vector2_t2156229523  L_21 = ((uGuiLayout_t2579011462_StaticFields*)il2cpp_codegen_static_fields_for(uGuiLayout_t2579011462_il2cpp_TypeInfo_var))->get_currentPosition_1();
		RectTransform_set_pivot_m909387058(L_20, L_21, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_22 = V_1;
		return L_22;
	}
}
// System.Void uGuiLayout::EndSubControl()
extern "C"  void uGuiLayout_EndSubControl_m47764685 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (uGuiLayout_EndSubControl_m47764685_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(uGuiLayout_t2579011462_il2cpp_TypeInfo_var);
		Stack_1_t253079184 * L_0 = ((uGuiLayout_t2579011462_StaticFields*)il2cpp_codegen_static_fields_for(uGuiLayout_t2579011462_il2cpp_TypeInfo_var))->get_subControls_0();
		Stack_1_Pop_m1483992283(L_0, /*hidden argument*/Stack_1_Pop_m1483992283_RuntimeMethod_var);
		return;
	}
}
// System.Void uGuiLayout::AddChild(UnityEngine.RectTransform)
extern "C"  void uGuiLayout_AddChild_m2495970376 (RuntimeObject * __this /* static, unused */, RectTransform_t3704657025 * ___trans0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (uGuiLayout_AddChild_m2495970376_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RectTransform_t3704657025 * L_0 = ___trans0;
		IL2CPP_RUNTIME_CLASS_INIT(uGuiLayout_t2579011462_il2cpp_TypeInfo_var);
		Stack_1_t253079184 * L_1 = ((uGuiLayout_t2579011462_StaticFields*)il2cpp_codegen_static_fields_for(uGuiLayout_t2579011462_il2cpp_TypeInfo_var))->get_subControls_0();
		RectTransform_t3704657025 * L_2 = Stack_1_Peek_m855668811(L_1, /*hidden argument*/Stack_1_Peek_m855668811_RuntimeMethod_var);
		Transform_set_parent_m786917804(L_0, L_2, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_3 = ___trans0;
		Vector2_t2156229523  L_4 = ((uGuiLayout_t2579011462_StaticFields*)il2cpp_codegen_static_fields_for(uGuiLayout_t2579011462_il2cpp_TypeInfo_var))->get_currentPosition_1();
		RectTransform_set_anchoredPosition_m4126691837(L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.UI.Text uGuiLayout::Label(System.String,System.String)
extern "C"  Text_t1901882714 * uGuiLayout_Label_m3379332580 (RuntimeObject * __this /* static, unused */, String_t* ___text0, String_t* ___name1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (uGuiLayout_Label_m3379332580_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	Text_t1901882714 * V_1 = NULL;
	{
		String_t* L_0 = ___name1;
		GameObject_t1113636619 * L_1 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m2093116449(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t1113636619 * L_2 = V_0;
		Text_t1901882714 * L_3 = GameObject_AddComponent_TisText_t1901882714_m942857714(L_2, /*hidden argument*/GameObject_AddComponent_TisText_t1901882714_m942857714_RuntimeMethod_var);
		V_1 = L_3;
		Text_t1901882714 * L_4 = V_1;
		String_t* L_5 = ___text0;
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, L_5);
		Text_t1901882714 * L_6 = V_1;
		RectTransform_t3704657025 * L_7 = Graphic_get_rectTransform_m1167152468(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(uGuiLayout_t2579011462_il2cpp_TypeInfo_var);
		uGuiLayout_AddChild_m2495970376(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Text_t1901882714 * L_8 = V_1;
		return L_8;
	}
}
// System.Void uGuiLayout::.cctor()
extern "C"  void uGuiLayout__cctor_m196720147 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (uGuiLayout__cctor_m196720147_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Stack_1_t253079184 * L_0 = (Stack_1_t253079184 *)il2cpp_codegen_object_new(Stack_1_t253079184_il2cpp_TypeInfo_var);
		Stack_1__ctor_m1535627942(L_0, /*hidden argument*/Stack_1__ctor_m1535627942_RuntimeMethod_var);
		((uGuiLayout_t2579011462_StaticFields*)il2cpp_codegen_static_fields_for(uGuiLayout_t2579011462_il2cpp_TypeInfo_var))->set_subControls_0(L_0);
		Vector2_t2156229523  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector2__ctor_m3970636864((&L_1), (0.0f), (0.0f), /*hidden argument*/NULL);
		((uGuiLayout_t2579011462_StaticFields*)il2cpp_codegen_static_fields_for(uGuiLayout_t2579011462_il2cpp_TypeInfo_var))->set_currentPosition_1(L_1);
		Stack_1_t2240656229 * L_2 = (Stack_1_t2240656229 *)il2cpp_codegen_object_new(Stack_1_t2240656229_il2cpp_TypeInfo_var);
		Stack_1__ctor_m20774007(L_2, /*hidden argument*/Stack_1__ctor_m20774007_RuntimeMethod_var);
		((uGuiLayout_t2579011462_StaticFields*)il2cpp_codegen_static_fields_for(uGuiLayout_t2579011462_il2cpp_TypeInfo_var))->set_positionCache_2(L_2);
		Stack_1_t4010345780 * L_3 = (Stack_1_t4010345780 *)il2cpp_codegen_object_new(Stack_1_t4010345780_il2cpp_TypeInfo_var);
		Stack_1__ctor_m2382698871(L_3, /*hidden argument*/Stack_1__ctor_m2382698871_RuntimeMethod_var);
		((uGuiLayout_t2579011462_StaticFields*)il2cpp_codegen_static_fields_for(uGuiLayout_t2579011462_il2cpp_TypeInfo_var))->set_modeCache_3(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIWindowBase::.ctor()
extern "C"  void UIWindowBase__ctor_m2586940078 (UIWindowBase_t957717849 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWindowBase::Start()
extern "C"  void UIWindowBase_Start_m860749202 (UIWindowBase_t957717849 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWindowBase_Start_m860749202_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RectTransform_t3704657025 * L_0 = Component_GetComponent_TisRectTransform_t3704657025_m3396022872(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t3704657025_m3396022872_RuntimeMethod_var);
		__this->set_m_transform_2(L_0);
		return;
	}
}
// System.Void UIWindowBase::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void UIWindowBase_OnDrag_m2792229854 (UIWindowBase_t957717849 * __this, PointerEventData_t3807901092 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIWindowBase_OnDrag_m2792229854_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_t3704657025 * L_0 = __this->get_m_transform_2();
		RectTransform_t3704657025 * L_1 = L_0;
		Vector3_t3722313464  L_2 = Transform_get_position_m36019626(L_1, /*hidden argument*/NULL);
		PointerEventData_t3807901092 * L_3 = ___eventData0;
		Vector2_t2156229523  L_4 = PointerEventData_get_delta_m1062010255(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = (&V_0)->get_x_0();
		PointerEventData_t3807901092 * L_6 = ___eventData0;
		Vector2_t2156229523  L_7 = PointerEventData_get_delta_m1062010255(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = (&V_1)->get_y_1();
		Vector3_t3722313464  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m1719387948((&L_9), L_5, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_10 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_2, L_9, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_1, L_10, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Water::.ctor()
extern "C"  void Water__ctor_m1387834787 (Water_t1083516957 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water__ctor_m1387834787_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_WaterMode_2(2);
		__this->set_m_DisablePixelLights_3((bool)1);
		__this->set_m_TextureSize_4(((int32_t)256));
		__this->set_m_ClipPlaneOffset_5((0.07f));
		LayerMask_t3493934918  L_0 = LayerMask_op_Implicit_m90232283(NULL /*static, unused*/, (-1), /*hidden argument*/NULL);
		__this->set_m_ReflectLayers_6(L_0);
		LayerMask_t3493934918  L_1 = LayerMask_op_Implicit_m90232283(NULL /*static, unused*/, (-1), /*hidden argument*/NULL);
		__this->set_m_RefractLayers_7(L_1);
		Hashtable_t1853889766 * L_2 = (Hashtable_t1853889766 *)il2cpp_codegen_object_new(Hashtable_t1853889766_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1815022027(L_2, /*hidden argument*/NULL);
		__this->set_m_ReflectionCameras_8(L_2);
		Hashtable_t1853889766 * L_3 = (Hashtable_t1853889766 *)il2cpp_codegen_object_new(Hashtable_t1853889766_il2cpp_TypeInfo_var);
		Hashtable__ctor_m1815022027(L_3, /*hidden argument*/NULL);
		__this->set_m_RefractionCameras_9(L_3);
		__this->set_m_HardwareWaterSupport_12(2);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Water::OnWillRenderObject()
extern "C"  void Water_OnWillRenderObject_m2156857888 (Water_t1083516957 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water_OnWillRenderObject_m2156857888_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Camera_t4157153871 * V_0 = NULL;
	int32_t V_1 = 0;
	Camera_t4157153871 * V_2 = NULL;
	Camera_t4157153871 * V_3 = NULL;
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	int32_t V_6 = 0;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	Vector4_t3319028937  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Matrix4x4_t1817901843  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t3722313464  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector4_t3319028937  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Matrix4x4_t1817901843  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Vector3_t3722313464  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Vector4_t3319028937  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Matrix4x4_t1817901843  V_16;
	memset(&V_16, 0, sizeof(V_16));
	{
		bool L_0 = Behaviour_get_enabled_m753527255(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0040;
		}
	}
	{
		Renderer_t2627027031 * L_1 = Component_GetComponent_TisRenderer_t2627027031_m2651633905(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m2651633905_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0040;
		}
	}
	{
		Renderer_t2627027031 * L_3 = Component_GetComponent_TisRenderer_t2627027031_m2651633905(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m2651633905_RuntimeMethod_var);
		Material_t340375123 * L_4 = Renderer_get_sharedMaterial_m1936632411(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0040;
		}
	}
	{
		Renderer_t2627027031 * L_6 = Component_GetComponent_TisRenderer_t2627027031_m2651633905(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m2651633905_RuntimeMethod_var);
		bool L_7 = Renderer_get_enabled_m3482452518(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0041;
		}
	}

IL_0040:
	{
		return;
	}

IL_0041:
	{
		Camera_t4157153871 * L_8 = Camera_get_current_m929992396(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_8;
		Camera_t4157153871 * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0053;
		}
	}
	{
		return;
	}

IL_0053:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Water_t1083516957_il2cpp_TypeInfo_var);
		bool L_11 = ((Water_t1083516957_StaticFields*)il2cpp_codegen_static_fields_for(Water_t1083516957_il2cpp_TypeInfo_var))->get_s_InsideWater_15();
		if (!L_11)
		{
			goto IL_005e;
		}
	}
	{
		return;
	}

IL_005e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Water_t1083516957_il2cpp_TypeInfo_var);
		((Water_t1083516957_StaticFields*)il2cpp_codegen_static_fields_for(Water_t1083516957_il2cpp_TypeInfo_var))->set_s_InsideWater_15((bool)1);
		int32_t L_12 = Water_FindHardwareWaterSupport_m1195528645(__this, /*hidden argument*/NULL);
		__this->set_m_HardwareWaterSupport_12(L_12);
		int32_t L_13 = Water_GetWaterMode_m3334219937(__this, /*hidden argument*/NULL);
		V_1 = L_13;
		Camera_t4157153871 * L_14 = V_0;
		Water_CreateWaterObjects_m3692736303(__this, L_14, (&V_2), (&V_3), /*hidden argument*/NULL);
		Transform_t3600365921 * L_15 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_16 = Transform_get_position_m36019626(L_15, /*hidden argument*/NULL);
		V_4 = L_16;
		Transform_t3600365921 * L_17 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_18 = Transform_get_up_m3972993886(L_17, /*hidden argument*/NULL);
		V_5 = L_18;
		int32_t L_19 = QualitySettings_get_pixelLightCount_m3013306133(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_6 = L_19;
		bool L_20 = __this->get_m_DisablePixelLights_3();
		if (!L_20)
		{
			goto IL_00b4;
		}
	}
	{
		QualitySettings_set_pixelLightCount_m3523654033(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_00b4:
	{
		Camera_t4157153871 * L_21 = V_0;
		Camera_t4157153871 * L_22 = V_2;
		Water_UpdateCameraModes_m552426476(__this, L_21, L_22, /*hidden argument*/NULL);
		Camera_t4157153871 * L_23 = V_0;
		Camera_t4157153871 * L_24 = V_3;
		Water_UpdateCameraModes_m552426476(__this, L_23, L_24, /*hidden argument*/NULL);
		int32_t L_25 = V_1;
		if ((((int32_t)L_25) < ((int32_t)1)))
		{
			goto IL_0203;
		}
	}
	{
		float L_26 = QualitySettings_get_shadowDistance_m2189244662(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_7 = L_26;
		QualitySettings_set_shadowDistance_m3878605578(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		Vector3_t3722313464  L_27 = V_5;
		Vector3_t3722313464  L_28 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_29 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_27, L_28, /*hidden argument*/NULL);
		float L_30 = __this->get_m_ClipPlaneOffset_5();
		V_8 = ((float)il2cpp_codegen_subtract((float)((-L_29)), (float)L_30));
		float L_31 = (&V_5)->get_x_1();
		float L_32 = (&V_5)->get_y_2();
		float L_33 = (&V_5)->get_z_3();
		float L_34 = V_8;
		Vector4__ctor_m2498754347((&V_9), L_31, L_32, L_33, L_34, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t1817901843_il2cpp_TypeInfo_var);
		Matrix4x4_t1817901843  L_35 = Matrix4x4_get_zero_m2898777066(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_10 = L_35;
		Vector4_t3319028937  L_36 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Water_t1083516957_il2cpp_TypeInfo_var);
		Water_CalculateReflectionMatrix_m1781634015(NULL /*static, unused*/, (&V_10), L_36, /*hidden argument*/NULL);
		Camera_t4157153871 * L_37 = V_0;
		Transform_t3600365921 * L_38 = Component_get_transform_m3162698980(L_37, /*hidden argument*/NULL);
		Vector3_t3722313464  L_39 = Transform_get_position_m36019626(L_38, /*hidden argument*/NULL);
		Vector3_t3722313464  L_40 = Matrix4x4_MultiplyPoint_m1575665487((&V_10), L_39, /*hidden argument*/NULL);
		V_11 = L_40;
		Camera_t4157153871 * L_41 = V_2;
		Camera_t4157153871 * L_42 = V_0;
		Matrix4x4_t1817901843  L_43 = Camera_get_worldToCameraMatrix_m22661425(L_42, /*hidden argument*/NULL);
		Matrix4x4_t1817901843  L_44 = V_10;
		Matrix4x4_t1817901843  L_45 = Matrix4x4_op_Multiply_m1876492807(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		Camera_set_worldToCameraMatrix_m2548466927(L_41, L_45, /*hidden argument*/NULL);
		Camera_t4157153871 * L_46 = V_2;
		Vector3_t3722313464  L_47 = V_4;
		Vector3_t3722313464  L_48 = V_5;
		Vector4_t3319028937  L_49 = Water_CameraSpacePlane_m2359059317(__this, L_46, L_47, L_48, (1.0f), /*hidden argument*/NULL);
		V_12 = L_49;
		Camera_t4157153871 * L_50 = V_0;
		Matrix4x4_t1817901843  L_51 = Camera_get_projectionMatrix_m667780853(L_50, /*hidden argument*/NULL);
		V_13 = L_51;
		Vector4_t3319028937  L_52 = V_12;
		Water_CalculateObliqueMatrix_m3061601437(NULL /*static, unused*/, (&V_13), L_52, /*hidden argument*/NULL);
		Camera_t4157153871 * L_53 = V_2;
		Matrix4x4_t1817901843  L_54 = V_13;
		Camera_set_projectionMatrix_m3293177686(L_53, L_54, /*hidden argument*/NULL);
		Camera_t4157153871 * L_55 = V_2;
		LayerMask_t3493934918 * L_56 = __this->get_address_of_m_ReflectLayers_6();
		int32_t L_57 = LayerMask_get_value_m1881709263(L_56, /*hidden argument*/NULL);
		Camera_set_cullingMask_m1402455777(L_55, ((int32_t)((int32_t)((int32_t)-17)&(int32_t)L_57)), /*hidden argument*/NULL);
		Camera_t4157153871 * L_58 = V_2;
		RenderTexture_t2108887433 * L_59 = __this->get_m_ReflectionTexture_10();
		Camera_set_targetTexture_m3148311140(L_58, L_59, /*hidden argument*/NULL);
		GL_SetRevertBackfacing_m310228429(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		Camera_t4157153871 * L_60 = V_0;
		Transform_t3600365921 * L_61 = Component_get_transform_m3162698980(L_60, /*hidden argument*/NULL);
		Vector3_t3722313464  L_62 = Transform_get_eulerAngles_m2743581774(L_61, /*hidden argument*/NULL);
		V_14 = L_62;
		Camera_t4157153871 * L_63 = V_2;
		Transform_t3600365921 * L_64 = Component_get_transform_m3162698980(L_63, /*hidden argument*/NULL);
		Vector3_t3722313464  L_65 = V_11;
		Transform_set_position_m3387557959(L_64, L_65, /*hidden argument*/NULL);
		Camera_t4157153871 * L_66 = V_2;
		Transform_t3600365921 * L_67 = Component_get_transform_m3162698980(L_66, /*hidden argument*/NULL);
		float L_68 = (&V_14)->get_x_1();
		float L_69 = (&V_14)->get_y_2();
		float L_70 = (&V_14)->get_z_3();
		Vector3_t3722313464  L_71;
		memset(&L_71, 0, sizeof(L_71));
		Vector3__ctor_m3353183577((&L_71), ((-L_68)), L_69, L_70, /*hidden argument*/NULL);
		Transform_set_eulerAngles_m135219616(L_67, L_71, /*hidden argument*/NULL);
		Camera_t4157153871 * L_72 = V_2;
		Camera_Render_m2813253190(L_72, /*hidden argument*/NULL);
		GL_SetRevertBackfacing_m310228429(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		Renderer_t2627027031 * L_73 = Component_GetComponent_TisRenderer_t2627027031_m2651633905(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m2651633905_RuntimeMethod_var);
		Material_t340375123 * L_74 = Renderer_get_sharedMaterial_m1936632411(L_73, /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_75 = __this->get_m_ReflectionTexture_10();
		Material_SetTexture_m1829349465(L_74, _stringLiteral253045501, L_75, /*hidden argument*/NULL);
		float L_76 = V_7;
		QualitySettings_set_shadowDistance_m3878605578(NULL /*static, unused*/, L_76, /*hidden argument*/NULL);
	}

IL_0203:
	{
		int32_t L_77 = V_1;
		if ((((int32_t)L_77) < ((int32_t)2)))
		{
			goto IL_02ae;
		}
	}
	{
		Camera_t4157153871 * L_78 = V_3;
		Camera_t4157153871 * L_79 = V_0;
		Matrix4x4_t1817901843  L_80 = Camera_get_worldToCameraMatrix_m22661425(L_79, /*hidden argument*/NULL);
		Camera_set_worldToCameraMatrix_m2548466927(L_78, L_80, /*hidden argument*/NULL);
		Camera_t4157153871 * L_81 = V_3;
		Vector3_t3722313464  L_82 = V_4;
		Vector3_t3722313464  L_83 = V_5;
		Vector4_t3319028937  L_84 = Water_CameraSpacePlane_m2359059317(__this, L_81, L_82, L_83, (-1.0f), /*hidden argument*/NULL);
		V_15 = L_84;
		Camera_t4157153871 * L_85 = V_0;
		Matrix4x4_t1817901843  L_86 = Camera_get_projectionMatrix_m667780853(L_85, /*hidden argument*/NULL);
		V_16 = L_86;
		Vector4_t3319028937  L_87 = V_15;
		IL2CPP_RUNTIME_CLASS_INIT(Water_t1083516957_il2cpp_TypeInfo_var);
		Water_CalculateObliqueMatrix_m3061601437(NULL /*static, unused*/, (&V_16), L_87, /*hidden argument*/NULL);
		Camera_t4157153871 * L_88 = V_3;
		Matrix4x4_t1817901843  L_89 = V_16;
		Camera_set_projectionMatrix_m3293177686(L_88, L_89, /*hidden argument*/NULL);
		Camera_t4157153871 * L_90 = V_3;
		LayerMask_t3493934918 * L_91 = __this->get_address_of_m_RefractLayers_7();
		int32_t L_92 = LayerMask_get_value_m1881709263(L_91, /*hidden argument*/NULL);
		Camera_set_cullingMask_m1402455777(L_90, ((int32_t)((int32_t)((int32_t)-17)&(int32_t)L_92)), /*hidden argument*/NULL);
		Camera_t4157153871 * L_93 = V_3;
		RenderTexture_t2108887433 * L_94 = __this->get_m_RefractionTexture_11();
		Camera_set_targetTexture_m3148311140(L_93, L_94, /*hidden argument*/NULL);
		Camera_t4157153871 * L_95 = V_3;
		Transform_t3600365921 * L_96 = Component_get_transform_m3162698980(L_95, /*hidden argument*/NULL);
		Camera_t4157153871 * L_97 = V_0;
		Transform_t3600365921 * L_98 = Component_get_transform_m3162698980(L_97, /*hidden argument*/NULL);
		Vector3_t3722313464  L_99 = Transform_get_position_m36019626(L_98, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_96, L_99, /*hidden argument*/NULL);
		Camera_t4157153871 * L_100 = V_3;
		Transform_t3600365921 * L_101 = Component_get_transform_m3162698980(L_100, /*hidden argument*/NULL);
		Camera_t4157153871 * L_102 = V_0;
		Transform_t3600365921 * L_103 = Component_get_transform_m3162698980(L_102, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_104 = Transform_get_rotation_m3502953881(L_103, /*hidden argument*/NULL);
		Transform_set_rotation_m3524318132(L_101, L_104, /*hidden argument*/NULL);
		Camera_t4157153871 * L_105 = V_3;
		Camera_Render_m2813253190(L_105, /*hidden argument*/NULL);
		Renderer_t2627027031 * L_106 = Component_GetComponent_TisRenderer_t2627027031_m2651633905(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m2651633905_RuntimeMethod_var);
		Material_t340375123 * L_107 = Renderer_get_sharedMaterial_m1936632411(L_106, /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_108 = __this->get_m_RefractionTexture_11();
		Material_SetTexture_m1829349465(L_107, _stringLiteral521248767, L_108, /*hidden argument*/NULL);
	}

IL_02ae:
	{
		bool L_109 = __this->get_m_DisablePixelLights_3();
		if (!L_109)
		{
			goto IL_02c0;
		}
	}
	{
		int32_t L_110 = V_6;
		QualitySettings_set_pixelLightCount_m3523654033(NULL /*static, unused*/, L_110, /*hidden argument*/NULL);
	}

IL_02c0:
	{
		int32_t L_111 = V_1;
		if (!L_111)
		{
			goto IL_02d9;
		}
	}
	{
		int32_t L_112 = V_1;
		if ((((int32_t)L_112) == ((int32_t)1)))
		{
			goto IL_02fc;
		}
	}
	{
		int32_t L_113 = V_1;
		if ((((int32_t)L_113) == ((int32_t)2)))
		{
			goto IL_031f;
		}
	}
	{
		goto IL_0342;
	}

IL_02d9:
	{
		Shader_EnableKeyword_m3103559844(NULL /*static, unused*/, _stringLiteral1050277699, /*hidden argument*/NULL);
		Shader_DisableKeyword_m433641454(NULL /*static, unused*/, _stringLiteral2712144963, /*hidden argument*/NULL);
		Shader_DisableKeyword_m433641454(NULL /*static, unused*/, _stringLiteral1727694679, /*hidden argument*/NULL);
		goto IL_0342;
	}

IL_02fc:
	{
		Shader_DisableKeyword_m433641454(NULL /*static, unused*/, _stringLiteral1050277699, /*hidden argument*/NULL);
		Shader_EnableKeyword_m3103559844(NULL /*static, unused*/, _stringLiteral2712144963, /*hidden argument*/NULL);
		Shader_DisableKeyword_m433641454(NULL /*static, unused*/, _stringLiteral1727694679, /*hidden argument*/NULL);
		goto IL_0342;
	}

IL_031f:
	{
		Shader_DisableKeyword_m433641454(NULL /*static, unused*/, _stringLiteral1050277699, /*hidden argument*/NULL);
		Shader_DisableKeyword_m433641454(NULL /*static, unused*/, _stringLiteral2712144963, /*hidden argument*/NULL);
		Shader_EnableKeyword_m3103559844(NULL /*static, unused*/, _stringLiteral1727694679, /*hidden argument*/NULL);
		goto IL_0342;
	}

IL_0342:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Water_t1083516957_il2cpp_TypeInfo_var);
		((Water_t1083516957_StaticFields*)il2cpp_codegen_static_fields_for(Water_t1083516957_il2cpp_TypeInfo_var))->set_s_InsideWater_15((bool)0);
		return;
	}
}
// System.Void Water::OnDisable()
extern "C"  void Water_OnDisable_m2513774421 (Water_t1083516957 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water_OnDisable_m2513774421_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DictionaryEntry_t3123975638  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RuntimeObject* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	DictionaryEntry_t3123975638  V_3;
	memset(&V_3, 0, sizeof(V_3));
	RuntimeObject* V_4 = NULL;
	RuntimeObject* V_5 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RenderTexture_t2108887433 * L_0 = __this->get_m_ReflectionTexture_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		RenderTexture_t2108887433 * L_2 = __this->get_m_ReflectionTexture_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m3193525861(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_m_ReflectionTexture_10((RenderTexture_t2108887433 *)NULL);
	}

IL_0022:
	{
		RenderTexture_t2108887433 * L_3 = __this->get_m_RefractionTexture_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0044;
		}
	}
	{
		RenderTexture_t2108887433 * L_5 = __this->get_m_RefractionTexture_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m3193525861(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->set_m_RefractionTexture_11((RenderTexture_t2108887433 *)NULL);
	}

IL_0044:
	{
		Hashtable_t1853889766 * L_6 = __this->get_m_ReflectionCameras_8();
		RuntimeObject* L_7 = VirtFuncInvoker0< RuntimeObject* >::Invoke(27 /* System.Collections.IDictionaryEnumerator System.Collections.Hashtable::GetEnumerator() */, L_6);
		V_1 = L_7;
	}

IL_0050:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0077;
		}

IL_0055:
		{
			RuntimeObject* L_8 = V_1;
			RuntimeObject * L_9 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_8);
			V_0 = ((*(DictionaryEntry_t3123975638 *)((DictionaryEntry_t3123975638 *)UnBox(L_9, DictionaryEntry_t3123975638_il2cpp_TypeInfo_var))));
			RuntimeObject * L_10 = DictionaryEntry_get_Value_m618120527((&V_0), /*hidden argument*/NULL);
			GameObject_t1113636619 * L_11 = Component_get_gameObject_m442555142(((Camera_t4157153871 *)CastclassSealed((RuntimeObject*)L_10, Camera_t4157153871_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
			Object_DestroyImmediate_m3193525861(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		}

IL_0077:
		{
			RuntimeObject* L_12 = V_1;
			bool L_13 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_12);
			if (L_13)
			{
				goto IL_0055;
			}
		}

IL_0082:
		{
			IL2CPP_LEAVE(0x9B, FINALLY_0087);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0087;
	}

FINALLY_0087:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_14 = V_1;
			RuntimeObject* L_15 = ((RuntimeObject*)IsInst((RuntimeObject*)L_14, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			V_2 = L_15;
			if (!L_15)
			{
				goto IL_009a;
			}
		}

IL_0094:
		{
			RuntimeObject* L_16 = V_2;
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_16);
		}

IL_009a:
		{
			IL2CPP_END_FINALLY(135)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(135)
	{
		IL2CPP_JUMP_TBL(0x9B, IL_009b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_009b:
	{
		Hashtable_t1853889766 * L_17 = __this->get_m_ReflectionCameras_8();
		VirtActionInvoker0::Invoke(25 /* System.Void System.Collections.Hashtable::Clear() */, L_17);
		Hashtable_t1853889766 * L_18 = __this->get_m_RefractionCameras_9();
		RuntimeObject* L_19 = VirtFuncInvoker0< RuntimeObject* >::Invoke(27 /* System.Collections.IDictionaryEnumerator System.Collections.Hashtable::GetEnumerator() */, L_18);
		V_4 = L_19;
	}

IL_00b3:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00db;
		}

IL_00b8:
		{
			RuntimeObject* L_20 = V_4;
			RuntimeObject * L_21 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_20);
			V_3 = ((*(DictionaryEntry_t3123975638 *)((DictionaryEntry_t3123975638 *)UnBox(L_21, DictionaryEntry_t3123975638_il2cpp_TypeInfo_var))));
			RuntimeObject * L_22 = DictionaryEntry_get_Value_m618120527((&V_3), /*hidden argument*/NULL);
			GameObject_t1113636619 * L_23 = Component_get_gameObject_m442555142(((Camera_t4157153871 *)CastclassSealed((RuntimeObject*)L_22, Camera_t4157153871_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
			Object_DestroyImmediate_m3193525861(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		}

IL_00db:
		{
			RuntimeObject* L_24 = V_4;
			bool L_25 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_24);
			if (L_25)
			{
				goto IL_00b8;
			}
		}

IL_00e7:
		{
			IL2CPP_LEAVE(0x103, FINALLY_00ec);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00ec;
	}

FINALLY_00ec:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_26 = V_4;
			RuntimeObject* L_27 = ((RuntimeObject*)IsInst((RuntimeObject*)L_26, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			V_5 = L_27;
			if (!L_27)
			{
				goto IL_0102;
			}
		}

IL_00fb:
		{
			RuntimeObject* L_28 = V_5;
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_28);
		}

IL_0102:
		{
			IL2CPP_END_FINALLY(236)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(236)
	{
		IL2CPP_JUMP_TBL(0x103, IL_0103)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0103:
	{
		Hashtable_t1853889766 * L_29 = __this->get_m_RefractionCameras_9();
		VirtActionInvoker0::Invoke(25 /* System.Void System.Collections.Hashtable::Clear() */, L_29);
		return;
	}
}
// System.Void Water::LateUpdate()
extern "C"  void Water_LateUpdate_m1551635237 (Water_t1083516957 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water_LateUpdate_m1551635237_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Material_t340375123 * V_0 = NULL;
	Vector4_t3319028937  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector4_t3319028937  V_3;
	memset(&V_3, 0, sizeof(V_3));
	double V_4 = 0.0;
	Vector4_t3319028937  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Transform_t3600365921 * V_6 = NULL;
	Vector3_t3722313464  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t3722313464  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t3722313464  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3_t3722313464  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t3722313464  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Bounds_t2266837910  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t3722313464  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Matrix4x4_t1817901843  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Vector3_t3722313464  V_15;
	memset(&V_15, 0, sizeof(V_15));
	Vector3_t3722313464  V_16;
	memset(&V_16, 0, sizeof(V_16));
	Vector4_t3319028937  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Transform_t3600365921 * G_B7_0 = NULL;
	{
		Renderer_t2627027031 * L_0 = Component_GetComponent_TisRenderer_t2627027031_m2651633905(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m2651633905_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Renderer_t2627027031 * L_2 = Component_GetComponent_TisRenderer_t2627027031_m2651633905(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m2651633905_RuntimeMethod_var);
		Material_t340375123 * L_3 = Renderer_get_sharedMaterial_m1936632411(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Material_t340375123 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		Material_t340375123 * L_6 = V_0;
		Vector4_t3319028937  L_7 = Material_GetVector_m806950826(L_6, _stringLiteral4103337048, /*hidden argument*/NULL);
		V_1 = L_7;
		Material_t340375123 * L_8 = V_0;
		float L_9 = Material_GetFloat_m2210875428(L_8, _stringLiteral3029421264, /*hidden argument*/NULL);
		V_2 = L_9;
		float L_10 = V_2;
		float L_11 = V_2;
		float L_12 = V_2;
		float L_13 = V_2;
		Vector4__ctor_m2498754347((&V_3), L_10, L_11, ((float)il2cpp_codegen_multiply((float)L_12, (float)(0.4f))), ((float)il2cpp_codegen_multiply((float)L_13, (float)(0.45f))), /*hidden argument*/NULL);
		float L_14 = Time_get_timeSinceLevelLoad_m2224611026(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = ((double)((double)(((double)((double)L_14)))/(double)(20.0)));
		float L_15 = (&V_1)->get_x_1();
		float L_16 = (&V_3)->get_x_1();
		double L_17 = V_4;
		double L_18 = Math_IEEERemainder_m1747102664(NULL /*static, unused*/, ((double)il2cpp_codegen_multiply((double)(((double)((double)((float)il2cpp_codegen_multiply((float)L_15, (float)L_16))))), (double)L_17)), (1.0), /*hidden argument*/NULL);
		float L_19 = (&V_1)->get_y_2();
		float L_20 = (&V_3)->get_y_2();
		double L_21 = V_4;
		double L_22 = Math_IEEERemainder_m1747102664(NULL /*static, unused*/, ((double)il2cpp_codegen_multiply((double)(((double)((double)((float)il2cpp_codegen_multiply((float)L_19, (float)L_20))))), (double)L_21)), (1.0), /*hidden argument*/NULL);
		float L_23 = (&V_1)->get_z_3();
		float L_24 = (&V_3)->get_z_3();
		double L_25 = V_4;
		double L_26 = Math_IEEERemainder_m1747102664(NULL /*static, unused*/, ((double)il2cpp_codegen_multiply((double)(((double)((double)((float)il2cpp_codegen_multiply((float)L_23, (float)L_24))))), (double)L_25)), (1.0), /*hidden argument*/NULL);
		float L_27 = (&V_1)->get_w_4();
		float L_28 = (&V_3)->get_w_4();
		double L_29 = V_4;
		double L_30 = Math_IEEERemainder_m1747102664(NULL /*static, unused*/, ((double)il2cpp_codegen_multiply((double)(((double)((double)((float)il2cpp_codegen_multiply((float)L_27, (float)L_28))))), (double)L_29)), (1.0), /*hidden argument*/NULL);
		Vector4__ctor_m2498754347((&V_5), (((float)((float)L_18))), (((float)((float)L_22))), (((float)((float)L_26))), (((float)((float)L_30))), /*hidden argument*/NULL);
		Camera_t4157153871 * L_31 = Camera_get_main_m3643453163(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_32 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_31, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0118;
		}
	}
	{
		Camera_t4157153871 * L_33 = Camera_get_main_m3643453163(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3600365921 * L_34 = Component_get_transform_m3162698980(L_33, /*hidden argument*/NULL);
		G_B7_0 = L_34;
		goto IL_011e;
	}

IL_0118:
	{
		Transform_t3600365921 * L_35 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		G_B7_0 = L_35;
	}

IL_011e:
	{
		V_6 = G_B7_0;
		Vector4_t3319028937 * L_36 = (&V_5);
		float L_37 = L_36->get_x_1();
		Transform_t3600365921 * L_38 = V_6;
		Vector3_t3722313464  L_39 = Transform_get_position_m36019626(L_38, /*hidden argument*/NULL);
		V_7 = L_39;
		float L_40 = (&V_7)->get_x_1();
		float L_41 = (&V_3)->get_x_1();
		L_36->set_x_1(((float)il2cpp_codegen_add((float)L_37, (float)((float)il2cpp_codegen_multiply((float)L_40, (float)L_41)))));
		Vector4_t3319028937 * L_42 = (&V_5);
		float L_43 = L_42->get_y_2();
		Transform_t3600365921 * L_44 = V_6;
		Vector3_t3722313464  L_45 = Transform_get_position_m36019626(L_44, /*hidden argument*/NULL);
		V_8 = L_45;
		float L_46 = (&V_8)->get_z_3();
		float L_47 = (&V_3)->get_y_2();
		L_42->set_y_2(((float)il2cpp_codegen_add((float)L_43, (float)((float)il2cpp_codegen_multiply((float)L_46, (float)L_47)))));
		Vector4_t3319028937 * L_48 = (&V_5);
		float L_49 = L_48->get_z_3();
		Transform_t3600365921 * L_50 = V_6;
		Vector3_t3722313464  L_51 = Transform_get_position_m36019626(L_50, /*hidden argument*/NULL);
		V_9 = L_51;
		float L_52 = (&V_9)->get_x_1();
		float L_53 = (&V_3)->get_z_3();
		L_48->set_z_3(((float)il2cpp_codegen_add((float)L_49, (float)((float)il2cpp_codegen_multiply((float)L_52, (float)L_53)))));
		Vector4_t3319028937 * L_54 = (&V_5);
		float L_55 = L_54->get_w_4();
		Transform_t3600365921 * L_56 = V_6;
		Vector3_t3722313464  L_57 = Transform_get_position_m36019626(L_56, /*hidden argument*/NULL);
		V_10 = L_57;
		float L_58 = (&V_10)->get_z_3();
		float L_59 = (&V_3)->get_w_4();
		L_54->set_w_4(((float)il2cpp_codegen_add((float)L_55, (float)((float)il2cpp_codegen_multiply((float)L_58, (float)L_59)))));
		Material_t340375123 * L_60 = V_0;
		Vector4_t3319028937  L_61 = V_5;
		Material_SetVector_m4214217286(L_60, _stringLiteral2268830102, L_61, /*hidden argument*/NULL);
		Material_t340375123 * L_62 = V_0;
		Vector4_t3319028937  L_63 = V_3;
		Material_SetVector_m4214217286(L_62, _stringLiteral2617886994, L_63, /*hidden argument*/NULL);
		Renderer_t2627027031 * L_64 = Component_GetComponent_TisRenderer_t2627027031_m2651633905(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m2651633905_RuntimeMethod_var);
		Bounds_t2266837910  L_65 = Renderer_get_bounds_m1803204000(L_64, /*hidden argument*/NULL);
		V_12 = L_65;
		Vector3_t3722313464  L_66 = Bounds_get_size_m1178783246((&V_12), /*hidden argument*/NULL);
		V_11 = L_66;
		float L_67 = (&V_11)->get_x_1();
		float L_68 = (&V_3)->get_x_1();
		float L_69 = (&V_11)->get_z_3();
		float L_70 = (&V_3)->get_y_2();
		Vector3__ctor_m3353183577((&V_13), ((float)il2cpp_codegen_multiply((float)L_67, (float)L_68)), ((float)il2cpp_codegen_multiply((float)L_69, (float)L_70)), (1.0f), /*hidden argument*/NULL);
		float L_71 = (&V_5)->get_x_1();
		float L_72 = (&V_5)->get_y_2();
		Vector3_t3722313464  L_73;
		memset(&L_73, 0, sizeof(L_73));
		Vector3__ctor_m3353183577((&L_73), L_71, L_72, (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_74 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_75 = V_13;
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t1817901843_il2cpp_TypeInfo_var);
		Matrix4x4_t1817901843  L_76 = Matrix4x4_TRS_m3801934620(NULL /*static, unused*/, L_73, L_74, L_75, /*hidden argument*/NULL);
		V_14 = L_76;
		Material_t340375123 * L_77 = V_0;
		Matrix4x4_t1817901843  L_78 = V_14;
		Material_SetMatrix_m4094650785(L_77, _stringLiteral662295082, L_78, /*hidden argument*/NULL);
		float L_79 = (&V_11)->get_x_1();
		float L_80 = (&V_3)->get_z_3();
		float L_81 = (&V_11)->get_z_3();
		float L_82 = (&V_3)->get_w_4();
		Vector3__ctor_m3353183577((&V_13), ((float)il2cpp_codegen_multiply((float)L_79, (float)L_80)), ((float)il2cpp_codegen_multiply((float)L_81, (float)L_82)), (1.0f), /*hidden argument*/NULL);
		float L_83 = (&V_5)->get_z_3();
		float L_84 = (&V_5)->get_w_4();
		Vector3_t3722313464  L_85;
		memset(&L_85, 0, sizeof(L_85));
		Vector3__ctor_m3353183577((&L_85), L_83, L_84, (0.0f), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_86 = Quaternion_get_identity_m3722672781(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_87 = V_13;
		Matrix4x4_t1817901843  L_88 = Matrix4x4_TRS_m3801934620(NULL /*static, unused*/, L_85, L_86, L_87, /*hidden argument*/NULL);
		V_14 = L_88;
		Material_t340375123 * L_89 = V_0;
		Matrix4x4_t1817901843  L_90 = V_14;
		Material_SetMatrix_m4094650785(L_89, _stringLiteral866112042, L_90, /*hidden argument*/NULL);
		Transform_t3600365921 * L_91 = V_6;
		Vector3_t3722313464  L_92 = Transform_get_position_m36019626(L_91, /*hidden argument*/NULL);
		Terrain_t3055443660 * L_93 = Terrain_get_activeTerrain_m2378246603(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3600365921 * L_94 = Component_get_transform_m3162698980(L_93, /*hidden argument*/NULL);
		Vector3_t3722313464  L_95 = Transform_get_position_m36019626(L_94, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_96 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_92, L_95, /*hidden argument*/NULL);
		V_15 = L_96;
		Terrain_t3055443660 * L_97 = Terrain_get_activeTerrain_m2378246603(NULL /*static, unused*/, /*hidden argument*/NULL);
		TerrainData_t657004131 * L_98 = Terrain_get_terrainData_m2711583617(L_97, /*hidden argument*/NULL);
		Vector3_t3722313464  L_99 = TerrainData_get_size_m1871576403(L_98, /*hidden argument*/NULL);
		V_16 = L_99;
		float L_100 = (&V_15)->get_x_1();
		float L_101 = (&V_15)->get_z_3();
		float L_102 = (&V_16)->get_x_1();
		float L_103 = (&V_16)->get_z_3();
		Vector4__ctor_m2498754347((&V_17), L_100, L_101, ((float)((float)(1.0f)/(float)L_102)), ((float)((float)(1.0f)/(float)L_103)), /*hidden argument*/NULL);
		Material_t340375123 * L_104 = V_0;
		Vector4_t3319028937  L_105 = V_17;
		Material_SetVector_m4214217286(L_104, _stringLiteral316339988, L_105, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Water::UpdateCameraModes(UnityEngine.Camera,UnityEngine.Camera)
extern "C"  void Water_UpdateCameraModes_m552426476 (Water_t1083516957 * __this, Camera_t4157153871 * ___src0, Camera_t4157153871 * ___dest1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water_UpdateCameraModes_m552426476_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Skybox_t2662837510 * V_0 = NULL;
	Skybox_t2662837510 * V_1 = NULL;
	{
		Camera_t4157153871 * L_0 = ___dest1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		Camera_t4157153871 * L_2 = ___dest1;
		Camera_t4157153871 * L_3 = ___src0;
		int32_t L_4 = Camera_get_clearFlags_m992534691(L_3, /*hidden argument*/NULL);
		Camera_set_clearFlags_m2207032996(L_2, L_4, /*hidden argument*/NULL);
		Camera_t4157153871 * L_5 = ___dest1;
		Camera_t4157153871 * L_6 = ___src0;
		Color_t2555686324  L_7 = Camera_get_backgroundColor_m3310993309(L_6, /*hidden argument*/NULL);
		Camera_set_backgroundColor_m1332346802(L_5, L_7, /*hidden argument*/NULL);
		Camera_t4157153871 * L_8 = ___src0;
		int32_t L_9 = Camera_get_clearFlags_m992534691(L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)1))))
		{
			goto IL_0097;
		}
	}
	{
		Camera_t4157153871 * L_10 = ___src0;
		RuntimeTypeHandle_t3027515415  L_11 = { reinterpret_cast<intptr_t> (Skybox_t2662837510_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		Component_t1923634451 * L_13 = Component_GetComponent_m886226392(L_10, L_12, /*hidden argument*/NULL);
		V_0 = ((Skybox_t2662837510 *)IsInstSealed((RuntimeObject*)L_13, Skybox_t2662837510_il2cpp_TypeInfo_var));
		Camera_t4157153871 * L_14 = ___dest1;
		RuntimeTypeHandle_t3027515415  L_15 = { reinterpret_cast<intptr_t> (Skybox_t2662837510_0_0_0_var) };
		Type_t * L_16 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		Component_t1923634451 * L_17 = Component_GetComponent_m886226392(L_14, L_16, /*hidden argument*/NULL);
		V_1 = ((Skybox_t2662837510 *)IsInstSealed((RuntimeObject*)L_17, Skybox_t2662837510_il2cpp_TypeInfo_var));
		Skybox_t2662837510 * L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0078;
		}
	}
	{
		Skybox_t2662837510 * L_20 = V_0;
		Material_t340375123 * L_21 = Skybox_get_material_m3789022787(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		if (L_22)
		{
			goto IL_0084;
		}
	}

IL_0078:
	{
		Skybox_t2662837510 * L_23 = V_1;
		Behaviour_set_enabled_m20417929(L_23, (bool)0, /*hidden argument*/NULL);
		goto IL_0097;
	}

IL_0084:
	{
		Skybox_t2662837510 * L_24 = V_1;
		Behaviour_set_enabled_m20417929(L_24, (bool)1, /*hidden argument*/NULL);
		Skybox_t2662837510 * L_25 = V_1;
		Skybox_t2662837510 * L_26 = V_0;
		Material_t340375123 * L_27 = Skybox_get_material_m3789022787(L_26, /*hidden argument*/NULL);
		Skybox_set_material_m3176166872(L_25, L_27, /*hidden argument*/NULL);
	}

IL_0097:
	{
		Camera_t4157153871 * L_28 = ___dest1;
		Camera_t4157153871 * L_29 = ___src0;
		float L_30 = Camera_get_farClipPlane_m538536689(L_29, /*hidden argument*/NULL);
		Camera_set_farClipPlane_m3828313665(L_28, L_30, /*hidden argument*/NULL);
		Camera_t4157153871 * L_31 = ___dest1;
		Camera_t4157153871 * L_32 = ___src0;
		float L_33 = Camera_get_nearClipPlane_m837839537(L_32, /*hidden argument*/NULL);
		Camera_set_nearClipPlane_m3667419702(L_31, L_33, /*hidden argument*/NULL);
		Camera_t4157153871 * L_34 = ___dest1;
		Camera_t4157153871 * L_35 = ___src0;
		bool L_36 = Camera_get_orthographic_m2831464531(L_35, /*hidden argument*/NULL);
		Camera_set_orthographic_m2855749523(L_34, L_36, /*hidden argument*/NULL);
		Camera_t4157153871 * L_37 = ___dest1;
		Camera_t4157153871 * L_38 = ___src0;
		float L_39 = Camera_get_fieldOfView_m1018585504(L_38, /*hidden argument*/NULL);
		Camera_set_fieldOfView_m1438246590(L_37, L_39, /*hidden argument*/NULL);
		Camera_t4157153871 * L_40 = ___dest1;
		Camera_t4157153871 * L_41 = ___src0;
		float L_42 = Camera_get_aspect_m862507514(L_41, /*hidden argument*/NULL);
		Camera_set_aspect_m2625464181(L_40, L_42, /*hidden argument*/NULL);
		Camera_t4157153871 * L_43 = ___dest1;
		Camera_t4157153871 * L_44 = ___src0;
		float L_45 = Camera_get_orthographicSize_m3903216845(L_44, /*hidden argument*/NULL);
		Camera_set_orthographicSize_m76971700(L_43, L_45, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Water::CreateWaterObjects(UnityEngine.Camera,UnityEngine.Camera&,UnityEngine.Camera&)
extern "C"  void Water_CreateWaterObjects_m3692736303 (Water_t1083516957 * __this, Camera_t4157153871 * ___currentCamera0, Camera_t4157153871 ** ___reflectionCamera1, Camera_t4157153871 ** ___refractionCamera2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water_CreateWaterObjects_m3692736303_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	GameObject_t1113636619 * V_1 = NULL;
	GameObject_t1113636619 * V_2 = NULL;
	{
		int32_t L_0 = Water_GetWaterMode_m3334219937(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Camera_t4157153871 ** L_1 = ___reflectionCamera1;
		*((RuntimeObject **)(L_1)) = (RuntimeObject *)NULL;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_1), (RuntimeObject *)NULL);
		Camera_t4157153871 ** L_2 = ___refractionCamera2;
		*((RuntimeObject **)(L_2)) = (RuntimeObject *)NULL;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_2), (RuntimeObject *)NULL);
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) < ((int32_t)1)))
		{
			goto IL_018b;
		}
	}
	{
		RenderTexture_t2108887433 * L_4 = __this->get_m_ReflectionTexture_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_6 = __this->get_m_OldReflectionTextureSize_13();
		int32_t L_7 = __this->get_m_TextureSize_4();
		if ((((int32_t)L_6) == ((int32_t)L_7)))
		{
			goto IL_00ae;
		}
	}

IL_0035:
	{
		RenderTexture_t2108887433 * L_8 = __this->get_m_ReflectionTexture_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0050;
		}
	}
	{
		RenderTexture_t2108887433 * L_10 = __this->get_m_ReflectionTexture_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m3193525861(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
	}

IL_0050:
	{
		int32_t L_11 = __this->get_m_TextureSize_4();
		int32_t L_12 = __this->get_m_TextureSize_4();
		RenderTexture_t2108887433 * L_13 = (RenderTexture_t2108887433 *)il2cpp_codegen_object_new(RenderTexture_t2108887433_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m769234016(L_13, L_11, L_12, ((int32_t)16), /*hidden argument*/NULL);
		__this->set_m_ReflectionTexture_10(L_13);
		RenderTexture_t2108887433 * L_14 = __this->get_m_ReflectionTexture_10();
		int32_t L_15 = Object_GetInstanceID_m1255174761(__this, /*hidden argument*/NULL);
		int32_t L_16 = L_15;
		RuntimeObject * L_17 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_16);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral3277177482, L_17, /*hidden argument*/NULL);
		Object_set_name_m291480324(L_14, L_18, /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_19 = __this->get_m_ReflectionTexture_10();
		RenderTexture_set_isPowerOfTwo_m3873419893(L_19, (bool)1, /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_20 = __this->get_m_ReflectionTexture_10();
		Object_set_hideFlags_m1648752846(L_20, ((int32_t)52), /*hidden argument*/NULL);
		int32_t L_21 = __this->get_m_TextureSize_4();
		__this->set_m_OldReflectionTextureSize_13(L_21);
	}

IL_00ae:
	{
		Camera_t4157153871 ** L_22 = ___reflectionCamera1;
		Hashtable_t1853889766 * L_23 = __this->get_m_ReflectionCameras_8();
		Camera_t4157153871 * L_24 = ___currentCamera0;
		RuntimeObject * L_25 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(21 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_23, L_24);
		*((RuntimeObject **)(L_22)) = (RuntimeObject *)((Camera_t4157153871 *)IsInstSealed((RuntimeObject*)L_25, Camera_t4157153871_il2cpp_TypeInfo_var));
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_22), (RuntimeObject *)((Camera_t4157153871 *)IsInstSealed((RuntimeObject*)L_25, Camera_t4157153871_il2cpp_TypeInfo_var)));
		Camera_t4157153871 ** L_26 = ___reflectionCamera1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_27 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, (*((Camera_t4157153871 **)L_26)), /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_018b;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_28 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4));
		ArrayElementTypeCheck (L_28, _stringLiteral1537969950);
		(L_28)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral1537969950);
		ObjectU5BU5D_t2843939325* L_29 = L_28;
		int32_t L_30 = Object_GetInstanceID_m1255174761(__this, /*hidden argument*/NULL);
		int32_t L_31 = L_30;
		RuntimeObject * L_32 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_31);
		ArrayElementTypeCheck (L_29, L_32);
		(L_29)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_32);
		ObjectU5BU5D_t2843939325* L_33 = L_29;
		ArrayElementTypeCheck (L_33, _stringLiteral1505539685);
		(L_33)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral1505539685);
		ObjectU5BU5D_t2843939325* L_34 = L_33;
		Camera_t4157153871 * L_35 = ___currentCamera0;
		int32_t L_36 = Object_GetInstanceID_m1255174761(L_35, /*hidden argument*/NULL);
		int32_t L_37 = L_36;
		RuntimeObject * L_38 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_37);
		ArrayElementTypeCheck (L_34, L_38);
		(L_34)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_38);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Concat_m2971454694(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		TypeU5BU5D_t3940880105* L_40 = ((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)2));
		RuntimeTypeHandle_t3027515415  L_41 = { reinterpret_cast<intptr_t> (Camera_t4157153871_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_42 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_40, L_42);
		(L_40)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_42);
		TypeU5BU5D_t3940880105* L_43 = L_40;
		RuntimeTypeHandle_t3027515415  L_44 = { reinterpret_cast<intptr_t> (Skybox_t2662837510_0_0_0_var) };
		Type_t * L_45 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_43, L_45);
		(L_43)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_45);
		GameObject_t1113636619 * L_46 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m1350607670(L_46, L_39, L_43, /*hidden argument*/NULL);
		V_1 = L_46;
		Camera_t4157153871 ** L_47 = ___reflectionCamera1;
		GameObject_t1113636619 * L_48 = V_1;
		Camera_t4157153871 * L_49 = GameObject_GetComponent_TisCamera_t4157153871_m3956151066(L_48, /*hidden argument*/GameObject_GetComponent_TisCamera_t4157153871_m3956151066_RuntimeMethod_var);
		*((RuntimeObject **)(L_47)) = (RuntimeObject *)L_49;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_47), (RuntimeObject *)L_49);
		Camera_t4157153871 ** L_50 = ___reflectionCamera1;
		Behaviour_set_enabled_m20417929((*((Camera_t4157153871 **)L_50)), (bool)0, /*hidden argument*/NULL);
		Camera_t4157153871 ** L_51 = ___reflectionCamera1;
		Transform_t3600365921 * L_52 = Component_get_transform_m3162698980((*((Camera_t4157153871 **)L_51)), /*hidden argument*/NULL);
		Transform_t3600365921 * L_53 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_54 = Transform_get_position_m36019626(L_53, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_52, L_54, /*hidden argument*/NULL);
		Camera_t4157153871 ** L_55 = ___reflectionCamera1;
		Transform_t3600365921 * L_56 = Component_get_transform_m3162698980((*((Camera_t4157153871 **)L_55)), /*hidden argument*/NULL);
		Transform_t3600365921 * L_57 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_58 = Transform_get_rotation_m3502953881(L_57, /*hidden argument*/NULL);
		Transform_set_rotation_m3524318132(L_56, L_58, /*hidden argument*/NULL);
		Camera_t4157153871 ** L_59 = ___reflectionCamera1;
		GameObject_t1113636619 * L_60 = Component_get_gameObject_m442555142((*((Camera_t4157153871 **)L_59)), /*hidden argument*/NULL);
		GameObject_AddComponent_TisFlareLayer_t1739223323_m1608860411(L_60, /*hidden argument*/GameObject_AddComponent_TisFlareLayer_t1739223323_m1608860411_RuntimeMethod_var);
		GameObject_t1113636619 * L_61 = V_1;
		Object_set_hideFlags_m1648752846(L_61, ((int32_t)61), /*hidden argument*/NULL);
		Hashtable_t1853889766 * L_62 = __this->get_m_ReflectionCameras_8();
		Camera_t4157153871 * L_63 = ___currentCamera0;
		Camera_t4157153871 ** L_64 = ___reflectionCamera1;
		VirtActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(22 /* System.Void System.Collections.Hashtable::set_Item(System.Object,System.Object) */, L_62, L_63, (*((Camera_t4157153871 **)L_64)));
	}

IL_018b:
	{
		int32_t L_65 = V_0;
		if ((((int32_t)L_65) < ((int32_t)2)))
		{
			goto IL_0309;
		}
	}
	{
		RenderTexture_t2108887433 * L_66 = __this->get_m_RefractionTexture_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_67 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_66, /*hidden argument*/NULL);
		if (!L_67)
		{
			goto IL_01b3;
		}
	}
	{
		int32_t L_68 = __this->get_m_OldRefractionTextureSize_14();
		int32_t L_69 = __this->get_m_TextureSize_4();
		if ((((int32_t)L_68) == ((int32_t)L_69)))
		{
			goto IL_022c;
		}
	}

IL_01b3:
	{
		RenderTexture_t2108887433 * L_70 = __this->get_m_RefractionTexture_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_71 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_70, /*hidden argument*/NULL);
		if (!L_71)
		{
			goto IL_01ce;
		}
	}
	{
		RenderTexture_t2108887433 * L_72 = __this->get_m_RefractionTexture_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m3193525861(NULL /*static, unused*/, L_72, /*hidden argument*/NULL);
	}

IL_01ce:
	{
		int32_t L_73 = __this->get_m_TextureSize_4();
		int32_t L_74 = __this->get_m_TextureSize_4();
		RenderTexture_t2108887433 * L_75 = (RenderTexture_t2108887433 *)il2cpp_codegen_object_new(RenderTexture_t2108887433_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m769234016(L_75, L_73, L_74, ((int32_t)16), /*hidden argument*/NULL);
		__this->set_m_RefractionTexture_11(L_75);
		RenderTexture_t2108887433 * L_76 = __this->get_m_RefractionTexture_11();
		int32_t L_77 = Object_GetInstanceID_m1255174761(__this, /*hidden argument*/NULL);
		int32_t L_78 = L_77;
		RuntimeObject * L_79 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_78);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_80 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral1366986456, L_79, /*hidden argument*/NULL);
		Object_set_name_m291480324(L_76, L_80, /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_81 = __this->get_m_RefractionTexture_11();
		RenderTexture_set_isPowerOfTwo_m3873419893(L_81, (bool)1, /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_82 = __this->get_m_RefractionTexture_11();
		Object_set_hideFlags_m1648752846(L_82, ((int32_t)52), /*hidden argument*/NULL);
		int32_t L_83 = __this->get_m_TextureSize_4();
		__this->set_m_OldRefractionTextureSize_14(L_83);
	}

IL_022c:
	{
		Camera_t4157153871 ** L_84 = ___refractionCamera2;
		Hashtable_t1853889766 * L_85 = __this->get_m_RefractionCameras_9();
		Camera_t4157153871 * L_86 = ___currentCamera0;
		RuntimeObject * L_87 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(21 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, L_85, L_86);
		*((RuntimeObject **)(L_84)) = (RuntimeObject *)((Camera_t4157153871 *)IsInstSealed((RuntimeObject*)L_87, Camera_t4157153871_il2cpp_TypeInfo_var));
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_84), (RuntimeObject *)((Camera_t4157153871 *)IsInstSealed((RuntimeObject*)L_87, Camera_t4157153871_il2cpp_TypeInfo_var)));
		Camera_t4157153871 ** L_88 = ___refractionCamera2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_89 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, (*((Camera_t4157153871 **)L_88)), /*hidden argument*/NULL);
		if (L_89)
		{
			goto IL_0309;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_90 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4));
		ArrayElementTypeCheck (L_90, _stringLiteral2541588253);
		(L_90)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral2541588253);
		ObjectU5BU5D_t2843939325* L_91 = L_90;
		int32_t L_92 = Object_GetInstanceID_m1255174761(__this, /*hidden argument*/NULL);
		int32_t L_93 = L_92;
		RuntimeObject * L_94 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_93);
		ArrayElementTypeCheck (L_91, L_94);
		(L_91)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_94);
		ObjectU5BU5D_t2843939325* L_95 = L_91;
		ArrayElementTypeCheck (L_95, _stringLiteral1505539685);
		(L_95)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral1505539685);
		ObjectU5BU5D_t2843939325* L_96 = L_95;
		Camera_t4157153871 * L_97 = ___currentCamera0;
		int32_t L_98 = Object_GetInstanceID_m1255174761(L_97, /*hidden argument*/NULL);
		int32_t L_99 = L_98;
		RuntimeObject * L_100 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_99);
		ArrayElementTypeCheck (L_96, L_100);
		(L_96)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_100);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_101 = String_Concat_m2971454694(NULL /*static, unused*/, L_96, /*hidden argument*/NULL);
		TypeU5BU5D_t3940880105* L_102 = ((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)2));
		RuntimeTypeHandle_t3027515415  L_103 = { reinterpret_cast<intptr_t> (Camera_t4157153871_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_104 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_103, /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_102, L_104);
		(L_102)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_104);
		TypeU5BU5D_t3940880105* L_105 = L_102;
		RuntimeTypeHandle_t3027515415  L_106 = { reinterpret_cast<intptr_t> (Skybox_t2662837510_0_0_0_var) };
		Type_t * L_107 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_106, /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_105, L_107);
		(L_105)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_107);
		GameObject_t1113636619 * L_108 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m1350607670(L_108, L_101, L_105, /*hidden argument*/NULL);
		V_2 = L_108;
		Camera_t4157153871 ** L_109 = ___refractionCamera2;
		GameObject_t1113636619 * L_110 = V_2;
		Camera_t4157153871 * L_111 = GameObject_GetComponent_TisCamera_t4157153871_m3956151066(L_110, /*hidden argument*/GameObject_GetComponent_TisCamera_t4157153871_m3956151066_RuntimeMethod_var);
		*((RuntimeObject **)(L_109)) = (RuntimeObject *)L_111;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_109), (RuntimeObject *)L_111);
		Camera_t4157153871 ** L_112 = ___refractionCamera2;
		Behaviour_set_enabled_m20417929((*((Camera_t4157153871 **)L_112)), (bool)0, /*hidden argument*/NULL);
		Camera_t4157153871 ** L_113 = ___refractionCamera2;
		Transform_t3600365921 * L_114 = Component_get_transform_m3162698980((*((Camera_t4157153871 **)L_113)), /*hidden argument*/NULL);
		Transform_t3600365921 * L_115 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_116 = Transform_get_position_m36019626(L_115, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_114, L_116, /*hidden argument*/NULL);
		Camera_t4157153871 ** L_117 = ___refractionCamera2;
		Transform_t3600365921 * L_118 = Component_get_transform_m3162698980((*((Camera_t4157153871 **)L_117)), /*hidden argument*/NULL);
		Transform_t3600365921 * L_119 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_120 = Transform_get_rotation_m3502953881(L_119, /*hidden argument*/NULL);
		Transform_set_rotation_m3524318132(L_118, L_120, /*hidden argument*/NULL);
		Camera_t4157153871 ** L_121 = ___refractionCamera2;
		GameObject_t1113636619 * L_122 = Component_get_gameObject_m442555142((*((Camera_t4157153871 **)L_121)), /*hidden argument*/NULL);
		GameObject_AddComponent_TisFlareLayer_t1739223323_m1608860411(L_122, /*hidden argument*/GameObject_AddComponent_TisFlareLayer_t1739223323_m1608860411_RuntimeMethod_var);
		GameObject_t1113636619 * L_123 = V_2;
		Object_set_hideFlags_m1648752846(L_123, ((int32_t)61), /*hidden argument*/NULL);
		Hashtable_t1853889766 * L_124 = __this->get_m_RefractionCameras_9();
		Camera_t4157153871 * L_125 = ___currentCamera0;
		Camera_t4157153871 ** L_126 = ___refractionCamera2;
		VirtActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(22 /* System.Void System.Collections.Hashtable::set_Item(System.Object,System.Object) */, L_124, L_125, (*((Camera_t4157153871 **)L_126)));
	}

IL_0309:
	{
		return;
	}
}
// Water/WaterMode Water::GetWaterMode()
extern "C"  int32_t Water_GetWaterMode_m3334219937 (Water_t1083516957 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_HardwareWaterSupport_12();
		int32_t L_1 = __this->get_m_WaterMode_2();
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_2 = __this->get_m_HardwareWaterSupport_12();
		return L_2;
	}

IL_0018:
	{
		int32_t L_3 = __this->get_m_WaterMode_2();
		return L_3;
	}
}
// Water/WaterMode Water::FindHardwareWaterSupport()
extern "C"  int32_t Water_FindHardwareWaterSupport_m1195528645 (Water_t1083516957 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water_FindHardwareWaterSupport_m1195528645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Material_t340375123 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		bool L_0 = SystemInfo_get_supportsRenderTextures_m466309287(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		Renderer_t2627027031 * L_1 = Component_GetComponent_TisRenderer_t2627027031_m2651633905(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m2651633905_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001c;
		}
	}

IL_001a:
	{
		return (int32_t)(0);
	}

IL_001c:
	{
		Renderer_t2627027031 * L_3 = Component_GetComponent_TisRenderer_t2627027031_m2651633905(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m2651633905_RuntimeMethod_var);
		Material_t340375123 * L_4 = Renderer_get_sharedMaterial_m1936632411(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Material_t340375123 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0035;
		}
	}
	{
		return (int32_t)(0);
	}

IL_0035:
	{
		Material_t340375123 * L_7 = V_0;
		String_t* L_8 = Material_GetTag_m2091721776(L_7, _stringLiteral713880165, (bool)0, /*hidden argument*/NULL);
		V_1 = L_8;
		String_t* L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m920492651(NULL /*static, unused*/, L_9, _stringLiteral1103090832, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0054;
		}
	}
	{
		return (int32_t)(2);
	}

IL_0054:
	{
		String_t* L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m920492651(NULL /*static, unused*/, L_11, _stringLiteral2505954588, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0066;
		}
	}
	{
		return (int32_t)(1);
	}

IL_0066:
	{
		return (int32_t)(0);
	}
}
// System.Single Water::sgn(System.Single)
extern "C"  float Water_sgn_m123353488 (RuntimeObject * __this /* static, unused */, float ___a0, const RuntimeMethod* method)
{
	{
		float L_0 = ___a0;
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_0011;
		}
	}
	{
		return (1.0f);
	}

IL_0011:
	{
		float L_1 = ___a0;
		if ((!(((float)L_1) < ((float)(0.0f)))))
		{
			goto IL_0022;
		}
	}
	{
		return (-1.0f);
	}

IL_0022:
	{
		return (0.0f);
	}
}
// UnityEngine.Vector4 Water::CameraSpacePlane(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector4_t3319028937  Water_CameraSpacePlane_m2359059317 (Water_t1083516957 * __this, Camera_t4157153871 * ___cam0, Vector3_t3722313464  ___pos1, Vector3_t3722313464  ___normal2, float ___sideSign3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water_CameraSpacePlane_m2359059317_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t1817901843  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Vector3_t3722313464  L_0 = ___pos1;
		Vector3_t3722313464  L_1 = ___normal2;
		float L_2 = __this->get_m_ClipPlaneOffset_5();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_3 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t3722313464  L_4 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Camera_t4157153871 * L_5 = ___cam0;
		Matrix4x4_t1817901843  L_6 = Camera_get_worldToCameraMatrix_m22661425(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		Vector3_t3722313464  L_7 = V_0;
		Vector3_t3722313464  L_8 = Matrix4x4_MultiplyPoint_m1575665487((&V_1), L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		Vector3_t3722313464  L_9 = ___normal2;
		Vector3_t3722313464  L_10 = Matrix4x4_MultiplyVector_m3808798942((&V_1), L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		Vector3_t3722313464  L_11 = Vector3_get_normalized_m2454957984((&V_4), /*hidden argument*/NULL);
		float L_12 = ___sideSign3;
		Vector3_t3722313464  L_13 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		float L_14 = (&V_3)->get_x_1();
		float L_15 = (&V_3)->get_y_2();
		float L_16 = (&V_3)->get_z_3();
		Vector3_t3722313464  L_17 = V_2;
		Vector3_t3722313464  L_18 = V_3;
		float L_19 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		Vector4_t3319028937  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector4__ctor_m2498754347((&L_20), L_14, L_15, L_16, ((-L_19)), /*hidden argument*/NULL);
		return L_20;
	}
}
// System.Void Water::CalculateObliqueMatrix(UnityEngine.Matrix4x4&,UnityEngine.Vector4)
extern "C"  void Water_CalculateObliqueMatrix_m3061601437 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843 * ___projection0, Vector4_t3319028937  ___clipPlane1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water_CalculateObliqueMatrix_m3061601437_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector4_t3319028937  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t3319028937  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Matrix4x4_t1817901843 * L_0 = ___projection0;
		Matrix4x4_t1817901843  L_1 = Matrix4x4_get_inverse_m1870592360(L_0, /*hidden argument*/NULL);
		float L_2 = (&___clipPlane1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Water_t1083516957_il2cpp_TypeInfo_var);
		float L_3 = Water_sgn_m123353488(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		float L_4 = (&___clipPlane1)->get_y_2();
		float L_5 = Water_sgn_m123353488(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Vector4_t3319028937  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector4__ctor_m2498754347((&L_6), L_3, L_5, (1.0f), (1.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Matrix4x4_t1817901843_il2cpp_TypeInfo_var);
		Vector4_t3319028937  L_7 = Matrix4x4_op_Multiply_m1839501195(NULL /*static, unused*/, L_1, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Vector4_t3319028937  L_8 = ___clipPlane1;
		Vector4_t3319028937  L_9 = ___clipPlane1;
		Vector4_t3319028937  L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t3319028937_il2cpp_TypeInfo_var);
		float L_11 = Vector4_Dot_m3492158352(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		Vector4_t3319028937  L_12 = Vector4_op_Multiply_m213790997(NULL /*static, unused*/, L_8, ((float)((float)(2.0f)/(float)L_11)), /*hidden argument*/NULL);
		V_1 = L_12;
		Matrix4x4_t1817901843 * L_13 = ___projection0;
		float L_14 = (&V_1)->get_x_1();
		Matrix4x4_t1817901843 * L_15 = ___projection0;
		float L_16 = Matrix4x4_get_Item_m567451091(L_15, 3, /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1906605342(L_13, 2, ((float)il2cpp_codegen_subtract((float)L_14, (float)L_16)), /*hidden argument*/NULL);
		Matrix4x4_t1817901843 * L_17 = ___projection0;
		float L_18 = (&V_1)->get_y_2();
		Matrix4x4_t1817901843 * L_19 = ___projection0;
		float L_20 = Matrix4x4_get_Item_m567451091(L_19, 7, /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1906605342(L_17, 6, ((float)il2cpp_codegen_subtract((float)L_18, (float)L_20)), /*hidden argument*/NULL);
		Matrix4x4_t1817901843 * L_21 = ___projection0;
		float L_22 = (&V_1)->get_z_3();
		Matrix4x4_t1817901843 * L_23 = ___projection0;
		float L_24 = Matrix4x4_get_Item_m567451091(L_23, ((int32_t)11), /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1906605342(L_21, ((int32_t)10), ((float)il2cpp_codegen_subtract((float)L_22, (float)L_24)), /*hidden argument*/NULL);
		Matrix4x4_t1817901843 * L_25 = ___projection0;
		float L_26 = (&V_1)->get_w_4();
		Matrix4x4_t1817901843 * L_27 = ___projection0;
		float L_28 = Matrix4x4_get_Item_m567451091(L_27, ((int32_t)15), /*hidden argument*/NULL);
		Matrix4x4_set_Item_m1906605342(L_25, ((int32_t)14), ((float)il2cpp_codegen_subtract((float)L_26, (float)L_28)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Water::CalculateReflectionMatrix(UnityEngine.Matrix4x4&,UnityEngine.Vector4)
extern "C"  void Water_CalculateReflectionMatrix_m1781634015 (RuntimeObject * __this /* static, unused */, Matrix4x4_t1817901843 * ___reflectionMat0, Vector4_t3319028937  ___plane1, const RuntimeMethod* method)
{
	{
		Matrix4x4_t1817901843 * L_0 = ___reflectionMat0;
		float L_1 = Vector4_get_Item_m2380866393((&___plane1), 0, /*hidden argument*/NULL);
		float L_2 = Vector4_get_Item_m2380866393((&___plane1), 0, /*hidden argument*/NULL);
		L_0->set_m00_0(((float)il2cpp_codegen_subtract((float)(1.0f), (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(2.0f), (float)L_1)), (float)L_2)))));
		Matrix4x4_t1817901843 * L_3 = ___reflectionMat0;
		float L_4 = Vector4_get_Item_m2380866393((&___plane1), 0, /*hidden argument*/NULL);
		float L_5 = Vector4_get_Item_m2380866393((&___plane1), 1, /*hidden argument*/NULL);
		L_3->set_m01_4(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_4)), (float)L_5)));
		Matrix4x4_t1817901843 * L_6 = ___reflectionMat0;
		float L_7 = Vector4_get_Item_m2380866393((&___plane1), 0, /*hidden argument*/NULL);
		float L_8 = Vector4_get_Item_m2380866393((&___plane1), 2, /*hidden argument*/NULL);
		L_6->set_m02_8(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_7)), (float)L_8)));
		Matrix4x4_t1817901843 * L_9 = ___reflectionMat0;
		float L_10 = Vector4_get_Item_m2380866393((&___plane1), 3, /*hidden argument*/NULL);
		float L_11 = Vector4_get_Item_m2380866393((&___plane1), 0, /*hidden argument*/NULL);
		L_9->set_m03_12(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_10)), (float)L_11)));
		Matrix4x4_t1817901843 * L_12 = ___reflectionMat0;
		float L_13 = Vector4_get_Item_m2380866393((&___plane1), 1, /*hidden argument*/NULL);
		float L_14 = Vector4_get_Item_m2380866393((&___plane1), 0, /*hidden argument*/NULL);
		L_12->set_m10_1(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_13)), (float)L_14)));
		Matrix4x4_t1817901843 * L_15 = ___reflectionMat0;
		float L_16 = Vector4_get_Item_m2380866393((&___plane1), 1, /*hidden argument*/NULL);
		float L_17 = Vector4_get_Item_m2380866393((&___plane1), 1, /*hidden argument*/NULL);
		L_15->set_m11_5(((float)il2cpp_codegen_subtract((float)(1.0f), (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(2.0f), (float)L_16)), (float)L_17)))));
		Matrix4x4_t1817901843 * L_18 = ___reflectionMat0;
		float L_19 = Vector4_get_Item_m2380866393((&___plane1), 1, /*hidden argument*/NULL);
		float L_20 = Vector4_get_Item_m2380866393((&___plane1), 2, /*hidden argument*/NULL);
		L_18->set_m12_9(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_19)), (float)L_20)));
		Matrix4x4_t1817901843 * L_21 = ___reflectionMat0;
		float L_22 = Vector4_get_Item_m2380866393((&___plane1), 3, /*hidden argument*/NULL);
		float L_23 = Vector4_get_Item_m2380866393((&___plane1), 1, /*hidden argument*/NULL);
		L_21->set_m13_13(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_22)), (float)L_23)));
		Matrix4x4_t1817901843 * L_24 = ___reflectionMat0;
		float L_25 = Vector4_get_Item_m2380866393((&___plane1), 2, /*hidden argument*/NULL);
		float L_26 = Vector4_get_Item_m2380866393((&___plane1), 0, /*hidden argument*/NULL);
		L_24->set_m20_2(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_25)), (float)L_26)));
		Matrix4x4_t1817901843 * L_27 = ___reflectionMat0;
		float L_28 = Vector4_get_Item_m2380866393((&___plane1), 2, /*hidden argument*/NULL);
		float L_29 = Vector4_get_Item_m2380866393((&___plane1), 1, /*hidden argument*/NULL);
		L_27->set_m21_6(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_28)), (float)L_29)));
		Matrix4x4_t1817901843 * L_30 = ___reflectionMat0;
		float L_31 = Vector4_get_Item_m2380866393((&___plane1), 2, /*hidden argument*/NULL);
		float L_32 = Vector4_get_Item_m2380866393((&___plane1), 2, /*hidden argument*/NULL);
		L_30->set_m22_10(((float)il2cpp_codegen_subtract((float)(1.0f), (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(2.0f), (float)L_31)), (float)L_32)))));
		Matrix4x4_t1817901843 * L_33 = ___reflectionMat0;
		float L_34 = Vector4_get_Item_m2380866393((&___plane1), 3, /*hidden argument*/NULL);
		float L_35 = Vector4_get_Item_m2380866393((&___plane1), 2, /*hidden argument*/NULL);
		L_33->set_m23_14(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_34)), (float)L_35)));
		Matrix4x4_t1817901843 * L_36 = ___reflectionMat0;
		L_36->set_m30_3((0.0f));
		Matrix4x4_t1817901843 * L_37 = ___reflectionMat0;
		L_37->set_m31_7((0.0f));
		Matrix4x4_t1817901843 * L_38 = ___reflectionMat0;
		L_38->set_m32_11((0.0f));
		Matrix4x4_t1817901843 * L_39 = ___reflectionMat0;
		L_39->set_m33_15((1.0f));
		return;
	}
}
// System.Void Water::.cctor()
extern "C"  void Water__cctor_m1600874932 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WorldCanvasUI::.ctor()
extern "C"  void WorldCanvasUI__ctor_m4011242336 (WorldCanvasUI_t1156772134 * __this, const RuntimeMethod* method)
{
	{
		__this->set_renderMode_3(2);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GameObject WorldCanvasUI::get_CanvasGO()
extern "C"  GameObject_t1113636619 * WorldCanvasUI_get_CanvasGO_m332321093 (WorldCanvasUI_t1156772134 * __this, const RuntimeMethod* method)
{
	{
		GameObject_t1113636619 * L_0 = __this->get_canvasGO_4();
		return L_0;
	}
}
// System.Void WorldCanvasUI::Awake()
extern "C"  void WorldCanvasUI_Awake_m2157806672 (WorldCanvasUI_t1156772134 * __this, const RuntimeMethod* method)
{
	{
		WorldCanvasUI_InitWorldCanvasUI_m724433920(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WorldCanvasUI::Update()
extern "C"  void WorldCanvasUI_Update_m3332101880 (WorldCanvasUI_t1156772134 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void WorldCanvasUI::InitWorldCanvasUI()
extern "C"  void WorldCanvasUI_InitWorldCanvasUI_m724433920 (WorldCanvasUI_t1156772134 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WorldCanvasUI_InitWorldCanvasUI_m724433920_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Canvas_t3310196443 * V_1 = NULL;
	GraphicRaycaster_t2999697109 * V_2 = NULL;
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		String_t* L_1 = Object_get_name_m4211327027(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m3937257545(NULL /*static, unused*/, L_1, _stringLiteral4236429424, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Canvas_t3310196443 * L_4 = GameObject_GetComponentInChildren_TisCanvas_t3310196443_m2710541779(L_3, /*hidden argument*/GameObject_GetComponentInChildren_TisCanvas_t3310196443_m2710541779_RuntimeMethod_var);
		V_1 = L_4;
		Canvas_t3310196443 * L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_5, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0046;
		}
	}
	{
		Canvas_t3310196443 * L_7 = V_1;
		String_t* L_8 = Object_get_name_m4211327027(L_7, /*hidden argument*/NULL);
		String_t* L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m920492651(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0046;
		}
	}
	{
		Canvas_t3310196443 * L_11 = V_1;
		__this->set_worldCanvas_2(L_11);
	}

IL_0046:
	{
		Canvas_t3310196443 * L_12 = __this->get_worldCanvas_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_12, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_008f;
		}
	}
	{
		String_t* L_14 = V_0;
		GameObject_t1113636619 * L_15 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m2093116449(L_15, L_14, /*hidden argument*/NULL);
		__this->set_canvasGO_4(L_15);
		GameObject_t1113636619 * L_16 = __this->get_canvasGO_4();
		Transform_t3600365921 * L_17 = GameObject_get_transform_m1369836730(L_16, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_18 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_19 = GameObject_get_transform_m1369836730(L_18, /*hidden argument*/NULL);
		Transform_SetParent_m381167889(L_17, L_19, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_20 = __this->get_canvasGO_4();
		Canvas_t3310196443 * L_21 = GameObject_AddComponent_TisCanvas_t3310196443_m284883347(L_20, /*hidden argument*/GameObject_AddComponent_TisCanvas_t3310196443_m284883347_RuntimeMethod_var);
		__this->set_worldCanvas_2(L_21);
	}

IL_008f:
	{
		GameObject_t1113636619 * L_22 = __this->get_canvasGO_4();
		GraphicRaycaster_t2999697109 * L_23 = GameObject_GetComponent_TisGraphicRaycaster_t2999697109_m3357594245(L_22, /*hidden argument*/GameObject_GetComponent_TisGraphicRaycaster_t2999697109_m3357594245_RuntimeMethod_var);
		V_2 = L_23;
		GraphicRaycaster_t2999697109 * L_24 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_25 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_24, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00b3;
		}
	}
	{
		GameObject_t1113636619 * L_26 = __this->get_canvasGO_4();
		GraphicRaycaster_t2999697109 * L_27 = GameObject_AddComponent_TisGraphicRaycaster_t2999697109_m1217468238(L_26, /*hidden argument*/GameObject_AddComponent_TisGraphicRaycaster_t2999697109_m1217468238_RuntimeMethod_var);
		V_2 = L_27;
	}

IL_00b3:
	{
		Canvas_t3310196443 * L_28 = __this->get_worldCanvas_2();
		int32_t L_29 = __this->get_renderMode_3();
		Canvas_set_renderMode_m1564704306(L_28, L_29, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
